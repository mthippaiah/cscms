﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using Microsoft.SqlServer.Dts.Runtime;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Data.EntityClient;
using System.Data;
using CSCMS.Data;
using System.Globalization;

namespace CSCMS.OAGCaseImport
{
    class Program
    {
        static void Main(string[] args)
        {
            string packageFile = ConfigurationManager.AppSettings["DTSXFile"];
            string inputFolder = ConfigurationManager.AppSettings["InputFolder"];
            string dtsConfigFile = ConfigurationManager.AppSettings["DTSXConfigFile"];
            string fileMask = "*.DATA";
            try
            {
                string[] foundFiles = System.IO.Directory.GetFiles(inputFolder, fileMask);
                if (foundFiles.Length > 0)
                {
                    //Just make sure that the file available and not in use
                    FileStream fs = System.IO.File.Open(foundFiles[0], System.IO.FileMode.Open);
                    fs.Close();

                    string targetFileName = inputFolder + "\\" + "OAGData.txt";
                    System.IO.File.Move(foundFiles[0], targetFileName);

                    Application app = new Application();
                    // Load package from file system
                    Package package = app.LoadPackage(packageFile, null);
                    package.ImportConfigurationFile(dtsConfigFile);
                    Variables vars = package.Variables;

                    vars["FileName"].Value = targetFileName;
                    vars["InputFolder"].Value = inputFolder;
             
                    DTSExecResult result = package.Execute();
                    Console.WriteLine("Package Execution results: {0}", result.ToString());
                    bool dataImported = false;
                    using (CscmsEntities CscmsContext = new CscmsEntities())
                    {
                        CscmsContext.Connection.Open();

                        dataImported = CscmsContext.OAGDATA.Count() > 0;
                        List<OCACourt> courtList = CscmsContext.OCACourt.Where(it => it.ExpirationDate == null || it.ExpirationDate > DateTime.Now).ToList();
                        SqlConnection conn = (SqlConnection)((EntityConnection)CscmsContext.Connection).StoreConnection;

                        foreach (OCACourt court in courtList)
                        {
                            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(conn,
                                            CommandType.StoredProcedure,
                                            "OAG_GetNewCasesForCourt",
                                            new SqlParameter("@OCACourtID", court.OCACourtID));

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                //long id = (long)row["Id"];
                                string causeNumber = (string)row["CauseNumber"];
                                string ouNumber = (string)row["OuNumber"];

                                //OAGDATA c = CscmsContext.OAGDATA.Where(it => it.Id == id).FirstOrDefault();
                                OAGDATA c = CscmsContext.OAGDATA.Where(it => it.CauseNumber == causeNumber && it.OuNumber == ouNumber).FirstOrDefault();
                                County county = CscmsContext.OAGCountyCodeMap.Where(it => it.CountyCode == c.CountyCode).Select(it => it.County).FirstOrDefault();

                                //Add Cause
                                Cause cause = AddCause(CscmsContext, c, court, county);

                                //Add an Action
                                ActionType at = CscmsContext.OAGActionMap.Where(it => it.OAGActionCode == c.LegalActnCode).Select(it => it.ActionType).FirstOrDefault();
                                CSCMS.Data.Action a = AddAction(CscmsContext, c, cause, at);


                                //Add Action Person
                                AddActionPersonHelper(CscmsContext, a, c.MbrSrvdFname1, c.MbrSrvdMname1, c.MbrSrvdLname1, c.MbrSrvdRtc1, c.MbrSrvdRtd1);
                                AddActionPersonHelper(CscmsContext, a, c.MbrSrvdFname2, c.MbrSrvdMname2, c.MbrSrvdLname2, c.MbrSrvdRtc2, c.MbrSrvdRtd2);
                                AddActionPersonHelper(CscmsContext, a, c.MbrSrvdFname3, c.MbrSrvdMname3, c.MbrSrvdLname3, c.MbrSrvdRtc3, c.MbrSrvdRtd3);
                                AddActionPersonHelper(CscmsContext, a, c.MbrSrvdFname4, c.MbrSrvdMname4, c.MbrSrvdLname4, c.MbrSrvdRtc4, c.MbrSrvdRtd4);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname1, c.DpMname1, c.DpLname1, c.DpDOB1);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname2, c.DpMname2, c.DpLname2, c.DpDOB2);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname3, c.DpMname3, c.DpLname3, c.DpDOB3);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname4, c.DpMname4, c.DpLname4, c.DpDOB4);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname5, c.DpMname5, c.DpLname5, c.DpDOB5);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname6, c.DpMname6, c.DpLname6, c.DpDOB6);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname7, c.DpMname7, c.DpLname7, c.DpDOB7);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname8, c.DpMname8, c.DpLname8, c.DpDOB8);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname9, c.DpMname9, c.DpLname9, c.DpDOB9);
                                AddActionPersonHelper(CscmsContext, a, c.DpFname10, c.DpMname10, c.DpLname10, c.DpDOB10);
                                AddHearing(CscmsContext, a, c.DocketDate);

                                //Add case imported
                                CSCMS.Data.OAGImportedCase caseAdded = new OAGImportedCase();
                                caseAdded.CauseNumber = causeNumber;
                                caseAdded.OCACourt = court;
                                caseAdded.CreateDate = DateTime.Now;
                                CscmsContext.AddToOAGImportedCase(caseAdded);
                            }
                            CscmsContext.SaveChanges();
                        }

                    }

                    if (File.Exists(targetFileName) && dataImported)
                        File.Delete(targetFileName);

                }
            }
            catch (Exception ex)
            {
                Console.Write("Exception occurred: " + ex.ToString());
            }
        }

        private static void AddHearing(CscmsEntities CscmsContext, CSCMS.Data.Action action, string p)
        {
            DateTime? dt = ConvertToDate(p, "DDMMYYYY");

            if (dt != null)
            {
                CSCMS.Data.Hearing h = new Hearing();
                h.Action = action;
                h.HearingDate = dt.Value;
                h.HearingTypeID = 1;
                h.HearingTime = CscmsContext.OCACourt.Where(it => it.OCACourtID == action.Cause.OCACourt.OCACourtID).Select(it => it.DefaultHearingTime).FirstOrDefault();
                h.MorningDocket = true;
                h.CreateDate = DateTime.Now;
                h.LastUpdateDate = DateTime.Now;

                CscmsContext.AddToHearing(h);
            }
        }


        private static CSCMS.Data.FatherType GetFatherType(CscmsEntities CscmsContext, string name)
        {
            CSCMS.Data.FatherType ft = null;

            switch (name)
            {
                case "PF":
                    ft = CscmsContext.FatherType.Where(it => it.FatherTypeName == "Presumed (PRS)").FirstOrDefault();
                    break;
                case "AF":
                    ft = CscmsContext.FatherType.Where(it => it.FatherTypeName == "Alleged (ALG)").FirstOrDefault();
                    break;
            }
            return ft;
        }

        private static CSCMS.Data.CustodialType GetCustodialType(CscmsEntities CscmsContext, string name)
        {
            CSCMS.Data.CustodialType ct = null;

            switch (name)
            {
                case "AP":
                    ct = CscmsContext.CustodialType.Where(it => it.CustodialTypeName == "Non-Custodial").FirstOrDefault();
                    break;
                case "CP":
                    ct = CscmsContext.CustodialType.Where(it => it.CustodialTypeName == "Custodial").FirstOrDefault();
                    break;
            }
            return ct;
        }

        private static void AddActionPersonHelper(CscmsEntities CscmsContext, CSCMS.Data.Action action, string fname, string mname, string lname, string dob)
        {
            //Add Person
            if (!string.IsNullOrEmpty(fname) || !string.IsNullOrEmpty(mname) || !string.IsNullOrEmpty(lname))
            {
                CSCMS.Data.Person p = AddPerson(CscmsContext, fname, mname, lname, dob);

                //Add ActionPerson
                CSCMS.Data.ActionPerson ap = AddActionPerson(CscmsContext, action, p, null, null, null);
            }
        }

        private static CSCMS.Data.Person AddPerson(CscmsEntities CscmsContext, string fname, string mname, string lname, string dob)
        {
            CSCMS.Data.Person p = new Person();
            p.PersonGUID = Guid.NewGuid();
            p.CriminalSystemHistory = false;
            p.SecurityRisk = false;
            p.Deceased = false;
            p.InterpreterNeeded = false;
            p.Disability = false;
            p.NPMinorFlag = false;
            p.CreateDate = DateTime.Now;
            p.LastUpdateDate = DateTime.Now;
            p.FirstName = fname;
            p.MiddleName = mname;
            p.LastName = lname;

            if (!string.IsNullOrEmpty(dob))
                p.Birthdate = ConvertToDate(dob, "MMDDYYYY");

            CscmsContext.AddToPerson(p);
            return p;
        }

        private static CSCMS.Data.ActionPerson AddActionPerson(CscmsEntities CscmsContext, CSCMS.Data.Action action, CSCMS.Data.Person p, CSCMS.Data.PersonType pt, CSCMS.Data.CustodialType ct, CSCMS.Data.FatherType ft)
        {
            CSCMS.Data.ActionPerson ap = new ActionPerson();
            ap.Action = action;
            ap.Person = p;
            ap.PersonType = pt;
            ap.FatherType = ft;
            ap.CustodialType = ct;
            ap.NPFlag = true;
            ap.NonDisclosureFinding = false;
            ap.IndigenceHearing = false;
            ap.RequestedAdditionalTime = false;
            ap.NeedAttorneyAppointed = false;
            ap.CreateDate = DateTime.Now;
            ap.LastUpdateDate = DateTime.Now;
            CscmsContext.AddToActionPerson(ap);
            return ap;
        }

        private static void AddActionPersonHelper(CscmsEntities CscmsContext, CSCMS.Data.Action action, string fname, string mname, string lname, string custodialType, string personType)
        {
            if (!string.IsNullOrEmpty(fname) || !string.IsNullOrEmpty(mname) || !string.IsNullOrEmpty(lname))
            {
                CSCMS.Data.PersonType pt = CscmsContext.OAGPersonTypeMap.Where(it => it.Code == personType).Select(it => it.PersonType).FirstOrDefault();
                CSCMS.Data.FatherType ft = GetFatherType(CscmsContext, custodialType);
                CSCMS.Data.CustodialType ct = GetCustodialType(CscmsContext, custodialType);

                //Add Person
                CSCMS.Data.Person p = AddPerson(CscmsContext, fname, mname, lname, "");

                //Add ActionPerson
                CSCMS.Data.ActionPerson ap = AddActionPerson(CscmsContext, action, p, pt, ct, ft);
            }
        }

        private static CSCMS.Data.Action AddAction(CscmsEntities CscmsContext, OAGDATA c, Cause cause, ActionType at)
        {
            CSCMS.Data.Action a = new CSCMS.Data.Action();
            a.ActionGUID = Guid.NewGuid();
            a.Cause = cause;
            a.ActionNumber = 1;
            a.OAGCauseNumber = c.CsdCaseId;
            a.ActionType = at;
            a.ActionFiledDate = ConvertToDate(c.FileDate, "DDMMYYYY").Value;
            a.Inactive = true;
            a.ActionStatus = "A";
            a.CreateDate = DateTime.Now;
            a.LastUpdateDate = DateTime.Now;
            CscmsContext.AddToAction(a);
            return a;
        }

        private static Cause AddCause(CscmsEntities CscmsContext, OAGDATA c, OCACourt court, County county)
        {
            Cause cause = new Cause();
            cause.CauseGUID = Guid.NewGuid();
            cause.CauseNumber = c.CauseNumber;
            cause.CauseNumberClean = c.CauseNumber.Replace("-", "").Replace(",", "").Replace(" ", "").Replace("(", "").Replace(")", "").Replace("/", "").Replace(@"\", "").Replace(";", "").Replace("%", "").Replace(@"&", "");
            cause.OCACourt = court;
            cause.County = county;
            cause.NumberOfAction = 1;
            cause.LastActionFiledDate = ConvertToDate(c.FileDate, "DDMMYYYY").Value;
            cause.Inactive = true;
            cause.CauseStatus = "A";
            cause.OCAUnit = c.OuNumber;
            cause.CreateDate = DateTime.Now;
            cause.LastUpdateDate = DateTime.Now;
            cause.LastAccessDate = DateTime.Now;
            CscmsContext.AddToCause(cause);

            return cause;
        }

        private static DateTime? ConvertToDate(string date, string format)
        {
            DateTime? dt = null;
            if (format == "DDMMYYYY") //docketdate and filedate
            {
                date = string.Format("{0}/{1}/{2}", date.Substring(0, 2), date.Substring(2, 2), date.Substring(4, 4));
                dt = DateTime.Parse(date, new CultureInfo("fr-FR", false));
            }
            if (format == "MMDDYYYY")
            {
                date = string.Format("{0}/{1}/{2}", date.Substring(0, 2), date.Substring(2, 2), date.Substring(4, 4));
                dt = DateTime.Parse(date);
            }
            return dt;
        }

    }
}

