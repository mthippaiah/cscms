﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    public string FileUploadInstallUrl
    {
        get
        {
            return (this.Request.ApplicationPath == "/" ? "" : Request.ApplicationPath) + "/bin/Setup.exe";
        }
    }

    public bool IsIE
    {
        get
        {
            if (Request.Browser.Browser == "IE")
                return true;

            return false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.fileUploadInstall.NavigateUrl = FileUploadInstallUrl;
        divActiveX.Visible = IsIE;
        this.divFu.Visible = (IsIE == false);
        this.hfActionGUID.Value = Guid.NewGuid().ToString();
        this.hfUserID.Value = 24.ToString();
    }

    protected void btnUpload_OnClick(object sender, EventArgs args)
    {
    }
}
