﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .column1
        {
            float: left;
            width: 150px;
            text-align: right;
            height: 28px;
            vertical-align: middle;
        }
        .column2
        {
            float: right;
            width: 439px;
            height: 28px;
            text-align: left;
            vertical-align: middle;
        }
        .outerFrame
        {
            width: 600px;
        }
        .buttons
        {
            text-align: center;
            clear: right;
        }
        .ActivexDownloadLink
        {
            display: none;
        }
    </style>

    <script type="text/javascript" language="javascript">

        function verifyactivex() {
            //debugger;
            try {
                var obj = new ActiveXObject("FileUploadActiveX.FileUploadX");
                if (obj == null || obj == undefined) {
                    SetFileUploadVisible();
                }
            }
            catch (Err) {
                SetFileUploadVisible();
            }
        }

        function SetFileUploadVisible() {
            var id = document.getElementById("<%= divFileUpLoadActivex.ClientID %>");
            id.style.display = 'block';
        }

        function UploadFiles() {
            //debugger;
            try {
                if (Page_ClientValidate() == false)
                    return false;

                var fileupload = document.getElementById("fileuploadId");
                var q = fileupload.GetFile();
                if (q != null || q != undefined) {
                    var uid = document.getElementById("hfUserID");
                    var aid = document.getElementById("hfActionGUID");
                    var xml = '<data>' + element('UserID', uid.value) +
                        element('ActionGUID', aid.value) + '</data>';
                    fileupload.SetDocumentMetaData(xml);
                    fileupload.Upload();
                    var le = fileupload.GetLastError();
                    if (le != null) {
                        alert(le);
                        return false;
                    }
                    return true;
                }
            }
            catch (Err) {
                alert(Err.description);
            }

            return false;
        }

        function element(name, content) {
            var xml
            if (!content) {
                xml = '<' + name + '/>'
            }
            else {
                xml = '<' + name + '>' + content + '</' + name + '>'
            }
            return xml
        }
    </script>

</head>
<body onload="verifyactivex()" style="background-color: #e3f1fe">
    <form id="frm" runat="server">
    <div class="outerFrame">
        <div class="column1">
            Document to Upload:
        </div>
        <div class="column2" id="divActiveX" runat="server">
            <object id="fileuploadId" name="fileupload" classid="clsid:0D454BAF-AC25-40f9-AB31-DC745044E65B"
                width="400" height="28" style="background-color: inherit">
                <param name="UploadUrl" value='http://localhost/DragDropTest/Document.aspx' />
                <param name="FormFileInputCtrlId" value="caseFile" />
                <param name="FileSizeLimitinMB" value="10" />
                <param name="AllowedFileExtensions" value="txt,doc,pdf,xls,jpeg,jpg,gif" />
            </object>
        </div>
        <div class="column2" id="divFu" runat="server">
            <asp:FileUpload ID="FileUpload1" runat="server" />
        </div>
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfActionGUID" runat="server" />
        <div class="column1">
            Document Description:
        </div>
        <div class="column2">
            <asp:TextBox ID="txtDocDesc" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqVal" runat="server" ControlToValidate="txtDocDesc"
                ErrorMessage="Needs description">*</asp:RequiredFieldValidator>
        </div>
        <br />
        <br />
        <div class="buttons">
            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClientClick="return UploadFiles();"
                OnClick="btnUpload_OnClick" />
        </div>
        <br />
        <div id="divFileUpLoadActivex" runat="server" style="display: none">
            <pre>
            Please click the link below to install the Fileupload ActiveX control. Then refresh the screen.
            If you see an error, contact system administrator and tell them to set "Read" for the folder containing the
            setup.exe.
            </pre>
            <br />
            <asp:HyperLink ID="fileUploadInstall" runat="server" Text="Install  Fileupload Activex"></asp:HyperLink>
        </div>
    </div>
    </form>
</body>
</html>
