﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Document : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string userId = "";
        string actionGuid = "";
        if (this.Page.Request.Form["UserID"] != null)
        {
            userId = this.Page.Request.Form["UserID"];
        }
        if (this.Page.Request.Form["ActionGUID"] != null)
        {
            actionGuid = this.Page.Request.Form["ActionGUID"];
        }
        // Check to see if a file was actually selected
        if (caseFile.PostedFile != null && caseFile.PostedFile.ContentLength > 0)
        {
            string fileUploadPath = @"c:\FileUpload";
            fileUploadPath = fileUploadPath.TrimEnd('\\');

            Guid aid = new Guid(actionGuid);

            short courtId = 37;
         
            if (!Directory.Exists(fileUploadPath + "\\" + courtId.ToString()))
            {
                Directory.CreateDirectory(fileUploadPath + "\\" + courtId.ToString());
                Directory.CreateDirectory(fileUploadPath + "\\" + courtId.ToString() + "\\" + actionGuid);
            }
            else if (!Directory.Exists(fileUploadPath + "\\" + courtId.ToString() + "\\" + actionGuid))
            {
                Directory.CreateDirectory(fileUploadPath + "\\" + courtId.ToString() + "\\" + actionGuid);
            }

            // Get the filename and folder to write to
            string fileName = Path.GetFileName(caseFile.PostedFile.FileName);
            // Save the file to the folder
            caseFile.PostedFile.SaveAs(Path.Combine(fileUploadPath + "\\" + courtId.ToString() + "\\" + actionGuid + "\\", fileName));
            Response.Write("File upload complete, UserID: " + userId + " ActionGUID: " + actionGuid);
        }
    }
}
