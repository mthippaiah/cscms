﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Threading;
using System.IO;
using System.Net;
using System.Xml;
using Microsoft.Win32;

namespace FileUploadActiveX
{
    [ProgId("FileUploadActiveX.FileUploadX")]
    [Guid("0D454BAF-AC25-40f9-AB31-DC745044E65B")]
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public partial class FileUploadX : UserControl, IDropTargetControl, IObjectSafety
    {
        public event DroppedByteArrayAvailableHandler DroppedByteArrayAvailable;
        string _postUrl = "http://localhost/dragdroptest/document.aspx";
        string fileInputCtlId = "file";
        int fileSizeLimit = 10; //in mb
        string allowedFileExtensions = "pdf, doc, xls";
        string xmlData = "";
        string backColor = "227, 241, 254";
        private bool registeredForDragDrop = false;
        private static readonly Hashtable lookupImageForExtension = new Hashtable();
        FileListItem fileItem = null;
        string lastErrorMessage = string.Empty;

        public FileUploadX()
        {
            InitializeComponent();
            Application.OleRequired();
        }

        private void FileUploadX_VisibleChanged(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                this.BackColor = Color.FromArgb(227, 241, 254);
                if (Visible && !registeredForDragDrop)
                {
                    RegisterForDragDrop();
                }
                else if (registeredForDragDrop)
                {
                    UnregisterForDragDrop();
                }
            }
        }

        private void RegisterForDragDrop()
        {
            if (FindForm() != null)
            {
                Console.WriteLine("Become visible - registering: {0}", FindForm().Visible);
            }
            FileDropTarget dt = new FileDropTarget(this);
            dt.StreamDropped += new StreamDroppedHandler(dt_StreamDropped);
            dt.StorageDropped += new StorageDroppedHandler(dt_StorageDropped);
            dt.FilenameDropped += new FilenameDroppedHandler(dt_FilenameDropped);
            NativeMethods.RegisterDragDrop(new HandleRef(this, Handle), dt);
            registeredForDragDrop = true;

        }

        private void UnregisterForDragDrop()
        {
            Console.WriteLine("Become hidden - unregistering");
            NativeMethods.RevokeDragDrop(new HandleRef(this, Handle));
            registeredForDragDrop = false;
        }

        void IDropTargetControl.Activate()
        {
            Form f = FindForm();
            if (f != null)
            {
                f.Activate();
            }
        }

        void IDropTargetControl.DragDrop()
        {
            //pictureBox1.Image = null;
            //pictureBox1.BackgroundImage = global::DragDropPanel.Properties.Resources.down;
        }

        void IDropTargetControl.DragEnter(string filename)
        {
            //pictureBox1.Image = GetImageForFilename(filename);
        }

        void IDropTargetControl.DragLeave()
        {
            //pictureBox1.Image = null;
        }

        void IDropTargetControl.DragOver()
        {
        }

        private void dt_StreamDropped(object sender, string filename, NativeMethods.IStream stream)
        {
            try
            {
                NativeMethods.STATSTG stg = new NativeMethods.STATSTG();
                stream.Stat(stg, 0);
                IntPtr buf = Marshal.AllocHGlobal((int)stg.cbSize);
                IntPtr lockedBuf = NativeMethods.GlobalLock(new HandleRef(this, buf));
                int x = stream.Read(buf, (int)stg.cbSize);
                byte[] bytes = new byte[stg.cbSize];
                Marshal.Copy(buf, bytes, 0, (int)stg.cbSize);
                NativeMethods.GlobalUnlock(new HandleRef(this, buf));
                Marshal.FreeHGlobal(buf);
                string msg = IsvalidFile(filename, bytes.Length);
                if (!string.IsNullOrEmpty(msg))
                {
                    MessageBox.Show(msg);
                    return;
                }

                fileItem = new FileListItem { FileData = bytes, FileName = filename, ShortFileName = filename, FileSize = bytes.Length };
                this.txtFile.Text = filename;
                if (DroppedByteArrayAvailable != null)
                {
                    DroppedByteArrayAvailable(this, filename, bytes);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void dt_StorageDropped(object sender, string filename, NativeMethods.IStorage storage)
        {
            try
            {
                uint grfFlags = NativeMethods.STGM_CREATE |
                NativeMethods.STGM_READWRITE |
                NativeMethods.STGM_SHARE_EXCLUSIVE;
                NativeMethods.ILockBytes lpBytes = NativeMethods.CreateILockBytesOnHGlobal(IntPtr.Zero, true);
                NativeMethods.IStorage lpDest = NativeMethods.StgCreateDocfileOnILockBytes(lpBytes, grfFlags, 0);
                storage.CopyTo(0, null, IntPtr.Zero, lpDest);
                lpBytes.Flush();
                lpDest.Commit(NativeMethods.STGC_DEFAULT);
                NativeMethods.STATSTG pStatstg = new NativeMethods.STATSTG();
                lpBytes.Stat(pStatstg, NativeMethods.STATFLAG_NONAME);
                IntPtr hGlobal = NativeMethods.GetHGlobalFromILockBytes(lpBytes);
                HandleRef hRef = new HandleRef(this, hGlobal);
                IntPtr lpBuf = NativeMethods.GlobalLock(hRef);
                byte[] bytes = new byte[pStatstg.cbSize];
                Marshal.Copy(lpBuf, bytes, 0, (int)pStatstg.cbSize);
                string msg = IsvalidFile(filename, bytes.Length);
                if (!string.IsNullOrEmpty(msg))
                {
                    MessageBox.Show(msg);
                    return;
                }
                fileItem = new FileListItem { FileData = bytes, FileName = filename, ShortFileName = filename, FileSize = bytes.Length };
                this.txtFile.Text = filename;
                if (DroppedByteArrayAvailable != null)
                {
                    DroppedByteArrayAvailable(this, filename, bytes);
                }
                NativeMethods.GlobalUnlock(hRef);
                Marshal.ReleaseComObject(lpDest);
                Marshal.ReleaseComObject(lpBytes);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void dt_FilenameDropped(object sender, string filename)
        {
            using (FileStream stream = File.OpenRead(filename))
            {
                long length = stream.Length;
                byte[] buffer = new byte[length];
                stream.Read(buffer, 0, (int)length);
                string shortFilename = Path.GetFileName(filename);
                string msg = IsvalidFile(filename, buffer.Length);
                if (!string.IsNullOrEmpty(msg))
                {
                    MessageBox.Show(msg);
                    return;
                }
                fileItem = new FileListItem { FileData = buffer, FileName = filename, ShortFileName = shortFilename, FileSize = length };
                this.txtFile.Text = filename;
                if (DroppedByteArrayAvailable != null)
                {
                    DroppedByteArrayAvailable(this, shortFilename, buffer);
                }
            }
        }

        private string IsvalidFile(string filename, int bytes)
        {
            string resp = "";

            string extn = Path.GetExtension(filename);
            extn = extn.Remove(0, 1);
            string[] arr = AllowedFileExtensions.Split(',');
            bool found = false;
            foreach (string s in arr)
            {
                if (s.Trim().ToLower() == extn.ToLower())
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                return filename + ". File type is not allowed.";
            }

            int mb = (bytes / (1024 * 1024));

            if (mb > this.FileSizeLimitinMB)
            {
                return filename + ". File size exceeds the limit allowed";
            }

            return resp;
        }

        [ComVisible(true)]
        public string UploadUrl
        {
            get
            {
                return _postUrl;
            }
            set
            {
                _postUrl = value;
            }
        }

        [ComVisible(true)]
        public string FormFileInputCtrlId
        {
            get
            {
                return fileInputCtlId;
            }
            set
            {
                fileInputCtlId = value;
            }
        }

        [ComVisible(true)]
        public int FileSizeLimitinMB
        {
            get
            {
                return fileSizeLimit;
            }
            set
            {
                fileSizeLimit = value;
            }
        }

        [ComVisible(true)]
        public string AllowedFileExtensions
        {
            get
            {
                return allowedFileExtensions;
            }
            set
            {
                allowedFileExtensions = value;
            }
        }

        [ComVisible(true)]
        public string BgColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
            }
        }

        [DispId(1)]
        [ComVisible(true)]
        public void Upload()
        {
            lastErrorMessage = "";
            Stream rs = null;
            WebResponse wresp = null;
            StreamReader reader2 = null;
            HttpWebRequest wr = null;
            int bytesCopied = 0;
            int totBytes = fileItem.FileData.Length;
            int remainingbytes = 0;
            int offset = 0;
            int chunkSize = 32768;
            string sbFormData = "";

            try
            {
                RegistryKey k = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\OCA.FileUploadActiveX", RegistryKeyPermissionCheck.ReadSubTree, System.Security.AccessControl.RegistryRights.ReadKey);
                if (k != null)
                {
                    string str = (string)k.GetValue("ChunkSize");
                    chunkSize = int.Parse(str);
                }
                string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(this.xmlData);

                XmlElement elem = doc.CreateElement("SecretWord");
                elem.InnerText = "ActiveXFileUpload";

                doc.FirstChild.AppendChild(elem);

                elem = doc.CreateElement("SeekPosition");
                elem.InnerText = "0";

                doc.FirstChild.AppendChild(elem);

                elem = doc.CreateElement("FileLength");
                elem.InnerText = totBytes.ToString();

                doc.FirstChild.AppendChild(elem);

                string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}\r\n";
                foreach (XmlNode node in doc.FirstChild.ChildNodes)
                {
                    sbFormData += string.Format("--{0}\r\n", boundary);
                    sbFormData += string.Format(formdataTemplate, node.Name, node.InnerText);
                }

                sbFormData += string.Format("--{0}\r\n", boundary);

                string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n";
                sbFormData += string.Format(headerTemplate, FormFileInputCtrlId, fileItem.ShortFileName);

                byte[] header = Encoding.UTF8.GetBytes(sbFormData);
                byte[] footer = Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
                long contentLength = header.Length + fileItem.FileSize + footer.Length;

                //When SendChunked = true, then we don't need Content-Length
                // Also, when SendChunked = true, we need to use HttpVersion 1.1
                wr = (HttpWebRequest)WebRequest.Create(UploadUrl);
                wr.ContentType = "multipart/form-data; boundary=" + boundary;
                wr.Method = "POST";
                wr.ContentLength = contentLength;
                wr.AllowWriteStreamBuffering = false;
                wr.Timeout = 600000;
                wr.KeepAlive = false;
                wr.ReadWriteTimeout = 600000;
                wr.ProtocolVersion = HttpVersion.Version11;
                wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
                wr.SendChunked = true;
                wr.UserAgent = "Mozilla/3.0 (compatible; My Browser/1.0)";
                rs = wr.GetRequestStream();

                rs.Write(header, 0, header.Length);
           
                byte[] temp = new byte[chunkSize];

                while (true)
                {
                    remainingbytes = totBytes - bytesCopied;
                    if (remainingbytes > chunkSize)
                    {
                        offset += bytesCopied;
                        Array.Copy(fileItem.FileData, bytesCopied, temp, 0, chunkSize);
                        rs.Write(temp, 0, temp.Length);
                        rs.Flush();
                        bytesCopied += chunkSize;
                    }
                    else
                    {
                        temp = new byte[remainingbytes];
                        Array.Copy(fileItem.FileData, bytesCopied, temp, 0, remainingbytes);
                        rs.Write(temp, 0, temp.Length);
                        rs.Flush();
                        bytesCopied += remainingbytes;
                        break;
                    }
                }
                rs.Write(footer, 0, footer.Length);
                rs.Flush();
                rs.Close();
                rs = null;
                fileItem.Sent = bytesCopied;
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                reader2 = new StreamReader(stream2);
                string target = reader2.ReadToEnd();
            }
            catch (Exception ex)
            {
                lastErrorMessage = ex.Message;
            }
            finally
            {
                if (rs != null)
                    rs.Close();

                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                if (reader2 != null)
                    reader2.Close();
                wr = null;
            }

        }

        [DispId(2)]
        [ComVisible(true)]
        public void SetDocumentMetaData([MarshalAs(UnmanagedType.BStr)]string xmlData)
        {
            this.xmlData = xmlData;
        }

        [DispId(2)]
        [ComVisible(true)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public object GetFile()
        {
            if (string.IsNullOrEmpty(this.txtFile.Text))
                return null;
            else
                return (object)this.txtFile.Text;
        }

        [DispId(3)]
        [ComVisible(true)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public object GetLastError()
        {
            if (string.IsNullOrEmpty(this.lastErrorMessage))
                return null;
            else
                return (object)this.lastErrorMessage;
        }

        ///	<summary>
        ///	Register the class as a	control	and	set	it's CodeBase entry
        ///	</summary>
        ///	<param name="key">The registry key of the control</param>
        [ComRegisterFunction()]
        public static void RegisterClass(string key)
        {
            // Strip off HKEY_CLASSES_ROOT\ from the passed key as I don't need it
            StringBuilder sb = new StringBuilder(key);

            sb.Replace(@"HKEY_CLASSES_ROOT\", "");
            // Open the CLSID\{guid} key for write access
            RegistryKey k = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);

            // And create	the	'Control' key -	this allows	it to show up in
            // the ActiveX control container
            RegistryKey ctrl = k.CreateSubKey("Control");
            ctrl.Close();

            // Next create the CodeBase entry	- needed if	not	string named and GACced.
            RegistryKey inprocServer32 = k.OpenSubKey("InprocServer32", true);
            inprocServer32.SetValue("CodeBase", Assembly.GetExecutingAssembly().CodeBase);
            inprocServer32.Close();
            // Finally close the main	key
            k.Close();
            MessageBox.Show("Registered");
        }

        ///	<summary>
        ///	Called to unregister the control
        ///	</summary>
        ///	<param name="key">Tke registry key</param>
        [ComUnregisterFunction()]
        public static void UnregisterClass(string key)
        {
            StringBuilder sb = new StringBuilder(key);
            sb.Replace(@"HKEY_CLASSES_ROOT\", "");

            // Open	HKCR\CLSID\{guid} for write	access
            RegistryKey k = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);

            // Delete the 'Control'	key, but don't throw an	exception if it	does not exist
            k.DeleteSubKey("Control", false);

            // Next	open up	InprocServer32
            //RegistryKey	inprocServer32 = 
            k.OpenSubKey("InprocServer32", true);

            // And delete the CodeBase key,	again not throwing if missing
            k.DeleteSubKey("CodeBase", false);

            // Finally close the main key
            k.Close();
            MessageBox.Show("UnRegistered");
        }

        public IObjectSafetyRetVals GetInterfaceSafetyOptions(ref Guid riid, out IObjectSafetyOpts supportedOpts, out IObjectSafetyOpts enabledOpts)
        {
            supportedOpts = IObjectSafetyOpts.INTERFACESAFE_FOR_UNTRUSTED_CALLER | IObjectSafetyOpts.INTERFACESAFE_FOR_UNTRUSTED_DATA;
            enabledOpts = IObjectSafetyOpts.INTERFACESAFE_FOR_UNTRUSTED_CALLER | IObjectSafetyOpts.INTERFACESAFE_FOR_UNTRUSTED_DATA;
            return IObjectSafetyRetVals.S_OK;
        }

        public IObjectSafetyRetVals SetInterfaceSafetyOptions(ref Guid riid, IObjectSafetyOpts optsMask, IObjectSafetyOpts enabledOpts)
        {
            return IObjectSafetyRetVals.S_OK;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            NativeMethods.OpenFileName ofn = new NativeMethods.OpenFileName();

            string[] arr = AllowedFileExtensions.Split(',');
            string filter = "Supported Files(";
            string append = "";
            foreach (string s in arr)
            {
                filter += ("*." + s.Trim().ToLower() + ", ");
                append += ("*." + s.Trim().ToLower() + ";");
            }

            append = append.TrimEnd(';');
            append += "\0";
            filter = filter.TrimEnd(',', ' ');
            filter += ")\0" + append;

            ofn.structSize = Marshal.SizeOf(ofn);
            ofn.filter = filter;
            ofn.file = new string(new char[256]);
            ofn.maxFile = ofn.file.Length;
            ofn.fileTitle = new string(new char[64]);
            ofn.maxFileTitle = ofn.fileTitle.Length;

            DriveInfo[] dis = DriveInfo.GetDrives();

            ofn.initialDir = dis[0].Name;
            ofn.title = "Select a file to upload";
            ofn.defExt = "txt";

            if (NativeMethods.GetOpenFileName(ofn))
            {
                this.txtFile.Text = ofn.file;
                using (FileStream stream = File.OpenRead(ofn.file))
                {
                    long length = stream.Length;
                    byte[] buffer = new byte[length];
                    stream.Read(buffer, 0, (int)length);
                    string shortFilename = Path.GetFileName(ofn.file);
                    fileItem = new FileListItem { FileData = buffer, FileName = ofn.file, ShortFileName = shortFilename, FileSize = buffer.Length };
                    if (DroppedByteArrayAvailable != null)
                    {
                        DroppedByteArrayAvailable(this, shortFilename, buffer);
                    }
                }
            }
        }
    }
}

