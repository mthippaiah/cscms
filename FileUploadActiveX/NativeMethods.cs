﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace FileUploadActiveX
{
    public class NativeMethods
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public sealed class OpenFileName
        {
            public int structSize = 0;
            public IntPtr hwnd = IntPtr.Zero;
            public IntPtr hinst = IntPtr.Zero;
            public string filter = null;
            public string custFilter = null;
            public int custFilterMax = 0;
            public int filterIndex = 0;
            public string file = null;
            public int maxFile = 0;
            public string fileTitle = null;
            public int maxFileTitle = 0;
            public string initialDir = null;
            public string title = null;
            public int flags = 0;
            public short fileOffset = 0;
            public short fileExtMax = 0;
            public string defExt = null;
            public int custData = 0;
            public IntPtr pHook = IntPtr.Zero;
            public string template = null;
        }

        [StructLayout(LayoutKind.Sequential)]

        public sealed class FORMATETC
        {

            public ushort cfFormat;

            public short dummy;

            public IntPtr ptd;

            public int dwAspect;

            public int lindex;

            public int tymed;

        }

        [StructLayout(LayoutKind.Sequential)]

        public class STGMEDIUM
        {

            public int tymed;

            public IntPtr unionmember;

            public IntPtr pUnkForRelease;

        }

        [StructLayout(LayoutKind.Sequential)]

        public sealed class FILEDESCRIPTOR
        {

            public uint dwFlags;

            public Guid clsid;

            public SIZEL sizel;

            public POINTL pointl;

            public uint dwFileAttributes;

            public FILETIME ftCreationTime;

            public FILETIME ftLastAccessTime;

            public FILETIME ftLastWriteTime;

            public uint nFileSizeHigh;

            public uint nFileSizeLow;

            public string cFileName;

        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]

        public sealed class FILEDESCRIPTORA
        {

            public uint dwFlags;

            public Guid clsid;

            public SIZEL sizel;

            public POINTL pointl;

            public uint dwFileAttributes;

            public FILETIME ftCreationTime;

            public FILETIME ftLastAccessTime;

            public FILETIME ftLastWriteTime;

            public uint nFileSizeHigh;

            public uint nFileSizeLow;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string cFileName;

        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]

        public sealed class FILEDESCRIPTORW
        {

            public uint dwFlags;

            public Guid clsid;

            public SIZEL sizel;

            public POINTL pointl;

            public uint dwFileAttributes;

            public FILETIME ftCreationTime;

            public FILETIME ftLastAccessTime;

            public FILETIME ftLastWriteTime;

            public uint nFileSizeHigh;

            public uint nFileSizeLow;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string cFileName;

        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]

        public sealed class FILEGROUPDESCRIPTORA
        {

            public uint cItems;

            public FILEDESCRIPTORA[] fgd;

        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]

        public sealed class FILEGROUPDESCRIPTORW
        {

            public uint cItems;

            public FILEDESCRIPTORW[] fgd;

        }

        public const short CF_TEXT = 1,

        CF_BITMAP = 2,

        CF_METAFILEPICT = 3,

        CF_SYLK = 4,

        CF_DIF = 5,

        CF_TIFF = 6,

        CF_OEMTEXT = 7,

        CF_DIB = 8,

        CF_PALETTE = 9,

        CF_PENDATA = 10,

        CF_RIFF = 11,

        CF_WAVE = 12,

        CF_UNICODETEXT = 13,

        CF_ENHMETAFILE = 14,

        CF_HDROP = 15,

        CF_LOCALE = 16,

        CF_MAX = 17,

        CF_OWNERDISPLAY = 0x0080,

        CF_DSPTEXT = 0x0081,

        CF_DSPBITMAP = 0x0082,

        CF_DSPMETAFILEPICT = 0x0083,

        CF_DSPENHMETAFILE = 0x008E,

        CF_PRIVATEFIRST = 0x0200,

        CF_PRIVATELAST = 0x02FF,

        CF_GDIOBJFIRST = 0x0300,

        CF_GDIOBJLAST = 0x03FF;

        public const int DVASPECT_CONTENT = 1,

        DVASPECT_THUMBNAIL = 2,

        DVASPECT_ICON = 4,

        DVASPECT_DOCPRINT = 8;

        public const int TYMED_HGLOBAL = 1,

        TYMED_FILE = 2,

        TYMED_ISTREAM = 4,

        TYMED_ISTORAGE = 8,

        TYMED_GDI = 16,

        TYMED_MFPICT = 32,

        TYMED_ENHMF = 64,

        TYMED_NULL = 0;

        public const int STGC_DEFAULT = 0,

        STGC_OVERWRITE = 1,

        STGC_ONLYIFCURRENT = 2,

        STGC_DANGEROUSLYCOMMITMERELYTODISKCACHE = 4,

        STGC_CONSOLIDATE = 8;

        public const int STATFLAG_DEFAULT = 0,

        STATFLAG_NONAME = 1,

        STATFLAG_NOOPEN = 2;

        public const int S_OK = 0,

        S_FALSE = 1,

        E_FAIL = (unchecked((int)0x80004005));

        public const uint STGM_DIRECT = 0x00000000,

        STGM_TRANSACTED = 0x00010000,

        STGM_SIMPLE = 0x08000000,

        STGM_READ = 0x00000000,

        STGM_WRITE = 0x00000001,

        STGM_READWRITE = 0x00000002,

        STGM_SHARE_DENY_NONE = 0x00000040,

        STGM_SHARE_DENY_READ = 0x00000030,

        STGM_SHARE_DENY_WRITE = 0x00000020,

        STGM_SHARE_EXCLUSIVE = 0x00000010,

        STGM_PRIORITY = 0x00040000,

        STGM_DELETEONRELEASE = 0x04000000,

        STGM_NOSCRATCH = 0x00100000,

        STGM_CREATE = 0x00001000,

        STGM_CONVERT = 0x00020000,

        STGM_FAILIFTHERE = 0x00000000,

        STGM_NOSNAPSHOT = 0x00200000,

        STGM_DIRECT_SWMR = 0x00400000;

        public const uint FILE_ATTRIBUTE_READONLY = 0x00000001,

        FILE_ATTRIBUTE_HIDDEN = 0x00000002,

        FILE_ATTRIBUTE_SYSTEM = 0x00000004,

        FILE_ATTRIBUTE_DIRECTORY = 0x00000010,

        FILE_ATTRIBUTE_ARCHIVE = 0x00000020,

        FILE_ATTRIBUTE_NORMAL = 0x00000080,

        FILE_ATTRIBUTE_TEMPORARY = 0x00000100;

        public static bool Succeeded(int hr)
        {

            return (hr >= 0);

        }

        public static bool Failed(int hr)
        {

            return (hr < 0);

        }

        public const int DROPEFFECT_NONE = 0,

        DROPEFFECT_COPY = 1,

        DROPEFFECT_MOVE = 2,

        DROPEFFECT_LINK = 4,

        DROPEFFECT_SCROLL = (unchecked((int)0x80000000));

        [StructLayout(LayoutKind.Sequential)]

        public sealed class POINTL
        {

            public int x;

            public int y;

        }

        [StructLayout(LayoutKind.Sequential)]

        public sealed class SIZEL
        {

            public int cx;

            public int cy;

        }

        [StructLayout(LayoutKind.Sequential)]

        public sealed class FILETIME
        {

            public uint nFileTimeHigh;

            public uint nFileTimeLow;

        }

        [StructLayout(LayoutKind.Sequential)]

        public class STATSTG
        {

            [MarshalAs(UnmanagedType.LPWStr)]
            public string pwcsName;

            public int type;

            [MarshalAs(UnmanagedType.I8)]
            public long cbSize;

            [MarshalAs(UnmanagedType.I8)]
            public long mtime;

            [MarshalAs(UnmanagedType.I8)]
            public long ctime;

            [MarshalAs(UnmanagedType.I8)]
            public long atime;

            [MarshalAs(UnmanagedType.I4)]
            public int grfMode;

            [MarshalAs(UnmanagedType.I4)]
            public int grfLocksSupported;

            public int clsid_data1;

            [MarshalAs(UnmanagedType.I2)]
            public short clsid_data2;

            [MarshalAs(UnmanagedType.I2)]
            public short clsid_data3;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b0;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b1;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b2;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b3;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b4;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b5;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b6;

            [MarshalAs(UnmanagedType.U1)]
            public byte clsid_b7;

            [MarshalAs(UnmanagedType.I4)]
            public int grfStateBits;

            [MarshalAs(UnmanagedType.I4)]
            public int reserved;

        }

        [DllImport("USER32.DLL", CharSet = CharSet.Auto, SetLastError = true)]

        public static extern int RegisterClipboardFormat(string format);

        [DllImport("KERNEL32.DLL", CharSet = CharSet.None, SetLastError = true)]

        public static extern IntPtr GlobalLock(HandleRef hGlobal);

        [DllImport("KERNEL32.DLL", CharSet = CharSet.None, SetLastError = true)]

        public static extern bool GlobalUnlock(HandleRef hGlobal);

        [DllImport("OLE32.DLL", ExactSpelling = true, CharSet = CharSet.Auto)]

        public static extern int OleGetClipboard([In, Out] ref IOleDataObject data);

        [DllImport("OLE32.DLL", ExactSpelling = true, CharSet = CharSet.Auto, PreserveSig = false)]

        public static extern void RegisterDragDrop(HandleRef hwnd, IOleDropTarget target);

        [DllImport("OLE32.DLL", CharSet = CharSet.None)]

        public static extern void ReleaseStgMedium(STGMEDIUM pmedium);

        [DllImport("OLE32.DLL", ExactSpelling = true, CharSet = CharSet.Auto, PreserveSig = false)]

        public static extern void RevokeDragDrop(HandleRef hwnd);

        [DllImport("OLE32.DLL", CharSet = CharSet.Unicode, PreserveSig = false)]

        public static extern IStorage StgCreateDocfile(String pwcsName, uint grfMode, uint reserved);

        [DllImport("ole32.dll", PreserveSig = false)]

        public static extern ILockBytes CreateILockBytesOnHGlobal(IntPtr hGlobal, bool

        fDeleteOnRelease);

        [DllImport("OLE32.DLL", CharSet = CharSet.Auto, PreserveSig = false)]

        public static extern IntPtr GetHGlobalFromILockBytes(ILockBytes pLockBytes);

        [DllImport("OLE32.DLL", CharSet = CharSet.Unicode, PreserveSig = false)]

        public static extern IStorage StgCreateDocfileOnILockBytes(ILockBytes plkbyt, uint grfMode, uint reserved);

        /*

        [DllImport("ole32.dll")]

        public static extern int StgCreateDocfileOnILockBytes(ILockBytes plkbyt, uint grfMode, uint reserved, out IStorage ppstgOpen);

        */

        // Declare a managed prototype for the unmanaged function.
        [DllImport("Comdlg32.dll", CharSet = CharSet.Auto)]
        public static extern bool GetOpenFileName([In, Out] OpenFileName ofn);

        #region IEnumFORMATETC interface

        [

        ComImport(),

        Guid("00000103-0000-0000-C000-000000000046"),

        InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)

        ]

        public interface IEnumFORMATETC
        {

            [PreserveSig]

            int Next(

            [In, MarshalAs(UnmanagedType.U4)] int celt,

            [Out] FORMATETC rgelt,

            [In, Out, MarshalAs(UnmanagedType.LPArray)] int[] pceltFetched);

            [PreserveSig]

            int Skip(

            [In, MarshalAs(UnmanagedType.U4)] int celt);

            [PreserveSig]

            int Reset();

            [PreserveSig]

            int Clone(

            [Out, MarshalAs(UnmanagedType.LPArray)] IEnumFORMATETC[] ppenum);

        }

        #endregion

        #region IDataObject interface

        [

        ComImport(),

        Guid("0000010E-0000-0000-C000-000000000046"),

        InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown),

        SuppressUnmanagedCodeSecurity

        ]

        public interface IOleDataObject
        {

            [PreserveSig]

            int OleGetData(

            FORMATETC pFormatetc,

            [Out] STGMEDIUM pMedium);

            [PreserveSig]

            int OleGetDataHere(

            FORMATETC pFormatetc,

            [In, Out] STGMEDIUM pMedium);

            [PreserveSig]

            int OleQueryGetData(

            FORMATETC pFormatetc);

            [PreserveSig]

            int OleGetCanonicalFormatEtc(

            FORMATETC pformatectIn,

            [Out] FORMATETC pformatetcOut);

            [PreserveSig]

            int OleSetData(

            FORMATETC pFormatectIn,

            STGMEDIUM pmedium,

            int fRelease);

            [return: MarshalAs(UnmanagedType.Interface)]

            IEnumFORMATETC OleEnumFormatEtc(

            [In, MarshalAs(UnmanagedType.U4)] int dwDirection);

            [PreserveSig]

            int OleDAdvise(

            FORMATETC pFormatetc,

            [In, MarshalAs(UnmanagedType.U4)] int advf,

            [In, MarshalAs(UnmanagedType.Interface)] object pAdvSink,

            [Out, MarshalAs(UnmanagedType.LPArray)] int[] pdwConnection);

            [PreserveSig]

            int OleDUnadvise(

            [In, MarshalAs(UnmanagedType.U4)] int dwConnection);

            [PreserveSig]

            int OleEnumDAdvise(

            [Out, MarshalAs(UnmanagedType.LPArray)] object[] ppenumAdvise);

        }

        #endregion

        #region IDataObject interface

        [

        ComImport(),

        Guid("00000122-0000-0000-C000-000000000046"),

        InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)

        ]

        public interface IOleDropTarget
        {

            [PreserveSig]

            int OleDragEnter(

            [In, MarshalAs(UnmanagedType.Interface)] object pDataObj,

            [In, MarshalAs(UnmanagedType.U4)] int grfKeyState,

            [In, MarshalAs(UnmanagedType.U8)] long pt,

            [In, Out] ref int pdwEffect);

            [PreserveSig]

            int OleDragOver(

            [In, MarshalAs(UnmanagedType.U4)] int grfKeyState,

            [In, MarshalAs(UnmanagedType.U8)] long pt,

            [In, Out] ref int pdwEffect);

            [PreserveSig]

            int OleDragLeave();

            [PreserveSig]

            int OleDrop(

            [In, MarshalAs(UnmanagedType.Interface)] object pDataObj,

            [In, MarshalAs(UnmanagedType.U4)] int grfKeyState,

            [In, MarshalAs(UnmanagedType.U8)] long pt,

            [In, Out] ref int pdwEffect);

        }

        #endregion

        #region ILockByte interface

        [ComImport, Guid("0000000A-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]

        public interface ILockBytes
        {

            void ReadAt([In, MarshalAs(UnmanagedType.U8)] long ulOffset, [Out] IntPtr pv, [In, MarshalAs(UnmanagedType.U4)] int cb, [Out, MarshalAs(UnmanagedType.LPArray)] int[] pcbRead);

            void WriteAt([In, MarshalAs(UnmanagedType.U8)] long ulOffset, IntPtr pv, [In, MarshalAs(UnmanagedType.U4)] int cb, [Out, MarshalAs(UnmanagedType.LPArray)] int[] pcbWritten);

            void Flush();

            void SetSize([In, MarshalAs(UnmanagedType.U8)] long cb);

            void LockRegion([In, MarshalAs(UnmanagedType.U8)] long libOffset, [In, MarshalAs(UnmanagedType.U8)] long cb, [In, MarshalAs(UnmanagedType.U4)] int dwLockType);

            void UnlockRegion([In, MarshalAs(UnmanagedType.U8)] long libOffset, [In, MarshalAs(UnmanagedType.U8)] long cb, [In, MarshalAs(UnmanagedType.U4)] int dwLockType);

            void Stat([Out] STATSTG pstatstg, [In, MarshalAs(UnmanagedType.U4)] int grfStatFlag);

        }

        #endregion

        #region IStream interface

        [ComImport(), Guid("0000000C-0000-0000-C000-000000000046"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]

        public interface IStream
        {

            int Read(

            IntPtr buf,

            int len);



            int Write(

            IntPtr buf,

            int len);

            [return: MarshalAs(UnmanagedType.I8)]

            long Seek(

            [In, MarshalAs(UnmanagedType.I8)] long dlibMove,

            int dwOrigin);



            void SetSize(

            [In, MarshalAs(UnmanagedType.I8)] long libNewSize);

            [return: MarshalAs(UnmanagedType.I8)]

            long CopyTo(

            [In, MarshalAs(UnmanagedType.Interface)] IStream pstm,

            [In, MarshalAs(UnmanagedType.I8)] long cb,

            [Out, MarshalAs(UnmanagedType.LPArray)] long[] pcbRead);



            void Commit(

            int grfCommitFlags);



            void Revert();



            void LockRegion(

            [In, MarshalAs(UnmanagedType.I8)] long libOffset,

            [In, MarshalAs(UnmanagedType.I8)] long cb,

            int dwLockType);



            void UnlockRegion(

            [In, MarshalAs(UnmanagedType.I8)] long libOffset,

            [In, MarshalAs(UnmanagedType.I8)] long cb,

            int dwLockType);



            void Stat(

            [Out] STATSTG pStatstg,

            int grfStatFlag);

            [return: MarshalAs(UnmanagedType.Interface)]

            IStream Clone();

        }

        #endregion

        #region IStorage interface

        [Flags]

        public enum STGM
        {

            // Fields

            STGM_CONVERT = 0x20000,

            STGM_CREATE = 0x1000,

            STGM_DELETEONRELEASE = 0x4000000,

            STGM_DIRECT = 0,

            STGM_DIRECT_SWMR = 0x400000,

            STGM_FAILIFTHERE = 0,

            STGM_NOSCRATCH = 0x100000,

            STGM_NOSNAPSHOT = 0x200000,

            STGM_PRIORITY = 0x40000,

            STGM_READ = 0,

            STGM_READWRITE = 2,

            STGM_SHARE_DENY_NONE = 0x40,

            STGM_SHARE_DENY_READ = 0x30,

            STGM_SHARE_DENY_WRITE = 0x20,

            STGM_SHARE_EXCLUSIVE = 0x10,

            STGM_SIMPLE = 0x8000000,

            STGM_TRANSACTED = 0x10000,

            STGM_WRITE = 1

        }

        [StructLayout(LayoutKind.Sequential)]

        public struct tagRemSNB
        {

            public uint ulCntStr;

            public uint ulCntChar;

            public IntPtr rgString;

        }

        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("0000000B-0000-0000-C000-000000000046")]

        public interface IStorage
        {

            [return: MarshalAs(UnmanagedType.Interface)]

            IStream CreateStream([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In, MarshalAs(UnmanagedType.U4)] int grfMode, [In, MarshalAs(UnmanagedType.U4)] int reserved1, [In, MarshalAs(UnmanagedType.U4)] int reserved2);

            [return: MarshalAs(UnmanagedType.Interface)]

            IStream OpenStream([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, IntPtr reserved1, [In, MarshalAs(UnmanagedType.U4)] int grfMode, [In, MarshalAs(UnmanagedType.U4)] int reserved2);

            [return: MarshalAs(UnmanagedType.Interface)]

            IStorage CreateStorage([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In, MarshalAs(UnmanagedType.U4)] int grfMode, [In, MarshalAs(UnmanagedType.U4)] int reserved1, [In, MarshalAs(UnmanagedType.U4)] int reserved2);

            [return: MarshalAs(UnmanagedType.Interface)]

            IStorage OpenStorage([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, IntPtr pstgPriority, [In, MarshalAs(UnmanagedType.U4)] int grfMode, IntPtr snbExclude, [In, MarshalAs(UnmanagedType.U4)] int reserved);

            void CopyTo(int ciidExclude, [In, MarshalAs(UnmanagedType.LPArray)] Guid[] pIIDExclude, IntPtr snbExclude, [In, MarshalAs(UnmanagedType.Interface)] IStorage stgDest);

            void MoveElementTo([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In, MarshalAs(UnmanagedType.Interface)] IStorage stgDest, [In, MarshalAs(UnmanagedType.BStr)] string pwcsNewName, [In, MarshalAs(UnmanagedType.U4)] int grfFlags);

            void Commit(int grfCommitFlags);

            void Revert();

            void EnumElements([In, MarshalAs(UnmanagedType.U4)] int reserved1, IntPtr reserved2, [In, MarshalAs(UnmanagedType.U4)] int reserved3, [MarshalAs(UnmanagedType.Interface)] out object ppVal);

            void DestroyElement([In, MarshalAs(UnmanagedType.BStr)] string pwcsName);

            void RenameElement([In, MarshalAs(UnmanagedType.BStr)] string pwcsOldName, [In, MarshalAs(UnmanagedType.BStr)] string pwcsNewName);

            void SetElementTimes([In, MarshalAs(UnmanagedType.BStr)] string pwcsName, [In] FILETIME pctime, [In] FILETIME patime, [In] FILETIME pmtime);

            void SetClass([In] ref Guid clsid);

            void SetStateBits(int grfStateBits, int grfMask);

            void Stat([Out] STATSTG pStatStg, int grfStatFlag);

        }



        [ComImport]

        [Guid("0000000d-0000-0000-C000-000000000046")]

        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]

        public interface IEnumSTATSTG
        {

            // The user needs to allocate an STATSTG array whose size is celt.

            [PreserveSig]

            uint

            Next(

            uint celt,

            [MarshalAs(UnmanagedType.LPArray), Out] STATSTG[] rgelt,

            out uint pceltFetched

            );

            void Skip(uint celt);

            void Reset();

            [return: MarshalAs(UnmanagedType.Interface)]

            IEnumSTATSTG Clone();

        }

        #endregion

        #region Shell Clipboard Formats

        public class ShellClipboardFormats
        {

            public static DataFormats.Format CFSTR_SHELLIDLIST
            {

                get { return DataFormats.GetFormat("Shell IDList Array"); }

            }

            public static DataFormats.Format CFSTR_SHELLIDLISTOFFSET
            {

                get { return DataFormats.GetFormat("Shell Objects Offsets"); }

            }

            public static DataFormats.Format CFSTR_NETRESOURCES
            {

                get { return DataFormats.GetFormat("Net Resource"); }

            }

            public static DataFormats.Format CFSTR_FILEDESCRIPTORA
            {

                get { return DataFormats.GetFormat("FileGroupDescriptor"); }

            }

            public static DataFormats.Format CFSTR_FILEDESCRIPTORW
            {

                get { return DataFormats.GetFormat("FileGroupDescriptorW"); }

            }

            public static DataFormats.Format CFSTR_FILECONTENTS
            {

                get { return DataFormats.GetFormat("FileContents"); }

            }

            public static DataFormats.Format CFSTR_FILENAMEA
            {

                get { return DataFormats.GetFormat("FileName"); }

            }

            public static DataFormats.Format CFSTR_FILENAMEW
            {

                get
                {

                    DataFormats.Format obj = DataFormats.GetFormat("FileNameW");

                    return obj;

                }

            }

            public static DataFormats.Format CFSTR_PRINTERGROUP
            {

                get { return DataFormats.GetFormat("PrinterFriendlyName"); }

            }

            public static DataFormats.Format CFSTR_FILENAMEMAPA
            {

                get { return DataFormats.GetFormat("FileNameMap"); }

            }

            public static DataFormats.Format CFSTR_FILENAMEMAPW
            {

                get { return DataFormats.GetFormat("FileNameMapW"); }

            }

            public static DataFormats.Format CFSTR_SHELLURL
            {

                get { return DataFormats.GetFormat("UniformResourceLocator"); }

            }

            public static DataFormats.Format CFSTR_INETURLA
            {

                get { return CFSTR_SHELLURL; }

            }

            public static DataFormats.Format CFSTR_INETURLW
            {

                get { return DataFormats.GetFormat("UniformResourceLocatorW"); }

            }

            public static DataFormats.Format CFSTR_PREFERREDDROPEFFECT
            {

                get { return DataFormats.GetFormat("Preferred DropEffect"); }

            }

            public static DataFormats.Format CFSTR_PERFORMEDDROPEFFECT
            {

                get { return DataFormats.GetFormat("Performed DropEffect"); }

            }

            public static DataFormats.Format CFSTR_PASTESUCCEEDED
            {

                get { return DataFormats.GetFormat("Paste Succeeded"); }

            }

            public static DataFormats.Format CFSTR_INDRAGLOOP
            {

                get { return DataFormats.GetFormat("InShellDragLoop"); }

            }

            public static DataFormats.Format CFSTR_DRAGCONTEXT
            {

                get { return DataFormats.GetFormat("DragContext"); }

            }

            public static DataFormats.Format CFSTR_MOUNTEDVOLUME
            {

                get { return DataFormats.GetFormat("MountedVolume"); }

            }

            public static DataFormats.Format CFSTR_PERSISTEDDATAOBJECT
            {

                get { return DataFormats.GetFormat("PersistedDataObject"); }

            }

            public static DataFormats.Format CFSTR_TARGETCLSID
            {

                get { return DataFormats.GetFormat("TargetCLSID"); }

            }

            public static DataFormats.Format CFSTR_LOGICALPERFORMEDDROPEFFECT
            {

                get { return DataFormats.GetFormat("Logical Performed DropEffect"); }

            }

            public static DataFormats.Format CFSTR_AUTOPLAY_SHELLIDLISTS
            {

                get { return DataFormats.GetFormat("Autoplay Enumerated IDList Array"); }

            }

        }

        #endregion

    }
}
