﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace FileUploadActiveX
{
    public class Shell32
    {

        private Shell32()
        {

        }

        #region www.pinvoke.net

        internal const uint SHGFI_ICON = 0x100;

        internal const uint SHGFI_LARGEICON = 0x0; // 'Large icon

        internal const uint SHGFI_SMALLICON = 0x1; // 'Small icon

        internal const int SHGFI_USEFILEATTRIBUTES = 0x10;

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        public static extern int DragQueryFile(HandleRef hDrop, int iFile, StringBuilder lpszFile, int cch);

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]

        internal static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbFileInfo, uint uFlags);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]

        internal struct SHFILEINFO
        {

            public IntPtr hIcon;

            public int iIcon;

            public uint dwAttributes;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szDisplayName;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;

        } ;

        [DllImport("user32.dll", SetLastError = true)]

        internal static extern int DestroyIcon(IntPtr hIcon);

        // also from pinvoke.net, with two small tweaks.

        // 1) the Icon is cloned, so as to allow the native icon to be destroyed (preventing a leak) - tip from CodeProject.

        // 2) passing the SHGFI_USEFILEATTRIBUTES so as to allow e-mail to be dropped, where the filename doesn't correspond to a real file.

        public static Icon GetSmallIcon(string fileName)
        {

            IntPtr hImgSmall; //the handle to the system image list

            SHFILEINFO shinfo = new SHFILEINFO();

            //Use this to get the small Icon

            hImgSmall = SHGetFileInfo(fileName, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), SHGFI_ICON | SHGFI_SMALLICON | SHGFI_USEFILEATTRIBUTES);

            //The icon is returned in the hIcon member of the shinfo struct

            Icon icon = (Icon)Icon.FromHandle(shinfo.hIcon).Clone();

            DestroyIcon(shinfo.hIcon);

            return icon;

        }

        public static Icon GetLargeIcon(string fileName)
        {

            IntPtr hImgLarge; //the handle to the system image list

            SHFILEINFO shinfo = new SHFILEINFO();

            //Use this to get the large Icon

            hImgLarge = SHGetFileInfo(fileName, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), SHGFI_ICON | SHGFI_LARGEICON | SHGFI_USEFILEATTRIBUTES);

            //The icon is returned in the hIcon member of the shinfo struct

            Icon icon = (Icon)Icon.FromHandle(shinfo.hIcon).Clone();

            DestroyIcon(shinfo.hIcon);

            return icon;

        }

        #endregion

    }
}
