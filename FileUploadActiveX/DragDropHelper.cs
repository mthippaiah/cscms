﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace FileUploadActiveX
{
    public delegate void FilenameDroppedHandler(object sender, string filename);

    public delegate void StreamDroppedHandler(object sender, string filename, NativeMethods.IStream stream);

    public delegate void StorageDroppedHandler(object sender, string filename, NativeMethods.IStorage storage);

    public delegate void DroppedByteArrayAvailableHandler(object sender, string filename, byte[] bytes);

    /// <summary>
    /// Event handler for events that will be visible from JavaScript
    /// </summary>
    public delegate void ControlEventHandler(string redirectUrl);


    /// <summary>
    /// This interface shows events to javascript
    /// </summary>
    [Guid("68BD4E0D-D7BC-4cf6-BEB7-CAB950161E79")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ControlEvents
    {
        //Add a DispIdAttribute to any members in the source interface to specify the COM DispId.
        [DispId(0x60020001)]
        void OnClose(string redirectUrl); //This method will be visible from JS
    }

    [Flags]
    public enum IObjectSafetyOpts : int //DWORD
    {
        // Object is safe for untrusted callers.
        INTERFACESAFE_FOR_UNTRUSTED_CALLER = 0x00000001,
        // Object is safe for untrusted data.
        INTERFACESAFE_FOR_UNTRUSTED_DATA = 0x00000002,
        // Object uses IDispatchEx.
        INTERFACE_USES_DISPEX = 0x00000004,
        // Object uses IInternetHostSecurityManager.
        INTERFACE_USES_SECURITY_MANAGER = 0x00000008
    }

    public enum IObjectSafetyRetVals : uint //HRESULT
    {
        //The object is safe for loading.
        S_OK = 0x0,
        //The riid parameter specifies an interface that is unknown to the object.
        E_NOINTERFACE = 0x80000004
    }

    [ComImport()]
    //This GUID is that of IObjectSafety. Do not replace!
    [Guid("CB5BDC81-93C1-11CF-8F20-00805F2CD064")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IObjectSafety
    {
        [PreserveSig()]
        IObjectSafetyRetVals GetInterfaceSafetyOptions(ref Guid riid, out IObjectSafetyOpts supportedOpts, out IObjectSafetyOpts enabledOpts);
        [PreserveSig()]
        IObjectSafetyRetVals SetInterfaceSafetyOptions(ref Guid riid, IObjectSafetyOpts optsMask, IObjectSafetyOpts enabledOpts);
    }

    public class FileListItem
    {
        public string FileName { get; set; }
        public byte[] FileData { get; set; }
        public int ListIndex { get; set; }
        public bool Uploaded { get; set; }
        public string ShortFileName { get; set; }
        public long FileSize { get; set; }
        public long Sent { get; set; }
        public string FileDescription { get; set; }
    }

    public interface IDropTargetControl
    {
        void Activate();

        void DragEnter(string filename);

        void DragLeave();

        void DragOver();

        void DragDrop();
    }

    public class FileDropTarget : NativeMethods.IOleDropTarget
    {

        private IDropTargetControl owner;

        private DragDropEffects cachedEffect;

        public FileDropTarget(IDropTargetControl owner)
        {

            this.owner = owner;

            cachedEffect = DragDropEffects.None;

        }

        private string GetFilename(NativeMethods.IOleDataObject dataObject)
        {

            NativeMethods.STGMEDIUM medium = new NativeMethods.STGMEDIUM();

            NativeMethods.FORMATETC format = new NativeMethods.FORMATETC();

            format.cfFormat = (ushort)NativeMethods.ShellClipboardFormats.CFSTR_FILENAMEW.Id;

            format.dwAspect = NativeMethods.DVASPECT_CONTENT;

            format.lindex = -1;

            format.ptd = new IntPtr(0);

            format.tymed = NativeMethods.TYMED_HGLOBAL | NativeMethods.TYMED_ISTORAGE | NativeMethods.TYMED_ISTREAM | NativeMethods.TYMED_FILE;

            dataObject.OleGetData(format, medium);

            string filename;

            unsafe
            {

                IntPtr ptr = NativeMethods.GlobalLock(new HandleRef(null, medium.unionmember));

                filename = new string((char*)ptr);

                NativeMethods.GlobalUnlock(new HandleRef(null, medium.unionmember));

            }

            return filename;

        }

        private List<string> GetFileNames(NativeMethods.IOleDataObject dataObject)
        {
            int decAgain = int.Parse("FFFFFFFF", System.Globalization.NumberStyles.HexNumber);
            List<string> fileNames = new List<string>();

            NativeMethods.STGMEDIUM medium = new NativeMethods.STGMEDIUM();

            NativeMethods.FORMATETC format = new NativeMethods.FORMATETC();

            format.cfFormat = (ushort)NativeMethods.CF_HDROP;

            format.dwAspect = NativeMethods.DVASPECT_CONTENT;

            format.lindex = -1;

            format.ptd = new IntPtr(0);

            format.tymed = NativeMethods.TYMED_HGLOBAL | NativeMethods.TYMED_ISTORAGE | NativeMethods.TYMED_ISTREAM | NativeMethods.TYMED_FILE;

            int result = dataObject.OleGetData(format, medium);

           if(NativeMethods.Succeeded(result))
           {
               HandleRef href = new HandleRef(null, medium.unionmember);
               int files = Shell32.DragQueryFile(href, -1, null, 0);

                for(int i=0; i<files; i++)
                {
                    StringBuilder sb = new StringBuilder(256);
                    int val = Shell32.DragQueryFile(href, i, sb, sb.Capacity + 1);

                    fileNames.Add(sb.ToString());
                }
           }

            return fileNames;
        }
    
        int NativeMethods.IOleDropTarget.OleDragEnter(object pDataObj, int grfKeyState, long pt, ref int pdwEffect)
        {

            //

            // Default to DROPEFFECT_NONE

            cachedEffect = DragDropEffects.None;

            //

            // Does the data object support CFSTR_FILEDESCRIPTOR

            if (QueryGetFileDescriptorArray((NativeMethods.IOleDataObject)pDataObj))
            {

                // Retrieve the list of files/folders

                NativeMethods.IOleDataObject dataObject = (NativeMethods.IOleDataObject)pDataObj;

                NativeMethods.FILEDESCRIPTOR[] files = GetFileDescriptorArray(dataObject);

                if (files == null || files.Length == 0)
                {

                    cachedEffect = DragDropEffects.Copy;

                    //string filename = GetFilename(dataObject);
                    //owner.DragEnter(filename);

                    List<string> filenames = GetFileNames(dataObject);

                    if(filenames.Count > 0)
                    owner.DragEnter(filenames[0]);

                }

                else
                {

                    NativeMethods.FILEDESCRIPTOR firstFile = files[0];

                    string firstFilename = firstFile.cFileName;

                    // Indicate that we can copy the item(s)

                    cachedEffect = DragDropEffects.Copy;

                    owner.DragEnter(firstFilename);

                }

            }

            pdwEffect = (int)cachedEffect;

            return NativeMethods.S_OK;

        }

        int NativeMethods.IOleDropTarget.OleDragOver(int grfKeyState, long pt, ref int pdwEffect)
        {

            pdwEffect = (int)cachedEffect;

            owner.DragOver();

            return NativeMethods.S_OK;

        }

        int NativeMethods.IOleDropTarget.OleDragLeave()
        {

            cachedEffect = DragDropEffects.None;

            owner.DragLeave();

            return NativeMethods.S_OK;

        }

        int NativeMethods.IOleDropTarget.OleDrop(object pDataObj, int grfKeyState, long pt, ref int pdwEffect)
        {

            int result;

            //

            // Default to DROPEFFECT_NONE

            cachedEffect = DragDropEffects.None;

            // Retrieve the list of files/folders

            NativeMethods.IOleDataObject dataObject = (NativeMethods.IOleDataObject)pDataObj;

            NativeMethods.FILEDESCRIPTOR[] files = GetFileDescriptorArray(dataObject);

            if (files != null && files.Length > 0)
            {

                result = CopyFileContents((NativeMethods.IOleDataObject)pDataObj, files);

                if (NativeMethods.Succeeded(result))
                {

                    owner.DragDrop();

                    cachedEffect = DragDropEffects.Copy;

                    owner.Activate();

                }

            }

            else
            {
                //string filename = GetFilename(dataObject);
                //SaveFilename(filename);

                List<string> filenames = GetFileNames(dataObject);
                if (filenames.Count > 0)
                {
                    foreach (string filename in filenames)
                        SaveFilename(filename);
                }
                owner.DragDrop();

                owner.Activate();

            }

            pdwEffect = (int)cachedEffect;

            return NativeMethods.S_OK;

        }

        private NativeMethods.FILEDESCRIPTOR[] GetFileDescriptorArray(NativeMethods.IOleDataObject dataObject)
        {

            int result;

            NativeMethods.FILEDESCRIPTOR[] files;

            NativeMethods.FORMATETC format = new NativeMethods.FORMATETC();

            NativeMethods.STGMEDIUM medium = new NativeMethods.STGMEDIUM();

            Type formatType;

            HandleRef hGlobal;

            IntPtr pdata;

            //

            // Query the data object for CFSTR_FILEDESCRIPTORW

            format.cfFormat = (ushort)NativeMethods.ShellClipboardFormats.CFSTR_FILEDESCRIPTORW.Id;

            format.dwAspect = NativeMethods.DVASPECT_CONTENT;

            format.lindex = -1;

            format.ptd = new IntPtr(0);

            format.tymed = NativeMethods.TYMED_HGLOBAL;

            result = dataObject.OleGetData(format, medium);

            if (NativeMethods.Succeeded(result))
            {

                formatType = typeof(NativeMethods.FILEDESCRIPTORW);

            }

            else
            {

                //

                // Query the data object for CFSTR_FILEDESCRIPTORA

                format = new NativeMethods.FORMATETC();

                format.cfFormat = (ushort)NativeMethods.ShellClipboardFormats.CFSTR_FILEDESCRIPTORA.Id;

                format.dwAspect = NativeMethods.DVASPECT_CONTENT;

                format.lindex = -1;

                format.ptd = new IntPtr(0);

                format.tymed = NativeMethods.TYMED_HGLOBAL;

                result = dataObject.OleGetData(format, medium);

                if (NativeMethods.Succeeded(result))
                {

                    formatType = typeof(NativeMethods.FILEDESCRIPTORA);

                }

                else
                {

                    //

                    // This data object does not support CFSTR_FILEDESCRIPTOR

                    return new NativeMethods.FILEDESCRIPTOR[0];

                }

            }

            hGlobal = new HandleRef(null, medium.unionmember);

            pdata = NativeMethods.GlobalLock(hGlobal);

            try
            {

                //

                // Determine the number of items in the array

                NativeMethods.FILEGROUPDESCRIPTORW fgd = (NativeMethods.FILEGROUPDESCRIPTORW)Marshal.PtrToStructure(pdata, typeof(NativeMethods.FILEGROUPDESCRIPTORW));

                //

                // Allocate an array of FILEDESCRIPTOR structures

                files = new NativeMethods.FILEDESCRIPTOR[fgd.cItems];

                //

                // Set our pointer offset to the beginning of the FILEDESCRIPTOR* array

                pdata = (IntPtr)((int)pdata + Marshal.SizeOf(pdata));

                //

                // Walk the array, converting each FILEDESCRIPTOR* to a FILEDESCRIPTOR

                for (int index = 0; index < fgd.cItems; index++)
                {

                    NativeMethods.FILEDESCRIPTORA fdA = (NativeMethods.FILEDESCRIPTORA)Marshal.PtrToStructure(pdata, typeof(NativeMethods.FILEDESCRIPTORA));

                    NativeMethods.FILEDESCRIPTORW fdW = (NativeMethods.FILEDESCRIPTORW)Marshal.PtrToStructure(pdata, typeof(NativeMethods.FILEDESCRIPTORW));

                    files[index] = new NativeMethods.FILEDESCRIPTOR();

                    if (formatType == typeof(NativeMethods.FILEDESCRIPTORW))
                    {

                        files[index].dwFlags = fdW.dwFlags;

                        files[index].clsid = fdW.clsid;

                        files[index].sizel = fdW.sizel;

                        files[index].pointl = fdW.pointl;

                        files[index].dwFileAttributes = fdW.dwFileAttributes;

                        files[index].ftCreationTime = fdW.ftCreationTime;

                        files[index].ftLastAccessTime = fdW.ftLastAccessTime;

                        files[index].ftLastWriteTime = fdW.ftLastWriteTime;

                        files[index].nFileSizeHigh = fdW.nFileSizeHigh;

                        files[index].nFileSizeLow = fdW.nFileSizeLow;

                        files[index].cFileName = fdW.cFileName;

                    }

                    else
                    {

                        files[index].dwFlags = fdA.dwFlags;

                        files[index].clsid = fdA.clsid;

                        files[index].sizel = fdA.sizel;

                        files[index].pointl = fdA.pointl;

                        files[index].dwFileAttributes = fdA.dwFileAttributes;

                        files[index].ftCreationTime = fdA.ftCreationTime;

                        files[index].ftLastAccessTime = fdA.ftLastAccessTime;

                        files[index].ftLastWriteTime = fdA.ftLastWriteTime;

                        files[index].nFileSizeHigh = fdA.nFileSizeHigh;

                        files[index].nFileSizeLow = fdA.nFileSizeLow;

                        files[index].cFileName = fdA.cFileName;

                    }

                    //

                    // Advance to the next item in the array

                    pdata = (IntPtr)((int)pdata + Marshal.SizeOf(formatType));

                }

            }

            catch
            {

                //

                // Bail on any exceptions

                files = new NativeMethods.FILEDESCRIPTOR[0];

            }

            NativeMethods.GlobalUnlock(hGlobal);

            NativeMethods.ReleaseStgMedium(medium);

            return files;

        }
        
        private int CopyFileContents(NativeMethods.IOleDataObject dataObject, NativeMethods.FILEDESCRIPTOR[] files)
        {

            int result = NativeMethods.E_FAIL;

            NativeMethods.STGMEDIUM medium = new NativeMethods.STGMEDIUM();

            if (files.Length > 0)
            {

                for (int index = 0; index < files.Length; index++)
                {

                    String filename = files[index].cFileName;

                    //

                    // If the object is a folder, make sure it exists.

                    if ((files[index].dwFileAttributes & NativeMethods.FILE_ATTRIBUTE_DIRECTORY) == NativeMethods.FILE_ATTRIBUTE_DIRECTORY)
                    {

                        //

                        // TODO: Make sure that the specified directory exists

                    }

                    else
                    {

                        //

                        // Otherwise, create the file and save its contents

                        result = SaveToFile(dataObject, NativeMethods.ShellClipboardFormats.CFSTR_FILECONTENTS, index, filename);

                        if (NativeMethods.Failed(result))
                        {

                            break;

                        }

                    }

                }

                NativeMethods.ReleaseStgMedium(medium);

            }

            return result;

        }

        private int SaveToFile(NativeMethods.IOleDataObject pdata, DataFormats.Format cfFormat, int index, String filename)
        {

            int result;

            NativeMethods.FORMATETC format = new NativeMethods.FORMATETC();

            NativeMethods.STGMEDIUM medium = new NativeMethods.STGMEDIUM();

            //

            // Get the data for this file

            format.cfFormat = (ushort)cfFormat.Id;

            format.dwAspect = NativeMethods.DVASPECT_CONTENT;

            format.lindex = index;

            format.tymed = NativeMethods.TYMED_HGLOBAL | NativeMethods.TYMED_ISTORAGE | NativeMethods.TYMED_ISTREAM;

            result = pdata.OleGetData(format, medium);

            if (NativeMethods.Failed(result))
            {

                return result;

            }

            //

            // Save the data to the specified file based on the medium

            try
            {

                switch (medium.tymed)
                {

                    case NativeMethods.TYMED_HGLOBAL:

                        // TODO: Save HGLOBAL data to a file

                        break;

                    case NativeMethods.TYMED_ISTREAM:

                        SaveStreamToStream(filename, (NativeMethods.IStream)Marshal.GetTypedObjectForIUnknown(medium.unionmember, typeof(NativeMethods.IStream)));

                        break;

                    case NativeMethods.TYMED_ISTORAGE:

                        SaveStorageToStream(filename, (NativeMethods.IStorage)Marshal.GetTypedObjectForIUnknown(medium.unionmember, typeof(NativeMethods.IStorage)));

                        break;

                }

            }

            catch
            {

                result = NativeMethods.E_FAIL;

            }

            NativeMethods.ReleaseStgMedium(medium);

            return result;

        }

        public event StorageDroppedHandler StorageDropped;

        public event StreamDroppedHandler StreamDropped;

        public event FilenameDroppedHandler FilenameDropped;

        // public event EventHandler HGlobalDropped;

        public void SaveFilename(string filename)
        {

            if (FilenameDropped != null)
            {

                FilenameDropped(this, filename);

            }

        }

        public void SaveStreamToStream(string filename, NativeMethods.IStream stream)
        {

            if (StreamDropped != null)
            {

                StreamDropped(this, filename, stream);

            }

        }

        public void SaveStorageToStream(string filename, NativeMethods.IStorage storage)
        {

            if (StorageDropped != null)
            {

                StorageDropped(this, filename, storage);

            }

        }

        private bool QueryGetFileDescriptorArray(NativeMethods.IOleDataObject dataObject)
        {

            //

            // Called to determine if the specified data object supports

            // CFSTR_FILEDESCRIPTORA or CFSTR_FILEDESCRIPTORW

            int result;

            NativeMethods.FORMATETC format = new NativeMethods.FORMATETC();

            //

            // Determine if the data object supports CFSTR_FILEDESCRIPTORA

            format.cfFormat = (ushort)NativeMethods.ShellClipboardFormats.CFSTR_FILEDESCRIPTORA.Id;

            format.dwAspect = NativeMethods.DVASPECT_CONTENT;

            format.lindex = -1;

            format.ptd = new IntPtr(0);

            format.tymed = NativeMethods.TYMED_HGLOBAL;

            result = dataObject.OleQueryGetData(format);

            if (NativeMethods.Succeeded(result))
            {

                return true;

            }

            //

            // Determine if the data object supports CFSTR_FILEDESCRIPTORW

            format.cfFormat = (ushort)NativeMethods.ShellClipboardFormats.CFSTR_FILEDESCRIPTORW.Id;

            format.dwAspect = NativeMethods.DVASPECT_CONTENT;

            format.lindex = -1;

            format.ptd = new IntPtr(0);

            format.tymed = NativeMethods.TYMED_HGLOBAL;

            result = dataObject.OleQueryGetData(format);

            if (NativeMethods.Succeeded(result))
            {

                return true;

            }

            return false;

        }

    }
  
}
