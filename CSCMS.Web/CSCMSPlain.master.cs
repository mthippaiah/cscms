﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CSCMSPlain : System.Web.UI.MasterPage
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        string szTheme = Request.Cookies["Theme"].Value;
        Page.Theme = szTheme;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        topBkImage.Attributes.Add("Style", "background-image: url('" + Page.Request.ApplicationRoot() + "/images/topbg.jpg" + "'); height: 40px; padding-left: 20px;");
        SetFooterLink1Url(ContactsUrl, "Contacts");
    }

    public string CscmsImageUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/images/cscms.gif";
        }
    }

    public string ContactsUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/AnonPages/Contact.aspx";
        }
    }

    public void SetFooterLink1Url(string href, string innerText)
    {
        footerLink1.HRef = href;
        footerLink1.InnerText = innerText; 
    }

}
