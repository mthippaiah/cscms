﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using CSCMS.WebControls;
using System.Data.Objects;
using System.Data.EntityClient;
public partial class MemberPages_MyPage : BasePage
{
    const string INVALID_CASE_COURT = "Cannot open. Sorry!. Case belongs to a different court ";
    #region [Page]
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["MyPageHearingDateTime"] != null)
            {
                dtGoto.SelectedDate = DateTime.Parse(Session["MyPageHearingDateTime"].ToString());
                scHearings.HeaderText = (DateTime.Parse(Session["MyPageHearingDateTime"].ToString())).ToShortDateString() + " HEARINGS";
            }
            if (Session["MyPageActivityDateTime"] != null)
            {
                this.dtGotoActivityDate.SelectedDate = DateTime.Parse(Session["MyPageActivityDateTime"].ToString());
                this.scDailyCourtUserActivity.HeaderText = (DateTime.Parse(Session["MyPageActivityDateTime"].ToString())).ToShortDateString() + " Court Users Activities";
            }

            if (Session["ShowActivityDateTime"] == null) Session["ShowActivityDateTime"] = 1;
            if (Session["MyPageShowHearing"] == null) Session["MyPageShowHearing"] = 1;

            if (int.Parse(Session["MyPageShowHearing"].ToString()) != 1)
            {
                this.grdHearings.Visible = false;
                this.btnGotoHideShow.Text = "Show Hearings";
                this.btnGotoHideShow.CssClass = "submitMediumBlue";
            }
            else
            {
                this.grdHearings.Visible = true;
                this.btnGotoHideShow.Text = "Hide Hearings";
                this.btnGotoHideShow.CssClass = "submitMedium";
            }
            if (int.Parse(Session["ShowActivityDateTime"].ToString()) != 1)
            {
                this.grdDailyCourtUserActivity.Visible = false;
                this.btnGotoActivityShowHide.Text = "Show Activities";
                this.btnGotoActivityShowHide.CssClass = "submitMediumBlue";
            }
            else
            {
                this.grdDailyCourtUserActivity.Visible = true;
                this.btnGotoActivityShowHide.Text = "Hide Activities";
                this.btnGotoActivityShowHide.CssClass = "submitMedium";
            }

            if (Session["ScreenResolution"] == null)
            {
                // Session variable is not set
                // Redirect to the screen resolution detection script
                //Response.Redirect("../detectscreen.aspx"); 
                //todo:cscms
            }
            if (Request.QueryString["message"] == "DeleteCase")
            {
                messageBox.SuccessMessage = "Case deleted successfully.";
            }
            if (Session["lastLogin"] != null)
            {
                messageBox.InfoMessage = Session["lastLogin"].ToString();
                Session["lastLogin"] = null;
            }
            if (this.CurrentUser.ViewAllUser && Convert.ToBoolean(Session["Impersonating"]) == false) btnCreateCase.Visible = false;
            btnCourtPreferences.Visible = btnCreateCase.Visible;

            //btnDismissAllTickler.Visible = btnCreateCase.Visible;

            ddlOCACourt.DataBind();
            if (ddlOCACourt.Items.Count > 1)
            {
                ddlOCACourt.Visible = true;
                this.ddlOCACourt.SelectedValue = this.CurrentUser.OCACourtID.ToString();
                var query = CompiledQueryList.GetUser.Invoke(CscmsContext, this.CurrentUser.OCACourtID, (short)UserType.Judge);
                CSCMS.Data.User user = query.FirstOrDefault(); ;
                if (user != null)
                    this.CurrentUser.OCACourtJudge = user.UserFullName;
            }

            //Get Pending cases Data and Bind to grid
            //PendingCasesWithAge.DataSource = PendingCase.GetPendingCaseSummary(base.CurrentUser.OCACourtID);
            //PendingCasesWithAge.DataSource = GetPendingCasesWithAge();
        }

    }
    #endregion

    #region [DataSet]
    protected void dsTicklers_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID;
    }

    protected void dsOpenCases_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID;
    }

    protected void dsDailyCourtUserActivity_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        if (this.CurrentUser.OCACourtID > 0)
        {
            if (Session["MyPageActivityDateTime"] == null)
            {
                e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID;
                e.Command.Parameters["@StartDate"].Value = System.DateTime.Today;
                e.Command.Parameters["@EndDate"].Value = System.DateTime.Today;
                //e.Result = dc.GetCourtActivities(this.CurrentUser.OCACourtID, System.DateTime.Today, System.DateTime.Today).OrderByDescending(t => t.CreateDate);
            }
            else
            {
                e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID;
                e.Command.Parameters["@StartDate"].Value = DateTime.Parse(Session["MyPageActivityDateTime"].ToString());
                e.Command.Parameters["@EndDate"].Value = DateTime.Parse(Session["MyPageActivityDateTime"].ToString());
                //e.Result = dc.GetCourtActivities(this.CurrentUser.OCACourtID, DateTime.Parse(Session["MyPageActivityDateTime"].ToString()), DateTime.Parse(Session["MyPageActivityDateTime"].ToString())).OrderByDescending(t => t.CreateDate);
            }
        }
        else
        {
            e.Command.Parameters["@OCACourtID"].Value = 9999;
            e.Command.Parameters["@StartDate"].Value = System.DateTime.Today;
            e.Command.Parameters["@EndDate"].Value = System.DateTime.Today;
            //e.Result = new string[0];
        }
    }

    protected void dsOCACourt_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@UserID"].Value = this.CurrentUser.UserID;
    }

    protected void dsReminder_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID.ToString();
        e.Command.Parameters["@UserID"].Value = this.CurrentUser.UserID.ToString();
    }

    #endregion

    #region [Buttons]
    protected void btnCreateCase_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewCase.aspx", true);
    }

    protected void btnDismissAllTickler_Click(object sender, EventArgs e)
    {
        Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
           CommandType.StoredProcedure,
           "DismissTicklersByOCACourt",
           new SqlParameter("@OCACourtID", this.CurrentUser.OCACourtID));

        grdTicklers.DataBind();
    }
    protected void ddlOCACourt_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHelper.ResetSessionForUserCourtChange();
        this.CurrentUser.OCACourtID = short.Parse(ddlOCACourt.SelectedValue);
        this.CurrentUser.OCACourtName = ddlOCACourt.Items[ddlOCACourt.SelectedIndex].Text;
        List<CSCMS.Data.County> list = new List<CSCMS.Data.County>();
        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            // try to figure out the judge for this court
            var query = CompiledQueryList.GetUser.Invoke(CscmsContext, this.CurrentUser.OCACourtID, (int)UserType.Judge);
            CSCMS.Data.User user = query.FirstOrDefault();

            if (user != null)
                this.CurrentUser.OCACourtJudge = user.UserFullName;

            var query1 = CompiledQueryList.GetUserCounties.Invoke(CscmsContext, this.CurrentUser.OCACourtID);
            ((ObjectQuery)query1).MergeOption = MergeOption.NoTracking;
            list = query1.Distinct().ToList();
        }

        this.CurrentUser.Counties.Clear();
        foreach (CSCMS.Data.County cy in list)
        {
            this.CurrentUser.Counties.Add(cy.CountyID);
        }

        Session["CurrentUser"] = this.CurrentUser;

        base.CurrentAction = null;
        base.CurrentCause = null;
        base.CurrentActionPerson = null;
        base.CurrentContinuance = null;
        base.CurrentDocument = null;
        base.CurrentHearing = null;
        base.CurrentPerson = null;
        base.CurrentSpecialOrder = null;
        Response.Redirect("~/MemberPages/MyPage.aspx", true);
    }

    protected void grdTicklers_DeleteCommand(object source, GridCommandEventArgs e)
    {
        DateTime triggerDate = Convert.ToDateTime(grdTicklers.MasterTableView.DataKeyValues[e.Item.ItemIndex]["TriggerDate"]);
        byte ticklerTypeID = Convert.ToByte(grdTicklers.MasterTableView.DataKeyValues[e.Item.ItemIndex]["TicklerTypeID"]);
        string ticklerMessage = grdTicklers.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Tickler"].ToString();
        Guid ActionGUID = new Guid(grdTicklers.MasterTableView.DataKeyValues[e.Item.ItemIndex]["ActionGUID"].ToString());

        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            var query = CompiledQueryList.GetUserTicklerDismissed.Invoke(CscmsContext, new UserTicklerQueryData { ActionGuid = ActionGUID, TicklerMessage = ticklerMessage, TriggerDate = triggerDate, TicklerTypeId = ticklerTypeID, OcaCourtId = this.CurrentUser.OCACourtID });
            CSCMS.Data.UserTicklerDismissed ut = query.FirstOrDefault();
            if (ut == null)
            {
                ut = new CSCMS.Data.UserTicklerDismissed();
                ut.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == this.CurrentUser.OCACourtID).FirstOrDefault();
                ut.ActionGUID = ActionGUID;
                ut.TicklerType = CscmsContext.TicklerType.Where(it => it.TicklerTypeID == ticklerTypeID).FirstOrDefault();
                ut.TriggerDate = triggerDate;
                ut.Tickler = ticklerMessage;
                ut.CreateDate = DateTime.Now;
                ut.LastUpdateDate = DateTime.Now;

                CscmsContext.AddToUserTicklerDismissed(ut);
                CscmsContext.SaveChanges();
            }
        }
        string key = "Ticklers" + this.ddlTicklerType.SelectedValue;
        if (Page.Session[key] != null)
        {
            DataSet ds = (DataSet)Page.Session[key];
            DataRow[] rows = ds.Tables[0].Select("ActionGUID = '" + ActionGUID.ToString() + "' and Tickler = '" + ticklerMessage + "' and TriggerDate = '" + triggerDate.ToString() + "' and TicklerTypeId = '" + ticklerTypeID.ToString() + "'");
            if (rows != null && rows.Count() > 0 && rows.Count() == 1)
            {
                ds.Tables[0].Rows.Remove(rows[0]);
            }
            grdTicklers.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCauseNumber.Text) && string.IsNullOrEmpty(this.txtOAGCauseNumber.Text)) return;
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (!string.IsNullOrEmpty(txtCauseNumber.Text))
            {
                DataSet ds = Helper.GetCaseByCauseNumber(SqlConn, ((BasePage)this.Page).CurrentUser.UserID, txtCauseNumber.Text);
                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);
                    if (c.OCACourtID == this.CurrentUser.OCACourtID)
                        Response.Redirect(string.Format("~/MemberPages/ActionDetail.aspx?cid={0}&a={1}", ds.Tables[0].Rows[0]["CauseGUID"].ToString(), ds.Tables[0].Rows[0]["NumberOfAction"].ToString()), true);
                    else
                        messageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    messageBox.ErrorMessage = "Cause Number not found";
                    //cvtxtCauseNumber.IsValid = false;
                    //Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&cn={0}", txtCauseNumber.Text), true);
                }
            }
            else
            {

                DataSet ds = Helper.GetCaseByOAGNumber(SqlConn, ((BasePage)this.Page).CurrentUser.UserID, this.txtOAGCauseNumber.Text);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);

                    if (c.OCACourtID == this.CurrentUser.OCACourtID)
                        Response.Redirect(string.Format("~/MemberPages/ActionDetail.aspx?cid={0}&a={1}", ds.Tables[0].Rows[0]["CauseGUID"].ToString(), ds.Tables[0].Rows[0]["NumberOfAction"].ToString()), true);
                    else
                        messageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    messageBox.ErrorMessage = "OAG Cause Number not found";
                    //cvtxtCauseNumber.IsValid = false;
                    //Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&cn={0}", txtCauseNumber.Text), true);
                }
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void btnHearing_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCauseNumber.Text) && string.IsNullOrEmpty(this.txtOAGCauseNumber.Text)) return;
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (!string.IsNullOrEmpty(txtCauseNumber.Text))
            {
                DataSet ds = Helper.GetCaseByCauseNumber(SqlConn, ((BasePage)this.Page).CurrentUser.UserID, txtCauseNumber.Text);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);

                    if (c.OCACourtID == this.CurrentUser.OCACourtID)
                        OpenHearingScreen((Guid)ds.Tables[0].Rows[0]["CauseGUID"], (short)ds.Tables[0].Rows[0]["NumberOfAction"]);
                    else
                        messageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&cn={0}", txtCauseNumber.Text), true);
                }
            }
            else
            {
                DataSet ds = Helper.GetCaseByOAGNumber(SqlConn, ((BasePage)this.Page).CurrentUser.UserID, this.txtOAGCauseNumber.Text);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);

                    if (c.OCACourtID == this.CurrentUser.OCACourtID)
                        OpenHearingScreen((Guid)ds.Tables[0].Rows[0]["CauseGUID"], (short)ds.Tables[0].Rows[0]["NumberOfAction"]);
                    else
                        messageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&oag={0}", this.txtOAGCauseNumber.Text), true);
                }
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    private void OpenHearingScreen(Guid causeGUID, short numberOfAction)
    {
        CSCMS.Data.Hearing h = Helper.GetLatestHearing(CscmsContext, causeGUID, numberOfAction);
        if (h != null)
        {
            Response.Redirect(string.Format("~/MemberPages/HearingDetail.aspx?cid={0}&a={1}&hid={2}", causeGUID, numberOfAction, h.HearingID), true);
        }
        else
        {
            Response.Redirect(string.Format("~/MemberPages/HearingDetail.aspx?cid={0}&a={1}", causeGUID, numberOfAction), true);
        }
    }

    protected void btnGotoHideShow_Click(object sender, EventArgs e)
    {
        if (Session["MyPageShowHearing"] == null) Session["MyPageShowHearing"] = 1;
        if (int.Parse(Session["MyPageShowHearing"].ToString()) != 1)
        {
            grdHearings.Visible = true;

            this.grdHearings.Rebind();
            this.btnGotoHideShow.Text = "Hide Hearings";
            this.btnGotoHideShow.CssClass = "submitMedium";
            Session["MyPageShowHearing"] = 1;
        }
        else
        {
            grdHearings.Visible = false;
            this.btnGotoHideShow.Text = "Show Hearings";
            this.btnGotoHideShow.CssClass = "submitMediumBlue";
            Session["MyPageShowHearing"] = 0;
        }
    }
    protected void btnGotoActivityShowHide_Click(object sender, EventArgs e)
    {
        if (Session["ShowActivityDateTime"] == null) Session["ShowActivityDateTime"] = 1;
        if (int.Parse(Session["ShowActivityDateTime"].ToString()) != 1)
        {
            grdDailyCourtUserActivity.Visible = true;

            this.grdDailyCourtUserActivity.DataBind();
            this.btnGotoActivityShowHide.Text = "Hide Activities";
            this.btnGotoActivityShowHide.CssClass = "submitMedium";
            Session["ShowActivityDateTime"] = 1;
        }
        else
        {
            grdDailyCourtUserActivity.Visible = false;
            this.btnGotoActivityShowHide.Text = "Show Activities";
            this.btnGotoActivityShowHide.CssClass = "submitMediumBlue";
            Session["ShowActivityDateTime"] = 0;
        }
    }
    protected void btnGoto_Click(object sender, EventArgs e)
    {
        //scheduler.SelectedDate = (DateTime)this.dtGoto.SelectedDate;
        Session["MyPageHearingDateTime"] = (DateTime)this.dtGoto.SelectedDate;
        scHearings.HeaderText = (DateTime.Parse(Session["MyPageHearingDateTime"].ToString())).ToShortDateString() + " HEARINGS";
        Page.Session["MyPageHearings"] = null;
        this.grdHearings.Rebind();
    }

    protected void btnRefreshHearings_OnClick(object sender, EventArgs e)
    {
        Page.Session["MyPageHearings"] = null;
        this.grdHearings.Rebind();
    }

    protected void btnGotoActivity_Click(object sender, EventArgs e)
    {
        Session["MyPageActivityDateTime"] = (DateTime)this.dtGotoActivityDate.SelectedDate;
        scDailyCourtUserActivity.HeaderText = (DateTime.Parse(Session["MyPageActivityDateTime"].ToString())).ToShortDateString() + " COURTS USERS ACTIVITIES";
        this.grdDailyCourtUserActivity.DataBind();
    }

    protected void btnExportHearings_Click(object sender, EventArgs e)
    {
        this.grdHearings.ExportSettings.ExportOnlyData = true;
        grdHearings.ExportSettings.IgnorePaging = true;
        grdHearings.ExportSettings.OpenInNewWindow = true;
        grdHearings.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        grdHearings.MasterTableView.ExportToExcel();
    }

    protected void btnExportActivity_Click(object sender, EventArgs e)
    {
        this.grdDailyCourtUserActivity.ExportSettings.ExportOnlyData = true;
        grdDailyCourtUserActivity.ExportSettings.IgnorePaging = true;
        grdDailyCourtUserActivity.ExportSettings.OpenInNewWindow = true;
        grdDailyCourtUserActivity.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        grdDailyCourtUserActivity.MasterTableView.ExportToExcel();
    }

    protected void btnExportReminder_Click(object sender, EventArgs e)
    {
        this.grdReminder.ExportSettings.ExportOnlyData = true;
        grdReminder.ExportSettings.IgnorePaging = true;
        grdReminder.ExportSettings.OpenInNewWindow = true;
        grdReminder.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        grdReminder.MasterTableView.ExportToExcel();
    }

    protected void btnExporTickler_Click(object sender, EventArgs e)
    {
        this.grdTicklers.ExportSettings.ExportOnlyData = true;
        grdTicklers.ExportSettings.IgnorePaging = true;
        grdTicklers.ExportSettings.OpenInNewWindow = true;
        grdTicklers.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        grdTicklers.MasterTableView.ExportToExcel();
    }


    #endregion

    #region [Grids]
    protected void grd_ExcelMLExportStylesCreated(object source, GridExportExcelMLStyleCreatedArgs e)
    {
        foreach (StyleElement style in e.Styles)
        {
            if (style.Id == "headerStyle")
            {
                style.FontStyle.Bold = true;
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "itemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "alternatingItemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
        }
    }
    #endregion

    protected List<PendingCaseSummary> GetPendingCasesWithAge()
    {
        List<PendingCaseSummary> pcsList = new List<PendingCaseSummary>();
        if (Page.Session["PendingCaseWithAge"] == null)
        {
            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
              CommandType.StoredProcedure,
              "GetPendingCasesForCourt",
              new SqlParameter("@OCACourtID", base.CurrentUser.OCACourtID));

            DataRow[] rows = ds.Tables[0].Select();
            List<PendingCaseWithRangeData> list = new List<PendingCaseWithRangeData>();
            foreach (DataRow row in rows)
            {
                PendingCaseWithRangeData obj = new PendingCaseWithRangeData();
                obj.ActionGUID = (Guid)row["ActionGUID"];
                obj.CauseNumber = (string)row["CauseNumber"];
                obj.AgeInDays = (int)row["AgeInDays"];
                list.Add(obj);
            }

            PendingCaseSummary p90 = new PendingCaseSummary { RangeName = "Pending <= 90 Days", RangeTypeID = 1 };
            p90.NumberOfCase = list.Where(it => it.AgeInDays != null && it.AgeInDays >= 0 && it.AgeInDays <= 90).Count();

            PendingCaseSummary p180 = new PendingCaseSummary { RangeName = "Pending 91-180 Days", RangeTypeID = 2 };
            p180.NumberOfCase = list.Where(it => it.AgeInDays != null && it.AgeInDays >= 91 && it.AgeInDays <= 180).Count();

            PendingCaseSummary p365 = new PendingCaseSummary { RangeName = "Pending 181-365 Days", RangeTypeID = 3 };
            p365.NumberOfCase = list.Where(it => it.AgeInDays != null && it.AgeInDays >= 181 && it.AgeInDays <= 365).Count();

            PendingCaseSummary p365More = new PendingCaseSummary { RangeName = "Pending > 365 Days", RangeTypeID = 4 };
            p365More.NumberOfCase = list.Where(it => it.AgeInDays != null && it.AgeInDays >= 366).Count();

            pcsList.Add(p90);
            pcsList.Add(p180);
            pcsList.Add(p365);
            pcsList.Add(p365More);
        }
        else
        {
            pcsList = (List<PendingCaseSummary>)Page.Session["PendingCaseWithAge"];
        }

        Page.Session["PendingCaseWithAge"] = pcsList;
        return pcsList;
    }

    protected void RadOpenCases_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        DataSet ds = new DataSet();

        if (Page.Session["CauseSummaryByOCACourt"] == null)
        {
            ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                  CommandType.StoredProcedure,
                  "GetCauseSummaryByOCACourt",
                  new SqlParameter("@OCACourtID", base.CurrentUser.OCACourtID));
        }
        else
        {
            ds = (DataSet)Page.Session["CauseSummaryByOCACourt"];
        }
        Page.Session["CauseSummaryByOCACourt"] = ds;
        RadOpenCases.DataSource = ds;
    }

    protected void PendingCasesWithAge_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        PendingCasesWithAge.DataSource = GetPendingCasesWithAge();
    }

    protected void grdTicklers_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (!string.IsNullOrEmpty(this.ddlTicklerType.SelectedValue))
        {
            int ticker = int.Parse(this.ddlTicklerType.SelectedValue);
            DataSet ds = null;
            string key = "Ticklers" + this.ddlTicklerType.SelectedValue;
            if (Page.Session[key] == null)
            {
                ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                  CommandType.StoredProcedure,
                  "GetTicklersByOCACourt",
                  new SqlParameter("@OCACourtID", base.CurrentUser.OCACourtID),
                  new SqlParameter("@RequestedTickler", ticker));

            }
            else
            {
                ds = (DataSet)Page.Session[key];
            }

            Page.Session[key] = ds;
            grdTicklers.DataSource = ds;
        }
    }

    protected void grdHearings_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<HearingData> list = new List<HearingData>();
        if (Page.Session["MyPageHearings"] == null)
        {
            DateTime st = System.DateTime.Today;
            DateTime et = System.DateTime.Today;
            short courtId = 9999;

            if (this.CurrentUser.OCACourtID > 0)
            {
                courtId = this.CurrentUser.OCACourtID;
                if (Session["MyPageHearingDateTime"] != null)
                {
                    st = DateTime.Parse(Session["MyPageHearingDateTime"].ToString());
                    et = DateTime.Parse(Session["MyPageHearingDateTime"].ToString());
                }
            }
            list = Helper.GetCalendarHearings(courtId, st, et);
        }
        else
        {
            list = (List<HearingData>)Page.Session["MyPageHearings"];
        }
        Page.Session["MyPageHearings"] = list;
        grdHearings.DataSource = list;
    }

    private class PendingCaseWithRangeData
    {
        public string CauseNumber { get; set; }
        public Guid ActionGUID { get; set; }
        public int? AgeInDays { get; set; }
    }

    DateTime dt1 = DateTime.Now;
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }
    protected override void OnUnload(EventArgs e)
    {
        DateTime dt2 = DateTime.Now;
        int secs = dt2.Subtract(dt1).Seconds;
        base.OnUnload(e);
    }

    protected void btnRefreshCaseLoad_OnClick(object sender, EventArgs e)
    {
        Page.Session["PendingCaseWithAge"] = null;
        PendingCasesWithAge.Rebind();

        Page.Session["CauseSummaryByOCACourt"] = null;
        RadOpenCases.Rebind();
    }

    protected void btnRefreshTicklers_OnClick(object sender, EventArgs e)
    {
        string key = "Ticklers" + this.ddlTicklerType.SelectedValue;
        Page.Session[key] = null;
        grdTicklers.Rebind();
    }

    protected override void OnPreRenderComplete(EventArgs e)
    {
        Panel pnlUserActivity = (Panel)scDailyCourtUserActivity.FindControl("pnlUserActivity");
        Panel pnlHearings = (Panel)scHearings.FindControl("pnlHearings");

        ajaxManager.AjaxSettings.AddAjaxSetting(pnlUserActivity, pnlUserActivity, this.loadingPanel);
        ajaxManager.AjaxSettings.AddAjaxSetting(pnlHearings, pnlHearings, this.loadingPanel);

        Action<SectionContainer, string> setMinMax = delegate(SectionContainer section, string sessionKey)
        {
            if (Helper.IsSectionMinimized(sessionKey))
            {
                section.Minimized = true;
            }
        };
        setMinMax(scTicklers, "MyPageTicklersSectionMinMax");
        setMinMax(scCaseLoad, "MyPageCaseLoadSectionMinMax");
        setMinMax(scHearings, "MyPageTodaysHearingsSectionMinMax");
        setMinMax(scDailyCourtUserActivity, "MyPageUserActivitySectionMinMax");
        setMinMax(scMessage, "MyPageBroadCastMessagesSectionMinMax");
        setMinMax(scReminders, "MyPageRemindersSectionMinMax");

        base.OnPreRenderComplete(e);
    }

    protected void btnGetTicklers_OnClick(object sender, EventArgs e)
    {
        grdTicklers.Rebind();
    }
}
