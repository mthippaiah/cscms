﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="RelatedCauses.aspx.cs" Inherits="MemberPages_RelatedCauses" %>

<%@ Register Src="../UserControls/CauseSearch.ascx" TagName="CauseSearch" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .posCenter
        {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle" style="float: left;">Add Related Cause</div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <div>
        <uc1:CauseSearch ID="causeSearch" runat="server" OnCauseSearchResult="HandleCauseSearchResult" />
    </div>
    <CSCMSUC:SectionContainer ID="sectionResults" runat="server" HeaderText="Search Result"
        Width="100%">
        <div id="divSearchResult" runat="server" visible="false">
            <div>
                <telerik:RadGrid ID="grdCases" runat="server" AllowPaging="True" PageSize="10" AllowSorting="True"
                    OnPageIndexChanged="grdCases_OnPageIndexChanged">
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <MasterTableView NoMasterRecordsText="Your search found no results. Try broadening your search criteria."
                        DataKeyNames="CauseGUID, NumberOfAction,Style ">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Cause Number" DataField="CauseNumber" />
                            <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                            <telerik:GridBoundColumn HeaderText="OAG" DataField="OAGCauseNumber" />
                            <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                            <telerik:GridBoundColumn HeaderText="Actions" DataField="NumberofAction" />
                            <telerik:GridBoundColumn HeaderText="Status" DataField="CauseStatus" />
                        </Columns>
                    </MasterTableView>
                    <PagerStyle AlwaysVisible="true" />
                </telerik:RadGrid>
            </div>
            <div class="posCenter">
                <asp:Button ID="btnSelectCause" runat="server" Text="Save" OnClick="btnSave_Click"
                    CssClass="submitMedium" />
                <asp:Button ID="Button1" runat="server" Text="Save &amp; Return to Case" OnClick="btnSaveAndReturn_Click"
                    CssClass="submitMedium" />
            </div>
        </div>
    </CSCMSUC:SectionContainer>
</asp:Content>
