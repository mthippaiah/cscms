﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI.Calendar;

public partial class MemberPages_HearingDetail : ActionBasePage
{
    #region [Properties]

    protected bool NewHearing
    {
        get { return Convert.ToBoolean(ViewState["NewHearing"]); }
        set { ViewState["NewHearing"] = value; }
    }

    public string DispositionTypeIDString
    {
        get
        {
            if (this.CurrentAction != null && this.CurrentAction.DispositionType != null)
                return this.CurrentAction.DispositionType.DispositionTypeID.ToString();

            return "";
        }
    }

    public string DispositionDetailIDString
    {
        get
        {
            if (this.CurrentAction != null && this.CurrentAction.DispositionDetail != null)
                return this.CurrentAction.DispositionDetail.DispositionDetailID.ToString();

            return "";
        }
    }
    #endregion

    #region [Page]
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        actionInfo.CauseGUID = new Guid(Request.QueryString["cid"]);
        actionInfo.ActionNumber = short.Parse(Request.QueryString["a"]);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bool iOpenedConn = false;
            try
            {
                if (CscmsContext.Connection.State != ConnectionState.Open)
                {
                    CscmsContext.Connection.Open();
                    iOpenedConn = true;
                }
                rvDispositionRenderedDate.MaximumValue = DateTime.Now.ToShortDateString();
                this.rvDispositionSignedDate.MaximumValue = DateTime.Now.ToShortDateString();

                hearingDateTooltip.TargetControlID = dtHearingDate.DateInput.ClientID;

                if (Request.QueryString["message"] == "SaveAndAddAnotherHearing")
                {
                    messageBox.SuccessMessage = "Hearing was saved successfully.";
                }

                LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));
                CSCMS.Data.Hearing h = null;
                if (!string.IsNullOrEmpty(Request.QueryString["hid"]))
                {
                    long hearingId = long.Parse(Request.QueryString["hid"]);
                    h = this.CscmsContext.Hearing.Where(it => it.HearingID == hearingId).FirstOrDefault();
                }

                if (h != null)
                {
                    this.NewHearing = false;
                    LoadHearing(int.Parse(Request.QueryString["hid"]));

                }
                else
                {
                    this.NewHearing = true;
                    InitializeHearing();
                }

                PopulateForm();

                if (!this.NewHearing)
                {
                    CheckForWarnings();
                }

                if (this.CurrentUser.ViewAllUser && Convert.ToBoolean(Session["Impersonating"]) == false) btnSaveHearingSchedule.Visible = false;

                btnDelete.Visible = btnSaveHearingSchedule.Visible;

                //dtHearingDate.MinDate = ((DateTime)this.CurrentAction.ActionFiledDate).Date;
                this.ddlDisposition.DataBind();
            }
            finally
            {
                if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                    CscmsContext.Connection.Close();
            }
        }

        if (Request.QueryString["pr"] != "1")
        {
            hearingDateTooltip.VisibleOnPageLoad = false;
            hearingDateTooltip.Visible = false;
        }

        this.dtHearingDate.Focus();
    }
    #endregion

    #region Load Data
    private void PopulateForm()
    {
        dtHearingDate.SelectedDate = this.CurrentHearing.HearingDate != DateTime.MinValue ? this.CurrentHearing.HearingDate : (DateTime?)null;

        if (this.NewHearing)
        {
            TimeSpan ts = CscmsContext.OCACourt.Where(it => it.OCACourtID == this.CurrentUser.OCACourtID).Select(it => it.DefaultHearingTime).FirstOrDefault();
            hearingTime.SelectedDate = DateTime.Today.Add(ts);
        }
        else
        {
            hearingTime.SelectedDate = this.CurrentHearing.HearingTime.HasValue ? DateTime.Today.Add(this.CurrentHearing.HearingTime.Value) : (DateTime?)null;
            rblMorningDocket.SelectedValue = this.CurrentHearing.MorningDocket.ToString();
            txtHearingNote.Text = this.CurrentHearing.HearingNote;
        }

        if (this.CurrentAction.DispositionRenderedDate.HasValue && this.CurrentAction.DispositionRenderedDate != DateTime.MinValue)
        {
            dtDispositionRenderedDate.SelectedDate = this.CurrentAction.DispositionRenderedDate > DateTime.Now ? DateTime.Now : this.CurrentAction.DispositionRenderedDate;
        }
        if (this.CurrentAction.DispositionSignedDate.HasValue && this.CurrentAction.DispositionSignedDate != DateTime.MinValue)
        {
            this.dtDispositionSignedDate.SelectedDate = this.CurrentAction.DispositionSignedDate > DateTime.Now ? DateTime.Now : this.CurrentAction.DispositionSignedDate;
        }
        if (this.CurrentAction.DispositionType != null)
        {
            ddlDisposition.SelectedValue = this.CurrentAction.DispositionType.DispositionTypeID.ToString();
        }

        if (this.CurrentAction.DispositionDetail != null)
        {
            ddlDispositionDetail.SelectedValue = this.CurrentAction.DispositionDetail.DispositionDetailID.ToString();
        }

        SetFieldVisibility();
    }

    private void SetFieldVisibility()
    {
        if (!NewHearing)
        {
            btnDelete.Visible = true;
            divHearingButtons.Visible = true;
            divMain.Attributes["class"] = "existingCase";
            divMainSub.Attributes["class"] = "existingCaseSub";
            if (Session["ScreenHeight"] != null) divMain.Style["Height"] = (325 + System.Math.Max(768, int.Parse(Session["ScreenHeight"].ToString())) - 768).ToString() + "px";
        }
    }

    private void InitializeHearing()
    {
        CSCMS.Data.Hearing hearing = new CSCMS.Data.Hearing();
        hearing.Action = CscmsContext.Action.Where(it => it.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();
        hearing.CreateDate = DateTime.Now;
        hearing.LastUpdateDate = DateTime.Now;

        this.CurrentHearing = hearing;
    }

    private void LoadHearing(long hearingID)
    {
        this.CurrentHearing = CscmsContext.Hearing.Include("Action").Where(it => it.HearingID == hearingID).FirstOrDefault();

        if (this.CurrentHearing.Action.ActionGUID != this.CurrentAction.ActionGUID)
        {
            CurrentHearing = null;
            Response.Redirect("~/AnonPages/Unauthorized.aspx", true);
        }

        divCaseNotes.Visible = true;
    }

    #endregion

    #region [Buttons]

    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        bool iOpenedCon = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedCon = true;
            }

            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(SqlConn,
                    CommandType.StoredProcedure,
                    "DeleteActionHearing",
                    new SqlParameter("@ActionGUID", this.CurrentHearing.Action.ActionGUID.ToString()),
                    new SqlParameter("@HearingID", this.CurrentHearing.HearingID));

            Helper.SetActionStatusWhenHearingUpdatedOrDeleted(CscmsContext, this.CurrentAction, null);
            this.CurrentHearing = null;
            Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=DeleteHearing", true);
        }
        finally
        {
            if (iOpenedCon && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            UpdateHearing();

            if (this.NewHearing)
            {
                this.NewHearing = false;
            }

            LoadHearing(this.CurrentHearing.HearingID);

            PopulateForm();

            messageBox.SuccessMessage = "Hearing saved successfully.";
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void btnSaveAndAddAnother_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        bool redirect = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            UpdateHearing();
            redirect = true;
        }
        catch (Exception ex)
        {
            LogError.LogErrorToSQL(ex.ToString());
            messageBox.ErrorMessage = ex.Message;
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();

            //Redirect is done like this, otherwise System.Threading.ThreadAbortException is raised
            if (redirect)
                Response.Redirect(string.Format("HearingDetail.aspx?cid={0}&a={1}&message=SaveAndAddAnotherHearing", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber), true);
        }
    }

    protected void btnSaveAndReturntoCase_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        bool redirect = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            UpdateHearing();

            messageBox.SuccessMessage = "Hearing saved successfully.";
            redirect = true;
        }
        catch (Exception ex)
        {
            LogError.LogErrorToSQL(ex.ToString());
            messageBox.ErrorMessage = ex.Message;
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();

            //Redirect is done like this, otherwise System.Threading.ThreadAbortException is raised
            if (redirect)
                Response.Redirect(string.Format("ActionDetail.aspx?cid={0}&a={1}&message=SaveHearing&hd={2}", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber, this.dtHearingDate.SelectedDate.Value), true);
        }
    }

    protected void dtHearingDate_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
    {
        if (this.grdHearings.Visible)
        {
            SetHearingGridData();
            grdHearings.DataBind();
        }

        GetHearingbyCountyCount();

        if (dtHearingDate.SelectedDate.HasValue && dtHearingDate.SelectedDate.Value.Date < DateTime.Now.Date)
        {
            messageBox.InfoMessage = "The Hearing Date selected is a date in the past.";  
            //hearingDateTooltip.Text = "The Hearing Date selected is a date in the past.";
            //hearingDateTooltip.VisibleOnPageLoad = true;
            //hearingDateTooltip.Visible = true;
        }
        else
        {
            hearingDateTooltip.VisibleOnPageLoad = false;
            hearingDateTooltip.Visible = false;
        }

        this.hearingTime.Focus();
    }

    private void UpdateHearing()
    {
        if (this.NewHearing)
        {
            InitializeHearing();
            CscmsContext.AddToHearing(this.CurrentHearing);
        }
        else
        {
            if (this.CurrentHearing.EntityState == EntityState.Detached)
                CscmsContext.Attach(this.CurrentHearing);
        }

        this.CurrentHearing.HearingDate = dtHearingDate.SelectedDate.Value;
        this.CurrentHearing.HearingTime = hearingTime.SelectedDate.HasValue ? hearingTime.SelectedDate.Value.TimeOfDay : (TimeSpan?)null;

        if (hearingTime.SelectedDate.HasValue)
        {
            this.CurrentHearing.MorningDocket = (hearingTime.SelectedDate.Value.TimeOfDay.Hours <= 12) ? true : false;
        }
        else
        {
            this.CurrentHearing.HearingTime = System.DateTime.Parse("09:00:00").TimeOfDay;
            this.CurrentHearing.MorningDocket = true;
        }

        // this is also use for warning message when schedule on date off
        rblMorningDocket.SelectedValue = this.CurrentHearing.MorningDocket.ToString();

        this.CurrentHearing.HearingNote = txtHearingNote.Text;

        short? dispTypeId = !string.IsNullOrEmpty(ddlDisposition.SelectedValue) ? short.Parse(ddlDisposition.SelectedValue) : (short?)null;
        short? dispDetailID = !string.IsNullOrEmpty(ddlDispositionDetail.SelectedValue) ? short.Parse(ddlDispositionDetail.SelectedValue) : (short?)null;

        if (dispTypeId != null)
            this.CurrentAction.DispositionType = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == dispTypeId).FirstOrDefault();
        else
            this.CurrentAction.DispositionType = null;

        if (dispDetailID != null)
            this.CurrentAction.DispositionDetail = CscmsContext.DispositionDetail.Where(it => it.DispositionDetailID == dispDetailID).FirstOrDefault();
        else
            this.CurrentAction.DispositionDetail = null;

        bool dispRDateChanged = (this.CurrentAction.DispositionRenderedDate != dtDispositionRenderedDate.SelectedDate);
        this.CurrentAction.DispositionRenderedDate = dtDispositionRenderedDate.SelectedDate.HasValue ? dtDispositionRenderedDate.SelectedDate.Value : (DateTime?)null;
        this.CurrentAction.DispositionSignedDate = this.dtDispositionSignedDate.SelectedDate.HasValue ? dtDispositionSignedDate.SelectedDate.Value : (DateTime?)null;

        if (this.CurrentAction.EntityState == EntityState.Detached)
            CscmsContext.Attach(this.CurrentAction);

        if (dispRDateChanged)
        {
            CSCMS.Data.ChildSupport childSupport = this.CurrentAction.ChildSupport.Where(d => d.Action.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();
            if (childSupport != null)
            {
                if (childSupport.EntityState == EntityState.Detached)
                    CscmsContext.Attach(childSupport);
            }
            else
            {
                childSupport = new CSCMS.Data.ChildSupport();
                childSupport.Action = CscmsContext.Action.Where(it => it.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();

                childSupport.CreateDate = DateTime.Now;
                childSupport.LastUpdateDate = DateTime.Now;
                CscmsContext.AddToChildSupport(childSupport);

            }
            childSupport.ArrearageAsOf = dtDispositionRenderedDate.SelectedDate;
            childSupport.PastDueMedicalAsOf = dtDispositionRenderedDate.SelectedDate;
        }

        CscmsContext.SaveChanges();
        Helper.SetActionStatusWhenHearingUpdatedOrDeleted(CscmsContext, this.CurrentAction, CurrentHearing);
        LoadCause(this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
        CheckForWarnings();

        grdHearings.Rebind();

        grdCurrentActionHearing.Rebind();
    }

    private void CheckForWarnings()
    {
        // Warn the user if this is on a weekend or day off
        if (dtHearingDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Saturday ||
            dtHearingDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Sunday)
        {
            messageBox.ErrorMessage = "Warning: The selected Hearing Date occurs on a weekend.";
        }
        else
        {
            bool isMorningDocket = Convert.ToBoolean(rblMorningDocket.SelectedValue);
            CSCMS.Data.SchedulingException se = CscmsContext.SchedulingException.Where(it => it.OCACourt.OCACourtID == this.CurrentUser.OCACourtID &&
                it.ExceptionDate == dtHearingDate.SelectedDate.Value &&
                ((it.BlockMorning && isMorningDocket) || (it.BlockAfternoon && isMorningDocket))).FirstOrDefault();

            if (se != null)
            {
                messageBox.ErrorMessage = string.Format("Warning: The selected Hearing Date occurs on a scheduled day off ({0})", se.Note);
            }
        }

        GetHearingbyCountyCount();
    }

    private void GetHearingbyCountyCount()
    {
        if (dtHearingDate.SelectedDate.HasValue)
        {
            Dictionary<DateTime, string> hsummary = Helper.GetDateHearingSummary(this.CscmsContext, dtHearingDate.SelectedDate.Value, dtHearingDate.SelectedDate.Value, base.CurrentUser.OCACourtID);
            if (hsummary.Count > 0)
            {
                lblgrdHearings.Text = "Hearings on Same Date: " + hsummary[dtHearingDate.SelectedDate.Value];

                int countyCnt = lblgrdHearings.Text.Count(f => f.Equals('('));

                if (countyCnt > 1)
                    messageBox.ErrorMessage += @"<br>Multiple counties hearing on scheduled hearing date: " + dtHearingDate.SelectedDate.Value.ToString("MM/dd/yyyy") + " " + hsummary[dtHearingDate.SelectedDate.Value];

                lblgrdHearings.ForeColor = (countyCnt > 1) ? System.Drawing.Color.Red : System.Drawing.Color.Blue;
            }
        }
    }

    #endregion

    #region [DataSet]

    protected void SetHearingGridData()
    {
        List<HearingData> list = new List<HearingData>();
        if (this.CurrentUser.OCACourtID > 0 && dtHearingDate.SelectedDate.HasValue)
        {
            list = Helper.GetCalendarHearings(this.CurrentUser.OCACourtID, dtHearingDate.SelectedDate.Value, dtHearingDate.SelectedDate.Value);
            list = list.OrderByDescending(it => it.MorningDocket).ThenBy(it => it.HearingTime).ToList();
        }
        grdHearings.DataSource = list;
    }
    protected void dsActions_Selecting(object sender, LinqDataSourceSelectEventArgs e)
    {
        //CSCMSDataContext dc = new CSCMSDataContext();
        //e.Result = dc.GetHearingRelatedDocumentsAndOrders(this.CurrentHearing.HearingID);
    }
    protected void grdCurrentActionHearing_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
        CommandType.StoredProcedure,
        "GetActionHearings",
        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
        new SqlParameter("@Filter", ""));

        if (ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].DefaultView.Sort = "ActionDate DESC";
        }
        ((Telerik.Web.UI.RadGrid)source).DataSource = ds.Tables[0].DefaultView;
    }

    #endregion

    protected List<CSCMS.Data.DispositionType> GetDispositionTypes()
    {
        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            CscmsContext.DispositionType.MergeOption = System.Data.Objects.MergeOption.NoTracking;
            List<CSCMS.Data.DispositionType> list = CscmsContext.DispositionType.Where(it => it.IsActive == true).ToList();
            list = list.Where(it => !it.DispositionTypeName.Contains(MyConsts.DB_NONE_CASE_OPEN)).ToList();

            if (this.CurrentUser.UserType != UserType.OCAAdmin)
                list = list.Where(it => it.DispositionTypeName != MyConsts.DB_SYSTEM_ADMIN_CLOSURE).ToList();

            if (this.CurrentAction != null)
            {
                if (this.CurrentAction.DispositionType != null)
                {
                    CSCMS.Data.DispositionType temp = list.Where(it => it.DispositionTypeID == this.CurrentAction.DispositionType.DispositionTypeID).FirstOrDefault();
                    if (temp == null)
                    {
                        temp = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == this.CurrentAction.DispositionType.DispositionTypeID).FirstOrDefault();
                        list.Add(temp);
                    }
                }
            }
            list = list.OrderBy(it => it.SortOrder).ToList();
            return list;
        }
    }

    protected void btnShowHearings_OnClick(object sender, EventArgs args)
    {
        if (!this.grdHearings.Visible)
        {
            this.btnShowHearings.Text = "Hide Hearings";
            this.grdHearings.Visible = true;
            SetHearingGridData();
            grdHearings.DataBind();
        }
        else
        {
            this.btnShowHearings.Text = "Show Hearings";
            this.grdHearings.Visible = false;
        }

    }

    protected void grdHearings_OnPageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        SetHearingGridData();
        grdHearings.DataBind();
    }
}