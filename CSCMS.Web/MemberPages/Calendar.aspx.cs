﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CSCMS.Calendar;
using System.Data.Linq;
using System.Web.UI.HtmlControls;

public partial class MemberPages_Calendar : BasePage
{
    Dictionary<DateTime, string> calendarData = new Dictionary<DateTime, string>();
    #region [Page]
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["rows"]))
            {
                scheduler.MonthVisibleAppointmentsPerDay = int.Parse(Request.QueryString["rows"].ToString());
            }
            if (!string.IsNullOrEmpty(Request.QueryString["uid"]))
            {
                chkTimeVisible.Checked = bool.Parse(Request.QueryString["uid"].ToString());
            }

            if (!string.IsNullOrEmpty(Request.QueryString["um"]))
            {
                messageBox.InfoMessage = string.Format(Request.QueryString["rows"].ToString() + " Hearings moved successfully. <a href=\"Calendar.aspx?sd={0}\">Click here</a> if you would like to go to [{0}]. We could not move hearings where case dismissal date is less than [{0}]. Please select [Ignore Dismissal Validation] below if you want to move these hearings.", Request.QueryString["sd"], Request.QueryString["sd"]);

                CheckForWarnings(DateTime.Parse(Request.QueryString["sd"].ToString()));
            }
            if (Session["ums"] == null) Session["ums"] = "n";

            lnkTurnOffMoveSelected.Visible = false;
            if (!string.IsNullOrEmpty(Request.QueryString["ums"]))
            {
                Session["ums"] = Request.QueryString["ums"].ToString();
                if (Session["ums"].ToString() == "y")
                {
                    this.btnMoveHearings.Text = "Move Selected Hearings";
                    lnkTurnOffMoveSelected.Visible = true;
                    lnkTurnOffMoveSelected.NavigateUrl = "~/MemberPages/Calendar.aspx?ums=n";
                }
            }
            if (Session["Calendar.SelectedView"] != null)
            {
                scheduler.SelectedView = (SchedulerViewType)Session["Calendar.SelectedView"];
            }
            else
            {
                CSCMS.Data.OCACourt oc = CscmsContext.OCACourt.Where(it => it.OCACourtID == this.CurrentUser.OCACourtID).FirstOrDefault();
                if (oc != null)
                {
                    if (oc.DefaultCalendarView == "Day")
                    {
                        scheduler.SelectedView = SchedulerViewType.DayView;
                    }
                    else if (oc.DefaultCalendarView == "Week")
                    {
                        scheduler.SelectedView = SchedulerViewType.WeekView;
                    }
                    else
                    {
                        scheduler.SelectedView = SchedulerViewType.MonthView;
                    }
                }

            }
            if (Session["Calendar.SelectedDate"] != null)
            {
                scheduler.SelectedDate = (DateTime)Session["Calendar.SelectedDate"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["sd"]) && string.IsNullOrEmpty(Request.QueryString["um"]) && string.IsNullOrEmpty(Request.QueryString["uc"]))
            {
                scheduler.SelectedDate = DateTime.Parse(Request.QueryString["sd"]);
                Session["Calendar.SelectedDate"] = scheduler.SelectedDate;
            }
        }
        sectionAdditionalFunctions.Visible = (scheduler.SelectedView == SchedulerViewType.DayView);
        dtMoveToDate.MinDate = DateTime.Today.AddDays(0);
        dtMoveToDate.MaxDate = DateTime.Today.AddDays(1240);
    }

    protected override void OnPreRender(EventArgs e)
    {
        this.hfCurrentView.Value = "";
        base.OnPreRender(e);
    }

    #endregion

    #region [Buttons]

    protected void btnMoveHearings_Click(object sender, EventArgs e)
    {
        int rowsMoved = 0;

        if (Session["ums"].ToString() == "y")
        {
            if (Request.Form.GetValues("chkApt") == null)
            {
                messageBox.ErrorMessage = "You must select at least one hearing in order to use move hearing feature with [selected hearing option enable].";
                return;
            }
            foreach (string hearingID in Request.Form.GetValues("chkApt"))
            {

            }
        }

        Response.Redirect("~/MemberPages/Calendar.aspx?um=1&uid=" + chkTimeVisible.Checked.ToString() + "&rows=" + rowsMoved.ToString() + "&sd=" + ((DateTime)dtMoveToDate.SelectedDate).ToString("d"), true); // refresh calendar HN 10/06
    }

    protected void hearingTime_SelectedDateChanged(object sender, EventArgs e)
    {
        RadTimePicker timePicker = (RadTimePicker)sender;

        int hearingID = int.Parse(timePicker.Attributes["HearingID"]);
        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            CSCMS.Data.Hearing hearing = CscmsContext.Hearing.Where(it => it.HearingID == hearingID).FirstOrDefault();
            if (timePicker.SelectedDate.HasValue)
            {
                hearing.HearingTime = timePicker.SelectedDate.Value.TimeOfDay;
            }
            else
            {
                hearing.HearingTime = null;
            }
            CscmsContext.SaveChanges();
        }
        Response.Redirect("~/MemberPages/Calendar.aspx?uid=" + chkTimeVisible.Checked.ToString(), true); // refresh calendar HN 10/06
    }
    #endregion

    #region [scheduler]
    protected void scheduler_TimeSlotCreated(object sender, Telerik.Web.UI.TimeSlotCreatedEventArgs e)
    {
        if (scheduler.SelectedView == SchedulerViewType.MonthView)
        {
            HtmlGenericControl dateHeader = (HtmlGenericControl)e.TimeSlot.Control.Controls[0].Controls[0].Controls[0];

            if (string.IsNullOrEmpty(hfCurrentView.Value) || hfCurrentView.Value == "2")
            {
                if (e.TimeSlot != null && e.TimeSlot.Control != null)
                {
                    if (HttpContext.Current.Session["CalendarViewData"] != null)
                    {
                        if (((CalendarViewData)HttpContext.Current.Session["CalendarViewData"]).MonthViewData != null)
                            calendarData = ((CalendarViewData)HttpContext.Current.Session["CalendarViewData"]).MonthViewData;
                        else
                        {
                            calendarData = Helper.GetDateHearingSummary(this.CscmsContext, scheduler.VisibleRangeStart, scheduler.VisibleRangeEnd, this.CurrentUser.OCACourtID);
                            ((CalendarViewData)HttpContext.Current.Session["CalendarViewData"]).MonthViewData = calendarData;
                        }
                    }
                    if (calendarData.ContainsKey(e.TimeSlot.Start.Date))
                    {
                        dateHeader.InnerHtml = string.Format("{0}&nbsp;&nbsp;&nbsp;<span style='color:#CC0000'>{1}</span>",
                        dateHeader.InnerText, calendarData[e.TimeSlot.Start.Date]);
                    }
                }
            }
            else
            {
                dateHeader.InnerText = "";
            }
        }
    }


    protected void scheduler_NavigationComplete(object sender, SchedulerNavigationCompleteEventArgs e)
    {
        Session["Calendar.SelectedView"] = scheduler.SelectedView;
        Session["Calendar.SelectedDate"] = scheduler.SelectedDate;
        sectionAdditionalFunctions.Visible = (scheduler.SelectedView == SchedulerViewType.DayView);
    }

    protected void scheduler_AppointmentDataBound(object sender, SchedulerEventArgs e)
    {
        e.Appointment.ToolTip = e.Appointment.Attributes["DayCount"];
        if (e.Appointment.Attributes["DispositionDate"] != "")
        {
            if (e.Appointment.CssClass.Equals("rsCategoryGreen"))
                e.Appointment.CssClass = "rsCategoryDarkBlue";  // e.Appointment.Font.Bold = true;
            else
                e.Appointment.CssClass = "rsCategoryGreen";  // e.Appointment.Font.Bold = true;
        }
        else if (e.Appointment.Attributes["SpecialOrder"] != "")
            e.Appointment.CssClass = "rsCategoryGreen";
        else
            e.Appointment.CssClass = "rsCategory" + e.Appointment.Attributes["Color"];
    }
    #endregion

    private void CheckForWarnings(DateTime HearingDate)
    {
        RadDatePicker dtHearingDate = new RadDatePicker();

        dtHearingDate.SelectedDate = HearingDate;

        // Warn the user if this is on a weekend or day off
        if (dtHearingDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Saturday ||
            dtHearingDate.SelectedDate.Value.DayOfWeek == DayOfWeek.Sunday)
        {
            messageBox.ErrorMessage = "Warning: The selected Hearing Date occurs on a weekend.";
        }
        else
        {
            CSCMS.Data.SchedulingException se = CscmsContext.SchedulingException.Where(it => it.OCACourt.OCACourtID == this.CurrentUser.OCACourtID && it.ExceptionDate == dtHearingDate.SelectedDate).FirstOrDefault();
            if (se != null)
            {
                messageBox.ErrorMessage = string.Format("Warning: The selected Hearing Date occurs on a date that has scheduled time off ({0})", se.Note);
            }
        }
    }

    protected void HandleDateSetClickedEvent(object sender, EventArgs e)
    {
        scheduler.SelectedDate = DateCalculation.GetDate();
        Session["Calendar.SelectedDate"] = scheduler.SelectedDate;
        HttpContext.Current.Session["CalendarViewData"] = null;
    }

    protected void btnRefresh_OnClick(object sender, EventArgs args)
    {
        HttpContext.Current.Session["CalendarViewData"] = null;
        scheduler.Rebind();
    }

    protected void printScheduler_OnClick(object sender, EventArgs args)
    {
        scheduler.Rebind();
        DocumentDir dd = Helper.CreateDirectory(this.CurrentUser.OCACourtID.ToString(), this.CurrentUser.UserID.ToString(), "", "");
        string fileName = Path.Combine(dd.UserLevelDir + "\\", "Calendar" + ".pdf");
        try
        {
            if (File.Exists(fileName))
                File.Delete(fileName);

            Helper.DeleteFiles(dd.UserLevelDir, "*.pdf", DateTime.Now.AddDays(-1));
        }
        catch
        {
        }
        CalendarPdfBuilder.Build(this.scheduler, fileName);
        string filePath = Request.ApplicationRoot() + "/documents/" + this.CurrentUser.OCACourtID.ToString() + "/" + this.CurrentUser.UserID.ToString() + "/Calendar.pdf";
        string script = "<script language=javascript>window.open('" + filePath + "', 'PrintMe','height=800px,width=977px,scrollbars=1,resizable=1');</script>";
        ClientScript.RegisterStartupScript(this.GetType(), "PrintPdf", script);
    }
}
