﻿<%@ Page Title="CSCMS | Merge Person" Language="C#" MasterPageFile="~/CSCMS.master"
    AutoEventWireup="true" CodeFile="PersonMerger.aspx.cs" Inherits="MemberPages_PersonMerger"
    EnableEventValidation="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="MetaBuilders.WebControls" Namespace="MetaBuilders.WebControls"
    TagPrefix="mb" %>
<%@ Register TagPrefix="UC" TagName="PersonMerge" Src="~/UserControls/PersonMerge.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        MERGE PERSONS</div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <CSCMSUC:SectionContainer ID="sectionCriteria" runat="server" Width="100%" DisplayType="Note">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td class="fieldLabel">
                                            Person Category:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlPersonCategory" runat="server" Width="200">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                                <asp:ListItem Value="1">Child</asp:ListItem>
                                                <asp:ListItem Value="2">Respondent</asp:ListItem>
                                                <asp:ListItem Value="3">Attorney</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" Text="Search Persons" CssClass="submit"
                                                OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td class="fieldLabel">
                                            Last Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLastName" runat="server" Width="100" ToolTip=":D" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            First Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFirstName" runat="server" Width="100" ToolTip=":D" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td class="fieldLabel">
                                            Birth Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtBirthdate" runat="server" Width="92" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            Sex:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSex" runat="server" Width="77">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </CSCMSUC:SectionContainer>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:RadioButton ID="rbKeepA" runat="server" GroupName="MergeChoice" Text="Keep A (Delete B)" />
            </td>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbKeepB" runat="server" GroupName="MergeChoice" Text="Keep B (Delete A)" />
                        </td>
                        <td>
                            &nbsp;<asp:Button ID="btnMerge" runat="server" Text="Merge Persons" CssClass="submitMedium"
                                OnClick="btnMerge_Click" />
                            <asp:Button ID="btnMergeValidate" runat="server" Text="Validate" CssClass="submitMedium"
                                OnClick="btnMergeValidate_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="5">
            </td>
        </tr>
        <tr>
            <td valign="top">
                <CSCMSUC:SectionContainer ID="sectionA" runat="server" HeaderText="Person A" Width="350">
                    <UC:PersonMerge ID="personMergeA" runat="server" />
                </CSCMSUC:SectionContainer>
            </td>
            <td width="10">
                &nbsp;
            </td>
            <td valign="top">
                <CSCMSUC:SectionContainer ID="sectionB" runat="server" HeaderText="Person B" Width="350">
                    <UC:PersonMerge ID="personMergeB" runat="server" />
                </CSCMSUC:SectionContainer>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <!-- grdCases -->
                <%-- NoMasterRecordsText="There is no case where these two persons are included. You should be able to merge these two persons."--%>
                <telerik:RadGrid ID="grdCases" runat="server" AllowPaging="True" PageSize="10" AllowSorting="True">
                    <MasterTableView>
                        <Columns>
                            <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, NumberOfAction"
                                DataNavigateUrlFormatString="../MemberPages/ActionDetail.aspx?cid={0}&a={1}"
                                SortExpression="CauseNumber" DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                            <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                            <telerik:GridBoundColumn HeaderText="OAG" DataField="OAGCauseNumber" />
                        </Columns>
                    </MasterTableView>
                    <PagerStyle AlwaysVisible="true" />
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="dsPersons" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
        SelectCommand="SearchCaseByPersonGUIDDuplicate" SelectCommandType="StoredProcedure"
        CancelSelectOnNullParameter="false" OnSelecting="dsPersons_Selecting">
        <SelectParameters>
            <asp:Parameter Name="UserId" Type="Int32" />
            <asp:Parameter Name="PersonGUID" Type="string" />
            <asp:Parameter Name="PersonGUID2" Type="string" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
