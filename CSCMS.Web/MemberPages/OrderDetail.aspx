﻿<%@ Page Title="CSCMS | Order" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="OrderDetail.aspx.cs" Inherits="MemberPages_OrderDetail" %>

<%@ Register TagPrefix="UC" TagName="ActionInfo" Src="~/UserControls/ActionSummary/ActionInfo.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNotes" Src="~/UserControls/CaseNotes.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNavigation" Src="~/UserControls/CaseNavigation.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        var actionType;
        function ValidateSave() {
            if (Page_ClientValidate("SaveOrder") == false)
                return false;
            var ot = document.getElementById("<%= this.ddlOrderType.ClientID %>");
            var dtOr = $find("<%= dtOrderedDate.ClientID %>");
            var dtAr = $find("<%= dtArrestCommitmentEffectiveDate.ClientID %>");
            var orText = ot.options[ot.selectedIndex].text;
            if (orText != "Commitment" && orText != "Paternity Test") {
                var dt1 = dtOr.get_selectedDate();
                var dt2 = dtAr.get_selectedDate();
                if (dt2 != null && dt2 < dt1) {
                    alert("Arrest date should not be before order date");
                    return false;
                }
            }
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle" style="width: 100%">
        <asp:Label ID="lblSpecialOrder" Text="Special/Supplemental Order" runat="server"></asp:Label>
    </div>
    <div style="float: right; width: 100%; padding-bottom: 5px;" id="divSpecialOrderButtons"
        runat="server" visible="false">
        <div style="float: left;">
            <UC:CaseNavigation ID="caseNavigation" runat="server" IncludeEmptyItem="true" />
        </div>
        <div style="float: left;">
            &nbsp;
            <asp:Button ID="btnDelete" runat="server" type="button" Visible="false" class="submitMediumRed"
                Style="width: 80px" OnCommand="btnDelete_Command" OnClientClick="return confirm('Are you sure you want to delete this SpecialOrder?');"
                Text="Delete Order" />
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" />
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <telerik:RadWindow ID="winAssociate" runat="server" Skin="Office2007" Modal="true"
        ShowContentDuringLoad="false" ReloadOnShow="true" Behaviors="Close" VisibleStatusbar="false"
        Width="410" Height="200" />
    <div id="divMain" runat="server" class="newCase">
        <div id="divMainSub" runat="server" class="newCaseSub" style="width: 873px;">
            <UC:ActionInfo ID="actionInfo" runat="server" />
            <CSCMSUC:SectionContainer ID="sectionSpecialOrder" runat="server" HeaderText="ORDER INFORMATION"
                Width="100%">
                <asp:Panel ID="pnlSpecialOrder" runat="server" DefaultButton="btnSaveSpecialOrder">
                    <table align="center">
                        <tr>
                            <td valign="top">
                                <table cellspacing="3" align="center">
                                    <tr>
                                        <td class="fieldLabelReq">
                                            Order for:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlRequestedBy" runat="server" Width="202" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvddlRequestedBy" runat="server" ControlToValidate="ddlRequestedBy"
                                                ValidationGroup="SaveOrder" Display="Dynamic" ErrorMessage="Please select a person">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabelReq">
                                            Order Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlOrderType" runat="server" Width="202" DataSource='<%# GetOrderTypes() %>'
                                                DataValueField="SpecialOrderTypeID" DataTextField="SpecialOrderTypeName" AppendDataBoundItems="true"
                                                SelectedValue='<%# SpecialOrderTypeIDString %>'>
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvOrderType" runat="server" ControlToValidate="ddlOrderType"
                                                ValidationGroup="SaveOrder" Display="Dynamic" ErrorMessage="Please select an order type">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabelReq">
                                            <asp:Label ID="lblCapiasOrderedDate" Text="Ordered Date" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtOrderedDate" runat="server" />
                                            <asp:RequiredFieldValidator ID="rfvdtOrderedDate" runat="server" ControlToValidate="dtOrderedDate"
                                                ValidationGroup="SaveOrder" Display="Dynamic" ErrorMessage="Please select an Order date">*</asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="rvdtOrderedDate" runat="server" ControlToValidate="dtOrderedDate"
                                                ValidationGroup="SaveOrder" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trPatEstDate" runat="server">
                                        <td class="fieldLabel">
                                            <asp:Label ID="lblPatEstDate" Text="Paternity Established Date" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtPatEstDate" runat="server" />
                                            <asp:RangeValidator ID="rvdtPatEstDate" runat="server" ControlToValidate="dtPatEstDate"
                                                ValidationGroup="SaveOrder" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trPatResultDate" runat="server">
                                        <td class="fieldLabel">
                                            <asp:Label ID="lblResultDate" Text="Result Date" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtPatResultDate" runat="server" />
                                            <asp:RangeValidator ID="rvdtPatResultDate" runat="server" ControlToValidate="dtPatResultDate"
                                                ValidationGroup="SaveOrder" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trPatTestResult" runat="server">
                                        <td class="fieldLabel">
                                            Test Result Positive
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkPatTestResult" runat="server" Text="" />
                                        </td>
                                    </tr>
                                     <tr id="trIsExculded" runat="server">
                                        <td class="fieldLabel">
                                            Excluded
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkExcluded" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr id="trCapIssuedDate" runat="server">
                                        <td class="fieldLabel">
                                            <asp:Label ID="lblCapiasIssuedDate" Text="Issued Date" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtIssuedDate" runat="server" />
                                            <asp:RangeValidator ID="rvdtIssuedDate" runat="server" ControlToValidate="dtIssuedDate"
                                                ValidationGroup="SaveOrder" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trBondAmount" runat="server">
                                        <td class="fieldLabel">
                                            <asp:Label ID="Label3" Text="Bond Amount" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <telerik:RadNumericTextBox ID="RadNumericTextBoxBondAmount" runat="server" Width="126px">
                                            </telerik:RadNumericTextBox>
                                        </td>
                                    </tr>
                                    <tr id="trArrestDate" runat="server">
                                        <td class="fieldLabel">
                                            <asp:Label ID="lblArrestedDate" Text="Arrested Date" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtArrestCommitmentEffectiveDate" runat="server" />
                                            <%--<asp:CompareValidator ID="cvArrestCommitmentEffectiveDate" runat="server" ErrorMessage="Arrested date must be after order date."
                                                ControlToValidate="dtArrestCommitmentEffectiveDate" ControlToCompare="dtOrderedDate" ValidationGroup="SaveOrder"
                                                Operator="GreaterThanEqual">*</asp:CompareValidator>--%>
                                        </td>
                                    </tr>
                                    <tr id="trReleaseDate" runat="server">
                                        <td class="fieldLabel">
                                            <asp:Label ID="Label1" Text="Release Date" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtReleaseDate" runat="server" />
                                            <asp:CompareValidator ID="cvdtReleaseDate" runat="server" ErrorMessage="Released date must be after arrested date."
                                                ControlToValidate="dtReleaseDate" ControlToCompare="dtArrestCommitmentEffectiveDate"
                                                ValidationGroup="SaveOrder" Operator="GreaterThanEqual">*</asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr id="trDaysIncarcerated" runat="server">
                                        <td class="fieldLabel">
                                            <asp:Label ID="Label2" Text="Days Incarcerated" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDaysIncarcerated" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            <asp:Label ID="lblNote" Text="SpecialOrder Note" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSpecialOrderNote" runat="server" TextMode="MultiLine" Width="438"
                                                Height="40" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="padding-top: 10px; padding-bottom: 5px;">
                                            <asp:Button ID="btnSaveAndReturnToCase" runat="server" Visible="true" Text="Save &amp; Return to Case"
                                                ValidationGroup="SaveOrder" CssClass="submit" OnClick="btnSaveAndReturnToCase_Click"
                                                OnClientClick="return ValidateSave();" />
                                            <asp:Button ID="btnSaveSpecialOrder" runat="server" Text="Save Order" CssClass="submit"
                                                ValidationGroup="SaveOrder" OnClick="btnSave_Click" OnClientClick="return ValidateSave();" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
        </div>
    </div>
    <div class="bottomSection" id="divCaseNotes" runat="server" visible="false">
        <UC:CaseNotes ID="caseNotes" runat="server" />
    </div>
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 50px;" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
