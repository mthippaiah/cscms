﻿<%@ Page Title="CSCMS | Report" Language="C#" MasterPageFile="~/cscmsplain.master"
    AutoEventWireup="true" CodeFile="ReportView.aspx.cs" Inherits="MemberPages_ReportView" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
        <div class="pageTitle">
            <asp:Label ID="lblReportTitle" runat="server" /></div>
        <CSCMSUC:MessageBox ID="messageBox" runat="server" />
        <table cellpadding="0" cellspacing="0" width="957px" style="height: 1280px">
            <tr>
                <td valign="top">
                    <rsweb:ReportViewer ID="reportViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                        ProcessingMode="Remote" Width="100%" Height="1120px" BackColor="#FFFFDD" BorderColor="#DDDDDD"
                        BorderStyle="Solid" BorderWidth="1px" InternalBorderColor="221, 221, 221" ZoomPercent="91">
                    </rsweb:ReportViewer>
                </td>
            </tr>
        </table>
</asp:Content>
