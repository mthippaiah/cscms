﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ReportingServices;
using Microsoft.Reporting;
using Microsoft.Reporting.WebForms;
using System.Configuration;

public partial class MemberPages_ReportView : BasePage
{
    private const string IE8_MESSAGE = "Please note that there is a known issue with Internet Explorer 8 which will prevent the values in this report from displaying correctly in your web browser. If you are experiencing this issue, please export the report to Excel to view its content.";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            reportViewer.Reset();

            reportViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServer"]);

            reportViewer.ServerReport.ReportServerCredentials = new ReportServerCredentials(ConfigurationManager.AppSettings["AnonymousUsername"], ConfigurationManager.AppSettings["AnonymousUserPassword"], ConfigurationManager.AppSettings["AnonymousUserDomain"]);

            //if (Session["ScreenHeight"] != null) reportViewer.Style["Height"] = (System.Math.Max(768, int.Parse(Session["ScreenHeight"].ToString())) - 350).ToString() + "px";

            Func<short, int, int> addToReportUsage = delegate(short userId, int reportId)
            {
                CSCMS.Data.ReportUsage obj = new CSCMS.Data.ReportUsage();
                obj.Report = CscmsContext.Report.Where(it => it.ReportId == reportId).FirstOrDefault();
                obj.User = CscmsContext.User.Where(it => it.UserId == userId).FirstOrDefault();
                obj.CreateDate = DateTime.Now;
                CscmsContext.AddToReportUsage(obj);
                CscmsContext.SaveChanges();
                return 1;
            };

            if (!string.IsNullOrEmpty(Request.QueryString[MyConsts.QUERY_RID]))
            {
                if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETPUBLIC)
                {
                    lblReportTitle.Text = "Child Support Docket (Public)";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/ChildSupportDocket(Public)";
                    addToReportUsage(CurrentUser.UserID, 1);
                    //dc.ExecuteCommand(string.Format("INSERT INTO ReportUsage (UserId ,ReportId , CreateDate ) VALUES ({0},{1},'{2}')", CurrentUser.UserID.ToString(), 1, DateTime.Now.ToString()));
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET)
                {
                    lblReportTitle.Text = "Child Support Judge's Docket";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/ChildSupportJudgesDocket";
                    addToReportUsage(CurrentUser.UserID, 2);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.PENDINGCASESWITHAGES)
                {
                    lblReportTitle.Text = "Pending Cases With Age";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/PendingCasesWithAge";
                    addToReportUsage(CurrentUser.UserID, 3);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CASESLACKINGSERVICE)
                {
                    lblReportTitle.Text = "Pending Expedited Cases Lacking Service";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/CasesLackingService";
                    addToReportUsage(CurrentUser.UserID, 4);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.SUMMARYREPORTONEXPEDITEDPROCESSCOMPLIANCE)
                {
                    lblReportTitle.Text = "Summary Report On Expedited Process Compliance";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/SummaryReportOnExpeditedProcessCompliance";
                    addToReportUsage(CurrentUser.UserID, 5);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.GENERALCOURTSTATISTICSCHILDSUPPORT)
                {
                    lblReportTitle.Text = "General Court Statistics Child Support";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/GeneralCourtStatisticsChildSupport";
                    addToReportUsage(CurrentUser.UserID, 6);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET_2)
                {
                    lblReportTitle.Text = "Child Support Docket";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/ChildSupportDocket";
                    addToReportUsage(CurrentUser.UserID, 7);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETWITHPAGEBREAK)
                {
                    lblReportTitle.Text = "Child Support Docket with Page Break";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/ChildSupportDocketWithPageBreak";
                    addToReportUsage(CurrentUser.UserID, 8);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.PENDINGCASEAGEINDAYSANDPENDINGRATE)
                {
                    lblReportTitle.Text = "Pending Case Age In Days And Pending Rate";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/PendingCaseAgeInDaysAndPendingRate";
                    addToReportUsage(CurrentUser.UserID, 9);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.OUTSTANDING_CAPIAS)
                {
                    lblReportTitle.Text = "Outstanding Capias";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/OutstandingCapias";
                    addToReportUsage(CurrentUser.UserID, 10);
                }
                else if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.INCARCERATED_PARTIES)
                {
                    lblReportTitle.Text = "Incarcerated Parties";
                    reportViewer.ServerReport.ReportPath = "/CSCMS/IncarceratedParties";
                    addToReportUsage(CurrentUser.UserID, 11);
                }
            }

            // Default is CSCMS report folder, but user could specify another folder
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ReportPath"])) reportViewer.ServerReport.ReportPath = reportViewer.ServerReport.ReportPath.Replace("CSCMS", ConfigurationManager.AppSettings["ReportPath"]);

            if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.OUTSTANDING_CAPIAS)
            {
                ReportParameter user = new ReportParameter();
                user.Name = "User";
                user.Visible = false;
                user.Values.Clear();
                user.Values.Add(base.CurrentUser.FullName);
                reportViewer.ServerReport.SetParameters(
                   new ReportParameter[] { user });
            }

            if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETPUBLIC
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET_2
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETWITHPAGEBREAK
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CASESLACKINGSERVICE
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.PENDINGCASESWITHAGES
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.PENDINGCASEAGEINDAYSANDPENDINGRATE
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.OUTSTANDING_CAPIAS
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.INCARCERATED_PARTIES)
            {
                ReportParameter casePageURL = new ReportParameter();
                casePageURL.Name = "CasePageURL";
                casePageURL.Visible = false;
                casePageURL.Values.Clear();
                casePageURL.Values.Add(HttpContext.Current.Request.Url.GetBase() + (HttpContext.Current.Request.ApplicationPath == "/" ? "" : HttpContext.Current.Request.ApplicationPath) + "/MemberPages/ActionDetail.aspx?cn=");
                reportViewer.ServerReport.SetParameters(
                   new ReportParameter[] { casePageURL });
            }

            // OCACourtID
            if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETPUBLIC
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.PENDINGCASESWITHAGES
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CASESLACKINGSERVICE
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.SUMMARYREPORTONEXPEDITEDPROCESSCOMPLIANCE
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.GENERALCOURTSTATISTICSCHILDSUPPORT
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET_2
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETWITHPAGEBREAK
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.PENDINGCASEAGEINDAYSANDPENDINGRATE
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.OUTSTANDING_CAPIAS
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.INCARCERATED_PARTIES
                )
            {
                ReportParameter OCACourtID = new ReportParameter();
                OCACourtID.Name = "OCACourtID";
                OCACourtID.Visible = false;
                OCACourtID.Values.Clear();
                OCACourtID.Values.Add(this.CurrentUser.OCACourtID.ToString());
                reportViewer.ServerReport.SetParameters(
                   new ReportParameter[] { OCACourtID });
            }

            // Hearing Date
            if (Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETPUBLIC
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKET_2
                || Request.QueryString[MyConsts.QUERY_RID] == MyConsts.CHILDSUPPORTDOCKETWITHPAGEBREAK)
            {
                ReportParameter HearingDate = new ReportParameter();
                HearingDate.Name = "HearingDate";
                HearingDate.Visible = true;
                //if (Session["Calendar.SelectedDate"] != null)
                //{
                //    HearingDate.Values.Add(Session["Calendar.SelectedDate"].ToString());
                //}
                //else
                //{
                //    HearingDate.Values.Add(DateTime.Today.ToShortDateString());
                //}

                reportViewer.ServerReport.SetParameters(
                   new ReportParameter[] { HearingDate });
            }
            //ReportParameter UserID = new ReportParameter();
            //UserID.Name = "UserID";
            //UserID.Visible = false;
            //UserID.Values.Clear();
            //UserID.Values.Add(this.CurrentUser.UserName);
            //reportViewer.ServerReport.SetParameters(
            //   new ReportParameter[] { UserID });
        }
    }

    protected override void OnInit(EventArgs e)
    {
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.EnablePartialRendering = false;
        base.OnInit(e);
    }
}
