﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using Telerik.Web.UI.GridExcelBuilder;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AjaxControlToolkit;

public partial class MemberPages_CauseSearch : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rvDispositionRenderedDate.MaximumValue = DateTime.Now.ToShortDateString();
            this.rvDispositionSignedDate.MaximumValue = DateTime.Now.ToShortDateString();
            try
            {
                if (Session["SearchddlCounty"] != null) ddlCountyNew.SelectedIndex = int.Parse(Session["SearchddlCounty"].ToString());
            }
            catch { }

            if (!string.IsNullOrEmpty(Request.QueryString["message"]))
            {
                if (Request.QueryString["message"] == "NewCaseCreated")
                    messageBox.SuccessMessage = "New case was created successfully.";
            }

            if (!string.IsNullOrEmpty(Request.QueryString["cn"]))
            {
                causeSearch.ClearSearchParameters();
                causeSearch.CauseNumberText = Request.QueryString["cn"].ToString();
                causeSearch.btnSearchByCase_Click(sender, e);
            }

            if (!string.IsNullOrEmpty(Request.QueryString["oag"]))
            {
                causeSearch.ClearSearchParameters();
                causeSearch.OAGCauseNumberText = Request.QueryString["oag"].ToString();
                causeSearch.btnSearchByCase_Click(sender, e);
            }

            sectionCase.Visible = !string.IsNullOrEmpty(Request.QueryString["nc"]) ? true : false;
            divCaseSearch.Visible = !sectionCase.Visible;
            Page.Title = sectionCase.Visible ? "CSCMS | Add New Case" : "CSCMS | Case Search";
            lblPageLabel.Text = sectionCase.Visible ? "ADD NEW CASE" : "CASE SEARCH";
            dtFilingDate.SelectedDate = System.DateTime.Today;
            dtFilingDate.MaxDate = System.DateTime.Today;
            TimeSpan ts = CscmsContext.OCACourt.Where(it => it.OCACourtID == this.CurrentUser.OCACourtID).Select(it => it.DefaultHearingTime).FirstOrDefault();
            hearingTime.SelectedDate = DateTime.Today.Add(ts);

            hearingDateTooltip.TargetControlID = dtHearingDate.DateInput.ClientID;

            if (!string.IsNullOrEmpty(Request.QueryString["nc"]))
            {
                if (!string.IsNullOrEmpty(Request.QueryString["cid"]))
                {
                    LoadValuesFromTransferedCaseData();
                }
                this.ddlCountyNew.DataBind();
                this.dtChild1BirthDate.DataBind();
                this.dtChild2BirthDate.DataBind();
                this.dtChild3BirthDate.DataBind();
                this.dtChild4BirthDate.DataBind();
                this.dtP1BirthDate.DataBind();
                this.dtP2BirthDate.DataBind();
                this.dtCPBirthDate.DataBind();
                this.dtNCPBirthDate.DataBind();
                this.ddlCountyNew.SelectedValue = NewCaseCountyID.ToString();
                ddlDisposition.DataBind();
                List<CSCMS.Data.OriginatingCourt> list = GetRefferingCourt();
                list.Insert(0, new CSCMS.Data.OriginatingCourt { OriginatingCourtID = 0, CourtName = "- Select -" });
                ddlReferringCourt.DataSource = list;
                ddlReferringCourt.DataBind();
                if (ddlReferringCourt.Items.Count > 1)
                    ddlReferringCourt.SelectedIndex = 1;

            }
        }
        else
        {
            LoadValuesFromDbForSearchedPerson();
        }
        this.ddlActionTypeNew.Focus();
        grdCases.EnableAjaxSkinRendering = true;
    }

    private void LoadValuesFromDbForSearchedPerson()
    {
        bool iOpenedConn = false;

        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            //If it is a postback, we need to disable the controls that were populated for a person search
            //This seems like an unnecessary excercise, but we need it. There are some issues associated with 
            //diabling the controls on the clientside using javascript. The disabled controls values is not posted back
            //Also, you see weird behavior with the enabling disabling and also postbacks

            if (!string.IsNullOrEmpty(this.HFNCPPersonGUID.Value) && this.HFNCPPersonGUID.Value != "Current")
            {
                Guid id = new Guid(this.HFNCPPersonGUID.Value);
                CSCMS.Data.Person p = CscmsContext.Person.Where(it => it.PersonGUID == id).FirstOrDefault();
                SetCommonPersonControlData(txtNCPFirst, txtNCPLast, txtNCPMiddle, txtNCPSuffix, ddlNCPFatherMother, ddlNCPSex, dtNCPBirthDate, null, p.Sex == "M" ? "Father" : "Mother", p);
                EnablePersonControls(txtNCPFirst, txtNCPLast, txtNCPMiddle, txtNCPSuffix, ddlNCPFatherMother, ddlNCPSex, dtNCPBirthDate, false);
            }

            if (!string.IsNullOrEmpty(this.HFCPPersonGUID.Value) && this.HFCPPersonGUID.Value != "Current")
            {
                PopulatePersonSearchValues(txtCPFirst, txtCPLast, txtCPMiddle, txtCPSuffix, null, ddlCPSex, dtCPBirthDate, HFCPPersonGUID);
            }

            if (!string.IsNullOrEmpty(this.HFChild1PersonGUID.Value) && this.HFChild1PersonGUID.Value != "Current")
            {
                PopulatePersonSearchValues(txtChild1First, txtChild1Last, txtChild1Middle, txtChild1Suffix, null, ddlChild1Sex, dtChild1BirthDate, HFChild1PersonGUID);
            }

            if (!string.IsNullOrEmpty(this.HFChild2PersonGUID.Value) && this.HFChild2PersonGUID.Value != "Current")
            {
                PopulatePersonSearchValues(txtChild2First, txtChild2Last, txtChild2Middle, txtChild2Suffix, null, ddlChild2Sex, dtChild2BirthDate, HFChild2PersonGUID);
            }

            if (!string.IsNullOrEmpty(this.HFChild3PersonGUID.Value) && this.HFChild3PersonGUID.Value != "Current")
            {
                PopulatePersonSearchValues(txtChild3First, txtChild3Last, txtChild3Middle, txtChild3Suffix, null, ddlChild3Sex, dtChild3BirthDate, HFChild3PersonGUID);
            }

            if (!string.IsNullOrEmpty(this.HFChild4PersonGUID.Value) && this.HFChild4PersonGUID.Value != "Current")
            {
                PopulatePersonSearchValues(txtChild4First, txtChild4Last, txtChild4Middle, txtChild4Suffix, null, ddlChild4Sex, dtChild4BirthDate, HFChild4PersonGUID);
            }

            if (!string.IsNullOrEmpty(this.hfP1.Value) && this.hfP1.Value != "Current")
            {
                PopulatePersonSearchValues(txtP1First, txtP1Last, txtP1Middle, txtP1Suffix, null, ddlP1Sex, dtP1BirthDate, hfP1);
            }

            if (!string.IsNullOrEmpty(this.hfP2.Value) && this.hfP2.Value != "Current")
            {
                PopulatePersonSearchValues(txtP2First, txtP2Last, txtP2Middle, txtP2Suffix, null, ddlP2Sex, dtP2BirthDate, hfP2);
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    private void PopulatePersonSearchValues(TextBox first, TextBox last, TextBox middle, TextBox suffix, DropDownList ddlFM, DropDownList ddlSex, RadDatePicker dtBirthDate, HiddenField hfPersonGuid)
    {
        Guid id = new Guid(hfPersonGuid.Value);
        CSCMS.Data.Person p = CscmsContext.Person.Where(it => it.PersonGUID == id).FirstOrDefault();
        SetCommonPersonControlData(first, last, middle, suffix, ddlFM, ddlP2Sex, dtP2BirthDate, null, null, p);
        EnablePersonControls(first, last, middle, suffix, ddlFM, ddlP2Sex, dtP2BirthDate, false);
    }

    private void LoadValuesFromTransferedCaseData()
    {
        bool iOpenedConn = false;
        Guid actionGuid = ((BasePage)this.Page).CurrentAction.ActionGUID;
        List<PersonData> listpd = new List<PersonData>();
        CSCMS.Data.Action a = null;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));
            a = CscmsContext.Action.Include("Cause").Where(it => it.ActionGUID == actionGuid).FirstOrDefault();

            listpd = (from ap in CscmsContext.ActionPerson.Include("PersonType").Include("CustodialType")
                      join p in CscmsContext.Person on ap.Person.PersonGUID equals p.PersonGUID
                      where ap.Action.ActionGUID == actionGuid
                      select new PersonData
                      {
                          actionPerson = ap,
                          person = p,
                          custodialType = ap.CustodialType,
                          personType = ap.PersonType
                      }).ToList();
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }

        List<PersonData> ncpList = listpd.Where(it => it.custodialType != null && it.custodialType.CustodialTypeID == 1).ToList();
        List<PersonData> cpList = listpd.Where(it => it.custodialType != null && it.custodialType.CustodialTypeID == 0).ToList();
        List<PersonData> childList = listpd.Where(it => it.personType != null && it.personType.PersonTypeName == "Child").ToList();
        List<PersonData> other = listpd.Where(it => it.custodialType == null && it.personType != null && it.personType.PersonTypeName != "Child").ToList();

        CopyNcpFromTransferedCaseToControls(ncpList, actionGuid);
        CopyCPFromTransferedCaseToControls(cpList, actionGuid);
        CopyChildFromTransferedCaseToControls(childList, actionGuid);
        CopyOtherFromTransferedCaseToControls(other, actionGuid);

        if (a != null)
        {
            this.txtOAG1.Text = a.OAGCauseNumber;
            this.txtOAG2.Text = a.OAGCauseNumber2;
            this.txtOAG3.Text = a.OAGCauseNumber3;
            this.txtNewCaseStyle.Text = a.Style;
        }
        ((BasePage)this.Page).CurrentCause = null;
        ((BasePage)this.Page).CurrentAction = null;
        ((BasePage)this.Page).CurrentPerson = null;
        ((BasePage)this.Page).CurrentActionPerson = null;
    }

    private void CopyOtherFromTransferedCaseToControls(List<PersonData> other, Guid actionGuid)
    {
        if (other.Count > 0)
        {
            int i = 0;
            foreach (var item in other)
            {
                if (string.IsNullOrEmpty(this.hfP1.Value))
                {
                    Helper1(item.person, item.actionPerson, item.personType, item.custodialType != null ? "0" : "", actionGuid);
                }
                else if (string.IsNullOrEmpty(this.hfP2.Value))
                {
                    Helper2(item.person, item.actionPerson, item.personType, item.custodialType != null ? "0" : "", actionGuid);
                }
                i++;
            }
        }
    }

    private void CopyNcpFromTransferedCaseToControls(List<PersonData> ncpList, Guid actionGuid)
    {
        int i = 0;
        foreach (var ncp in ncpList)
        {
            if (i == 0)
            {
                SetCommonPersonControlData(txtNCPFirst, txtNCPLast, txtNCPMiddle, txtNCPSuffix, ddlNCPFatherMother, ddlNCPSex, dtNCPBirthDate, HFNCPPersonGUID, ncp.personType != null ? ncp.personType.PersonTypeName : "", ncp.person);
                SetCommonCpNcpControlData(ddlNCPNPFlag, this.ddlNCPServiceType, dtNCPSvcDate, ncp.actionPerson.NPFlag ? "NP" : "None", actionGuid, ncp.person);
            }
            else
            {
                if (string.IsNullOrEmpty(hfP1.Value))
                    Helper1(ncp.person, ncp.actionPerson, ncp.personType, "1", actionGuid);
                else if (string.IsNullOrEmpty(hfP2.Value))
                    Helper2(ncp.person, ncp.actionPerson, ncp.personType, "1", actionGuid);
            }
            i++;
        }
    }

    private void Helper1(CSCMS.Data.Person p, CSCMS.Data.ActionPerson ap, CSCMS.Data.PersonType pt, string custodialType, Guid actionGuid)
    {
        SetCommonPersonControlData(txtP1First, txtP1Last, txtP1Middle, txtP1Suffix, ddlP1FatherMother, ddlP1Sex, dtP1BirthDate, hfP1, pt != null ? pt.PersonTypeName : "", p);
        SetCommonCpNcpControlData(this.ddlP1NPFlag, this.ddlP1SvcType, this.dtP1Svcdate, ap.NPFlag ? "NP" : "None", actionGuid, p);
        this.ddlP1CustodialType.SelectedValue = custodialType;
    }

    private void Helper2(CSCMS.Data.Person p, CSCMS.Data.ActionPerson ap, CSCMS.Data.PersonType pt, string custodialType, Guid actionGuid)
    {
        SetCommonPersonControlData(txtP2First, txtP2Last, txtP2Middle, txtP2Suffix, ddlP2FatherMother, ddlP2Sex, dtP2BirthDate, hfP2, pt != null ? pt.PersonTypeName : "", p);
        SetCommonCpNcpControlData(this.ddlP2NPFlag, this.ddlP2SvcType, this.dtP2Svcdate, ap.NPFlag ? "NP" : "None", actionGuid, p);
        this.ddlP2CustodialType.SelectedValue = custodialType;
    }

    private void CopyChildFromTransferedCaseToControls(List<PersonData> childList, Guid actionGuid)
    {
        int i = 0;
        foreach (var item in childList)
        {
            if (i == 0)
            {
                SetCommonPersonControlData(txtChild1First, txtChild1Last, txtChild1Middle, txtChild1Suffix, ddlChild1FatherMother, ddlChild1Sex, dtChild1BirthDate, HFChild1PersonGUID, "Child", item.person);
            }
            if (i == 1)
            {
                SetCommonPersonControlData(txtChild2First, txtChild2Last, txtChild2Middle, txtChild2Suffix, ddlChild2FatherMother, ddlChild2Sex, dtChild2BirthDate, HFChild2PersonGUID, "Child", item.person);
            }
            if (i == 2)
            {
                SetCommonPersonControlData(txtChild3First, txtChild3Last, txtChild3Middle, txtChild3Suffix, ddlChild3FatherMother, ddlChild3Sex, dtChild3BirthDate, HFChild3PersonGUID, "Child", item.person);
            }
            if (i == 3)
            {
                SetCommonPersonControlData(txtChild4First, txtChild4Last, txtChild4Middle, txtChild4Suffix, ddlChild4FatherMother, ddlChild4Sex, dtChild4BirthDate, HFChild4PersonGUID, "Child", item.person);
            }
            if (i == 4 || i == 5)
            {
                if (string.IsNullOrEmpty(hfP1.Value))
                    SetCommonPersonControlData(txtP1First, txtP1Last, txtP1Middle, txtP1Suffix, ddlP1FatherMother, ddlP1Sex, dtP1BirthDate, hfP1, "Child", item.person);
                else if (string.IsNullOrEmpty(hfP2.Value))
                    SetCommonPersonControlData(txtP2First, txtP2Last, txtP2Middle, txtP2Suffix, ddlP2FatherMother, ddlP2Sex, dtP2BirthDate, hfP2, "Child", item.person);
            }
            i++;
        }
    }

    private void CopyCPFromTransferedCaseToControls(List<PersonData> cpQuery, Guid actionGuid)
    {
        int i = 0;
        foreach (var cp in cpQuery)
        {
            if (i == 0)
            {
                SetCommonPersonControlData(txtCPFirst, txtCPLast, txtCPMiddle, txtCPSuffix, ddlCPFatherMother, ddlCPSex, dtCPBirthDate, HFCPPersonGUID, cp.personType != null ? cp.personType.PersonTypeName : "", cp.person);
                SetCommonCpNcpControlData(ddlCPNPFlag, this.ddlCPServiceType, dtCPSvcdate, cp.actionPerson.NPFlag ? "NP" : "None", actionGuid, cp.person);
            }
            else
            {
                if (string.IsNullOrEmpty(hfP1.Value))
                    Helper1(cp.person, cp.actionPerson, cp.personType, "0", actionGuid);
                else if (string.IsNullOrEmpty(hfP2.Value))
                    Helper2(cp.person, cp.actionPerson, cp.personType, "0", actionGuid);
            }
            i++;
        }
    }

    private void SetCommonCpNcpControlData(DropDownList ddlNcpNpFlag, DropDownList ddlSvcType, RadDatePicker svcDate, string npFlag, Guid actionGuid, CSCMS.Data.Person p)
    {
        ddlNcpNpFlag.Text = npFlag;
        CSCMS.Data.Service service = CscmsContext.Service.Include("ServiceType").Where(it => it.ActionPerson.Action.ActionGUID == actionGuid && it.ActionPerson.Person.PersonGUID == p.PersonGUID).FirstOrDefault();
        if (service != null && service.ServiceDate != null)
        {
            svcDate.SelectedDate = service.ServiceDate;
            ddlSvcType.SelectedValue = service.ServiceType.ServiceTypeID.ToString();
        }
    }

    private void SetCommonPersonControlData(TextBox first, TextBox last, TextBox middle, TextBox suffix, DropDownList ddlFM, DropDownList ddlSex, RadDatePicker dtBirthDate, HiddenField hfPersonGuid, string ddlFMTExt, CSCMS.Data.Person p)
    {
        first.Text = p.FirstName;
        last.Text = p.LastName;
        middle.Text = p.MiddleName;
        suffix.Text = p.NameSuffix;


        if (ddlFM != null && !string.IsNullOrEmpty(ddlFMTExt))
        {
            foreach (ListItem li in ddlFM.Items)
            {
                if (li.Text == ddlFMTExt)
                {
                    ddlFM.SelectedValue = li.Value;
                    break;
                }
            }
        }

        if (p.Sex != null)
            ddlSex.SelectedIndex = (p.Sex == "M") ? 0 : 1;

        if (p.Birthdate != null)
            dtBirthDate.SelectedDate = p.Birthdate;

        if (hfPersonGuid != null)
            hfPersonGuid.Value = p.PersonGUID.ToString();
    }

    private void EnablePersonControls(TextBox first, TextBox last, TextBox middle, TextBox suffix, DropDownList ddlFM, DropDownList ddlSex, RadDatePicker dtBirthDate, bool enable)
    {
        first.Enabled = enable;
        last.Enabled = enable;
        middle.Enabled = enable;
        suffix.Enabled = enable;
        ddlSex.Enabled = enable;
        if (ddlFM != null)
            ddlFM.Enabled = enable;
        dtBirthDate.Enabled = enable;
    }
    #region [Buttons]
    protected void HandleCauseSearchResult(DataSet ds, bool isSearchByCase)
    {
        this.grdCases.DataSource = ds;
        this.grdCases.DataBind();
        this.grdCases.Visible = true;
        this.btnExportCase.Visible = true;
        if (grdCases.Items.Count == 1)
        {
            string caseGUID = grdCases.MasterTableView.DataKeyValues[0]["CauseGUID"].ToString();
            string NumberOfAction = grdCases.MasterTableView.DataKeyValues[0]["NumberOfAction"].ToString();

            grdCases.DataSource = null;
            grdCases.DataBind();

            Response.Redirect("ActionDetail.aspx?cid=" + caseGUID + "&a=" + NumberOfAction, true);
        }
    }

    protected void dtHearingDate_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
    {
        if (dtHearingDate.SelectedDate.HasValue && dtHearingDate.SelectedDate.Value.Date < DateTime.Now.Date)
        {
            //hearingDateTooltip.Text = "The Hearing Date selected is a date in the past.";
            //hearingDateTooltip.VisibleOnPageLoad = true;
            //hearingDateTooltip.Visible = true;
        }
        else
        {
            hearingDateTooltip.VisibleOnPageLoad = false;
            hearingDateTooltip.Visible = false;
        }

        GetHearingbyCountyCount();

        this.hearingTime.Focus();
    }

    private void GetHearingbyCountyCount()
    {
        if (dtHearingDate.SelectedDate.HasValue)
        {
            Dictionary<DateTime, string> hsummary = Helper.GetDateHearingSummary(this.CscmsContext, dtHearingDate.SelectedDate.Value, dtHearingDate.SelectedDate.Value, this.CurrentUser.OCACourtID);

            if (hsummary.Count > 0)
            {
                lblgrdHearings.Text = "Hearings on Same Date: " + hsummary[dtHearingDate.SelectedDate.Value];

                int countyCnt = lblgrdHearings.Text.Count(f => f.Equals('('));

                lblgrdHearings.ForeColor = (countyCnt > 1) ? System.Drawing.Color.Red : System.Drawing.Color.Blue;
            }
        }
    }

    protected void btnExportCase_Click(object sender, EventArgs e)
    {
        causeSearch.DoSearch();
        if (grdCases.Items.Count > 0)
        {
            this.grdCases.ExportSettings.ExportOnlyData = true;
            grdCases.ExportSettings.IgnorePaging = true;
            grdCases.ExportSettings.OpenInNewWindow = true;
            grdCases.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
            grdCases.MasterTableView.ExportToExcel();
        }
    }

    protected void grdCases_ExcelMLExportStylesCreated(object source, GridExportExcelMLStyleCreatedArgs e)
    {
        foreach (StyleElement style in e.Styles)
        {
            if (style.Id == "headerStyle")
            {
                style.FontStyle.Bold = true;
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "itemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "alternatingItemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
        }
    }


    #endregion

    #region [DataSet]
    protected void dsCounty_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@UserId"].Value = this.CurrentUser.UserID;
        e.Command.Parameters["@CourtID"].Value = this.CurrentUser.OCACourtID;
    }

    private List<CSCMS.Data.OriginatingCourt> GetRefferingCourt()
    {
        short v1 = short.Parse(ddlCountyNew.SelectedValue);
        return (from oc in CscmsContext.OriginatingCourt
                where oc.OCACourt.OCACourtID == this.CurrentUser.OCACourtID
                    && oc.County.CountyID == v1
                    && (oc.ExpirationDate == null || oc.ExpirationDate > DateTime.Today)
                select oc).OrderBy(it => it.SortOrder).ToList();
    }

    protected void ddlCountyNew_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlReferringCourt.Items.Clear();
        List<CSCMS.Data.OriginatingCourt> list = GetRefferingCourt();
        list.Insert(0, new CSCMS.Data.OriginatingCourt { OriginatingCourtID = 0, CourtName = "- Select -" });
        ddlReferringCourt.DataSource = list;
        ddlReferringCourt.DataBind();
        if (ddlReferringCourt.Items.Count > 1)
            ddlReferringCourt.SelectedIndex = 1;
        ddlCountyNew.Focus();
    }

    #endregion

    #region [Create New Case]

    private void SetNewPerson(CSCMS.Data.Person p, string ln, string fn, string ml, string suffix, DateTime? bdate, string sex)
    {
        p.PersonGUID = Guid.NewGuid();
        p.LastName = ln;
        p.FirstName = fn;
        p.MiddleName = ml;
        p.NameSuffix = suffix;
        p.Birthdate = bdate;
        p.Sex = sex;
        p.CreateDate = System.DateTime.Now;
        p.LastUpdateDate = System.DateTime.Now;
    }

    private void SetNewActionPerson(CSCMS.Data.ActionPerson ap, CSCMS.Data.Action action, CSCMS.Data.Person p, short personTypeId, short? custodialTypeId, bool npFlag)
    {
        ap.Action = action;
        ap.Person = p;
        ap.PersonType = CscmsContext.PersonType.Where(it => it.PersonTypeID == personTypeId).FirstOrDefault();
        if (custodialTypeId != null)
            ap.CustodialType = CscmsContext.CustodialType.Where(it => it.CustodialTypeID == custodialTypeId).FirstOrDefault();
        else
            ap.CustodialType = null;

        ap.LastUpdateDate = System.DateTime.Now;
        ap.CreateDate = System.DateTime.Now;
        ap.NPFlag = npFlag;
    }

    protected void btnSaveAndAddAnother_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && VaidateNewCaseData())
        {
            CSCMS.Data.Cause newCause1 = CreateCase();
            Response.Redirect("~/MemberPages/NewCase.aspx?nc=yes&message=NewCaseCreated", true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && VaidateNewCaseData())
        {
            CSCMS.Data.Cause newCause1 = CreateCase();
            Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + newCause1.CauseGUID + "&a=1&message=NewCaseCreated" + "&hd=" + (this.dtHearingDate.SelectedDate.HasValue ? dtHearingDate.SelectedDate.Value.ToShortDateString() : "no"), true);
        }
    }

    private bool VaidateNewCaseData()
    {
        short countyId = short.Parse(this.ddlCountyNew.SelectedValue);
        CSCMS.Data.Cause cause1 = CscmsContext.Cause.Where(it => it.CauseNumber == txtNewCaseNumber.Text && it.OCACourt.OCACourtID == this.CurrentUser.OCACourtID && it.County.CountyID == countyId).FirstOrDefault();
        if (cause1 != null)
        {
            messageBox.ErrorMessage = string.Format("Cause Number already exists for selected Court and County.");
            return false;
        }

        // add first Hearing
        if (!this.dtHearingDate.SelectedDate.HasValue && this.txtHearingNote.Text != string.Empty)
        {
            messageBox.ErrorMessage = string.Format("You must enter a hearing date if you entered hearing note.");
            return false;
        }

        if (this.txtNCPFirst.Text != "" && this.txtNCPLast.Text == "")
        {
            messageBox.ErrorMessage = string.Format("Last name is required for NCP.");
            return false;
        }

        if (this.txtCPFirst.Text != "" && this.txtCPLast.Text == "")
        {
            messageBox.ErrorMessage = string.Format("Last name is required for CP.");
            return false;
        }

        if (this.txtChild1First.Text != "" && this.txtChild1Last.Text == "")
        {
            messageBox.ErrorMessage = string.Format("Last name is required for Child1.");
            return false;
        }

        if (this.txtChild2First.Text != "" && this.txtChild2Last.Text == "")
        {
            messageBox.ErrorMessage = string.Format("Last name is required for Child2.");
            return false;
        }

        if (this.ddlP1FatherMother.SelectedIndex == 0 && (this.txtP1Last.Text != "" || this.txtP1First.Text != "" || this.txtP1Middle.Text != ""))
        {
            messageBox.ErrorMessage = string.Format("Please select Person Type for " + this.txtP1First.Text + " " + this.txtP1Middle.Text + " " + this.txtP1Last.Text);
            return false;
        }

        if (this.ddlP2FatherMother.SelectedIndex == 0 && (this.txtP2Last.Text != "" || this.txtP2First.Text != "" || this.txtP2Middle.Text != ""))
        {
            messageBox.ErrorMessage = string.Format("Please select Person Type for " + this.txtP2First.Text + " " + this.txtP2Middle.Text + " " + this.txtP2Last.Text);
            return false;
        }

        if (this.txtP1First.Text != "" && this.txtP1Last.Text == "")
        {
            messageBox.ErrorMessage = string.Format("Last name is required for " + this.txtP1First.Text + " " + this.txtP1Middle.Text + " " + this.txtP1Last.Text);
            return false;
        }

        if (this.txtP2First.Text != "" && this.txtP2Last.Text == "")
        {
            messageBox.ErrorMessage = string.Format("Last name is required for " + this.txtP2First.Text + " " + this.txtP2Middle.Text + " " + this.txtP2Last.Text);
            return false;
        }
        return true;
    }

    private CSCMS.Data.ActionPerson AddPerson(CSCMS.Data.CscmsEntities CscmsContext, CSCMS.Data.Action newAction1, TextBox first, TextBox last, TextBox middle, TextBox suffix, DropDownList ddlFM, DropDownList ddlSex, DropDownList ddlNpFlg, RadDatePicker dtBirthDate, HiddenField hf, short? custodilTypeId)
    {
        CSCMS.Data.Person newPerson = null;
        if (!string.IsNullOrEmpty(hf.Value) && hf.Value != "Current")
        {
            Guid id = new Guid(hf.Value);
            newPerson = CscmsContext.Person.Where(it => it.PersonGUID == id).FirstOrDefault();
        }
        else
        {
            newPerson = new CSCMS.Data.Person();
            SetNewPerson(newPerson, last.Text, first.Text, middle.Text, suffix.Text, dtBirthDate.SelectedDate, char.Parse(ddlSex.SelectedValue).ToString());
            CscmsContext.AddToPerson(newPerson);
        }

        CSCMS.Data.ActionPerson newActionPerson = new CSCMS.Data.ActionPerson();
        SetNewActionPerson(newActionPerson, newAction1, newPerson, short.Parse(ddlFM.SelectedValue), custodilTypeId, ddlNpFlg.SelectedValue == "1" ? true : false);
        CscmsContext.AddToActionPerson(newActionPerson);

        return newActionPerson;
    }

    private void AddService(CSCMS.Data.CscmsEntities CscmsContext, RadDatePicker dtSvcDate, CSCMS.Data.ActionPerson ap, DropDownList ddlSvcType)
    {
        if (dtSvcDate.SelectedDate != null)
        {
            CSCMS.Data.Service service = new CSCMS.Data.Service();
            service.ServiceDate = dtSvcDate.SelectedDate.Value;
            short? svcTypeId = !string.IsNullOrEmpty(ddlSvcType.SelectedValue) ? short.Parse(ddlSvcType.SelectedValue) : (short?)null;
            if (svcTypeId != null)
                service.ServiceType = CscmsContext.ServiceType.Where(it => it.ServiceTypeID == svcTypeId).FirstOrDefault();
            service.ActionPerson = ap;
            service.CreateDate = DateTime.Now;
            service.LastUpdateDate = DateTime.Now;
            CscmsContext.AddToService(service);
        }
    }

    private CSCMS.Data.Cause CreateCase()
    {
        bool iOpenedCon = false;

        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedCon = true;
            }

            short countyId = short.Parse(this.ddlCountyNew.SelectedValue);
            CSCMS.Data.Cause newCause1 = new CSCMS.Data.Cause();
            CSCMS.Data.Action newAction1 = new CSCMS.Data.Action();

            newCause1.CauseGUID = Guid.NewGuid();
            newCause1.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == this.CurrentUser.OCACourtID).FirstOrDefault();
            newCause1.County = CscmsContext.County.Where(it => it.CountyID == countyId).FirstOrDefault();
            short? orc = !string.IsNullOrEmpty(ddlReferringCourt.SelectedValue) ? short.Parse(ddlReferringCourt.SelectedValue) : (short?)null;
            if (orc != null)
                newCause1.OriginatingCourt = CscmsContext.OriginatingCourt.Where(it => it.OriginatingCourtID == orc).FirstOrDefault();
            else
                newCause1.OriginatingCourt = null;

            newCause1.CauseNumber = txtNewCaseNumber.Text;
            newCause1.NumberOfAction = 1;
            newCause1.CreateDate = System.DateTime.Now;
            newCause1.LastUpdateDate = System.DateTime.Now;
            newCause1.LastAccessDate = System.DateTime.Now;
            newCause1.LastActionFiledDate = dtFilingDate.SelectedDate;
            newCause1.CauseNumberClean = txtNewCaseNumber.Text.Replace("-", "").Replace(",", "").Replace(" ", "").Replace("(", "").Replace(")", "").Replace("/", "").Replace(@"\", "").Replace(";", "").Replace("%", "").Replace(@"&", "");
            newCause1.OCAUnit = txtOAGOffice.Text;

            // Cause.OAGCauseNumber gets updated when we call UpdateActionStatus SP at the end
            //string oagCauseNumber = "";

            //if (!string.IsNullOrEmpty(this.txtOAG1.Text))
            //    oagCauseNumber += (this.txtOAG1.Text + ",");

            //if (!string.IsNullOrEmpty(this.txtOAG2.Text))
            //    oagCauseNumber += (this.txtOAG2.Text + ",");

            //if (!string.IsNullOrEmpty(this.txtOAG3.Text))
            //    oagCauseNumber += this.txtOAG3.Text;

            //newCause1.OAGCauseNumber = oagCauseNumber;

            CscmsContext.AddToCause(newCause1);

            newAction1.ActionGUID = Guid.NewGuid();
            newAction1.Cause = newCause1;
            short actionTypeId = short.Parse(this.ddlActionTypeNew.SelectedValue);
            newAction1.ActionType = CscmsContext.ActionType.Where(it => it.ActionTypeID == actionTypeId).FirstOrDefault();
            newAction1.OAGCauseNumber = txtOAG1.Text;
            newAction1.OAGCauseNumber2 = txtOAG2.Text;
            newAction1.OAGCauseNumber3 = txtOAG3.Text;
            newAction1.Style = txtNewCaseStyle.Text;
            newAction1.ActionFiledDate = dtFilingDate.SelectedDate;
            newAction1.ActionNumber = 1;
            newAction1.CreateDate = System.DateTime.Now;
            newAction1.LastUpdateDate = System.DateTime.Now;

            CscmsContext.AddToAction(newAction1);

            if (!string.IsNullOrEmpty(txtCPLast.Text))
            {
                CSCMS.Data.ActionPerson newActionPerson1 = AddPerson(CscmsContext, newAction1, txtCPFirst, txtCPLast, txtCPMiddle, txtCPSuffix, ddlCPFatherMother, ddlCPSex, ddlCPNPFlag, dtCPBirthDate, HFCPPersonGUID, 0);
                AddService(CscmsContext, dtCPSvcdate, newActionPerson1, ddlCPServiceType);
            }

            if (!string.IsNullOrEmpty(txtNCPLast.Text))
            {
                CSCMS.Data.ActionPerson newActionPerson2 = AddPerson(CscmsContext, newAction1, txtNCPFirst, txtNCPLast, txtNCPMiddle, txtNCPSuffix, ddlNCPFatherMother, ddlNCPSex, ddlNCPNPFlag, dtNCPBirthDate, HFNCPPersonGUID, 1);
                AddService(CscmsContext, dtNCPSvcDate, newActionPerson2, ddlNCPServiceType);
            }

            if (!string.IsNullOrEmpty(txtChild1Last.Text))
            {
                AddPerson(CscmsContext, newAction1, txtChild1First, txtChild1Last, txtChild1Middle, txtChild1Suffix, ddlChild1FatherMother, ddlChild1Sex, ddlChild1NPFlag, dtChild1BirthDate, HFChild1PersonGUID, null);
            }

            if (!string.IsNullOrEmpty(txtChild2Last.Text))
            {
                AddPerson(CscmsContext, newAction1, txtChild2First, txtChild2Last, txtChild2Middle, txtChild2Suffix, ddlChild2FatherMother, ddlChild2Sex, ddlChild2NPFlag, dtChild2BirthDate, HFChild2PersonGUID, null);
            }

            if (!string.IsNullOrEmpty(txtChild3Last.Text))
            {
                AddPerson(CscmsContext, newAction1, txtChild3First, txtChild3Last, txtChild3Middle, txtChild3Suffix, ddlChild3FatherMother, ddlChild3Sex, ddlChild3NPFlag, dtChild3BirthDate, HFChild3PersonGUID, null);
            }

            if (!string.IsNullOrEmpty(txtChild4Last.Text))
            {
                AddPerson(CscmsContext, newAction1, txtChild4First, txtChild4Last, txtChild4Middle, txtChild4Suffix, ddlChild4FatherMother, ddlChild4Sex, ddlChild4NPFlag, dtChild4BirthDate, HFChild4PersonGUID, null);
            }

            if (!string.IsNullOrEmpty(txtP1Last.Text))
            {
                short? custodialTypeId = null;
                if (this.ddlP1CustodialType.SelectedIndex > 0)
                    custodialTypeId = short.Parse(this.ddlP1CustodialType.SelectedValue);
                CSCMS.Data.ActionPerson newActionPerson7 = AddPerson(CscmsContext, newAction1, txtP1First, txtP1Last, txtP1Middle, txtP1Suffix, ddlP1FatherMother, ddlP1Sex, ddlP1NPFlag, dtP1BirthDate, hfP1, custodialTypeId);
                AddService(CscmsContext, dtP1Svcdate, newActionPerson7, ddlP1SvcType);
            }

            if (!string.IsNullOrEmpty(txtP2Last.Text))
            {
                short? custodialTypeId = null;
                if (this.ddlP2CustodialType.SelectedIndex > 0)
                    custodialTypeId = short.Parse(this.ddlP2CustodialType.SelectedValue);

                CSCMS.Data.ActionPerson newActionPerson8 = AddPerson(CscmsContext, newAction1, txtP2First, txtP2Last, txtP2Middle, txtP2Suffix, ddlP2FatherMother, ddlP2Sex, ddlP2NPFlag, dtP2BirthDate, hfP2, custodialTypeId);
                AddService(CscmsContext, dtP2Svcdate, newActionPerson8, ddlP2SvcType);
            }

            // add first Hearing
            if (this.dtHearingDate.SelectedDate.HasValue)
            {
                CSCMS.Data.Hearing newHearing1 = new CSCMS.Data.Hearing();
                newHearing1.Action = newAction1;
                newHearing1.HearingNote = this.txtHearingNote.Text;
                newHearing1.LastUpdateDate = System.DateTime.Now;
                newHearing1.CreateDate = System.DateTime.Now;
                newHearing1.HearingDate = dtHearingDate.SelectedDate.Value;
                newHearing1.HearingTime = hearingTime.SelectedDate.HasValue ? hearingTime.SelectedDate.Value.TimeOfDay : (TimeSpan?)null;

                if (hearingTime.SelectedDate.HasValue)
                {
                    newHearing1.MorningDocket = (hearingTime.SelectedDate.Value.TimeOfDay.Hours <= 12) ? true : false;
                }
                else
                {
                    newHearing1.HearingTime = System.DateTime.Parse("09:00:00").TimeOfDay;
                    newHearing1.MorningDocket = true;
                }
                CscmsContext.AddToHearing(newHearing1);
            }
            short? dispTypeId = !string.IsNullOrEmpty(ddlDisposition.SelectedValue) ? short.Parse(ddlDisposition.SelectedValue) : (short?)null;
            short? dispDetailID = !string.IsNullOrEmpty(ddlDispositionDetail.SelectedValue) ? short.Parse(ddlDispositionDetail.SelectedValue) : (short?)null;

            if (dispTypeId != null)
                newAction1.DispositionType = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == dispTypeId).FirstOrDefault();

            if (dispDetailID != null)
                newAction1.DispositionDetail = CscmsContext.DispositionDetail.Where(it => it.DispositionDetailID == dispDetailID).FirstOrDefault();

            newAction1.DispositionRenderedDate = dtDispositionRenderedDate.SelectedDate.HasValue ? dtDispositionRenderedDate.SelectedDate.Value : (DateTime?)null;
            newAction1.DispositionSignedDate = this.dtDispositionSignedDate.SelectedDate.HasValue ? dtDispositionSignedDate.SelectedDate.Value : (DateTime?)null;
            newAction1.Inactive = true;

            if (dtDispositionRenderedDate.SelectedDate.HasValue)
            {
                newAction1.Inactive = false;
                CSCMS.Data.ChildSupport childSupport = new CSCMS.Data.ChildSupport();
                childSupport.Action = newAction1;

                childSupport.CreateDate = DateTime.Now;
                childSupport.LastUpdateDate = DateTime.Now;
                CscmsContext.AddToChildSupport(childSupport);

                childSupport.ArrearageAsOf = dtDispositionRenderedDate.SelectedDate;
                childSupport.PastDueMedicalAsOf = dtDispositionRenderedDate.SelectedDate;

                if (dtHearingDate.SelectedDate.HasValue && dtHearingDate.SelectedDate > dtDispositionRenderedDate.SelectedDate)
                    newAction1.Inactive = true;
            }
            CscmsContext.SaveChanges();

            Session["NewCauseAdded"] = newCause1.CauseNumber;

            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
            CommandType.StoredProcedure,
            "UpdateActionStatus",
            new SqlParameter("@ActionGUID", newAction1.ActionGUID));
            return newCause1;
        }
        finally
        {
            if (iOpenedCon && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }
    #endregion

    public short NewCaseCountyID
    {
        get
        {
            short countyId = 0;
            if (Session["NewCauseAdded"] != null)
            {
                string causeNumber = (string)Session["NewCauseAdded"];
                countyId = CscmsContext.Cause.Where(it => it.CauseNumber == causeNumber).Select(it => it.County.CountyID).FirstOrDefault();
            }
            else if (this.ddlCountyNew.Items.Count > 1)
            {
                countyId = short.Parse(this.ddlCountyNew.Items[1].Value);
            }
            return countyId;
        }
    }

    protected void grdCases_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (this.Page.IsPostBack)
        {
            causeSearch.btnSearchByCase_Click(source, e);
            grdCases.Visible = true;
            this.btnExportCase.Visible = true;
        }
        else
        {
            grdCases.DataSource = new string[] { };
            grdCases.MasterTableView.NoMasterRecordsText = "";
            grdCases.Visible = false;
            this.btnExportCase.Visible = false;
        }
    }

    protected void HandleSearchClickedEvent(object sender, EventArgs args)
    {
        PersonMPE.Show();
    }

    protected void HandlePeasonSearchPageChange(object sender, EventArgs args)
    {
        PersonMPE.Show();
    }

    protected void ShowSearchWindow(object sender, EventArgs args)
    {
        if (sender == this.btnNCPPersonSearch)
        {
            this.HFNCPPersonGUID.Value = "Current";
        }
        PersonMPE.Show();
    }

    protected void grdCases_OnPageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        causeSearch.DoSearch();
    }

    protected void grdCases_OnSortCommand(object sender, GridSortCommandEventArgs e)
    {
        causeSearch.DoSearch();
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected List<CSCMS.Data.DispositionType> GetDispositionTypes()
    {
        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            CscmsContext.DispositionType.MergeOption = System.Data.Objects.MergeOption.NoTracking;
            List<CSCMS.Data.DispositionType> list = CscmsContext.DispositionType.Where(it => it.IsActive == true).ToList();
            list = list.Where(it => !it.DispositionTypeName.Contains(MyConsts.DB_NONE_CASE_OPEN)).ToList();

            if (this.CurrentUser.UserType != UserType.OCAAdmin)
                list = list.Where(it => it.DispositionTypeName != MyConsts.DB_SYSTEM_ADMIN_CLOSURE).ToList();

            if (this.CurrentAction != null)
            {
                if (this.CurrentAction.DispositionType != null)
                {
                    CSCMS.Data.DispositionType temp = list.Where(it => it.DispositionTypeID == this.CurrentAction.DispositionType.DispositionTypeID).FirstOrDefault();
                    if (temp == null)
                    {
                        temp = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == this.CurrentAction.DispositionType.DispositionTypeID).FirstOrDefault();
                        list.Add(temp);
                    }
                }
            }
            list = list.OrderBy(it => it.SortOrder).ToList();
            return list;
        }

    }

    private class PersonData
    {
        public CSCMS.Data.ActionPerson actionPerson { get; set; }
        public CSCMS.Data.Person person { get; set; }
        public CSCMS.Data.CustodialType custodialType { get; set; }
        public CSCMS.Data.PersonType personType { get; set; }
    }
}
