﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using System.Data.Linq;
using System.IO;

public partial class MemberPages_ContinuanceDetail : ActionBasePage
{
    Guid OagPersonGuid
    {
        get
        {
            return new Guid("6C19BEC0-7CC0-4017-A259-8F7FF6147C64");
        }
    }

    #region [Properties]

    protected bool NewContinuance
    {
        get { return Convert.ToBoolean(ViewState["NewContinuance"]); }
        set { ViewState["NewContinuance"] = value; }
    }
    #endregion


    protected override void OnInit(EventArgs e)
    {
        actionInfo.CauseGUID = new Guid(Request.QueryString["cid"]);
        actionInfo.ActionNumber = short.Parse(Request.QueryString["a"]);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (!Page.IsPostBack)
            {
                LoadRequestedBy();

                //rvRequesteDate.MaximumValue = DateTime.Now.ToShortDateString();

                LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));

                if (!string.IsNullOrEmpty(Request.QueryString["did"]))
                {
                    this.NewContinuance = false;
                    LoadContinuance(int.Parse(Request.QueryString["did"]));
                }
                else
                {
                    this.NewContinuance = true;
                    InitializeContinuance(Guid.Empty);
                }

                PopulateForm();

                if (!this.NewContinuance)
                {
                    btnDelete.Visible = true;
                }
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }
    private bool UpdateContinuance()
    {
        if (this.NewContinuance)
        {
            Guid perId = Guid.Empty;

            if (ddlRequestedBy.SelectedIndex > 0)
                perId = new Guid(ddlRequestedBy.SelectedValue);

            InitializeContinuance(perId);
            this.CurrentContinuance.CreateDate = DateTime.Now;
            this.CurrentContinuance.LastUpdateDate = DateTime.Now;
            CscmsContext.AddToContinuance(this.CurrentContinuance);
        }
        else
        {
            if (this.CurrentContinuance.EntityState == EntityState.Detached)
                CscmsContext.Attach(this.CurrentContinuance);
        }

        this.CurrentContinuance.Action = this.CurrentAction;
        this.CurrentContinuance.ContinuanceDate = dtRequestedDate.SelectedDate.Value;

        if (ddlRequestedBy.SelectedIndex > 0)
        {
            Guid perId = new Guid(ddlRequestedBy.SelectedValue);
            this.CurrentContinuance.Person = CscmsContext.Person.Where(it => it.PersonGUID == perId).FirstOrDefault();
        }
        else
            this.CurrentContinuance.Person = null;

        this.CurrentContinuance.ContinuanceNote = txtContinuanceNote.Text.Trim();
        this.CurrentContinuance.Agreed = chkAgreed.Checked;

        CSCMS.Data.Hearing h = GetHearingForOldContinuanceDate();
        if (this.dtContinueToDate.SelectedDate.HasValue)
        {
            if (this.NewContinuance || h == null)
            {
                CSCMS.Data.Hearing newHearing1 = new CSCMS.Data.Hearing();
                newHearing1.Action = this.CurrentAction;
                newHearing1.LastUpdateDate = System.DateTime.Now;
                newHearing1.CreateDate = System.DateTime.Now;
                newHearing1.HearingDate = this.dtContinueToDate.SelectedDate.Value;
                TimeSpan ts = CscmsContext.OCACourt.Where(it => it.OCACourtID == this.CurrentUser.OCACourtID).Select(it => it.DefaultHearingTime).FirstOrDefault();
                newHearing1.HearingTime = DateTime.Today.Add(ts).TimeOfDay;
                newHearing1.MorningDocket = true;
                CscmsContext.AddToHearing(newHearing1);
            }
            else
            {
                if (h != null && h.HearingDate != this.dtContinueToDate.SelectedDate)
                    h.HearingDate = this.dtContinueToDate.SelectedDate.Value;
            }

            GetHearingbyCountyCount(this.dtContinueToDate.SelectedDate.Value);
        }
        else if (!this.NewContinuance)
        {
            if (h != null)
                CscmsContext.DeleteObject(h);
        }

        this.CurrentContinuance.ContinuedToDate = dtContinueToDate.SelectedDate;
        CscmsContext.SaveChanges();

        return true;
    }

    private void GetHearingbyCountyCount(DateTime dtHearingDate)
    {
        messageBox.ErrorMessage = "A hearing is added/updated for the ContinuedToDate";

        // Warn the user if this is on a weekend or day off

        if (dtHearingDate.DayOfWeek == DayOfWeek.Saturday ||
            dtHearingDate.DayOfWeek == DayOfWeek.Sunday)
        {
            messageBox.ErrorMessage = "Warning: The Hearing Date occurs on a weekend.";
        }
        else
        {
            CSCMS.Data.SchedulingException se = CscmsContext.SchedulingException.Where(it => it.OCACourt.OCACourtID == this.CurrentUser.OCACourtID && it.ExceptionDate == dtHearingDate && ((it.BlockMorning) || it.BlockAfternoon)).FirstOrDefault();

            if (se != null)
            {
                messageBox.ErrorMessage = string.Format("Warning: The Hearing Date occurs on a scheduled day off ({0})", se.Note);
            }
        }

        Dictionary<DateTime, string> hsummary = Helper.GetDateHearingSummary(this.CscmsContext, dtHearingDate, dtHearingDate, this.CurrentUser.OCACourtID);

        if (hsummary.Count > 0)
        {
            if (hsummary.ContainsKey(dtHearingDate))
            {
                int countyCnt = hsummary[dtHearingDate].Count(f => f.Equals('('));
                if (countyCnt > 1)
                    messageBox.ErrorMessage += @"<br>Multiple counties hearing on hearing date: " + dtHearingDate.ToString("MM/dd/yyyy") + " " + hsummary[dtHearingDate];
            }
        }
    }

    private CSCMS.Data.Hearing GetHearingForOldContinuanceDate()
    {
        CSCMS.Data.Hearing h = null;
        CSCMS.Data.Continuance temp = CscmsContext.Continuance.Where(it => it.ContinuanceID == this.CurrentContinuance.ContinuanceID).FirstOrDefault();
        if (temp != null && temp.ContinuedToDate != null)
        {
            TimeSpan ht = System.DateTime.Parse("09:00:00").TimeOfDay;
            h = CscmsContext.Hearing.Where(it => it.Action.ActionGUID == this.CurrentAction.ActionGUID && it.HearingDate == temp.ContinuedToDate).FirstOrDefault();
        }
        return h;
    }

    private void InitializeContinuance(Guid perGuid)
    {
        CSCMS.Data.Continuance continuance = new CSCMS.Data.Continuance();
        continuance.CreateDate = DateTime.Now;
        continuance.LastUpdateDate = DateTime.Now;
        continuance.Action = this.CurrentAction;
        continuance.Person = CscmsContext.Person.Where(it => it.PersonGUID == perGuid).FirstOrDefault();
        this.CurrentContinuance = continuance;
    }

    protected void btnSaveAndReturnToCase_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (UpdateContinuance())
            {
                if (this.dtContinueToDate.SelectedDate.HasValue)
                    Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID
                        + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString()
                        + "&message=SaveContinuance" + "&hd=" + this.dtContinueToDate.SelectedDate.Value.ToShortDateString(), true);
                else
                    Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID
                         + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=SaveContinuance", true);
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (UpdateContinuance())
            {
                this.NewContinuance = false;
                LoadContinuance(this.CurrentContinuance.ContinuanceID);
                PopulateForm();
                messageBox.SuccessMessage = "Continuance saved successfully.";
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void ContinuanceDate_SelectedDateChanged(object sender, EventArgs e)
    {
        DateTime selectedDate = dtRequestedDate.SelectedDate.Value;
        double contdays = (selectedDate - DateTime.Now).TotalDays;
        if ((selectedDate - DateTime.Now).TotalDays > 90)
        {
            messageBox.InfoMessage = "The Continuance Date you entered is in " +  Convert.ToInt32(contdays)  +  " days.";
        }

    }

    private void PopulateForm()
    {
        //dtRequestedDate.MaxDate = DateTime.Today;
        dtRequestedDate.SelectedDate = this.CurrentContinuance.ContinuanceDate != DateTime.MinValue ? this.CurrentContinuance.ContinuanceDate : (DateTime?)null;
        ddlRequestedBy.SelectedValue = this.CurrentContinuance.Person != null ? this.CurrentContinuance.Person.PersonGUID.ToString() : "";

        if (!NewContinuance)
        {
            txtContinuanceNote.Text = this.CurrentContinuance.ContinuanceNote;
            dtContinueToDate.SelectedDate = this.CurrentContinuance.ContinuedToDate != DateTime.MinValue ? this.CurrentContinuance.ContinuedToDate : (DateTime?)null;
            chkAgreed.Checked = (this.CurrentContinuance.Agreed != null ? this.CurrentContinuance.Agreed.Value : false);
        }

        SetFieldVisibility();
    }

    private void SetFieldVisibility()
    {
        if (!NewContinuance)
        {
            btnDelete.Visible = true;
            divCondinuanceButtons.Visible = true;
            //sectionActions.Visible = true;
            divMain.Attributes["class"] = "existingCase";
            divMainSub.Attributes["class"] = "existingCaseSub";

        }

        if (Request.QueryString["return"] == "c")
        {
            btnDelete.Visible = false;
            divCondinuanceButtons.Visible = false;
            btnSaveContinuance.Visible = false;

            //btnSaveAndReturnToCase.Visible = true && (!this.CurrentUser.ViewAllUser) || (Convert.ToBoolean(Session["Impersonating"]) == true);
        }
    }

    private void LoadContinuance(int continuanceID)
    {
        //TODO: Check to make sure the current user has access to this record
        this.CurrentContinuance = CscmsContext.Continuance.Include("Action").Include("Person").Where(it => it.ContinuanceID == continuanceID).FirstOrDefault();

        if (this.CurrentContinuance.Action.ActionGUID != this.CurrentAction.ActionGUID)
        {
            CurrentContinuance = null;
            Response.Redirect("~/AnonPages/Unauthorized.aspx", true);
        }

        divCaseNotes.Visible = true;
    }


    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(SqlConn,
           CommandType.StoredProcedure,
           "DeleteActionContinuance",
           new SqlParameter("@ActionGUID", this.CurrentContinuance.Action.ActionGUID.ToString()),
           new SqlParameter("@ContinuanceID", this.CurrentContinuance.ContinuanceID));
        this.CurrentContinuance = null;
        Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=DeleteContinuance", true);
        //dc.DeleteRegisterOfAction(this.CurrentCause.CauseGUID.ToString(), this.CurrentHearing.HearingID, "Hearing");
        //this.CurrentHearing = null;
        //Response.Redirect("CaseSummary.aspx?message=Deletehearing&cid=" + this.CurrentCause.CauseGUID.ToString(), true);
    }

    protected void LoadRequestedBy()
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
             CommandType.StoredProcedure,
             "GetActionPersons",
             new SqlParameter("@ActionGUID", ((BasePage)this.Page).CurrentAction.ActionGUID));

        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                ListItem item = new ListItem(string.Format("{0} - {1}",
                    Helper.GetFormattedName(Helper.EvaluateDbStringValue(r["FirstName"]), Helper.EvaluateDbStringValue(r["MiddleName"]), Helper.EvaluateDbStringValue(r["LastName"]), Helper.EvaluateDbStringValue(r["NameSuffix"])),
                    Helper.EvaluateDbStringValue(r["PersonTypeName"])), string.Format("{0}", r["PersonGUID"]));

                ddlRequestedBy.Items.Add(item);
            }
            ddlRequestedBy.Items.Add(new ListItem("OAG", string.Format("{0}", this.OagPersonGuid)));
        }
    }
}