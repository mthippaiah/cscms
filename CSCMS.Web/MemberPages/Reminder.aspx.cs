﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

public partial class MemberPages_Reminder : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["message"] == "Add")
            {
                messageBox.SuccessMessage = "Reminder added successfully.";
            }
            else if (Request.QueryString["message"] == "Delete")
            {
                messageBox.SuccessMessage = "Reminder deleted successfully.";
            }
            else if (Request.QueryString["message"] == "Update")
            {
                messageBox.SuccessMessage = "Reminder updated successfully.";
            }

        }
    }
}