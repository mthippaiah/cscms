﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

public partial class MemberPages_Attorneys : BasePage
{

    #region [Properties]

    protected bool NewPerson
    {
        get { return Convert.ToBoolean(ViewState["NewPerson"]); }
        set { ViewState["NewPerson"] = value; }
    }

    #endregion

    #region [Page]

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.Form.DefaultFocus = txtSearchLastName.ClientID;

            if (Request.QueryString["message"] == "Updated")
            {
                messageBox.SuccessMessage = "Attorney saved successfully.";
            }

            if (Session["txtAttorneySearchLastName"] != null) txtSearchLastName.Text = Session["txtAttorneySearchLastName"].ToString();
            if (Session["txtAttorneySearchFirstName"] != null) this.txtSearchFirstName.Text = Session["txtAttorneySearchFirstName"].ToString();


        }
        if (ddlCounty.Items.Count == 1) ddlCounty.DataBind();
        if (ddlState.Items.Count == 1) ddlState.DataBind();
        if (this.ddlFirstLanguage.Items.Count == 1) ddlFirstLanguage.DataBind();
        if (this.ddlSecondLanguage.Items.Count == 1) ddlSecondLanguage.DataBind();
        if (this.ddlThirdLanguage.Items.Count == 1) ddlThirdLanguage.DataBind();

        grdResults.EnableAjaxSkinRendering = true;
    }

    #endregion

    #region [Search]
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        grdResults.Rebind();

        Session["txtAttorneySearchLastName"] = txtSearchLastName.Text;
        Session["txtAttorneySearchFirstName"] = this.txtSearchFirstName.Text;
    }

    protected void btnAddAttorney_Command(object sender, CommandEventArgs e)
    {

        sectionDetail.Visible = true;
        sectionSearch.Visible = false;
        grdResults.Visible = false;
        btnAddAttorney.Visible = false;

        this.NewPerson = true;

        ViewState["AttorneyPid"] = "NEW";

        this.Form.DefaultFocus = txtFirstName.ClientID;

    }

    protected void dsCounty_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@UserId"].Value = this.CurrentUser.UserID;
        e.Command.Parameters["@CourtID"].Value = this.CurrentUser.OCACourtID;
    }

    protected void dsPersons_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID;
    }

    protected void grdResults_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == "editAttorney")
        {
            Guid selectId = (Guid)(grdResults.MasterTableView.DataKeyValues[e.Item.ItemIndex]["PersonGUID"]);

            sectionDetail.Visible = true;

            sectionSearch.Visible = false;

            grdResults.Visible = false;

            btnAddAttorney.Visible = false;

            this.NewPerson = false;

            LoadPerson(selectId);

            PopulateForm();

            this.Form.DefaultFocus = txtFirstName.ClientID;

            ViewState["AttorneyPid"] = this.CurrentPerson.PersonGUID;

        }
    }

    private void PopulateForm()
    {
        txtFirstName.Text = this.CurrentPerson.FirstName;
        txtLastName.Text = this.CurrentPerson.LastName;
        txtMiddleName.Text = this.CurrentPerson.MiddleName;
        txtSuffix.Text = this.CurrentPerson.NameSuffix;
        txtBarCardNumber.Text = this.CurrentPerson.BarCardNumber.HasValue ? this.CurrentPerson.BarCardNumber.Value.ToString() : "";
        txtFirm.Text = this.CurrentPerson.Firm;
        this.txtPhoneCell.Text = this.CurrentPerson.CellPhone;
        this.txtPhoneFax3.Text = this.CurrentPerson.FaxNumber;
        this.txtPhoneHome.Text = this.CurrentPerson.HomePhone;
        this.txtPhoneOffice.Text = this.CurrentPerson.OfficePhone;

        ddlSex.SelectedValue = !string.IsNullOrEmpty(this.CurrentPerson.Sex) ? this.CurrentPerson.Sex.ToString() : "";

        txtAddress1.Text = this.CurrentPerson.Address1;
        txtAddress2.Text = this.CurrentPerson.Address2;
        txtAddress3.Text = this.CurrentPerson.Address3;
        txtCity.Text = this.CurrentPerson.City;
        ddlState.Text = (this.CurrentPerson.State != null) ? this.CurrentPerson.State.StateID : "";
        txtZipCode.Text = this.CurrentPerson.ZipCode.HasValue ? this.CurrentPerson.ZipCode.ToString() : "";
        txtZipCodeExt.Text = this.CurrentPerson.ZipCodeExtension.HasValue ? this.CurrentPerson.ZipCodeExtension.ToString() : "";
        ddlCounty.SelectedValue = this.CurrentPerson.CountyID.HasValue ? this.CurrentPerson.CountyID.ToString() : "";

        txtEmailAddress.Text = this.CurrentPerson.EmailAddress;

        ddlFirstLanguage.SelectedValue = (this.CurrentPerson.IntepreterLanguage != null) ? this.CurrentPerson.IntepreterLanguage.IntepreterLanguageID.ToString() : "";
        ddlSecondLanguage.SelectedValue = (this.CurrentPerson.IntepreterLanguage1 != null) ? this.CurrentPerson.IntepreterLanguage1.IntepreterLanguageID.ToString() : "";
        ddlThirdLanguage.SelectedValue = (this.CurrentPerson.IntepreterLanguage2 != null) ? this.CurrentPerson.IntepreterLanguage2.IntepreterLanguageID.ToString() : "";

        CSCMS.Data.AttorneyCounty ac = CscmsContext.AttorneyCounty.Where(it => it.Person.PersonGUID == this.CurrentPerson.PersonGUID).FirstOrDefault();

        if (ac != null)
        {
            chkInActive.Checked = ac.Inactive;
        }
    }

    protected string FormatCounties(string counties)
    {
        if (string.IsNullOrEmpty(counties))
        {
            return "&nbsp;";
        }
        if (counties.EndsWith("|"))
        {
            counties = counties.Remove(counties.Length - 1);
        }
        return counties.Replace("|", ", ");
    }

    #endregion

    #region [Save]

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        bool redirect = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            UpdatePerson();
            redirect = true;
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.Message;
            LogError.LogErrorToSQL(ex.ToString());
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();

            if (redirect)
                Response.Redirect(string.Format("Attorneys.aspx?message=Updated"), true);
        }
    }

    protected void dsSelectedCounties_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        Guid pid = new Guid();
        if (!this.NewPerson && this.CurrentPerson != null)
        {
            pid = this.CurrentPerson.PersonGUID;
        }

        e.Command.Parameters["@PersonGUID"].Value = pid.ToString();
    }

    #endregion

    #region [Update Data]

    private void UpdatePerson()
    {
        Guid attorneyPersonGuid = Guid.Empty;
        Guid casaPersonGuid = Guid.Empty;
        Guid galPersonGuid = Guid.Empty;

        if (this.NewPerson)
        {
            CSCMS.Data.Person p = new CSCMS.Data.Person();
            p.PersonGUID = Guid.NewGuid();
            p.LastUpdateDate = System.DateTime.Today;
            p.CreateDate = System.DateTime.Today;
            this.CurrentPerson = p;
            CscmsContext.AddToPerson(this.CurrentPerson);
        }
        else
        {
            Guid pGuid = new Guid(ViewState["AttorneyPid"].ToString());
            base.LoadPerson(pGuid);  //This sets this.CurrentPerson
        }

        this.CurrentPerson.FirstName = txtFirstName.Text.Trim();
        this.CurrentPerson.LastName = txtLastName.Text.Trim();
        this.CurrentPerson.MiddleName = txtMiddleName.Text.Trim();
        this.CurrentPerson.NameSuffix = txtSuffix.Text.Trim();

        this.CurrentPerson.Sex = !string.IsNullOrEmpty(ddlSex.SelectedValue) ? char.Parse(ddlSex.SelectedValue).ToString() : null;

        this.CurrentPerson.Address1 = txtAddress1.Text.Trim();
        this.CurrentPerson.Address2 = txtAddress2.Text.Trim();
        this.CurrentPerson.Address3 = txtAddress3.Text.Trim();
        this.CurrentPerson.City = txtCity.Text.Trim();
        this.CurrentPerson.CountyID = !string.IsNullOrEmpty(ddlCounty.SelectedValue) ? short.Parse(ddlCounty.SelectedValue) : (short?)null;
        string stateId = !string.IsNullOrEmpty(ddlState.SelectedValue) ? ddlState.SelectedValue : null;
        if (!string.IsNullOrEmpty(stateId))
            this.CurrentPerson.State = CscmsContext.State.Where(it => it.StateID == stateId).FirstOrDefault();
        else
            this.CurrentPerson.State = null;

        this.CurrentPerson.ZipCode = !string.IsNullOrEmpty(txtZipCode.Text) ? int.Parse(txtZipCode.Text) : (int?)null;
        this.CurrentPerson.ZipCodeExtension = !string.IsNullOrEmpty(txtZipCodeExt.Text) ? short.Parse(txtZipCodeExt.Text) : (short?)null;

        this.CurrentPerson.EmailAddress = this.txtEmailAddress.Text;
        this.CurrentPerson.HomePhone = this.txtPhoneHome.Text;
        this.CurrentPerson.OfficePhone = this.txtPhoneOffice.Text;
        this.CurrentPerson.CellPhone = this.txtPhoneCell.Text;
        this.CurrentPerson.FaxNumber = this.txtPhoneFax3.Text;
        this.CurrentPerson.BarCardNumber = !string.IsNullOrEmpty(txtBarCardNumber.Text) ? int.Parse(txtBarCardNumber.Text) : (int?)null;
        this.CurrentPerson.Firm = txtFirm.Text.Trim();

        short? fLangId = !string.IsNullOrEmpty(this.ddlFirstLanguage.SelectedValue) ? short.Parse(ddlFirstLanguage.SelectedValue) : (short?)null;
        short? sLangId = !string.IsNullOrEmpty(this.ddlSecondLanguage.SelectedValue) ? short.Parse(ddlSecondLanguage.SelectedValue) : (short?)null;
        short? tLangId = !string.IsNullOrEmpty(this.ddlThirdLanguage.SelectedValue) ? short.Parse(ddlThirdLanguage.SelectedValue) : (short?)null;

        if (fLangId != null)
            this.CurrentPerson.IntepreterLanguage = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == fLangId).FirstOrDefault();
        else
            this.CurrentPerson.IntepreterLanguage = null;

        if (sLangId != null)
            this.CurrentPerson.IntepreterLanguage1 = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == sLangId).FirstOrDefault();
        else
            this.CurrentPerson.IntepreterLanguage1 = null;

        if (tLangId != null)
            this.CurrentPerson.IntepreterLanguage2 = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == tLangId).FirstOrDefault();
        else
            this.CurrentPerson.IntepreterLanguage2 = null;

        if (dlAttorneyCounties.RightItems.Count == 0) throw new Exception("Please select at least one County.");

        List<CSCMS.Data.AttorneyCounty> deleteList = new List<CSCMS.Data.AttorneyCounty>();
        foreach (CSCMS.Data.AttorneyCounty ac in this.CurrentPerson.AttorneyCounty)
        {
            bool countyFound = false;

            foreach (ListItem item in dlAttorneyCounties.RightItems)
            {
                if (short.Parse(item.Value) == ac.CountyID)
                {
                    countyFound = true;
                    break;
                }
            }

            if (!countyFound)
            {
                deleteList.Add(ac);
            }
            else
            {
                var attorneyCounty = CscmsContext.AttorneyCounty.Where(p => p.Person.PersonGUID == this.CurrentPerson.PersonGUID && p.County.CountyID == ac.CountyID).FirstOrDefault();

                if (attorneyCounty != null)
                {
                    attorneyCounty.Inactive = chkInActive.Checked;
                }
            }
        }


        foreach (ListItem item in dlAttorneyCounties.RightItems)
        {
            // see if this county already exists
            CSCMS.Data.AttorneyCounty attorneyCounty = null;

            foreach (CSCMS.Data.AttorneyCounty ac in this.CurrentPerson.AttorneyCounty)
            {
                if (short.Parse(item.Value) == ac.CountyID)
                {
                    attorneyCounty = ac;
                    break;
                }
            }

            if (attorneyCounty == null)
            {
                // Add the Attorney County record
                CSCMS.Data.AttorneyCounty ac = new CSCMS.Data.AttorneyCounty();
                ac.Person = this.CurrentPerson;
                short countyId = short.Parse(item.Value);
                ac.County = CscmsContext.County.Where(it => it.CountyID == countyId).FirstOrDefault();
                ac.Inactive = this.chkInActive.Checked;
                ac.CreateDate = DateTime.Now;
                ac.LastUpdateDate = DateTime.Now;
                CscmsContext.AddToAttorneyCounty(ac);
            }
        }

        foreach (CSCMS.Data.AttorneyCounty ac in deleteList)
            CscmsContext.DeleteObject(ac);

        CscmsContext.SaveChanges();
    }

    #endregion

    #region [Grids]

    protected void btnExportAttorney_Click(object sender, EventArgs e)
    {
        if (grdResults.Visible && grdResults.Items.Count > 0)
        {
            this.grdResults.ExportSettings.ExportOnlyData = true;
            grdResults.ExportSettings.IgnorePaging = true;
            grdResults.ExportSettings.OpenInNewWindow = true;
            grdResults.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
            grdResults.MasterTableView.ExportToExcel();
        }
    }

    protected void grdResults_ExcelMLExportStylesCreated(object source, GridExportExcelMLStyleCreatedArgs e)
    {
        foreach (StyleElement style in e.Styles)
        {
            if (style.Id == "headerStyle")
            {
                style.FontStyle.Bold = true;
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "itemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "alternatingItemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
        }
    }

    protected void grdResults_ExcelMLExportRowCreated(object source, GridExportExcelMLRowCreatedArgs e)
    {
        if (e.RowType == GridExportExcelMLRowType.DataRow)
        {
            int currentRow = e.Worksheet.Table.Rows.IndexOf(e.Row) - 1;

            //populate the data cell 
            e.Row.Cells.GetCellByName("Counties").Data.DataItem = FormatCounties((string)((System.Data.DataRowView)grdResults.MasterTableView.Items[currentRow].DataItem).Row["Counties"]).Replace("&nbsp;", "");
        }
    }

    protected void grdResults_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (this.Page.IsPostBack)
        {
            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
         CommandType.StoredProcedure,
         "SearchAttorneys",
         new SqlParameter("@OCACourtID", this.CurrentUser.OCACourtID),
         new SqlParameter("@LastName", !string.IsNullOrEmpty(txtSearchLastName.Text) ? txtSearchLastName.Text : null),
         new SqlParameter("@FirstName", !string.IsNullOrEmpty(txtSearchFirstName.Text) ? this.txtSearchFirstName.Text : null),
         new SqlParameter("@Firm", !string.IsNullOrEmpty(txtSearchFirm.Text) ? this.txtSearchFirm.Text : null),
         new SqlParameter("@CountyID", ddlSearchCounty.SelectedIndex > 0 ? short.Parse(ddlSearchCounty.SelectedValue) : (short?)null),
         new SqlParameter("@Status", ddlStatus.SelectedIndex > 0 ? ddlStatus.SelectedValue : null),
         new SqlParameter("@Language", ddlSearchLanguage.SelectedIndex > 0 ? ddlSearchLanguage.SelectedValue : null)
            );

            grdResults.DataSource = ds;
        }
        else
        {
            grdResults.DataSource = new string[] { };
            grdResults.MasterTableView.NoMasterRecordsText = "";
        }
    }

    protected override void OnPreRenderComplete(EventArgs e)
    {
        RadGrid grdResults = (RadGrid)sectionSearch.FindControl("grdResults");

        //ajaxManager.AjaxSettings.AddAjaxSetting(btnSearch, grdResults, this.loadingPanel);
        base.OnPreRenderComplete(e);
    }
    #endregion
}

