﻿<%@ Page Title="CSCMS | Manage Days Off"  Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="DaysOff.aspx.cs" Inherits="MemberPages_DaysOff" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
    <div class="pageTitle" style="float: left">MANAGE DAYS OFF</div>
    <div style="clear: both;" />
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <telerik:RadGrid ID="grdDaysOff" runat="server" DataSourceID="dsDaysOff" GridLines="None" AllowPaging="False" AllowAutomaticUpdates="True" AllowAutomaticInserts="True" AllowAutomaticDeletes="true" AllowSorting="true" Width="100%" OnItemCommand="grdDaysOff_ItemCommand" OnItemInserted="grdDaysOff_ItemInserted" OnItemUpdated="grdDaysOff_ItemUpdated">
        <MasterTableView DataSourceID="dsDaysOff" EditMode="EditForms" AutoGenerateColumns="false" DataKeyNames="OCACourtID, ExceptionDate" CommandItemDisplay="Top">
            <CommandItemSettings AddNewRecordText="Add New Day Off" />
            <Columns>
                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" />
                <telerik:GridButtonColumn ConfirmText="Delete this day off?" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" />
                <telerik:GridDateTimeColumn HeaderText="Date" DataField="ExceptionDate" DataFormatString="{0:MM/dd/yyyy}" />
                <telerik:GridCheckBoxColumn HeaderText="Block AM" DataField="BlockMorning" ItemStyle-HorizontalAlign="Center" />
                <telerik:GridCheckBoxColumn HeaderText="Block PM" DataField="BlockAfternoon" ItemStyle-HorizontalAlign="Center" />
                <telerik:GridTemplateColumn HeaderText="Appointment 1" DataField="TimePeriod1StartTime" SortExpression="TimePeriod1StartTime" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
					<ItemTemplate>
						<%# Eval("TimePeriod1StartTime") != DBNull.Value ? Eval("TimePeriod1StartTime", "{0:t}") + " - " + Eval("TimePeriod1EndTime", "{0:t}") : "&nbsp;"%>
					</ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Appointment 2" DataField="TimePeriod2StartTime" SortExpression="TimePeriod2StartTime" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
					<ItemTemplate>
						<%# Eval("TimePeriod2StartTime") != DBNull.Value ? Eval("TimePeriod2StartTime", "{0:t}") + " - " + Eval("TimePeriod2EndTime", "{0:t}") : "&nbsp;"%>
					</ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="AM Note" DataField="Note" />
                <telerik:GridBoundColumn HeaderText="PM Note" DataField="AfternoonNote" />
                <telerik:GridBoundColumn HeaderText="Apt 1 Note" DataField="TimePeriod1Note" />
                <telerik:GridBoundColumn HeaderText="Apt 2 Note" DataField="TimePeriod2Note" />
            </Columns>
            <EditFormSettings EditFormType="Template">
                <EditColumn ButtonType="ImageButton" />
                <FormTemplate>
					<table cellpadding="2">
						<tr>
							<td><b>Date: </b></td>
							<td colspan="3"><telerik:RadDatePicker ID="dtDate" Runat="server" Width="92" DbSelectedDate='<%# Bind("ExceptionDate") %>' /></td>
						</tr>
						<tr>
							<td><b>Block Morning: </b></td>
							<td><asp:CheckBox ID="chkBlockMorning" runat="server" Checked='<%# Bind("BlockMorning") %>' /></td>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;Note:
								<asp:TextBox ID="txtNote" runat="server" Width="300" Text='<%# Bind("Note") %>' />
							</td>
						</tr>
						<tr>
							<td><b>Block Afternoon: </b></td>
							<td><asp:CheckBox ID="chkBlockAfternoon" runat="server" Checked='<%# Bind("BlockAfternoon") %>' /></td>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;Note:
								<asp:TextBox ID="txtAfternoonNote" runat="server" Width="300" Text='<%# Bind("AfternoonNote") %>' />
							</td>
						</tr>
						<tr>
							<td><b>Appointment 1: </b></td>
							<td colspan="2">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td style="border: 0px; padding: 0px;">Start:&nbsp;</td>
										<td style="border: 0px; padding: 0px;">
											<telerik:RadTimePicker ID="timePeriod1StartTime" runat="server" Skin="Office2007" Width="75px" DbSelectedDate='<%# Bind("TimePeriod1StartTime") %>'>
												<DateInput Font-Size="11px" Font-Names="Arial" />
												<TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:30:00" EndTime="18:00:00" Columns="4" />
												<TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
											</telerik:RadTimePicker>
										</td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td style="border: 0px; padding: 0px;">End: </td>
										<td style="border: 0px; padding: 0px;">
											<telerik:RadTimePicker ID="timePeriod1EndTime" runat="server" Skin="Office2007" Width="75px" DbSelectedDate='<%# Bind("TimePeriod1EndTime") %>'>
												<DateInput Font-Size="11px" Font-Names="Arial" />
												<TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:30:00" EndTime="18:00:00" Columns="4" />
												<TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
											</telerik:RadTimePicker>
										</td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td style="border: 0px; padding: 0px;">Note:&nbsp;</td>
										<td style="border: 0px; padding: 0px;"><asp:TextBox ID="txtApt1Note" runat="server" Width="300" Text='<%# Bind("TimePeriod1Note") %>' /></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><b>Appointment 2: </b></td>
							<td colspan="2">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td style="border: 0px; padding: 0px;">Start:&nbsp;</td>
										<td style="border: 0px; padding: 0px;">
											<telerik:RadTimePicker ID="timePeriod2StartTime" runat="server" Skin="Office2007" Width="75px" DbSelectedDate='<%# Bind("TimePeriod2StartTime") %>'>
												<DateInput Font-Size="11px" Font-Names="Arial" />
												<TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:30:00" EndTime="18:00:00" Columns="4" />
												<TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
											</telerik:RadTimePicker>
										</td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td style="border: 0px; padding: 0px;">End: </td>
										<td style="border: 0px; padding: 0px;">
											<telerik:RadTimePicker ID="timePeriod2EndTime" runat="server" Skin="Office2007" Width="75px" DbSelectedDate='<%# Bind("TimePeriod2EndTime") %>'>
												<DateInput Font-Size="11px" Font-Names="Arial" />
												<TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:30:00" EndTime="18:00:00" Columns="4" />
												<TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
											</telerik:RadTimePicker>
										</td>
										<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
										<td style="border: 0px; padding: 0px;">Note:&nbsp;</td>
										<td style="border: 0px; padding: 0px;"><asp:TextBox ID="txtApt2Note" runat="server" Width="300" Text='<%# Bind("TimePeriod2Note") %>' /></td>
									</tr>
								</table>
							</td>
						</tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnUpdate" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>' runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />&nbsp;
								<asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False" CommandName="Cancel"></asp:Button>
							</td>
                        </tr>
                    </table>
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:SqlDataSource ID="dsDaysOff" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>" OnSelecting="dsDaysOff_Selecting" OnInserting="dsDaysOff_Inserting" OnUpdating="dsDaysOff_Updating" OnDeleting="dsDaysOff_Deleting"
        DeleteCommand="DELETE FROM SchedulingException WHERE OCACourtID = @OCACourtID AND ExceptionDate = @ExceptionDate"
        InsertCommand="INSERT INTO SchedulingException (OCACourtID, ExceptionDate, BlockMorning, BlockAfternoon, Note, CreateDate, LastUpdateDate, TimePeriod1StartTime, TimePeriod1EndTime, TimePeriod2StartTime, TimePeriod2EndTime, AfternoonNote, TimePeriod1Note, TimePeriod2Note) VALUES (@OCACourtID, @ExceptionDate, @BlockMorning, @BlockAfternoon, @Note, @CreateDate, @LastUpdateDate, @TimePeriod1StartTime, @TimePeriod1EndTime, @TimePeriod2StartTime, @TimePeriod2EndTime, @AfternoonNote, @TimePeriod1Note, @TimePeriod2Note)"
        SelectCommand="SELECT OCACourtID, ExceptionDate, BlockMorning, BlockAfternoon, Note, TimePeriod1StartTime, TimePeriod1EndTime, TimePeriod2StartTime, TimePeriod2EndTime, AfternoonNote, TimePeriod1Note, TimePeriod2Note FROM SchedulingException WHERE OCACourtID = @OCACourtID AND ExceptionDate >= CAST(GetDate() as Date) ORDER BY ExceptionDate"
        UpdateCommand="UPDATE SchedulingException SET OCACourtID = @OCACourtID, ExceptionDate = @ExceptionDate, BlockMorning = @BlockMorning, BlockAfternoon = @BlockAfternoon, Note = @Note, TimePeriod1StartTime = @TimePeriod1StartTime, TimePeriod1EndTime = @TimePeriod1EndTime, TimePeriod2StartTime = @TimePeriod2StartTime, TimePeriod2EndTime = @TimePeriod2EndTime, AfternoonNote = @AfternoonNote, TimePeriod1Note = @TimePeriod1Note, TimePeriod2Note = @TimePeriod2Note WHERE OCACourtID = @OCACourtID AND ExceptionDate = @ExceptionDate">
        <SelectParameters>
            <asp:Parameter Name="OCACourtID" Type="Int16" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="OCACourtID" Type="Int16" />
            <asp:Parameter Name="ExceptionDate" Type="DateTime" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="OCACourtID" Type="Int16" />
            <asp:Parameter Name="ExceptionDate" Type="DateTime" />
            <asp:Parameter Name="BlockMorning" Type="Boolean" />
            <asp:Parameter Name="BlockAfternoon" Type="Boolean" />
            <asp:Parameter Name="Note" Type="String" />
            <asp:Parameter Name="TimePeriod1StartTime" Type="DateTime" />
            <asp:Parameter Name="TimePeriod1EndTime" Type="DateTime" />
            <asp:Parameter Name="TimePeriod2StartTime" Type="DateTime" />
            <asp:Parameter Name="TimePeriod2EndTime" Type="DateTime" />
            <asp:Parameter Name="AfternoonNote" Type="String" />
            <asp:Parameter Name="TimePeriod1Note" Type="String" />
            <asp:Parameter Name="TimePeriod2Note" Type="String" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="OCACourtID" Type="Int16" />
            <asp:Parameter Name="ExceptionDate" Type="DateTime" />
            <asp:Parameter Name="BlockMorning" Type="Boolean" />
            <asp:Parameter Name="BlockAfternoon" Type="Boolean" />
            <asp:Parameter Name="Note" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="LastUpdateDate" Type="DateTime" />
            <asp:Parameter Name="TimePeriod1StartTime" Type="DateTime" />
            <asp:Parameter Name="TimePeriod1EndTime" Type="DateTime" />
            <asp:Parameter Name="TimePeriod2StartTime" Type="DateTime" />
            <asp:Parameter Name="TimePeriod2EndTime" Type="DateTime" />
            <asp:Parameter Name="AfternoonNote" Type="String" />
            <asp:Parameter Name="TimePeriod1Note" Type="String" />
            <asp:Parameter Name="TimePeriod2Note" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdDaysOff">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdDaysOff" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 80px" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>

