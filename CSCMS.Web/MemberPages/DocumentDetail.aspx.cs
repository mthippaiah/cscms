﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using System.Data.Linq;
using System.IO;


public partial class MemberPages_DocumentDetail : ActionBasePage
{
    #region [Properties]

    protected bool NewDocument
    {
        get { return Convert.ToBoolean(ViewState["NewDocument"]); }
        set { ViewState["NewDocument"] = value; }
    }
    #endregion
    #region [Page]
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        if (Request.QueryString["cid"] != null)
            actionInfo.CauseGUID = new Guid(Request.QueryString["cid"]);

        if (Request.QueryString["a"] != null)
            actionInfo.ActionNumber = short.Parse(Request.QueryString["a"]);
        //casePersons.CauseGUID = actionInfo.CauseGUID;
        //caseActions.CauseGUID = actionInfo.CauseGUID;
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Request.QueryString["downloaddoc"] != null)
            {
                LoadDocument(int.Parse(Request.QueryString["did"]));
                btnDownload_Click(null, null);
                return;
            }

            rvDocumentDate.MaximumValue = DateTime.Now.ToShortDateString();

            LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));

            if (!string.IsNullOrEmpty(Request.QueryString["did"]))
            {
                this.NewDocument = false;

                LoadDocument(int.Parse(Request.QueryString["did"]));

            }
            else
            {
                this.NewDocument = true;
                InitializeDocument();
            }

            PopulateForm();

            if (!this.NewDocument)
            {
                btnDelete.Visible = true;
                //CheckForWarnings();
            }

            //if (this.CurrentUser.ViewAllUser && Convert.ToBoolean(Session["Impersonating"]) == false) btnSaveHearingSchedule.Visible = false;



            //btnSaveHearingScheduleAndAdd.Visible = btnSaveHearingSchedule.Visible;

            //btnCreateOrder.Visible = btnSaveHearingSchedule.Visible;


        }

        //if (Request.QueryString["pr"] != "1")
        //{
        //    hearingDateTooltip.VisibleOnPageLoad = false;
        //    hearingDateTooltip.Visible = false;
        //}
    }

    private void InitializeDocument()
    {
        CSCMS.Data.Document document = new CSCMS.Data.Document();
        document.CreateDate = DateTime.Now;
        document.LastUpdateDate = DateTime.Now;
        document.Action = CscmsContext.Action.Where(it => it.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();
        document.RowGuid = Guid.NewGuid();

        if (!string.IsNullOrEmpty(Request.QueryString["dd"]))
        {
            document.DocumentDate = DateTime.Parse(Request.QueryString["dd"]);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["dt"]))
        {
            byte id = byte.Parse(Request.QueryString["dt"]);
            document.DocumentType = CscmsContext.DocumentType.Where(it => it.DocumentTypeID == id).FirstOrDefault();
            ddlDocumentType.Enabled = false;
        }

        this.CurrentDocument = document;
    }
    #endregion

    #region [Buttons]

    private bool UpdateDocument()
    {
        if (this.NewDocument && !fileUpload.HasFile)
        {
            messageBox.ErrorMessage = "There was an error uploading the file. Please try again.";
            return false;
        }

        if (this.NewDocument)
        {
            InitializeDocument();
            CscmsContext.AddToDocument(this.CurrentDocument);
        }
        else
        {
            if (this.CurrentDocument.EntityState == EntityState.Detached)
                CscmsContext.Attach(this.CurrentDocument);
        }

        this.CurrentDocument.DocumentDate = dtDocumentDate.SelectedDate.Value;
        byte id = byte.Parse(ddlDocumentType.SelectedValue);
        this.CurrentDocument.DocumentType = CscmsContext.DocumentType.Where(it => it.DocumentTypeID == id).FirstOrDefault();
        this.CurrentDocument.DocumentFileName = txtDocumentName.Text.Trim();
        this.CurrentDocument.DocumentNote = txtDocumentNote.Text.Trim();

        if (fileUpload.HasFile)
        {
            this.CurrentDocument.FileSizeKB = fileUpload.PostedFile.ContentLength / 1024;
            this.CurrentDocument.FileExtension = Path.GetExtension(fileUpload.PostedFile.FileName).Replace(".", "");
            byte[] bytes = new byte[fileUpload.PostedFile.InputStream.Length];
            fileUpload.PostedFile.InputStream.Read(bytes, 0, (int)fileUpload.PostedFile.InputStream.Length);
            this.CurrentDocument.FileData = bytes;

            if (string.IsNullOrEmpty(this.CurrentDocument.DocumentFileName))
            {
                this.CurrentDocument.DocumentFileName = Path.GetFileNameWithoutExtension(fileUpload.PostedFile.FileName);
            }
        }

        CscmsContext.SaveChanges();


        return true;
    }

    protected void btnSaveAndAdd_Click(object sender, EventArgs e)
    {
        if (UpdateDocument())
        {
            Response.Redirect(string.Format("DocumentDetail.aspx?cid={0}&dd={1}",
                Request.QueryString["cid"],
                Server.UrlEncode(dtDocumentDate.SelectedDate.ToString())), true);
        }
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.{1}", this.CurrentDocument.DocumentFileName, this.CurrentDocument.FileExtension));
        Response.BinaryWrite(this.CurrentDocument.FileData.ToArray());
        Response.Flush();
        Response.End();
    }
    protected void btnSaveAndReturnToCase_Click(object sender, EventArgs e)
    {
        if (UpdateDocument())
        {
            Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=SaveDocument", true);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (UpdateDocument())
        {
            trFileUpload.Visible = false;
            revFileUpload.Enabled = false;
            this.NewDocument = false;
            LoadDocument(this.CurrentDocument.DocumentID);
            PopulateForm();
            messageBox.SuccessMessage = "Document saved successfully.";
        }
    }

    private void PopulateForm()
    {
        dtDocumentDate.MaxDate = DateTime.Today;
        dtDocumentDate.SelectedDate = this.CurrentDocument.DocumentDate != DateTime.MinValue ? this.CurrentDocument.DocumentDate : (DateTime?)null;
        ddlDocumentType.SelectedValue = this.CurrentDocument.DocumentType != null ? this.CurrentDocument.DocumentType.DocumentTypeID.ToString() : "";

        if (!NewDocument)
        {
            txtDocumentName.Text = this.CurrentDocument.DocumentFileName.Trim();
            txtDocumentNote.Text = this.CurrentDocument.DocumentNote;
            btnDownload.Text = string.Format("{0}.{1} ({2:0,0} KB)", this.CurrentDocument.DocumentFileName, this.CurrentDocument.FileExtension, this.CurrentDocument.FileSizeKB);
        }

        SetFieldVisibility();
    }

    private void SetFieldVisibility()
    {
        if (!NewDocument)
        {
            btnDelete.Visible = true;
            divDocumentButtons.Visible = true;
            //sectionActions.Visible = true;
            divMain.Attributes["class"] = "existingCase";
            divMainSub.Attributes["class"] = "existingCaseSub";
            trDownload.Visible = true;
            tdFileUpload.Attributes["class"] = "fieldLabel";
            rfvFileUpload.Enabled = false;
        }

        if (Request.QueryString["return"] == "c")
        {
            btnDelete.Visible = false;
            divDocumentButtons.Visible = false;
            btnSaveDocument.Visible = false;
            btnSaveDocumentAndAdd.Visible = false;
            //btnSaveAndReturnToCase.Visible = true && (!this.CurrentUser.ViewAllUser) || (Convert.ToBoolean(Session["Impersonating"]) == true);
        }
    }

    private void LoadDocument(int documentID)
    {
        //TODO: Check to make sure the current user has access to this document
        this.CurrentDocument = CscmsContext.Document.Include("Action").Where(it => it.DocumentID == documentID).FirstOrDefault();

        if (this.CurrentDocument.Action.ActionGUID != this.CurrentAction.ActionGUID)
        {
            CurrentDocument = null;
            Response.Redirect("~/AnonPages/Unauthorized.aspx", true);
        }

        divCaseNotes.Visible = true;
    }

    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                CommandType.StoredProcedure,
                "DeleteActionDocument",
                new SqlParameter("@ActionGUID", this.CurrentDocument.Action.ActionGUID.ToString()),
                new SqlParameter("@DocumentID", this.CurrentDocument.DocumentID));
        this.CurrentDocument = null;
        Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=DeleteDocument", true);
    }
    #endregion

}