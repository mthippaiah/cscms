﻿<%@ Page Title="CSCMS | Case Search" Language="C#" MasterPageFile="~/CSCMS.master"
    AutoEventWireup="true" CodeFile="CauseSearch.aspx.cs" Inherits="MemberPages_CauseSearch" %>

<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/PersonSearch.ascx" TagName="PersonSearch" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/CauseSearch.ascx" TagName="CauseSearch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script language="javascript" type="text/javascript">
        var csjava = new CauseSearch();
        function RunPageValidation() {
            if (Page_ClientValidate("AddNew"))
                return true;
            else
                return false;
        }
        function createHFObjects() {
            var vNCP = document.getElementById("<%= HFNCPPersonGUID.ClientID %>");
            var vCP = document.getElementById("<%= HFCPPersonGUID.ClientID %>");
            var vChild1 = document.getElementById("<%= HFChild1PersonGUID.ClientID %>");
            var vChild2 = document.getElementById("<%= HFChild2PersonGUID.ClientID %>");
            var vChild3 = document.getElementById("<%= HFChild3PersonGUID.ClientID %>");
            var vChild4 = document.getElementById("<%= HFChild4PersonGUID.ClientID %>");
            var vP1 = document.getElementById("<%= hfP1.ClientID %>");
            var vP2 = document.getElementById("<%= hfP2.ClientID %>");
            return { vNCP: vNCP, vCP: vCP, vChild1: vChild1, vChild2: vChild2, vChild3: vChild3, vChild4: vChild4, vP1:vP1, vP2:vP2 };
        }
        function createNcpObjects() {
            var vncpFn = document.getElementById("<%= txtNCPFirst.ClientID %>");
            var vncpLn = document.getElementById("<%= txtNCPLast.ClientID %>");
            var vncpMn = document.getElementById("<%= txtNCPMiddle.ClientID %>");
            var vncpSx = document.getElementById("<%= txtNCPSuffix.ClientID %>");
            var vncpPersonType = document.getElementById("<%= ddlNCPFatherMother.ClientID %>");
            var vncpSex = document.getElementById("<%= ddlNCPSex.ClientID %>");
            var vbdate = $find("<%= dtNCPBirthDate.ClientID %>");
            return { vncpFn: vncpFn, vncpLn: vncpLn, vncpMn: vncpMn, vncpSx: vncpSx, vncpPersonType: vncpPersonType, vncpSex: vncpSex, vbdate: vbdate };
        }
        function createCpObjects() {
            var vcpFn = document.getElementById("<%= txtCPFirst.ClientID %>");
            var vcpLn = document.getElementById("<%= txtCPLast.ClientID %>");
            var vcpMn = document.getElementById("<%= txtCPMiddle.ClientID %>");
            var vcpSx = document.getElementById("<%= txtCPSuffix.ClientID %>");
            var vcpSex = document.getElementById("<%= ddlCPSex.ClientID %>");
            var vbdate = $find("<%= dtCPBirthDate.ClientID %>");
            var vcpnp = document.getElementById("<%= ddlCPNPFlag.ClientID %>");
            return { vcpFn: vcpFn, vcpLn: vcpLn, vcpMn: vcpMn, vcpSx: vcpSx, vcpSex: vcpSex, vbdate: vbdate, vcpnp: vcpnp };
        }
        function createChild1Objects() {
            var vc1Fn = document.getElementById("<%= txtChild1First.ClientID %>");
            var vc1Ln = document.getElementById("<%= txtChild1Last.ClientID %>");
            var vc1Mn = document.getElementById("<%= txtChild1Middle.ClientID %>");
            var vc1Sx = document.getElementById("<%= txtChild1Suffix.ClientID %>");
            var vc1Sex = document.getElementById("<%= ddlChild1Sex.ClientID %>");
            var vbdate = $find("<%= dtChild1BirthDate.ClientID %>");
            return { vc1Fn: vc1Fn, vc1Ln: vc1Ln, vc1Mn: vc1Mn, vc1Sx: vc1Sx, vc1Sex: vc1Sex, vbdate: vbdate };
        }
        function createChild2Objects() {
            var vc2Fn = document.getElementById("<%= txtChild2First.ClientID %>");
            var vc2Ln = document.getElementById("<%= txtChild2Last.ClientID %>");
            var vc2Mn = document.getElementById("<%= txtChild2Middle.ClientID %>");
            var vc2Sx = document.getElementById("<%= txtChild2Suffix.ClientID %>");
            var vc2Sex = document.getElementById("<%= ddlChild2Sex.ClientID %>");
            var vbdate = $find("<%= dtChild2BirthDate.ClientID %>");
            return { vc2Fn: vc2Fn, vc2Ln: vc2Ln, vc2Mn: vc2Mn, vc2Sx: vc2Sx, vc2Sex: vc2Sex, vbdate: vbdate };
        }
        function createChild3Objects() {
            var vc3Fn = document.getElementById("<%= txtChild3First.ClientID %>");
            var vc3Ln = document.getElementById("<%= txtChild3Last.ClientID %>");
            var vc3Mn = document.getElementById("<%= txtChild3Middle.ClientID %>");
            var vc3Sx = document.getElementById("<%= txtChild3Suffix.ClientID %>");
            var vc3Sex = document.getElementById("<%= ddlChild3Sex.ClientID %>");
            var vbdate = $find("<%= dtChild3BirthDate.ClientID %>");
            return { vc3Fn: vc3Fn, vc3Ln: vc3Ln, vc3Mn: vc3Mn, vc3Sx: vc3Sx, vc3Sex: vc3Sex, vbdate: vbdate };
        }
        function createChild4Objects() {
            var vc4Fn = document.getElementById("<%= txtChild4First.ClientID %>");
            var vc4Ln = document.getElementById("<%= txtChild4Last.ClientID %>");
            var vc4Mn = document.getElementById("<%= txtChild4Middle.ClientID %>");
            var vc4Sx = document.getElementById("<%= txtChild4Suffix.ClientID %>");
            var vc4Sex = document.getElementById("<%= ddlChild4Sex.ClientID %>");
            var vbdate = $find("<%= dtChild4BirthDate.ClientID %>");
            return { vc4Fn: vc4Fn, vc4Ln: vc4Ln, vc4Mn: vc4Mn, vc4Sx: vc4Sx, vc4Sex: vc4Sex, vbdate: vbdate };
        }
        function createP1Objects() {
            var vP1Fn = document.getElementById("<%= txtP1First.ClientID %>");
            var vP1Ln = document.getElementById("<%= txtP1Last.ClientID %>");
            var vP1Mn = document.getElementById("<%= txtP1Middle.ClientID %>");
            var vP1Sx = document.getElementById("<%= txtP1Suffix.ClientID %>");
            var vP1PersonType = document.getElementById("<%= ddlP1FatherMother.ClientID %>");
            var vP1Sex = document.getElementById("<%= ddlP1Sex.ClientID %>");
            var vbdate = $find("<%= dtP1BirthDate.ClientID %>");
            return { vP1Fn: vP1Fn, vP1Ln: vP1Ln, vP1Mn: vP1Mn, vP1Sx: vP1Sx, vP1PersonType: vP1PersonType, vP1Sex: vP1Sex, vbdate: vbdate };
        }
        function createP2Objects() {
            var vP2Fn = document.getElementById("<%= txtP2First.ClientID %>");
            var vP2Ln = document.getElementById("<%= txtP2Last.ClientID %>");
            var vP2Mn = document.getElementById("<%= txtP2Middle.ClientID %>");
            var vP2Sx = document.getElementById("<%= txtP2Suffix.ClientID %>");
            var vP2PersonType = document.getElementById("<%= ddlP2FatherMother.ClientID %>");
            var vP2Sex = document.getElementById("<%= ddlP2Sex.ClientID %>");
            var vbdate = $find("<%= dtP2BirthDate.ClientID %>");
            return { vP2Fn: vP2Fn, vP2Ln: vP2Ln, vP2Mn: vP2Mn, vP2Sx: vP2Sx, vP2PersonType: vP2PersonType, vP2Sex: vP2Sex, vbdate: vbdate };
        }
        function createDispObjects() {
            var dsdt = $find("<%= dtDispositionSignedDate.ClientID %>");
            var drdt = $find("<%= dtDispositionRenderedDate.ClientID %>");
            var d = document.getElementById("<%= ddlDisposition.ClientID %>");
            var dd = document.getElementById("<%= ddlDispositionDetail.ClientID %>");
            return {dsdt:dsdt, drdt:drdt, d:d, dd:dd};
        }
        function VerifyNewCasePersons() {
            //debugger;
            //This validation doesn't seem to work. meaning, after this, it doesn't fire OnClik event
            //So I am goin with server side validationfor now
            var txtNCPLast = document.getElementById("<%= txtNCPLast.ClientID %>");
            var txtNCPFirst = document.getElementById("<%= txtNCPFirst.ClientID %>");

            if (txtNCPFirst.value != "" && txtNCPLast.value == "") {
                alert("Last name is a required value for NCP");
                return false;
            }

            return true;
        }
        function HidePopup() {
            $find('PersonMPE').hide();
            return false;
        }
    </script>

    <style type="text/css">
        .ncPersonN
        {
            width: 80px;
        }
        .ncDateCol
        {
            width: 50px;
        }
        .ncPType
        {
            width: 70px;
        }
        .ncPersonSx
        {
            width: 30px;
        }
        .rowLabel
        {
            width: 45px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <!-- messageBox -->
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <!-- pageTitle -->
    <div class="pageTitle" style="float: left;">
        <asp:Label runat="server" ID="lblPageLabel" Text="Case Search"></asp:Label>
    </div>
    <asp:HiddenField ID="HFSearchDataSource" runat="server" />
    <br />
    <br />
    <div id="divCaseSearch" runat="server">
        <uc1:CauseSearch ID="causeSearch" runat="server" OnCauseSearchResult="HandleCauseSearchResult" />
    </div>
    <!-- ADD NEW CASE -->
    <CSCMSUC:SectionContainer ID="sectionCase" runat="server" HeaderText="ADD NEW CASE"
        Width="100%">
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSave">
            <table width="100%">
                <tr>
                    <td valign="top" width="50%">
                        <table cellspacing="1" width="100%">
                            <tr>
                                <td class="fieldLabelReq">
                                    Action Type:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlActionTypeNew" runat="server" Width="200" DataSourceID="dsActionType"
                                        DataValueField="ActionTypeID" DataTextField="ActionTypeName" AppendDataBoundItems="true"
                                        onChange="csjava.ActionTypeChange(this);">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlActionType" runat="server" ControlToValidate="ddlActionTypeNew"
                                        ValidationGroup="AddNew" ErrorMessage="Please select an Action Type">*</asp:RequiredFieldValidator>
                                    <asp:SqlDataSource ID="dsActionType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="SELECT ActionTypeID, ActionTypeName FROM ActionType order by SortOrder">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabelReq">
                                    County:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCountyNew" runat="server" Width="200" DataSourceID="dsCounty"
                                        DataValueField="CountyID" DataTextField="CountyName" AppendDataBoundItems="true"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCountyNew_SelectedIndexChanged">
                                        <asp:ListItem Value="0">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlCounty" runat="server" ControlToValidate="ddlCountyNew"
                                        InitialValue="0" ValidationGroup="AddNew" ErrorMessage="Please select a County">*</asp:RequiredFieldValidator>
                                    <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="GetUserCounties" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                                        OnSelecting="dsCounty_Selecting">
                                        <SelectParameters>
                                            <asp:Parameter Name="UserId" Type="Int32" />
                                            <asp:Parameter Name="CourtID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabelReq">
                                    Cause Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNewCaseNumber" runat="server" Width="150" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="rfvCauseNumber" runat="server" ControlToValidate="txtNewCaseNumber"
                                        ValidationGroup="AddNew" ErrorMessage="Please enter a Cause Number">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Style:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNewCaseStyle" runat="server" Width="250" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Disposition Rendered Date:
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="dtDispositionRenderedDate" runat="server" MaxDate='<%# DateTime.Today %>'>
                                        <ClientEvents OnDateSelected="csjava.DispRDateSelected" />
                                    </telerik:RadDatePicker>
                                    <asp:RangeValidator ID="rvDispositionRenderedDate" runat="server" ControlToValidate="dtDispositionRenderedDate"
                                        ValidationGroup="AddNew" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Disposition Rendered Date must not be in the future">*</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Disposition Signed Date:
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="dtDispositionSignedDate" runat="server" MaxDate='<%# DateTime.Today %>' />
                                    <asp:RangeValidator ID="rvDispositionSignedDate" runat="server" ControlToValidate="dtDispositionSignedDate"
                                        ValidationGroup="AddNew" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Disposition Signed Date must not be in the future">*</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Disposition:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDisposition" runat="server" Width="200" DataSource='<%# GetDispositionTypes() %>'
                                        DataValueField="DispositionTypeID" DataTextField="DispositionTypeName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsDispositionType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="select DispositionTypeID, DispositionTypeName from DispositionType">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Disposition Detail
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDispositionDetail" runat="server" Width="200" DataSourceID="dsDispositionDetail"
                                        DataValueField="DispositionDetailID" DataTextField="DispositionDetailName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsDispositionDetail" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="select DispositionDetailID, DispositionDetailName from DispositionDetail">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" width="50%">
                        <table cellspacing="3" width="100%">
                            <tr>
                                <td class="fieldLabel">
                                    OAG Unit:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOAGOffice" runat="server" Width="100" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    OAG #:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOAG1" runat="server" Width="70" MaxLength="10" />&nbsp;
                                    <asp:TextBox ID="txtOAG2" runat="server" Width="70" MaxLength="10" />
                                    <asp:TextBox ID="txtOAG3" runat="server" Width="70" MaxLength="10" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabelReq">
                                    Filing Date:
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="dtFilingDate" runat="server" MaxDate='<%# DateTime.Today %>'
                                        SelectedDate='<%# DateTime.Today %>' />
                                    <asp:RequiredFieldValidator ID="rfvFilingDate" runat="server" ControlToValidate="dtFilingDate"
                                        ValidationGroup="AddNew" ErrorMessage="Please enter a Filing Date">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Originating Court:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlReferringCourt" runat="server" Width="250" DataValueField="OriginatingCourtID"
                                        DataTextField="CourtName" AppendDataBoundItems="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr align="left">
                                <td colspan="2">
                                    <telerik:RadToolTip ID="hearingDateTooltip" runat="server" Width="400" Skin="Hay"
                                        Animation="Slide" Position="TopCenter" Visible="false" AutoCloseDelay="10000"
                                        RelativeTo="Element" IsClientID="true" />
                                    &nbsp;
                                    <asp:Label ID="lblgrdHearings" runat="server" Text="Hearings on Same Date:" Font-Bold="true"
                                        EnableViewState="true" />
                                </td>
                            </tr>
                            <tr align="left">
                                <td class="fieldLabel" valign="top" style="padding-top: 4px;">
                                    <asp:Label ID="lblHearingDate" runat="server">Hearing Date:</asp:Label>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <telerik:RadDatePicker ID="dtHearingDate" runat="server" AutoPostBack="true" OnSelectedDateChanged="dtHearingDate_SelectedDateChanged" />
                                            </td>
                                            <td>
                                                <telerik:RadTimePicker ID="hearingTime" runat="server" Skin="Office2007" Width="75px">
                                                    <DateInput ID="DateInput1" runat="server" Font-Size="11px" Font-Names="Arial" />
                                                    <TimeView ID="TimeView1" runat="server" Skin="Office2007" StartTime="07:00:00" Interval="00:15:00"
                                                        EndTime="18:00:00" Columns="4" />
                                                    <TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
                                                </telerik:RadTimePicker>
                                                &nbsp;
                                                <asp:CompareValidator ID="cvdtHearingDate" runat="server" ValidationGroup="AddNew"
                                                    ErrorMessage="Hearing Date must be less than or equal Filing Date." ControlToValidate="dtHearingDate"
                                                    ControlToCompare="dtFilingDate" Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr align="left">
                                <td colspan="2" align="left" style="text-align: left">
                                    <b>Hearing Note:</b>
                                    <asp:TextBox ID="txtHearingNote" runat="server" Width="380" Height="80" TextMode="MultiLine" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="1px" cellpadding="0px">
                <tr>
                    <td colspan="11" style="padding-top: 3px">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        &nbsp
                    </td>
                    <td class="ncPersonN">
                        Last Name
                    </td>
                    <td class="ncPersonN">
                        First Name
                    </td>
                    <td class="ncPersonN">
                        Middle Name
                    </td>
                    <td class="ncPersonSx">
                        Suffix
                    </td>
                    <td class="ncDateCol">
                        Svc Date
                    </td>
                    <td class="ncPType">
                        Svc Type
                    </td>
                    <td class="ncDateCol">
                        Birth Date
                    </td>
                    <td>
                        NP
                    </td>
                    <td class="ncPType">
                        Person Type
                    </td>
                    <td>
                        Sex
                    </td>
                </tr>
                <tr>
                    <td colspan="11" style="padding-top: 3px">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        NCP
                        <asp:HiddenField ID="HFNCPPersonGUID" runat="server" />
                    </td>
                    <td class="ncPersonN">
                        <asp:TextBox ID="txtNCPLast" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td class="ncPersonN">
                        <asp:TextBox ID="txtNCPFirst" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td class="ncPersonN">
                        <asp:TextBox ID="txtNCPMiddle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td class="ncPersonSx">
                        <asp:TextBox ID="txtNCPSuffix" runat="server" MaxLength="50" CssClass="ncPersonSx"></asp:TextBox>
                    </td>
                    <td class="ncDateCol">
                        <telerik:RadDatePicker ID="dtNCPSvcDate" runat="server" CssClass="ncDateCol" />
                    </td>
                    <td class="ncPType">
                        <asp:DropDownList ID="ddlNCPServiceType" runat="server" CssClass="ncPType" DataSourceID="dsServiceTypes"
                            DataValueField="ServiceTypeID" DataTextField="ServiceTypeName" AppendDataBoundItems="true">
                            <asp:ListItem Value="">- Select -</asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="dsServiceTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                            SelectCommand="select ServiceTypeID, ServiceTypeName from ServiceType"></asp:SqlDataSource>
                    </td>
                    <td class="ncDateCol">
                        <telerik:RadDatePicker ID="dtNCPBirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlNCPNPFlag" runat="server">
                            <asp:ListItem Value="1">NP</asp:ListItem>
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="ncPType">
                        <asp:DropDownList ID="ddlNCPFatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="2">Father</asp:ListItem>
                            <asp:ListItem Value="1">Mother</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlNCPSex" runat="server">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnNCPPersonSearch" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearNCP" runat="server" Text="Clear" CssClass="submitMedium"
                            OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        CP
                        <asp:HiddenField ID="HFCPPersonGUID" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCPLast" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCPFirst" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCPMiddle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCPSuffix" runat="server" MaxLength="50" CssClass="ncPersonSx"></asp:TextBox>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtCPSvcdate" runat="server" CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCPServiceType" runat="server" CssClass="ncPType" DataSourceID="dsServiceTypes"
                            DataValueField="ServiceTypeID" DataTextField="ServiceTypeName" AppendDataBoundItems="true">
                            <asp:ListItem Value="">- Select -</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtCPBirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCPNPFlag" runat="server">
                            <asp:ListItem Value="1">NP</asp:ListItem>
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCPFatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="2">Father</asp:ListItem>
                            <asp:ListItem Value="5">GrandFather</asp:ListItem>
                            <asp:ListItem Value="4">GrandMother</asp:ListItem>
                            <asp:ListItem Value="1" Selected="True">Mother</asp:ListItem>
                            <asp:ListItem Value="7">Other</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCPSex" runat="server">
                            <asp:ListItem Value="F">Female</asp:ListItem>
                            <asp:ListItem Value="M">Male</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnCPPersonSearch" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearCP" runat="server" Text="Clear" CssClass="submitMedium" OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        Child1
                        <asp:HiddenField ID="HFChild1PersonGUID" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild1Last" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild1First" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild1Middle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild1Suffix" runat="server" MaxLength="50" CssClass="ncPersonSx"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtChild1BirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild1NPFlag" runat="server">
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild1FatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="3">Child</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild1Sex" runat="server">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnChild1PersonSearch" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearChild1" runat="server" Text="Clear" CssClass="submitMedium"
                            OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        Child2
                        <asp:HiddenField ID="HFChild2PersonGUID" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild2Last" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild2First" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild2Middle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild2Suffix" runat="server" CssClass="ncPersonSx" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtChild2BirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild2NPFlag" runat="server">
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild2FatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="3">Child</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild2Sex" runat="server">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnChild2PersonSearch" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearChild2" runat="server" Text="Clear" CssClass="submitMedium"
                            OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        Child3
                        <asp:HiddenField ID="HFChild3PersonGUID" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild3Last" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild3First" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild3Middle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild3Suffix" runat="server" CssClass="ncPersonSx" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtChild3BirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild3NPFlag" runat="server">
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild3FatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="3">Child</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild3Sex" runat="server">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnChild3PersonSearch" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearChild3" runat="server" Text="Clear" CssClass="submitMedium"
                            OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        Child4
                        <asp:HiddenField ID="HFChild4PersonGUID" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild4Last" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild4First" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild4Middle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtChild4Suffix" runat="server" CssClass="ncPersonSx" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtChild4BirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild4NPFlag" runat="server">
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild4FatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="3">Child</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlChild4Sex" runat="server">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnChild4PersonSearch" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearChild4" runat="server" Text="Clear" CssClass="submitMedium"
                            OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        <asp:DropDownList ID="ddlP1CustodialType" runat="server" CssClass="rowLabel">
                            <asp:ListItem Value="" Selected="True">- Select -</asp:ListItem>
                            <asp:ListItem Value="0">CP</asp:ListItem>
                            <asp:ListItem Value="1">NCP</asp:ListItem>
                        </asp:DropDownList>
                        <asp:HiddenField ID="hfP1" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtP1Last" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtP1First" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtP1Middle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtP1Suffix" runat="server" CssClass="ncPersonSx" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtP1Svcdate" runat="server" CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP1SvcType" runat="server" CssClass="ncPType" DataSourceID="dsServiceTypes"
                            DataValueField="ServiceTypeID" DataTextField="ServiceTypeName" AppendDataBoundItems="true">
                            <asp:ListItem Value="">- Select -</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtP1BirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP1NPFlag" runat="server">
                            <asp:ListItem Value="1">NP</asp:ListItem>
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP1FatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="0" Selected="True">- Select -</asp:ListItem>
                            <asp:ListItem Value="3">Child</asp:ListItem>
                            <asp:ListItem Value="2">Father</asp:ListItem>
                            <asp:ListItem Value="5">GrandFather</asp:ListItem>
                            <asp:ListItem Value="4">GrandMother</asp:ListItem>
                            <asp:ListItem Value="1">Mother</asp:ListItem>
                            <asp:ListItem Value="7">Other</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP1Sex" runat="server">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnP1Search" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearP1" runat="server" Text="Clear" CssClass="submitMedium" OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td class="rowLabel">
                        <asp:DropDownList ID="ddlP2CustodialType" runat="server" CssClass="rowLabel">
                            <asp:ListItem Value="" Selected="True">- Select -</asp:ListItem>
                            <asp:ListItem Value="0">CP</asp:ListItem>
                        </asp:DropDownList>
                        <asp:HiddenField ID="hfP2" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtP2Last" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtP2First" runat="server" CssClass="ncPersonN" MaxLength="50" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtP2Middle" runat="server" CssClass="ncPersonN" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="txtP2Suffix" runat="server" CssClass="ncPersonSx" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtP2Svcdate" runat="server" CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP2SvcType" runat="server" CssClass="ncPType" DataSourceID="dsServiceTypes"
                            DataValueField="ServiceTypeID" DataTextField="ServiceTypeName" AppendDataBoundItems="true">
                            <asp:ListItem Value="">- Select -</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="dtP2BirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                            CssClass="ncDateCol" />
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP2NPFlag" runat="server">
                            <asp:ListItem Value="1">NP</asp:ListItem>
                            <asp:ListItem Value="0">None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP2FatherMother" runat="server" CssClass="ncPType">
                            <asp:ListItem Value="0" Selected="True">- Select -</asp:ListItem>
                            <asp:ListItem Value="3">Child</asp:ListItem>
                            <asp:ListItem Value="2">Father</asp:ListItem>
                            <asp:ListItem Value="5">GrandFather</asp:ListItem>
                            <asp:ListItem Value="4">GrandMother</asp:ListItem>
                            <asp:ListItem Value="1">Mother</asp:ListItem>
                            <asp:ListItem Value="7">Other</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlP2Sex" runat="server">
                            <asp:ListItem Value="M">Male</asp:ListItem>
                            <asp:ListItem Value="F">Female</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="btnP2Search" runat="server" Text="Search" CssClass="submitMedium"
                            OnClientClick="return csjava.ShowSearchWindow(this);" />
                        <asp:Button ID="btnClearP2" runat="server" Text="Clear" CssClass="submitMedium" OnClientClick="return csjava.Clear(this);" />
                    </td>
                </tr>
                <tr>
                    <td colspan="11">
                        <asp:ModalPopupExtender ID="PersonMPE" runat="server" TargetControlID="btnNCPPersonSearch"
                            PopupControlID="ModPop" BackgroundCssClass="modalBackground" PopupDragHandleControlID="divpopupheader1"
                            BehaviorID="PersonMPE" RepositionMode="None">
                        </asp:ModalPopupExtender>
                        <asp:Panel ID="ModPop" runat="server" Style="display: none" BackColor="Bisque" BorderStyle="Outset"
                            Height="500" Width="495" HorizontalAlign="Center">
                            <div id="divpopupheader1" style="border-bottom-style: Outset; text-align: center;
                                background-color: Gray">
                                Person Search
                            </div>
                            <div id="divpopid1">
                                <uc1:PersonSearch ID="personSearch" runat="server" OnSearchClicked="HandleSearchClickedEvent"
                                    OnSearchPageChanged="HandlePeasonSearchPageChange" ParentPageName="CauseSearch" />
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="11" align="center" style="padding-top: 5px; padding-bottom: 5px;">
                        <asp:Button ID="btnSave" runat="server" Text="Create New Case" CssClass="submit"
                            CausesValidation="true" ValidationGroup="AddNew" OnClick="btnSave_Click" OnClientClick="if(!csjava.ValidateSaveNewCase()) return false;" />
                        <asp:Button ID="btnCreateAnother" runat="server" Text="Save &amp; Create Another" CssClass="submit"
                            CausesValidation="true" ValidationGroup="AddNew" OnClick="btnSaveAndAddAnother_Click" OnClientClick="if(!csjava.ValidateSaveNewCase()) return false;" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </CSCMSUC:SectionContainer>
    <!-- ajaxManager -->
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="sectionCase" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="messageBox" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlCountyNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dsReferringCourt" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dtHearingDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dtHearingDate" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="hearingDateTooltip" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="lblgrdHearings" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdCases">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdCases" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <!-- loadingPanel -->
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 80px" />
    </telerik:RadAjaxLoadingPanel>
    <br />
    <!-- grdCases -->
    <telerik:RadGrid ID="grdCases" runat="server" AllowPaging="True" PageSize="10" AllowSorting="True"
        Visible="false" OnExcelMLExportStylesCreated="grdCases_ExcelMLExportStylesCreated"
        OnPageIndexChanged="grdCases_OnPageIndexChanged" OnSortCommand="grdCases_OnSortCommand">
        <MasterTableView NoMasterRecordsText="Your search found no results. Try broadening your search criteria."
            DataKeyNames="CauseGUID, NumberOfAction,Style ">
            <Columns>
                <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, NumberOfAction"
                    DataNavigateUrlFormatString="ActionDetail.aspx?cid={0}&a={1}" SortExpression="CauseNumber"
                    DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                <telerik:GridBoundColumn HeaderText="OAG" DataField="OAGCauseNumber" />
                <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                <telerik:GridBoundColumn HeaderText="Action" DataField="ActionTypeName" />
                <telerik:GridBoundColumn HeaderText="Last Filing" DataField="LastActionFiledDate"
                    DataFormatString="{0:MM/dd/yyyy}" />
                <telerik:GridBoundColumn HeaderText="Action" DataField="ActionNumber" />
                <telerik:GridBoundColumn HeaderText="Status" DataField="CauseStatus" />
            </Columns>
        </MasterTableView>
        <PagerStyle AlwaysVisible="true" />
    </telerik:RadGrid>
    <br />
    <br />
    <!-- btnExportAttorney -->
    <div style="float: right; padding-bottom: 5px;">
        <asp:Button ID="btnExportCase" runat="server" Text="Export to Excel" CssClass="submitMedium"
            Visible="false" Width="130" CausesValidation="false" OnClick="btnExportCase_Click" />
    </div>
    <br />
</asp:Content>
