﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="CaseLoad.aspx.cs" Inherits="MemberPages_CaseLoad" %>

<%@ Register Src="../UserControls/CauseSearch.ascx" TagName="CauseSearch" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle" style="float: left;">
        <asp:Label runat="server" ID="lblPageLabel" Text="Case List"></asp:Label>
    </div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <div  visible="false">
        <uc1:CauseSearch ID="causeSearch" runat="server" OnCauseSearchResult="HandleCauseSearchResult" Visible="false" />
    </div>
    <CSCMSUC:SectionContainer ID="sectionResults" runat="server" HeaderText="Search Result"
        Width="100%">
        <div id="divSearchResult" runat="server">
            <telerik:RadGrid ID="grdCases" runat="server" AllowPaging="True" PageSize="10" AllowSorting="True"
                OnExcelMLExportStylesCreated="grdCases_ExcelMLExportStylesCreated" OnNeedDataSource="grdCases_OnNeedDataSource">
                <MasterTableView NoMasterRecordsText="Your search found no results. Try broadening your search criteria."
                    DataKeyNames="CauseGUID, NumberOfAction,Style ">
                    <Columns>
                        <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, NumberOfAction"
                            DataNavigateUrlFormatString="ActionDetail.aspx?cid={0}&a={1}" SortExpression="CauseNumber"
                            DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                        <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                        <telerik:GridBoundColumn HeaderText="OAG" DataField="OAGCauseNumber" />
                        <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                        <telerik:GridBoundColumn HeaderText="Action" DataField="ActionTypeName" />
                        <telerik:GridBoundColumn HeaderText="Last Filing" DataField="LastActionFiledDate"
                            DataFormatString="{0:MM/dd/yyyy}" />
                        <telerik:GridBoundColumn HeaderText="Action" DataField="ActionNumber" />
                        <telerik:GridBoundColumn HeaderText="Status" DataField="CauseStatus" Visible="false" />
                    </Columns>
                </MasterTableView>
                <PagerStyle AlwaysVisible="true" />
            </telerik:RadGrid>
        </div>
        <br />
        <br />
        <div style="float: right; padding-bottom: 5px;">
            <asp:Button ID="btnExportCase" runat="server" Text="Export to Excel" CssClass="submitMedium"
                Width="130" CausesValidation="false" OnClick="btnExportCase_Click" />
        </div>
    </CSCMSUC:SectionContainer>
</asp:Content>
