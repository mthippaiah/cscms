﻿<%@ Page Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="Calendar.aspx.cs"
    Inherits="MemberPages_Calendar" Title="CSCMS | Calendar" %>

<%@ Register Src="../UserControls/DateCalculator.ascx" TagName="DateCalculation"
    TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script language="javascript" type="text/javascript">
        var caljava = new Calendar();
        function OnClientNavigationCommand(sender, eventArgs) {
            var hf = document.getElementById("<%= hfCurrentView.ClientID %>");
            hf.value = eventArgs.get_command();
        }
    </script>

    <style type="text/css">
        .center
        {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <asp:HiddenField ID="hfCurrentView" runat="server" />
    <div style="width: 100%">
        <div class="pageTitle" style="float: left;width:90%;">
            CALENDAR</div>
        <div style="float: right; width: 10%; text-align: right; vertical-align: top">
            <asp:LinkButton ID="printScheduler" runat="server" Text="Print" OnClick="printScheduler_OnClick" Visible="true"></asp:LinkButton>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <table width="100%">
        <tr>
            <td width="80%">
                <uc1:DateCalculation ID="DateCalculation" runat="server" OnDateSetClicked="HandleDateSetClickedEvent" />
            </td>
            <td width="20%" align="right">
                <asp:CheckBox ID="chkTimeVisible" runat="server" Checked="False" Text="Allow Update Time"
                    TextAlign="Left" Visible="false" />
                &nbsp;
                <input id="btnManageDaysOff" type="button" runat="server" value="Manage Days Off"
                    class="submitMedium" style="width: 100px; vertical-align: middle" onclick="document.location.href = 'DaysOff.aspx'" />
                <asp:Button ID="btnRefresh" runat="server" Style="width: 50px; vertical-align: middle"
                    OnClick="btnRefresh_OnClick" Text="Refresh" CssClass="submitMedium" />
            </td>
        </tr>
    </table>
    <div style="float: right; padding-bottom: 5px; vertical-align: bottom;">
    </div>
    <CSCMSUC:SectionContainer ID="sectionCaseInitiationReason" runat="server" HeaderText="CALENDAR"
        Width="100%" DisplayType="Grid" Visible="true">
        <telerik:RadScheduler ID="scheduler" EnableViewState="false" runat="server" Skin="Office2007"
            ShowHoursColumn="false" HoursPanelTimeFormat="htt" ValidationGroup="scheduler"
            OverflowBehavior="Expand" ShowFooter="False" AllowInsert="False" AllowEdit="True"
            AllowDelete="False" ShowAllDayRow="False" DayStartTime="7:00:00" DayEndTime="18:00:00"
            TimelineView-UserSelectable="false" SelectedView="MonthView" FirstDayOfWeek="Monday"
            LastDayOfWeek="Friday" MinutesPerRow="5" ProviderName="SchedulerProvider" AdvancedForm-Enabled="false"
            MonthView-VisibleAppointmentsPerDay="2" OnClientAppointmentEditing="caljava.AppointmentEditing"
            OnAppointmentDataBound="scheduler_AppointmentDataBound" OnNavigationComplete="scheduler_NavigationComplete"
            OnTimeSlotCreated="scheduler_TimeSlotCreated" OnClientAppointmentMoveStart="caljava.AppointmentMoveStart"
            OnClientNavigationCommand="OnClientNavigationCommand">
            <Localization AdvancedAllDayEvent="All day"></Localization>
            <AdvancedForm DateFormat="M/d/yyyy" TimeFormat="h:mm tt"></AdvancedForm>
            <AppointmentTemplate>
                <table id="tbl" class="aptTable<%# Container.Appointment.Attributes["Color"] %>">
                    <tr>
                        <td id="Td1" width="30" runat="server" visible='<%# scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            <%# Container.Appointment.Attributes["Order"] %>
                        </td>
                        <td id="Td2" width="30" runat="server" visible='<%# (scheduler.SelectedView == SchedulerViewType.DayView && Container.Appointment.Attributes["HearingType"] != null && Container.Appointment.Attributes["HearingType"].ToString().StartsWith("Placement Review")) || (Session["ums"].ToString().StartsWith("y"))  %>'>
                            <input type="checkbox" name="chkApt" value='<%# Container.Appointment.ID %>' />
                        </td>
                        <td id="Td8" width="30" runat="server" visible='<%# (scheduler.SelectedView == SchedulerViewType.DayView && (Container.Appointment.Attributes["HearingType"] == null || !Container.Appointment.Attributes["HearingType"].ToString().StartsWith("Placement Review"))) && (Session["ums"].ToString().StartsWith("n"))  %>'>
                        </td>
                        <td id="Td3" width="80" runat="server" visible='<%# chkTimeVisible.Checked && scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            <telerik:RadTimePicker ID="hearingTime" runat="server" EnableViewState="false" Skin="Office2007"
                                Width="70px" HearingID='<%# Container.Appointment.ID %>' SelectedDate='<%# Container.Appointment.Attributes["HearingTime"] != "" ? DateTime.Today.AddTicks(long.Parse(Container.Appointment.Attributes["HearingTime"])) : (DateTime?)null %>'
                                AutoPostBack="true" OnSelectedDateChanged="hearingTime_SelectedDateChanged">
                                <DateInput Font-Size="10px" />
                                <TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:15:00" EndTime="18:00:00"
                                    Columns="4" />
                                <TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif"
                                    Visible='<%# (!this.CurrentUser.ViewAllUser || (Convert.ToBoolean(Session["Impersonating"]) == true)) %>' />
                            </telerik:RadTimePicker>
                        </td>
                        <td id="Td7" class="center" width="80" runat="server" visible='<%# !chkTimeVisible.Checked && scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            &#39;<%# Container.Appointment.Attributes["HearingTime"] != "" ? DateTime.Today.AddTicks(long.Parse(Container.Appointment.Attributes["HearingTime"])).ToShortTimeString() : string.Empty %>&#39;
                        </td>
                        <%--  <td id="Td4" width="30" runat="server" visible='<%# scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            <%# Container.Appointment.Resources[0].Key == "Morning" ? "AM" : "PM" %>
                        </td>--%>
                        <td width="80" class="center">
                            <%# Container.Appointment.Attributes["County"] %>
                        </td>
                        <td width="130" class="center">
                            <%# Container.Appointment.Subject %>
                        </td>
                        <td id="Td5" class="center" width="120" runat="server" visible='<%# scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            <%# Container.Appointment.Attributes["HearingType"] %>
                        </td>
                        <td id="Td6" class="center" width="270" runat="server" visible='<%# scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            <%# Container.Appointment.Attributes["Style"] %>
                        </td>
                        <td id="Td9" class="center" width="270" runat="server" visible='<%# scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            <%# Container.Appointment.Attributes["ActionTypeName"]%>
                        </td>
                        <td id="Td10" class="center" width="270" runat="server" visible='<%# scheduler.SelectedView == SchedulerViewType.DayView %>'>
                            <%# Container.Appointment.Attributes["DispositionDate"]%>
                        </td>
                    </tr>
                </table>
            </AppointmentTemplate>
        </telerik:RadScheduler>
    </CSCMSUC:SectionContainer>
    <CSCMSUC:SectionContainer ID="sectionAdditionalFunctions" runat="server" HeaderText="ADDITIONAL HEARING FUNCTIONS"
        Width="100%" DisplayType="Grid" Visible='<%# scheduler.SelectedView == SchedulerViewType.DayView %>'>
        <div style="float: left; padding-left: 20px; padding-bottom: 5px;">
            <table cellpadding="0" cellspacing="0" id="Table1" runat="server" visible="true">
                <tr>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Move Current Date Hearing(s) To Date:</b>
                        <telerik:RadDatePicker ID="dtMoveToDate" runat="server" DateInput-EmptyMessage="Enter date"
                            DateInput-MaxLength="10" PopupDirection="TopRight" />
                        <asp:RequiredFieldValidator ID="rfvMoveToDate" runat="server" ControlToValidate="dtMoveToDate"
                            Display="Dynamic" ValidationGroup="MoveHearings" ErrorMessage="Please select a move to hearing date">* Required</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button ID="btnMoveHearings" runat="server" Text="Move All Hearings" CssClass="submitMedium"
                            Width="150px" ValidationGroup="MoveHearings" OnClick="btnMoveHearings_Click" />
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="chkIgnoreDimmisalValidation" runat="server" Checked="False" Text="Ignore Dismissal Validation"
                            TextAlign="Left" Visible="false" />
                        &nbsp; &nbsp&nbsp<asp:HyperLink ID="lnkTurnOffMoveSelected" runat="server" Visible="false"
                            NavigateUrl='<%# "Calendar.aspx?usm=n" %>'>Turn Off Hearing Selection</asp:HyperLink>
                    </td>
                </tr>
            </table>
        </div>
    </CSCMSUC:SectionContainer>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnRefresh">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="scheduler" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="scheduler">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="scheduler" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="divButtons" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="messageBox" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="sectionAdditionalFunctions" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCopyHearings">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="scheduler" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="divButtons" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="messageBox" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="sectionAdditionalFunctions" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnMoveHearings">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="scheduler" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="divButtons" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="messageBox" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="sectionAdditionalFunctions" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 80px" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
