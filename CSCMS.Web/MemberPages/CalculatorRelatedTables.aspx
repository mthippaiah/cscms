﻿<%@ Page Title="CSCMS | Child Support Calculator Related Data" Language="C#" MasterPageFile="~/CSCMSPlain2.master"
    AutoEventWireup="true" CodeFile="CalculatorRelatedTables.aspx.cs" Inherits="MemberPages_CalculatorRelatedTables" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <CSCMSUC:SectionContainer ID="SectionContainer1" runat="server" HeaderText="Child Support Calculator Related Tables"
        Width="100%" DisplayType="Grid">
        <div style="float: right; padding-bottom: 5px; vertical-align: bottom;">
            <table>
                <tr>
                    <td align="right">
                        Select Effective Year:
                    </td>
                    <td align="right">
                        <asp:DropDownList ID="ddlCalendarYear" runat="server" DataSourceID="dsCalendarYear"
                            DataTextField="CalendarYear" DataValueField="CalendarYear" AutoPostBack="True"
                            Visible="true">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both;">
        </div>
        <telerik:RadGrid ID="grdEmployedPerson" runat="server" DataSourceID="dsEmployedPerson"
            AllowPaging="True" PageSize="10">
            <MasterTableView NoMasterRecordsText="No Records" DataKeyNames="EmployedPersonID"
                Caption="Employed Table">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="MonthlyGrossWages" DataField="MonthlyGrossWages "
                        SortExpression="MonthlyGrossWages " />
                    <telerik:GridBoundColumn HeaderText="OldAgeSurvivorsDisabilityInsuranceTaxes" DataField="OldAgeSurvivorsDisabilityInsuranceTaxes" />
                    <telerik:GridBoundColumn HeaderText="HospitalMedicareInsuranceTaxes" DataField="HospitalMedicareInsuranceTaxes" />
                    <telerik:GridBoundColumn HeaderText="FederalIncomeTaxes" DataField="FederalIncomeTaxes" />
                    <telerik:GridBoundColumn HeaderText="NetMonthlyIncome" DataField="NetMonthlyIncome " />
                    <telerik:GridBoundColumn HeaderText="AboveCap" DataField="AboveCap" />
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsEmployedPerson" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT * FROM EmployedPerson WHERE CalendarYear=@CalendarYear"
            CancelSelectOnNullParameter="false">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlCalendarYear" Name="CalendarYear" DefaultValue=""
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div style="clear: both;">
        </div>
        <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="dsSelfEmployedPerson"
            AllowPaging="True" PageSize="10">
            <MasterTableView NoMasterRecordsText="No Records" DataKeyNames="SelfEmployedID" Caption="Self-Employed Table">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="MonthlyGrossWages" DataField="MonthlyGrossWages "
                        SortExpression="MonthlyGrossWages " />
                    <telerik:GridBoundColumn HeaderText="OldAgeSurvivorsDisabilityInsuranceTaxes" DataField="OldAgeSurvivorsDisabilityInsuranceTaxes" />
                    <telerik:GridBoundColumn HeaderText="HospitalMedicareInsuranceTaxes" DataField="HospitalMedicareInsuranceTaxes" />
                    <telerik:GridBoundColumn HeaderText="FederalIncomeTaxes" DataField="FederalIncomeTaxes" />
                    <telerik:GridBoundColumn HeaderText="NetMonthlyIncome" DataField="NetMonthlyIncome " />
                    <telerik:GridBoundColumn HeaderText="AboveCap" DataField="AboveCap" />
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsSelfEmployedPerson" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT * FROM SelfEmployed WHERE CalendarYear=@CalendarYear" CancelSelectOnNullParameter="false">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlCalendarYear" Name="CalendarYear" DefaultValue=""
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div style="clear: both;">
        </div>
        <telerik:RadGrid ID="RadGrid2" runat="server" DataSourceID="dsPercentofNetResourcesID"
            AllowPaging="True" PageSize="10">
            <MasterTableView NoMasterRecordsText="No Records" DataKeyNames="PercentofNetResourcesID"
                Caption="Percent of Net Resources">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Number of Other Kids" DataField="ID " />
                    <telerik:GridBoundColumn HeaderText="OneKids" DataField="OneKids" />
                    <telerik:GridBoundColumn HeaderText="TwoKids" DataField="TwoKids" />
                    <telerik:GridBoundColumn HeaderText="ThreeKids" DataField="ThreeKids" />
                    <telerik:GridBoundColumn HeaderText="FourKids" DataField="FourKids " />
                    <telerik:GridBoundColumn HeaderText="FiveKids" DataField="FiveKids " />
                    <telerik:GridBoundColumn HeaderText="SixKids" DataField="SixKids " />
                    <telerik:GridBoundColumn HeaderText="SevenKids" DataField="SevenKids " />
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsPercentofNetResourcesID" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT * FROM PercentofNetResources WHERE BeginCalendarYear=@CalendarYear or EndCalendarYear=@CalendarYear"
            CancelSelectOnNullParameter="false">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlCalendarYear" Name="CalendarYear" DefaultValue=""
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsCalendarYear" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT DISTINCT CalendarYear FROM EmployedPerson ORDER BY CalendarYear DESC"
            CancelSelectOnNullParameter="False"></asp:SqlDataSource>
        <div style="clear: both;">
        </div>
        <div>
            <ul>
                <li>Child Support Calculation</li>
                <li></li>
                <li>Use the applicable Employed or Self-Employed tax table lookup to get the Net Monthly Income from Monthly Gross Income.</li>
                <li></li>
                <li>Add Monthly Other Nontaxable Income to the Net Monthly Income amount to get the Adjusted Net Monthly Income amount.</li><li></li>
                <li>If the Adjusted Net Monthly Income amount is greater than $7500, $7500 will be used as this is the legislatively imposed cap amount.</li><li></li>
                <li>Subtract the Monthly Health Care Cost from Adjusted Net Monthly Income to get the Total Net Monthly Income amount.</li>
                <li></li>
                <li>Use the Percent of Net Resources table to get the Child Support Percentage for Number of Children Before the Court (column) and Number of Other Children (row).</li>
                <li></li>
                <li>Total Monthly Net Income * Child Support Percentage = Monthly Calculated Child Support Amount.</li>
            </ul>
        </div>
    </CSCMSUC:SectionContainer>
</asp:Content>

