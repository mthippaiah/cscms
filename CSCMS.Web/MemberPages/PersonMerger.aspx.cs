﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

public partial class MemberPages_PersonMerger : BasePage
{
    #region [Page]
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (this.CurrentUser.ViewAllUser && Convert.ToBoolean(Session["Impersonating"]) == false) btnMerge.Visible = false;
        }
    }
    #endregion

    #region [Button]

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindLists();
    }

    private void BindLists()
    {
        DataSet results = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
          CommandType.StoredProcedure,
          "PersonMergeSearchPerson",
          new SqlParameter("@OCACourtID", this.CurrentUser.OCACourtID),
          new SqlParameter("@LastName", txtLastName.Text.Trim().Length > 0 ? txtLastName.Text.Trim() : null),
          new SqlParameter("@FirstName", txtFirstName.Text.Trim().Length > 0 ? txtFirstName.Text.Trim() : null),
          new SqlParameter("@PersonCategory", ddlPersonCategory.SelectedIndex > 0 ? byte.Parse(ddlPersonCategory.SelectedValue) : (byte?)null),
          new SqlParameter("@BirthDate", dtBirthdate.SelectedDate),
          new SqlParameter("@Sex", ddlSex.SelectedIndex > 0 ? ddlSex.SelectedValue[0] : (char?)null)
          );

        List<ListItem> items = new List<ListItem>();

        int counter = 1;

        foreach (DataRow result in results.Tables[0].Rows)
        {
            items.Add(new ListItem(counter.ToString() + ". " + Helper.GetFormattedName(Helper.EvaluateDbStringValue(result["FirstName"]),
                Helper.EvaluateDbStringValue(result["MiddleName"]),
                Helper.EvaluateDbStringValue(result["LastName"]),
                Helper.EvaluateDbStringValue(result["NameSuffix"])), result["PersonGUID"].ToString()));
            counter++;
        }

        personMergeA.SetPersonList(items);
        personMergeB.SetPersonList(items);

        if (counter == 1)
        {
            messageBox.ErrorMessage = "No person found.";
        }
        else
        {
            counter -= 1;
            messageBox.ErrorMessage = counter.ToString() + " person(s) found.";
        }
    }

    protected void btnMergeValidate_Click(object sender, EventArgs e)
    {
        grdCases.DataSourceID = this.dsPersons.ID;
        grdCases.DataBind();

        if (grdCases.Items.Count != 0) messageBox.SuccessMessage = "These two persons can not merge because both of them are on the same action."; ;
    }

    protected void btnMerge_Click(object sender, EventArgs e)
    {
        Guid personGuidToDelete;
        Guid personGuidToKeep;

        if (rbKeepA.Checked)
        {
            personGuidToKeep = personMergeA.PersonGuid;
            personGuidToDelete = personMergeB.PersonGuid;
        }
        else
        {
            personGuidToKeep = personMergeB.PersonGuid;
            personGuidToDelete = personMergeA.PersonGuid;
        }

        ValidateData(personGuidToDelete, personGuidToKeep);

        if (messageBox.ErrorMessage != string.Empty) return;

        try
        {
            Helper.ExecuteMergePersons(personGuidToDelete, personGuidToKeep);
            messageBox.SuccessMessage = "Persons Merged Successfully.";
            BindLists();
        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("PK_PersonCase"))
            {
                messageBox.ErrorMessage = "Unable to merge persons. These two people are associated with the same case(s). Please remove one of the persons from each case before merging. (Cases listed below)<br/><br/>";
                Helper.ExecuteFindActionWithTwoPersons(personGuidToDelete, personGuidToKeep);
                DataSet ds = Helper.ExecuteFindActionWithTwoPersons(personGuidToDelete, personGuidToKeep);
                foreach (DataRow c in ds.Tables[0].Rows)
                {
                    messageBox.ErrorMessage += string.Format("<a href='CaseSummary.aspx?cid={0}'>{1}</a><br/>", c["ActionNumber"], c["CauseNumber"]);
                }
            }
            else
            {
                messageBox.ErrorMessage = "Unable to merge persons. Technical details: " + ex.Message;
            }
        }

    }

    private void ValidateData(Guid personGuidToDelete, Guid personGuidToKeep)
    {
        messageBox.ErrorMessage = string.Empty;

        if (rbKeepA.Checked == false && rbKeepB.Checked == false)
        {
            messageBox.ErrorMessage = "You must check Keep A or Keep B.";
        }

        if (personGuidToKeep.ToString() == personGuidToDelete.ToString())
        {
            messageBox.ErrorMessage = "Can not merge same person.";
        }
        if (personGuidToKeep.ToString() == "00000000-0000-0000-0000-000000000000" || personGuidToDelete.ToString() == "00000000-0000-0000-0000-000000000000")
        {
            messageBox.ErrorMessage = "You must select two persons to merge.";
        }
    }

    #endregion

    #region [DataSet]

    protected void dsPersons_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        Guid personGuidToDelete;
        Guid personGuidToKeep;

        if (rbKeepA.Checked)
        {
            personGuidToKeep = personMergeA.PersonGuid;
            personGuidToDelete = personMergeB.PersonGuid;
        }
        else
        {
            personGuidToKeep = personMergeB.PersonGuid;
            personGuidToDelete = personMergeA.PersonGuid;
        }

        ValidateData(personGuidToDelete, personGuidToKeep);

        if (messageBox.ErrorMessage != string.Empty) return;

        e.Command.Parameters["@UserId"].Value = 1;  //  this.CurrentUser.UserID;
        e.Command.Parameters["@PersonGUID"].Value = personGuidToKeep.ToString();
        e.Command.Parameters["@PersonGUID2"].Value = personGuidToDelete.ToString();//  this.CurrentUser.UserID;
    }

    #endregion
}
