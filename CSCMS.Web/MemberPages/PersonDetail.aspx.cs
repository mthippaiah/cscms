﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI.Calendar;
public partial class MemberPages_PersonDetail : ActionBasePage
{
    #region [Properties]

    protected bool NewPersonCase
    {
        get { return Convert.ToBoolean(ViewState["NewPersonCase"]); }
        set { ViewState["NewPersonCase"] = value; }
    }
    protected bool NewPerson
    {
        get { return Convert.ToBoolean(ViewState["NewPerson"]); }
        set { ViewState["NewPerson"] = value; }
    }

    protected bool Child
    {
        get { return Convert.ToBoolean(ViewState["Child"]); }
        set { ViewState["Child"] = true; }
    }
    protected bool GrandFather
    {
        get { return Convert.ToBoolean(ViewState["GrandFather"]); }
        set { ViewState["GrandFather"] = true; }
    }
    protected bool GrandMother
    {
        get { return Convert.ToBoolean(ViewState["GrandMother"]); }
        set { ViewState["GrandMother"] = true; }
    }
    protected bool Father
    {
        get { return Convert.ToBoolean(ViewState["Father"]); }
        set { ViewState["Father"] = true; }
    }
    protected bool Mother
    {
        get { return Convert.ToBoolean(ViewState["Mother"]); }
        set { ViewState["Mother"] = true; }
    }
    //protected bool Respondent
    //{
    //    get { return Convert.ToBoolean(ViewState["Respondent"]); }
    //    set { ViewState["Respondent"] = true; }
    //}
    protected bool OtherOrRelative
    {
        get { return Convert.ToBoolean(ViewState["OtherOrRelative"]); }
        set { ViewState["OtherOrRelative"] = true; }
    }
    protected bool CaseAttorneyOrGAL
    {
        get { return Convert.ToBoolean(ViewState["CaseAttorney"]); }
        set { ViewState["CaseAttorney"] = true; }
    }

    protected bool PersonOnly
    {
        get { return Request.QueryString["po"] == "1"; }
    }

    #endregion

    #region [Page]
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        // Load UserControls
        actionInfo.CauseGUID = new Guid(Request.QueryString["cid"]);
        actionInfo.ActionNumber = short.Parse(Request.QueryString["a"]);
        base.OnInit(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        bool iOpenedConn = false;

        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (!IsPostBack)
            {
                rvdtOrderDate.MaximumValue = DateTime.Now.ToShortDateString();
                rvdtResultDate.MaximumValue = DateTime.Now.ToShortDateString();
                rvdtPaternityEstablishedDate.MaximumValue = DateTime.Now.ToShortDateString();
                LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));

                if (!string.IsNullOrEmpty(Request.QueryString["pid"]))
                {
                    this.NewPerson = false;
                    this.NewPersonCase = false;
                    this.btnPersonSearch.Visible = false;
                    this.btnClear.Visible = false;
                    LoadActionPerson(this.CurrentAction.ActionGUID, new Guid(Request.QueryString["pid"]));
                }
                else
                {
                    this.NewPerson = true;
                    this.NewPersonCase = true;
                    InitializePersonAction();
                }
                SetPersonTypes(); //Note: We load PersonTypes here.
                PopulateForm();

                this.DataBind();

                this.Form.DefaultFocus = txtLastName.ClientID;

                if (Request.QueryString["message"] == "SavePerson")
                {
                    messageBox.SuccessMessage = "Person was saved successfully.";
                }

                if (Request.QueryString["message"] == "SaveAndAddAnotherChild")
                {
                    messageBox.SuccessMessage = "Person was saved successfully.";
                }

            }
            else
            {
                //If it is a postback, we need to disable the controls that were populated for a person search
                if (!string.IsNullOrEmpty(this.HFPersonGUID.Value) && this.HFPersonGUID.Value != "Current")
                {
                    Guid id = new Guid(this.HFPersonGUID.Value);
                    CSCMS.Data.Person p = CscmsContext.Person.Include("IntepreterLanguage").Where(it => it.PersonGUID == id).FirstOrDefault();

                    this.txtFirstName.Text = p.FirstName;
                    this.txtLastName.Text = p.LastName;
                    this.txtMiddleName.Text = p.MiddleName;
                    this.txtSuffix.Text = p.NameSuffix;
                    this.ddlSex.SelectedIndex = (p.Sex == "M") ? 1 : 2;
                    this.dtBirthDate.SelectedDate = p.Birthdate;
                    this.chkInterpreterNeeded.Checked = p.InterpreterNeeded;

                    if (p.IntepreterLanguage != null)
                        this.ddlFirstLanguage.SelectedValue = p.IntepreterLanguage.IntepreterLanguageID.ToString();
                    else
                        this.ddlFirstLanguage.SelectedIndex = 0;

                    this.chkSecurityRisk.Checked = p.SecurityRisk;
                    this.chkDisability.Checked = p.Disability;

                    this.txtFirstName.Enabled = false;
                    this.txtLastName.Enabled = false;
                    this.txtMiddleName.Enabled = false;
                    this.txtSuffix.Enabled = false;
                    this.ddlSex.Enabled = false;
                    this.dtBirthDate.Enabled = false;
                    this.chkInterpreterNeeded.Enabled = false;
                    this.ddlFirstLanguage.Enabled = false;
                    this.chkSecurityRisk.Enabled = false;
                    this.chkDisability.Enabled = false;
                }
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }
    #endregion

    #region [Load Data]

    /// <summary>
    /// 
    /// </summary>
    /// <param name="birthDate"></param>
    private int CalculateAge(DateTime? birthDate)
    {
        int age = -1;
        if (birthDate.HasValue)
        {
            TimeSpan tsAge = DateTime.Today - birthDate.Value;
            DateTime dtAge = DateTime.MinValue.Add(tsAge);
            int year = dtAge.Year == 1 ? 0 : dtAge.Year - 1;
            int month = dtAge.Year == 1 && dtAge.Month == 1 ? 0 : dtAge.Month - 1;
            lblAge.Text = string.Format("{0} Yrs {1} Mos", year, month);
            age = year;
            if (this.Child && year >= 16)
            {
                lblAge.ForeColor = ColorTranslator.FromHtml("#DD0000");
                lblAge.Font.Bold = true;
            }
        }
        else
        {
            lblAge.Text = string.Empty;
        }
        return age;
    }

    /// <summary>
    /// 
    /// </summary>
    private void PopulateForm()
    {
        dtOrderDate.MaxDate = DateTime.Today;
        dtResultDate.MaxDate = DateTime.Today;
        dtPaternityEstablishedDate.MaxDate = DateTime.Today;
        txtFirstName.Text = this.CurrentPerson.FirstName;
        txtLastName.Text = this.CurrentPerson.LastName;
        txtMiddleName.Text = this.CurrentPerson.MiddleName;
        txtSuffix.Text = this.CurrentPerson.NameSuffix;
        txtBarCardNumber.Text = this.CurrentPerson.BarCardNumber.HasValue ? this.CurrentPerson.BarCardNumber.Value.ToString() : "";
        txtFirm.Text = this.CurrentPerson.Firm;
        txtAlias.Text = this.CurrentPerson.AliasName;
        dtBirthDate.SelectedDate = this.CurrentPerson.Birthdate;
        if (this.NewPerson)
        {
            if (this.Father || this.Mother)
                ddlSex.SelectedIndex = this.Father ? 1 : 2;

            if (this.GrandFather || this.GrandMother)
                ddlSex.SelectedIndex = this.GrandFather ? 1 : 2;
        }
        else
            ddlSex.SelectedValue = !string.IsNullOrEmpty(this.CurrentPerson.Sex) ? this.CurrentPerson.Sex.ToString() : "";

        if (this.CaseAttorneyOrGAL)
        {
            txtAddress1.Text = this.CurrentPerson.Address1;
            txtAddress2.Text = this.CurrentPerson.Address2;
            txtAddress3.Text = this.CurrentPerson.Address3;
            txtCity.Text = this.CurrentPerson.City;
            ddlState.Text = (this.CurrentPerson.State != null) ? this.CurrentPerson.State.StateID : "";
            txtZipCode.Text = this.CurrentPerson.ZipCode.HasValue ? this.CurrentPerson.ZipCode.ToString() : "";
            txtZipCodeExt.Text = this.CurrentPerson.ZipCodeExtension.HasValue ? this.CurrentPerson.ZipCodeExtension.ToString() : "";
            ddlCounty.SelectedValue = this.CurrentPerson.CountyID.HasValue ? this.CurrentPerson.CountyID.ToString() : "";
        }
        else
        {
            txtAddress1.Text = this.CurrentActionPerson.Address1;
            txtAddress2.Text = this.CurrentActionPerson.Address2;
            txtAddress3.Text = this.CurrentActionPerson.Address3;
            txtCity.Text = this.CurrentActionPerson.City;
            ddlState.Text = (this.CurrentActionPerson.State != null) ? this.CurrentActionPerson.State.StateID : "";
            txtZipCode.Text = this.CurrentActionPerson.ZipCode.HasValue ? this.CurrentActionPerson.ZipCode.ToString() : "";
            txtZipCodeExt.Text = this.CurrentActionPerson.ZipCodeExtension.HasValue ? this.CurrentActionPerson.ZipCodeExtension.ToString() : "";
            ddlCounty.SelectedValue = this.CurrentActionPerson.CountyID.HasValue ? this.CurrentActionPerson.CountyID.ToString() : "";

        }
        txtEmailAddress.Text = this.CurrentPerson.EmailAddress;
        chkInterpreterNeeded.Checked = this.CurrentPerson.InterpreterNeeded;

        ddlFirstLanguage.SelectedValue = (this.CurrentPerson.IntepreterLanguage != null) ? this.CurrentPerson.IntepreterLanguage.IntepreterLanguageID.ToString() : "";
        ddlSecondLanguage.SelectedValue = (this.CurrentPerson.IntepreterLanguage1 != null) ? this.CurrentPerson.IntepreterLanguage1.IntepreterLanguageID.ToString() : "";
        ddlThirdLanguage.SelectedValue = (this.CurrentPerson.IntepreterLanguage2 != null) ? this.CurrentPerson.IntepreterLanguage2.IntepreterLanguageID.ToString() : "";

        chkSecurityRisk.Checked = this.CurrentPerson.SecurityRisk;
        chkDisability.Checked = this.CurrentPerson.Disability;
        chkNonDisclosureFinding.Checked = this.CurrentActionPerson.NonDisclosureFinding;

        chkNP.Checked = this.CurrentActionPerson.NPFlag;

        int years = CalculateAge(dtBirthDate.SelectedDate);
        if (this.CurrentActionPerson.NPFlag && years >= 0 && years < 18)
        {
            chkNPMinor.Checked = true;
        }
        else if (this.CurrentActionPerson.NPMinor.HasValue)
        {
            chkNPMinor.Checked = this.CurrentActionPerson.NPMinor.Value;
        }

        CSCMS.Data.SpecialOrder so = this.CscmsContext.SpecialOrder.Where(it => it.ActionPerson.Action.ActionGUID == this.CurrentAction.ActionGUID
            && it.ActionPerson.Person.PersonGUID == this.CurrentActionPerson.PersonGUID).FirstOrDefault();

        if (so != null)
        {
            so = this.CscmsContext.SpecialOrder.Where(it => it.ActionPerson.Action.ActionGUID == this.CurrentAction.ActionGUID
            && it.ActionPerson.Person.PersonGUID == this.CurrentActionPerson.PersonGUID).OrderByDescending(it => it.LastUpdateDate).FirstOrDefault();

            chkInJail.Checked = false;
            if (so.ArrestCommitmentEffectiveDate != null && so.ReleaseDate == null)
                chkInJail.Checked = true;

        }
        else if (this.CurrentActionPerson.InJail.HasValue)
        {
            chkInJail.Checked = this.CurrentActionPerson.InJail.Value;
        }

        this.ddlPersonType.SelectedValue = this.CurrentActionPerson.PersonType.PersonTypeID.ToString();
        this.ddlPersonType.Enabled = true; // 

        this.ddlCustodialTypes.SelectedValue = (this.CurrentActionPerson.CustodialType != null) ? this.CurrentActionPerson.CustodialType.CustodialTypeID.ToString() : "";
        this.ddlCustodialTypes.Enabled = (this.Father || this.Mother || this.OtherOrRelative || this.GrandFather || this.GrandMother) ? true : false;

        this.ddlFatherType.SelectedValue = (this.CurrentActionPerson.FatherType != null) ? this.CurrentActionPerson.FatherType.FatherTypeID.ToString() : "";
        this.ddlFatherType.Enabled = (this.Father) ? true : false;

        this.ddlGrantparent.SelectedValue = (this.CurrentActionPerson.GrantParentType != null) ? this.CurrentActionPerson.GrantParentType.GrantParentTypeID.ToString() : "";
        this.ddlGrantparent.Enabled = (this.GrandFather || this.GrandMother) ? true : false;

        CSCMS.Data.Service service = CscmsContext.Service.Include("ServiceType").Where(it => it.ActionPerson.Action.ActionGUID == this.CurrentAction.ActionGUID && it.ActionPerson.Person.PersonGUID == this.CurrentPerson.PersonGUID).FirstOrDefault();
        if (service != null)
        {
            this.ddlServiceType.SelectedValue = (service.ServiceType != null) ? service.ServiceType.ServiceTypeID.ToString() : "";
            this.dtServiceDate.SelectedDate = service.ServiceDate;
            this.dtAnswerToServiceDate.SelectedDate = service.AnswerToService;
        }

        // indigence 
        this.chkIndigenceHearing.Checked = CurrentActionPerson.IndigenceHearing;
        this.chkNeedAttorneyAppointed.Checked = CurrentActionPerson.NeedAttorneyAppointed;
        this.chkRequestedAdditionalTime.Checked = CurrentActionPerson.RequestedAdditionalTime;
        this.ddlCounselStatus.SelectedValue = CurrentActionPerson.CounselStatus.HasValue ? CurrentActionPerson.CounselStatus.ToString() : "";
        this.dtDateFoundIndegent.SelectedDate = CurrentActionPerson.DateFoundIndegent;
        this.dtCounselDate.SelectedDate = CurrentActionPerson.CounselDate;


        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
        CommandType.StoredProcedure,
        "GetRelatedPersonByType",
        new SqlParameter("@ActionGUID", this.CurrentActionPerson.ActionGUID),
        new SqlParameter("@PersonGUID", this.CurrentActionPerson.PersonGUID),
        new SqlParameter("@PersonRelationshipTypeID", (byte)PersonRelationshipTypeEnum.ClientAttorney));

        if (this.Father || this.Mother || this.OtherOrRelative || this.GrandFather || this.GrandMother)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlAttorney.SelectedValue = ds.Tables[0].Rows[0]["RelatedPersonGUID"].ToString();
                //dtAttorneyAppointedDate.SelectedDate = relatedAttorney.RelationshipBeginDate;
                //if (relatedAttorney.Retained) ddlAttorneyDateType.SelectedValue = "1";
                //else ddlAttorneyDateType.SelectedValue = "0";
            }
            //if (CurrentPersonCase.WaivedRightToCounselDate.HasValue)
            //{
            //    dtAttorneyAppointedDate.SelectedDate = CurrentPersonCase.WaivedRightToCounselDate;
            //    ddlAttorneyDateType.SelectedValue = "2";
            //}


        }

        if (this.Father)
        {
            CSCMS.Data.PaternityTest paternityTest = CscmsContext.PaternityTest.Where(p => p.ActionPerson.Action.ActionGUID == this.CurrentAction.ActionGUID && p.ActionPerson.Person.PersonGUID == this.CurrentPerson.PersonGUID).FirstOrDefault();
            if (paternityTest != null)
            {
                this.chkNotOrder.Checked = (paternityTest.NotOrder == null) ? false : (bool)paternityTest.NotOrder;
                this.chkTestPositive.Checked = (paternityTest.TestPositive == null) ? false : (bool)paternityTest.TestPositive;
                this.chkIsExcluded.Checked = (paternityTest.IsExcluded == null) ? false : (bool)paternityTest.IsExcluded;
                this.txtTestOpenedNote.Text = paternityTest.PaternityTestNote == null ? string.Empty : paternityTest.PaternityTestNote.ToString();
                this.dtOrderDate.SelectedDate = paternityTest.OrderedDate;
                this.dtPaternityEstablishedDate.SelectedDate = paternityTest.PaternityEstablishedDate;
                this.dtResultDate.SelectedDate = paternityTest.ResultDate;

            }
        }
        SetFieldVisibility();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="actionGuid"></param>
    /// <param name="personGuid"></param>
    /// 
    public override void LoadActionPerson(Guid actionGuid, Guid personGuid)
    {
        base.LoadActionPerson(actionGuid, personGuid);

        lblPageTitle.Text = this.CurrentActionPerson.PersonType.PersonTypeName.ToUpper();

        //lblPageTitle.Text = "RESPONDENT";

        switch ((PersonTypeEnum)(this.CurrentActionPerson.PersonType.PersonTypeID))
        {
            case PersonTypeEnum.Child:
                this.Child = true;
                break;
            case PersonTypeEnum.GrandFather:
                this.GrandFather = true;
                break;
            case PersonTypeEnum.GrandMother:
                this.GrandMother = true;
                break;
            case PersonTypeEnum.Father:
                this.Father = true;
                break;
            case PersonTypeEnum.Mother:
                this.Mother = true;
                break;
            case PersonTypeEnum.Relative:
                this.OtherOrRelative = true;
                break;
            case PersonTypeEnum.Other:
                this.OtherOrRelative = true;
                break;
            case PersonTypeEnum.Attorney:
                this.CaseAttorneyOrGAL = true;
                break;
            case PersonTypeEnum.GuardianadItem:
                this.CaseAttorneyOrGAL = true;
                break;
        }


        divPersonButtons.Visible = true;

        divCaseNotes.Visible = true;
    }
    private void InitializePersonAction()
    {
        InitializeActionPerson(Guid.Empty);
    }

    private void InitializeActionPerson(Guid personGuid)
    {
        divPersonButtons.Visible = false;

        CSCMS.Data.Person person;

        if (personGuid == Guid.Empty)
        {
            person = new CSCMS.Data.Person();
            person.PersonGUID = Guid.NewGuid();
            person.CreateDate = DateTime.Now;
            person.LastUpdateDate = DateTime.Now;
            person.LastName = Request.QueryString["ln"];
            person.FirstName = Request.QueryString["fn"];
            person.Birthdate = !string.IsNullOrEmpty(Request.QueryString["bd"]) ? DateTime.Parse(Request.QueryString["bd"]) : (DateTime?)null;
            person.Sex = !string.IsNullOrEmpty(Request.QueryString["sex"]) ? char.Parse(Request.QueryString["sex"]).ToString() : null;
        }
        else
        {
            person = CscmsContext.Person.Where(p => p.PersonGUID == personGuid).FirstOrDefault();
        }

        CSCMS.Data.ActionPerson ap = new CSCMS.Data.ActionPerson();
        ap.Person = person;
        ap.Action = CscmsContext.Action.Where(it => it.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();
        ap.CreateDate = DateTime.Now;
        ap.LastUpdateDate = DateTime.Now;

        int personTypeId = short.Parse(Request.QueryString["t"].ToString());
        ap.PersonType = CscmsContext.PersonType.Where(it => it.PersonTypeID == personTypeId).FirstOrDefault();
        this.CurrentActionPerson = ap;

        lblPageTitle.Text = this.CurrentActionPerson.PersonType.PersonTypeName.ToUpper();
        switch ((PersonTypeEnum)(this.CurrentActionPerson.PersonType.PersonTypeID))
        {
            case PersonTypeEnum.Child:
                this.Child = true;
                this.btnSaveAndAnotherChild.Visible = true;
                break;
            case PersonTypeEnum.GrandFather:
                this.GrandFather = true;
                break;
            case PersonTypeEnum.GrandMother:
                this.GrandMother = true;
                break;
            case PersonTypeEnum.Father:
                this.Father = true;
                break;
            case PersonTypeEnum.Mother:
                this.Mother = true;
                break;
            case PersonTypeEnum.Relative:
                this.OtherOrRelative = true;
                break;
            case PersonTypeEnum.Other:
                this.OtherOrRelative = true;
                break;
            case PersonTypeEnum.Attorney:
                this.CaseAttorneyOrGAL = true;
                break;
            case PersonTypeEnum.GuardianadItem:
                this.CaseAttorneyOrGAL = true;
                break;
        }
        this.CurrentPerson = person;
    }

    private void SetFieldVisibility()
    {

        if (!this.NewPersonCase && !PersonOnly)
        {
            divMain.Attributes["class"] = "existingCase";
            if (Session["ScreenHeight"] != null) divMain.Style["Height"] = (325 + System.Math.Max(768, int.Parse(Session["ScreenHeight"].ToString())) - 768).ToString() + "px";
            divMainSub.Attributes["class"] = "existingCaseSub";
            trRelatedCasesLabel.Visible = true;
            trRelatedCasesList.Visible = true;
            trRelatedCasesHr.Visible = true;
        }

        if (this.Child)
        {
            trBirthDate.Visible = true;
            trIncarcerated.Visible = true;
            trAlias.Visible = true;
            trSecurityRisk.Visible = true;
            trInterpreterNeeded.Visible = true;
            trFirstLanguage.Visible = true;
            trInterpreterHr.Visible = true;
            trSecondLanguage.Visible = false;
            trThirdLanguage.Visible = false;
            sectionCaseIndividual.Visible = false;
            if (!this.NewPersonCase)
            {
                trAge.Visible = true;
            }
            this.btnSaveAndAnotherChild.Visible = true;
        }
        else // Non-child 
        {
            //sectionCaseIndividual.Visible = true;

            trAddress1.Visible = true;
            trAddress2.Visible = true;
            trAddress3.Visible = true;
            trCity.Visible = true;
            trCounty.Visible = true;
            trState.Visible = true;
            trZipCode.Visible = true;
            trPhoneHome.Visible = true;
            trPhoneOffice.Visible = true;
            trPhoneFax.Visible = true;
            trPhoneCell.Visible = true;
            trEmailAddress.Visible = true;


            if (this.CaseAttorneyOrGAL)
            {
                //this.trAttorneyEndDate.Visible = true;
                //sharedCalendar.Visible = true;
            }


        }

        rptRelatedCases.DataBind();

        if (rptRelatedCases.Items.Count == 0)
        {
            trRelatedCasesLabel.Visible = false;
            trRelatedCasesList.Visible = false;
            trRelatedCasesHr.Visible = false;
        }

        // new
        if (!this.CaseAttorneyOrGAL)
        {
            this.trPhoneHomeHR0.Visible = false;
            this.trPhoneHomeHR1.Visible = false;
            this.trPhoneFaxHR2.Visible = false;
            this.trPhoneHome.Visible = false;
            this.trPhoneCell.Visible = false;
            this.trPhoneFax.Visible = false;
            this.trPhoneOffice.Visible = false;
            this.trEmailAddress.Visible = false;
            trSecondLanguage.Visible = false;
            trThirdLanguage.Visible = false;
        }
        if (this.CaseAttorneyOrGAL)
        {
            this.trInterpreterNeeded.Visible = false;
            this.trInterpreterHr.Visible = false;
            this.trIncarcerated.Visible = false;
            this.trSecurityRisk.Visible = false;
            this.trDisability.Visible = false;
            this.trBirthDate.Visible = false;
            //this.sharedCalendar.Visible = false;
            this.trBarCardNumber.Visible = true;
            this.trFirm.Visible = true;
            this.trchkNP.Visible = false;
            this.trNonDisclosureFinding.Visible = false;
            this.trAttorney.Visible = false;
        }

        this.trCustodialTypes.Visible = (this.Father || this.Mother || this.OtherOrRelative || this.GrandFather || this.GrandMother) ? true : false;
        this.trFatherType.Visible = (this.Father) ? true : false;
        this.trGrantparent.Visible = (this.GrandFather || this.GrandMother) ? true : false;
        this.trServiceType.Visible = (this.trCustodialTypes.Visible) ? true : false;
        this.trServiceDate.Visible = (this.trCustodialTypes.Visible) ? true : false;
        this.trAnswerToServiceDate.Visible = (this.trCustodialTypes.Visible) ? true : false;

        //if (((this.CurrentAction.ActionType.ActionTypeID == (short)ActionTypeEnum.MotionToRevoke ||
        //    this.CurrentAction.ActionType.ActionTypeID == (short)ActionTypeEnum.UIFSAEnforcement ||
        //    this.CurrentAction.ActionType.ActionTypeID == (short)ActionTypeEnum.MotiontoEnforce) && (this.CurrentActionPerson.NPFlag == true)))
        //{
        //}
        //else 
        //{
        //    this.trNeedAttorneyAppointed.Visible = false;
        //    this.trCounselDate.Visible = false;
        //    this.trDateFoundIndegent.Visible = false;
        //    this.trIndigenceHearing.Visible = false;
        //    this.trddlCounselStatus.Visible = false;
        //    this.trRequestedAdditionalTime.Visible = false;
        //    this.trddlCounselStatus.Visible = false;
        //}

        if (!this.NewPersonCase)
        {
            SetNPRelatedVisibility(this.chkNP.Checked ? "NpShow" : "NpHide");
        }
        else
        {
            SetNPRelatedVisibility("NpHide");
        }
        sectionPaternityTest.Visible = (this.Father) ? true : false;
    }

    private void SetNPRelatedVisibility(string value)
    {
        this.trNeedAttorneyAppointed.Attributes["class"] = value;
        this.trCounselDate.Attributes["class"] = value;
        this.trDateFoundIndegent.Attributes["class"] = value;
        this.trIndigenceHearing.Attributes["class"] = value;
        this.trRequestedAdditionalTime.Attributes["class"] = value;
        this.trddlCounselStatus.Attributes["class"] = value;
    }
    #endregion

    #region [Buttons]

    protected void btnDeletePerson_Click(object sender, EventArgs e)
    {
        Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(SqlConn,
            CommandType.StoredProcedure,
            "DeleteActionPerson",
            new SqlParameter("@UserId", this.CurrentUser.UserID),
            new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
            new SqlParameter("@PersonGUID", this.CurrentPerson.PersonGUID));
        this.CurrentPerson = null;
        Response.Redirect(string.Format("ActionDetail.aspx?cid={0}&a={1}&message=DeletePerson", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber), true);
        //http://texdeck-dev/csDEV/MemberPages/PersonDetail.aspx?cid=5e9693ac-433e-44cf-81ff-2ad7a282c497&a=1&pid=080db1cc-0cf1-42a7-9ddb-69d183bdff98

    }

    protected void btnSaveAndAddAnother_Click(object sender, EventArgs e)
    {
        bool redirect = false;
        try
        {
            UpdatePerson();
            redirect = true;
        }
        catch (Exception ex)
        {
            LogError.LogErrorToSQL(ex.ToString());
            messageBox.ErrorMessage = ex.Message;
        }
        finally
        {
            if (redirect)
                Response.Redirect(string.Format("PersonDetail.aspx?cid={0}&a={1}&t=3&message=SaveAndAddAnotherChild", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber, Request.QueryString["t"]), true);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool redirect = false;
        try
        {
            UpdatePerson();
            messageBox.SuccessMessage = "Person saved successfully.";
            redirect = true;
        }
        catch (Exception ex)
        {
            LogError.LogErrorToSQL(ex.ToString());
            messageBox.ErrorMessage = ex.Message;
        }
        finally
        {
            if (redirect)
                Response.Redirect(string.Format("PersonDetail.aspx?cid={0}&a={1}&pid={2}&message=SavePerson", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber, this.CurrentPerson.PersonGUID), true);
        }
    }

    protected void btnSaveAndReturn_Click(object sender, EventArgs e)
    {
        bool redirect = false;
        try
        {
            UpdatePerson();
            messageBox.SuccessMessage = "Person saved successfully.";
            redirect = true;
        }
        catch (Exception ex)
        {
            LogError.LogErrorToSQL(ex.ToString());
            messageBox.ErrorMessage = ex.Message;
        }
        finally
        {
            if (redirect)
                Response.Redirect(string.Format("ActionDetail.aspx?cid={0}&a={1}&message=SavePerson", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber), true);
        }
    }
    #endregion

    #region [Update Data]

    private void UpdatePerson()
    {
        bool iOpenedConn = false;

        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            Guid attorneyPersonGuid = Guid.Empty;
            Guid casaPersonGuid = Guid.Empty;
            Guid galPersonGuid = Guid.Empty;
            bool wasNP = false;

            if (this.NewPersonCase)
            {
                if (!string.IsNullOrEmpty(this.HFPersonGUID.Value) && this.HFPersonGUID.Value != "Current")
                    InitializeActionPerson(new Guid(this.HFPersonGUID.Value));
                else
                    InitializeActionPerson(Guid.Empty);

                CscmsContext.AddToActionPerson(this.CurrentActionPerson);

                if (this.NewPerson && (string.IsNullOrEmpty(this.HFPersonGUID.Value) || this.HFPersonGUID.Value == "Current"))
                {
                    CscmsContext.AddToPerson(this.CurrentPerson);
                }
                else
                {
                    if (this.CurrentPerson.EntityState == EntityState.Detached)
                        CscmsContext.Attach(this.CurrentPerson);
                }
            }
            else
            {
                wasNP = this.CurrentActionPerson.NPFlag;
                if (this.CurrentActionPerson.EntityState == EntityState.Detached)
                    CscmsContext.Attach(this.CurrentActionPerson);

                if (this.CurrentPerson.EntityState == EntityState.Detached)
                    CscmsContext.Attach(this.CurrentPerson);
            }


            //FirstLanguage is also the InterpterLanguage for a party
            //For attorneys, all three languages are reuired.

            short? fLangId = !string.IsNullOrEmpty(this.ddlFirstLanguage.SelectedValue) ? short.Parse(ddlFirstLanguage.SelectedValue) : (short?)null;
            short? sLangId = !string.IsNullOrEmpty(this.ddlSecondLanguage.SelectedValue) ? short.Parse(ddlSecondLanguage.SelectedValue) : (short?)null;
            short? tLangId = !string.IsNullOrEmpty(this.ddlThirdLanguage.SelectedValue) ? short.Parse(ddlThirdLanguage.SelectedValue) : (short?)null;

            if (string.IsNullOrEmpty(this.HFPersonGUID.Value) || this.HFPersonGUID.Value == "Current")
            {
                this.CurrentPerson.FirstName = txtFirstName.Text.Trim();
                this.CurrentPerson.LastName = txtLastName.Text.Trim();
                this.CurrentPerson.MiddleName = txtMiddleName.Text.Trim();
                this.CurrentPerson.NameSuffix = txtSuffix.Text.Trim();
                this.CurrentPerson.AliasName = txtAlias.Text.Trim();
                this.CurrentPerson.Birthdate = dtBirthDate.SelectedDate;
                this.CurrentPerson.Sex = !string.IsNullOrEmpty(ddlSex.SelectedValue) ? char.Parse(ddlSex.SelectedValue).ToString() : null;
                this.CurrentPerson.InterpreterNeeded = chkInterpreterNeeded.Checked;
                this.CurrentPerson.SecurityRisk = chkSecurityRisk.Checked;
                this.CurrentPerson.Disability = chkDisability.Checked;

                if (fLangId != null)
                    this.CurrentPerson.IntepreterLanguage = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == fLangId).FirstOrDefault();
                else
                    this.CurrentPerson.IntepreterLanguage = null;

            }

            if (sLangId != null)
                this.CurrentPerson.IntepreterLanguage1 = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == sLangId).FirstOrDefault();
            else
                this.CurrentPerson.IntepreterLanguage1 = null;

            if (tLangId != null)
                this.CurrentPerson.IntepreterLanguage2 = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == tLangId).FirstOrDefault();
            else
                this.CurrentPerson.IntepreterLanguage2 = null;

            this.CurrentActionPerson.NPFlag = chkNP.Checked;

            int years = CalculateAge(dtBirthDate.SelectedDate);
            if (this.CurrentActionPerson.NPFlag && years >= 0)
                chkNPMinor.Checked = (years < 18);

            this.CurrentActionPerson.NPMinor = chkNPMinor.Checked;
            this.CurrentActionPerson.InJail = chkInJail.Checked;
            this.CurrentActionPerson.NonDisclosureFinding = chkNonDisclosureFinding.Checked;

            if (this.CaseAttorneyOrGAL)
            {
                this.CurrentPerson.Address1 = txtAddress1.Text.Trim();
                this.CurrentPerson.Address2 = txtAddress2.Text.Trim();
                this.CurrentPerson.Address3 = txtAddress3.Text.Trim();
                this.CurrentPerson.City = txtCity.Text.Trim();
                this.CurrentPerson.CountyID = !string.IsNullOrEmpty(ddlCounty.SelectedValue) ? short.Parse(ddlCounty.SelectedValue) : (short?)null;
                string stateId = !string.IsNullOrEmpty(ddlState.SelectedValue) ? ddlState.SelectedValue : null;

                if (!string.IsNullOrEmpty(stateId))
                    this.CurrentPerson.State = CscmsContext.State.Where(it => it.StateID == stateId).FirstOrDefault();
                else
                    this.CurrentPerson.State = null;

                this.CurrentPerson.ZipCode = !string.IsNullOrEmpty(txtZipCode.Text) ? int.Parse(txtZipCode.Text) : (int?)null;
                this.CurrentPerson.ZipCodeExtension = !string.IsNullOrEmpty(txtZipCodeExt.Text) ? short.Parse(txtZipCodeExt.Text) : (short?)null;
            }
            else
            {
                this.CurrentActionPerson.Address1 = txtAddress1.Text.Trim();
                this.CurrentActionPerson.Address2 = txtAddress2.Text.Trim();
                this.CurrentActionPerson.Address3 = txtAddress3.Text.Trim();
                this.CurrentActionPerson.City = txtCity.Text.Trim();
                this.CurrentActionPerson.CountyID = !string.IsNullOrEmpty(ddlCounty.SelectedValue) ? short.Parse(ddlCounty.SelectedValue) : (short?)null;

                string stateId = !string.IsNullOrEmpty(ddlState.SelectedValue) ? ddlState.SelectedValue : null;
                if (!string.IsNullOrEmpty(stateId))
                    this.CurrentActionPerson.State = CscmsContext.State.Where(it => it.StateID == stateId).FirstOrDefault();
                else
                    this.CurrentActionPerson.State = null;

                this.CurrentActionPerson.ZipCode = !string.IsNullOrEmpty(txtZipCode.Text) ? int.Parse(txtZipCode.Text) : (int?)null;
                this.CurrentActionPerson.ZipCodeExtension = !string.IsNullOrEmpty(txtZipCodeExt.Text) ? short.Parse(txtZipCodeExt.Text) : (short?)null;

                short? custodialTypeId = !string.IsNullOrEmpty(ddlCustodialTypes.SelectedValue) ? short.Parse(ddlCustodialTypes.SelectedValue) : (short?)null;
                short? fatherTypeId = !string.IsNullOrEmpty(this.ddlFatherType.SelectedValue) ? short.Parse(ddlFatherType.SelectedValue) : (short?)null;
                if (custodialTypeId != null)
                    CurrentActionPerson.CustodialType = CscmsContext.CustodialType.Where(it => it.CustodialTypeID == custodialTypeId).FirstOrDefault();
                else
                    CurrentActionPerson.CustodialType = null;

                if (fatherTypeId != null)
                    CurrentActionPerson.FatherType = CscmsContext.FatherType.Where(it => it.FatherTypeID == fatherTypeId).FirstOrDefault();
                else
                    CurrentActionPerson.FatherType = null;

                CSCMS.Data.Service service = CscmsContext.Service.Where(it => it.ActionPerson.Action.ActionGUID == this.CurrentAction.ActionGUID && it.ActionPerson.Person.PersonGUID == this.CurrentPerson.PersonGUID).FirstOrDefault();
                short? svcTypeId = !string.IsNullOrEmpty(ddlServiceType.SelectedValue) ? short.Parse(ddlServiceType.SelectedValue) : (short?)null;

                Action<CSCMS.Data.Service> setValues = delegate(CSCMS.Data.Service s)
                {
                    s.ActionPerson = this.CurrentActionPerson;
                    s.LastUpdateDate = DateTime.Now;

                    s.ServiceDate = dtServiceDate.SelectedDate.Value;
                    if (svcTypeId != null)
                        s.ServiceType = CscmsContext.ServiceType.Where(it => it.ServiceTypeID == svcTypeId).FirstOrDefault();
                    else
                        s.ServiceType = null;

                    s.AnswerToService = dtAnswerToServiceDate.SelectedDate;
                };

                if (service == null)
                {
                    if (dtServiceDate.SelectedDate != null)
                    {
                        service = new CSCMS.Data.Service();
                        service.CreateDate = DateTime.Now;
                        setValues(service);
                        CscmsContext.AddToService(service);
                    }
                }
                else if (service != null)
                {
                    if (!dtServiceDate.SelectedDate.HasValue)
                    {
                        CscmsContext.DeleteObject(service);
                    }
                    else
                    {
                        setValues(service);
                    }
                }
            }

            this.CurrentPerson.EmailAddress = txtEmailAddress.Text;

            this.CurrentPerson.BarCardNumber = !string.IsNullOrEmpty(txtBarCardNumber.Text) ? int.Parse(txtBarCardNumber.Text) : (int?)null;
            this.CurrentPerson.Firm = txtFirm.Text.Trim();

            if (wasNP && !chkNP.Checked)
            {
                chkNeedAttorneyAppointed.Checked = false;
                ddlCounselStatus.SelectedValue = "";
                chkIndigenceHearing.Checked = false;
                chkRequestedAdditionalTime.Checked = false;
                this.dtDateFoundIndegent.SelectedDate = null;
                this.dtCounselDate.SelectedDate = null;
            }
            CurrentActionPerson.CounselStatus = !string.IsNullOrEmpty(ddlCounselStatus.SelectedValue) ? short.Parse(ddlCounselStatus.SelectedValue) : (short?)null;
            CurrentActionPerson.NeedAttorneyAppointed = chkNeedAttorneyAppointed.Checked;
            CurrentActionPerson.IndigenceHearing = chkIndigenceHearing.Checked;
            CurrentActionPerson.RequestedAdditionalTime = chkRequestedAdditionalTime.Checked;
            CurrentActionPerson.DateFoundIndegent = this.dtDateFoundIndegent.SelectedDate;
            CurrentActionPerson.CounselDate = this.dtCounselDate.SelectedDate;

            if (this.Father)
            {
                if (dtOrderDate.SelectedDate == null)
                {
                    Helper.DeletePaternityTest(CscmsContext, this.CurrentAction.ActionGUID, this.CurrentPerson.PersonGUID, false, true);
                }
                else
                {
                    Helper.AddorUpdatePaternityTest(CscmsContext,
                                   new PaternityTestData
                                   {
                                       ActionGuid = this.CurrentAction.ActionGUID,
                                       PersonGuid = this.CurrentPerson.PersonGUID,
                                       Note = this.txtTestOpenedNote.Text,
                                       OrderedDate = this.dtOrderDate.SelectedDate,
                                       PaternityestablishedDate = this.dtPaternityEstablishedDate.SelectedDate,
                                       ResultDate = this.dtResultDate.SelectedDate,
                                       TestPositive = this.chkTestPositive.Checked,
                                       isExcluded = this.chkIsExcluded.Checked,
                                       NotOrder = this.chkNotOrder.Checked
                                   }, false, true);
                }
            }

            short perTypeId = short.Parse(ddlPersonType.SelectedValue);
            CurrentActionPerson.PersonType = CscmsContext.PersonType.Where(it => it.PersonTypeID == perTypeId).FirstOrDefault();

            CscmsContext.SaveChanges();

            // update attorney
            if (this.Father || this.Mother || this.OtherOrRelative || this.GrandFather || this.GrandMother)
            {
                if (ddlAttorney.SelectedIndex > 0)
                    attorneyPersonGuid = new Guid(ddlAttorney.SelectedValue);

                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(SqlConn,
                CommandType.StoredProcedure,
                "InsertUpdateDeleteRelatedPerson",
                new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID.ToString()),
                new SqlParameter("@PersonGUID", this.CurrentActionPerson.PersonGUID.ToString()),
                new SqlParameter("@RelatedPersonGUID", attorneyPersonGuid.ToString()),
                new SqlParameter("@PersonRelationshipTypeID", (byte)PersonRelationshipTypeEnum.ClientAttorney));
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    #endregion

    #region [ddl]


    #endregion

    #region [DataSet]
    private void SetPersonTypes()
    {
        var query = (from pc in CscmsContext.PersonType
                     orderby pc.SortOrder
                     select new { pc.PersonTypeID, pc.PersonTypeName });
        if (this.NewPerson)
        {
            if (this.Child)
            {
                int v1 = (int)PersonTypeEnum.Child;
                query = query.Where(it => it.PersonTypeID == v1);
            }
            else if (this.Father || this.Mother)
            {
                int v1 = this.Father ? (int)PersonTypeEnum.Father : (int)PersonTypeEnum.Mother;
                query = query.Where(it => it.PersonTypeID == v1);
            }
            else if (this.GrandMother || this.GrandFather)
            {
                int v1 = this.GrandMother ? (int)PersonTypeEnum.GrandMother : (int)PersonTypeEnum.GrandFather;
                query = query.Where(it => it.PersonTypeID == v1);
            }
            else if (this.CaseAttorneyOrGAL)
            {
                int v1 = (int)PersonTypeEnum.Attorney;
                int v2 = (int)PersonTypeEnum.GuardianadItem;
                query = query.Where(it => it.PersonTypeID == v1 || it.PersonTypeID == v2);
            }
            else
            {
                int v1 = (int)PersonTypeEnum.Other;
                int v2 = (int)PersonTypeEnum.Relative;
                query = query.Where(it => it.PersonTypeID == v1 || it.PersonTypeID == v2);
            }
        }
        ddlPersonType.DataSource = query.ToList();
    }
    protected void dsRelatedCases_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        if (this.CaseAttorneyOrGAL)
        {
            e.Cancel = true;
            return;
        }

        e.Command.Parameters["@PersonGUID"].Value = this.CurrentActionPerson.PersonGUID.ToString();
        e.Command.Parameters["@CauseGUID"].Value = this.CurrentCause.CauseGUID.ToString();
    }

    protected void dsAttorney_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        Guid? relatedAttorneyGuid = null;

        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
       CommandType.StoredProcedure,
       "GetRelatedPersonByType",
       new SqlParameter("@ActionGUID", this.CurrentActionPerson.ActionGUID),
       new SqlParameter("@PersonGUID", this.CurrentPerson.PersonGUID),
       new SqlParameter("@PersonRelationshipTypeID", (byte)PersonRelationshipTypeEnum.ClientAttorney)
       );

        if (ds.Tables[0].Rows.Count > 0)
        {
            relatedAttorneyGuid = (Guid)ds.Tables[0].Rows[0]["RelatedPersonGUID"];

        }
        else
        {
            relatedAttorneyGuid = new Guid("99999999999999999999999999999999");

        }

        e.Command.Parameters["@ActionGUID"].Value = this.CurrentActionPerson.ActionGUID.ToString();
        e.Command.Parameters["@PersonGUID"].Value = relatedAttorneyGuid.Value.ToString();
        e.Command.Parameters["@CountyID"].Value = this.CurrentCause.County.CountyID;
        e.Command.Parameters["@PersonTypeID"].Value = 25;
    }

    #endregion

    protected void HandleSearchClickedEvent(object sender, EventArgs args)
    {
        PersonMPE.Show();
    }

    protected void HandlePeasonSearchPageChange(object sender, EventArgs args)
    {
        PersonMPE.Show();
    }

    protected void ShowSearchWindow(object sender, EventArgs args)
    {
        if (sender == this.btnPersonSearch)
        {
            this.HFPersonGUID.Value = "Current";
        }
        PersonMPE.Show();
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (this.OtherOrRelative)
        {
            sexLabel.Attributes.Add("class", "fieldLabel");
            this.rfvSex.Visible = false;
        }
        base.OnPreRender(e);
    }
}