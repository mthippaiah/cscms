﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

public partial class MemberPages_PendingCases : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["RangeTypeID"]))
            {
                lblPageLabel.Text = "Pending Expedited Cases with Service ";
                int rangeType = int.Parse(Request.QueryString["RangeTypeID"]);
                switch (rangeType)
                {
                    case 1: lblPageLabel.Text += " <= 90 Days"; break;
                    case 2: lblPageLabel.Text += " 91- 180 Days"; break;
                    case 3: lblPageLabel.Text += " 181 - 365 Days"; break;
                    case 4: lblPageLabel.Text += " > 365 Days"; break;
                }
            }
        }
    }

    protected void RadGridPendingCases_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        int rangeType = int.Parse(Request.QueryString["RangeTypeID"]);
        DataSet ds = Helper.GetPendingCases(rangeType, base.CurrentUser.OCACourtID);
        string filter = @"ActionTypeName <> 'After Trial Motion' and ActionTypeName <> 'Other (Non-expedited)'";
        DataRow[] rows = ds.Tables[0].Select(filter);
        RadGridPendingCases.DataSource = rows;

    }

    protected void RadGridPendingCases_ExcelMLExportStylesCreated(object source, GridExportExcelMLStyleCreatedArgs e)
    {
        foreach (StyleElement style in e.Styles)
        {
            if (style.Id == "headerStyle")
            {
                style.FontStyle.Bold = true;
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "itemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "alternatingItemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
        }
    }

    protected void btnExportCase_Click(object sender, EventArgs e)
    {
        if (RadGridPendingCases.Items.Count > 0)
        {
            this.RadGridPendingCases.ExportSettings.ExportOnlyData = true;
            RadGridPendingCases.ExportSettings.IgnorePaging = true;
            RadGridPendingCases.ExportSettings.OpenInNewWindow = true;
            RadGridPendingCases.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
            RadGridPendingCases.MasterTableView.ExportToExcel();
        }
    }
}
