﻿<%@ Page Title="CSCMS | Child Support Calculator" Language="C#" MasterPageFile="~/CSCMSPlain2.master" AutoEventWireup="true"
    CodeFile="ChildSupportCalculation.aspx.cs" Inherits="MemberPages_ChildSupportCalculation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/ChildSupportCalculation.ascx" TagName="ChildSupportCalculation"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <table width="100%">
        <tr>
            <td style="text-align:center;">
                <asp:Panel ID="ModPop" runat="server" BackColor="Bisque" BorderStyle="Outset" Height="675" Width="495" HorizontalAlign="Center" >
                    <div id="divpopupheader1" style="border-bottom-style: Outset; text-align: center;
                        background-color: Gray">
                        Child Support Calculator
                    </div>
                    <div id="divpopid1">
                    <uc1:ChildSupportCalculation ID="ChildSupportCalculation" runat="server" />
                     </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
