﻿using System;

public partial class MemberPages_CaseNote : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.caseNotes.SetCaseNoteSize();

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cid"]))
            {
                LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));
                txtCauseNumber.Text = ((BasePage)this.Page).CurrentCause.CauseNumber;
                txtOAG.Text = ((BasePage)this.Page).CurrentCause.OAGCauseNumber;
              
            }
        }

    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        ((BasePage)this.Page).CurrentCause = null;
        ((BasePage)this.Page).CurrentAction = null;
        ((BasePage)this.Page).CurrentPerson = null;
        ((BasePage)this.Page).CurrentActionPerson = null;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/MemberPages/NewCase.aspx?nc=yes&cid=" + Request.QueryString["cid"].ToString() + "&a=" + Request.QueryString["a"].ToString(), true);
    }
}