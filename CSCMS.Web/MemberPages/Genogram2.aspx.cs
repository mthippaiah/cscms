﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Collections.Generic;

public partial class MemberPages_Genogram2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //test2();
        test7();

    }
    protected void test2()
    {
        // We generate a new bitmap and   // draw an ellipse on it
        Bitmap oCanvas = new Bitmap(200, 150);
        Graphics g = Graphics.FromImage(oCanvas);
        g.Clear(Color.White);
        g.DrawEllipse(Pens.Red, 10, 10, 150, 100);

        // Now, we only need to send it    // to the client
        Response.ContentType = "image/jpeg";
        oCanvas.Save(Response.OutputStream, ImageFormat.Jpeg);
        Response.End();

        // Cleanup
        g.Dispose();
        oCanvas.Dispose();
    }
    protected void test3()
    {
        Graphics objGraphics;
        int intHeight;
        int intWidth;
        string strLetter = "test"; //  Request.QueryString["letter"];
        Font objFont = new Font("Garamond", 36, FontStyle.Bold);
        Bitmap bmpDummy = new Bitmap(100, 100);
        objGraphics = Graphics.FromImage(bmpDummy);
        SizeF objSize = objGraphics.MeasureString(strLetter, objFont);
        intHeight = (int)(objSize.Height) - 10;
        intWidth = (int)(objSize.Width) + 2;
        Bitmap bmpLetter = new Bitmap(intWidth, intHeight);
        objGraphics = Graphics.FromImage(bmpLetter);
        objGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        //  You are now ready to draw the string into 
        //  your box...
        objGraphics.Clear(Color.Navy);
        RectangleF objRect = new RectangleF(0, 3, intWidth, intHeight);
        RectangleF objRect2 = new RectangleF(2, 5, intWidth, intHeight);
        StringFormat objSF = new StringFormat();
        objSF.Alignment = StringAlignment.Center;
        objSF.LineAlignment = StringAlignment.Center;
        objGraphics.DrawString(strLetter, objFont, Brushes.Gray, objRect2, objSF);
        objGraphics.DrawString(strLetter, objFont, Brushes.White, objRect, objSF);
        //  You send the Bitmap to the browser
        Response.ContentType = "image/gif";
        bmpLetter.Save(Response.OutputStream, ImageFormat.Gif);
        //  You clean up
        bmpLetter.Dispose();
        bmpDummy.Dispose();
        objGraphics.Dispose();
        objFont.Dispose();
        objSF.Dispose();


    }


    protected void test7()
    {
        int childCount = 0;

        LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));

        DataSet attorneys = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                        CommandType.StoredProcedure,
                        "GetActionPersonsByPersonCategory",
                        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
                        new SqlParameter("@PersonCategory", (byte)PersonCategory.CaseAttorney));

        DataSet children = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                        CommandType.StoredProcedure,
                        "GetActionPersonsByPersonCategory",
                        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
                        new SqlParameter("@PersonCategory", (byte)PersonCategory.Child));

        DataSet respondents = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                        CommandType.StoredProcedure,
                        "GetActionPersonsByPersonCategory",
                        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
                        new SqlParameter("@PersonCategory", (byte)PersonCategory.Respondent));

        //var individuals = dc1.GetActionPersonsByPersonCategory(this.CurrentAction.ActionGUID, (byte)PersonCategory.Individual).ToList();

        DataSet relationships = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                        CommandType.StoredProcedure,
                        "GetRelationships",
                        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID));

        childCount = children.Tables[0].Rows.Count;

        Bitmap genogramDiagram = new Bitmap(850, 480);
        if (respondents.Tables[0].Rows.Count > 5)
        {
            if (attorneys.Tables[0].Rows.Count > 5)
            {
                genogramDiagram = new Bitmap(1000, 680);
                if (childCount > 5) genogramDiagram = new Bitmap(1000, 880);
                if (childCount > 10) genogramDiagram = new Bitmap(1000, 1080);
            }
            else
            {
                genogramDiagram = new Bitmap(1000, 480);
                if (childCount > 5) genogramDiagram = new Bitmap(1000, 680);
                if (childCount > 10) genogramDiagram = new Bitmap(1000, 880);
            }
        }
        else
        {
            if (attorneys.Tables[0].Rows.Count > 5)
            {
                genogramDiagram = new Bitmap(1000, 880);
                if (childCount > 5) genogramDiagram = new Bitmap(850, 880);
                if (childCount > 10) genogramDiagram = new Bitmap(850, 1080);
            }
            else
            {
                if (childCount > 5) genogramDiagram = new Bitmap(850, 680);
                if (childCount > 10) genogramDiagram = new Bitmap(850, 880);
            }
        }
        Graphics g = Graphics.FromImage(genogramDiagram);
        g.Clear(Color.White);
        g.SmoothingMode = SmoothingMode.HighQuality;

        GraphicsPath Person = new GraphicsPath();
        Person.AddEllipse(23, 1, 14, 14);
        Person.AddLine(18, 16, 42, 16);
        Person.AddLine(50, 40, 44, 42);
        Person.AddLine(38, 25, 37, 42);
        Person.AddLine(45, 75, 37, 75);
        Person.AddLine(30, 50, 23, 75);
        Person.AddLine(16, 75, 23, 42);
        Person.AddLine(22, 25, 16, 42);
        Person.AddLine(10, 40, 18, 16);

        int r = 0;
        foreach (DataRow respondent in respondents.Tables[0].Rows)
        {
            if (Helper.EvaluateDbStringValue(respondent["PersonTypeName"]).EndsWith("Father"))
                this.DrawEntity(g, Person, Color.Red, 150 + 150 * r, 10, Helper.EvaluateDbStringValue(respondent["FirstName"]) + " " + Helper.EvaluateDbStringValue(respondent["LastName"]) + "\n" + Helper.EvaluateDbStringValue(respondent["PersonTypeName"]));
            else
                this.DrawEntity(g, Person, Color.Green, 150 + 150 * r, 10, Helper.EvaluateDbStringValue(respondent["FirstName"]) + " " + Helper.EvaluateDbStringValue(respondent["LastName"]) + "\n" + Helper.EvaluateDbStringValue(respondent["PersonTypeName"]));
            r++;
        }

        int c = 0;
        int k = 0;
        foreach (DataRow child in children.Tables[0].Rows)
        {
            this.DrawEntity(g, Person, Color.Blue, 50 + 150 * c, 170 + 200 * k, Helper.EvaluateDbStringValue(child["FirstName"]) + " " + Helper.EvaluateDbStringValue(child["LastName"]) + "\n" + Helper.EvaluateDbStringValue(child["PersonTypeName"]));

            int l = 0;
            foreach (DataRow respondent in respondents.Tables[0].Rows)
            {
                string filter = @"PersonGUID = '" + child["PersonGUID"].ToString() + "'  and RelatedPersonGUID = '" + respondent["PersonGUID"].ToString() + "'";
                DataRow[] parentRelationships = relationships.Tables[0].Select(filter);

                if (parentRelationships.Count() > 0)
                {
                    if ((int)parentRelationships[0]["PersonRelationshipTypeID"] == 18)
                        this.EntityRelation(g, 80 + 150 * c, 160 + 200 * k, 180 + 150 * l, 120, Color.Red);
                    else if ((int)parentRelationships[0]["PersonRelationshipTypeID"] == 19)
                        this.EntityRelation(g, 80 + 150 * c, 160 + 200 * k, 180 + 150 * l, 120, Color.Blue);
                    else if ((int)parentRelationships[0]["PersonRelationshipTypeID"] == 20)
                        this.EntityRelation(g, 80 + 150 * c, 160 + 200 * k, 180 + 150 * l, 120, Color.MediumPurple);
                    else if ((int)parentRelationships[0]["PersonRelationshipTypeID"] == 21)
                        this.EntityRelation(g, 80 + 150 * c, 160 + 200 * k, 180 + 150 * l, 120, Color.MediumVioletRed);
                    else
                        this.EntityRelation(g, 80 + 150 * c, 160 + 200 * k, 180 + 150 * l, 120, Color.Green);
                }

                l++;
            }

            c++;
            if (c == 5)
            {
                c = 0;
                k++;
            }
        }

        c = 0;
        k = 0;
        foreach (DataRow attorney in attorneys.Tables[0].Rows)
        {
            int ad = 0;

            if (childCount > 5) ad = 200;
            if (childCount > 10) ad = 400;

            //if (attorney.FullName.EndsWith("Ad Litem"))
            //    this.DrawEntity(g, Person, Color.AliceBlue, 50 + 150 * c, ad + 350 + 200 * k, attorney.FullName.Replace("-", "\n"));
            //else
            this.DrawEntity(g, Person, Color.Black, 50 + 150 * c, ad + 350 + 200 * k, Helper.EvaluateDbStringValue(attorney["FirstName"]) + " " + Helper.EvaluateDbStringValue(attorney["LastName"]) + "\n" + Helper.EvaluateDbStringValue(attorney["PersonTypeName"])); //attorney.FullName.Replace("-", "\n") 

            int l = 0;

            foreach (DataRow respondent in respondents.Tables[0].Rows)
            {
                string filter = @"PersonGUID = '" + attorney["PersonGUID"].ToString() + "'  and RelatedPersonGUID = '" + respondent["PersonGUID"].ToString() + "'";
                DataRow[] parentRelationships = relationships.Tables[0].Select(filter);

                if (parentRelationships.Count() > 0)
                {
                    this.EntityRelation(g, 80 + 150 * c, ad + 340 + 200 * k, 180 + 150 * l, 120, Color.BurlyWood);
                }

                filter = @"PersonGUID = '" + respondent["PersonGUID"].ToString() + "'  and RelatedPersonGUID = '" + attorney["PersonGUID"].ToString() + "'";
                parentRelationships = relationships.Tables[0].Select(filter);

                if (parentRelationships.Count() > 0)
                {
                    this.EntityRelation(g, 80 + 150 * c, ad + 340 + 200 * k, 180 + 150 * l, 120, Color.BurlyWood);
                }

                l++;
            }

            l = 0;
            int m = 0;
            foreach (DataRow child in children.Tables[0].Rows)
            {

                if (l == 5)
                {
                    l = 0;
                    m++;
                }

                string filter = @"PersonGUID = '" + attorney["PersonGUID"].ToString() + "'  and RelatedPersonGUID = '" + child["PersonGUID"].ToString() + "'";
                DataRow[] parentRelationships = relationships.Tables[0].Select(filter);

                if (parentRelationships.Count() > 0)
                {
                    this.EntityRelation(g, 80 + 150 * c, ad + 340 + 200 * k, 30 + 50 + 150 * l, 170 + 110 + 200 * m, Color.CornflowerBlue);
                }

                filter = @"PersonGUID = '" + child["PersonGUID"].ToString() + "'  and RelatedPersonGUID = '" + attorney["PersonGUID"].ToString() + "'";
                parentRelationships = relationships.Tables[0].Select(filter);

                if (parentRelationships.Count() > 0)
                {
                    this.EntityRelation(g, 80 + 150 * c, ad + 340 + 200 * k, 30 + 50 + 150 * l, 170 + 110 + 200 * m, Color.CornflowerBlue);
                }

                l++;
            }

            c++;
            if (c == 5)
            {
                c = 0;
                k++;
            }
        }



        //EntityCaption(g, 10, genogramDiagram.Height - 10, 30, genogramDiagram.Height - 10, Color.Red, Brushes.Red, "Acknowledged ");
        //EntityCaption(g, 110, genogramDiagram.Height - 10, 130, genogramDiagram.Height - 10, Color.Blue, Brushes.Blue, "Adjudicated ");
        //EntityCaption(g, 210, genogramDiagram.Height - 10, 230, genogramDiagram.Height - 10, Color.MediumPurple, Brushes.MediumPurple, "Presumed ");
        //EntityCaption(g, 310, genogramDiagram.Height - 10, 330, genogramDiagram.Height - 10, Color.MediumVioletRed, Brushes.MediumVioletRed, "Alleged ");
        EntityCaption(g, 10, genogramDiagram.Height - 10, 30, genogramDiagram.Height - 10, Color.Black, Brushes.MediumVioletRed, this.CurrentCause.CauseNumber + " " + this.CurrentAction.Style + " " + this.CurrentAction.OAGCauseNumber);

        Response.ContentType = "image/jpeg";
        genogramDiagram.Save(Response.OutputStream, ImageFormat.Jpeg);
        Response.End();

    }

    private void DrawEntity(Graphics graphics, GraphicsPath Shape, Color fill, float x, float y, string Name)
    {
        graphics.TranslateTransform(x, y);

        System.Drawing.Brush oBrush =
           new LinearGradientBrush(
           new Rectangle(0, 0, 60, 90),
           Color.White, fill, 90, true);
        graphics.FillPath(oBrush, Shape);
        graphics.DrawPath(Pens.Black, Shape);

        StringFormat sf = new StringFormat();
        sf.Alignment = StringAlignment.Center;
        Font oFont = new Font("Tahoma", 8);
        System.Drawing.SizeF size =
           graphics.MeasureString(Name, oFont);
        System.Drawing.RectangleF rect =
           new RectangleF(30 - (size.Width / 2),
           80, size.Width, size.Height);
        graphics.DrawString(Name, oFont,
           Brushes.Black, rect, sf);

        graphics.ResetTransform();
    }

    public void EntityRelation(Graphics g, float x1, float y1, float x2, float y2, Color lineColor)
    {
        Pen p = new Pen(lineColor, 1);
        CustomLineCap myCap = new AdjustableArrowCap(5, 5, true);
        p.EndCap = LineCap.Custom;
        p.CustomEndCap = myCap;
        g.DrawLine(p, x1, y1, x2, y2);
    }
    public void EntityCaption(Graphics g, float x1, float y1, float x2, float y2, Color lineColor, System.Drawing.Brush brush, string Name)
    {
        Pen p = new Pen(lineColor, 1);
        CustomLineCap myCap = new AdjustableArrowCap(5, 5, true);
        p.EndCap = LineCap.Custom;
        p.CustomEndCap = myCap;
        g.DrawLine(p, x1, y1, x2, y2);

        //g.TranslateTransform(x2, y2);
        StringFormat sf = new StringFormat();
        sf.Alignment = StringAlignment.Center;
        Font oFont = new Font("Tahoma", 8);
        System.Drawing.SizeF size = g.MeasureString(Name, oFont);
        System.Drawing.RectangleF rect = new RectangleF(30 - (size.Width / 2),
           80, size.Width, size.Height);
        // Create point for upper-left corner of drawing.
        PointF drawPoint = new PointF(150.0F, 150.0F);
        //System.Drawing.Brush brush = Brushes.Black;

        g.DrawString(Name, oFont, brush, x2, y2 - 6);
        //g.ResetTransform();


    }
    protected void test1()
    {
        // We load an existing bitmap 
        Bitmap oCanvas = (Bitmap)Bitmap.FromFile(Server.MapPath("Images\\Fans1.jpg"));
        Graphics g = Graphics.FromImage(oCanvas);

        // We render some text
        Font f1 = new Font("Arial", 24);
        g.DrawString("The Fans Go Crazy!", f1, Brushes.LightCyan, 5, 5);

        // We render the current score
        Font f2 = new Font("Arial Black", 24);
        Rectangle r = new Rectangle(0, 0, oCanvas.Width, oCanvas.Height);
        StringFormat sf = new StringFormat();
        sf.Alignment = StringAlignment.Far;
        sf.LineAlignment = StringAlignment.Far;
        g.DrawString("EKZ 4 : SBG 1", f2, Brushes.LightCyan, r, sf);

        // We render the copyright at a 90 degree angle
        g.RotateTransform(270);
        Font f3 = new Font("Arial", 10);
        g.DrawString("© by MarkusEgger.com", f3, Brushes.Silver, oCanvas.Height * -1 + 2, 2);
        g.ResetTransform();

        // Now, we only need to send it to the client
        Response.ContentType = "image/jpeg";
        oCanvas.Save(Response.OutputStream, ImageFormat.Jpeg);
        Response.End();

        // Cleanup
        g.Dispose();
        oCanvas.Dispose();
        f1.Dispose();
        f2.Dispose();
        f3.Dispose();
        sf.Dispose();
    }



}



