﻿using System;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class MemberPages_ExportNotice : BasePage
{
    string actionType = "";
    DateTime? hearingDate = null;
    //TimeSpan? hearingTime = null;
    string hearingTime = null;
    string hearingCourt = "";
    string county = "";
    string courtCity = "";
    DateTime? actionFiledDate = null;
    DataRow[] children = null;
    string cpName = "";
    string ncpName = "";
    string causeNumber = "";
    string oagNumber = "";
    Guid? actionGuid = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["hid"]))
        {
            long hearingid = long.Parse(Request.QueryString["hid"]);
            var query = (from h in CscmsContext.Hearing
                         join xn in CscmsContext.Action on h.Action.ActionGUID equals xn.ActionGUID
                         join c in CscmsContext.Cause on xn.Cause.CauseGUID equals c.CauseGUID
                         where h.HearingID == hearingid
                         select new
                         {
                             hearing = h,
                             cause = c,
                             action = xn,
                             ocacourt = c.OCACourt,
                             orgCourt = c.OriginatingCourt,
                             county = c.County,
                             actionType = xn.ActionType
                         }).FirstOrDefault();

            if (query != null)
            {
                actionGuid = query.action.ActionGUID;
                actionFiledDate = query.action.ActionFiledDate;
                hearingDate = query.hearing.HearingDate;
                
                county = query.county.CountyName;
                actionType = query.actionType.ActionTypeName;
                if (query.orgCourt != null)
                {
                    hearingCourt = query.orgCourt.CourtName;
                }

                //hearingTime = query.hearing.HearingTime;
                string time = query.hearing.HearingTime.ToString();
                if (query.hearing.HearingTime.Value.Hours < 12)
                {
                    hearingTime = time.Substring(0, 5) + " AM";
                }
                else
                {
                    hearingTime = time.Substring(0, 5) + " PM";
                }
                
                causeNumber = query.cause.CauseNumber;
                oagNumber = query.cause.OAGCauseNumber;
                DataSet ds = GetPersonsData();
                DataRow[] rows = ds.Tables[0].Select("CustodialTypeName = 'Custodial'");
                if (rows.Count() > 0)
                {
                    cpName = Helper.GetFormattedName(Helper.EvaluateDbStringValue(rows[0]["FirstName"]), Helper.EvaluateDbStringValue(rows[0]["MiddleName"]), Helper.EvaluateDbStringValue(rows[0]["LastName"]), "");
                }

                rows = ds.Tables[0].Select("CustodialTypeName = 'Non-Custodial'");
                if (rows.Count() > 0)
                {
                    ncpName = Helper.GetFormattedName(Helper.EvaluateDbStringValue(rows[0]["FirstName"]), Helper.EvaluateDbStringValue(rows[0]["MiddleName"]), Helper.EvaluateDbStringValue(rows[0]["LastName"]), "");
                }

                string filter = @"PersonTypeName = 'Child'";
                children = ds.Tables[0].Select(filter);
            }
        }

        if (!Page.IsPostBack)
        {
            this.txtCpName.Text = cpName;
            this.txtNcpName.Text = ncpName;
            this.txtOagNumber.Text = oagNumber;
            this.lblCauseNumber.Text = causeNumber;

            string childrenList = "IN THE INTEREST OF \n";
            int i = 1;
            foreach (DataRow r1 in children)
            {
                string name = Helper.GetFormattedName(Helper.EvaluateDbStringValue(r1["FirstName"]), Helper.EvaluateDbStringValue(r1["MiddleName"]), Helper.EvaluateDbStringValue(r1["LastName"]), "");
                childrenList += ("   " + name.ToUpper() + System.Environment.NewLine);
                int temp = i + 1;
                if(temp == children.Count())
                    childrenList += ("   AND " + System.Environment.NewLine);
                i++;
            }
            childrenList += "CHILD(REN)";

            this.txtChildrenNames.Text = childrenList;

            int countLines = children.Count() + 3;

            string courtLocation = "§   IN THE ";
            if(!string.IsNullOrEmpty(hearingCourt))
               courtLocation += hearingCourt.ToUpper() + "\n";
            else
               courtLocation += "____(court)_______\n";

            courtLocation += "§   OF \n";
            courtLocation += "§   " + county.ToUpper() + " COUNTY, TEXAS \n";

            this.txtCourtName.Text = courtLocation;

            this.txtNoticeBody.Text = @"You are hereby notified that the " +
            "above entitled and numbered cause has been set for hearing, before the Associate Judge at the " + (string.IsNullOrEmpty(hearingCourt) ? "____(court)____" : hearingCourt.ToUpper())
                + ", at the " + county.ToUpper() + " COUNTY COURTHOUSE, " + hearingDate.Value.ToShortDateString() + " at " + hearingTime + "." + System.Environment.NewLine + System.Environment.NewLine
            + @"All parties and all attorneys must appear in person at this hearing. ALL PARTIES ARE NOTIFIED THAT FAILURE TO APPEAR MAY RESULT IN THE ISSUANCE OF A DEFAULT ORDER, OR ARREST OF A PARTY, UPON PROPER MOTION.";
        }

    }

    private DataSet GetPersonsData()
    {
        return Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
   CommandType.StoredProcedure,
   "GetActionPersons",
   new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID));
    }

    protected void grdPersons_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        DataSet ds = GetPersonsData();

        if (ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].DefaultView.Sort = "BirthDate DESC";
        }

        string filter = @"PersonTypeName <> 'Child'";
        ((Telerik.Web.UI.RadGrid)source).DataSource = ds.Tables[0].Select(filter);
    }
    protected void btnGenNotice_Click(object sender, EventArgs e)
    {
        var doc1 = new Document();
        MemoryStream os = new MemoryStream();
        PdfWriter writer = PdfWriter.GetInstance(doc1, os);
        writer.CloseStream = false;

        doc1.Open();
        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = null;
        if (!string.IsNullOrEmpty(this.txtNcpName.Text))
        {
            Phrase p = new Phrase();
            times = new Font(bfTimes, 12, Font.NORMAL, BaseColor.BLACK);
            p.Add(new Chunk("NCP Name:     ", times));
            times = new Font(bfTimes, 12, Font.ITALIC, BaseColor.BLACK);
            p.Add(new Chunk(this.txtNcpName.Text.ToUpper() + "\n", times));
            doc1.Add(p);
        }
        if (!string.IsNullOrEmpty(this.txtCpName.Text))
        {
            Phrase p = new Phrase();
            times = new Font(bfTimes, 12, Font.NORMAL, BaseColor.BLACK);
            p.Add(new Chunk("CP Name:        ", times));
            times = new Font(bfTimes, 12, Font.ITALIC, BaseColor.BLACK);
            p.Add(new Chunk(this.txtCpName.Text.ToUpper() + "\n", times));
            doc1.Add(p);
        }
        if (!string.IsNullOrEmpty(this.txtOagNumber.Text))
        {
            Phrase p = new Phrase();
            times = new Font(bfTimes, 12, Font.NORMAL, BaseColor.BLACK);
            p.Add(new Chunk("OAG Number:  ", times));
            times = new Font(bfTimes, 12, Font.BOLD, BaseColor.BLACK);
            p.Add(new Chunk(this.txtOagNumber.Text + "\n", times));
            doc1.Add(p);
        }

        Phrase p2 = new Phrase();
        times = new Font(bfTimes, 12, Font.BOLD, BaseColor.BLACK);
        p2.Add(new Chunk("CAUSE NUMBER ", times));
        times = new Font(bfTimes, 12, Font.BOLDITALIC, BaseColor.BLACK);
        p2.Add(new Chunk(this.lblCauseNumber.Text, times));

        PdfPTable table = new PdfPTable(2);

        PdfPCell cell = new PdfPCell(p2);
        cell.Colspan = 2;
        cell.Border = 0;
        cell.HorizontalAlignment = 1;
        table.AddCell(cell);

        PdfPCell cLeft = new PdfPCell();
        cLeft.Colspan = 1;
        cLeft.Border = 0;
        times = new Font(bfTimes, 12, Font.NORMAL, BaseColor.BLACK);
        cLeft.AddElement(new Chunk(this.txtChildrenNames.Text, times));
        table.AddCell(cLeft);

        PdfPCell cRight = new PdfPCell();
        cRight.Colspan = 1;
        cRight.Border = 0;
        cRight.AddElement(new Chunk(this.txtCourtName.Text, times));
        table.AddCell(cRight);
        
        times = new Font(bfTimes, 12, Font.BOLD, BaseColor.BLACK);
        cell = new PdfPCell(new Phrase(new Chunk("\n\nNOTICE OF HEARING", times)));
        cell.Colspan = 2;
        cell.Border = 0;
        cell.HorizontalAlignment = 1;
        table.AddCell(cell);

        cell = new PdfPCell();
        cell.Colspan = 2;
        cell.Border = 0;
        cell.HorizontalAlignment = 1;
        times = new Font(bfTimes, 12, Font.NORMAL, BaseColor.BLACK);
        Phrase body = new Phrase(new Chunk(this.txtNoticeBody.Text + "\n\n\n\n", times));
        cell.AddElement(body);
        table.AddCell(cell);
        doc1.Add(table);

        Chunk underline = new Chunk("                                              \n");
        underline.SetUnderline(0.5f, 0);
        doc1.Add(underline);

        doc1.Add(new Chunk("Associate Judge\n\n\n", times));
        doc1.Add(new Chunk(this.txtMailtoList.Text, times));

        Font times1 = new Font(bfTimes, 12, Font.UNDERLINE, BaseColor.BLACK);
        doc1.Add(new Chunk(DateTime.Now.ToShortDateString(), times1));
        doc1.Add(new Chunk("\nDate Signed", times));
        
        doc1.Close();
        os.Seek(0, SeekOrigin.Begin);

        Response.Clear();
        Response.ContentType = "application/octet-stream";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.{1}", "notice", "pdf"));
        Response.BinaryWrite(os.ToArray());
        Response.Flush();
        Response.End();

    }

    protected override void OnPreRender(EventArgs e)
    {
        ((CSCMSPlain)Page.Master).SetFooterLink1Url(this.Page.Request.ApplicationRoot() + "/MemberPages/MyPage.aspx", "MyPage");
        base.OnPreRender(e);
    }

    protected void ToggleRowSelection(object sender, EventArgs e)
    {
        ((sender as CheckBox).NamingContainer as GridItem).Selected = (sender as CheckBox).Checked;
        bool checkHeader = true;
        foreach (GridDataItem dataItem in grdPersons.MasterTableView.Items)
        {
            if (!(dataItem.FindControl("ckbRowSelect") as CheckBox).Checked)
            {
                checkHeader = false;
                break;
            }
        }
        GridHeaderItem headerItem = grdPersons.MasterTableView.GetItems(GridItemType.Header)[0] as GridHeaderItem;
        (headerItem.FindControl("ckbHeaderSelect") as CheckBox).Checked = checkHeader;

        BuildMailtoList();
    }
    protected void ToggleSelectedState(object sender, EventArgs e)
    {
        CheckBox headerCheckBox = (sender as CheckBox);
        foreach (GridDataItem dataItem in grdPersons.MasterTableView.Items)
        {
            (dataItem.FindControl("ckbRowSelect") as CheckBox).Checked = headerCheckBox.Checked;
            dataItem.Selected = headerCheckBox.Checked;
        }
        if (headerCheckBox.Checked)
            BuildMailtoList();
        else
            txtMailtoList.Text = "";
    }

    private void BuildMailtoList()
    {
        string mailtoList = "";
        foreach (GridDataItem item in grdPersons.MasterTableView.Items)
        {
            if ((item.FindControl("ckbRowSelect") as CheckBox).Checked)
            {
                string add1 = "";
                string add2 = "";
                string add3 = "";
                string city = "";
                string state = "";
                string zip = "";
                Guid id = new Guid(item["PersonGUID"].Text);
                string name = "";
                if (item["PersonTypeName"].Text != "Attorney")
                {
                    CSCMS.Data.ActionPerson ap = CscmsContext.ActionPerson.Include("State").Include("Person").Where(it => it.ActionGUID == actionGuid && it.PersonGUID == id).FirstOrDefault();
                    name = Helper.GetFormattedName(ap.Person.FirstName, ap.Person.MiddleName, ap.Person.LastName, ap.Person.NameSuffix);
                    add1 = ap.Address1;
                    add2 = ap.Address2;
                    add3 = ap.Address3;
                    city = ap.City;
                    if (ap.State != null)
                        state = ap.State.StateName;
                    zip = (ap.ZipCode != null) ? ap.ZipCode.ToString() : "";
                }
                else
                {
                    CSCMS.Data.Person p = CscmsContext.Person.Include("State").Where(it => it.PersonGUID == id).FirstOrDefault();
                    name = Helper.GetFormattedName(p.FirstName, p.MiddleName, p.LastName, p.NameSuffix);
                }


                mailtoList += (name + "\n");
                string add = "";
                if (!string.IsNullOrEmpty(add1))
                    add += (add1 + "\n");

                if (!string.IsNullOrEmpty(add2))
                    add += (add2 + "\n");

                if (!string.IsNullOrEmpty(add3))
                    add += (add3 + "\n");

                if (!string.IsNullOrEmpty(city))
                    add += city + ", ";

                if (!string.IsNullOrEmpty(state))
                    add += state + " ";

                if (!string.IsNullOrEmpty(zip))
                    add += zip;
                mailtoList += (add + "\n\n");

            }
        }
        txtMailtoList.Text = mailtoList;
    }

}
