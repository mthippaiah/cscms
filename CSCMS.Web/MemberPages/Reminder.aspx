﻿<%@ Page Title="CSCMS | Reminder" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="Reminder.aspx.cs" Inherits="MemberPages_Reminder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="UC" TagName="CaseReminder" Src="~/UserControls/CaseReminder.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div id="pageTitle" runat="server" class="pageTitle" style="float: left">
        MANAGE REMINDER</div>
    <!-- buttons -->
    <div style="clear: both;">
    </div>
    <!-- MessageBox -->
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" />
    <!-- sectionSearch -->
    <CSCMSUC:SectionContainer ID="sectionSearch" runat="server" HeaderText="User/Court Reminder"
        Width="100%">
      <%--  DefaultButton="btnSearch"--%>
        <asp:Panel ID="pnlPerson" runat="server" >
            <UC:CaseReminder ID="caseReminder1" runat="server" />
        </asp:Panel>
    </CSCMSUC:SectionContainer>
</asp:Content>
