﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PopUpConfirm.aspx.cs" Inherits="MemberPages_PopUpConfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="stylesheet" href="~/styles/stylesheet_2.css" />
    <style type="text/css">
        body { background-color: #ffffff; }
        .messageText { font-size: 11px; font-weight: bold; color: #336699; }
    </style>
    <script language="javascript" type="text/javascript">
        function GetRadWindow()
        {
            var win = null;
            if (window.radWindow)
                win = window.radWindow;
            else if (window.frameElement.radWindow)
                win = window.frameElement.radWindow;
            return win;
        }
         
        function CloseWindow(proceed)
        {
            var win = GetRadWindow();
            win.close(proceed);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="padding: 10px;">
        <table cellpadding="5" align="center">
            <tr>
                <td><img src="../images/icon_question.png" width="44" height="44" /></td>
                <td class="messageText"><asp:Label ID="lblMessage" runat="server" /></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="submit" Width="60" OnClick="btnOK_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="submit" Width="60" OnClientClick="CloseWindow(false); return false;" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td style="color: #114477;"><asp:CheckBox ID="chkSuppress" runat="server" Text="Do not show this message again" /></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
