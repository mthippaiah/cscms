﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MemberPages_DaysOff : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void dsDaysOff_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        ValidateForm(e);
    }

    protected void dsDaysOff_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {

    }

    protected void dsDaysOff_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID;
        e.Command.Parameters["@CreateDate"].Value = DateTime.Now;
        e.Command.Parameters["@LastUpdateDate"].Value = DateTime.Now;

        ValidateForm(e);
    }

    private void ValidateForm(SqlDataSourceCommandEventArgs e)
    {
        if ((e.Command.Parameters["@TimePeriod1StartTime"].Value != null && e.Command.Parameters["@TimePeriod1EndTime"].Value == null) ||
            (e.Command.Parameters["@TimePeriod1EndTime"].Value != null && e.Command.Parameters["@TimePeriod1StartTime"].Value == null) ||
            (e.Command.Parameters["@TimePeriod1EndTime"].Value != null && e.Command.Parameters["@TimePeriod1StartTime"].Value != null && (DateTime)e.Command.Parameters["@TimePeriod1EndTime"].Value <= (DateTime)e.Command.Parameters["@TimePeriod1StartTime"].Value))
        {
            e.Command.Parameters["@TimePeriod1StartTime"].Value = null;
            e.Command.Parameters["@TimePeriod1EndTime"].Value = null;
            throw new Exception("An error occured while updating the appointment. Please ensure that both a start and end time are entered. The end time must also occur after the start time.");
        }

        if ((e.Command.Parameters["@TimePeriod2StartTime"].Value != null && e.Command.Parameters["@TimePeriod2EndTime"].Value == null) ||
            (e.Command.Parameters["@TimePeriod2EndTime"].Value != null && e.Command.Parameters["@TimePeriod2StartTime"].Value == null) ||
            (e.Command.Parameters["@TimePeriod2EndTime"].Value != null && e.Command.Parameters["@TimePeriod2StartTime"].Value != null && (DateTime)e.Command.Parameters["@TimePeriod2EndTime"].Value <= (DateTime)e.Command.Parameters["@TimePeriod2StartTime"].Value))
        {
            e.Command.Parameters["@TimePeriod2StartTime"].Value = null;
            e.Command.Parameters["@TimePeriod2EndTime"].Value = null;
            throw new Exception("An error occured while updating the appointment. Please ensure that both a start and end time are entered. The end time must also occur after the start time.");
        }

        FixTimePeriod(e, "@TimePeriod1StartTime");
        FixTimePeriod(e, "@TimePeriod1EndTime");
        FixTimePeriod(e, "@TimePeriod2StartTime");
        FixTimePeriod(e, "@TimePeriod2EndTime");
    }

    private static void FixTimePeriod(SqlDataSourceCommandEventArgs e, string parameterName)
    {
        DateTime selectedDate = (DateTime)e.Command.Parameters["@ExceptionDate"].Value;

        if (e.Command.Parameters[parameterName].Value != null)
        {
            TimeSpan ts = ((DateTime)e.Command.Parameters[parameterName].Value).TimeOfDay;
            e.Command.Parameters[parameterName].Value = selectedDate.Add(ts);
        }
    }

    protected void grdDaysOff_ItemInserted(object source, GridInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
            DisplayMessage(e.Exception.Message);
        }
    }

    protected void grdDaysOff_ItemUpdated(object source, GridUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
            DisplayMessage(e.Exception.Message);
        }
    }

    private void DisplayMessage(string text)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "EditError", string.Format("alert('{0}')", text), true);
    }

    protected void dsDaysOff_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@OCACourtID"].Value = this.CurrentUser.OCACourtID;
    }

    protected void grdDaysOff_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.InitInsertCommandName)
        {
            // cancel the default operation
            e.Canceled = true;

            //Prepare an IDictionary with the predefined values
            System.Collections.Specialized.ListDictionary newValues = new System.Collections.Specialized.ListDictionary();
            newValues["ExceptionDate"] = null;
            newValues["TimePeriod1StartTime"] = null;
            newValues["TimePeriod1EndTime"] = null;
            newValues["TimePeriod2StartTime"] = null;
            newValues["TimePeriod2EndTime"] = null;
            newValues["BlockMorning"] = false;
            newValues["BlockAfternoon"] = false;
            newValues["Note"] = "";

            //Insert the item and rebind
            e.Item.OwnerTableView.InsertItem(newValues);

        }
    }
}
