﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CSCMS.WebControls;
using Telerik.Web.UI;
using AjaxControlToolkit;
using System.Data.Objects;

/// <summary>
/// 
/// </summary>
public partial class MemberPages_ActionDetail : ActionBasePage
{
    static List<string> listCapiasOrders = new List<string> { MyConsts.DB_CAPIAS_FTA, MyConsts.DB_CAPIAS_ATTACHMENT, MyConsts.DB_CAPIAS_MTRP, MyConsts.DB_CAPIAS_DEFERRED_COMMITMENT, MyConsts.DB_CAPIAS_COMPLIANCE, MyConsts.DB_COMMITMENT };
    #region [Properties]
    private const int MAX_RELATED_CASE_LENGTH = 50;
    UserControls_ChildSupportCalculation childSupportCalcControl = null;
    bool bSetFous = false;
    bool bPersonEdited = false;
    protected string chkNoIdentifiedAbuseClientID
    {
        get { return ViewState["chkNoIdentifiedAbuseClientID"] as string; }
        set { ViewState["chkNoIdentifiedAbuseClientID"] = value; }
    }

    protected string ModPopClienIDClientID
    {
        get
        {
            return fvCase.FindControl("scChildSupport").FindControl("ModalPopupExtender1").ClientID;
        }
    }

    public string GetAddPersonUrl(int arg)
    {
        return string.Format("PersonDetail.aspx?cid={0}&a={1}&pid=&t={2}", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber, arg.ToString());
    }

    public string GetAddNewDocumentUrl()
    {
        return string.Format("DocumentDetail.aspx?cid={0}&a={1}", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
    }

    public string GetAddHearingUrl()
    {
        return string.Format("HearingDetail.aspx?cid={0}&a={1}", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
    }

    public string GetAddContinuanceUrl()
    {
        return string.Format("ContinuanceDetail.aspx?cid={0}&a={1}", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
    }

    public string GetAddCapiasOrderUrl()
    {
        return string.Format("OrderDetail.aspx?cid={0}&a={1}&t=A", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
    }

    public string GetAddPaternityTestOrderUrl()
    {
        return string.Format("OrderDetail.aspx?cid={0}&a={1}&t=A&PatOrder=1", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
    }

    public string GetAddCommitmentOrderUrl()
    {
        return string.Format("OrderDetail.aspx?cid={0}&a={1}&t=O", this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
    }

    public string TransferToOCACOurtIDString
    {
        get
        {
            if (this.CurrentCause != null && this.CurrentCause.OCACourt1 != null)
                return this.CurrentCause.OCACourt1.OCACourtID.ToString();

            return "";
        }
    }

    public string OriginatingCourtIDString
    {
        get
        {
            if (this.CurrentCause != null && this.CurrentCause.OriginatingCourt != null)
                return this.CurrentCause.OriginatingCourt.OriginatingCourtID.ToString();

            return "";
        }
    }

    public string TransferToCountyIDString
    {
        get
        {
            if (this.CurrentCause != null && this.CurrentCause.County1 != null)
                return this.CurrentCause.County1.CountyID.ToString();

            return "";
        }
    }

    public string DispositionTypeIDString
    {
        get
        {
            if (this.CurrentAction != null && this.CurrentAction.DispositionType != null)
                return this.CurrentAction.DispositionType.DispositionTypeID.ToString();

            return "";
        }
    }

    public string DispositionDetailIDString
    {
        get
        {
            if (this.CurrentAction != null && this.CurrentAction.DispositionDetail != null)
                return this.CurrentAction.DispositionDetail.DispositionDetailID.ToString();

            return "";
        }
    }

    public string DispositionRenderedDateClientID
    {
        get
        {
            RadDatePicker dtDispositionRenderedDate = ((RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtDispositionRenderedDate"));
            return dtDispositionRenderedDate.ClientID;
        }
    }

    public string DispositionDropDownClientID
    {
        get
        {
            DropDownList ddlDisposition = ((DropDownList)fvCase.FindControl("sectionCase").FindControl("ddlDisposition"));
            return ddlDisposition.ClientID;
        }
    }

    public bool IsNewAction
    {
        get
        {
            if (this.Request.QueryString["message"] != null && this.Request.QueryString["message"] == "AddAction")
                return true;

            return false;
        }
    }

    #endregion

    #region [Page]

    protected DateTime? ConvertToTime(object o)
    {
        string t = o.ToString();

        if (!string.IsNullOrEmpty(t))
            return System.Convert.ToDateTime(t);

        return null;
    }

    protected List<OCACourtData> GetTransferToCourtList()
    {
        if (this.CurrentCause != null && this.CurrentCause.County1 != null)
        {
            return GetTransferToCourtList(this.CurrentCause.County1.CountyID);
        }
        return new List<OCACourtData>();
    }

    private List<OCACourtData> GetTransferToCourtList(short countyId)
    {
        List<OCACourtData> list = new List<OCACourtData>();

        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            var query = (from it in CscmsContext.OCACourtCounty
                         where it.County.CountyID == countyId || it.OCACourt.CourtName == "Unknown"
                         orderby it.OCACourt.CourtName
                         select new
                         {
                             courtName = it.OCACourt.CourtName,
                             Id = it.OCACourt.OCACourtID
                         }).Distinct();

            ((ObjectQuery)query).MergeOption = MergeOption.NoTracking;
            foreach (var item in query)
                list.Add(new OCACourtData { CourtName = item.courtName, OCACourtID = item.Id.ToString() });

            list = list.OrderBy(it => it.CourtName).ToList();
        }
        return list;
    }

    protected List<CSCMS.Data.DispositionType> GetDispositionTypes()
    {
        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            CscmsContext.DispositionType.MergeOption = MergeOption.NoTracking;
            List<CSCMS.Data.DispositionType> list = CscmsContext.DispositionType.Where(it => it.IsActive == true).ToList();
            list = list.Where(it => !it.DispositionTypeName.Contains(MyConsts.DB_NONE_CASE_OPEN)).ToList();

            if (this.CurrentUser.UserType != UserType.OCAAdmin)
                list = list.Where(it => it.DispositionTypeName != MyConsts.DB_SYSTEM_ADMIN_CLOSURE).ToList();

            if (this.CurrentAction != null)
            {
                if (this.CurrentAction.DispositionType != null)
                {
                    CSCMS.Data.DispositionType temp = list.Where(it => it.DispositionTypeID == this.CurrentAction.DispositionType.DispositionTypeID).FirstOrDefault();
                    if (temp == null)
                    {
                        temp = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == this.CurrentAction.DispositionType.DispositionTypeID).FirstOrDefault();
                        list.Add(temp);
                    }
                }
            }
            list = list.OrderBy(it => it.SortOrder).ToList();
            return list;
        }
    }

    protected List<CSCMS.Data.OriginatingCourt> GetOriginatingCourts()
    {
        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.Connection.Open();
            CscmsContext.OriginatingCourt.MergeOption = MergeOption.NoTracking;
            List<CSCMS.Data.OriginatingCourt> list = CscmsContext.OriginatingCourt.Where(it => it.OCACourt.OCACourtID == this.CurrentCause.OCACourt.OCACourtID && it.County.CountyID == this.CurrentCause.County.CountyID && (it.ExpirationDate == null || it.ExpirationDate > DateTime.Now)).ToList();
            if (this.CurrentCause != null)
            {
                if (this.CurrentCause.OriginatingCourt != null)
                {
                    CSCMS.Data.OriginatingCourt temp = list.Where(it => it.OriginatingCourtID == this.CurrentCause.OriginatingCourt.OriginatingCourtID).FirstOrDefault();
                    if (temp == null)
                    {
                        temp = CscmsContext.OriginatingCourt.Where(it => it.OriginatingCourtID == this.CurrentCause.OriginatingCourt.OriginatingCourtID).FirstOrDefault();
                        list.Add(temp);
                    }
                }
            }
            list = list.OrderBy(it => it.SortOrder).ToList();
            return list;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        inputIsAdmin.Value = this.CurrentUser.UserType.ToString();
        caseNavigation.MessageBox = this.messageBox;
        if (!IsPostBack)
        {
            bool iOpenedCon = false;
            bSetFous = true;

            try
            {
                if (CscmsContext.Connection.State != ConnectionState.Open)
                {
                    CscmsContext.Connection.Open();
                    iOpenedCon = true;
                }

                if (Request.QueryString["message"] == "AddAction")
                {
                    messageBox.SuccessMessage = "New action was added successfully.";
                }
                else if (Request.QueryString["message"] == "SavePerson")
                {
                    messageBox.SuccessMessage = "Person was saved successfully.";
                }
                else if (Request.QueryString["message"] == "DeleteAction")
                {
                    messageBox.SuccessMessage = "Last action was deleted successfully from this case.";
                }
                else if (Request.QueryString["message"] == "DeletePerson")
                {
                    messageBox.SuccessMessage = "Person was deleted successfully from this action.";
                }
                else if (Request.QueryString["message"] == "NewCaseCreated")
                {
                    messageBox.SuccessMessage = "New case was created successfully.";

                    GetHearingbyCountyCount(); // TODO

                }
                else if (Request.QueryString["message"] == "DeleteHearing")
                {
                    messageBox.SuccessMessage = "Hearing was deleted successfully from this case.";
                }
                else if (Request.QueryString["message"] == "DeleteDocument")
                {
                    messageBox.SuccessMessage = "Document was deleted successfully from this case.";
                }
                else if (Request.QueryString["message"] == "DeleteContinuance")
                {
                    messageBox.SuccessMessage = "Continuance was deleted successfully for this case.";
                }
                else if (Request.QueryString["message"] == "DeleteSpecialOrder")
                {
                    messageBox.SuccessMessage = "Order was deleted successfully for this case.";
                }
                else if (Request.QueryString["message"] == "SaveContinuance")
                {
                    messageBox.SuccessMessage = "Continuance was saved successfully for this case.";
                    GetHearingbyCountyCount();
                }
                else if (Request.QueryString["message"] == "SaveDocument")
                {
                    messageBox.SuccessMessage = "Document was saved successfully for this case.";
                }
                else if (Request.QueryString["message"] == "SaveSpecialOrder")
                {
                    messageBox.SuccessMessage = "Order was saved successfully for this case.";
                }
                else if (Request.QueryString["message"] == "SaveHearing")
                {
                    messageBox.SuccessMessage = "Hearing was saved successfully for this case.";
                    GetHearingbyCountyCount();
                }
                ddlCaseFunction.Attributes.Add("onchange", "showConfirmDialog(); return false;");

                if (Request.QueryString["cid"] != null)
                {
                    LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));
                }
                else if (Request.QueryString["cn"] != null)
                {
                    string cn = Request.QueryString["cn"];
                    CSCMS.Data.Cause c = CscmsContext.Cause.Where(it => it.CauseNumber == cn && it.OCACourt.OCACourtID == this.CurrentUser.OCACourtID).FirstOrDefault();
                    CSCMS.Data.Action a = CscmsContext.Action.Where(it => it.Cause.CauseGUID == c.CauseGUID).OrderByDescending(it => it.ActionNumber).FirstOrDefault();
                    LoadCause(c.CauseGUID, a.ActionNumber);
                }

                aGenogram2.NavigateUrl += "?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + (((BasePage)this.Page).CurrentCause.NumberOfAction);

                fvCase.DataSource = new CSCMS.Data.Cause[] { this.CurrentCause };
                fvCase.DataBind(); // fvCase_DataBound

                if (this.CurrentUser.ViewAllUser && Convert.ToBoolean(Session["Impersonating"]) == false)
                {
                    ((Button)fvCase.FindControl("sectionCase").FindControl("btnSave")).Visible = false;
                    //((Button)fvCase.FindControl("sectionCase").FindControl("btnSaveAdd")).Visible = false; //TODO: This line gives error
                    ddlCaseFunction.Enabled = false;
                    ((Button)fvCase.FindControl("sectionCase").FindControl("btnAddRelatedCase")).Visible = false;
                }

                if (this.CurrentAction.DispositionType != null)
                    inputOriginalDisposition.Value = this.CurrentAction.DispositionType.DispositionTypeName;
            }
            finally
            {
                if (iOpenedCon && CscmsContext.Connection.State == ConnectionState.Open)
                    CscmsContext.Connection.Close();
            }
        }

        childSupportCalcControl = (UserControls_ChildSupportCalculation)fvCase.FindControl("scChildSupport").FindControl("ChildSupportCalculation");
        childSupportCalcControl.CurrentActionGuid = this.CurrentAction.ActionGUID;

        RadNumericTextBox txtPP1 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtPP1");
        RadNumericTextBox txtArrearage = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtArrearage");
        RadNumericTextBox txtPP2 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtPP2");
        RadNumericTextBox txtMedicalPastDue = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMedicalPastDue");
        RadNumericTextBox txtMS2 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMS2");
        RadNumericTextBox txtTotal = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtTotal");

        childSupportCalcControl.DestinationMonthlyAmountTextBoxClientID = txtPP1.ClientID;
        childSupportCalcControl.DestinationArrearsAmountTextBoxClientID = txtArrearage.ClientID;
        childSupportCalcControl.DestinationArrearsPaymentAmountTextBoxClientID = txtPP2.ClientID;
        childSupportCalcControl.DestinationMedicalPastDueAmountTextBoxClientID = txtMedicalPastDue.ClientID;
        childSupportCalcControl.DestinationMedicalPastDuePaymentAmountTextBoxClientID = txtMS2.ClientID;
    }

    private void GetHearingbyCountyCount()
    {
        if (Request.QueryString["hd"] != null && Request.QueryString["hd"] != "no")
        {
            messageBox.ErrorMessage = string.Empty;

            DateTime dtHearingDate = DateTime.Parse(Request.QueryString["hd"]);

            // Warn the user if this is on a weekend or day off

            if (dtHearingDate.DayOfWeek == DayOfWeek.Saturday ||
                dtHearingDate.DayOfWeek == DayOfWeek.Sunday)
            {
                messageBox.ErrorMessage = "Warning: The selected Hearing Date occurs on a weekend.";
            }
            else
            {
                CSCMS.Data.SchedulingException se = CscmsContext.SchedulingException.Where(it => it.OCACourt.OCACourtID == this.CurrentUser.OCACourtID && it.ExceptionDate == dtHearingDate && ((it.BlockMorning) || it.BlockAfternoon)).FirstOrDefault();

                if (se != null)
                {
                    messageBox.ErrorMessage = string.Format("Warning: The selected Hearing Date occurs on a scheduled day off ({0})", se.Note);
                }
            }
            Dictionary<DateTime, string> hsummary = Helper.GetDateHearingSummary(this.CscmsContext, dtHearingDate, dtHearingDate, this.CurrentUser.OCACourtID);
            if (hsummary.Count > 0)
            {
                if (hsummary.ContainsKey(dtHearingDate))
                {
                    int countyCnt = hsummary[dtHearingDate].Count(f => f.Equals('('));
                    if (countyCnt > 1)
                        messageBox.ErrorMessage += @"<br>Multiple counties hearing on scheduled hearing date: " + dtHearingDate.ToString("MM/dd/yyyy") + " " + hsummary[dtHearingDate];
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void fvCase_DataBound(object sender, EventArgs e)
    {
        RadDatePicker dtFilingDate = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtFilingDate");

        TextBox txtOAGOffice = (TextBox)fvCase.FindControl("sectionHead").FindControl("txtOAGOffice");

        TextBox txtOAG1 = (TextBox)fvCase.FindControl("sectionHead").FindControl("txtOAG1");
        TextBox txtOAG2 = (TextBox)fvCase.FindControl("sectionHead").FindControl("txtOAG2");
        TextBox txtOAG3 = (TextBox)fvCase.FindControl("sectionHead").FindControl("txtOAG3");
        Label lblPending = (Label)fvCase.FindControl("sectionHead").FindControl("lblPending");
        Label lblAppealPending = (Label)fvCase.FindControl("sectionHead").FindControl("lblAppealPending");

        Label lblServiceLastNecessaryParyDate = (Label)fvCase.FindControl("sectionCase").FindControl("lblServiceLastNecessaryParyDate");

        RadDatePicker dtDispositionRenderedDate = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtDispositionRenderedDate");
        RadDatePicker dtMedicalPastDueAsOf = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtMedicalPastDueAsOf");
        RadDatePicker dtArrearageAsOf = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtArrearageAsOf");

        RadDatePicker dtDispositionSignedDate = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtDispositionSignedDate");

        RadDatePicker dtAppealFiledDate = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtAppealFiledDate");

        Label lblEarliestDismissalDate = (Label)fvCase.FindControl("sectionCase").FindControl("lblEarliestDismissalDate");

        TextBox txtCaseOpenedNote = (TextBox)fvCase.FindControl("sectionCase").FindControl("txtCaseOpenedNote");
        DropDownList ddlPetitionersAttorney = (DropDownList)fvCase.FindControl("sectionCase").FindControl("ddlPetitionersAttorney");
        RadDatePicker dtFinalOrderSignedDate = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtFinalOrderSignedDate");
        RangeValidator rgvFinalOrderSignedDate = (RangeValidator)fvCase.FindControl("sectionCase").FindControl("rgvFinalOrderSignedDate");

        CheckBox chkShared = (CheckBox)fvCase.FindControl("sectionCase").FindControl("chkShared");
        CheckBox chkActive = (CheckBox)fvCase.FindControl("sectionCase").FindControl("chkActive");
        CheckBox chkDJS = (CheckBox)fvCase.FindControl("sectionCase").FindControl("chkDJS");

        //I need to set ddlTransferToCourt data source manually
        DropDownList ddlTransferToOCACourt = (DropDownList)fvCase.FindControl("sectionCase").FindControl("ddlTransferToOCACourt");
        ddlTransferToOCACourt.DataSource = GetTransferToCourtList();
        ddlTransferToOCACourt.DataBind();
        ddlTransferToOCACourt.SelectedValue = TransferToOCACOurtIDString;

        LoadHeaderData();
        SetLastNecessaryPartyServiceDate();

        if (this.CurrentAction.ActionFiledDate != DateTime.MinValue)
        {
            dtFilingDate.SelectedDate = this.CurrentAction.ActionFiledDate > DateTime.Now ? DateTime.Now : this.CurrentAction.ActionFiledDate;
        }

        if (this.CurrentAction.DispositionRenderedDate != DateTime.MinValue)
        {
            dtDispositionRenderedDate.SelectedDate = this.CurrentAction.DispositionRenderedDate > DateTime.Now ? DateTime.Now : this.CurrentAction.DispositionRenderedDate;
        }

        if (this.CurrentAction.DispositionSignedDate != DateTime.MinValue)
        {
            dtDispositionSignedDate.SelectedDate = this.CurrentAction.DispositionSignedDate > DateTime.Now ? DateTime.Now : this.CurrentAction.DispositionSignedDate;
        }

        if (this.CurrentAction.AppealFiledDate != DateTime.MinValue)
        {
            dtAppealFiledDate.SelectedDate = this.CurrentAction.AppealFiledDate > DateTime.Now ? DateTime.Now : this.CurrentAction.AppealFiledDate;
        }

        txtOAG1.Text = this.CurrentAction.OAGCauseNumber;
        txtOAG2.Text = this.CurrentAction.OAGCauseNumber2;
        txtOAG3.Text = this.CurrentAction.OAGCauseNumber3;

        chkShared.Checked = this.CurrentCause.SharedWithOtherCourt.HasValue ? (bool)this.CurrentCause.SharedWithOtherCourt : false;

        chkActive.Checked = this.CurrentAction.Inactive.HasValue ? (bool)this.CurrentAction.Inactive : false;
        chkDJS.Checked = this.CurrentAction.DistrictJudgeSigned.HasValue ? (bool)this.CurrentAction.DistrictJudgeSigned : false;

        RadDatePicker dtJudgeSignedDate = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtJudgeSignedDate");
        dtJudgeSignedDate.SelectedDate = this.CurrentAction.DistrictJudgeSignedDate > DateTime.Now ? DateTime.Now : this.CurrentAction.DistrictJudgeSignedDate;

        chkActive.Enabled = (lblPending.Text == "Disposed") ? true : false;

        CSCMS.Data.ChildSupport childSupport = this.CurrentAction.ChildSupport.Where(it => it.Action.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();

        RadNumericTextBox txtPP1 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtPP1");
        RadNumericTextBox txtArrearage = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtArrearage");
        RadNumericTextBox txtMedicalPastDue = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMedicalPastDue");
        RadNumericTextBox txtMS1 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMS1");
        RadNumericTextBox txtPP2 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtPP2");
        RadNumericTextBox txtMS2 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMS2");
        RadNumericTextBox txtTotal = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtTotal");

        if (childSupport != null)
        {
            if (childSupport.ChildSupportPP1.HasValue) txtPP1.Value = (double)childSupport.ChildSupportPP1;
            if (childSupport.ArrearageAndRetroActive.HasValue) txtArrearage.Value = (double)childSupport.ArrearageAndRetroActive;
            if (childSupport.PP2.HasValue) txtPP2.Value = (double)childSupport.PP2;
            if (childSupport.MedicalSupportMS1.HasValue) txtMS1.Value = (double)childSupport.MedicalSupportMS1;
            if (childSupport.PastDueMedicalSupport.HasValue) txtMedicalPastDue.Value = (double)childSupport.PastDueMedicalSupport;
            if (childSupport.MSPP2.HasValue) txtMS2.Value = (double)childSupport.MSPP2;

            txtTotal.Value = txtPP1.Value + txtMS1.Value + txtPP2.Value + txtMS2.Value;

            if (childSupport.PastDueMedicalAsOf != DateTime.MinValue)
            {
                dtMedicalPastDueAsOf.SelectedDate = childSupport.PastDueMedicalAsOf > DateTime.Now ? DateTime.Now : childSupport.PastDueMedicalAsOf;
            }
            if (childSupport.ArrearageAsOf != DateTime.MinValue)
            {
                dtArrearageAsOf.SelectedDate = childSupport.ArrearageAsOf > DateTime.Now ? DateTime.Now : childSupport.ArrearageAsOf;
            }
        }
        SetFieldVisibility();
    }
    #endregion

    private void SetLastNecessaryPartyServiceDate()
    {
        DateTime? lastNPDate = Helper.GetLastNecessaryPartyServiceDate(base.SqlConn, this.CurrentAction.ActionGUID);
        Label lblServiceLastNecessaryParyDate = (Label)fvCase.FindControl("sectionCase").FindControl("lblServiceLastNecessaryParyDate");

        if (lastNPDate != null)
        {
            lblServiceLastNecessaryParyDate.Text = lastNPDate.Value.ToString("MM/dd/yyyy");
        }
        else
            lblServiceLastNecessaryParyDate.Text = "";
    }

    #region [Load Data]

    public override void LoadCause(Guid caseGuid, short actionNumber)
    {
        base.LoadCause(caseGuid, actionNumber);

        divCaseButtons.Visible = true;
        divCaseNotes.Visible = true;
    }

    private void SetFieldVisibility()
    {
        ddlCaseFunction.Items.Clear();
        ddlCaseFunction.Items.Add(new ListItem("- Select -", ""));
        ddlCaseFunction.Items.Add(new ListItem("Add Action", "AddAction"));

        if (this.CurrentAction.ActionNumber == this.CurrentCause.NumberOfAction && this.CurrentAction.ActionNumber != 1) ddlCaseFunction.Items.Add(new ListItem("Delete Action", "DeleteAction"));

        if (this.CurrentAction.ActionNumber == this.CurrentCause.NumberOfAction && this.CurrentAction.ActionNumber == 1) ddlCaseFunction.Items.Add(new ListItem("Delete Case", "DeleteCase"));

        if (this.CurrentCause.OCACourt1 == null)
        {
            fvCase.FindControl("sectionCase").FindControl("trTranferToCounty").Visible = false;
            fvCase.FindControl("sectionCase").FindControl("trTransferToOCACourt").Visible = false;
        }

        if (this.CurrentAction.ActionType.ActionTypeID == (short)ActionTypeEnum.MotionToTransfer && this.CurrentAction.DispositionSignedDate != null)
        {
            fvCase.FindControl("sectionCase").FindControl("trTranferToCounty").Visible = true;
            fvCase.FindControl("sectionCase").FindControl("trTransferToOCACourt").Visible = true;
        }
        else
        {
            SectionContainer sectionCase = (SectionContainer)fvCase.FindControl("sectionCase");
            Panel pnlCase = (Panel)sectionCase.FindControl("pnlCase");
            DropDownList ddlTransferToOCACourt = (DropDownList)pnlCase.FindControl("ddlTransferToOCACourt");
            ddlTransferToOCACourt.Enabled = false;
        }

        fvCase.FindControl("sectionPersons").Visible = true;
        fvCase.FindControl("sectionActionHearings").Visible = true;

        divMain.Attributes["class"] = "existingCase";
        divMainSub.Attributes["class"] = "existingCaseSub";


    }

    protected string CalculateAge(object birthDate)
    {
        if (birthDate != System.DBNull.Value && (DateTime)birthDate < DateTime.Today)
        {
            TimeSpan tsAge = DateTime.Today - (DateTime)birthDate;
            DateTime dtAge = DateTime.MinValue.Add(tsAge);
            int year = dtAge.Year == 1 ? 0 : dtAge.Year - 1;
            int month = dtAge.Year == 1 && dtAge.Month == 1 ? 0 : dtAge.Month - 1;
            return string.Format("{0} Yrs {1} Mos", year, month);
        }
        else
        {
            return "&nbsp;";
        }
    }

    protected string GetRelationships(object relationships)
    {
        string temp = (relationships != System.DBNull.Value) ? (string)relationships : null;

        if (string.IsNullOrEmpty(temp))
        {
            return "&nbsp;";
        }
        else
        {
            return temp.Replace("|", "<br />");
        }
    }

    public new string[] GetDocuments(string documentIDs)
    {
        if (!string.IsNullOrEmpty(documentIDs))
        {
            if (documentIDs.EndsWith("|"))
            {
                documentIDs = documentIDs.Substring(0, documentIDs.Length - 1);
            }

            return documentIDs.Split('|');
        }
        else
        {
            return null;
        }
    }

    protected void rptDocuments_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ImageButton btnDocument = (ImageButton)e.Item.FindControl("btnDocument");
            btnDocument.OnClientClick = String.Format(@"{0}.__doPostBack(""{1}"",""{2}"");return false;", ajaxManager.ClientID, btnDocument.UniqueID, btnDocument.CommandArgument);
        }
    }
    #endregion

    #region [Buttons]

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bSetFous = true;
        //Clear HeaderData cache
        Session["ActionHeaderData"] = null;
        bool iOpenedCon = false;
        try
        {
            SectionContainer sectionCase = (SectionContainer)fvCase.FindControl("sectionCase");
            Panel pnlCase = (Panel)sectionCase.FindControl("pnlCase");
            string causeNumber = ((TextBox)pnlCase.FindControl("txtCauseNumber")).Text;
            DropDownList ddlCounty = (DropDownList)pnlCase.FindControl("ddlCounty");

            if (this.CurrentCause.CauseNumber != causeNumber)
            {
                short countyId = short.Parse(ddlCounty.SelectedValue);
                CSCMS.Data.Cause c1 = CscmsContext.Cause.Where(it => it.CauseGUID != this.CurrentCause.CauseGUID
                    && it.CauseNumber == causeNumber
                    && it.OCACourt.OCACourtID == this.CurrentCause.OCACourt.OCACourtID
                    && it.County.CountyID == countyId).FirstOrDefault();
                if (c1 != null)
                {
                    messageBox.ErrorMessage = string.Format("Cause Number already exists for selected Court and County");
                    return;
                }
            }


            CheckBox chk = (CheckBox)  sectionCase.FindControl("chkDJS");
            if(chk.Checked == true) 
            {
                RadDatePicker jsd = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtJudgeSignedDate");
                if (jsd.SelectedDate == null)
                {
                    messageBox.ErrorMessage = "Please enter the judge signed Date";
                    return;
                }
            }

            fvCase.UpdateItem(true); // fvCase_ItemUpdating
            messageBox.SuccessMessage = "Case saved successfully.";

            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedCon = true;
            }
            LoadCause(this.CurrentCause.CauseGUID, this.CurrentAction.ActionNumber);
            fvCase.DataSource = new CSCMS.Data.Cause[] { this.CurrentCause };
            fvCase.DataBind();

            SectionContainer sectionActionHearings = (SectionContainer)fvCase.FindControl("sectionActionHearings");
            RadGrid grdActions = (RadGrid)sectionActionHearings.FindControl("grdActions");
            SectionContainer sectionPersons = (SectionContainer)fvCase.FindControl("sectionPersons");
            RadGrid grdPersons = (RadGrid)sectionPersons.FindControl("grdPersons");
            SectionContainer scActionOrders = (SectionContainer)fvCase.FindControl("scActionOrders");
            RadGrid grdOrders = (RadGrid)scActionOrders.FindControl("grdOrders");
            SectionContainer scContinuance = (SectionContainer)fvCase.FindControl("scContinuance");
            RadGrid grdContinuances = (RadGrid)scContinuance.FindControl("grdContinuances");
            grdActions.Rebind();
            grdPersons.Rebind();
            grdOrders.Rebind();
            grdContinuances.Rebind();

        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.Message;
        }
        finally
        {
            if (iOpenedCon && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    private void SetControlFocus()
    {
        RadDatePicker dtDispR = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtDispositionRenderedDate");

        if (!Helper.IsSectionMinimized("ActionDetailCaseSectionMinMax") && bSetFous)
        {
            if (this.Request.QueryString["message"] != null && this.Request.QueryString["message"] == "AddAction")
            {
                SectionContainer sectionCase = (SectionContainer)fvCase.FindControl("sectionCase");
                Panel pnlCase = (Panel)sectionCase.FindControl("pnlCase");
                DropDownList ddlActionType = (DropDownList)pnlCase.FindControl("ddlActionType");
                ddlActionType.Focus();
            }
            else
            dtDispR.Focus();
        }
        else if (bPersonEdited)
        {
            RadGrid grdPerson = (RadGrid)fvCase.FindControl("sectionPersons").FindControl("grdPersons");
            foreach (GridDataItem item in grdPerson.Items)
            {
                ImageButton btn = item.Controls[2].Controls[0] as ImageButton;
                if (btn != null)
                {
                    btn.Focus();
                    break;
                }
            }
        }
        else if (caseNotes.WasSaved)
        {
            caseNotes.SetFocus();
        }
    }

    protected void ddlCaseFunction_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddlCaseFunction.SelectedValue)
        {
            case "AddAction":
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(base.SqlConn,
                   CommandType.StoredProcedure,
                   "CreateNewAction",
                   new SqlParameter("@UserId", this.CurrentUser.UserID),
                   new SqlParameter("@CauseGUID", this.CurrentCause.CauseGUID),
                   new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID));
                Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentCause.NumberOfAction) + 1).ToString() + "&message=AddAction", true);
                break;
            case "DeleteAction":

                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(base.SqlConn,
                  CommandType.StoredProcedure,
                  "DeleteLastAction",
                  new SqlParameter("@UserId", this.CurrentUser.UserID),
                  new SqlParameter("@CauseGUID", this.CurrentCause.CauseGUID),
                  new SqlParameter("@DeleteCause", Convert.ToInt32(0)));
                Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentCause.NumberOfAction) - 1).ToString() + "&message=DeleteAction", true);
                break;
            case "DeleteCase":
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(base.SqlConn,
                  CommandType.StoredProcedure,
                  "DeleteLastAction",
                  new SqlParameter("@UserId", this.CurrentUser.UserID),
                  new SqlParameter("@CauseGUID", this.CurrentCause.CauseGUID),
                  new SqlParameter("@DeleteCause", 1));
                this.CurrentCause = null;
                this.CurrentPerson = null;
                this.CurrentAction = null;
                this.CurrentActionPerson = null;

                Response.Redirect("~/MemberPages/Mypage.aspx?message=DeleteCase", true);
                break;
        }
    }

    protected void btnDeleteRelatedCase_Command(object sender, CommandEventArgs e)
    {
        bool iOpenedCon = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedCon = true;
            }
            Guid id = new Guid((string)e.CommandArgument);
            List<CSCMS.Data.RelatedCause> list = CscmsContext.RelatedCause.Where(it => (it.Cause.CauseGUID == id && it.Cause1.CauseGUID == this.CurrentCause.CauseGUID) || (it.Cause.CauseGUID == this.CurrentCause.CauseGUID && it.Cause1.CauseGUID == id)).ToList();

            foreach (CSCMS.Data.RelatedCause c in list)
                CscmsContext.DeleteObject(c);

            CscmsContext.SaveChanges();
        }
        finally
        {
            if (iOpenedCon && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }

        Repeater r = (Repeater)fvCase.FindControl("sectionCase").FindControl("rptRelatedCases");
        r.DataBind();
    }

    protected void btnDownloadDocument_Command(object sender, CommandEventArgs e)
    {
        int docId = Convert.ToInt32(e.CommandArgument);
        CSCMS.Data.Document document = CscmsContext.Document.Where(it => it.DocumentID == docId).FirstOrDefault();
        if (document != null)
        {
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.{1}", document.DocumentFileName, document.FileExtension));
            Response.BinaryWrite(document.FileData.ToArray());
            Response.Flush();
        }
    }

    #endregion

    #region [DataGrid]

    protected void grdActions_OnItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            HyperLink btnExport = dataItem["ExportNoticeColumn"].FindControl("btnExportNotice") as HyperLink;
            if ((string)((System.Data.DataRowView)dataItem.DataItem).Row["ActionType"] == "Document")
            {
                long docId = (long)((System.Data.DataRowView)dataItem.DataItem).Row["ActionID"];
                CSCMS.Data.Document doc = CscmsContext.Document.Where(it => it.DocumentID == docId).FirstOrDefault();
                btnExport.Text = doc.DocumentFileName + "." + doc.FileExtension;
                btnExport.NavigateUrl = Request.ApplicationRoot() + "/MemberPages/DocumentDetail.aspx?did=" + docId.ToString() + "&downloaddoc=true";
            }
            else if ((string)((System.Data.DataRowView)dataItem.DataItem).Row["ActionType"] == "Hearing")
            {
                DateTime date = (DateTime)((System.Data.DataRowView)dataItem.DataItem).Row["ActionDate"];
                if (date > DateTime.Now)
                {
                    e.Item.CssClass = "rgRow borderBottom";
                }
                
                // copied the next line of code here from the "else" statement because the Notice link wasn't generating the URL.
                btnExport.NavigateUrl = Request.ApplicationRoot() + "/MemberPages/ExportNotice.aspx?hid=" +((System.Data.DataRowView)dataItem.DataItem).Row["ActionID"];
            }
            // This is where Ravi had the code that generates the URL for the Notice link but it wasn't working because of the "else if" statement above.
            // I left it here because I don't know if there are other options for ActionType.
            else
                btnExport.NavigateUrl = Request.ApplicationRoot() + "/MemberPages/ExportNotice.aspx?hid=" + ((System.Data.DataRowView)dataItem.DataItem).Row["ActionID"];
        }
    }

    protected void grdActions_DeleteCommand(object source, GridCommandEventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            SectionContainer sectionActionHearings = (SectionContainer)fvCase.FindControl("sectionActionHearings");
            RadGrid grdActions = (RadGrid)sectionActionHearings.FindControl("grdActions");

            int ActionID = Convert.ToInt32(grdActions.MasterTableView.DataKeyValues[e.Item.ItemIndex]["ActionID"]);
            string ActionType = grdActions.MasterTableView.DataKeyValues[e.Item.ItemIndex]["ActionType"].ToString();

            if (ActionType == "Hearing")
            {
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(base.SqlConn,
                    CommandType.StoredProcedure,
                    "DeleteActionHearing",
                    new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID.ToString()),
                    new SqlParameter("@HearingID", ActionID));  //TODO: This sp takes ActionGUID and HearingID. Verify if we are passing HearingID

                messageBox.InfoMessage = "Hearing Deleted.";
                if (this.CurrentAction.DispositionRenderedDate != null)
                {
                    Helper.SetActionStatusWhenHearingUpdatedOrDeleted(CscmsContext, this.CurrentAction, null);
                }
                ((CheckBox)(fvCase.FindControl("sectionCase").FindControl("chkActive"))).Checked = this.CurrentAction.Inactive != null ? this.CurrentAction.Inactive.Value : false;
                this.CurrentHearing = null;
            }
            else
            {
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(base.SqlConn,
                        CommandType.StoredProcedure,
                        "DeleteActionDocument",
                        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID.ToString()),
                        new SqlParameter("@DocumentID", ActionID));

                messageBox.InfoMessage = "Document Deleted.";

                this.CurrentDocument = null;
            }
            //Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=DeleteHearing", true);
            grdActions.Rebind();
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.Message;
            LogError.LogErrorToSQL("ActionDetail.grdActions_DeleteCommand " + ex.ToString());
            throw new Exception("ActionDetail.grdActions_DeleteCommand ", ex);
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void HandleCalculateClickedEvent(object sender, EventArgs args)
    {
        ModalPopupExtender c = (ModalPopupExtender)fvCase.FindControl("scChildSupport").FindControl("ModalPopupExtender1");
        c.Show();
    }
    #endregion

    #region [DataSet]
    protected void dsCounty_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@UserId"].Value = this.CurrentUser.UserID;
        e.Command.Parameters["@CourtID"].Value = this.CurrentUser.OCACourtID;
    }

    protected void dsRelatedActions_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@CauseGUID"].Value = this.CurrentCause.CauseGUID.ToString();
        e.Command.Parameters["@ActionGUID"].Value = this.CurrentAction.ActionGUID.ToString();
    }

    protected void grdActions_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(base.SqlConn,
        CommandType.StoredProcedure,
        "GetActionHearings",
        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
        new SqlParameter("@Filter", ""));

        if (ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].DefaultView.Sort = "ActionDate DESC";
        }
        ((Telerik.Web.UI.RadGrid)source).DataSource = ds.Tables[0].DefaultView;
    }

    protected void grdOrders_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        bool iOpnedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpnedConn = true;
            }

            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(base.SqlConn,
           CommandType.StoredProcedure,
           "GetActionOrders",
           new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
           new SqlParameter("@Filter", ""));

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    int id = (int)row["SpecialOrderID"];
                    CSCMS.Data.SpecialOrder _so = CompiledQueryList.GetSpecialOrderForSoId.Invoke(CscmsContext, id).FirstOrDefault();

                    List<CSCMS.Data.SpecialOrder> arrList = CompiledQueryList.GetSpecialOrderWithArrestDate(CscmsContext, _so.ActionPerson.Person.PersonGUID, this.CurrentAction.ActionGUID).ToList();

                    int tot = 0;
                    foreach (CSCMS.Data.SpecialOrder temp in arrList)
                    {
                        if (temp.ReleaseDate == null)
                            tot += (int)(DateTime.Now - temp.ArrestCommitmentEffectiveDate.Value).TotalDays + 1;
                        else
                            tot += (int)(temp.ReleaseDate.Value - temp.ArrestCommitmentEffectiveDate.Value).TotalDays + 1;
                    }
                    if (tot != 0)
                    {
                        row["TotalDaysIncarcerated"] = tot;
                    }
                }
                ds.Tables[0].DefaultView.Sort = "OrderedDate DESC";
            }
            ((Telerik.Web.UI.RadGrid)source).DataSource = ds.Tables[0].DefaultView;
        }
        finally
        {
            if (iOpnedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void dsReferringCourt_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@CountyID"].Value = this.CurrentCause.County.CountyID;
        e.Command.Parameters["@Date"].Value = DateTime.Today.Date;
        e.Command.Parameters["@OrgCourtID"].Value = System.DBNull.Value;
        if (this.CurrentCause.OriginatingCourt != null)
            e.Command.Parameters["@OrgCourtID"].Value = this.CurrentCause.OriginatingCourt.OriginatingCourtID;

    }

    protected void grdPersons_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(base.SqlConn,
        CommandType.StoredProcedure,
        "GetActionPersons",
        new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID));

        if (ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].DefaultView.Sort = "NPFlag desc, CustodialTypeName desc, SortOrder desc";
        }

        ((Telerik.Web.UI.RadGrid)source).DataSource = ds.Tables[0].DefaultView;
    }

    protected void dsRelatedCases_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@CauseGUID"].Value = this.CurrentCause.CauseGUID.ToString();
    }

    protected void grdContinuances_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(base.SqlConn,
       CommandType.StoredProcedure,
       "GetActionContinuances",
       new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID),
       new SqlParameter("@Filter", ""));

        if (ds.Tables[0].Rows.Count > 0)
        {
            ds.Tables[0].DefaultView.Sort = "ContinuedToDate DESC";
        }
        ((Telerik.Web.UI.RadGrid)source).DataSource = ds.Tables[0].DefaultView;
    }
    #endregion

    #region [Update Data]
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void fvCase_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        //bool? originalInactivevalue = this.CurrentAction.Inactive;
        bool wasDisposedBefore = (this.CurrentAction.DispositionRenderedDate != null);

        SectionContainer sectionCase = (SectionContainer)fvCase.FindControl("sectionCase");
        Panel pnlCase = (Panel)sectionCase.FindControl("pnlCase");
        RadDatePicker dtFinalOrderSignedDate = (RadDatePicker)pnlCase.FindControl("dtFinalOrderSignedDate");
        DropDownList ddlPetitionersAttorney = (DropDownList)pnlCase.FindControl("ddlPetitionersAttorney");
        DropDownList ddlActionType = (DropDownList)pnlCase.FindControl("ddlActionType");
        DropDownList ddlCounty = (DropDownList)pnlCase.FindControl("ddlCounty");
        RadDatePicker dtDispositionRenderedDate = (RadDatePicker)pnlCase.FindControl("dtDispositionRenderedDate");
        RadDatePicker dtDispositionSignedDate = (RadDatePicker)pnlCase.FindControl("dtDispositionSignedDate");
        RadDatePicker dtAppealFiledDate = (RadDatePicker)pnlCase.FindControl("dtAppealFiledDate");
        DropDownList ddlReferringCourt = (DropDownList)pnlCase.FindControl("ddlReferringCourt");
        DropDownList ddlTransferToCounty = (DropDownList)pnlCase.FindControl("ddlTransferToCounty");
        DropDownList ddlTransferToOCACourt = (DropDownList)pnlCase.FindControl("ddlTransferToOCACourt");
        DropDownList ddlDisposition = (DropDownList)pnlCase.FindControl("ddlDisposition");
        DropDownList ddlDispositionDetail = (DropDownList)pnlCase.FindControl("ddlDispositionDetail");
        RadNumericTextBox txtPP1 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtPP1");
        RadNumericTextBox txtArrearage = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtArrearage");
        RadNumericTextBox txtMedicalPastDue = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMedicalPastDue");
        RadNumericTextBox txtMS1 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMS1");
        RadNumericTextBox txtPP2 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtPP2");
        RadNumericTextBox txtMS2 = (RadNumericTextBox)fvCase.FindControl("scChildSupport").FindControl("txtMS2");
        RadDatePicker dtMedicalPastDueAsOf = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtMedicalPastDueAsOf");
        RadDatePicker dtArrearageAsOf = (RadDatePicker)fvCase.FindControl("sectionCase").FindControl("dtArrearageAsOf");

        string causeNumber = ((TextBox)pnlCase.FindControl("txtCauseNumber")).Text;
        short countyId = short.Parse(((DropDownList)pnlCase.FindControl("ddlCounty")).SelectedValue);
        short actionTypeId = short.Parse(((DropDownList)pnlCase.FindControl("ddlActionType")).SelectedValue);
        short? orgCourtID = !string.IsNullOrEmpty(ddlReferringCourt.SelectedValue) ? short.Parse(((DropDownList)pnlCase.FindControl("ddlReferringCourt")).SelectedValue) : (short?)null;
        short? traCountyId = !string.IsNullOrEmpty(ddlTransferToCounty.SelectedValue) ? short.Parse(ddlTransferToCounty.SelectedValue) : (short?)null;
        short? traCourtId = !string.IsNullOrEmpty(ddlTransferToOCACourt.SelectedValue) ? short.Parse(((DropDownList)pnlCase.FindControl("ddlTransferToOCACourt")).SelectedValue) : (short?)null;
        short? dispTypeId = !string.IsNullOrEmpty(ddlDisposition.SelectedValue) ? short.Parse(((DropDownList)pnlCase.FindControl("ddlDisposition")).SelectedValue) : (short?)null;
        short? ddId = !string.IsNullOrEmpty(ddlDispositionDetail.SelectedValue) ? short.Parse(((DropDownList)pnlCase.FindControl("ddlDispositionDetail")).SelectedValue) : (short?)null;

        this.CurrentCause.CauseNumber = causeNumber;
        this.CurrentAction.ActionType = CscmsContext.ActionType.Where(it => it.ActionTypeID == actionTypeId).FirstOrDefault();
        this.CurrentCause.County = CscmsContext.County.Where(it => it.CountyID == countyId).FirstOrDefault();
        this.CurrentAction.Style = ((TextBox)pnlCase.FindControl("txtStyle")).Text;
        this.CurrentAction.DispositionRenderedDate = dtDispositionRenderedDate.SelectedDate.HasValue ? dtDispositionRenderedDate.SelectedDate.Value : (DateTime?)null;
        this.CurrentAction.DispositionSignedDate = dtDispositionSignedDate.SelectedDate.HasValue ? dtDispositionSignedDate.SelectedDate.Value : (DateTime?)null;
        this.CurrentAction.AppealFiledDate = dtAppealFiledDate.SelectedDate.HasValue ? dtAppealFiledDate.SelectedDate.Value : (DateTime?)null;
        this.CurrentCause.OCAUnit = ((TextBox)pnlCase.FindControl("txtOAGOffice")).Text;
        this.CurrentAction.OAGCauseNumber = ((TextBox)pnlCase.FindControl("txtOAG1")).Text;
        this.CurrentAction.OAGCauseNumber2 = ((TextBox)pnlCase.FindControl("txtOAG2")).Text;
        this.CurrentAction.OAGCauseNumber3 = ((TextBox)pnlCase.FindControl("txtOAG3")).Text;
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (orgCourtID != null)
                this.CurrentCause.OriginatingCourt = CscmsContext.OriginatingCourt.Where(it => it.OriginatingCourtID == orgCourtID).FirstOrDefault();
            else
                this.CurrentCause.OriginatingCourt = null;

            if (traCountyId != null)
                this.CurrentCause.County1 = CscmsContext.County.Where(it => it.CountyID == traCountyId).FirstOrDefault();
            else
                this.CurrentCause.County1 = null;

            if (traCourtId != null)
                this.CurrentCause.OCACourt1 = CscmsContext.OCACourt.Where(it => it.OCACourtID == traCourtId).FirstOrDefault();
            else
                this.CurrentCause.OCACourt1 = null;

            this.CurrentAction.ActionFiledDate = ((RadDatePicker)pnlCase.FindControl("dtFilingDate")).SelectedDate.Value;

            if (dispTypeId != null)
                this.CurrentAction.DispositionType = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == dispTypeId).FirstOrDefault();
            else
                this.CurrentAction.DispositionType = null;

            if (ddId != null)
                this.CurrentAction.DispositionDetail = CscmsContext.DispositionDetail.Where(it => it.DispositionDetailID == ddId).FirstOrDefault();
            else
                this.CurrentAction.DispositionDetail = null;

            CSCMS.Data.ChildSupport childSupport = this.CurrentAction.ChildSupport.Where(d => d.Action.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();

            this.CurrentCause.SharedWithOtherCourt = ((CheckBox)(fvCase.FindControl("sectionCase").FindControl("chkShared"))).Checked;

            if (wasDisposedBefore == false && this.CurrentAction.DispositionRenderedDate.HasValue)
            {
                this.CurrentAction.Inactive = false;
                CSCMS.Data.Hearing h = CscmsContext.Hearing.Where(it => it.Action.ActionGUID == this.CurrentAction.ActionGUID && it.HearingDate > this.CurrentAction.DispositionRenderedDate).FirstOrDefault();
                if (h != null)
                {
                    this.CurrentAction.Inactive = true;
                }
            }
            else
            {
                this.CurrentAction.Inactive = ((CheckBox)(fvCase.FindControl("sectionCase").FindControl("chkActive"))).Checked;
            }

            this.CurrentAction.DistrictJudgeSigned = ((CheckBox)(fvCase.FindControl("sectionCase").FindControl("chkDJS"))).Checked;

            RadDatePicker dtJudgeSignedDate = (RadDatePicker)pnlCase.FindControl("dtJudgeSignedDate");
            this.CurrentAction.DistrictJudgeSignedDate = dtJudgeSignedDate.SelectedDate.HasValue ? dtJudgeSignedDate.SelectedDate.Value : (DateTime?)null;

            if (childSupport != null)
            {
                if (childSupport.EntityState == EntityState.Detached)
                    CscmsContext.Attach(childSupport);
            }
            else
            {
                childSupport = new CSCMS.Data.ChildSupport();
                childSupport.Action = CscmsContext.Action.Where(it => it.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();

                childSupport.CreateDate = DateTime.Now;
                childSupport.LastUpdateDate = DateTime.Now;
                CscmsContext.AddToChildSupport(childSupport);

            }
            childSupport.ChildSupportPP1 = txtPP1.Value == null ? 0 : (decimal)txtPP1.Value;
            childSupport.ArrearageAndRetroActive = txtArrearage.Value == null ? 0 : (decimal)txtArrearage.Value;
            childSupport.PP2 = txtPP2.Value == null ? 0 : (decimal)txtPP2.Value;
            childSupport.MedicalSupportMS1 = txtMS1.Value == null ? 0 : (decimal)txtMS1.Value;
            childSupport.PastDueMedicalSupport = txtMedicalPastDue.Value == null ? 0 : (decimal)txtMedicalPastDue.Value;
            childSupport.MSPP2 = txtMS2.Value == null ? 0 : (decimal)txtMS2.Value;

            childSupport.ArrearageAsOf = dtArrearageAsOf.SelectedDate.HasValue ? dtArrearageAsOf.SelectedDate.Value : (DateTime?)null;
            childSupport.PastDueMedicalAsOf = dtMedicalPastDueAsOf.SelectedDate.HasValue ? dtMedicalPastDueAsOf.SelectedDate.Value : (DateTime?)null;

            if (CurrentCause.EntityState == EntityState.Detached)
                CscmsContext.Attach(this.CurrentCause);

            if (this.CurrentAction.EntityState == EntityState.Detached)
                CscmsContext.Attach(this.CurrentAction);

            CscmsContext.SaveChanges();

            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(base.SqlConn,
                CommandType.StoredProcedure,
                "UpdateActionStatus",
                new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID));

            //We have decided to remove this business rule
            ////Auto populate the Active flag if there ia an hearing after disposition
            //if ((!wasDisposedBefore && this.CurrentAction.DispositionRenderedDate != null) || (wasDisposedBefore && originalInactivevalue == this.CurrentAction.Inactive))
            //{
            //    CSCMS.Data.Hearing h = CscmsContext.Hearing.Where(it => it.Action.ActionGUID == this.CurrentAction.ActionGUID && it.HearingDate > this.CurrentAction.DispositionRenderedDate).FirstOrDefault();
            //    if (h != null)
            //    {
            //        this.CurrentAction.Inactive = true;
            //    }
            //    else if (this.CurrentAction.Inactive != null)
            //    {
            //        this.CurrentAction.Inactive = false;
            //    }
            //    CscmsContext.SaveChanges();
            //}
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }

        e.Cancel = true;
    }

    protected void grdOrders_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        RadGrid grdOrders = source as RadGrid;
        GridEditableItem editedItem = e.Item as GridEditableItem;
        int specialOrderID = (int)editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["SpecialOrderID"];
        string note = ((TextBox)editedItem.FindControl("txtNote")).Text;
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            CSCMS.Data.SpecialOrder so = CscmsContext.SpecialOrder.Where(it => it.SpecialOrderID == specialOrderID).FirstOrDefault();
            if (so != null)
                so.SpecialOrderNote = note;

            CscmsContext.SaveChanges();
        }
        catch (Exception ex)
        {
            grdOrders.Controls.Add(new System.Web.UI.LiteralControl("Unable to update order note. Reason: " + ex.Message));
            e.Canceled = true;
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }
    protected void grdPersons_OnUpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        bPersonEdited = true;
        RadGrid grdPersons = source as RadGrid;
        GridEditableItem editedItem = e.Item as GridEditableItem;
        Guid personGUID = (Guid)editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["PersonGUID"];
        DateTime? svcDate = ((RadDatePicker)editedItem.FindControl("dtSvcDate")).SelectedDate;
        string serviceType = ((DropDownList)editedItem.FindControl("ddlServiceType")).SelectedValue;
        short? svcTypeId = !string.IsNullOrEmpty(serviceType) ? short.Parse(serviceType) : (short?)null;
        bool NP = ((CheckBox)editedItem.FindControl("ckbNP")).Checked;
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            CSCMS.Data.ActionPerson ap = CompiledQueryList.GetActionPerson.Invoke(CscmsContext, this.CurrentAction.ActionGUID, personGUID).FirstOrDefault();
            if (ap != null)
            {
                ap.NPFlag = NP;
            }

            CSCMS.Data.Service service = CscmsContext.Service.Where(it => it.ActionPerson.Action.ActionGUID == this.CurrentAction.ActionGUID && it.ActionPerson.Person.PersonGUID == personGUID).FirstOrDefault();
            if (service != null)
            {
                if (svcDate != null)
                {
                    service.ServiceDate = svcDate.Value;
                    if (svcTypeId != null)
                        service.ServiceType = CscmsContext.ServiceType.Where(it => it.ServiceTypeID == svcTypeId).FirstOrDefault();
                }
                else
                    CscmsContext.DeleteObject(service);
            }
            else
            {
                if (svcDate != null)
                {
                    service = new CSCMS.Data.Service();
                    service.ServiceDate = svcDate.Value;
                    if (svcTypeId != null)
                        service.ServiceType = CscmsContext.ServiceType.Where(it => it.ServiceTypeID == svcTypeId).FirstOrDefault();
                    service.ActionPerson = CompiledQueryList.GetActionPerson.Invoke(CscmsContext, this.CurrentAction.ActionGUID, personGUID).FirstOrDefault();
                    service.CreateDate = DateTime.Now;
                    service.LastUpdateDate = DateTime.Now;
                    CscmsContext.AddToService(service);
                }

            }
            CscmsContext.SaveChanges();
            SetLastNecessaryPartyServiceDate();
        }
        catch (Exception ex)
        {
            grdPersons.Controls.Add(new System.Web.UI.LiteralControl("Unable to update servicedate. Reason: " + ex.Message));
            e.Canceled = true;
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }
    protected void grdContinuances_OnUpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        RadGrid grdContinuance = source as RadGrid;
        GridEditableItem editedItem = e.Item as GridEditableItem;
        int continuanceID = (int)editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["ContinuanceID"];
        string note = ((TextBox)editedItem.FindControl("txtContinuanceNote")).Text;
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            CSCMS.Data.Continuance c = CscmsContext.Continuance.Where(it => it.ContinuanceID == continuanceID).FirstOrDefault();
            if (c != null)
                c.ContinuanceNote = note;

            CscmsContext.SaveChanges();
        }
        catch (Exception ex)
        {
            grdContinuance.Controls.Add(new System.Web.UI.LiteralControl("Unable to update continuance note. Reason: " + ex.Message));
            e.Canceled = true;
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }
    #endregion

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        SetControlFocus();
    }

    protected override void OnPreRenderComplete(EventArgs e)
    {
        SectionContainer sectionPersons = (SectionContainer)fvCase.FindControl("sectionPersons");
        RadGrid grdPersons = (RadGrid)sectionPersons.FindControl("grdPersons");

        SectionContainer scActionOrders = (SectionContainer)fvCase.FindControl("scActionOrders");
        RadGrid grdOrders = (RadGrid)scActionOrders.FindControl("grdOrders");

        SectionContainer scContinuance = (SectionContainer)fvCase.FindControl("scContinuance");
        RadGrid grdContinuances = (RadGrid)scContinuance.FindControl("grdContinuances");

        ajaxManager.AjaxSettings.AddAjaxSetting(grdPersons, grdPersons, this.loadingPanel);
        ajaxManager.AjaxSettings.AddAjaxSetting(grdOrders, grdOrders, this.loadingPanel);
        ajaxManager.AjaxSettings.AddAjaxSetting(grdContinuances, grdContinuances, this.loadingPanel);

        Action<string, string> setMinMax = delegate(string sessionkey, string sectionId)
        {
            if (Helper.IsSectionMinimized(sessionkey))
            {
                SectionContainer section = (SectionContainer)fvCase.FindControl(sectionId);
                section.Minimized = true;
            }
        };
        setMinMax("ActionDetailCaseSectionMinMax", "sectionCase");
        setMinMax("ActionDetailChildSupportSectionMinMax", "scChildSupport");
        setMinMax("ActionDetailContinuanceSectionMinMax", "scContinuance");
        base.OnPreRenderComplete(e);
    }

    protected void grdOrders_OnItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            bool iOpenedConn = false;
            try
            {
                if (CscmsContext.Connection.State != ConnectionState.Open)
                {
                    CscmsContext.Connection.Open();
                    iOpenedConn = true;
                }
                GridDataItem item = (GridDataItem)e.Item;
                int id = (int)((System.Data.DataRowView)item.DataItem).Row["SpecialOrderID"];
                HyperLink link = e.Item.Cells[3].Controls[1] as HyperLink;
                if (link.NavigateUrl.Contains("Paternity"))
                    link.NavigateUrl += "&PatOrder=1";

                CSCMS.Data.SpecialOrder _so = CompiledQueryList.GetSpecialOrderForSoId(CscmsContext, id).FirstOrDefault();

                string ordtype = (_so.SpecialOrderType != null) ? _so.SpecialOrderType.SpecialOrderTypeName : "";
                if (_so != null)
                {
                    if (_so.ArrestCommitmentEffectiveDate == null && _so.ReleaseDate == null && _so.OrderedDate != null && (listCapiasOrders.Contains(ordtype) || (_so.SpecialOrderType == null && !string.IsNullOrEmpty(_so.CommitmentOrCapias))))
                    {
                        CSCMS.Data.SpecialOrder _withdrawn = CompiledQueryList.GetWithDrawnSpecialOrder.Invoke(CscmsContext, _so.ActionPerson.Person.PersonGUID, this.CurrentAction.ActionGUID, _so.OrderedDate).FirstOrDefault();

                        if (_withdrawn == null)
                            e.Item.Cells[3].BackColor = System.Drawing.Color.Red;
                    }
                    else if ((_so.ArrestCommitmentEffectiveDate != null && _so.ReleaseDate == null) || (_so.ActionPerson != null && _so.ActionPerson.InJail.HasValue && _so.ActionPerson.InJail.Value == true))
                        e.Item.Cells[3].BackColor = System.Drawing.Color.Yellow;
                }
            }
            finally
            {
                if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                    CscmsContext.Connection.Close();
            }
        }
    }

    protected void grdPersons_OnItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            bool iOpenedConn = false;
            try
            {
                if (CscmsContext.Connection.State != ConnectionState.Open)
                {
                    CscmsContext.Connection.Open();
                    iOpenedConn = true;
                }
                GridDataItem item = (GridDataItem)e.Item;
                Guid pid = (Guid)((System.Data.DataRowView)item.DataItem).Row["PersonGUID"];
                CSCMS.Data.SpecialOrder _so = CompiledQueryList.GetCapiasSpecialOrder.Invoke(CscmsContext, pid, this.CurrentAction.ActionGUID).FirstOrDefault();

                if (_so != null)
                {
                    CSCMS.Data.SpecialOrder _withdrawn = CompiledQueryList.GetWithDrawnSpecialOrder.Invoke(CscmsContext, pid, this.CurrentAction.ActionGUID, _so.OrderedDate).FirstOrDefault();
                    if (_withdrawn == null)
                        e.Item.Cells[4].BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    _so = CompiledQueryList.GetIncarceratedSpecialOrder.Invoke(CscmsContext, pid, this.CurrentAction.ActionGUID).FirstOrDefault();

                    CSCMS.Data.ActionPerson ap = CompiledQueryList.GetActionPerson.Invoke(CscmsContext, this.CurrentAction.ActionGUID, pid).FirstOrDefault();

                    if (_so != null || (ap != null && ap.InJail.HasValue && ap.InJail.Value == true))
                        e.Item.Cells[4].BackColor = System.Drawing.Color.Yellow;
                }
                if ((string)((System.Data.DataRowView)item.DataItem).Row["PersonTypeName"] == "Child" || (string)((System.Data.DataRowView)item.DataItem).Row["PersonTypeName"] == "Attorney")
                {
                    ImageButton imageButton = (ImageButton)item["EditCommandColumn"].Controls[0];
                    imageButton.Visible = false;
                }
            }
            finally
            {
                if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                    CscmsContext.Connection.Close();
            }
        }
    }

    private static void GetEditedValues(GridEditableItem editedItem, out string note)
    {
        note = null;
        note = ((TextBox)editedItem.FindControl("txtNote")).Text;
    }

    protected void grdActions_OnItemCommand(object source, GridCommandEventArgs e)
    {
        RadGrid grdActions = source as RadGrid;
        if (e.CommandName == "Update")
        {
            GridEditableItem editedItem = e.Item as GridEditableItem;
            long Id = (long)editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["ActionID"];
            string type = (string)editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["ActionType"];
            string note = ((TextBox)editedItem.FindControl("txtNote")).Text;
            DateTime? date = ((RadDatePicker)editedItem.FindControl("dtHearingDate")).SelectedDate;
            DateTime? time = ((RadDatePicker)editedItem.FindControl("hearingTime")).SelectedDate;

            bool iOpenedConn = false;
            try
            {
                if (CscmsContext.Connection.State != ConnectionState.Open)
                {
                    CscmsContext.Connection.Open();
                    iOpenedConn = true;
                }
                if (type == "Hearing")
                {
                    CSCMS.Data.Hearing h = base.CscmsContext.Hearing.Where(it => it.HearingID == Id).FirstOrDefault();
                    if (h != null)
                    {
                        h.HearingNote = note;
                        h.HearingDate = date.Value;
                        h.HearingTime = time != null ? time.Value.TimeOfDay : (TimeSpan?)null;
                    }

                    Helper.SetActionStatusWhenHearingUpdatedOrDeleted(CscmsContext, this.CurrentAction, h);
                }
                if (type == "Document")
                {
                    CSCMS.Data.Document doc = this.CscmsContext.Document.Where(it => it.DocumentID == Id).FirstOrDefault();
                    if (doc != null)
                    {
                        doc.DocumentNote = note;
                        doc.DocumentDate = date.Value;
                    }
                }
                base.CscmsContext.SaveChanges();
            }
            catch (Exception ex)
            {
                grdActions.Controls.Add(new System.Web.UI.LiteralControl("Unable to update Note. Reason: " + ex.Message));
                e.Canceled = true;
            }
            finally
            {
                if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                    CscmsContext.Connection.Close();
            }

            grdActions.MasterTableView.ClearEditItems();
        }
    }

    protected void printCaseDetail_OnClick(object sender, EventArgs args)
    {
        DocumentDir dd = Helper.CreateDirectory(this.CurrentUser.OCACourtID.ToString(), this.CurrentUser.UserID.ToString(), "", "");
        string fileName = Path.Combine(dd.UserLevelDir + "\\", this.CurrentCause.CauseNumber + "_" + this.CurrentAction.ActionGUID.ToString() + ".pdf");
        try
        {
            if (File.Exists(fileName))
                File.Delete(fileName);

            Helper.DeleteFiles(dd.UserLevelDir, "*.pdf", DateTime.Now.AddDays(-1));
        }
        catch
        {
        }

        ActionDetailPdfBuilder.Build(this.fvCase, this.caseNotes, fileName, Helper.GetActionHeaderData(this.CurrentAction.ActionGUID));
        string filePath = Request.ApplicationRoot() + "/documents/" + this.CurrentUser.OCACourtID.ToString() + "/" + this.CurrentUser.UserID.ToString() + "/" + this.CurrentCause.CauseNumber + "_" + this.CurrentAction.ActionGUID.ToString() + ".pdf";
        string script = "<script language=javascript>window.open('" + filePath + "', 'PrintMe','height=800px,width=977px,scrollbars=1,resizable=1');</script>";
        ClientScript.RegisterStartupScript(this.GetType(), "PrintPdf", script);
    }

    protected void ddlTransferToCounty_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlTransferToCounty = sender as DropDownList;
        short countyId = short.Parse(ddlTransferToCounty.SelectedValue);
        List<OCACourtData> list = GetTransferToCourtList(countyId);
        list.Insert(0, new OCACourtData { CourtName = "- Select -", OCACourtID = "" });
        DropDownList ddlTransferToOCACourt = (DropDownList)fvCase.FindControl("sectionCase").FindControl("ddlTransferToOCACourt");
        ddlTransferToOCACourt.Items.Clear();
        ddlTransferToOCACourt.DataSource = list;
        ddlTransferToOCACourt.DataBind();
    }

    private void LoadHeaderData()
    {
        Label lblStatus = (Label)fvCase.FindControl("sectionHead").FindControl("lblStatus");
        Label lblPending = (Label)fvCase.FindControl("sectionHead").FindControl("lblPending");
        Label lblAging = (Label)fvCase.FindControl("sectionHead").FindControl("lblAging");
        Label lblRegion = (Label)fvCase.FindControl("sectionHead").FindControl("lblRegion");

        ActionHeaderData hdata = Helper.GetActionHeaderData(this.CurrentAction.ActionGUID);
        if (hdata != null)
        {
            lblRegion.Text = hdata.Region;
            lblAging.Text = hdata.ActionAge;
            lblPending.Text = hdata.PendingStatus;
            lblStatus.Text = hdata.ActiveStatus;
        }
    }

    protected void btnRefreshHeader_Click(object sender, EventArgs args)
    {
        Session["ActionHeaderData"] = null;
        LoadHeaderData();
    }
}

[Serializable]
public class OCACourtData
{
    public string CourtName { get; set; }
    public string OCACourtID { get; set; }
}