﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="PendingCases.aspx.cs" Inherits="MemberPages_PendingCases" %>

<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle" style="float: left;">
        <asp:Label runat="server" ID="lblPageLabel" Text="Case List"></asp:Label>
    </div>
    <br />
    <br />
    <div>
        <telerik:RadGrid ID="RadGridPendingCases" runat="server" AllowPaging="True" PageSize="10"
            AllowSorting="True" OnExcelMLExportStylesCreated="RadGridPendingCases_ExcelMLExportStylesCreated"
            OnNeedDataSource="RadGridPendingCases_NeedDataSource">
            <MasterTableView NoMasterRecordsText="Your search found no results. Try broadening your search criteria."
                DataKeyNames="ActionGUID">
                <Columns>
                    <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, ActionNumber"
                        DataNavigateUrlFormatString="ActionDetail.aspx?cid={0}&a={1}" SortExpression="CauseNumber"
                        DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                    <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                    <telerik:GridBoundColumn HeaderText="Action" DataField="ActionTypeName" />
                    <telerik:GridBoundColumn HeaderText="Filed Date" DataField="FileDate" DataFormatString="{0:MM/dd/yyyy}" />
                    <telerik:GridBoundColumn HeaderText="Age (Days)" DataField="AgeInDays" />
                    <telerik:GridBoundColumn HeaderText="Last Svc Date" DataField="ServiceOnLastNecessaryPartyDate"
                        DataFormatString="{0:MM/dd/yyyy}" />
                    <telerik:GridBoundColumn HeaderText="Dispostion Date" DataField="DispositionRenderedDate"
                        DataFormatString="{0:MM/dd/yyyy}" />
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
    </div>
    <br />
    <!-- btnExportAttorney -->
    <div style="float: right; padding-bottom: 5px;">
        <asp:Button ID="btnExportCase" runat="server" Text="Export to Excel" CssClass="submitMedium"
            Width="130" CausesValidation="false" OnClick="btnExportCase_Click" />
    </div>
    <br />
</asp:Content>
