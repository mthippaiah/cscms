﻿<%@ Page Title="CSCMS | Case Note" Language="C#" MasterPageFile="~/CSCMSPlain2.master"
    AutoEventWireup="true" CodeFile="CaseNote.aspx.cs" Inherits="MemberPages_CaseNote"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="UC" TagName="CaseNotes" Src="~/UserControls/CaseNotes.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <br />
    <div class="pageTitle" style="float: left">
        CAUSE NOTES</div>
    <div style="float: left; padding-bottom: 5px;" id="divHearingButtons" runat="server"
        visible="true">
    </div>
    <div style="clear: both;">
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" />
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <table>
        <tr>
            <td>
                Cause Number:
            </td>
            <td>
                <asp:TextBox ID="txtCauseNumber" runat="server" Text="test" Enabled="false"></asp:TextBox>
            </td>
            <td>
                OAG:
            </td>
            <td>
                <asp:TextBox ID="txtOAG" runat="server" Text="test" Enabled="false"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Create New Case" CssClass="submit"
                    OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
    <div id="divMain" runat="server" class="newCase">
        <UC:CaseNotes ID="caseNotes" runat="server" />
    </div>
</asp:Content>
