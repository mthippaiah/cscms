﻿<%@ Page Title="CSCMS | Attorneys" Language="C#" MasterPageFile="~/CSCMS.master"
    AutoEventWireup="true" CodeFile="Attorneys.aspx.cs" Inherits="MemberPages_Attorneys"
    EnableEventValidation="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="MetaBuilders.WebControls" Namespace="MetaBuilders.WebControls"
    TagPrefix="mb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div id="pageTitle" runat="server" class="pageTitle" style="float: left">
        MANAGE ATTORNEYS</div>
    <!-- buttons -->
    <div style="float: right; padding-bottom: 5px;">
        <asp:Button ID="btnAddAttorney" runat="server" Text="Add Attorney" CssClass="submitMedium"
            Width="130" CausesValidation="false" CommandArgument="4" OnCommand="btnAddAttorney_Command" />
    </div>
    <div style="clear: both;">
    </div>
    <!-- MessageBox -->
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" />
    <!-- sectionSearch -->
    <CSCMSUC:SectionContainer ID="sectionSearch" runat="server" HeaderText="Attorney Search"
        Width="100%">
        <asp:Panel ID="pnlPerson" runat="server" DefaultButton="btnSearch">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top">
                        <table cellspacing="3" cellpadding="1">
                            <tr>
                                <td class="fieldLabel">
                                    Last Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchLastName" runat="server" Width="200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    First Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchFirstName" runat="server" Width="200" />
                                </td>
                            </tr>
                            <tr id="tr2" runat="server">
                                <td class="fieldLabel">
                                    Firm:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearchFirm" runat="server" Width="200" MaxLength="50" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table cellspacing="3" cellpadding="1">
                            <tr>
                                <td class="fieldLabel">
                                    County:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSearchCounty" runat="server" Width="200" DataSourceID="dsCounty"
                                        DataValueField="CountyID" DataTextField="CountyName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="GetUserCounties" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                                        OnSelecting="dsCounty_Selecting">
                                        <SelectParameters>
                                            <asp:Parameter Name="UserId" Type="Int32" />
                                            <asp:Parameter Name="CourtID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Active/Inactive:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlStatus" runat="server" Width="200">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                        <asp:ListItem Value="0" Selected="True">Active</asp:ListItem>
                                        <asp:ListItem Value="1">Inactive</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="tr3" runat="server">
                                <td class="fieldLabel">
                                    Language:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSearchLanguage" runat="server" Width="200" DataSourceID="dsInteperterLanguage"
                                        DataValueField="IntepreterLanguageID" DataTextField="IntepreterLanguageName"
                                        AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnSearch" runat="server" CssClass="submit" Text="Search" Width="100"
                            OnClick="btnSearch_Click" />
                    </td>
                </tr>
                <tr>
                    <td height="5">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <asp:Button ID="btnExportAttorney" runat="server" Text="Export to Excel" CssClass="submitMedium"
                            Width="130" CausesValidation="false" OnClick="btnExportAttorney_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <!-- grdResults -->
                        <telerik:RadGrid ID="grdResults" runat="server" AllowPaging="True" PageSize="10"
                            AllowSorting="True" OnItemCommand="grdResults_ItemCommand" OnExcelMLExportStylesCreated="grdResults_ExcelMLExportStylesCreated"
                            OnExcelMLExportRowCreated="grdResults_ExcelMLExportRowCreated" OnNeedDataSource="grdResults_OnNeedDataSource">
                            <mastertableview nomasterrecordstext="Your search found no results." datakeynames="PersonGUID">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            [<asp:LinkButton ID="btnEdit" runat="server" Text="Edit" CommandName="editAttorney"
                                                Style="font-weight: normal;" />]</ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Inactive" DataField="InActive" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" />
                                    <telerik:GridBoundColumn HeaderText="First" DataField="FirstName" />
                                    <telerik:GridBoundColumn HeaderText="Last" DataField="LastName" />
                                    <telerik:GridBoundColumn HeaderText="Middle" DataField="MiddleName" />
                                    <telerik:GridBoundColumn HeaderText="Suffix" DataField="NameSuffix" />
                                    <telerik:GridTemplateColumn HeaderText="Counties" SortExpression="Counties" DataField="Counties"
                                        UniqueName="Counties">
                                        <ItemTemplate>
                                            <%# FormatCounties(Eval("Counties") as string) %></ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </mastertableview>
                            <pagerstyle alwaysvisible="true" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </CSCMSUC:SectionContainer>
    <!-- sectionDetail -->
    <CSCMSUC:SectionContainer ID="sectionDetail" runat="server" HeaderText="Attorney Detail"
        Visible="false" Width="100%">
        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSavePerson">
            <table width="100%">
                <tr>
                    <td valign="top" width="50%">
                        <table cellspacing="3" width="100%">
                            <tr>
                                <td id="tdLastName" runat="server" class="fieldLabelReq">
                                    Last Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" Width="200" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                        ErrorMessage="Please enter a Last Name">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    First Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" Width="200" MaxLength="50" />
                                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                        ErrorMessage="Please enter a First Name"  Enabled="false">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Middle Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMiddleName" runat="server" Width="200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Suffix:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSuffix" runat="server" Width="200" MaxLength="10" />
                                </td>
                            </tr>
                            <tr id="trBarCardNumber" runat="server">
                                <td class="fieldLabel">
                                    Bar Card Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBarCardNumber" runat="server" Width="200" />
                                    <asp:RangeValidator ID="rgvBarCardNumber" runat="server" ControlToValidate="txtBarCardNumber"
                                        MinimumValue="0" MaximumValue="999999999" Type="Integer" ErrorMessage="Bar Card Number is invalid">*</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr id="trFirm" runat="server">
                                <td class="fieldLabel">
                                    Firm:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirm" runat="server" Width="200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabelReq">
                                    Sex:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSex" runat="server">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                        <asp:ListItem Value="M">Male</asp:ListItem>
                                        <asp:ListItem Value="F">Female</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvSex" runat="server" ControlToValidate="ddlSex"
                                        ErrorMessage="Please select a Sex">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="10px">
                        &nbsp;
                    </td>
                    <td valign="top" width="50%">
                        <table cellspacing="3" width="100%">
                            <tr id="trCountiesOfPractice" runat="server" visible="true">
                                <td colspan="2">
                                    <CSCMSUC:SectionContainer ID="sectionCounties" runat="server" HeaderText="Counties of Practice"
                                        Width="100%" MainBackColor="#eeeeee" EnableMinimize="false">
                                        <table cellspacing="3">
                                            <tr>
                                                <td>
                                                    <mb:DualList ID="dlAttorneyCounties" runat="server" LeftDataSourceID="dsCounty" RightDataSourceID="dsSelectedCounties"
                                                        LeftDataValueField="CountyID" LeftDataTextField="CountyName" RightDataValueField="CountyID"
                                                        RightDataTextField="CountyName" LeftListLabelText="Available Counties" RightListLabelText="Selected Counties"
                                                        LeftListStyle-Height="80px" RightListStyle-Height="80px" LeftListStyle-Width="150px"
                                                        RightListStyle-Width="150px" ButtonStyle-Width="80px" />
                                                    <asp:SqlDataSource ID="dsSelectedCounties" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                        SelectCommand="select County.CountyID, County.CountyName from County, AttorneyCounty where County.CountyID = AttorneyCounty.CountyID and AttorneyCounty.PersonGUID = @PersonGUID"
                                                        OnSelecting="dsSelectedCounties_Selecting">
                                                        <SelectParameters>
                                                            <asp:Parameter Name="PersonGUID" Type="String" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                        </table>
                                    </CSCMSUC:SectionContainer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td valign="top" width="50%">
                        <table cellspacing="3" width="100%">
                            <tr id="trActive" runat="server">
                                <td class="fieldLabel">
                                    Inactive:
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkInActive" runat="server" CssClass="chkNoPad" />
                                </td>
                            </tr>
                            <tr id="trAddress1" runat="server" visible="true">
                                <td class="fieldLabel">
                                    Address 1:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress1" runat="server" Width="200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr id="trAddress2" runat="server" visible="true">
                                <td class="fieldLabel">
                                    Address 2:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress2" runat="server" Width="200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr id="trAddress3" runat="server" visible="true">
                                <td class="fieldLabel">
                                    Address 3:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress3" runat="server" Width="200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr id="trPhoneHomeHR0" runat="server">
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr id="trPhoneHomeHR1" runat="server">
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr id="trPhoneHome" runat="server">
                                <td class="fieldLabel">
                                    Home Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhoneHome" runat="server" Width="98" MaxLength="30" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr id="trPhoneOffice" runat="server">
                                <td class="fieldLabel">
                                    Office Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhoneOffice" runat="server" Width="98" MaxLength="30" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="10px">
                        &nbsp;
                    </td>
                    <td valign="top" width="50%">
                        <table cellspacing="3" width="100%">
                            <tr id="trCity" runat="server" visible="true">
                                <td class="fieldLabel">
                                    City:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCity" runat="server" Width="200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr id="trCounty" runat="server" visible="true">
                                <td class="fieldLabel">
                                    County:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCounty" runat="server" Width="204" DataSourceID="dsCounty"
                                        DataValueField="CountyID" DataTextField="CountyName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trState" runat="server" visible="true">
                                <td class="fieldLabel">
                                    State:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlState" runat="server" Width="204" DataSourceID="dsState"
                                        DataValueField="StateID" DataTextField="StateName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsState" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="SELECT StateID, StateName FROM State order by SortOrder"></asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr id="trZipCode" runat="server" visible="true">
                                <td class="fieldLabel">
                                    Zip Code:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtZipCode" runat="server" Width="50" MaxLength="5" />
                                    <asp:RangeValidator ID="rgvZipCode" runat="server" ControlToValidate="txtZipCode"
                                        MinimumValue="10000" MaximumValue="99999" Type="Integer" ErrorMessage="Zip Code is invalid">*</asp:RangeValidator>
                                    &nbsp;&nbsp;<b>Ext:</b>
                                    <asp:TextBox ID="txtZipCodeExt" runat="server" Width="40" MaxLength="4" />
                                    <asp:RangeValidator ID="rgvZipCodeExt" runat="server" ControlToValidate="txtZipCodeExt"
                                        MinimumValue="1000" MaximumValue="9999" Type="Integer" ErrorMessage="Zip Code Ext is invalid">*</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr id="trPhoneFaxHR2" runat="server">
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr id="trPhoneFax" runat="server">
                                <td class="fieldLabel">
                                    Fax:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhoneFax3" runat="server" Width="98" MaxLength="30" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr id="trPhoneCell" runat="server">
                                <td class="fieldLabel">
                                    Cell:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhoneCell" runat="server" Width="98" MaxLength="30" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr id="trEmailAddress" runat="server">
                                <td class="fieldLabel">
                                    Email Address:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmailAddress" runat="server" Width="210" MaxLength="50" />
                                    <asp:RegularExpressionValidator ID="rfvEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
                                        ErrorMessage="Please enter a valid Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr id="tr1" runat="server">
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr id="trFirstLanguage" runat="server">
                                <td class="fieldLabel">
                                    Language:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlFirstLanguage" runat="server" Width="204" DataSourceID="dsInteperterLanguage"
                                        DataValueField="IntepreterLanguageID" DataTextField="IntepreterLanguageName"
                                        AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsInteperterLanguage" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="SELECT IntepreterLanguageID, IntepreterLanguageName FROM IntepreterLanguage order by IntepreterLanguageName">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr id="trSecondLanguage" runat="server">
                                <td class="fieldLabel">
                                    Language:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSecondLanguage" runat="server" Width="204" DataSourceID="dsInteperterLanguage"
                                        DataValueField="IntepreterLanguageID" DataTextField="IntepreterLanguageName"
                                        AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trThirdLanguage" runat="server">
                                <td class="fieldLabel">
                                    Language:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlThirdLanguage" runat="server" Width="204" DataSourceID="dsInteperterLanguage"
                                        DataValueField="IntepreterLanguageID" DataTextField="IntepreterLanguageName"
                                        AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style="padding-bottom: 5px;">
                        <asp:Button ID="btnSavePerson" runat="server" Text="&nbsp;Save Attorney&nbsp;" CssClass="submitMedium"
                            OnClick="btnSave_Click" />
                        &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <table cellpadding="0" cellspacing="0">
            </table>
        </asp:Panel>
    </CSCMSUC:SectionContainer>
    <!-- Ajax -->
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
    </telerik:RadAjaxManager>
    <!-- RadAjaxLoadingPanel -->
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 80px" />
    </telerik:RadAjaxLoadingPanel>
    <!-- Export -->
    <div style="float: right; padding-bottom: 5px;">
    </div>
    <!-- dsPersons -->
    <asp:SqlDataSource ID="dsPersons" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
        SelectCommand="SearchAttorneys" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
        OnSelecting="dsPersons_Selecting">
        <SelectParameters>
            <asp:Parameter Name="OCACourtID" Type="Int16" />
            <asp:ControlParameter Name="LastName" Type="String" ControlID="txtSearchLastName"
                ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="FirstName" Type="String" ControlID="txtSearchFirstName"
                ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="Firm" Type="String" ControlID="txtSearchFirm" ConvertEmptyStringToNull="true" />
            <asp:ControlParameter Name="CountyID" Type="Int16" ControlID="ddlSearchCounty" />
            <asp:ControlParameter Name="Status" Type="String" ControlID="ddlStatus" />
            <asp:ControlParameter Name="Language" Type="String" ControlID="ddlSearchLanguage" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
