﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMSPlain.master" AutoEventWireup="true"
    CodeFile="ExportNotice.aspx.cs" Inherits="MemberPages_ExportNotice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 951px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <table border="0" width="960">
        <tr>
            <td class="style1">
                <telerik:RadGrid ID="grdPersons" runat="server" OnNeedDataSource="grdPersons_OnNeedDataSource"
                    AllowMultiRowSelection="true">
                   
                    <MasterTableView NoMasterRecordsText="There are currently no persons.">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="PersonGuid" DataField="PersonGUID" Visible="false" />
                            <telerik:GridTemplateColumn UniqueName="SendNotice">
                                <ItemTemplate>
                                    <asp:CheckBox ID="ckbRowSelect" runat="server" OnCheckedChanged="ToggleRowSelection" AutoPostBack="True" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ckbHeaderSelect" runat="server" OnCheckedChanged="ToggleSelectedState" AutoPostBack="True" />
                                </HeaderTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridCheckBoxColumn HeaderText="NP?" DataField="NPFlag" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" ReadOnly="true" />
                            <telerik:GridTemplateColumn HeaderText="Name" SortExpression="LastName, FirstName, MiddleName"
                                ReadOnly="true">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkPerson" runat="server" NavigateUrl='<%# "PersonDetail.aspx?cid=" + this.CurrentCause.CauseGUID.ToString() + "&a=" + this.CurrentAction.ActionNumber.ToString() + "&pid=" + Eval("PersonGUID").ToString() %>'><%# Eval("LastName") %>,&nbsp;<%# Eval("FirstName") %><%# Convert.ToString(Eval("MiddleName")).Length > 0 ? "&nbsp;" + Eval("MiddleName").ToString() : "" %></asp:HyperLink></ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Type" DataField="PersonTypeName" ReadOnly="true" />
                            <telerik:GridBoundColumn HeaderText="Custodial" DataField="CustodialTypeName" ReadOnly="true" />
                            <telerik:GridBoundColumn HeaderText="Attorney" DataField="Relationships" ReadOnly="true" />
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table>
                    <tr>
                        <td>
                            NCP Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNcpName" runat="server" Height="16px" Width="296px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CP Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCpName" runat="server" Width="296px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            OAG Number:
                        </td>
                        <td>
                            <asp:TextBox ID="txtOagNumber" runat="server" Width="296px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <table>
                    <tr>
                        <td colspan="2" align="center" style="font-weight:bold; font-size:12;">
                            CAUSE NUMBER <asp:Label ID="lblCauseNumber" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtChildrenNames" runat="server" TextMode="MultiLine" 
                                Height="90px" Width="470px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCourtName" runat="server" TextMode="MultiLine" 
                                Height="90px" Width="470px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="font-weight:bold; font-size:12;">
                NOTICE OF HEARING
            </td>
        </tr>
        <tr>
            <td class="style1">
                  <asp:TextBox ID="txtNoticeBody" runat="server" TextMode="MultiLine" Height="100px"
                    Width="942px" />
            </td>
        </tr>
        <tr>
            <td align="center">
                    Mail to:
                  <asp:TextBox ID="txtMailtoList" runat="server" TextMode="MultiLine" Height="150px"
                    Width="942px" />
            </td>
        </tr>
        <tr>
            <td align="center" class="style1">
                <asp:Button ID="btnGenNotice" runat="server" Text="Generate Notice" OnClick="btnGenNotice_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
