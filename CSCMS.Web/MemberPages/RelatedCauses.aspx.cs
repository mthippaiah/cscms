﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MemberPages_RelatedCauses : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void HandleCauseSearchResult(DataSet ds, bool isSearchByCase)
    {
        this.grdCases.DataSource = ds;
        this.grdCases.DataBind();
        divSearchResult.Visible = true;
    }

    private bool Save()
    {
        GridDataItem[] coln = this.grdCases.MasterTableView.GetSelectedItems();
   
        foreach (GridDataItem item in coln)
        {
            Guid cid = (Guid)item.GetDataKeyValue("CauseGUID");

            if (!VerifyAccosiation(this.CurrentCause.CauseGUID, cid, true))
                return false;
         
            List<Guid> list1 = GetAssociatedCauseList(this.CurrentCause.CauseGUID);
            List<Guid> list2 = GetAssociatedCauseList(cid);

            foreach (Guid id1 in list1)
            {
                foreach (Guid id2 in list2)
                {
                    CreateAssociation(id1, id2);
                }
            }
        }
        CscmsContext.SaveChanges();
        return true;
    }

    private bool VerifyAccosiation(Guid id1, Guid id2, bool setMessage)
    {
        if (this.CurrentCause.CauseGUID == id2)
        {
            this.messageBox.ErrorMessage = setMessage?"Cannot add relationship with the same cause":"";
            return false;
        }

        CSCMS.Data.RelatedCause r1 = CscmsContext.RelatedCause.Where(it => it.Cause.CauseGUID == id2 && it.Cause1.CauseGUID == id1).FirstOrDefault();
        CSCMS.Data.RelatedCause r2 = CscmsContext.RelatedCause.Where(it => it.Cause.CauseGUID == id1 && it.Cause1.CauseGUID == id2).FirstOrDefault();

        if (r1 != null || r2 != null)
        {
            this.messageBox.ErrorMessage = setMessage?"A relationship already exists between these two causes":"";
            return false;
        }
        return true;
    }

    private List<Guid> GetAssociatedCauseList(Guid id)
    {
        List<Guid> list1 = (from it in CscmsContext.RelatedCause
                            where it.Cause.CauseGUID == id
                            select it.Cause1.CauseGUID).Distinct().ToList();
        list1.Add(id);
        List<Guid> list2 = (from it in CscmsContext.RelatedCause
                            where it.Cause1.CauseGUID == id
                            select it.Cause.CauseGUID).Distinct().ToList();
        list1.AddRange(list2);
        return list1.Distinct().ToList();
    }

    private bool CreateAssociation(Guid id1, Guid id2)
    {
        if (VerifyAccosiation(id1, id2, false))
        {
            CSCMS.Data.RelatedCause rc = new CSCMS.Data.RelatedCause();
            rc.Cause = CscmsContext.Cause.Where(it => it.CauseGUID == id2).FirstOrDefault();
            rc.Cause1 = CscmsContext.Cause.Where(it => it.CauseGUID == id1).FirstOrDefault();
            rc.CreateDate = DateTime.Now;
            rc.LastUpdateDate = DateTime.Now;

            CscmsContext.AddToRelatedCause(rc);
            return true;
        }
        return false;
    }

    protected void btnSave_Click(object sender, EventArgs args)
    {
        if (Save())
            this.messageBox.SuccessMessage = "Successfully added the relationship";
    }

    protected void btnSaveAndReturn_Click(object sender, EventArgs args)
    {
        if (Save())
        {
            Response.Redirect(string.Format("~/MemberPages/ActionDetail.aspx?cid={0}&a={1}", this.CurrentCause.CauseGUID.ToString(), this.CurrentCause.NumberOfAction.ToString()), true);
        }
    }

    protected void grdCases_OnPageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        causeSearch.DoSearch();
    }
}
