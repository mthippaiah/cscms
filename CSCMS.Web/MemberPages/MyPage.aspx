﻿<%@ Page Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="MyPage.aspx.cs"
    Inherits="MemberPages_MyPage" Title="CSCMS | My Page" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
        var mpjava = new MyPage();
        function showConfirmDialog()
        {
            if (<%= IsMessageVisible(ConfirmMessage.MyPageDeleteAllTicklerConfirm).ToString().ToLower() %>)
            {
                var dialog = $find('<%= winConfirm.ClientID %>');
                dialog.show();
            }
            else
            {
                <%= ClientScript.GetPostBackEventReference(btnDismissAllTickler, null) %>
            }
        }
        function dialogClose(sender, args)
        {
            if (args.get_argument() != null && args.get_argument() == true)
            {
                <%= ClientScript.GetPostBackEventReference(btnDismissAllTickler, null) %>
            }
        }
    </script>

</asp:Content>
<asp:Content ID="master_content" ContentPlaceHolderID="PageContent" runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="pageTitle">
                My Page
            </td>
            <td align="right">
                <asp:Panel ID="pnlCaseNavigation" runat="server" DefaultButton="btnSearch">
                    <asp:DropDownList ID="ddlOCACourt" runat="server" DataSourceID="dsOCACourt" DataTextField="CourtName"
                        DataValueField="OCACourtID" AutoPostBack="True" Visible="false" OnSelectedIndexChanged="ddlOCACourt_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                    <asp:SqlDataSource ID="dsOCACourt" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                        OnSelecting="dsOCACourt_Selecting" SelectCommand="SELECT DISTINCT a.OCACourtID, a.CourtName FROM OCACourt a, UserCourt b WHERE a.OCACourtID = b.OCACourtID AND b.UserID = @UserID">
                        <SelectParameters>
                            <asp:Parameter Name="UserID" Type="Int16" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    &nbsp;OAG #:&nbsp;
                    <asp:TextBox ID="txtOAGCauseNumber" runat="server" Font-Bold="true" Width="70" MaxLength="10"></asp:TextBox>
                    &nbsp;Cause #:&nbsp;
                    <asp:TextBox ID="txtCauseNumber" runat="server" Font-Bold="true" MaxLength="25" AccessKey="C"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="&nbsp;Detail&nbsp;" CssClass="submitMedium"
                        OnClick="btnSearch_Click" CausesValidation="False" UseSubmitBehavior="false" />
                    <asp:Button ID="btnHearing" runat="server" Text="&nbsp;Hearing&nbsp;" CssClass="submitMedium"
                        OnClick="btnHearing_Click" CausesValidation="False" UseSubmitBehavior="false" />
                    <asp:CustomValidator ID="cvtxtCauseNumber" runat="server" ErrorMessage="&nbsp;Cause Not Found"
                        ControlToValidate="txtCauseNumber" EnableViewState="False" Text="&nbsp;Cause Not Found"></asp:CustomValidator>
                    <asp:Button ID="btnCreateCase" runat="server" Visible="false" Text="Create Case"
                        CssClass="submit" OnClick="btnCreateCase_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnCourtPreferences" runat="server" Enabled="false" Text="Court Preferences"
                        CssClass="submit" PostBackUrl="CourtPreference.aspx" CausesValidation="False" />
                </asp:Panel>
            </td>
        </tr>
    </table>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <telerik:RadWindow ID="winConfirm" runat="server" Skin="Office2007" NavigateUrl="PopUpConfirm.aspx?mid=2"
        Modal="true" Behaviors="Close" VisibleStatusbar="false" Width="350" Height="250"
        OpenerElementID="btnDismissAllTickler" OnClientClose="dialogClose" />
    <!-- TICKLERS -->
    <CSCMSUC:SectionContainer ID="scTicklers" runat="server" HeaderText="TICKLERS" Width="100%"
        DisplayType="Grid">
        <telerik:RadAjaxPanel ID="ajaxPanelTickler" runat="server" LoadingPanelID="loadingPanel"
            ClientEvents-OnRequestStart="mpjava.AjaxPanelRequestStart">
            <div style="float: right; padding-bottom: 5px; vertical-align: bottom;">
                <table>
                    <tr>
                        <td align="right">
                            <asp:DropDownList ID="ddlTicklerType" runat="server">
                                <asp:ListItem Value="" Selected="True">- Select -</asp:ListItem>
                                <asp:ListItem Value="0">Action Not Disposed</asp:ListItem>
                                <asp:ListItem Value="1">Paternity No Test Results</asp:ListItem>
                                <asp:ListItem Value="2">Paternity Not Established</asp:ListItem>
                                <asp:ListItem Value="3">Transferred Cases</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlTicklerType" runat="server" ControlToValidate="ddlTicklerType"
                                ValidationGroup="Tickler" Display="Dynamic" ErrorMessage="Please select a TicklerType">*</asp:RequiredFieldValidator>
                            <asp:Button ID="btnGetTicklers" runat="server" Text="Go" OnClick="btnGetTicklers_OnClick" ValidationGroup="Tickler"
                                CssClass="submitMedium" Width="70px" />
                        </td>
                        <td align="right">
                            <asp:Button ID="btnExporTickler" runat="server" Text="Export to Excel" CssClass="submitMedium"
                                OnClick="btnExporTickler_Click" ValidationGroup="Tickler" />
                        </td>
                        <td align="right">
                            <asp:Button ID="btnDismissAllTickler" Enabled="true" runat="server" Text="Dismiss All Ticklers" ValidationGroup="Tickler" 
                                CssClass="submitMedium" OnClientClick="showConfirmDialog(); return false;" OnClick="btnDismissAllTickler_Click" />
                        </td>
                        <td align="right">
                            <asp:Button ID="btnRefreshTicklers" runat="server" Text="Refresh" CssClass="submitMedium"
                                OnClick="btnRefreshTicklers_OnClick" ValidationGroup="Tickler" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both;">
            </div>
            <telerik:RadGrid ID="grdTicklers" runat="server" AllowPaging="True" PageSize="10"
                AllowSorting="True" OnDeleteCommand="grdTicklers_DeleteCommand" OnNeedDataSource="grdTicklers_OnNeedDataSource"
                OnExcelMLExportStylesCreated="grd_ExcelMLExportStylesCreated">
                <MasterTableView NoMasterRecordsText="You currently have no ticklers." DataKeyNames="TriggerDate,TicklerTypeID,Tickler,ActionGUID">
                    <Columns>
                        <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../images/x_sm.gif" CommandName="delete" /></ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Cause Number" ItemStyle-Font-Bold="true"
                            SortExpression="CauseNumber" DataField="CauseNumber">
                            <ItemTemplate>
                                <asp:HyperLink Visible='<%# (Eval("TicklerTypeID").ToString() != "1" ) %>' ID="HyperLinkColumnCauseGUID"
                                    runat="server" NavigateUrl='<%#  "../MemberPages/ActionDetail.aspx?" + "cid=" + Eval("CauseGUID").ToString() + "&a=" + Eval("ActionNumber").ToString() %>'><%# Eval("CauseNumber")%></asp:HyperLink>
                                <asp:HyperLink Visible='<%# (Eval("TicklerTypeID").ToString() == "1" ) %>' ID="HyperLink1"
                                    runat="server" NavigateUrl='<%#  "../MemberPages/CaseNote.aspx?" + "cid=" + Eval("CauseGUID").ToString() + "&a=" + Eval("ActionNumber").ToString() %>'><%# Eval("CauseNumber")%></asp:HyperLink>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--<telerik:GridBoundColumn HeaderText="Date" DataField="TriggerDate" DataFormatString="{0:MM/dd/yyyy}" />--%>
                        <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                        <telerik:GridTemplateColumn HeaderText="Message" SortExpression="Tickler" DataField="Tickler">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ((String)Eval("Tickler")) %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%-- <telerik:GridBoundColumn HeaderText="Delay" DataField="DelayDays" />--%>
                    </Columns>
                </MasterTableView>
                <PagerStyle AlwaysVisible="true" />
            </telerik:RadGrid>
            <asp:SqlDataSource ID="dsTicklers" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                SelectCommand="GetTicklersByOCACourt" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                OnSelecting="dsTicklers_Selecting">
                <SelectParameters>
                    <asp:Parameter Name="OCACourtID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </telerik:RadAjaxPanel>
    </CSCMSUC:SectionContainer>
    <!-- CASELOAD OVERVIEW -->
    <CSCMSUC:SectionContainer runat="server" HeaderText="CASELOAD OVERVIEW" Width="100%"
        DisplayType="Grid" ID="scCaseLoad">
        <telerik:RadAjaxPanel ID="ajaxPanelCaseLoad" runat="server" LoadingPanelID="loadingPanel">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="4" align="right">
                        <asp:Button ID="btnRefreshCaseLoad" runat="server" Text="Refresh" CssClass="submitMedium"
                            OnClick="btnRefreshCaseLoad_OnClick" />
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 30%">
                        <telerik:RadGrid ID="RadOpenCases" runat="server" AllowPaging="False" PageSize="10"
                            AllowSorting="False" OnNeedDataSource="RadOpenCases_OnNeedDataSource">
                            <MasterTableView NoMasterRecordsText="You currently have no open cases." DataKeyNames="CaseTypeID">
                                <Columns>
                                    <telerik:GridHyperLinkColumn HeaderText="Case Type" DataNavigateUrlFields="CaseTypeID,Disposed"
                                        DataNavigateUrlFormatString="CaseLoad.aspx?CaseTypeID={0}&Disposed={1}" DataTextField="CaseTypeName"
                                        ItemStyle-Font-Bold="true" />
                                    <telerik:GridHyperLinkColumn HeaderText="Number of Cases" DataNavigateUrlFields="CaseTypeID,Disposed"
                                        DataNavigateUrlFormatString="CaseLoad.aspx?CaseTypeID={0}&Disposed={1}" DataTextField="NumberOfCase"
                                        ItemStyle-Font-Bold="true" />
                                </Columns>
                            </MasterTableView>
                            <PagerStyle AlwaysVisible="true" />
                        </telerik:RadGrid>
                    </td>
                    <td style="width: 1%;">
                        &nbsp;&nbsp;&nbsp;
                    </td>
                    <td valign="top" style="width: 36%">
                        <telerik:RadGrid ID="PendingCasesWithAge" runat="server" AllowPaging="False" PageSize="10"
                            AllowSorting="False" OnNeedDataSource="PendingCasesWithAge_OnNeedDataSource">
                            <MasterTableView NoMasterRecordsText="You currently have no pending cases." DataKeyNames="RangeTypeID">
                                <Columns>
                                    <telerik:GridHyperLinkColumn HeaderText="Pending Expedited Cases with Service" DataNavigateUrlFields="RangeTypeID"
                                        DataNavigateUrlFormatString="PendingCases.aspx?RangeTypeID={0}" DataTextField="RangeName"
                                        ItemStyle-Font-Bold="true" />
                                    <telerik:GridHyperLinkColumn HeaderText="Number of Cases" DataNavigateUrlFields="RangeTypeID"
                                        DataNavigateUrlFormatString="PendingCases.aspx?RangeTypeID={0}" DataTextField="NumberOfCase"
                                        ItemStyle-Font-Bold="true" />
                                </Columns>
                            </MasterTableView>
                            <PagerStyle AlwaysVisible="true" />
                        </telerik:RadGrid>
                    </td>
                    <td style="width: 33%;">
                    </td>
                </tr>
            </table>
        </telerik:RadAjaxPanel>
    </CSCMSUC:SectionContainer>
    <!-- TODAY HEARINGS -->
    <CSCMSUC:SectionContainer runat="server" ID="scHearings" HeaderText="TODAY'S HEARINGS"
        Width="100%" DisplayType="Grid">
        <asp:Panel ID="pnlHearings" runat="server" DefaultButton="btnGoto">
            <div style="float: right; padding-bottom: 5px; vertical-align: bottom;">
                <table>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnExportHearings" runat="server" Text="Export to Excel" CssClass="submitMedium"
                                OnClick="btnExportHearings_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnGotoHideShow" runat="server" Text="Go" CssClass="submitMedium"
                                Width="140px" OnClick="btnGotoHideShow_Click" CausesValidation="False" />
                        </td>
                        <td>
                            <b>Go To:</b>
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="dtGoto" runat="server" Style="vertical-align: bottom;" />
                        </td>
                        <td>
                            <asp:Button ID="btnGoto" runat="server" Text="Go" CssClass="submitMedium" Width="40px"
                                ValidationGroup="GotoDate" OnClick="btnGoto_Click" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfvGoto" runat="server" ControlToValidate="dtGoto"
                                Display="Dynamic" ValidationGroup="GotoDate" ErrorMessage="Please select a go to Date">* Required</asp:RequiredFieldValidator>
                        </td>
                        <td colspan="4" align="right">
                            <asp:Button ID="btnRefreshHearings" runat="server" Text="Refresh" CssClass="submitMedium"
                                OnClick="btnRefreshHearings_OnClick" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both;">
            </div>
            <telerik:RadGrid ID="grdHearings" runat="server" AllowPaging="True" PageSize="10"
                AllowSorting="True" OnExcelMLExportStylesCreated="grd_ExcelMLExportStylesCreated"
                OnNeedDataSource="grdHearings_OnNeedDataSource">
                <MasterTableView NoMasterRecordsText="There are no hearings for the selected date."
                    UseAllDataFields="true">
                    <Columns>
                        <telerik:GridHyperLinkColumn HeaderText="Time" DataNavigateUrlFields="CauseGUID, ActionNumber, HearingID"
                            DataNavigateUrlFormatString="HearingDetail.aspx?cid={0}&a={1}&hid={2}" SortExpression="DisplayTime"
                            DataTextField="DisplayTime" ItemStyle-Font-Bold="true" />
                        <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                        <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, ActionNumber"
                            DataNavigateUrlFormatString="ActionDetail.aspx?cid={0}&a={1}" SortExpression="CauseNumber"
                            DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                        <telerik:GridBoundColumn HeaderText="OAG" DataField="OAGCauseNumber" />
                        <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                        <telerik:GridBoundColumn HeaderText="Action" DataField="ActionTypeName" />
                        <telerik:GridBoundColumn HeaderText="Filing Date" DataField="ActionFiledDate" DataFormatString="{0:MM/dd/yyyy}" />
                    </Columns>
                </MasterTableView>
                <PagerStyle AlwaysVisible="true" />
            </telerik:RadGrid>
        </asp:Panel>
    </CSCMSUC:SectionContainer>
    <!-- COURT ACTIVITIES -->
    <CSCMSUC:SectionContainer runat="server" ID="scDailyCourtUserActivity" HeaderText="TODAY'S COURT USER ACTIVITIES"
        Width="100%" DisplayType="Grid">
        <asp:Panel ID="pnlUserActivity" runat="server" DefaultButton="btnGotoActivity">
            <div style="float: right; padding-bottom: 5px; vertical-align: bottom;">
                <table>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnExportActivity" runat="server" Text="Export to Excel" CssClass="submitMedium"
                                OnClick="btnExportActivity_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnGotoActivityShowHide" runat="server" Text="Go" CssClass="submitMedium"
                                Width="140px" OnClick="btnGotoActivityShowHide_Click" CausesValidation="False" />
                        </td>
                        <td>
                            <b>Go To:</b>
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="dtGotoActivityDate" runat="server" Style="vertical-align: bottom;" />
                        </td>
                        <td>
                            <asp:Button ID="btnGotoActivity" runat="server" Text="Go" CssClass="submitMedium"
                                Width="40px" ValidationGroup="btnGotoActivity" OnClick="btnGotoActivity_Click" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfvdtGotoActivityDate" runat="server" ControlToValidate="dtGotoActivityDate"
                                Display="Dynamic" ValidationGroup="btnGotoActivity" ErrorMessage="Please select a go to Date">* Required</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both;">
            </div>
            <telerik:RadGrid ID="grdDailyCourtUserActivity" runat="server" DataSourceID="dsDailyCourtUserActivity"
                AllowPaging="True" PageSize="10" AllowSorting="True" OnExcelMLExportStylesCreated="grd_ExcelMLExportStylesCreated">
                <MasterTableView NoMasterRecordsText="There are no activities for the selected date.">
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                        <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, NumberOfAction"
                            DataNavigateUrlFormatString="ActionDetail.aspx?cid={0}&a={1}" SortExpression="CauseNumber"
                            DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                        <telerik:GridBoundColumn HeaderText="OAG" DataField="OAGCauseNumber" />
                        <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                        <telerik:GridBoundColumn HeaderText="Actions" DataField="NumberOfAction" />
                        <telerik:GridBoundColumn HeaderText="Last Activity" DataField="CreateDate" DataFormatString="{0:hh:mm}" />
                        <telerik:GridBoundColumn HeaderText="First Filed" DataField="FirstActionFiledDate"
                            DataFormatString="{0:MM/dd/yyyy}" />
                        <telerik:GridBoundColumn HeaderText="Last Filed" DataField="LastActionFiledDate"
                            DataFormatString="{0:MM/dd/yyyy}" />
                    </Columns>
                </MasterTableView>
                <PagerStyle AlwaysVisible="true" />
            </telerik:RadGrid>
            <asp:SqlDataSource ID="dsDailyCourtUserActivity" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                OnSelecting="dsDailyCourtUserActivity_Selecting" SelectCommand="GetCourtActivities"
                SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false">
                <SelectParameters>
                    <asp:Parameter Name="OCACourtID" Type="Int32" />
                    <asp:Parameter Name="StartDate" Type="DateTime" />
                    <asp:Parameter Name="EndDate" Type="DateTime" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:Panel>
    </CSCMSUC:SectionContainer>
    <!-- BROADCAST MESSAGES  -->
    <CSCMSUC:SectionContainer runat="server" ID="scMessage" HeaderText="BROADCAST MESSAGES "
        Width="100%" DisplayType="Grid">
        <telerik:RadGrid ID="grdMessages" runat="server" DataSourceID="dsMessages" AllowPaging="True"
            PageSize="10" AllowSorting="True">
            <MasterTableView NoMasterRecordsText="There are currently no broadcast messages.">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Date" DataField="StartDate" DataFormatString="{0:MM/dd/yyyy}" />
                    <telerik:GridBoundColumn HeaderText="Message" DataField="Message" />
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsMessages" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="GetBroadcastMessages" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false" />
    </CSCMSUC:SectionContainer>
    <!-- REMINDERS -->
    <CSCMSUC:SectionContainer runat="server" ID="scReminders" HeaderText="REMINDERS"
        Width="100%" DisplayType="Grid">
        <div style="float: right; padding-bottom: 5px; vertical-align: bottom;">
            <table>
                <tr>
                    <td align="right">
                        <asp:Button ID="Button1" runat="server" Text="Export to Excel" CssClass="submitMedium"
                            OnClick="btnExportReminder_Click" />
                    </td>
                    <td>
                        <asp:Button ID="Button2" runat="server" Text="Manage Reminder" CssClass="submitMedium"
                            Width="140px" CausesValidation="False" PostBackUrl="~/MemberPages/Reminder.aspx" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both;">
        </div>
        <!-- grdReminder -->
        <telerik:RadGrid ID="grdReminder" runat="server" DataSourceID="dsReminder" AllowPaging="True"
            PageSize="10" AllowSorting="True" OnExcelMLExportStylesCreated="grd_ExcelMLExportStylesCreated">
            <MasterTableView NoMasterRecordsText="There are currently no reminders." DataKeyNames="ReminderID">
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Date" HeaderStyle-Width="100" DataField="EffectiveDate"
                        DataFormatString="{0:MM/dd/yyyy}" />
                    <telerik:GridBoundColumn HeaderText="Reminder" DataField="ReminderNote" />
                    <telerik:GridTemplateColumn HeaderText="Cause Number" HeaderStyle-Width="100" DataField="CauseNumber">
                        <ItemTemplate>
                            <asp:HyperLink Visible='<%# (Eval("CauseNumber").ToString() != "" ) %>' ID="HyperLinkColumnCauseGUID"
                                runat="server" NavigateUrl='<%#  "../MemberPages/ActionDetail.aspx?" + "cid=" + Eval("CauseGUID").ToString() + "&a=" + Eval("NumberOfAction").ToString() %>'><%# Eval("CauseNumber")%></asp:HyperLink>
                            <asp:Label ID="nothing1" runat="server" Text='<%# Eval("CauseNumber").ToString() %>'
                                Visible='<%# (Eval("CauseNumber").ToString() == "" ) %>' BorderStyle="None" BorderWidth="0"></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <!-- dsReminder -->
        <asp:SqlDataSource ID="dsReminder" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="GetReminderByUserID" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
            OnSelecting="dsReminder_Selecting">
            <SelectParameters>
                <asp:Parameter Name="OCACourtID" Type="Int32" />
                <asp:Parameter Name="UserID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </CSCMSUC:SectionContainer>
    <!-- ajaxManager -->
    <telerik:RadAjaxManager ID="ajaxManager" runat="server" ClientEvents-OnRequestStart="mpjava.RadAjaxManagerRequestStarted">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="messageBox" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdMessages">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdMessages" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdReminder">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdReminder" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <!-- loadingPanel -->
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 80px;" />
    </telerik:RadAjaxLoadingPanel>

    <script type="text/javascript">
        if (typeof (WebForm_FireDefaultButton) != 'undefined')
            var origFireDefaultButton = WebForm_FireDefaultButton;

        WebForm_FireDefaultButton = function(event, target) {
            if (event.keyCode == 13) {
                var src = event.srcElement || event.target;
                // Don't call original function if focus is on a button 
                if (src && (src.tagName.toLowerCase() == "input") && (src.type.toLowerCase() == "submit" || src.type.toLowerCase() == "button")) {
                    return true;
                }
            }
            return origFireDefaultButton(event, target);
        }
    </script>

</asp:Content>
