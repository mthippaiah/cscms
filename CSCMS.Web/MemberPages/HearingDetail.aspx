﻿<%@ Page Title="CSCMS | Hearings" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="HearingDetail.aspx.cs" Inherits="MemberPages_HearingDetail" %>

<%@ Register TagPrefix="UC" TagName="ActionInfo" Src="~/UserControls/ActionSummary/ActionInfo.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNotes" Src="~/UserControls/CaseNotes.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNavigation" Src="~/UserControls/CaseNavigation.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        var actionType;
        var hdjava = new HearingDetail();
        function createHDObjects() {
            var dtDispRDate = $find("<%= dtDispositionRenderedDate.ClientID %>");
            var dtDispSDate = $find("<%= dtDispositionSignedDate.ClientID %>");
            var ddlDisp = document.getElementById("<%= ddlDisposition.ClientID %>");
            var note = document.getElementById("<%= txtHearingNote.ClientID %>");
            return { dtDispRDate: dtDispRDate, dtDispSDate: dtDispSDate, ddlDisp: ddlDisp, note: note };
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div style="width: 100%">
        <div class="pageTitle" style="float: left; width: 10%">
            HEARING</div>
        <div style="float: right; width: 90%; padding-bottom: 5px;" id="divHearingButtons"
            runat="server" visible="false">
            <div style="float: left;">
                <UC:CaseNavigation ID="caseNavigation" runat="server" IncludeEmptyItem="true" />
            </div>
            <div style="float: left;">
                &nbsp;
                <asp:Button ID="btnDelete" runat="server" type="button" Visible="false" class="submitMediumRed"
                    Style="width: 100px" OnCommand="btnDelete_Command" OnClientClick="return confirm('Are you sure you want to delete this hearing?');"
                    Text="Delete Hearing" />
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary"
        ValidationGroup="HearingSave" />
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <div id="divMain" runat="server" class="newCase">
        <div id="divMainSub" runat="server" class="newCaseSub" style="width: 873px;">
            <UC:ActionInfo ID="actionInfo" runat="server" />
            <CSCMSUC:SectionContainer ID="sectionScheduleHearing" runat="server" HeaderText="SCHEDULE HEARING INFORMATION"
                Width="100%">
                <asp:Panel ID="pnlScheduleHearing" runat="server" DefaultButton="btnSaveHearingSchedule">
                    <table align="center">
                        <tr>
                            <td valign="top" style="width: 45%">
                                <table cellspacing="3" align="center">
                                    <tr>
                                        <td colspan="2">
                                            <telerik:RadToolTip ID="hearingDateTooltip" runat="server" Width="400" Skin="Hay"
                                                Animation="Slide" Position="TopCenter" Visible="false" AutoCloseDelay="10000"
                                                RelativeTo="Element" IsClientID="true" />
                                            <asp:Label ID="lblMessage" runat="server" Font-Bold="true" ForeColor="#ff0000" EnableViewState="false"
                                                Text="&nbsp;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabelReq" valign="top" style="padding-top: 4px;">
                                            <asp:Label ID="lblHearingDate" runat="server">Hearing Date:</asp:Label>
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <telerik:RadDatePicker ID="dtHearingDate" runat="server" AutoPostBack="true" OnSelectedDateChanged="dtHearingDate_SelectedDateChanged" />
                                                        <asp:RequiredFieldValidator ID="rfvHearingDate" runat="server" ControlToValidate="dtHearingDate"
                                                            ValidationGroup="HearingSave" Display="Dynamic" ErrorMessage="Please select a Hearing Date">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <telerik:RadTimePicker ID="hearingTime" runat="server" Skin="Office2007" Width="75px">
                                                            <DateInput Font-Size="11px" Font-Names="Arial" />
                                                            <TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:15:00" EndTime="18:00:00"
                                                                Columns="4" OnClientTimeSelected="hdjava.HearingTimeSelected" />
                                                            <TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
                                                        </telerik:RadTimePicker>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- for cscms, this field is auto calculate from hearing date/time -->
                                    <tr runat="server" visible="false">
                                        <td class="fieldLabelReq" valign="top" style="padding-top: 4px;">
                                            Docket:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rblMorningDocket" runat="server" CellPadding="0" CellSpacing="0">
                                                <asp:ListItem Value="True" Selected="True">Morning</asp:ListItem>
                                                <asp:ListItem Value="False">Afternoon</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="rfvMorningDocket" runat="server" ControlToValidate="rblMorningDocket"
                                                ValidationGroup="HearingSave" Display="Dynamic" ErrorMessage="Please select a morning or afternoon docket">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td colspan="2" class="fieldLabel" align="left" style="text-align: left">
                                            Hearing Note:
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td colspan="2" align="left" style="text-align: left">
                                            <asp:TextBox ID="txtHearingNote" runat="server" Width="380" Height="80" TextMode="MultiLine" />
                                        </td>
                                    </tr>
                                    <!-- DATA FROM CASE -->
                                    <tr id="trDispositionRenderedDate" runat="server">
                                        <td class="fieldLabel">
                                            Disposition Rendered Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtDispositionRenderedDate" runat="server" MaxDate='<%# DateTime.Today %>'>
                                                <ClientEvents OnDateSelected="hdjava.DispRDateSelected" />
                                            </telerik:RadDatePicker>
                                            <asp:RangeValidator ID="rvDispositionRenderedDate" runat="server" ControlToValidate="dtDispositionRenderedDate"
                                                ValidationGroup="HearingSave" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Disposition Rendered Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trDispositionSignedDate" runat="server">
                                        <td class="fieldLabel">
                                            Disposition Signed Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtDispositionSignedDate" runat="server" MaxDate='<%# DateTime.Today %>' />
                                            <asp:RangeValidator ID="rvDispositionSignedDate" runat="server" ControlToValidate="dtDispositionSignedDate"
                                                ValidationGroup="HearingSave" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Disposition Signed Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <%-- MaximumValue='<%# DateTime.Today.ToString("d") %>' --%>
                                    <tr>
                                        <td class="fieldLabel">
                                            Disposition:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDisposition" runat="server" Width="200" DataSource='<%# GetDispositionTypes() %>'
                                                DataValueField="DispositionTypeID" DataTextField="DispositionTypeName" AppendDataBoundItems="true"
                                                SelectedValue='<%# DispositionTypeIDString %>'>
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsDispositionType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select DispositionTypeID, DispositionTypeName from DispositionType">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trlDispositionDetail" runat="server">
                                        <td class="fieldLabel">
                                            Disposition Detail:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDispositionDetail" runat="server" Width="200" DataSourceID="dsDispositionDetail"
                                                DataValueField="DispositionDetailID" DataTextField="DispositionDetailName" AppendDataBoundItems="true"
                                                SelectedValue='<%# DispositionDetailIDString %>'>
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsDispositionDetail" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select DispositionDetailID, DispositionDetailName from DispositionDetail">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <!-- DATA FROM CASE -->
                                    <tr>
                                        <td colspan="2" align="center" style="padding-top: 10px; padding-bottom: 5px;">
                                            <asp:Button ID="btnSaveHearingSchedule" runat="server" Text="Save Hearing" CssClass="submit"
                                                ValidationGroup="HearingSave" Width="90px" OnClick="btnSave_Click" OnClientClick="return hdjava.ValidateSaveHearing();" />
                                            &nbsp;&nbsp;
                                            <asp:Button ID="btnSaveAndAddNew" runat="server" Text="Save &amp; Add New" CssClass="submit"
                                                ValidationGroup="HearingSave" Width="110px" OnClick="btnSaveAndAddAnother_Click"
                                                OnClientClick="return hdjava.ValidateSaveHearing();" />
                                            <asp:Button ID="btnSaveAndReturnToCase" runat="server" Text="Save &amp; Return to Case"
                                                ValidationGroup="HearingSave" CssClass="submit" Width="130px" OnClick="btnSaveAndReturntoCase_Click"
                                                OnClientClick="return hdjava.ValidateSaveHearing();" />
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td colspan="2" class="fieldLabel" align="left" style="text-align: left">
                                            Action Hearings:
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td colspan="2" align="left" style="text-align: left">
                                            <telerik:RadGrid ID="grdCurrentActionHearing" runat="server" OnNeedDataSource="grdCurrentActionHearing_OnNeedDataSource">
                                                <HeaderStyle Font-Size="10px" />
                                                <ItemStyle CssClass="gridSmall" />
                                                <AlternatingItemStyle CssClass="gridSmallAlt" />
                                                <MasterTableView NoMasterRecordsText="There are currently no hearings.">
                                                    <Columns>
                                                        <telerik:GridBoundColumn HeaderText="Action Date" DataField="ActionDate" DataFormatString="{0:MM/dd/yy}"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                        <telerik:GridTemplateColumn HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lnkAction" Enabled='<%# ((Eval("ActionID").ToString() != this.CurrentHearing.HearingID.ToString())) %>'
                                                                    runat="server" Text='<%# Eval("HearingTime") %>' NavigateUrl='<%# GetActionUrl(Eval("ActionType").ToString(), Eval("ActionID").ToString()) %>' /></ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                        <telerik:GridBoundColumn HeaderText="Note" DataField="Note" UniqueName="Note" />
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 1%">
                                &nbsp;
                            </td>
                            <td valign="top" style="width: 54%">
                                <div>
                                    <asp:Label ID="lblgrdHearings" runat="server" Text="Hearings on Same Date:" Font-Bold="true"
                                        EnableViewState="true" />
                                    <div id="divHearings" runat="server">
                                        <asp:Button ID="btnShowHearings" runat="server" Text="Show Hearings" CssClass="submitMedium"
                                            OnClick="btnShowHearings_OnClick" />
                                        <telerik:RadGrid ID="grdHearings" runat="server" Visible="false" AllowPaging="true"
                                            PageSize="10" OnPageIndexChanged="grdHearings_OnPageIndexChanged">
                                            <MasterTableView NoMasterRecordsText="There are no hearings for the selected date.">
                                                <Columns>
                                                    <telerik:GridHyperLinkColumn HeaderText="Time" DataNavigateUrlFields="CauseGUID, ActionNumber, HearingID"
                                                        DataNavigateUrlFormatString="HearingDetail.aspx?cid={0}&a={1}&hid={2}" SortExpression="HearingTime"
                                                        DataTextField="HearingTime" ItemStyle-Font-Bold="true" />
                                                    <telerik:GridTemplateColumn HeaderText="Time" Visible="false">
                                                        <ItemTemplate>
                                                            <%# Eval("HearingTime") != null ? new DateTime(((TimeSpan)Eval("HearingTime")).Ticks).ToString("hh:mm") : "&nbsp;" %></ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, ActionNumber"
                                                        DataNavigateUrlFormatString="ActionDetail.aspx?cid={0}&a={1}" SortExpression="CauseNumber"
                                                        DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                                                    <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                                                    <telerik:GridBoundColumn HeaderText="Action" DataField="ActionTypeName" />
                                                    <telerik:GridBoundColumn HeaderText="Filing" DataField="ActionFiledDate" DataFormatString="{0:MM/dd/yy}" />
                                                    <telerik:GridBoundColumn HeaderText="Next" DataField="DispositionDate" DataFormatString="{0:MM/dd/yy}"
                                                        ItemStyle-ForeColor="DarkBlue" />
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
        </div>
    </div>
    <div class="bottomSection" id="divCaseNotes" runat="server" visible="false">
        <UC:CaseNotes ID="caseNotes" runat="server" />
    </div>
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dtHearingDate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lblgrdHearings" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="hearingDateTooltip" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="divHearings" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnShowHearings">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divHearings" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 50px;" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
