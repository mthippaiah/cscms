﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;

public partial class MemberPages_CaseLoad : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["CaseTypeID"]) && !string.IsNullOrEmpty(Request.QueryString["Disposed"]))
            {
                causeSearch.ClearSearchParameters();
                this.grdCases.MasterTableView.NoDetailRecordsText = "No records found.";
                int statusIndex = int.Parse(Request.QueryString["CaseTypeID"].ToString());
                int disposedIndex = int.Parse(Request.QueryString["Disposed"].ToString());
                causeSearch.CaseStatusIndex = statusIndex;
                causeSearch.DisposedIndex = disposedIndex;


                if (statusIndex == 1 && disposedIndex == 1)
                    lblPageLabel.Text = "Active Disposed Cases";

                if (statusIndex == 1 && disposedIndex == 2)
                    lblPageLabel.Text = "Active Pending Cases";

                if (statusIndex == 2 && disposedIndex == 0)
                    lblPageLabel.Text = "Inactive Cases";

                if (statusIndex == 0 && disposedIndex == 0)
                    lblPageLabel.Text = "All Cases";
            }
        }
    }

    protected void grdCases_ExcelMLExportStylesCreated(object source, GridExportExcelMLStyleCreatedArgs e)
    {
        foreach (StyleElement style in e.Styles)
        {
            if (style.Id == "headerStyle")
            {
                style.FontStyle.Bold = true;
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "itemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
            else if (style.Id == "alternatingItemStyle")
            {
                style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Center;
            }
        }
    }

    protected void btnExportCase_Click(object sender, EventArgs e)
    {
        if (grdCases.Items.Count > 0)
        {
            this.grdCases.ExportSettings.ExportOnlyData = true;
            grdCases.ExportSettings.IgnorePaging = true;
            grdCases.ExportSettings.OpenInNewWindow = true;
            grdCases.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
            grdCases.MasterTableView.ExportToExcel();
        }
    }

    protected void HandleCauseSearchResult(DataSet ds, bool isSearchByCase)
    {
        this.grdCases.DataSource = ds;
    }

    protected void grdCases_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        causeSearch.btnSearchByCase_Click(this, null);
    }
}
