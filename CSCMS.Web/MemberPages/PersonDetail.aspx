﻿<%@ Page Title="CSCMS | Person" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="PersonDetail.aspx.cs" Inherits="MemberPages_PersonDetail" %>

<%@ Register TagPrefix="UC" TagName="ActionInfo" Src="~/UserControls/ActionSummary/ActionInfo.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNotes" Src="~/UserControls/CaseNotes.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNavigation" Src="~/UserControls/CaseNavigation.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/PersonSearch.ascx" TagName="PersonSearch" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script language="javascript" type="text/javascript">
        var pdjava = new PersonDetail();
        function createMiscObjects()
        {
            var npCheck = document.getElementById('<%= chkNP.ClientID %>');
            var napp = document.getElementById('<%= chkNeedAttorneyAppointed.ClientID %>');
            return {npCheck:npCheck,napp:napp};
        }
        function createNpOnlyObjects()
        {
            var trih = document.getElementById("<%= trIndigenceHearing.ClientID %>");
            var trnaa = document.getElementById("<%= trNeedAttorneyAppointed.ClientID %>");
            var trrat = document.getElementById("<%= trRequestedAdditionalTime.ClientID %>");
            var trdfi = document.getElementById("<%= trDateFoundIndegent.ClientID %>");
            var trcs = document.getElementById("<%= trddlCounselStatus.ClientID %>");
            var trcd = document.getElementById("<%= trCounselDate.ClientID %>");
            return {trih:trih,trnaa:trnaa,trrat:trrat,trdfi:trdfi,trcs:trcs,trcd:trcd};
        }
        function createPersonDetailPersonSearchObjects()
        {
            var vhfperson = document.getElementById("<%= HFPersonGUID.ClientID %>");
            var vfn = document.getElementById("<%= txtFirstName.ClientID %>");
            var vln = document.getElementById("<%= txtLastName.ClientID %>");
            var vmn = document.getElementById("<%= txtMiddleName.ClientID %>");
            var vsuffix = document.getElementById("<%= txtSuffix.ClientID %>");
            var vsex = document.getElementById("<%= ddlSex.ClientID %>");
            var vintp = document.getElementById("<%= chkInterpreterNeeded.ClientID %>");
            var vfl = document.getElementById("<%= ddlFirstLanguage.ClientID %>");
            var vsecr = document.getElementById("<%= chkSecurityRisk.ClientID %>");
            var vdis = document.getElementById("<%= chkDisability.ClientID %>");
            var vbdate = $find("<%= dtBirthDate.ClientID %>");
            return {vhfperson : vhfperson, vfn : vfn, vln : vln, vmn : vmn, vsuffix : vsuffix, vsex : vsex, vintp : vintp, vfl : vfl, vsecr : vsecr, vdis : vdis, vbdate : vbdate};
        }
        function waiveRightChangedObjects()
        {
            var dtOrderDate = $find('<%= dtOrderDate.ClientID %>');
            var chkTestPositive = document.getElementById('<%= chkTestPositive.ClientID %>');
            var chkIsExcluded = document.getElementById('<%= chkIsExcluded.ClientID %>');
            var dtPaternityEstablishedDate = $find('<%= dtPaternityEstablishedDate.ClientID %>');
            var dtResultDate = $find('<%= dtResultDate.ClientID %>');
            return {dtOrderDate:dtOrderDate,chkTestPositive:chkTestPositive, chkIsExcluded:chkIsExcluded, dtPaternityEstablishedDate:dtPaternityEstablishedDate,dtResultDate:dtResultDate};
        }
        function showConfirmDialog()
        {
            if (<%= IsMessageVisible(ConfirmMessage.DeletePersonConfirm).ToString().ToLower() %>)
            {
                var dialog = $find('<%= winConfirm.ClientID %>');
                dialog.show();
            }
            else
            {
                <%= ClientScript.GetPostBackEventReference(btnDeletePerson, null) %>
            }
        }
       function dialogClose(sender, args)
        {
            if (args.get_argument() != null && args.get_argument() == true)
            {
                <%= ClientScript.GetPostBackEventReference(btnDeletePerson, null) %>
            }
        }
        function HidePopup() {
            $find('PersonMPE').hide();
            return false;
        }
    </script>

    <style type="text/css">
        .NpHide
        {
            display: none;
        }
        .NpShow
        {
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <telerik:RadWindow ID="winConfirm" runat="server" Skin="Office2007" NavigateUrl="PopUpConfirm.aspx?mid=4"
        Modal="true" Behaviors="Close" VisibleStatusbar="false" Width="350" Height="250"
        OpenerElementID="btnDeletePerson" OnClientClose="dialogClose" />
    <telerik:RadCalendar ID="sharedCalendar" runat="server" />
    <div class="pageTitle" style="float: left; width: 100%;">
        <asp:Label ID="lblPageTitle" runat="server" /></div>
    <div style="float: right; width: 100%; padding-bottom: 5px;" id="divPersonButtons"
        runat="server">
        <div style="float: left;">
            <UC:CaseNavigation ID="caseNavigation" runat="server" />
        </div>
        <div style="float: left;">
            &nbsp;
            <asp:Button ID="btnDeletePerson" runat="server" Text="Delete Person" CssClass="submitMediumRed"
                CausesValidation="false" OnClientClick="showConfirmDialog(); return false;" OnClick="btnDeletePerson_Click" />
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" />
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <div id="divMain" runat="server" class="newCase">
        <div id="divMainSub" runat="server" class="newCaseSub" style="width: 873px;">
            <UC:ActionInfo ID="actionInfo" runat="server" />
            <!-- sectionPerson -->
            <CSCMSUC:SectionContainer ID="sectionPerson" runat="server" HeaderText="DEMOGRAPHICS"
                Width="100%">
                <asp:Panel ID="pnlPerson" runat="server" DefaultButton="btnSavePerson">
                    <table width="100%">
                        <tr>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr>
                                        <td class="fieldLabelReq">
                                            Last Name:
                                            <asp:HiddenField ID="HFPersonGUID" runat="server" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLastName" runat="server" Width="200" MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                                ValidationGroup="PersonDetail" ErrorMessage="Please enter a Last Name">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdLastName" runat="server" class="fieldLabel">
                                            First Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFirstName" runat="server" Width="200" MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                                ValidationGroup="PersonDetail" ErrorMessage="Please enter a First Name" Enabled="false">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            Middle Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMiddleName" runat="server" Width="200" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            Suffix:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSuffix" runat="server" Width="200" MaxLength="10" />
                                        </td>
                                    </tr>
                                    <tr id="trBarCardNumber" runat="server" visible="false">
                                        <td class="fieldLabel">
                                            Bar Card Number:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtBarCardNumber" runat="server" Width="200" />
                                            <asp:RangeValidator ID="rgvBarCardNumber" runat="server" ControlToValidate="txtBarCardNumber"
                                                ValidationGroup="PersonDetail" MinimumValue="0" MaximumValue="999999999" Type="Integer"
                                                ErrorMessage="Bar Card Number is invalid">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trFirm" runat="server" visible="false">
                                        <td class="fieldLabel">
                                            Firm:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFirm" runat="server" Width="200" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr id="trAlias" runat="server" visible="false">
                                        <td class="fieldLabel">
                                            AKA:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAlias" runat="server" Width="200" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr id="trBirthDate" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Birth Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtBirthDate" runat="server" MaxDate='<%# DateTime.Today %>'
                                                SharedCalendarID="sharedCalendar" />
                                            <asp:RangeValidator ID="rvBirthDate" runat="server" ControlToValidate="dtBirthDate"
                                                ValidationGroup="PersonDetail" MinimumValue="1/1/1900" MaximumValue='<%# DateTime.Today.ToString("d") %>'
                                                Type="Date" ErrorMessage="Birth Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trAge" runat="server" visible="false">
                                        <td class="fieldLabel">
                                            Age:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAge" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabelReq" id="sexLabel" runat="server">
                                            Sex:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSex" runat="server">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvSex" runat="server" ControlToValidate="ddlSex"
                                                ValidationGroup="PersonDetail" ErrorMessage="Please select a Sex">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10px">
                                &nbsp;
                            </td>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr id="trRelatedCasesLabel" runat="server" visible="false">
                                        <td class="fieldLabel" colspan="2" style="text-align: left;">
                                            Related Case(s):
                                        </td>
                                    </tr>
                                    <tr id="trRelatedCasesList" runat="server" visible="false">
                                        <td colspan="2">
                                            <table>
                                                <asp:Repeater ID="rptRelatedCases" runat="server" DataSourceID="dsRelatedCases">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <a href="ActionDetail.aspx?cid=<%# Eval("CauseGUID") %>&a=<%# Eval("NumberOfAction") %>"
                                                                    style="font-weight: normal;">
                                                                    <%# Eval("CauseNumber") %></a>&nbsp;&nbsp;
                                                            </td>
                                                            <td>
                                                                <%# Eval("Style") %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:SqlDataSource ID="dsRelatedCases" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                    SelectCommand="select Cause.CauseGUID, Cause.CauseNumber, Action.Style, Cause.NumberOfAction, ActionType.ActionTypeName from ActionPerson, Action, Cause, ActionType where ActionPerson.ActionGUID = Action.ActionGUID and Action.CauseGUID = Cause.CauseGUID and ActionType.ActionTypeID = Action.ActionTypeID and ActionPerson.PersonGUID = @PersonGUID and Cause.CauseGUID <> @CauseGUID order by Cause.CauseNumber"
                                                    OnSelecting="dsRelatedCases_Selecting">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="PersonGUID" Type="String" />
                                                        <asp:Parameter Name="CauseGUID" Type="String" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trRelatedCasesHr" runat="server">
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr id="trInterpreterNeeded" runat="server">
                                        <td class="fieldLabel">
                                            Interpreter Needed:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkInterpreterNeeded" runat="server" CssClass="chkNoPad" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trFirstLanguage" runat="server">
                                        <td class="fieldLabel">
                                            Language:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlFirstLanguage" runat="server" Width="204" DataSourceID="dsInteperterLanguage"
                                                DataValueField="IntepreterLanguageID" DataTextField="IntepreterLanguageName"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsInteperterLanguage" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select IntepreterLanguageID, IntepreterLanguageName from IntepreterLanguage order by IntepreterLanguageName">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trSecondLanguage" runat="server">
                                        <td class="fieldLabel">
                                            Language:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSecondLanguage" runat="server" Width="204" DataSourceID="dsInteperterLanguage"
                                                DataValueField="IntepreterLanguageID" DataTextField="IntepreterLanguageName"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trThirdLanguage" runat="server">
                                        <td class="fieldLabel">
                                            Language:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlThirdLanguage" runat="server" Width="204" DataSourceID="dsInteperterLanguage"
                                                DataValueField="IntepreterLanguageID" DataTextField="IntepreterLanguageName"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trInterpreterHr" runat="server">
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr id="trIncarcerated" runat="server" visible="false">
                                        <td class="fieldLabel">
                                            Incarcerated:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkIncarcerated" runat="server" CssClass="chkNoPad" Enabled="false" />
                                        </td>
                                    </tr>
                                    <tr id="trSecurityRisk" runat="server">
                                        <td class="fieldLabel">
                                            Security Risk:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkSecurityRisk" runat="server" CssClass="chkNoPad" />
                                        </td>
                                    </tr>
                                    <tr id="trDisability" runat="server">
                                        <td class="fieldLabel">
                                            Special Accessibility:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkDisability" runat="server" CssClass="chkNoPad" />
                                        </td>
                                    </tr>
                                    <tr id="trNonDisclosureFinding" runat="server">
                                        <td class="fieldLabel">
                                            Non Disclosure Finding:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkNonDisclosureFinding" runat="server" CssClass="chkNoPad" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:ModalPopupExtender ID="PersonMPE" runat="server" TargetControlID="btnPersonSearch"
                                    PopupControlID="ModPop" BackgroundCssClass="modalBackground" PopupDragHandleControlID="divpopupheader1"
                                    BehaviorID="PersonMPE" RepositionMode="None">
                                </asp:ModalPopupExtender>
                                <asp:Panel ID="ModPop" runat="server" Style="display: none" BackColor="Bisque" BorderStyle="Outset"
                                    Height="500" Width="495" HorizontalAlign="Center">
                                    <div id="divpopupheader1" style="border-bottom-style: Outset; text-align: center;
                                        background-color: Gray">
                                        Person Search
                                    </div>
                                    <div id="divpopid1">
                                        <uc1:PersonSearch ID="personSearch" runat="server" OnSearchClicked="HandleSearchClickedEvent"
                                            OnSearchPageChanged="HandlePeasonSearchPageChange" RequiresCompleteDetails="true"
                                            ParentPageName="PersonDetail" />
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" style="padding-bottom: 5px;">
                                <asp:Button ID="btnSavePerson" runat="server" Text="&nbsp;Save Person&nbsp;" CssClass="submitMedium"
                                    ValidationGroup="PersonDetail" OnClick="btnSave_Click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnSaveAndAnotherChild" runat="server" Text="&nbsp;Save &amp; Add New&nbsp;"
                                    CssClass="submitMedium" ValidationGroup="PersonDetail" OnClick="btnSaveAndAddAnother_Click"
                                    Visible="false" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnSaveAndReturn" runat="server" Text="Save &amp; Return to Case"
                                    CssClass="submitMedium" Visible="true" OnClick="btnSaveAndReturn_Click" ValidationGroup="PersonDetail" />
                                &nbsp;&nbsp;<asp:Button ID="btnPersonSearch" runat="server" Text="Search" CssClass="submitMedium"
                                    CausesValidation="false" OnClientClick="return pdjava.ShowSearchWindow(this);" />
                                &nbsp;&nbsp;<asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="submitMedium"
                                    CausesValidation="false" OnClientClick="return pdjava.Clear(this);" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
            <!-- SectionContactInfo -->
            <CSCMSUC:SectionContainer ID="SectionContactInfo" runat="server" HeaderText="CONTACT INFORMATION"
                Width="100%">
                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSavePerson">
                    <table width="100%">
                        <tr>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr id="trAddress1" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Address 1:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAddress1" runat="server" Width="200" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr id="trAddress2" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Address 2:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAddress2" runat="server" Width="200" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr id="trAddress3" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Address 3:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAddress3" runat="server" Width="200" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr id="trPhoneHomeHR0" runat="server">
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trPhoneHomeHR1" runat="server">
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr id="trPhoneHome" runat="server">
                                        <td class="fieldLabel">
                                            Home Phone:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPhoneHome" runat="server" Width="98" MaxLength="30" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trPhoneOffice" runat="server">
                                        <td class="fieldLabel">
                                            Office Phone:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPhoneOffice" runat="server" Width="98" MaxLength="30" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10px">
                                &nbsp;
                            </td>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr id="trCity" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            City:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCity" runat="server" Width="200" MaxLength="50" />
                                        </td>
                                    </tr>
                                    <tr id="trCounty" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            County:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCounty" runat="server" Width="204" DataSourceID="dsCounty"
                                                DataValueField="CountyID" DataTextField="CountyName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select CountyID, CountyName from County order by SortOrder"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trState" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            State:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlState" runat="server" Width="204" DataSourceID="dsState"
                                                DataValueField="StateID" DataTextField="StateName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsState" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select StateID, StateName from State order by SortOrder"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trZipCode" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Zip Code:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtZipCode" runat="server" Width="50" MaxLength="5" />
                                            <asp:RangeValidator ID="rgvZipCode" runat="server" ControlToValidate="txtZipCode"
                                                ValidationGroup="PersonDetail" MinimumValue="10000" MaximumValue="99999" Type="Integer"
                                                ErrorMessage="Zip Code is invalid">*</asp:RangeValidator>
                                            &nbsp;&nbsp;<b>Ext:</b>
                                            <asp:TextBox ID="txtZipCodeExt" runat="server" Width="40" MaxLength="4" />
                                            <asp:RangeValidator ID="rgvZipCodeExt" runat="server" ControlToValidate="txtZipCodeExt"
                                                ValidationGroup="PersonDetail" MinimumValue="1000" MaximumValue="9999" Type="Integer"
                                                ErrorMessage="Zip Code Ext is invalid">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trPhoneFaxHR2" runat="server">
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr id="trPhoneFax" runat="server">
                                        <td class="fieldLabel">
                                            Fax:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPhoneFax3" runat="server" Width="98" MaxLength="30" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trPhoneCell" runat="server">
                                        <td class="fieldLabel">
                                            Cell:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPhoneCell" runat="server" Width="98" MaxLength="30" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trEmailAddress" runat="server">
                                        <td class="fieldLabel">
                                            Email Address:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEmailAddress" runat="server" Width="210" MaxLength="50" />
                                            <asp:RegularExpressionValidator ID="rfvEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
                                                ValidationGroup="PersonDetail" ErrorMessage="Please enter a valid Email Address"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
            <!-- sectionCaseIndividual -->
            <CSCMSUC:SectionContainer ID="sectionCaseIndividual" runat="server" Visible="true"
                Width="100%" HeaderText="CASE/ACTION INFORMATION">
                <asp:Panel ID="pnlCaseIndividual" runat="server">
                    <table width="100%">
                        <tr>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr id="trCustodialTypes" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Custodial Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCustodialTypes" runat="server" Width="250" DataSourceID="dsCustodialTypes"
                                                DataValueField="CustodialTypeID" DataTextField="CustodialTypeName" AppendDataBoundItems="true"
                                                onchange="pdjava.SelectNPFlag(this);">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsCustodialTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select CustodialTypeID, CustodialTypeName from CustodialType">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="tr10" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Person Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlPersonType" runat="server" Width="250" DataValueField="PersonTypeID"
                                                DataTextField="PersonTypeName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfPersonType" runat="server" ControlToValidate="ddlPersonType"
                                                ValidationGroup="PersonDetail" ErrorMessage="Please enter a Person Type" Display="Dynamic">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trFatherType" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Father Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlFatherType" runat="server" Width="250" DataSourceID="dsFatherTypes"
                                                DataValueField="FatherTypeID" DataTextField="FatherTypeName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsFatherTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select FatherTypeID, FatherTypeName from FatherType"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trGrantparent" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Grantparent Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlGrantparent" runat="server" Width="250" DataSourceID="dsGrantparentTypes"
                                                DataValueField="GrantParentTypeID" DataTextField="GrantParentTypeName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsGrantparentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select GrantParentTypeID, GrantParentTypeName from GrantParentType">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trIndigenceHearing" runat="server">
                                        <td class="fieldLabel">
                                            Indigence Hearing:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkIndigenceHearing" runat="server" CssClass="chkNoPad" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trNeedAttorneyAppointed" runat="server">
                                        <td class="fieldLabel">
                                            Needs Attorney Appointed:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkNeedAttorneyAppointed" runat="server" CssClass="chkNoPad" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trRequestedAdditionalTime" runat="server">
                                        <td class="fieldLabel">
                                            Requested Additional Time:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkRequestedAdditionalTime" runat="server" CssClass="chkNoPad" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trDateFoundIndegent" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Date Found Indigent:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtDateFoundIndegent" runat="server" SharedCalendarID="sharedCalendar" />
                                        </td>
                                    </tr>
                                    <tr id="trddlCounselStatus" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Counsel Status:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCounselStatus" runat="server" onchange="pdjava.CounselStatusChange(this);">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                                <asp:ListItem Value="1">Attorney Appointed</asp:ListItem>
                                                <asp:ListItem Value="2">Attorney Retained</asp:ListItem>
                                                <asp:ListItem Value="3">Waived/Refused Right to Counsel</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trCounselDate" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Counsel Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtCounselDate" runat="server" SharedCalendarID="sharedCalendar" />
                                        </td>
                                    </tr>
                                    <tr id="trAttorney" runat="server">
                                        <td class="fieldLabel">
                                            Attorney:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAttorney" runat="server" Width="250" DataSourceID="dsAttorney"
                                                DataValueField="PersonGUID" DataTextField="FullName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsAttorney" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="GetAttorneyForCasePerson" SelectCommandType="StoredProcedure"
                                                CancelSelectOnNullParameter="false" OnSelecting="dsAttorney_Selecting">
                                                <SelectParameters>
                                                    <asp:Parameter Name="ActionGUID" Type="String" />
                                                    <asp:Parameter Name="PersonGUID" Type="String" />
                                                    <asp:Parameter Name="CountyID" Type="Int32" />
                                                    <asp:Parameter Name="PersonTypeID" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr id="trchkNP" runat="server">
                                        <td class="fieldLabel">
                                            <asp:CheckBox ID="chkNP" runat="server" CssClass="chkNoPad" Text="NP " TextAlign="Left"
                                                Font-Bold="true" onClick="pdjava.NPChanged(this);" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkNPMinor" runat="server" CssClass="chkNoPad" Text="NP Minor "
                                                TextAlign="Left" Font-Bold="true" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:CheckBox ID="chkInJail" runat="server" CssClass="chkNoPad" Text="In Jail " TextAlign="Left"
                                                Font-Bold="true" />
                                        </td>
                                    </tr>
                                    <tr id="trServiceType" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Service Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlServiceType" runat="server" Width="250" DataSourceID="dsServiceTypes"
                                                DataValueField="ServiceTypeID" DataTextField="ServiceTypeName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsServiceTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                SelectCommand="select ServiceTypeID, ServiceTypeName from ServiceType"></asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trServiceDate" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Service Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtServiceDate" runat="server" SharedCalendarID="sharedCalendar" />
                                        </td>
                                    </tr>
                                    <tr id="trAnswerToServiceDate" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Date Answer Filed:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtAnswerToServiceDate" runat="server" SharedCalendarID="sharedCalendar" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" style="padding-bottom: 5px;">
                                <asp:Button ID="Button1" runat="server" Text="&nbsp;Save Person&nbsp;" CssClass="submitMedium"
                                    ValidationGroup="PersonDetail" OnClick="btnSave_Click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="Button3" runat="server" Text="Save &amp; Return to Case" CssClass="submitMedium"
                                    ValidationGroup="PersonDetail" Visible="true" OnClick="btnSaveAndReturn_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
            <!-- sectionCaseIndividual -->
            <CSCMSUC:SectionContainer ID="sectionPaternityTest" runat="server" Visible="true"
                Width="100%" HeaderText="PATERNITY TESTING">
                <asp:Panel ID="pnlPaternityTest" runat="server">
                    <table width="100%">
                        <tr>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr id="trchkNotOrder" runat="server">
                                        <td class="fieldLabel">
                                            Not Ordered:&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkNotOrder" runat="server" CssClass="chkNoPad" onclick="pdjava.WaivedRightChanged(this);" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr id="trOrderDate" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Ordered Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtOrderDate" runat="server" SharedCalendarID="sharedCalendar" />
                                            <asp:RangeValidator ID="rvdtOrderDate" runat="server" ControlToValidate="dtOrderDate"
                                                ValidationGroup="PersonDetail" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="trdtResultDate" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Result Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtResultDate" runat="server" SharedCalendarID="sharedCalendar" />
                                            <asp:RangeValidator ID="rvdtResultDate" runat="server" ControlToValidate="dtResultDate"
                                                ValidationGroup="PersonDetail" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr id="tr9" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Test Result Positive:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkTestPositive" runat="server" CssClass="chkNoPad" />
                                        </td>
                                    </tr>
                                    <tr id="trExcluded" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Excluded:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkIsExcluded" runat="server" CssClass="chkNoPad" />
                                        </td>
                                    </tr>
                                    <tr id="trPaternityEstablishedDate" runat="server" visible="true">
                                        <td class="fieldLabel">
                                            Paternity Established Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtPaternityEstablishedDate" runat="server" SharedCalendarID="sharedCalendar" />
                                            <asp:RangeValidator ID="rvdtPaternityEstablishedDate" runat="server" ControlToValidate="dtPaternityEstablishedDate"
                                                ValidationGroup="PersonDetail" MinimumValue="1/1/1900" Type="Date" ErrorMessage="Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" width="50%">
                                <table cellspacing="3" width="100%">
                                    <tr id="TrTestOpenedNote" runat="server" visible="true">
                                        <td class="fieldLabel" colspan="2" style="text-align: left;">
                                            Test Note:<br />
                                            <asp:TextBox ID="txtTestOpenedNote" runat="server" TextMode="MultiLine" Width="100%"
                                                Height="70" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" style="padding-bottom: 5px;">
                                <asp:Button ID="Button2" runat="server" Text="&nbsp;Save Person&nbsp;" CssClass="submitMedium"
                                    ValidationGroup="PersonDetail" OnClick="btnSave_Click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="Button4" runat="server" Text="Save &amp; Return to Case" CssClass="submitMedium"
                                    ValidationGroup="PersonDetail" Visible="true" OnClick="btnSaveAndReturn_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
            <div class="fieldLabelReq" style="font-weight: normal; text-align: left;">
                Fields with <b>red labels</b> are required.</div>
        </div>
    </div>
    <div class="bottomSection" id="divCaseNotes" runat="server" visible="false">
        <UC:CaseNotes ID="caseNotes" runat="server" />
    </div>
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ddlPersonType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlAttorney" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
