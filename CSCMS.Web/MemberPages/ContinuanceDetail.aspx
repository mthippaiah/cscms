﻿<%@ Page Title="CSCMS | Continuance" Language="C#" MasterPageFile="~/CSCMS.master"
    AutoEventWireup="true" CodeFile="ContinuanceDetail.aspx.cs" Inherits="MemberPages_ContinuanceDetail" %>

<%@ Register TagPrefix="UC" TagName="ActionInfo" Src="~/UserControls/ActionSummary/ActionInfo.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNotes" Src="~/UserControls/CaseNotes.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNavigation" Src="~/UserControls/CaseNavigation.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script type="text/javascript">
        var actionType;
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div style="width: 100%">
        <div class="pageTitle" style="float: left; width: 10%;">
            CONTINUANCE</div>
        <div style="float: right; width: 90%; padding-bottom: 5px;" id="divCondinuanceButtons"
            runat="server" visible="false">
            <div style="float: left;">
                <UC:CaseNavigation ID="caseNavigation" runat="server" IncludeEmptyItem="true" />
            </div>
            <div style="float: left;">
                &nbsp;
                <asp:Button ID="btnDelete" runat="server" type="button" Visible="false" class="submitMediumRed"
                    Style="width: 110px" OnCommand="btnDelete_Command" OnClientClick="return confirm('Are you sure you want to delete this continuance?');"
                    Text="Delete Continuance" />
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" />
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <telerik:RadWindow ID="winAssociate" runat="server" Skin="Office2007" Modal="true"
        ShowContentDuringLoad="false" ReloadOnShow="true" Behaviors="Close" VisibleStatusbar="false"
        Width="410" Height="200" />
    <div id="divMain" runat="server" class="newCase">
        <div id="divMainSub" runat="server" class="newCaseSub" style="width: 873px;">
            <UC:ActionInfo ID="actionInfo" runat="server" />
            <CSCMSUC:SectionContainer ID="sectionContinuance" runat="server" HeaderText="CONTINUANCE INFORMATION"
                Width="100%">
                <asp:Panel ID="pnlContinuance" runat="server" DefaultButton="btnSaveContinuance">
                    <table align="center">
                        <tr>
                            <td valign="top">
                                <table cellspacing="3" align="center">
                                    <tr>
                                        <td class="fieldLabelReq">
                                            Continuance Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtRequestedDate" runat="server" AutoPostBack="true" OnSelectedDateChanged="ContinuanceDate_SelectedDateChanged" />
                                            <asp:RequiredFieldValidator ID="rfvRequestedDate" runat="server" ControlToValidate="dtRequestedDate"
                                                Display="Dynamic" ErrorMessage="Please select a Continuance Requested date">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabelReq">
                                            Requested by:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlRequestedBy" runat="server" Width="202" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvddlRequestedBy" runat="server" ControlToValidate="ddlRequestedBy"
                                                Display="Dynamic" ErrorMessage="Please select a Continuance Requested by person">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="Tr13" runat="server">
                                        <td class="fieldLabel">
                                            Agreed:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkAgreed" runat="server" Enabled="true" Text="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            Continuance Note:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtContinuanceNote" runat="server" TextMode="MultiLine" Width="438"
                                                Height="40" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            Continued to Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtContinueToDate" runat="server" />
                                            <asp:CompareValidator ID="cvdtContinueToDate" runat="server" ErrorMessage="Continue to date must be after requested date."
                                                ControlToValidate="dtContinueToDate" ControlToCompare="dtRequestedDate" Operator="GreaterThanEqual">*</asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="padding-top: 10px; padding-bottom: 5px;">
                                            <asp:Button ID="btnSaveAndReturnToCase" runat="server" Visible="true" Text="Save &amp; Return to Case"
                                                CssClass="submit" OnClick="btnSaveAndReturnToCase_Click" />
                                            <asp:Button ID="btnSaveContinuance" runat="server" Text="Save Continuance" CssClass="submit"
                                                OnClick="btnSave_Click" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
        </div>
    </div>
    <div class="bottomSection" id="divCaseNotes" runat="server" visible="false">
        <UC:CaseNotes ID="caseNotes" runat="server" />
    </div>
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 50px;" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
