﻿<%@ Page Title="CSCMS | Action Detail" Language="C#" MasterPageFile="~/CSCMS.master"
    AutoEventWireup="true" CodeFile="ActionDetail.aspx.cs" Inherits="MemberPages_ActionDetail"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC" TagName="CaseNotes" Src="~/UserControls/CaseNotes.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNavigation" Src="~/UserControls/CaseNavigation.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="UC" TagName="CaseActions" Src="~/UserControls/ActionSummary/CaseActions.ascx" %>
<%@ Register Src="../UserControls/ChildSupportCalculation.ascx" TagName="ChildSupportCalculation"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script language="javascript" type="text/javascript">
        var adjava = new ActionDetail();
        function createADCSObjects()
        {
            var txtPP1 = $find("<%= fvCase.FindControl("scChildSupport").FindControl("txtPP1").ClientID %>");
            var txtMS1 = $find("<%= fvCase.FindControl("scChildSupport").FindControl("txtMS1").ClientID %>");
            var txtPP2 = $find("<%= fvCase.FindControl("scChildSupport").FindControl("txtPP2").ClientID %>");
            var txtMS2 = $find("<%= fvCase.FindControl("scChildSupport").FindControl("txtMS2").ClientID %>");
            var txtTotal = $find("<%= fvCase.FindControl("scChildSupport").FindControl("txtTotal").ClientID %>");
            return {txtPP1:txtPP1,txtMS1:txtMS1,txtPP2:txtPP2,txtMS2:txtMS2,txtTotal:txtTotal};
        }
        function createCaseInfoObjects()
        {
            var oagU = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("txtOAGOffice").ClientID %>");
            var oag1 = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("txtOAG1").ClientID %>");
            var oag2 = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("txtOAG2").ClientID %>");
            var oag3 = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("txtOAG3").ClientID %>");
            var orgC = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("ddlReferringCourt").ClientID %>");
            var act = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("ddlActionType").ClientID %>");
            var county = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("ddlCounty").ClientID %>");
            var cause = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("txtCauseNumber").ClientID %>");
            var style = document.getElementById("<%= fvCase.FindControl("sectionCase").FindControl("txtStyle").ClientID %>");
            var fd = $find("<%= fvCase.FindControl("sectionCase").FindControl("dtFilingDate").ClientID %>");
            return {act:act, county:county, cause:cause, style:style, oagU:oagU, oag1:oag1, oag2:oag2, oag3:oag3, orgC:orgC, fd:fd };
        }
        function createADObjects()
        {
            //var dtDispRDate =  $find("<%= this.DispositionRenderedDateClientID %>");
            var dtArrears =  $find("<%= fvCase.FindControl("scChildSupport").FindControl("dtArrearageAsOf").ClientID %>");
            var dtMedical =  $find("<%= fvCase.FindControl("scChildSupport").FindControl("dtMedicalPastDueAsOf").ClientID %>");
            var dtDispSDate =  $find("<%= fvCase.FindControl("sectionCase").FindControl("dtDispositionSignedDate").ClientID %>");
            var dtDispRDate =  $find("<%= fvCase.FindControl("sectionCase").FindControl("dtDispositionRenderedDate").ClientID %>");
            var ddlDisp = document.getElementById("<%= this.DispositionDropDownClientID %>");
            var isAdmin = document.getElementById("<%= inputIsAdmin.ClientID %>");
            var hfODisp = document.getElementById("<%= inputOriginalDisposition.ClientID %>");
            return {dtDispRDate:dtDispRDate,ddlDisp:ddlDisp,isAdmin:isAdmin,hfODisp:hfODisp, dtArrears:dtArrears,dtMedical:dtMedical,dtDispSDate:dtDispSDate};
        }
        function showConfirmDialog()
        {
            var ddlCaseFunction = document.getElementById('<%= ddlCaseFunction.ClientID %>');
            var action = ddlCaseFunction.options[ddlCaseFunction.selectedIndex].value;
            
            if (action == 'DeleteAction' && <%= IsMessageVisible(ConfirmMessage.DeleteActionConfirm).ToString().ToLower() %>)
            {
                var dialog = $find('<%= winConfirm.ClientID %>');
                dialog.show();
            }
            else if (action == 'DeleteCase' && <%= IsMessageVisible(ConfirmMessage.DeleteCaseConfirm).ToString().ToLower() %>)
            {
                var dialog = $find('<%= winConfirm.ClientID %>');
                dialog.setUrl('PopUpConfirm.aspx?mid=6');
                dialog.show();
            }
            else
            {
                <%= ClientScript.GetPostBackEventReference(ddlCaseFunction, null) %>
            }
        }
        function dialogClose(sender, args)
        {
            if (args.get_argument() != null && args.get_argument() == true)
            {
                <%= ClientScript.GetPostBackEventReference(ddlCaseFunction, null) %>
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div style="width: 100%;">
        <div class="pageTitle" style="float: left; width: 90%;">
            Case</div>
        <div style="float: right; width: 10%; text-align: right; vertical-align: top">
            <asp:LinkButton ID="printCaseDetail" runat="server" Text="Print" OnClick="printCaseDetail_OnClick"
                CausesValidation="false"></asp:LinkButton>
        </div>
    </div>
    <asp:HiddenField ID="inputIsAdmin" runat="server" />
    <asp:HiddenField ID="inputOriginalDisposition" runat="server" />
    <telerik:RadWindow ID="winConfirm" runat="server" Skin="Office2007" NavigateUrl="PopUpConfirm.aspx?mid=3"
        Modal="true" Behaviors="Close" VisibleStatusbar="false" Width="350" Height="250"
        OpenerElementID="btnDeleteCase" OnClientClose="dialogClose" />
    <div style="width: 100%;">
        <div style="float: right; padding-bottom: 5px;" id="divCaseButtons" runat="server">
            <div style="float: left;">
                <UC:CaseNavigation ID="caseNavigation" runat="server" />
            </div>
            <div style="float: left;">
                &nbsp;&nbsp; <b>Case: </b>
                <asp:DropDownList ID="ddlCaseFunction" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCaseFunction_SelectedIndexChanged" />
                &nbsp;<asp:HyperLink ID="aGenogram2" runat="server" NavigateUrl="~/MemberPages/Genogram2.aspx"
                    Target="_blank" ToolTip="" CssClass="submitMedium">|</asp:HyperLink>&nbsp;
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" />
    <!-- messageBox -->
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <!-- divMain -->
    <div id="divMain" runat="server" class="newCase">
        <div id="divMainSub" runat="server" class="newCaseSub">
            <asp:FormView ID="fvCase" runat="server" DefaultMode="Edit" OnDataBound="fvCase_DataBound"
                OnItemUpdating="fvCase_ItemUpdating">
                <EditItemTemplate>
                    <!-- sectionHead -->
                    <CSCMSUC:SectionContainer ID="sectionHead" runat="server" Width="100%" DisplayType="Note">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td valign="top" style="width: 25%">
                                    <table cellspacing="0">
                                        <tr>
                                            <td class="fieldLabel">
                                                Region:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblRegion" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                Court:
                                            </td>
                                            <td>
                                                <%= this.CurrentUser.OCACourtName %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                Judge:
                                            </td>
                                            <td>
                                                <%= this.CurrentUser.OCACourtJudge %>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="width: 25%">
                                    <table cellspacing="0">
                                        <tr>
                                            <td class="fieldLabel">
                                                Action:
                                            </td>
                                            <td>
                                                <%= this.CurrentAction.ActionNumber %>
                                                /
                                                <%= this.CurrentCause.NumberOfAction %>&nbsp;
                                                <asp:HyperLink ID="lnkPerson" runat="server" Visible="<%# (this.CurrentAction.ActionNumber !=  this.CurrentCause.NumberOfAction) %>"
                                                    NavigateUrl='<%# "ActionDetail.aspx?cid=" + this.CurrentCause.CauseGUID.ToString() + "&a=" + this.CurrentCause.NumberOfAction.ToString() %>'>******</asp:HyperLink></ItemTemplate>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                CauseNumber:
                                            </td>
                                            <td>
                                                <%= this.CurrentCause.CauseNumber %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="width: 25%">
                                    <table cellspacing="0">
                                        <tr>
                                            <td class="fieldLabel">
                                                Active/Inactive:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblStatus" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                Pending/Disposed:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPending" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                Action Age (in days):
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAging" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="width: 25%">
                                    <table cellspacing="0">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="text-align: right">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btnRefreshHeader" runat="server" OnClick="btnRefreshHeader_Click"
                                                    Text="Refresh" CssClass="submitMedium" CausesValidation="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                FilingDate:
                                            </td>
                                            <td>
                                                <%= this.CurrentAction.ActionFiledDate.Value.ToShortDateString() %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                Filing Age(in days):
                                            </td>
                                            <td>
                                                <%= (int)(DateTime.Now - this.CurrentAction.ActionFiledDate.Value).TotalDays%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </CSCMSUC:SectionContainer>
                    <!-- CASE INFORMATION -->
                    <CSCMSUC:SectionContainer ID="sectionCase" runat="server" HeaderText="CASE INFORMATION"
                        Width="100%">
                        <asp:Panel ID="pnlCase" runat="server" DefaultButton="btnSave">
                            <table width="100%">
                                <tr>
                                    <td valign="top" width="50%">
                                        <table cellspacing="3" width="100%">
                                            <tr>
                                                <td class="fieldLabelReq">
                                                    Action Type:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlActionType" runat="server" Width="200" DataSourceID="dsActionType"
                                                        DataValueField="ActionTypeID" DataTextField="ActionTypeName" AppendDataBoundItems="true"
                                                        SelectedValue='<%# this.CurrentAction.ActionType.ActionTypeID %>' Enabled='<%# IsNewAction %>'>
                                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvddlActionType" runat="server" ControlToValidate="ddlActionType"
                                                        ValidationGroup="CaseSave" ErrorMessage="Please select an Action Type">*</asp:RequiredFieldValidator>
                                                    <asp:SqlDataSource ID="dsActionType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                        SelectCommand="select ActionTypeID, ActionTypeName from ActionType order by SortOrder">
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabelReq">
                                                    County:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCounty" runat="server" Width="200" DataSourceID="dsCounty"
                                                        DataValueField="CountyID" DataTextField="CountyName" AppendDataBoundItems="true"
                                                        SelectedValue='<%# this.CurrentCause.County.CountyID %>' Enabled='<%# IsNewAction %>'>
                                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvddlCounty" runat="server" ControlToValidate="ddlCounty"
                                                        ValidationGroup="CaseSave" ErrorMessage="Please select a County">*</asp:RequiredFieldValidator>
                                                    <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                        SelectCommand="GetUserCounties" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                                                        OnSelecting="dsCounty_Selecting">
                                                        <SelectParameters>
                                                            <asp:Parameter Name="UserId" Type="Int16" />
                                                            <asp:Parameter Name="CourtID" Type="Int16" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabelReq">
                                                    Cause Number:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCauseNumber" runat="server" Width="150" MaxLength="50" Text='<%# Eval("CauseNumber") %>'
                                                        Enabled='<%# IsNewAction %>' />
                                                    <asp:RequiredFieldValidator ID="rfvCauseNumber" runat="server" ControlToValidate="txtCauseNumber"
                                                        ValidationGroup="CaseSave" ErrorMessage="Please enter a Cause Number">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel">
                                                    Style:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtStyle" runat="server" Width="250" MaxLength="50" Text='<%# this.CurrentAction.Style %>'
                                                        Enabled='<%# IsNewAction %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="padding-top: 3px">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr id="trDispositionRenderedDate" runat="server">
                                                <td class="fieldLabel">
                                                    Disposition Rendered Date:
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="dtDispositionRenderedDate" runat="server" MaxDate='<%# DateTime.Today %>'>
                                                        <ClientEvents OnDateSelected="adjava.DispRDateSelected" />
                                                    </telerik:RadDatePicker>
                                                    <asp:RangeValidator ID="rvDispositionRenderedDate" runat="server" ControlToValidate="dtDispositionRenderedDate"
                                                        ValidationGroup="CaseSave" MinimumValue="1/1/1900" MaximumValue='<%# DateTime.Today.ToString("d") %>'
                                                        Type="Date" ErrorMessage="Disposition Rendered Date must not be in the future">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr id="tr12" runat="server">
                                                <td class="fieldLabel">
                                                    Disposition Signed Date:
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="dtDispositionSignedDate" runat="server" MaxDate='<%# DateTime.Today %>' />
                                                    <asp:RangeValidator ID="rvDispositionSignedDate" runat="server" ControlToValidate="dtDispositionSignedDate"
                                                        ValidationGroup="CaseSave" MinimumValue="1/1/1900" MaximumValue='<%# DateTime.Today.ToString("d") %>'
                                                        Type="Date" ErrorMessage="Disposition Signed Date must not be in the future">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr id="trServiceLastNecessaryParyDate" runat="server">
                                                <td class="fieldLabel">
                                                    Date of Service on last necessary party:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblServiceLastNecessaryParyDate" runat="server" />
                                                </td>
                                            </tr>
                                            <tr id="trAppealFiledDate" runat="server">
                                                <td class="fieldLabel">
                                                    Appeal Filed Date:
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="dtAppealFiledDate" runat="server" MaxDate='<%# DateTime.Today %>' />
                                                    <asp:RangeValidator ID="rvdtAppealFiledDate" runat="server" ControlToValidate="dtAppealFiledDate"
                                                        ValidationGroup="CaseSave" MinimumValue="1/1/1900" MaximumValue='<%# DateTime.Today.ToString("d") %>'
                                                        Type="Date" ErrorMessage="Appeal Filed Date must not be in the future">*</asp:RangeValidator>
                                                    <asp:Label ID="lblDaysUntilDismissal" runat="server" />
                                                </td>
                                            </tr>
                                            <tr id="trTranferToCounty" runat="server">
                                                <td class="fieldLabel">
                                                    Transfer to County:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlTransferToCounty" runat="server" Width="200" DataSourceID="dsAllCounty"
                                                        DataValueField="CountyID" DataTextField="CountyName" AppendDataBoundItems="true"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlTransferToCounty_SelectedIndexChanged"
                                                        SelectedValue='<%# TransferToCountyIDString %>'>
                                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:SqlDataSource ID="dsAllCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                        SelectCommand="select CountyID, CountyName from County order by CountyName">
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr id="trTransferToOCACourt" runat="server">
                                                <td class="fieldLabel">
                                                    Transfer to Court:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlTransferToOCACourt" runat="server" Width="200" DataValueField="OCACourtID"
                                                        DataTextField="CourtName" AppendDataBoundItems="true">
                                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="padding-top: 3px">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr id="trRelatedCasesAdd" runat="server" visible="true">
                                                <td class="fieldLabel" colspan="2" style="text-align: left;">
                                                    Related Cause(s):
                                                </td>
                                            </tr>
                                            <tr id="trRelatedCasesAdd2" runat="server" visible="true">
                                                <td colspan="2">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnAddRelatedCase" runat="server" Text="Add" CssClass="submitMedium"
                                                                    CausesValidation="false" Width="60" PostBackUrl="~/MemberPages/RelatedCauses.aspx" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trRelatedCasesList" runat="server" visible="true">
                                                <td colspan="2">
                                                    <table>
                                                        <asp:Repeater ID="rptRelatedCases" runat="server" DataSourceID="dsRelatedCases">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnDeleteRelateCase" runat="server" ImageUrl="../images/x_sm.gif"
                                                                            CausesValidation="false" AlternateText="Delete Case Relationship" Style="padding-right: 5px;"
                                                                            CommandArgument='<%# Eval("CauseGUID") %>' OnCommand="btnDeleteRelatedCase_Command"
                                                                            OnClientClick="return confirm('Are you sure you want to delete this relation?');"
                                                                            Visible='<%# (!this.CurrentUser.ViewAllUser || (Convert.ToBoolean(Session["Impersonating"]) == true)) %>' />
                                                                    </td>
                                                                    <td>
                                                                        <a href="ActionDetail.aspx?cid=<%# Eval("CauseGUID") %>&a=<%# Eval("NumberOfAction") %>"
                                                                            style="font-weight: normal;">
                                                                            <%# Eval("CauseNumber") %></a>&nbsp;&nbsp;
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:SqlDataSource ID="dsRelatedCases" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                            SelectCommand="GetRelatedCauses" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                                                            OnSelecting="dsRelatedCases_Selecting">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="CauseGUID" Type="String" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                        <asp:SqlDataSource ID="dsRelastionShipTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                            SelectCommand="select * from CauseRelationshipType order by SortOrder"></asp:SqlDataSource>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="10px">
                                        &nbsp;
                                    </td>
                                    <td valign="top" width="50%">
                                        <table cellspacing="3" width="100%">
                                            <tr>
                                                <td class="fieldLabel">
                                                    OAG Unit:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOAGOffice" runat="server" Width="100" MaxLength="50" Text='<%# Eval("OCAUnit") %>'
                                                        Enabled='<%# IsNewAction %>' />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnEditCaseInfo" Text="Edit" runat="server" OnClientClick="adjava.EnableCaseControls(); return false;"
                                                        Width="60px" CausesValidation="false" CssClass="submitMedium" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel">
                                                    OAG #:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtOAG1" runat="server" Width="70" MaxLength="10" Enabled='<%# IsNewAction %>' />&nbsp;
                                                    <asp:TextBox ID="txtOAG2" runat="server" Width="70" MaxLength="10" Enabled='<%# IsNewAction %>' />&nbsp;
                                                    <asp:TextBox ID="txtOAG3" runat="server" Width="70" MaxLength="10" Enabled='<%# IsNewAction %>' />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel">
                                                    Originating Court:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlReferringCourt" runat="server" Width="250" DataSource='<%# GetOriginatingCourts() %>'
                                                        DataValueField="OriginatingCourtID" DataTextField="CourtName" AppendDataBoundItems="true"
                                                        SelectedValue='<%# OriginatingCourtIDString %>' Enabled='<%# IsNewAction %>'>
                                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:SqlDataSource ID="dsReferringCourt" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                        SelectCommand="select OriginatingCourtID, CourtName from OriginatingCourt where (CountyID = @CountyID and (ExpirationDate is null or ExpirationDate > @Date)) or (OriginatingCourtID is null or OriginatingCourtID = @OrgCourtID) order by SortOrder"
                                                        OnSelecting="dsReferringCourt_Selecting">
                                                        <SelectParameters>
                                                            <asp:Parameter Name="CountyID" Type="Int16" />
                                                            <asp:Parameter Name="Date" Type="DateTime" />
                                                            <asp:Parameter Name="OrgCourtID" Type="Int16" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabelReq">
                                                    Filing Date:
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="dtFilingDate" runat="server" MaxDate='<%# DateTime.Today %>'
                                                        Enabled='<%# IsNewAction %>' />
                                                    <asp:RequiredFieldValidator ID="rfvFilingDate" runat="server" ControlToValidate="dtFilingDate"
                                                        ValidationGroup="CaseSave" ErrorMessage="Please enter a Filing Date">*</asp:RequiredFieldValidator>
                                                    <asp:RangeValidator ID="rgvFilingDate" runat="server" ControlToValidate="dtFilingDate"
                                                        ValidationGroup="CaseSave" MinimumValue="1/1/1900" MaximumValue='<%# DateTime.Today.ToString("d") %>'
                                                        Type="Date" ErrorMessage="Filing Date must not be in the future">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel">
                                                    Disposition:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlDisposition" runat="server" Width="200" DataSource='<%# GetDispositionTypes() %>'
                                                        DataValueField="DispositionTypeID" DataTextField="DispositionTypeName" AppendDataBoundItems="true"
                                                        SelectedValue='<%# DispositionTypeIDString %>'>
                                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:EntityDataSource ID="dsDispostion" runat="server" ConnectionString="name=CscmsEntities"
                                                        DefaultContainerName="CscmsEntities" EntitySetName="DispositionType" OrderBy="it.DispositionTypeName"
                                                        Where="it.DispositionTypeName != &quot; None - Case Open&quot;">
                                                    </asp:EntityDataSource>
                                                </td>
                                            </tr>
                                            <tr id="trlDispositionDetail" runat="server">
                                                <td class="fieldLabel">
                                                    Disposition Detail:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlDispositionDetail" runat="server" Width="200" DataSourceID="dsDispositionDetail"
                                                        DataValueField="DispositionDetailID" DataTextField="DispositionDetailName" AppendDataBoundItems="true"
                                                        SelectedValue='<%# DispositionDetailIDString %>'>
                                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:SqlDataSource ID="dsDispositionDetail" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                        SelectCommand="select DispositionDetailID, DispositionDetailName from DispositionDetail order by SortOrder">
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr runat="server">
                                                <td class="fieldLabel">
                                                    Active
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkActive" runat="server" Enabled="false" Text="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel">
                                                    District Judge Signed:
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkDJS" runat="server" Text="" />
                                                </td>
                                                <tr>
                                               <td class="fieldLabel">
                                                    Judge Signed Date:
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="dtJudgeSignedDate" runat="server" MaxDate='<%# DateTime.Today %>'/>
                                                </td>
                                            </tr>
                                            </tr>
                                            <tr id="Tr13" runat="server" visible="false">
                                                <td class="fieldLabel">
                                                    Share with other courts
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkShared" runat="server" Enabled="true" Text="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fieldLabel">
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr id="trRelatedCasesRule" runat="server">
                                                <td colspan="2">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr id="trRelatedActionsList" runat="server">
                                                <td colspan="2">
                                                    <table>
                                                        <tr id="trRelatedActionsLabel" runat="server" visible="true">
                                                            <td class="fieldLabel" colspan="2" style="text-align: left;">
                                                                Related Action(s):
                                                            </td>
                                                        </tr>
                                                        <tr id="tr2" runat="server" visible="true">
                                                            <td colspan="2">
                                                                <table>
                                                                    <asp:Repeater ID="rptRelatedActions" runat="server" DataSourceID="dsRelatedActions">
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td runat="server" id="OtherActions" visible='<%# Convert.ToInt32(Eval("CurrentAction")) == 0 %>'>
                                                                                    <a href="ActionDetail.aspx?cid=<%# Eval("CauseGUID") %>&a=<%# Eval("ActionNumber") %>"
                                                                                        style="font-weight: normal;">(<%# Eval("ActionNumber")%>)&nbsp;<%# Eval("ActionTypeName")%></a>&nbsp;&nbsp;
                                                                                </td>
                                                                                <td runat="server" id="CurrentAction" visible='<%# Convert.ToInt32(Eval("CurrentAction")) == 1 %>'>
                                                                                    <asp:Label CssClass="fieldLabelReq" runat="server">(<%# Eval("ActionNumber")%>)&nbsp;<%# Eval("ActionTypeName")%></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <%# string.Format("{0:M/d/yyyy}", Eval("ActionFiledDate"))%>
                                                                                </td>
                                                                                <td>
                                                                                    <%#  Eval("DispositionTypeName")%>
                                                                                </td>
                                                                                <td>
                                                                                    <%# string.Format("{0:M/d/yyyy}", Eval("DispositionRenderedDate"))%>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:SqlDataSource ID="dsRelatedActions" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                                        SelectCommand="GetRelatedActions" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                                                                        OnSelecting="dsRelatedActions_Selecting">
                                                                        <SelectParameters>
                                                                            <asp:Parameter Name="CauseGUID" Type="String" />
                                                                            <asp:Parameter Name="ActionGUID" Type="String" />
                                                                        </SelectParameters>
                                                                    </asp:SqlDataSource>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trRelatedActionsHr" runat="server" visible="true">
                                                <td colspan="2">
                                                    <hr />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" style="padding-top: 5px; padding-bottom: 5px;">
                                        <asp:Button ID="btnSave" runat="server" Text="Save Case" CssClass="submit" OnClick="btnSave_Click"
                                            ValidationGroup="CaseSave" CausesValidation="true" OnClientClick="return adjava.ValidateSaveCase();" />
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </CSCMSUC:SectionContainer>
                    <!-- PERSONS -->
                    <CSCMSUC:SectionContainer ID="sectionPersons" runat="server" Visible="false" Width="100%"
                        HeaderText="PERSONS" DisplayType="NoPadding">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnAddChild" runat="server" Text="Add Child" CssClass="submitMedium"
                                        Width="100" CausesValidation="false" PostBackUrl='<%# GetAddPersonUrl(3) %>' />&nbsp;
                                    <asp:Button ID="btnAddFather" runat="server" Text="Add Father" CssClass="submitMedium"
                                        Width="100" CausesValidation="false" PostBackUrl='<%# GetAddPersonUrl(2) %>' />&nbsp;
                                    <asp:Button ID="btnAddMother" runat="server" Text="Add Mother" CssClass="submitMedium"
                                        Width="100" CausesValidation="false" PostBackUrl='<%# GetAddPersonUrl(1) %>' />&nbsp;
                                    <asp:Button ID="btnAddGrandFather" runat="server" Text="Add GrandFather" CssClass="submitMedium"
                                        Width="100" CausesValidation="false" PostBackUrl='<%# GetAddPersonUrl(5) %>' />&nbsp;
                                    <asp:Button ID="btnAddGrandMother" runat="server" Text="Add GrandMother" CssClass="submitMedium"
                                        Width="100" CausesValidation="false" PostBackUrl='<%# GetAddPersonUrl(4) %>' />&nbsp;
                                    <asp:Button ID="btnAddIndividual" runat="server" Text="Add Others" CssClass="submitMedium"
                                        Width="100" CausesValidation="false" PostBackUrl='<%# GetAddPersonUrl(7) %>' />&nbsp;
                                    <asp:Button ID="btnAddAttorney" runat="server" Text="Add Attorney" CssClass="submitMedium"
                                        Width="100" CausesValidation="false" Visible="false" PostBackUrl='<%# GetAddPersonUrl(8) %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadGrid ID="grdPersons" runat="server" OnNeedDataSource="grdPersons_OnNeedDataSource"
                                        OnUpdateCommand="grdPersons_OnUpdateCommand" OnItemDataBound="grdPersons_OnItemDataBound">
                                        <MasterTableView NoMasterRecordsText="There are currently no persons." DataKeyNames="PersonGUID">
                                            <Columns>
                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                                </telerik:GridEditCommandColumn>
                                                <telerik:GridCheckBoxColumn HeaderText="NP?" DataField="NPFlag" HeaderStyle-HorizontalAlign="Center"
                                                    UniqueName="NPFlag" ItemStyle-HorizontalAlign="Center" />
                                                <telerik:GridTemplateColumn HeaderText="Name" SortExpression="LastName, FirstName, MiddleName"
                                                    UniqueName="Name">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lnkPerson" runat="server" NavigateUrl='<%# "PersonDetail.aspx?cid=" + this.CurrentCause.CauseGUID.ToString() + "&a=" + this.CurrentAction.ActionNumber.ToString() + "&pid=" + Eval("PersonGUID").ToString() %>'><%# Eval("LastName") %>,&nbsp;<%# Eval("FirstName") %><%# Convert.ToString(Eval("MiddleName")).Length > 0 ? "&nbsp;" + Eval("MiddleName").ToString() : "" %></asp:HyperLink></ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Person Type" DataField="PersonTypeName" UniqueName="PersonTypeName" />
                                                <telerik:GridBoundColumn HeaderText="Custodial" DataField="CustodialTypeName" UniqueName="CustodialTypeName" />
                                                <telerik:GridBoundColumn HeaderText="Father Type" DataField="FatherTypeName" Visible="true"
                                                    UniqueName="FatherTypeName" />
                                                <telerik:GridBoundColumn HeaderText="Birth Date" DataField="BirthDate" DataFormatString="{0:MM/dd/yyyy}"
                                                    UniqueName="BirthDate" />
                                                <telerik:GridTemplateColumn HeaderText="Age" UniqueName="Age">
                                                    <ItemTemplate>
                                                        <%# CalculateAge( Eval("BirthDate")) %></ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Sex" DataField="Sex" UniqueName="Sex" />
                                                <telerik:GridBoundColumn HeaderText="Service Date" DataField="ServiceDate" DataFormatString="{0:MM/dd/yyyy}"
                                                    UniqueName="ServiceDate" />
                                                <telerik:GridBoundColumn HeaderText="Service Type" DataField="ServiceTypeName" UniqueName="ServiceTypeName" />
                                                <telerik:GridBoundColumn HeaderText="Service Type ID" DataField="ServiceTypeID" UniqueName="ServiceTypeID"
                                                    Visible="false" />
                                                <telerik:GridCheckBoxColumn HeaderText="Ans Filed" DataField="DateAnswerFiled" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" UniqueName="DateAnswerFiled" />
                                                <telerik:GridCheckBoxColumn HeaderText="NC?" DataField="NeedAttorneyAppointed" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" UniqueName="NeedAttorneyAppointed" />
                                                <telerik:GridBoundColumn HeaderText="Attorney" DataField="Relationships" UniqueName="Attorney" />
                                                <telerik:GridBoundColumn HeaderText="Counsel Status" DataField="CounselStatus" UniqueName="CounselStatus" />
                                                <telerik:GridTemplateColumn HeaderText="Relationships" Visible="false" UniqueName="Relationships">
                                                    <ItemTemplate>
                                                        <%# GetRelationships(Eval("Relationships")) %></ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <EditFormSettings EditFormType="Template">
                                                <FormTemplate>
                                                    <table id="tableEditPersonTemplate" cellspacing="2" cellpadding="1" width="250" border="0"
                                                        rules="none" style="border-collapse: collapse; background: white;">
                                                        <tr>
                                                            <td colspan="2" style="font-size: small">
                                                                <b>Person Edit</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                NP:
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="ckbNP" runat="server" Checked='<%# Bind( "NPFlag") %>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Service Date:
                                                            </td>
                                                            <td>
                                                                <telerik:RadDatePicker ID="dtSvcDate" runat="server" DbSelectedDate='<%# Bind( "ServiceDate") %>'>
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Service Type:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlServiceType" runat="server" Width="250" DataSourceID="dsServiceTypes"
                                                                    DataValueField="ServiceTypeID" DataTextField="ServiceTypeName" AppendDataBoundItems="true"
                                                                    SelectedValue='<%# Bind("ServiceTypeID") %>'>
                                                                    <asp:ListItem Value="">- Select -</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                <asp:LinkButton ID="btnUpdate" CausesValidation="false" Text="Update" runat="server"
                                                                    CommandName="Update">
                                                                </asp:LinkButton>&nbsp;
                                                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                    CommandName="Cancel"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </FormTemplate>
                                            </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource ID="dsServiceTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="select ServiceTypeID, ServiceTypeName from ServiceType"></asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </CSCMSUC:SectionContainer>
                    <!-- HEARINGS -->
                    <CSCMSUC:SectionContainer ID="sectionActionHearings" runat="server" Visible="false"
                        Width="100%" HeaderText="HEARINGS and DOCUMENTS" DisplayType="NoPadding">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr id="trCommands" runat="server">
                                <td align="right">
                                    <asp:Button ID="btnAddNewHearing" runat="server" Text="Add Hearing" CssClass="submitMedium"
                                        Style="width: 110px;" CausesValidation="false" PostBackUrl='<%# GetAddHearingUrl() %>' />&nbsp;
                                    <asp:Button ID="btnAddNewDocument" runat="server" Text="Add Document" CssClass="submitMedium"
                                        Style="width: 110px;" CausesValidation="false" PostBackUrl='<%# GetAddNewDocumentUrl() %>' />&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadGrid ID="grdActions" runat="server" OnDeleteCommand="grdActions_DeleteCommand"
                                        OnItemCommand="grdActions_OnItemCommand" OnNeedDataSource="grdActions_OnNeedDataSource"
                                        OnItemDataBound="grdActions_OnItemDataBound">
                                        <HeaderStyle Font-Size="10px" />
                                        <ItemStyle CssClass="gridSmall" />
                                        <AlternatingItemStyle CssClass="gridSmallAlt" />
                                        <MasterTableView NoMasterRecordsText="There are currently no hearings." DataKeyNames="ActionID, ActionType">
                                            <Columns>
                                                <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnEdit" runat="server" Text="Edit" CommandName="edit" Font-Bold="false"
                                                            CausesValidation="false" ImageUrl="../images/edit.gif" />
                                                        &nbsp;|&nbsp;
                                                        <asp:ImageButton ID="btnDelete" CausesValidation="false" runat="server" ImageUrl="../images/x_sm.gif"
                                                            CommandName="delete" Visible='<%# ( !this.CurrentUser.ViewAllUser || (Convert.ToBoolean(Session["Impersonating"]) == true)) %>'
                                                            OnClientClick="return confirm('Are you sure you want to delete this hearing/document?');" /></ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Date" DataField="ActionDate" DataFormatString="{0:MM/dd/yyyy}"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="Date" />
                                                <telerik:GridBoundColumn HeaderText="Time" DataField="HearingTime" UniqueName="HearingTime" />
                                                <telerik:GridTemplateColumn HeaderText="Hearing/Document" UniqueName="Action">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lnkAction" runat="server" Text='<%# Eval("ActionType") %>' NavigateUrl='<%# GetActionUrl(Eval("ActionType").ToString(), Eval("ActionID").ToString()) %>' /></ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Note" DataField="Note" UniqueName="Note" HeaderStyle-Width="350" />
                                                <telerik:GridTemplateColumn HeaderText="View" UniqueName="ExportNoticeColumn">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="btnExportNotice" runat="server" Text="Notice" Target="_blank" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <EditFormSettings EditFormType="Template">
                                                <FormTemplate>
                                                    <table id="tableEditDocsHearings" cellspacing="2" cellpadding="1" width="250" border="0"
                                                        rules="none" style="border-collapse: collapse; background: white;">
                                                        <tr>
                                                            <td colspan="2" style="font-size: small">
                                                                <b>Hearing/Document Edit</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Date:
                                                            </td>
                                                            <td>
                                                                <telerik:RadDatePicker ID="dtHearingDate" runat="server" DbSelectedDate='<%# Bind( "ActionDate") %>' />
                                                                <asp:RequiredFieldValidator ID="rfvHearingDate" runat="server" ControlToValidate="dtHearingDate"
                                                                    ValidationGroup="ActionSave" Display="Dynamic" ErrorMessage="Please select a Hearing Date">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Time:
                                                            </td>
                                                            <td>
                                                                <telerik:RadTimePicker ID="hearingTime" runat="server" Skin="Office2007" Width="75px" SelectedDate='<%# ConvertToTime(Eval("HearingTime")) %>'>
                                                                    <DateInput Font-Size="11px" Font-Names="Arial"/>
                                                                    <TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:15:00" EndTime="18:00:00"
                                                                        Columns="4"/>
                                                                    <TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
                                                                </telerik:RadTimePicker>
                                                                <asp:RequiredFieldValidator ID="rfvHearingTime" runat="server" ControlToValidate="hearingTime"
                                                                    ValidationGroup="ActionSave" Display="Dynamic" ErrorMessage="Please select a Hearing Time">*</asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Note:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Text='<%# Bind( "Note") %>'
                                                                    Height="70px" Width="425px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update"
                                                                    ValidationGroup="ActionSave" CausesValidation="true">
                                                                </asp:LinkButton>&nbsp;
                                                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                    CommandName="Cancel"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </FormTemplate>
                                            </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </CSCMSUC:SectionContainer>
                    <!-- ORDERS -->
                    <CSCMSUC:SectionContainer ID="scActionOrders" runat="server" Visible="true" Width="100%"
                        HeaderText="ORDERS" DisplayType="NoPadding">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr id="tr10" runat="server">
                                <td align="right">
                                    <asp:Button ID="btnAddOrder" runat="server" Text="Add Special/Supplemental Order"
                                        CssClass="submitMedium" Style="width: 180px;" CausesValidation="false" PostBackUrl='<%# GetAddCapiasOrderUrl() %>' />&nbsp;
                                    <asp:Button ID="btnAddPaternityOrder" runat="server" Text="Add Paternity Test Order"
                                        CssClass="submitMedium" Style="width: 150px;" CausesValidation="false" PostBackUrl='<%# GetAddPaternityTestOrderUrl() %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadGrid ID="grdOrders" runat="server" OnNeedDataSource="grdOrders_OnNeedDataSource"
                                        OnUpdateCommand="grdOrders_UpdateCommand" OnItemDataBound="grdOrders_OnItemDataBound">
                                        <HeaderStyle Font-Size="10px" />
                                        <ItemStyle CssClass="gridSmall" />
                                        <AlternatingItemStyle CssClass="gridSmallAlt" />
                                        <MasterTableView NoMasterRecordsText="There are currently no orders." EditMode="EditForms"
                                            DataKeyNames="SpecialOrderID">
                                            <Columns>
                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                                </telerik:GridEditCommandColumn>
                                                <telerik:GridTemplateColumn HeaderText="Name" SortExpression="LastName, FirstName, MiddleName">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lnkPerson" runat="server" NavigateUrl='<%# "OrderDetail.aspx?cid=" + this.CurrentCause.CauseGUID.ToString() + "&a=" + this.CurrentAction.ActionNumber.ToString() + "&did=" + Eval("SpecialOrderID").ToString() + "&t=" + Eval("CommitmentOrCapias").ToString() %>'><%# Eval("LastName") %>,&nbsp;<%# Eval("FirstName") %><%# Convert.ToString(Eval("MiddleName")).Length > 0 ? "&nbsp;" + Eval("MiddleName").ToString() : "" %></asp:HyperLink></ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Type" DataField="CommitmentOrCapias" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" UniqueName="CommitmentOrCapias" />
                                                <telerik:GridBoundColumn HeaderText="Ordered Date" DataField="OrderedDate" DataFormatString="{0:MM/dd/yyyy}"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="OrderedDate" />
                                                <telerik:GridBoundColumn HeaderText="Issued Date" DataField="IssuedDate" DataFormatString="{0:MM/dd/yyyy}"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="IssuedDate" />
                                                <telerik:GridBoundColumn HeaderText="Bond Amount" DataField="BondAmount" DataFormatString="{0:C}"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="BondAmount" />
                                                <telerik:GridBoundColumn HeaderText="Arrest / Effective Date" DataField="ArrestCommitmentEffectiveDate"
                                                    DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                    UniqueName="ArrestCommitmentEffectiveDate" />
                                                <telerik:GridBoundColumn HeaderText="Release Date" DataField="ReleaseDate" DataFormatString="{0:MM/dd/yyyy}"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="ReleaseDate" />
                                                <telerik:GridBoundColumn HeaderText="Days Incarcerated" DataField="DaysIncarcerated"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="DaysIncarcerated"
                                                    ItemStyle-Width="30px" />
                                                <telerik:GridBoundColumn HeaderText="Total Days Incarcerated" DataField="TotalDaysIncarcerated"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="TotalDaysIncarcerated"
                                                    ItemStyle-Width="30px" />
                                                <telerik:GridBoundColumn HeaderText="Note" SortExpression="SpecialOrderNote" DataField="SpecialOrderNote"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="SpecialOrderNote" />
                                            </Columns>
                                            <EditFormSettings EditFormType="Template">
                                                <FormTemplate>
                                                    <table id="tableEditOrderTemplate" cellspacing="2" cellpadding="1" width="100%" border="0"
                                                        rules="none" style="border-collapse: collapse; background: white;">
                                                        <tr>
                                                            <td colspan="2" style="font-size: small">
                                                                <b>Order Edit</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Note:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Height="70px" Width="425px"
                                                                    Text='<%# Bind( "SpecialOrderNote") %>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" colspan="2">
                                                                <asp:LinkButton ID="btnUpdate" CausesValidation="false" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                    runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                                                                </asp:LinkButton>&nbsp;
                                                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                    CommandName="Cancel"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </FormTemplate>
                                            </EditFormSettings>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </CSCMSUC:SectionContainer>
                    <!-- CHLID SUPPORT -->
                    <CSCMSUC:SectionContainer ID="scChildSupport" runat="server" Visible="true" Width="100%"
                        HeaderText="CHILD SUPPORT" DisplayType="NoPadding">
                        <asp:Panel ID="pnlChildSupport" runat="server">
                            <table width="100%">
                                <tr>
                                    <td valign="top" width="50%">
                                        <table cellspacing="3" width="100%">
                                            <tr id="tr8" runat="server">
                                                <td class="fieldLabel">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="fieldLabel">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr id="tr11" runat="server">
                                                <td class="fieldLabel">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="fieldLabel">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr id="tr14" runat="server">
                                                <td class="fieldLabel">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="fieldLabel">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr id="tr3" runat="server">
                                                <td class="fieldLabel">
                                                    CS Judgment Amount:
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ID="txtArrearage" Width="100px" MaxLength="10" runat="server"
                                                        Type="Currency" NumberFormat-AllowRounding="True" NumberFormat-DecimalDigits="2"
                                                        CssClass="AlignRight" Style="text-align: right">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td class="fieldLabel">
                                                    As of:
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="dtArrearageAsOf" runat="server" MaxDate='<%# DateTime.Today %>'>
                                                        <ClientEvents OnDateSelected="adjava.ArrAsOfDateSelected" />
                                                    </telerik:RadDatePicker>
                                                    <asp:RangeValidator ID="rvdtArrearageAsOf" runat="server" ControlToValidate="dtArrearageAsOf"
                                                        MinimumValue="1/1/1900" MaximumValue='<%# DateTime.Today.ToString("d") %>' Type="Date"
                                                        ErrorMessage="Child Support Judgment as of Date must not be in the future">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr id="tr7" runat="server">
                                                <td class="fieldLabel">
                                                    MS Judgment Amount:
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ID="txtMedicalPastDue" Width="100px" MaxLength="10" runat="server"
                                                        Type="Currency" CssClass="AlignRight" Style="text-align: right" NumberFormat-AllowRounding="True"
                                                        NumberFormat-DecimalDigits="2">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                                <td class="fieldLabel">
                                                    As of:
                                                </td>
                                                <td>
                                                    <telerik:RadDatePicker ID="dtMedicalPastDueAsOf" runat="server" MaxDate='<%# DateTime.Today %>' />
                                                    <asp:RangeValidator ID="rvdtMedicalPastDueAsOf" runat="server" ControlToValidate="dtMedicalPastDueAsOf"
                                                        MinimumValue="1/1/1900" MaximumValue='<%# DateTime.Today.ToString("d") %>' Type="Date"
                                                        ErrorMessage="Medical Support Judgment as of Date must not be in the future">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" width="50%">
                                        <table cellspacing="3" width="100%">
                                            <tr id="tr1" runat="server">
                                                <td class="fieldLabel">
                                                    Current CS Amount:
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ID="txtPP1" Width="100px" MaxLength="10" runat="server"
                                                        Type="Currency" CssClass="AlignRight" Style="text-align: right" NumberFormat-AllowRounding="True"
                                                        NumberFormat-DecimalDigits="2">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr id="tr5" runat="server">
                                                <td class="fieldLabel">
                                                    Current MS Amount:
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ID="txtMS1" Width="100px" MaxLength="10" runat="server"
                                                        Type="Currency" CssClass="AlignRight" Style="text-align: right" NumberFormat-AllowRounding="True"
                                                        NumberFormat-DecimalDigits="2">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr id="tr4" runat="server">
                                                <td class="fieldLabel">
                                                    CS Arrearage Amount:
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ID="txtPP2" Width="100px" MaxLength="10" runat="server"
                                                        Type="Currency" CssClass="AlignRight" Style="text-align: right" NumberFormat-AllowRounding="True"
                                                        NumberFormat-DecimalDigits="2">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr id="tr6" runat="server">
                                                <td class="fieldLabel">
                                                    MS Arrearage Amount:
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ID="txtMS2" Width="100px" MaxLength="10" runat="server"
                                                        Type="Currency" CssClass="AlignRight" Style="text-align: right" NumberFormat-AllowRounding="True"
                                                        NumberFormat-DecimalDigits="2">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                            <tr id="tr15" runat="server">
                                                <td class="fieldLabel">
                                                    Total Payment Amount:
                                                </td>
                                                <td>
                                                    <telerik:RadNumericTextBox ID="txtTotal" Width="100px" MaxLength="10" runat="server"
                                                        Type="Currency" CssClass="AlignRight" Style="text-align: right" ReadOnly="true"
                                                        NumberFormat-AllowRounding="True" NumberFormat-DecimalDigits="2">
                                                    </telerik:RadNumericTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnCalculate"
                                            PopupControlID="ModPop" BackgroundCssClass="modalBackground" PopupDragHandleControlID="divpopupheader1"
                                            BehaviorID="ModalPopExtBehaviorID" RepositionMode="None">
                                        </asp:ModalPopupExtender>
                                        <asp:Panel ID="ModPop" runat="server" Style="display: none" BackColor="Bisque" BorderStyle="Outset"
                                            Height="680" Width="495" HorizontalAlign="Center">
                                            <div id="divpopupheader1" style="border-bottom-style: Outset; text-align: center;
                                                background-color: Gray">
                                                Child Support Calculator
                                            </div>
                                            <div id="divpopid1">
                                                <uc1:ChildSupportCalculation ID="ChildSupportCalculation" runat="server" OnCaluclateClicked="HandleCalculateClickedEvent"
                                                    OnApplyAmountCicked="HandleApplyAmountClickedEvent" />
                                            </div>
                                        </asp:Panel>
                                        <asp:Button ID="btnSave2" runat="server" Text="Save Case" CssClass="submit" OnClick="btnSave_Click"
                                            ValidationGroup="CaseSave" CausesValidation="true" OnClientClick="return adjava.ValidateSaveCase();" />
                                        <asp:Button ID="btnTotal" runat="server" Text="Calculate Total" CssClass="submit"
                                            CausesValidation="false" OnClientClick="return adjava.CalculateTotal();" />
                                        <asp:Button ID="btnCalculate" runat="server" Text="Calculator" CssClass="submit"
                                            CausesValidation="false" />
                                        &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/MemberPages/CalculatorRelatedTables.aspx"
                                            Target="_blank" ToolTip="Child Support Calculator Tables" CssClass="submitMedium">Calculator Help</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </CSCMSUC:SectionContainer>
                    <!-- CONTINUANCE -->
                    <CSCMSUC:SectionContainer ID="scContinuance" runat="server" Visible="true" Width="100%"
                        HeaderText="CONTINUANCE" DisplayType="NoPadding">
                        <asp:Panel ID="Panel1" runat="server">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr id="tr9" runat="server">
                                    <td align="right">
                                        <asp:Button ID="btnAddContinuance" runat="server" Text="Add Continuance" CssClass="submitMedium"
                                            Style="width: 110px;" CausesValidation="false" PostBackUrl='<%# GetAddContinuanceUrl() %>' />&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadGrid ID="grdContinuances" runat="server" OnNeedDataSource="grdContinuances_OnNeedDataSource"
                                            OnUpdateCommand="grdContinuances_OnUpdateCommand">
                                            <HeaderStyle Font-Size="10px" />
                                            <ItemStyle CssClass="gridSmall" />
                                            <AlternatingItemStyle CssClass="gridSmallAlt" />
                                            <MasterTableView NoMasterRecordsText="There are currently no continuances." DataKeyNames="ContinuanceID">
                                                <Columns>
                                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                                    </telerik:GridEditCommandColumn>
                                                    <telerik:GridCheckBoxColumn HeaderText="Agreed" DataField="Agreed" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" UniqueName="Agreed" />
                                                    <%--                <telerik:GridBoundColumn HeaderText="Requested by" SortExpression="RequestedBy" DataField="RequestedBy"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />--%>
                                                    <telerik:GridTemplateColumn HeaderText="Requested by" SortExpression="RequestedBy">
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="lnkRequestedBy" runat="server" NavigateUrl='<%# "ContinuanceDetail.aspx?cid=" + this.CurrentCause.CauseGUID.ToString() + "&a=" + this.CurrentAction.ActionNumber.ToString() + "&did=" + Eval("ContinuanceID").ToString() %>'><%# Eval("LastName") %>,&nbsp;<%# Eval("FirstName") %><%# Convert.ToString(Eval("MiddleName")).Length > 0 ? "&nbsp;" + Eval("MiddleName").ToString() : "" %></asp:HyperLink></ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn HeaderText="Continuance Date" DataField="ContinuanceDate"
                                                        DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                        UniqueName="ContinuanceDate" />
                                                    <telerik:GridBoundColumn HeaderText="Continued To Date" DataField="ContinuedToDate"
                                                        DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                        UniqueName="ContinuedToDate" />
                                                    <telerik:GridBoundColumn HeaderText="Note" SortExpression="ContinuanceNote" DataField="ContinuanceNote"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" UniqueName="ContinuanceNote" />
                                                </Columns>
                                                <EditFormSettings EditFormType="Template">
                                                    <FormTemplate>
                                                        <table id="tableEditContinuanceTemplate" cellspacing="2" cellpadding="1" width="100%"
                                                            border="0" rules="none" style="border-collapse: collapse; background: white;">
                                                            <tr>
                                                                <td colspan="2" style="font-size: small">
                                                                    <b>Continuance Edit</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Note:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtContinuanceNote" runat="server" TextMode="MultiLine" Height="70px"
                                                                        Width="425px" Text='<%# Bind( "ContinuanceNote") %>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="2">
                                                                    <asp:LinkButton ID="btnUpdate" CausesValidation="false" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                        runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                                                                    </asp:LinkButton>&nbsp;
                                                                    <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                        CommandName="Cancel"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FormTemplate>
                                                </EditFormSettings>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </CSCMSUC:SectionContainer>
                </EditItemTemplate>
            </asp:FormView>
            <div class="fieldLabelReq" style="font-weight: normal; text-align: left;">
                Fields with <b>red labels</b> are required.</div>
        </div>
    </div>
    <!-- caseNotes -->
    <div class="bottomSection" id="divCaseNotes" runat="server" visible="false">
        <UC:CaseNotes ID="caseNotes" runat="server" />
    </div>
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdActions">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdActions" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="chkActive" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdPersons">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdPersons" LoadingPanelID="loadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="lblServiceLastNecessaryParyDate" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlTransferToCounty">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlTransferToOCACourt" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnRefreshHeader">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="sectionHead" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlPersonType">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlAttorney" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 30px;" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
