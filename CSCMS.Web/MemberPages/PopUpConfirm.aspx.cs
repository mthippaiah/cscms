﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MemberPages_PopUpConfirm : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int msgId = int.Parse(Request.QueryString["mid"]);
            CSCMS.Data.Message message = CscmsContext.Message.Where(it => it.MessageID == msgId).FirstOrDefault();
            lblMessage.Text = message.DisplayText;
            chkSuppress.Visible = false;
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (chkSuppress.Checked)
        {
            int msgId = int.Parse(Request.QueryString["mid"]);
            CSCMS.Data.User user = CscmsContext.User.Include("Message").Where(it => it.UserId == this.CurrentUser.UserID).FirstOrDefault();
           
            if (user != null)
            {
                CSCMS.Data.Message suppressMessage = user.Message.Where(it => it.MessageID == msgId).FirstOrDefault();

                if (suppressMessage == null)
                {
                    CSCMS.Data.Message msg = CscmsContext.Message.Where(it => it.MessageID == msgId).FirstOrDefault();
                    user.Message.Add(msg);
                    CscmsContext.SaveChanges();
                }
            }
        }

        this.ClientScript.RegisterStartupScript(typeof(string), "CloseWindow", "CloseWindow(true);", true);
    }
}
