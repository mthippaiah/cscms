﻿<%@ Page Language="C#" MasterPageFile="~/cscms.master" AutoEventWireup="true" CodeFile="Report.aspx.cs"
    Inherits="MemberPages_Report" Title="CSCMS | Report" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../UserControls/ReportList.ascx" TagName="ReportList" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        REPORTS</div>
    <uc1:ReportList ID="reportList" runat="server" />
</asp:Content>
