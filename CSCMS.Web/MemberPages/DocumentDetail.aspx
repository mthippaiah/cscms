﻿<%@ Page Title="CSCMS | Document" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="DocumentDetail.aspx.cs" Inherits="MemberPages_DocumentDetail" %>

<%@ Register TagPrefix="UC" TagName="ActionInfo" Src="~/UserControls/ActionSummary/ActionInfo.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNotes" Src="~/UserControls/CaseNotes.ascx" %>
<%@ Register TagPrefix="UC" TagName="CaseNavigation" Src="~/UserControls/CaseNavigation.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        var actionType;
     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div style="width: 100%">
        <div class="pageTitle" style="float: left; width: 15%">
            DOCUMENT</div>
        <div style="float: right; width: 85%; padding-bottom: 5px;" id="divDocumentButtons"
            runat="server" visible="false">
              <div style="float: left;">
                 <UC:CaseNavigation ID="caseNavigation" runat="server" IncludeEmptyItem="true" />
            </div>
            <div style="float: left;">
            &nbsp;
                <%--  <input id="btnCreateOrder" runat="server" type="button" value="Create Order" class="submitMedium"  
            style="width: 130px" />--%>&nbsp;
                <asp:Button ID="btnDelete" runat="server" type="button" Visible="false" class="submitMediumRed"
                    Style="width: 100px" OnCommand="btnDelete_Command" OnClientClick="return confirm('Are you sure you want to delete this document?');"
                    Text="Delete Document" />
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" CssClass="validationSummary" ValidationGroup="SaveDocument" />
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <telerik:RadWindow ID="winAssociate" runat="server" Skin="Office2007" Modal="true"
        ShowContentDuringLoad="false" ReloadOnShow="true" Behaviors="Close" VisibleStatusbar="false"
        Width="410" Height="200" />
    <div id="divMain" runat="server" class="newCase">
        <div id="divMainSub" runat="server" class="newCaseSub" style="width: 873px;">
            <UC:ActionInfo ID="actionInfo" runat="server" />
            <CSCMSUC:SectionContainer ID="sectionDocument" runat="server" HeaderText="DOCUMENT INFORMATION"
                Width="100%">
                <asp:Panel ID="pnlDocument" runat="server" DefaultButton="btnSaveDocument">
                    <table align="center">
                        <tr>
                            <td valign="top">
                                <table cellspacing="3" align="center">
                                    <tr>
                                        <td class="fieldLabelReq">
                                            Document Date:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="dtDocumentDate" runat="server" />
                                            <asp:RequiredFieldValidator ID="rfvDocumentDate" runat="server" ControlToValidate="dtDocumentDate" ValidationGroup="SaveDocument"
                                                Display="Dynamic" ErrorMessage="Please select a Document Date">*</asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="rvDocumentDate" runat="server" ControlToValidate="dtDocumentDate" ValidationGroup="SaveDocument"
                                                MinimumValue="1/1/1900" Type="Date" ErrorMessage="Document Date must not be in the future">*</asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabelReq">
                                            Document Type:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDocumentType" runat="server" Width="202" DataSourceID="dsDocumentType"
                                                DataValueField="DocumentTypeID" DataTextField="DocumentTypeName" AppendDataBoundItems="true">
                                                <asp:ListItem Value="">- Select -</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="dsDocumentType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                                        SelectCommand="select DocumentTypeID, DocumentTypeName from DocumentType">
                                            </asp:SqlDataSource>
                                            <asp:RequiredFieldValidator ID="rfvDocumentType" runat="server" ControlToValidate="ddlDocumentType" ValidationGroup="SaveDocument"
                                                Display="Dynamic" ErrorMessage="Please select a Document Type">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trFileUpload" runat="server">
                                        <td id="tdFileUpload" runat="server" class="fieldLabelReq">
                                            Document To Upload:
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="fileUpload" runat="server" Width="372" Height="19" Style="vertical-align: middle;" />
                                            <asp:RequiredFieldValidator ID="rfvFileUpload" runat="server" ControlToValidate="fileUpload" ValidationGroup="SaveDocument"
                                                Display="Dynamic" ErrorMessage="Please select a Document to Upload">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revFileUpload" runat="server" Display="Dynamic" ValidationGroup="SaveDocument"
                                                ErrorMessage="Document is not an acceptable file type to upload (wpd, doc, docx, pdf, ppt, txt, xls, xlsx, gif, jpg, png, tif)"
                                                ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.wpd|.WPD|.doc|.DOC|.docx|.DOCX|.pdf|.PDF|.ppt|.PPT|.txt|.TXT|.xls|.XLS|.xlsx|.XSLX|.gif|.GIF|.jpg|.JPG|.jpeg|.JPEG|.png|.PNG|.tif|.TIF)$"
                                                ControlToValidate="fileUpload">!</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            Document Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDocumentName" runat="server" Width="300" MaxLength="100" />
                                            (leave blank to use the uploaded document's file name)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fieldLabel">
                                            Document Note:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDocumentNote" runat="server" TextMode="MultiLine" Width="438"
                                                Height="40" />
                                        </td>
                                    </tr>
                                    <tr id="trDownload" runat="server" visible="false">
                                        <td class="fieldLabel">
                                            Download Document:
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="btnDownload" runat="server" CausesValidation="false" OnClick="btnDownload_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="padding-top: 10px; padding-bottom: 5px;">
                                            <asp:Button ID="btnSaveAndReturnToCase" runat="server" Visible="true" Text="Save &amp; Return to Case"
                                                CssClass="submit" OnClick="btnSaveAndReturnToCase_Click"  ValidationGroup="SaveDocument"/>
                                            <asp:Button ID="btnSaveDocument" runat="server" Text="Save Document" CssClass="submit" ValidationGroup="SaveDocument"
                                                OnClick="btnSave_Click" />
                                            &nbsp;&nbsp;
                                            <asp:Button ID="btnSaveDocumentAndAdd" runat="server" Visible="false" Text="Save &amp; Add Another"
                                                CssClass="submit" OnClick="btnSaveAndAdd_Click" ValidationGroup="SaveDocument" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
        </div>
    </div>
    <div class="bottomSection" id="divCaseNotes" runat="server" visible="false">
        <UC:CaseNotes ID="caseNotes" runat="server" />
    </div>
</asp:Content>
