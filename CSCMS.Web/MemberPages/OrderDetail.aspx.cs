﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using System.Data.Linq;
using System.IO;

public partial class MemberPages_OrderDetail : ActionBasePage
{

    #region [Properties]

    protected bool NewSpecialOrder
    {
        get { return Convert.ToBoolean(ViewState["NewSpecialOrder"]); }
        set { ViewState["NewSpecialOrder"] = value; }
    }
    #endregion


    public string SpecialOrderTypeIDString
    {
        get
        {
            if (this.CurrentSpecialOrder != null && this.CurrentSpecialOrder.SpecialOrderType != null)
                return this.CurrentSpecialOrder.SpecialOrderType.SpecialOrderTypeID.ToString();
            else
                return "";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        actionInfo.CauseGUID = new Guid(Request.QueryString["cid"]);
        actionInfo.ActionNumber = short.Parse(Request.QueryString["a"]);
        ddlOrderType.DataBind();
        if (Page.Request.QueryString["PatOrder"] != null)
        {
            trBondAmount.Visible = false;
            trReleaseDate.Visible = false;
            trDaysIncarcerated.Visible = false;
            trCapIssuedDate.Visible = false;
            trArrestDate.Visible = false;
            lblNote.Text = "Test Note";
            ddlOrderType.SelectedIndex = 1;
        }
        else
        {
            trPatEstDate.Visible = false;
            trPatResultDate.Visible = false;
            trPatTestResult.Visible = false;
            trIsExculded.Visible = false;
        }
        base.OnInit(e);
    }

    protected List<CSCMS.Data.SpecialOrderType> GetOrderTypes()
    {
        using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
        {
            CscmsContext.SpecialOrderType.MergeOption = System.Data.Objects.MergeOption.NoTracking;
            List<CSCMS.Data.SpecialOrderType> list = null;
            if (Page.Request.QueryString["PatOrder"] != null)
                list = CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeName == MyConsts.DB_PATERNITY_TEST).ToList();
            else
                list = CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeName != MyConsts.DB_PATERNITY_TEST).OrderBy(it => it.SortOrder).ToList();

            return list;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (!Page.IsPostBack)
            {
                rvdtOrderedDate.MaximumValue = DateTime.Now.ToShortDateString();
                rvdtIssuedDate.MaximumValue = DateTime.Now.ToShortDateString();
                rvdtPatEstDate.MaximumValue = DateTime.Now.ToShortDateString();
                rvdtPatResultDate.MaximumValue = DateTime.Now.ToShortDateString();
                LoadRequestedBy();
                LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));

                if (!string.IsNullOrEmpty(Request.QueryString["did"]))
                {
                    this.NewSpecialOrder = false;
                    LoadSpecialOrder(int.Parse(Request.QueryString["did"]));
                }
                else
                {
                    this.NewSpecialOrder = true;
                    InitializeSpecialOrder(Guid.Empty);
                }

                PopulateForm();

                if (!this.NewSpecialOrder)
                {
                    btnDelete.Visible = true;
                }

            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    private bool VerifyAddNewPaternityTestOrder()
    {
        if (Page.Request.QueryString["PatOrder"] != null && ddlRequestedBy.SelectedIndex > 0)
        {
            Guid pid = new Guid(ddlRequestedBy.SelectedValue);
            CSCMS.Data.SpecialOrder so = CscmsContext.SpecialOrder.Where(it => it.ActionPerson.ActionGUID == this.CurrentAction.ActionGUID && it.ActionPerson.Person.PersonGUID == pid && it.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_PATERNITY_TEST).FirstOrDefault();

            if (this.NewSpecialOrder && so != null)
                return false;
        }
        return true;
    }

    private bool UpdateSpecialOrder()
    {
        if (this.NewSpecialOrder)
        {
            Guid pid = Guid.Empty;
            if (ddlRequestedBy.SelectedIndex > 0)
                pid = new Guid(ddlRequestedBy.SelectedValue);

            InitializeSpecialOrder(pid);
            this.CurrentSpecialOrder.CreateDate = DateTime.Now;
            this.CurrentSpecialOrder.LastUpdateDate = DateTime.Now;
            CscmsContext.AddToSpecialOrder(this.CurrentSpecialOrder);
        }
        else
        {
            if (this.CurrentSpecialOrder.EntityState == EntityState.Detached)
                CscmsContext.Attach(this.CurrentSpecialOrder);
        }

        short? orderTypeId = !string.IsNullOrEmpty(ddlOrderType.SelectedValue) ? short.Parse(ddlOrderType.SelectedValue) : (short?)null;
        if (orderTypeId != null)
            this.CurrentSpecialOrder.SpecialOrderType = this.CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeID == orderTypeId).FirstOrDefault();
        else
            this.CurrentSpecialOrder.SpecialOrderType = null;

        if (this.ddlOrderType.SelectedIndex > 0)
        {
            if (ddlOrderType.SelectedItem.Text.Contains("Capias"))
            {
                this.CurrentSpecialOrder.CommitmentOrCapias = "1";
            }
            else if (ddlOrderType.SelectedItem.Text == "Commitment")
            {
                this.CurrentSpecialOrder.CommitmentOrCapias = "0";
            }
            else
            {
                this.CurrentSpecialOrder.CommitmentOrCapias = "2"; //other
            }
        }

        this.CurrentSpecialOrder.OrderedDate = dtOrderedDate.SelectedDate.Value;
        this.CurrentSpecialOrder.IssuedDate = dtIssuedDate.SelectedDate;

        if (ddlRequestedBy.SelectedIndex > 0)
        {
            Guid pid = new Guid(ddlRequestedBy.SelectedValue);
            this.CurrentSpecialOrder.ActionPerson = CscmsContext.ActionPerson.Include("Action").Include("Person").Where(it => it.Person.PersonGUID == pid && it.Action.ActionGUID == this.CurrentAction.ActionGUID).FirstOrDefault();
        }
        else
        {
            this.CurrentSpecialOrder.ActionPerson = null;
        }

        this.CurrentSpecialOrder.SpecialOrderNote = txtSpecialOrderNote.Text.Trim();

        this.CurrentSpecialOrder.ArrestCommitmentEffectiveDate = dtArrestCommitmentEffectiveDate.SelectedDate;

        this.CurrentSpecialOrder.ReleaseDate = dtReleaseDate.SelectedDate;

        this.lblDaysIncarcerated.Text = "";
        if (this.CurrentSpecialOrder.ArrestCommitmentEffectiveDate != null)
        {
            DateTime dt = DateTime.Now;
            if (this.CurrentSpecialOrder.ReleaseDate != null)
                dt = this.CurrentSpecialOrder.ReleaseDate.Value;

            TimeSpan ts = (DateTime)dt - (DateTime)this.CurrentSpecialOrder.ArrestCommitmentEffectiveDate;
            this.lblDaysIncarcerated.Text = (ts.Days + 1).ToString();
        }

        this.CurrentSpecialOrder.BondAmount = this.RadNumericTextBoxBondAmount.Value == null ? 0 : (decimal)this.RadNumericTextBoxBondAmount.Value;

        CscmsContext.SaveChanges();

        if (this.Page.Request.QueryString["PatOrder"] != null)
        {
            Helper.AddorUpdatePaternityTest(CscmsContext,
                new PaternityTestData
                {
                    ActionGuid = this.CurrentSpecialOrder.ActionPerson.Action.ActionGUID,
                    PersonGuid = this.CurrentSpecialOrder.ActionPerson.Person.PersonGUID,
                    Note = txtSpecialOrderNote.Text.Trim(),
                    OrderedDate = this.dtOrderedDate.SelectedDate,
                    PaternityestablishedDate = this.dtPatEstDate.SelectedDate,
                    ResultDate = this.dtPatResultDate.SelectedDate,
                    TestPositive = this.chkPatTestResult.Checked,
                    isExcluded = this.chkExcluded.Checked
                }, true, false);
        }

        return true;
    }
    private void InitializeSpecialOrder(Guid perGuid)
    {
        CSCMS.Data.SpecialOrder SpecialOrder = new CSCMS.Data.SpecialOrder();
        SpecialOrder.CreateDate = DateTime.Now;
        SpecialOrder.LastUpdateDate = DateTime.Now;
        SpecialOrder.ActionPerson = CscmsContext.ActionPerson.Include("Action").Include("Person").Where(it => it.Action.ActionGUID == this.CurrentAction.ActionGUID && it.Person.PersonGUID == perGuid).FirstOrDefault();
        this.CurrentSpecialOrder = SpecialOrder;
    }

    protected void btnSaveAndReturnToCase_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (VerifyAddNewPaternityTestOrder())
            {
                if (UpdateSpecialOrder())
                {
                    Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=SaveSpecialOrder", true);
                }
            }
            else
            {
                messageBox.ErrorMessage = "Paternity Test Order already exists for the person";
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (VerifyAddNewPaternityTestOrder())
            {
                if (UpdateSpecialOrder())
                {
                    this.NewSpecialOrder = false;
                    LoadSpecialOrder(this.CurrentSpecialOrder.SpecialOrderID);
                    PopulateForm();
                    messageBox.SuccessMessage = "SpecialOrder saved successfully.";
                }
            }
            else
            {
                messageBox.ErrorMessage = "Paternity Test Order already exists for the person";
            }
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    private void PopulateForm()
    {
        ddlRequestedBy.SelectedValue = this.CurrentSpecialOrder.ActionPerson != null ? this.CurrentSpecialOrder.ActionPerson.Person.PersonGUID.ToString() : "";

        dtOrderedDate.MaxDate = DateTime.Today;
        dtIssuedDate.MaxDate = DateTime.Today;

        dtOrderedDate.SelectedDate = this.CurrentSpecialOrder.OrderedDate != DateTime.MinValue ? this.CurrentSpecialOrder.OrderedDate : (DateTime?)null;
        dtIssuedDate.SelectedDate = this.CurrentSpecialOrder.IssuedDate != DateTime.MinValue ? this.CurrentSpecialOrder.IssuedDate : (DateTime?)null;

        if (!NewSpecialOrder)
        {
            txtSpecialOrderNote.Text = this.CurrentSpecialOrder.SpecialOrderNote;
            dtArrestCommitmentEffectiveDate.SelectedDate = this.CurrentSpecialOrder.ArrestCommitmentEffectiveDate != DateTime.MinValue ? this.CurrentSpecialOrder.ArrestCommitmentEffectiveDate : (DateTime?)null;
            dtReleaseDate.SelectedDate = this.CurrentSpecialOrder.ReleaseDate != DateTime.MinValue ? this.CurrentSpecialOrder.ReleaseDate : (DateTime?)null;
            RadNumericTextBoxBondAmount.Value = this.CurrentSpecialOrder.BondAmount != null ? (double)this.CurrentSpecialOrder.BondAmount : 0;

            if (this.CurrentSpecialOrder.ArrestCommitmentEffectiveDate != null)
            {
                DateTime dt = DateTime.Now;
                if (this.CurrentSpecialOrder.ReleaseDate != null)
                    dt = this.CurrentSpecialOrder.ReleaseDate.Value;

                TimeSpan ts = (DateTime)dt - (DateTime)this.CurrentSpecialOrder.ArrestCommitmentEffectiveDate;
                this.lblDaysIncarcerated.Text = (ts.Days + 1).ToString();
            }

            if (Page.Request.QueryString["PatOrder"] != null)
            {
                CSCMS.Data.PaternityTest pt = CscmsContext.PaternityTest.Where(it => it.ActionPerson.Action.ActionGUID == this.CurrentSpecialOrder.ActionPerson.ActionGUID && it.ActionPerson.Person.PersonGUID == this.CurrentSpecialOrder.ActionPerson.Person.PersonGUID).FirstOrDefault();
                if (pt != null)
                {
                    this.dtPatEstDate.SelectedDate = pt.PaternityEstablishedDate;
                    this.dtPatResultDate.SelectedDate = pt.ResultDate;
                    this.chkPatTestResult.Checked = pt.TestPositive.HasValue ? (bool)pt.TestPositive : false;
                    this.chkExcluded.Checked = pt.IsExcluded.HasValue ? (bool)pt.IsExcluded : false;
                }
            }
        }

        SetFieldVisibility();
    }

    private void SetFieldVisibility()
    {
        if (!NewSpecialOrder)
        {
            btnDelete.Visible = true;
            divSpecialOrderButtons.Visible = true;
            //sectionActions.Visible = true;
            divMain.Attributes["class"] = "existingCase";
            divMainSub.Attributes["class"] = "existingCaseSub";

        }

        if (Request.QueryString["return"] == "c")
        {
            btnDelete.Visible = false;
            divSpecialOrderButtons.Visible = false;
            btnSaveSpecialOrder.Visible = false;

        }
    }

    private void LoadSpecialOrder(int SpecialOrderID)
    {
        this.CurrentSpecialOrder = CscmsContext.SpecialOrder.Include("ActionPerson.Action").Include("ActionPerson.Person").Include("SpecialOrderType").Where(it => it.SpecialOrderID == SpecialOrderID).FirstOrDefault();
        if (this.CurrentSpecialOrder.ActionPerson.Action.ActionGUID != this.CurrentAction.ActionGUID)
        {
            CurrentSpecialOrder = null;
            Response.Redirect("~/AnonPages/Unauthorized.aspx", true);
        }

        divCaseNotes.Visible = true;
    }


    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (this.Page.Request.QueryString["PatOrder"] != null)
            {
                Helper.DeletePaternityTest(CscmsContext, this.CurrentSpecialOrder.ActionPerson.Action.ActionGUID, this.CurrentSpecialOrder.ActionPerson.Person.PersonGUID, true, false);
            }

            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(SqlConn,
                CommandType.StoredProcedure,
                "DeleteActionSpecialOrder",
                new SqlParameter("@ActionGUID", this.CurrentSpecialOrder.ActionPerson.Action.ActionGUID.ToString()),
                new SqlParameter("@SpecialOrderID", this.CurrentSpecialOrder.SpecialOrderID));
            this.CurrentSpecialOrder = null;
            Response.Redirect("~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((((BasePage)this.Page).CurrentAction.ActionNumber)).ToString() + "&message=DeleteSpecialOrder", true);
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    protected void LoadRequestedBy()
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
       CommandType.StoredProcedure,
       "GetActionPersons",
       new SqlParameter("@ActionGUID", this.CurrentAction.ActionGUID));

        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                if ((string)r["PersonTypeName"] != "Child" && (string)r["PersonTypeName"] != "Attorney")
                {
                    ListItem item = new ListItem(string.Format("{0} - {1}",
                        Helper.GetFormattedName(Helper.EvaluateDbStringValue(r["FirstName"]), Helper.EvaluateDbStringValue(r["MiddleName"]), Helper.EvaluateDbStringValue(r["LastName"]), Helper.EvaluateDbStringValue(r["NameSuffix"])),
                        Helper.EvaluateDbStringValue(r["PersonTypeName"])), string.Format("{0}", r["PersonGUID"]));

                    if (Page.Request.QueryString["PatOrder"] == null)
                        ddlRequestedBy.Items.Add(item);
                    else if ((string)r["PersonTypeName"] == "Father")
                        ddlRequestedBy.Items.Add(item);
                }
            }
        }
    }
}