﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="DispositionType.aspx.cs" Inherits="AdminPages_DispositionType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
<div class="pageTitle">
        Manage DispositionType
    </div>
    <cscmsuc:messagebox id="messageBox" runat="server" />
    <div>
        <telerik:RadGrid id="grdDispType" runat="server" AllowPaging="True" PageSize="80"
            AllowSorting="True" DataSourceID="dsDispType"  OnItemCommand="grdDispType_OnItemCommand" >
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no document types" DataKeyNames="DispositionTypeID" AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                   <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this disposition type?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                    <telerik:GridBoundColumn HeaderText="DispositionTypeName" DataField="DispositionTypeName" UniqueName="DispositionTypeName"  />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder"  />
                    <telerik:GridCheckBoxColumn HeaderText="IsActive" DataField="IsActive" UniqueName="IsActive" />                  
                </Columns>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsDispType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT DispositionTypeID, DispositionTypeName, SortOrder, IsActive from DispositionType">
        </asp:SqlDataSource>
    </div>
</asp:Content>

