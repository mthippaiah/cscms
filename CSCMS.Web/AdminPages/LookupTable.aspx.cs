﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Telerik.Web.UI;


public partial class AdminPages_LookupTable : AdminBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grdBroadcastMessages_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        grd2_ItemCreated(sender, e, this.grdBroadcastMessages.MasterTableView.DataKeyNames[0].ToString(), "Message", "", "", false);
    }

    protected void btnAddCourtCounties_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(ddlCoordinatorJudge.SelectedValue))
        //{
        //    if (grdOCACourt.SelectedValue != null)
        //    {
        //        string j = ddlCoordinatorJudge.SelectedValue.ToString();
        //        string i = grdOCACourt.SelectedValue.ToString();
        //        string sqlString = "INSERT INTO UserCourtCounty (UserID, OCACourtID, CountyID, CreateDate, LastUpdateDate ) SELECT " + j + ", " + i + ", CountyID, GetDate(), GetDate() FROM OCACourtCounty WHERE OCACourtID = " + i;
        //        dc.ExecuteCommand(sqlString);
        //        grdUserCourtCountyCurrent.DataBind();
        //    }

        //}
    }

    protected void grdOCACourt_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        grd2_ItemCreated(sender, e, grdOCACourt.MasterTableView.DataKeyNames[0].ToString(), "CourtName", "", "", true);
    }
    protected void grdOCACourtCounty_2_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        grd2_ItemCreated(sender, e, grdOCACourtCounty_2.MasterTableView.DataKeyNames[0].ToString(), "", "", "", false);
    }
    protected void btnRemoveCourtCounties_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(ddlCoordinatorJudge.SelectedValue))
        //{
        //    string sqlString = "DELETE UserCourtCounty WHERE UserID = " + ddlCoordinatorJudge.SelectedValue.ToString();
        //    dc.ExecuteCommand(sqlString);
        //    grdUserCourtCountyCurrent.DataBind();
        //}
    }
    protected void grd1_ItemDeleted(object source, GridDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            ShowErrorMessage(e.Exception.Message);
        }
    }

    protected void grd1_ItemUpdated(object source, GridUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            ShowErrorMessage(e.Exception.Message);
        }
    }
    protected void grd1_ItemInserted(object source, GridInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            e.ExceptionHandled = true;
            ShowErrorMessage(e.Exception.Message);
        }
    }

    protected void grd2_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e, string primaryKeyName, string requireField1, string requireField2, string numericRequireField, bool sortOrder)
    {
        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            GridEditableItem item = e.Item as GridEditableItem;
            GridEditManager manager = item.EditManager;
            GridTextBoxColumnEditor editor = manager.GetColumnEditor(primaryKeyName) as GridTextBoxColumnEditor;
            if (editor != null)
            {
                editor.TextBoxControl.Enabled = false;
                if (e.Item.OwnerTableView.IsItemInserted) editor.TextBoxControl.Text = "0";
            }

            if (ddlLookUpTable.SelectedValue == "OCACourtCounty")
            {
                if (!e.Item.OwnerTableView.IsItemInserted)
                {
                    if ((e.Item as GridEditableItem)["OCACourtID"] != null) (e.Item as GridEditableItem)["OCACourtID"].Enabled = false;
                    if ((e.Item as GridEditableItem)["CountyID"] != null) (e.Item as GridEditableItem)["CountyID"].Enabled = false;
                    //if (((RadGrid)(sender)).DataSourceID == "dsUserCourtCounty_2")
                    //{
                    //    (e.Item as GridEditableItem)["UserID"].Enabled = false;
                    //}
                }
            }
            if (ddlLookUpTable.SelectedValue == "OCACourt")
            {
                if (!e.Item.OwnerTableView.IsItemInserted)
                {
                    if (((RadGrid)(sender)).DataSourceID == "dsOCACourtCounty_2")
                    {
                        (e.Item as GridEditableItem)["CountyID"].Enabled = false;
                    }
                }
            }

            if (!string.IsNullOrEmpty(requireField1))
            {
                //GridTextBoxColumnEditor requireField = (GridTextBoxColumnEditor)item.EditManager.GetColumnEditor(requireField1);
                //if (requireField != null)
                //{
                //    TableCell cell = (TableCell)requireField.TextBoxControl.Parent;
                //    RequiredFieldValidator validator = new RequiredFieldValidator();
                //    requireField.TextBoxControl.ID = "ID_for_validation1";
                //    validator.ControlToValidate = requireField.TextBoxControl.ID;
                //    validator.ErrorMessage = "*";
                //    cell.Controls.Add(validator);
                //    if (requireField1 == "Message")
                //    {
                //        requireField.TextBoxControl.TextMode = TextBoxMode.MultiLine;
                //        requireField.TextBoxControl.Width = 600;
                //        requireField.TextBoxControl.Height = 100;
                //    }
                //    else
                //    {
                //        requireField.TextBoxControl.Width = 200;
                //    }

                //}
            }
            if (!string.IsNullOrEmpty(requireField2))
            {
                GridTextBoxColumnEditor requireField = (GridTextBoxColumnEditor)item.EditManager.GetColumnEditor(requireField2);
                if (requireField != null)
                {
                    TableCell cell = (TableCell)requireField.TextBoxControl.Parent;
                    RequiredFieldValidator validator = new RequiredFieldValidator();
                    requireField.TextBoxControl.ID = "ID_for_validation2";
                    validator.ControlToValidate = requireField.TextBoxControl.ID;
                    validator.ErrorMessage = "*";
                    cell.Controls.Add(validator);
                }
            }
            if (!string.IsNullOrEmpty(numericRequireField))
            {
                GridNumericColumnEditor requireField = (GridNumericColumnEditor)item.EditManager.GetColumnEditor(numericRequireField);
                if (requireField != null)
                {
                    TableCell cell = (TableCell)requireField.NumericTextBox.Parent;
                    RequiredFieldValidator validator = new RequiredFieldValidator();
                    requireField.NumericTextBox.ID = "ID_for_validation3";
                    validator.ControlToValidate = requireField.NumericTextBox.ID;
                    validator.ErrorMessage = "*";
                    cell.Controls.Add(validator);
                }
            }
            if (sortOrder)
            {
                GridNumericColumnEditor requireField = (GridNumericColumnEditor)item.EditManager.GetColumnEditor("SortOrder");
                if (requireField != null)
                {
                    ////requireField.NumericTextBox.Height = 10;
                    ////requireField.NumericTextBox.Font.Size = 8;
                    TableCell cell = (TableCell)requireField.NumericTextBox.Parent;
                    RequiredFieldValidator validator = new RequiredFieldValidator();
                    requireField.NumericTextBox.ID = "ID_for_validation4";
                    validator.ControlToValidate = requireField.NumericTextBox.ID;
                    validator.ErrorMessage = "*";
                    cell.Controls.Add(validator);
                }
            }

        }
    }

    private void ShowErrorMessage(string errorMessage)
    {
        errorMessage = "<br>Please enter valid data:<br> " + errorMessage;

        //if (ddlLookUpTable.SelectedValue == "CaseOpenedReason") grdCaseOpenedReason.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "CaseReasonType") grdCaseReasonType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        if (ddlLookUpTable.SelectedValue == "BroadcastMessages") grdBroadcastMessages.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "CaseType") grdCaseType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "ConservatorshipStatusType") grdConservatorshipStatusType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "ContinuanceType") grdContinuanceType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "County") grdCounty.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "County") grdCourt_County.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "Court") grdCourt.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "DischargeReasonType") grdDischargeReasonType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "DocumentType") grdDocumentType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "EthnicityType") grdEthnicityType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "HearingType") grdHearingType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "ICPCStatusType") grdICPCStatusType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "LevelOfCareType") grdLevelOfCareType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "MedicationCategory") grdMedicationCategory.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "MedicationType") grdMedicationType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "MotionType") grdMotionType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "NoteType") grdNoteType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "OCACourt") this.grdOCACourt.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "OCACourt") this.grdOCACourtCounty_2.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "OCACourtCounty") this.grdOCACourtCounty.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "OCACourtCounty") this.grdUserCourtCounty_OCACourtCounty.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "OrderCategoryType") this.grdOrderCategoryType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "OrderCategoryType") this.grdOrderType_OrderCategory.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "OrderFilterType") this.grdOrderFilterType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "OrderType") this.grdOrderType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "PermanencyPlanType") this.grdPermanencyPlanType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "PersonRelationshipType") this.grdPersonRelationshipType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "PersonType") this.grdPersonType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "PhoneType") this.grdPhoneType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "PlacementChangeType") this.grdPlacementChangeType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "PlacementType") this.grdPlacementType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage))); if (ddlLookUpTable.SelectedValue == "PlacementType") this.grdPlacementType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "RaceType") this.grdRaceType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "ServiceItemType") this.grdServiceItemType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "ServiceMethodType") this.grdServiceMethodType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "SettlementAgreementType") this.grdSettlementAgreementType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "SettlementType") this.grdSettlementType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "State") this.grdState.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "SubstanceAbuseTestType") this.grdSubstanceAbuseTestType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "SubstanceAbuseType") this.grdSubstanceAbuseType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "TicklerType") this.grdTicklerType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "UserType") grdUserType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "UserCourtCounty") grdUserCourtCounty.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "UserCourtCounty") grdUserCourtCounty_OCACourtCounty.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));

        //if (ddlLookUpTable.SelectedValue == "User") grdUsers.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "User") grdUsers.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
        //if (ddlLookUpTable.SelectedValue == "RelationshipConfigurationType") grdRelationshipConfigurationType.Controls.Add(new LiteralControl(string.Format("<span style='color:red'>{0}</span>", errorMessage)));
    }

    protected void ddlLookUpTable_SelectedIndexChanged(object sender, EventArgs e)
    {
        //sectionCaseOpenedReason.Visible = false;
        //sectionCaseInitiationReason.Visible = false;
        //sectionCaseType.Visible = false;
        //sectionConservatorshipStatus.Visible = false;
        //sectionContinuanceType.Visible = false;
        sectionCounty.Visible = false;
        //sectionCourt.Visible = false;
        //sectionDischargeReasonType.Visible = false;
        //sectionDocumentType.Visible = false;
        //sectionEthnicityType.Visible = false;
        //sectionHearingType.Visible = false;
        //sectionICPCStatusType.Visible = false;
        //sectionLevelOfCareType.Visible = false;
        //sectionMedicationCategory.Visible = false;
        //sectionMedicationType.Visible = false;
        //sectionMotionType.Visible = false;
        //sectionNoteType.Visible = false;
        sectionOCACourt.Visible = false;
        //sectionOCACourtCounty.Visible = false;
        //sectionOrderCategoryType.Visible = false;
        //sectionOrderFilterType.Visible = false;
        //sectionOrderType.Visible = false;
        //sectionPermanencyPlanType.Visible = false;
        //sectionPersonRelationshipType.Visible = false;
        //sectionPersonType.Visible = false;
        //sectionPhoneType.Visible = false;
        //sectionPlacementChangeType.Visible = false;
        //sectionPlacementType.Visible = false;
        //sectionRaceType.Visible = false;

        //sectionUserType.Visible = false;
        //sectionUserCourtCounty.Visible = false;
        //sectionUsers.Visible = false;
        //SectionRelationshipConfigurationType.Visible = false;

        //if (ddlLookUpTable.SelectedValue == "CaseOpenedReason") sectionCaseOpenedReason.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "CaseReasonType") sectionCaseInitiationReason.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "CaseType") sectionCaseType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "ConservatorshipStatusType") sectionConservatorshipStatus.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "ContinuanceType") sectionContinuanceType.Visible = true;
        if (ddlLookUpTable.SelectedValue == "County") sectionCounty.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "Court") sectionCourt.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "DischargeReasonType") sectionDischargeReasonType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "DocumentType") sectionDocumentType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "EthnicityType") sectionEthnicityType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "HearingType") sectionHearingType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "ICPCStatusType") sectionICPCStatusType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "LevelOfCareType") sectionLevelOfCareType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "MedicationCategory") sectionMedicationCategory.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "MedicationType") sectionMedicationType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "MotionType") sectionMotionType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "NoteType") sectionNoteType.Visible = true;
        if (ddlLookUpTable.SelectedValue == "OCACourt") sectionOCACourt.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "OCACourtCounty") sectionOCACourtCounty.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "OrderCategoryType") sectionOrderCategoryType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "OrderFilterType") sectionOrderFilterType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "OrderType") sectionOrderType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "PermanencyPlanType") sectionPermanencyPlanType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "PersonRelationshipType") sectionPersonRelationshipType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "PersonType") sectionPersonType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "PhoneType") sectionPhoneType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "PlacementChangeType") sectionPlacementChangeType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "PlacementType") sectionPlacementType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "RaceType") sectionRaceType.Visible = true;
        //sectionServiceItemType.Visible = false;
        //if (ddlLookUpTable.SelectedValue == "ServiceItemType") sectionServiceItemType.Visible = true;
        //sectionServiceMethodType.Visible = false;
        //if (ddlLookUpTable.SelectedValue == "ServiceMethodType") sectionServiceMethodType.Visible = true;
        //sectionSettlementAgreementType.Visible = false;
        //if (ddlLookUpTable.SelectedValue == "SettlementAgreementType") sectionSettlementAgreementType.Visible = true;
        //sectionSettlementType.Visible = false;
        //if (ddlLookUpTable.SelectedValue == "SettlementType") sectionSettlementType.Visible = true;
        sectionState.Visible = false;
        if (ddlLookUpTable.SelectedValue == "State") sectionState.Visible = true;
        //sectionSubstanceAbuseTestType.Visible = false;
        //if (ddlLookUpTable.SelectedValue == "SubstanceAbuseTestType") sectionSubstanceAbuseTestType.Visible = true;
        sectionTicklerType.Visible = false;
        if (ddlLookUpTable.SelectedValue == "TicklerType") sectionTicklerType.Visible = true;
        sectionBroadcastMessages.Visible = false;
        if (ddlLookUpTable.SelectedValue == "BroadcastMessages") sectionBroadcastMessages.Visible = true;

        //sectionSubstanceAbuseType.Visible = false;
        //if (ddlLookUpTable.SelectedValue == "SubstanceAbuseType") sectionSubstanceAbuseType.Visible = true;

        //if (ddlLookUpTable.SelectedValue == "UserType") sectionUserType.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "UserCourtCounty") sectionUserCourtCounty.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "User") sectionUsers.Visible = true;
        //if (ddlLookUpTable.SelectedValue == "RelationshipConfigurationType") SectionRelationshipConfigurationType.Visible = true;

    }
    #region grdCounty
    protected void grdCounty_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        grd2_ItemCreated(sender, e, grdCounty.MasterTableView.DataKeyNames[0].ToString(), "CountyName", "", "", true);
    }
    #endregion
    #region grdState
    protected void grdState_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        grd2_ItemCreated(sender, e, grdState.MasterTableView.DataKeyNames[0].ToString(), "StateName", "", "", true);
    }
    #endregion
    #region grdTicklerType
    protected void grdTicklerType_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        grd2_ItemCreated(sender, e, grdTicklerType.MasterTableView.DataKeyNames[0].ToString(), "TicklerTypeName", "", "DelayDays", false);
    }
    #endregion
}