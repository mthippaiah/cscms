﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="DocumentType.aspx.cs" Inherits="AdminPages_DocumentType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
    <div class="pageTitle">
        Manage DocumentType
    </div>
    <cscmsuc:messagebox id="messageBox" runat="server" />
    <div>
        <telerik:RadGrid id="grdDocumentType" runat="server" AllowPaging="True" PageSize="80"
            AllowSorting="True" DataSourceID="dsDocType"  OnItemCommand="grdDocumentType_OnItemCommand" >
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no document types" DataKeyNames="DocumentTypeID" AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                   <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this document type?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                    <telerik:GridBoundColumn HeaderText="DocumentTypeName" DataField="DocumentTypeName" UniqueName="DocumentTypeName"  />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder"  />
                </Columns>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsDocType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT DocumentTypeID, DocumentTypeName, SortOrder from DocumentType">
        </asp:SqlDataSource>
    </div>
</asp:Content>

