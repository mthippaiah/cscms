﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_ManageAttorney : BasePage
{
    List<Guid> failedToMerge = new List<Guid>();
    List<Guid> replaceList = new List<Guid>();
    List<Guid> replaceWithList = new List<Guid>();
    List<Guid> deleteList = new List<Guid>();

    const string MSG_UNASSIGNED_DELETE = "Please select unassigned items (Gray color) to delete";
    const string MSG_CANNOTBE_DELETED = "The non-gray colored items you have seleted are not unassigned attorneys. They cannot be deleted";
    const string MSG_DELETED = "Sucessfully deleted the unassigned attorneys";
    const string MERGE_FAIL_2PERSON_SAMECASE = "Failed!. Unable to merge persons. These two people are associated with the same case(s). Please remove one of the persons from each case before merging. (Cases listed below)<br/><br/>";
    const string ERROR_2PERSON_SAMECASE = "Unable to merge persons. There may be 2 persons on the same action. Please run Validate to see the cases";
    const string MERGE_FAIL_TECHDETAILS = "Unable to merge persons. Technical details: ";
    const string ERROR_REPLACE = "You cannot select Replace when With is selected in the same row.";
    const string ERROR_INVALID_WITH = "Invalid number of items selected for replacing with. Select only one With";
    const string ATTORNEY_NOTSELECTED = "No attorneys selected for replacing.";
    const string VALIDATION_SUCCESS = "Validation success";
    const string MERGE_SUCCESS = "Merge success";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.ddlCourtList.DataSource = CscmsContext.OCACourt.ToList();
        this.ddlCourtList.DataBind();
        this.ddlCourtList.SelectedIndex = 0;
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private List<AttorneyData> GetAttorneyData()
    {
        int id = int.Parse(this.ddlCourtList.SelectedValue);
        List<short> listCountyIds = CscmsContext.OCACourtCounty.Where(it => it.OCACourtID == id && (it.ExpirationDate == null || it.ExpirationDate > DateTime.Now)).Select(it => it.CountyID).ToList();
        var query = (from p in CscmsContext.Person
                     join atc in CscmsContext.AttorneyCounty on p.PersonGUID equals atc.PersonGUID
                     join ccy in CscmsContext.OCACourtCounty on atc.County.CountyID equals ccy.County.CountyID
                     let _ap = (from ap in CscmsContext.ActionPerson
                                where ap.Person.PersonGUID == p.PersonGUID
                                select ap).FirstOrDefault()
                     where ccy.OCACourt.OCACourtID == id
                     select new AttorneyData
                     {
                         PersonGUID = p.PersonGUID,
                         Address1 = p.Address1,
                         Address2 = p.Address2,
                         Address3 = p.Address3,
                         BardCodeNumber = p.BarCardNumber,
                         BirthDate = p.Birthdate,
                         City = p.City,
                         Firm = p.Firm,
                         FirstName = p.FirstName,
                         LastName = p.LastName,
                         MiddleName = p.MiddleName,
                         NameSuffix = p.NameSuffix,
                         Sex = p.Sex,
                         EmailAddress = p.EmailAddress,
                         State = p.State.StateName,
                         HasCases = (_ap != null)
                     });

        if (!string.IsNullOrEmpty(this.txtFirstName.Text))
            query = query.Where(it => it.FirstName.StartsWith(this.txtFirstName.Text));

        if (!string.IsNullOrEmpty(this.txtLastName.Text))
            query = query.Where(it => it.LastName.StartsWith(this.txtLastName.Text));

        if (this.ddlSex.SelectedIndex > 0)
            query = query.Where(it => it.Sex == ddlSex.SelectedValue);

        if (this.dtBirthdate.SelectedDate.HasValue)
            query = query.Where(it => it.BirthDate == dtBirthdate.SelectedDate.Value);

        if (this.ddlPersonCategory.SelectedIndex == 1)
            query = query.Where(it => it.HasCases == false);

        return query.Distinct().Take(500).OrderBy(it => it.LastName).ThenBy(it => it.FirstName).ThenBy(it => it.NameSuffix).ThenBy(it => it.Sex).ToList();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        grdAttorney.Rebind();
    }

    protected void grdAttorney_OnItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem dataItem = (GridDataItem)e.Item;
            CheckBox checkbox = (CheckBox)dataItem["With"].FindControl("ckbWith");
            //checkbox.Attributes.Add("onClick", "onselect('" + dataitem.ItemIndex + "');");
            if (dataItem.DataItem != null)
            {
                if (((AttorneyData)dataItem.DataItem).HasCases)
                {
                    dataItem.BackColor = System.Drawing.Color.Green;
                }
                else if (failedToMerge.Contains(((AttorneyData)dataItem.DataItem).PersonGUID))
                {
                    dataItem.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    dataItem.BackColor = System.Drawing.Color.Gray;
                }
            }
        }
    }

    private void PrepareList()
    {
        foreach (GridDataItem dataItem in this.grdAttorney.MasterTableView.Items)
        {
            Guid id = (Guid)grdAttorney.MasterTableView.DataKeyValues[dataItem.ItemIndex]["PersonGUID"];
            if ((dataItem["Replace"].FindControl("ckbReplace") as CheckBox).Checked)
            {
                replaceList.Add(id);
            }
            if ((dataItem["With"].FindControl("ckbWith") as CheckBox).Checked)
            {
                replaceWithList.Add(id);
            }
        }
    }

    private void ValidateSelection()
    {
        if (replaceList.Count == 0)
        {
            messageBox.ErrorMessage = ATTORNEY_NOTSELECTED;
            return;
        }

        if (replaceWithList.Count > 1 || replaceWithList.Count == 0)
        {
            messageBox.ErrorMessage = ERROR_INVALID_WITH;
            return;
        }
        Guid withId = replaceWithList[0];

        if (replaceList.Contains(withId))
        {
            messageBox.ErrorMessage = ERROR_REPLACE;
        }
    }

    protected void btnExecute_Click(object sender, EventArgs e)
    {
        try
        {
            PrepareList();
            ValidateSelection();

            if (!string.IsNullOrEmpty(messageBox.ErrorMessage))
                return;

            Guid withId = replaceWithList[0];

            foreach (Guid id in replaceList)
            {
                try
                {
                    Helper.ExecuteMergePersons(id, withId);
                }
                catch (Exception ex1)
                {
                    if (ex1.Message.Contains("PK_PersonCase"))
                    {
                        messageBox.ErrorMessage = ERROR_2PERSON_SAMECASE;
                    }
                    else
                    {
                        messageBox.ErrorMessage = MERGE_FAIL_TECHDETAILS + ex1.Message;
                    }
                    break;
                }
            }

            if (!string.IsNullOrEmpty(messageBox.ErrorMessage))
                messageBox.SuccessMessage = MERGE_SUCCESS;

            grdAttorney.Rebind();
        }
        catch (Exception ex2)
        {
            messageBox.ErrorMessage = ex2.ToString();
        }
    }

    protected void grdAttorney_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        this.grdAttorney.DataSource = GetAttorneyData();
    }

    protected void btnValidate_Click(object sender, EventArgs e)
    {
        PrepareList();
        ValidateSelection();

        if (!string.IsNullOrEmpty(messageBox.ErrorMessage))
            return;

        Guid withId = replaceWithList[0];
        string errMsg = "";
        foreach (Guid id in replaceList)
        {
            DataSet ds1 = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                    CommandType.StoredProcedure,
                    "SearchCaseByPersonGUIDDuplicate",
                    new SqlParameter("@UserId", 1),
                    new SqlParameter("@PersonGUID", withId),
                    new SqlParameter("@PersonGUID2", id));

            if (ds1.Tables[0].Rows.Count != 0)
            {
                DataSet ds2 = Helper.ExecuteFindActionWithTwoPersons(id, withId);
                foreach (DataRow c in ds2.Tables[0].Rows)
                {
                    errMsg += string.Format("<a href='CaseSummary.aspx?cid={0}'>{1}</a><br/>", c["ActionNumber"], c["CauseNumber"]);
                }
            }
        }

        if (!string.IsNullOrEmpty(errMsg))
            messageBox.ErrorMessage = MERGE_FAIL_2PERSON_SAMECASE + errMsg;
        else
            messageBox.SuccessMessage = VALIDATION_SUCCESS;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }

    protected void btnDeleteUnAssigned_Click(object sender, EventArgs e)
    {
        try
        {
            List<Guid> list1 = new List<Guid>();
            bool itemsSelected = false;
            bool unAssignedSelected = false;

            foreach (GridDataItem dataItem in this.grdAttorney.MasterTableView.Items)
            {
                if ((dataItem["Replace"].FindControl("ckbReplace") as CheckBox).Checked)
                {
                    itemsSelected = true;
                    Guid id = (Guid)grdAttorney.MasterTableView.DataKeyValues[dataItem.ItemIndex]["PersonGUID"];
                    if (dataItem.BackColor == System.Drawing.Color.Gray)
                    {
                        unAssignedSelected = true;
                        List<CSCMS.Data.AttorneyCounty> atcList = CscmsContext.AttorneyCounty.Where(it => it.Person.PersonGUID == id).ToList();

                        foreach (CSCMS.Data.AttorneyCounty atc in atcList)
                            CscmsContext.DeleteObject(atc);

                        CSCMS.Data.Person p = CscmsContext.Person.Where(it => it.PersonGUID == id).FirstOrDefault();
                        if (p != null)
                            CscmsContext.DeleteObject(p);
                    }
                    else
                    {
                        list1.Add(id);
                    }
                }
            }
            CscmsContext.SaveChanges();

            if (!itemsSelected)
                messageBox.ErrorMessage = MSG_UNASSIGNED_DELETE;
            else if (!unAssignedSelected)
                messageBox.ErrorMessage = MSG_UNASSIGNED_DELETE;
            else if (list1.Count > 0)
                messageBox.ErrorMessage = MSG_CANNOTBE_DELETED;
            else
                messageBox.SuccessMessage = MSG_DELETED;

            if (unAssignedSelected) //I assume that we have deleted items, so I am going to rebind.
                grdAttorney.Rebind();
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }

    }
}

[Serializable]
public class AttorneyData
{
    public Guid PersonGUID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string MiddleName { get; set; }
    public string NameSuffix { get; set; }
    public string Sex { get; set; }
    public DateTime? BirthDate { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public int? BardCodeNumber { get; set; }
    public string Firm { get; set; }
    public string EmailAddress { get; set; }
    public bool HasCases { get; set; }
    public bool Replace { get; set; }
    public bool Delete { get; set; }
    public bool With { get; set; }
}