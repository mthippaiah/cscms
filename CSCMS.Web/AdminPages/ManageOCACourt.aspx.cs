﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_ManageOCACourt : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grdOCACourts_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<CSCMS.Data.OCACourt> list = (from c in CscmsContext.OCACourt.Include("Region")
                                          select c).OrderBy(it => it.CourtName).ToList();

        this.grdOCACourts.DataSource = list;
    }

    protected void grdOCACourts_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string courtName = "", so = "", calendarView = "";
            short sortOrder = 0;
            DateTime? edt = null;
            short? regionId = null;
            Func<bool> getValues = delegate()
            {
                courtName = (ei["CourtName"].Controls[0] as TextBox).Text;
                so = (ei["SortOrder"].Controls[0] as TextBox).Text;
                edt = (ei["ExpirationDate"].Controls[0] as RadDatePicker).SelectedDate;
                ListItem li1 = (ei["RegionName"].Controls[0] as DropDownList).SelectedItem;
                if (li1 != null)
                    regionId = short.Parse(li1.Value);
                ListItem li2 = (ei["DefaultCalendarView"].Controls[0] as DropDownList).SelectedItem;
                calendarView = li2.Text;

                if (string.IsNullOrEmpty(courtName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder) || string.IsNullOrEmpty(calendarView) || regionId == null)
                {
                    messageBox.ErrorMessage = "CourtName, SortOrder, CalendarView and Region are required fields. SortOrder is numeric";
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.OCACourt> setValues = delegate(CSCMS.Data.OCACourt c)
            {
                c.CourtName = courtName;
                c.DefaultCalendarView = calendarView;
                c.SortOrder = short.Parse(so);
                c.Region = CscmsContext.Region.Where(it => it.RegionID == regionId).FirstOrDefault();
                c.ExpirationDate = edt;
                c.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == "Delete")
            {
                short ocaCourtId = (short)grdOCACourts.MasterTableView.DataKeyValues[e.Item.ItemIndex]["OCACourtID"];

                CSCMS.Data.OCACourtCounty courtCounty = (from cc in CscmsContext.OCACourtCounty
                                                         where cc.OCACourt.OCACourtID == ocaCourtId
                                                         select cc).FirstOrDefault();
                CSCMS.Data.Cause cause = CscmsContext.Cause.Where(it => it.OCACourt.OCACourtID == ocaCourtId || it.OCACourt1.OCACourtID == ocaCourtId).FirstOrDefault();
                CSCMS.Data.SchedulingException se = CscmsContext.SchedulingException.Where(it => it.OCACourt.OCACourtID == ocaCourtId).FirstOrDefault();
                CSCMS.Data.OriginatingCourt oc = CscmsContext.OriginatingCourt.Where(it => it.OCACourt.OCACourtID == ocaCourtId).FirstOrDefault();
                CSCMS.Data.UserCourt uc = CscmsContext.UserCourt.Where(it => it.OCACourt.OCACourtID == ocaCourtId).FirstOrDefault();
                CSCMS.Data.DailyCourtUserActivity dac = CscmsContext.DailyCourtUserActivity.Where(it => it.OCACourt.OCACourtID == ocaCourtId).FirstOrDefault();
                CSCMS.Data.UserTicklerDismissed utc = CscmsContext.UserTicklerDismissed.Where(it => it.OCACourt.OCACourtID == ocaCourtId).FirstOrDefault();

                if (courtCounty == null && cause == null && se == null && oc == null && uc == null && dac == null && utc == null)
                {
                    CSCMS.Data.OCACourt court = CscmsContext.OCACourt.Where(it => it.OCACourtID == ocaCourtId).FirstOrDefault();
                    if (court != null)
                    {
                        CscmsContext.DeleteObject(court);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "Court deleted successfully.";
                    }
                }
                else
                {
                    messageBox.ErrorMessage = "Cannot delete court. There may be data associated with it.";
                    e.Canceled = true;
                }
            }

            if (e.CommandName == "Update")
            {
                if (getValues())
                {
                    short ocaCourtId = (short)grdOCACourts.MasterTableView.DataKeyValues[e.Item.ItemIndex]["OCACourtID"];
                    CSCMS.Data.OCACourt temp = CscmsContext.OCACourt.Where(it => it.OCACourtID != ocaCourtId && it.CourtName.ToLower() == courtName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a court with that name.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.OCACourt oca = CscmsContext.OCACourt.Where(it => it.OCACourtID == ocaCourtId).FirstOrDefault();
                    if (oca != null)
                    {
                        setValues(oca);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "Court updated successfully.";
                    }
                }
            }

            if (e.CommandName == "PerformInsert")
            {
                if (getValues())
                {
                    CSCMS.Data.OCACourt temp = CscmsContext.OCACourt.Where(it => it.CourtName.ToLower() == courtName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a court with that name.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.OCACourt court = new CSCMS.Data.OCACourt();
                    setValues(court);
                    court.CreateDate = DateTime.Now;
                    CscmsContext.AddToOCACourt(court);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "Court added successfully.";
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
