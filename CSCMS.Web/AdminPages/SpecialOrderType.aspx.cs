﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
public partial class AdminPages_SpecialOrderType : BasePage
{
    protected void grdSot_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string sotName = "", so = "";
            short sortOrder = 0;

            Func<bool> getValues = delegate()
            {
                sotName = (ei["SpecialOrderTypeName"].Controls[0] as TextBox).Text;
                so = (ei["SortOrder"].Controls[0] as TextBox).Text;

                if (string.IsNullOrEmpty(sotName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = "SpecialOrderTypeName and SortOrder are required fields. SortOrder is numeric";
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.SpecialOrderType> setValues = delegate(CSCMS.Data.SpecialOrderType sot)
            {
                sot.SpecialOrderTypeName = sotName;
                sot.SortOrder = byte.Parse(so);
                sot.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == "Delete")
            {
                short id = (short)grdSot.MasterTableView.DataKeyValues[e.Item.ItemIndex]["SpecialOrderTypeID"];

                CSCMS.Data.SpecialOrder s = (from it in CscmsContext.SpecialOrder
                                            where it.SpecialOrderType.SpecialOrderTypeID == id
                                            select it).FirstOrDefault();

                if (s == null)
                {
                    CSCMS.Data.SpecialOrderType sot = CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeID == id).FirstOrDefault();
                    if (sot != null)
                    {
                        CscmsContext.DeleteObject(sot);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "Special order type deleted successfully.";
                    }
                }
                else
                {
                    messageBox.ErrorMessage = "Cannot delete special order type. There may be data associated with it.";
                    e.Canceled = true;
                }
            }

            if (e.CommandName == "Update")
            {
                if (getValues())
                {
                    short id = (short)grdSot.MasterTableView.DataKeyValues[e.Item.ItemIndex]["SpecialOrderTypeID"];
                    CSCMS.Data.SpecialOrderType temp = CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeID != id && it.SpecialOrderTypeName.ToLower() == sotName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a special order type with that name.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.SpecialOrderType sot = CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeID == id).FirstOrDefault();
                    if (sot != null)
                    {
                        setValues(sot);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "Special order type updated successfully.";
                    }
                }
            }

            if (e.CommandName == "PerformInsert")
            {
                if (getValues())
                {
                    CSCMS.Data.SpecialOrderType temp = CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeName.ToLower() == sotName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a special order type with that name.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.SpecialOrderType sot = new CSCMS.Data.SpecialOrderType();
                    setValues(sot);
                    sot.CreateDate = DateTime.Now;
                    CscmsContext.AddToSpecialOrderType(sot);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "Special order type added successfully.";
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
