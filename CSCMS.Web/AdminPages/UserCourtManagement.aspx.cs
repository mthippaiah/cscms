﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CSCMS.Data;
public partial class AdminPages_UserCourtManagement : BasePage
{
    protected override void OnInit(EventArgs e)
    {
        List<CSCMS.Data.User> list = CscmsContext.User.OrderBy(it => it.UserName).ToList();

        this.ddlUserList.DataSource = list;
        this.ddlUserList.DataBind();
        this.ddlUserList.SelectedIndex = 0;
        base.OnInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ddlUserList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.grdUserCourts.Rebind();
    }

    private List<UserCourt> GetUserCourtList()
    {
        int userId = int.Parse(this.ddlUserList.SelectedValue);
        List<UserCourt> list = CscmsContext.UserCourt.Include("OCACourt").Where(it => it.User.UserId == userId).OrderBy(it => it.OCACourt.CourtName).ToList();
        return list;
    }

    protected void grdUserCourts_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string courtName = "";
            short? courtId = null;
            short userId = short.Parse(this.ddlUserList.SelectedValue);

            Func<bool> getValues = delegate()
            {
                ListItem li = (ei["CourtName"].Controls[0] as DropDownList).SelectedItem;
                if (li != null)
                {
                    courtName = li.Text;
                    courtId = short.Parse(li.Value);
                }
                if (courtId == null)
                {
                    messageBox.ErrorMessage = "CourtName is a required field.";
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.UserCourt> setValues = delegate(CSCMS.Data.UserCourt c)
            {
                c.User = CscmsContext.User.Where(it => it.UserId == userId).FirstOrDefault();
                c.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == courtId).FirstOrDefault();
                c.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == "Delete")
            {
                short cid = (short)grdUserCourts.MasterTableView.DataKeyValues[e.Item.ItemIndex]["OCACourtID"];
                CSCMS.Data.UserCourt uc = CscmsContext.UserCourt.Where(it => it.User.UserId == userId && it.OCACourt.OCACourtID == cid).FirstOrDefault();
                if (uc != null)
                {
                    CscmsContext.DeleteObject(uc);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "User-Court deleted successfully.";
                }
            }

            if (e.CommandName == "Update")
            {
                if (getValues())
                {
                    short cid = (short)grdUserCourts.MasterTableView.DataKeyValues[e.Item.ItemIndex]["OCACourtID"];

                    if (cid != courtId)
                    {
                        CSCMS.Data.UserCourt temp = CscmsContext.UserCourt.Where(it => it.OCACourt.OCACourtID == courtId && it.User.UserId == userId).FirstOrDefault();
                        if (temp != null)
                        {
                            messageBox.ErrorMessage = "There is already a User-Court.";
                            e.Canceled = true;
                            return;
                        }
                    }

                    CSCMS.Data.UserCourt uc = CscmsContext.UserCourt.Where(it => it.OCACourt.OCACourtID == cid && it.User.UserId == userId).FirstOrDefault();
                    
                    if (uc != null)
                    {
                        CscmsContext.DeleteObject(uc);
                        CSCMS.Data.UserCourt newUc = new UserCourt();
                        setValues(newUc);
                        newUc.CreateDate = DateTime.Now;
                        CscmsContext.AddToUserCourt(newUc);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "User-Court updated successfully.";
                    }
                }
            }

            if (e.CommandName == "PerformInsert")
            {
                if (getValues())
                {
                    CSCMS.Data.UserCourt temp = CscmsContext.UserCourt.Where(it => it.OCACourt.OCACourtID == courtId && it.User.UserId == userId).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a User-Court.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.UserCourt uc = new CSCMS.Data.UserCourt();
                    setValues(uc);
                    uc.CreateDate = DateTime.Now;
                    CscmsContext.AddToUserCourt(uc);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "User-Court added successfully.";
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }

    protected void grdUserCourts_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        this.grdUserCourts.DataSource = GetUserCourtList();
    }

}
