﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="ManageCourtCounty.aspx.cs" Inherits="AdminPages_ManageCourtCounty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        Manage Court-County 
    </div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <div>
        OCACourts:
        <asp:DropDownList ID="ddlCourtList" runat="server" DataTextField="CourtName" DataValueField="OCACourtID"
            OnSelectedIndexChanged="ddlCourtList_OnSelectedIndexChanged" AutoPostBack="true"
            CausesValidation="false">
        </asp:DropDownList>
    </div>
    <br />
    <div>
        <telerik:RadGrid ID="grdOCACourtCounty" runat="server" AllowPaging="True" PageSize="80"
            AllowSorting="True" OnItemCommand="grdOCACourtCounty_OnItemCommand" OnNeedDataSource="grdOCACourtCounty_OnNeedDataSource">
            <MasterTableView AutoGenerateColumns="false"  CommandItemDisplay="Top"  NoMasterRecordsText="Court is not assigned a county" DataKeyNames="CountyID">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px" >
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="You can set Expiration date instead. Do you want to delete this county from court?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                    <telerik:GridDropDownColumn HeaderText="CountyName" DataField="County.CountyID" ListValueField="CountyID"
                        ListTextField="CountyName" DataSourceID="dsCounty" UniqueName="CountyName" DropDownControlType="DropDownList" />
                    <telerik:GridDateTimeColumn DataField="ExpirationDate" HeaderText="ExpirationDate"
                        UniqueName="ExpirationDate" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
    </div>
    <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
        SelectCommand="SELECT CountyID, CountyName from County"></asp:SqlDataSource>
</asp:Content>
