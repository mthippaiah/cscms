﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="ManageAttorney.aspx.cs" Inherits="AdminPages_ManageAttorney" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <script language="javascript" type="text/javascript">
        function onselect(index) {
            debugger;
            var grid = $find("<%=grdAttorney.ClientID %>");
            var MasterTable = grid.get_masterTableView();
            var dataItem = MasterTable.get_dataItems()[index];
            var replace = MasterTable.getCellByColumnUniqueName(dataItem, "Replace");
            var del = MasterTable.getCellByColumnUniqueName(dataItem, "Delete");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div style="width: 907px">
        <div class="pageTitle">
            Attorney Merger
        </div>
        <CSCMSUC:MessageBox ID="messageBox" runat="server" />
        <div>
            OCACourts:
            <asp:DropDownList ID="ddlCourtList" runat="server" DataTextField="CourtName" DataValueField="OCACourtID"
                CausesValidation="false">
            </asp:DropDownList>
        </div>
        <br />
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="3">
                    <CSCMSUC:SectionContainer ID="sectionCriteria" runat="server" Width="100%" DisplayType="Note">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                    <table>
                                        <tr>
                                            <td class="fieldLabel">
                                                Person Category:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPersonCategory" runat="server" Width="200">
                                                    <asp:ListItem Value="0">Attorney</asp:ListItem>
                                                    <asp:ListItem Value="1">UnAssigned Attorney</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search Persons" CssClass="submit"
                                                    OnClick="btnSearch_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table>
                                        <tr>
                                            <td class="fieldLabel">
                                                First Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtFirstName" runat="server" Width="100" ToolTip=":D" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                Last Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtLastName" runat="server" Width="100" ToolTip=":D" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table>
                                        <tr>
                                            <td class="fieldLabel">
                                                Birth Date:
                                            </td>
                                            <td>
                                                <telerik:RadDatePicker ID="dtBirthdate" runat="server" Width="92" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldLabel">
                                                Sex:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlSex" runat="server" Width="77">
                                                    <asp:ListItem Value="">- Select -</asp:ListItem>
                                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </CSCMSUC:SectionContainer>
                </td>
            </tr>
        </table>
         <br />
        <div>
            <asp:Button ID="btnExecute" runat="server" OnClick="btnExecute_Click" Text="Execute Replace With"
                Width="160px" CssClass="submitMedium" />
            <asp:Button ID="btnValidateMerge" runat="server" OnClick="btnValidate_Click" Text="Validate Replace With"
                Width="160px" CssClass="submitMedium" />
             <asp:Button ID="btnDeleteUnAssigned" runat="server" OnClick="btnDeleteUnAssigned_Click" Text="Delete UnAssigned"
                Width="160px" CssClass="submitMedium" />
        </div>
        <br />
        <asp:Panel ID="gridPanel" runat="server" ScrollBars="Horizontal">
            <telerik:RadGrid ID="grdAttorney" runat="server" AllowPaging="True" Width="100%"
                PageSize="80" AllowSorting="True" OnNeedDataSource="grdAttorney_OnNeedDataSource"
                OnItemCreated="grdAttorney_OnItemCreated" AutoGenerateColumns="false">
                <PagerStyle Mode="NumericPages" />
                <MasterTableView NoMasterRecordsText="No attornies in county" Width="100%" DataKeyNames="PersonGUID">
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Replace<br/>/Delete" UniqueName="Replace">
                            <ItemTemplate>
                                <asp:CheckBox ID="ckbReplace" Checked="false" runat="server" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="With" UniqueName="With">
                            <ItemTemplate>
                                <asp:CheckBox ID="ckbWith" Checked="false" runat="server" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn HeaderText="LastName" DataField="LastName" UniqueName="LastName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="FirstName" DataField="FirstName" UniqueName="FirstName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="MiddleName" DataField="MiddleName" UniqueName="MiddleName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Suffix" DataField="NameSuffix" UniqueName="NameSuffix">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Sex" DataField="Sex" UniqueName="Sex">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Address1" DataField="Address1" UniqueName="Address1">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Address2" DataField="Address2" UniqueName="Address2">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Address3" DataField="Address3" UniqueName="Address3">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="City" DataField="City" UniqueName="City">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="State" DataField="State" UniqueName="State">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="BirthDate" DataField="BirthDate" UniqueName="BirthDate">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="BarCodeNumber" DataField="BarCodeNumber" UniqueName="BarCodeNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Firm" DataField="Firm" UniqueName="Firm">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
                 <PagerStyle AlwaysVisible="true" />
            </telerik:RadGrid>
        </asp:Panel>
       
    </div>
</asp:Content>
