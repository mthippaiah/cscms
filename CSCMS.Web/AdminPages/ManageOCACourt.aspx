﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="ManageOCACourt.aspx.cs" Inherits="AdminPages_ManageOCACourt" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        Manage OCACourt
    </div>
    <cscmsuc:messagebox id="messageBox" runat="server" />
    <div>
        <telerik:radgrid id="grdOCACourts" runat="server" allowpaging="True" pagesize="80"
            allowsorting="True" OnNeedDataSource="grdOCACourts_OnNeedDataSource" OnItemCommand="grdOCACourts_OnItemCommand" >
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no OCACourts" DataKeyNames="OCACourtID" AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                   <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this oca court?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                    <telerik:GridBoundColumn HeaderText="CourtName" DataField="CourtName" UniqueName="CourtName"  />
                    <telerik:GridDateTimeColumn DataField="ExpirationDate" HeaderText="ExpirationDate" UniqueName="ExpirationDate" DataFormatString="{0:MM/dd/yyyy}" >
                    </telerik:GridDateTimeColumn>   
                    <telerik:GridDropDownColumn HeaderText="Default Calendar View" DataField="DefaultCalendarView"
                        ListValueField="DefaultCalendarView" ListTextField="DefaultCalendarView"  DataSourceID="dsDefaultCalendarView" UniqueName="DefaultCalendarView" 
                        DropDownControlType="DropDownList" />
                    <telerik:GridDropDownColumn HeaderText="RegionName" DataField="Region.RegionID"
                        ListValueField="RegionID" ListTextField="RegionName" DataSourceID="dsRegion"  UniqueName="RegionName"
                        DropDownControlType="DropDownList" />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder"  />
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:radgrid>
        <asp:SqlDataSource ID="dsDefaultCalendarView" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT 'Month' DefaultCalendarView UNION SELECT 'Day' DefaultCalendarView UNION SELECT 'Year' DefaultCalendarView">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT RegionID, RegionName from Region">
        </asp:SqlDataSource>
    </div>
</asp:Content>
