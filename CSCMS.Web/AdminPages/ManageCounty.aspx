﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="ManageCounty.aspx.cs" Inherits="AdminPages_ManageCounty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        Manage County
    </div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <div>
        <telerik:RadGrid ID="grdCounty" runat="server" AllowPaging="True" PageSize="80" AllowSorting="True"
            OnNeedDataSource="grdCounty_OnNeedDataSource" OnItemCommand="grdCounty_OnItemCommand">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no counties" DataKeyNames="CountyID"
                AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                        HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this county?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                        HeaderStyle-Width="18px" />
                    <telerik:GridBoundColumn HeaderText="CountyName" DataField="CountyName" UniqueName="CountyName" />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder" />
                    <telerik:GridBoundColumn HeaderText="CalendarColor" DataField="CalendarColor" UniqueName="CalendarColor" />
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
    </div>
</asp:Content>
