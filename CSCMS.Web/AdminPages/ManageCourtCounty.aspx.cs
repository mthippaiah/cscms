﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_ManageCourtCounty : BasePage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.ddlCourtList.DataSource = CscmsContext.OCACourt.ToList();
        this.ddlCourtList.DataBind();
        this.ddlCourtList.SelectedIndex = 0;
        base.OnInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ddlCourtList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.grdOCACourtCounty.Rebind();
    }

    protected void grdOCACourtCounty_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        this.grdOCACourtCounty.DataSource = GetCourtCountyList();
    }

    private List<CSCMS.Data.OCACourtCounty> GetCourtCountyList()
    {
        int id = int.Parse(this.ddlCourtList.SelectedValue);
        List<CSCMS.Data.OCACourtCounty> list = (from it in CscmsContext.OCACourtCounty.Include("County")
                               where it.OCACourt.OCACourtID == id
                               select it).OrderBy(it => it.County.CountyName).ToList();
        return list;
    }

    protected void grdOCACourtCounty_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string countyName = "";
            short? countyId = null;
            DateTime? edt = null;
            int courtId = int.Parse(this.ddlCourtList.SelectedValue);

            Func<bool> getValues = delegate()
            {
                ListItem li2 = (ei["CountyName"].Controls[0] as DropDownList).SelectedItem;
                if (li2 != null)
                {
                    countyName = li2.Text;
                    countyId = short.Parse(li2.Value);
                }
                edt = (ei["ExpirationDate"].Controls[0] as RadDatePicker).SelectedDate;
                if (countyId == null)
                {
                    messageBox.ErrorMessage = "CountyName is a required field.";
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.OCACourtCounty> setValues = delegate(CSCMS.Data.OCACourtCounty c)
            {
                c.County = CscmsContext.County.Where(it => it.CountyID == countyId).FirstOrDefault();
                c.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == courtId).FirstOrDefault();
                c.ExpirationDate = edt;
                c.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == "Delete")
            {
                short cyid = (short)grdOCACourtCounty.MasterTableView.DataKeyValues[e.Item.ItemIndex]["CountyID"];
          
                CSCMS.Data.OCACourtCounty cc = CscmsContext.OCACourtCounty.Where(it => it.CountyID == cyid && it.OCACourt.OCACourtID == courtId).FirstOrDefault();
                if (cc != null)
                {
                    CscmsContext.DeleteObject(cc);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "Court-County deleted successfully.";
                }
            }

            if (e.CommandName == "Update")
            {
                if (getValues())
                {
                    short cyid = (short)grdOCACourtCounty.MasterTableView.DataKeyValues[e.Item.ItemIndex]["CountyID"];

                    if (cyid != countyId)
                    {
                        CSCMS.Data.OCACourtCounty temp = CscmsContext.OCACourtCounty.Where(it => it.OCACourt.OCACourtID == courtId && it.County.CountyID == countyId).FirstOrDefault();
                        if (temp != null)
                        {
                            messageBox.ErrorMessage = "There is already a court-county.";
                            e.Canceled = true;
                            return;
                        }
                    }

                    CSCMS.Data.OCACourtCounty cc = CscmsContext.OCACourtCounty.Where(it => it.OCACourt.OCACourtID == courtId && it.County.CountyID == cyid).FirstOrDefault();
                    if (cc != null)
                    {
                        CscmsContext.DeleteObject(cc);
                        CSCMS.Data.OCACourtCounty newcc = new CSCMS.Data.OCACourtCounty();
                        setValues(newcc);
                        newcc.CreateDate = DateTime.Now;
                        CscmsContext.AddToOCACourtCounty(newcc);
                        CscmsContext.SaveChanges();
                    }
                    messageBox.SuccessMessage = "Court-County updated successfully.";
                }
            }

            if (e.CommandName == "PerformInsert")
            {
                if (getValues())
                {
                    CSCMS.Data.OCACourtCounty temp = CscmsContext.OCACourtCounty.Where(it => it.OCACourt.OCACourtID == courtId && it.County.CountyID == countyId).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a court-county.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.OCACourtCounty cc = new CSCMS.Data.OCACourtCounty();
                    setValues(cc);
                    cc.CreateDate = DateTime.Now;
                    CscmsContext.AddToOCACourtCounty(cc);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "Court-County added successfully.";
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
