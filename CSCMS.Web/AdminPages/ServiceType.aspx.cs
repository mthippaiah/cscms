﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_ServiceType : BasePage
{
    protected void grdSvcType_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string svcTypeName = "", so = "";
            short sortOrder = 0;

            Func<bool> getValues = delegate()
            {
                svcTypeName = (ei["ServiceTypeName"].Controls[0] as TextBox).Text;
                so = (ei["SortOrder"].Controls[0] as TextBox).Text;

                if (string.IsNullOrEmpty(svcTypeName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = "ServiceTypeName and SortOrder are required fields. SortOrder is numeric";
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.ServiceType> setValues = delegate(CSCMS.Data.ServiceType svt)
            {
                svt.ServiceTypeName = svcTypeName;
                svt.SortOrder = byte.Parse(so);
                svt.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == "Delete")
            {
                short id = (short)grdSvcType.MasterTableView.DataKeyValues[e.Item.ItemIndex]["ServiceTypeID"];

                CSCMS.Data.Service svc = (from it in CscmsContext.Service
                                           where it.ServiceType.ServiceTypeID == id
                                           select it).FirstOrDefault();

                if (svc == null)
                {
                    CSCMS.Data.ServiceType svt = CscmsContext.ServiceType.Where(it => it.ServiceTypeID == id).FirstOrDefault();
                    if (svt != null)
                    {
                        CscmsContext.DeleteObject(svt);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "Service type deleted successfully.";
                    }
                }
                else
                {
                    messageBox.ErrorMessage = "Cannot delete service type. There may be data associated with it.";
                    e.Canceled = true;
                }
            }

            if (e.CommandName == "Update")
            {
                if (getValues())
                {
                    short id = (short)grdSvcType.MasterTableView.DataKeyValues[e.Item.ItemIndex]["ServiceTypeID"];
                    CSCMS.Data.ServiceType temp = CscmsContext.ServiceType.Where(it => it.ServiceTypeID != id && it.ServiceTypeName.ToLower() == svcTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a service type with that name.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.ServiceType svt = CscmsContext.ServiceType.Where(it => it.ServiceTypeID == id).FirstOrDefault();
                    if (svt != null)
                    {
                        setValues(svt);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "Service type updated successfully.";
                    }
                }
            }

            if (e.CommandName == "PerformInsert")
            {
                if (getValues())
                {
                    CSCMS.Data.ServiceType temp = CscmsContext.ServiceType.Where(it => it.ServiceTypeName.ToLower() == svcTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a service type with that name.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.ServiceType svt = new CSCMS.Data.ServiceType();
                    setValues(svt);
                    svt.CreateDate = DateTime.Now;
                    CscmsContext.AddToServiceType(svt);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "Service type added successfully.";
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
