﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_OAGOuNumberMap : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void grdOAGOuNumberMap_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string ouNumber = "";
            short? courtId = null;

            Func<bool> getValues = delegate()
            {
                ouNumber = (ei["OuNumber"].Controls[0] as TextBox).Text.Trim();
                ListItem li2 = (ei["OCACourtName"].FindControl("ddlCourt") as DropDownList).SelectedItem;
                if (li2 != null)
                    courtId = short.Parse(li2.Value);

                if (courtId == null || string.IsNullOrEmpty(ouNumber))
                {
                    messageBox.ErrorMessage = "OuNumber and CourtName are required fields";
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.OAGCourtNumberMap> setValues = delegate(CSCMS.Data.OAGCourtNumberMap c)
            {
                c.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == courtId).FirstOrDefault();
                c.OuNumber = ouNumber;
                c.LastUpdateDate = DateTime.Now;
            };


            if (e.CommandName == "Delete")
            {
                int id = (int)grdOAGOuNumberMap.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];

                CSCMS.Data.OAGCourtNumberMap am = CscmsContext.OAGCourtNumberMap.Where(it => it.Id == id).FirstOrDefault();
                if (am != null)
                {
                    CscmsContext.DeleteObject(am);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "OuNumberMap deleted successfully.";
                }

            }

            if (e.CommandName == "Update")
            {
                if (getValues())
                {
                    int id = (int)grdOAGOuNumberMap.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];
                    CSCMS.Data.OAGCourtNumberMap temp = CscmsContext.OAGCourtNumberMap.Where(it => it.Id != id && it.OCACourt.OCACourtID == courtId && it.OuNumber.ToLower() == ouNumber.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a map between OuNumber and OCACourt.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.OAGCourtNumberMap am = CscmsContext.OAGCourtNumberMap.Where(it => it.Id == id).FirstOrDefault();
                    if (am != null)
                    {
                        setValues(am);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "OuNumberMap updated successfully.";
                    }
                }
            }

            if (e.CommandName == "PerformInsert")
            {
                if (getValues())
                {
                    CSCMS.Data.OAGCourtNumberMap temp = CscmsContext.OAGCourtNumberMap.Where(it => it.OCACourt.OCACourtID == courtId && it.OuNumber.ToLower() == ouNumber.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a map between OuNumber and OCACourt.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.OAGCourtNumberMap am = new CSCMS.Data.OAGCourtNumberMap();
                    setValues(am);
                    am.CreateDate = DateTime.Now;
                    CscmsContext.AddToOAGCourtNumberMap(am);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "OuNumberMap added successfully.";
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }

    protected void grdOAGOuNumberMap_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<CSCMS.Data.OAGCourtNumberMap> list = (from am in CscmsContext.OAGCourtNumberMap.Include("OCACourt")
                                              select am).OrderBy(it => it.OuNumber).ToList();
        this.grdOAGOuNumberMap.DataSource = list;
    }

    protected void grdOAGOuNumberMap_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.IsInEditMode)
        {
            TextBox ouNumber = (TextBox)(e.Item as GridEditableItem)["OuNumber"].Controls[0];
            DropDownList ddlCourt = (DropDownList)(e.Item as GridEditableItem)["OCACourtName"].Controls[1];
            List<CSCMS.Data.OCACourt> list = CscmsContext.OCACourt.OrderBy(it => it.CourtName).ToList();
            ddlCourt.DataSource = list;
            ddlCourt.DataTextField = "CourtName";
            ddlCourt.DataValueField = "OCACourtID";
            ddlCourt.DataBind();

            if (e.Item.ItemIndex > 0)
            {
                int id = (int)grdOAGOuNumberMap.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];
                ddlCourt.SelectedValue = CscmsContext.OAGCourtNumberMap.Where(it => it.Id == id).Select(it => it.OCACourt.OCACourtID).FirstOrDefault().ToString();
            }
        }
    }
}
