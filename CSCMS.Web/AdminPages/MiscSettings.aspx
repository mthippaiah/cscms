﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="MiscSettings.aspx.cs" Inherits="AdminPages_MiscSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        Miscellaneous Court Settings
    </div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <br />
    <div>
        OCACourts:
        <asp:DropDownList ID="ddlCourtList" runat="server" DataTextField="CourtName" DataValueField="OCACourtID"
            OnSelectedIndexChanged="ddlCourtList_OnSelectedIndexChanged" AutoPostBack="true"
            CausesValidation="false">
        </asp:DropDownList>
    </div>
    <hr />
    <div>
        <table>
            <tr>
                <td style="width: 20%; text-align: right">
                    Hearing Time
                </td>
                <td style="width: 80%; text-align: left">
                    <telerik:RadTimePicker ID="hearingTime" runat="server" Skin="Office2007" Width="75px">
                        <DateInput Font-Size="11px" Font-Names="Arial" />
                        <TimeView Skin="Office2007" StartTime="07:00:00" Interval="00:15:00" EndTime="18:00:00"
                            Columns="4"/>
                        <TimePopupButton ImageUrl="~/images/icon_clock.gif" HoverImageUrl="~/images/icon_clock.gif" />
                    </telerik:RadTimePicker>
                    <asp:RequiredFieldValidator ID="rfvHearingTime" runat="server" ErrorMessage="Hearing Time Required" ControlToValidate="hearingTime">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center">
                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_OnClick" CssClass="submitMedium" Width="80px" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
