﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="InterpreterLanguage.aspx.cs" Inherits="AdminPages_InterpreterLanguage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
<div class="pageTitle">
        Manage InterpreterLanguage
    </div>
    <cscmsuc:messagebox id="messageBox" runat="server" />
    <div>
        <telerik:RadGrid id="grdLanguage" runat="server" AllowPaging="True" PageSize="80"
            AllowSorting="True" DataSourceID="dsLanguage"  OnItemCommand="grdLanguage_OnItemCommand" >
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no interpreter languages" DataKeyNames="IntepreterLanguageID" AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                   <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this interpreter language?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                    <telerik:GridBoundColumn HeaderText="InterpreterLanguageName" DataField="IntepreterLanguageName" UniqueName="IntepreterLanguageName"  />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder"  />
                </Columns>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsLanguage" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT IntepreterLanguageID, IntepreterLanguageName, SortOrder from IntepreterLanguage">
        </asp:SqlDataSource>
    </div>
</asp:Content>

