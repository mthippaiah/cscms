﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using EncryptDecrypt;
using System.Text;

public partial class AdminPages_UserList : AdminBasePage
{
    public string sb = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            grdUsers.PageSize = 100;
        }
    }

    protected void grdUsers_ItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == "impersonate")
        {
            SessionHelper.ResetSessionForUserCourtChange();
            this.SetUser(e.CommandArgument.ToString());
            Session["Impersonating"] = true;
            ((BasePage)this.Page).CurrentCause = null;
            ((BasePage)this.Page).CurrentAction = null;
            ((BasePage)this.Page).CurrentPerson = null;
            ((BasePage)this.Page).CurrentActionPerson = null;
            Response.Redirect("~/MemberPages/MyPage.aspx", true);
            return;
        }
        if (e.CommandName == "Update")
        {
            Update(source, e);
            grdUsers.MasterTableView.ClearEditItems();
        }

        if (e.CommandName == "PerformInsert")
        {
            grdUsers.MasterTableView.IsItemInserted = false;
            Insert(source, e);
            e.Canceled = true;
            grdUsers.Rebind();
        }

    }

    private void Update(object source, GridCommandEventArgs e)
    {
        GridEditableItem editedItem = e.Item as GridEditableItem;

        //Get the primary key value using the DataKeyValue.    
        short? userId;
        string password;
        string userName;
        short? userTypeId;
        DateTime? beginDate;
        DateTime? endDate;
        string fullName;
        short? court;
        string email;
        string phone;
        GetEditedValues(editedItem, out court, out userId, out password, out userName, out userTypeId, out beginDate, out endDate, out fullName, out email, out phone);
        EncDec ed = new EncDec();
        try
        {
            CSCMS.Data.User user = base.CscmsContext.User.Include("UserType").Where(it => it.UserId == userId).FirstOrDefault();
            CSCMS.Data.UserCourt uc = CscmsContext.UserCourt.Where(it => it.User.UserId == userId).FirstOrDefault();

            if (user != null)
            {
                user.UserFullName = fullName;
                user.password = ed.EncryptText(password);
                user.UserName = userName;
                user.email_address = email;
                user.BeginDate = beginDate.Value;
                user.EndDate = endDate;
                user.UserType = base.CscmsContext.UserType.Where(it => it.UserTypeID == userTypeId.Value).FirstOrDefault();

                //update user court
                if (uc != null)
                    uc.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == court).FirstOrDefault();
                else if (court != null)
                {
                    uc = new CSCMS.Data.UserCourt();
                    uc.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == court).FirstOrDefault();
                    uc.User = user;
                    base.CscmsContext.AddToUserCourt(uc);
                }

                base.CscmsContext.SaveChanges();
                UpdateCurrentUser(user);  //I don't know why we need this
            }
        }
        catch (Exception ex)
        {
            grdUsers.Controls.Add(new LiteralControl("Unable to update Employee. Reason: " + ex.Message));
            e.Canceled = true;
        }
    }

    private static void GetEditedValues(GridEditableItem editedItem, out short? court, out short? userId, out string password, out string userName, out short? userTypeId, out DateTime? beginDate, out DateTime? endDate, out string fullName, out string email, out string phone)
    {
        userId = null;
        if (editedItem.ItemIndex != -1) // in Insert mode it will be -1
            userId = (short)editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["UserId"];
        password = ((TextBox)editedItem.FindControl("txtPassword")).Text;
        userName = (editedItem["UserName"].Controls[0] as TextBox).Text;
        email = (editedItem["Email"].Controls[0] as TextBox).Text;
        phone = (editedItem["Phone"].Controls[0] as TextBox).Text;
        string userType = (editedItem["UserTypeID"].Controls[0] as DropDownList).SelectedValue;
        userTypeId = null;
        if (!string.IsNullOrEmpty(userType))
            userTypeId = short.Parse(userType);

        beginDate = (editedItem["BeginDate"].Controls[0] as RadDatePicker).SelectedDate;
        endDate = (editedItem["EndDate"].Controls[0] as RadDatePicker).SelectedDate;
        fullName = (editedItem["UserFullName"].Controls[0] as TextBox).Text;
        court = null;
        string c = (editedItem["OCACourtID"].Controls[0] as DropDownList).SelectedValue;
        if (!string.IsNullOrEmpty(c))
            court = short.Parse((editedItem["OCACourtID"].Controls[0] as DropDownList).SelectedValue);
    }

    private void Insert(object source, GridCommandEventArgs e)
    {
        GridEditFormInsertItem insertedItem = (GridEditFormInsertItem)e.Item;

        short? userId;
        string password;
        string userName;
        short? userTypeId;
        DateTime? beginDate;
        DateTime? endDate;
        string fullName;
        short? court;
        string email;
        string phone;
        GetEditedValues(insertedItem, out court, out userId, out password, out userName, out userTypeId, out beginDate, out endDate, out fullName, out email, out phone);

        try
        {

            CSCMS.Data.User user = new CSCMS.Data.User();
            EncDec ed = new EncDec();
            if (user != null)
            {
                user.UserFullName = fullName;
                user.password = ed.EncryptText(password);
                user.UserName = userName;
                user.BeginDate = beginDate.Value;
                user.EndDate = endDate;
                user.phone = phone;
                user.email_address = email;
                user.active = true;
                user.UserType = base.CscmsContext.UserType.Where(it => it.UserTypeID == userTypeId.Value).FirstOrDefault();
                base.CscmsContext.AddToUser(user);
                if (court != null)
                {
                    CSCMS.Data.UserCourt uc = new CSCMS.Data.UserCourt();
                    uc.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == court).FirstOrDefault();
                    uc.User = user;
                    uc.CreateDate = DateTime.Now;
                    base.CscmsContext.AddToUserCourt(uc);
                }
                CscmsContext.SaveChanges();
            }
        }
        catch (Exception ex)
        {
            grdUsers.Controls.Add(new LiteralControl("Unable to insert Employee. Reason: " + ex.Message));
            e.Canceled = true;
        }
    }

    private void UpdateCurrentUser(CSCMS.Data.User user)
    {
        if (user.UserId == this.CurrentUser.UserID)
        {
            SetUser(user.UserName);
        }
    }

    protected void grdUsers_OnItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if ((e.Item is GridEditFormItem) && e.Item.IsInEditMode)
        {
            TextBox txtPwd = (TextBox)e.Item.FindControl("txtPassword");
            if (txtPwd != null)
            {
                sb = string.Format("window['{0}'].SetValue({1});", txtPwd.ClientID, txtPwd.Text);
            }
        }
    }

    protected void grdUsers_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<UserData> list = (from u in base.CscmsContext.User
                               join ut in base.CscmsContext.UserType on u.UserType.UserTypeID equals ut.UserTypeID
                               let court = (from uc in CscmsContext.UserCourt
                                            join c in CscmsContext.OCACourt on uc.OCACourtID equals c.OCACourtID
                                            where u.UserId == uc.UserID
                                            select c).FirstOrDefault()
                               select new UserData
                                   {
                                       UserId = u.UserId,
                                       UserTypeID = ut.UserTypeID,
                                       BeginDate = u.BeginDate,
                                       EndDate = u.EndDate,
                                       password = u.password,
                                       UserFullName = u.UserFullName,
                                       UserName = u.UserName,
                                       UserTypeName = ut.UserTypeName,
                                       Email = u.email_address,
                                       Phone = u.phone,
                                       court = court
                                   }).OrderBy(it => it.UserName).ToList();
        EncDec ed = new EncDec();
        foreach (UserData u in list)
        {
            if (u.court != null)
            {
                u.OCACourtID = u.court.OCACourtID;
                u.CourtName = u.court.CourtName;
            }
        }
        grdUsers.DataSource = list;
    }
}

[Serializable]
public class UserData
{
    public short UserId { get; set; }
    public short UserTypeID { get; set; }
    public short OCACourtID { get; set; }
    public string UserName { get; set; }
    public string password { get; set; }
    public string UserFullName { get; set; }
    public string UserTypeName { get; set; }
    public string CourtName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    public DateTime? BeginDate { get; set; }
    public DateTime? EndDate { get; set; }
    public CSCMS.Data.OCACourt court { get; set; }
}

