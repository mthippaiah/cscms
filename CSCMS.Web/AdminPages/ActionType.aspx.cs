﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
public partial class AdminPages_ActionType : BasePage
{
    const string REQUIRED_FIELDS_ERROR = "ActionTypeName, ActionCategory and SortOrder are required fields. SortOrder is numeric";
    const string ACTIONTYPE_DELETE = "Action type deleted successfully.";
    const string CANNOT_DELETE_ACTIONTYPE = "Cannot delete action type. There may be data associated with it.";
    const string ACTIONTYPE_ALREADY_EXISTS = "There is already an action type with that name.";
    const string ACTIONTYPE_UPDATED = "Action type updated successfully.";
    const string ACTIONTYPE_ADDED = "Action type added successfully.";
    protected void grdActionType_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string actionTypeName = "", so = "", catId = "";
            bool isExpedite = false;
            short sortOrder = 0, actionCatId = 0;

            Func<bool> getValues = delegate()
            {
                actionTypeName = (ei[MyConsts.DB_ACTIONTYPE_NAME].Controls[0] as TextBox).Text;
                so = (ei[MyConsts.DB_SORT_ORDER].Controls[0] as TextBox).Text;
                isExpedite = (ei[MyConsts.DB_ISEXPEDITE].Controls[0] as CheckBox).Checked;
                ListItem li1 = (ei[MyConsts.DB_ACTIONCAT_NAME].Controls[0] as DropDownList).SelectedItem;
                if (li1 != null)
                {
                    catId = li1.Value;
                    actionCatId = short.Parse(li1.Value);
                }

                if (string.IsNullOrEmpty(actionTypeName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder) || string.IsNullOrEmpty(catId))
                {
                    messageBox.ErrorMessage = REQUIRED_FIELDS_ERROR;
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.ActionType> setValues = delegate(CSCMS.Data.ActionType at)
            {
                at.ActionTypeName = actionTypeName;
                at.SortOrder = byte.Parse(so);
                at.IsExpedite = isExpedite;
                at.ActionCategory = CscmsContext.ActionCategory.Where(it => it.ActionCategoryID == actionCatId).FirstOrDefault();
                at.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == MyConsts.GRID_DELETE)
            {
                short id = (short)grdActionType.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_ACTIONTYPE_ID];

                CSCMS.Data.Action action = (from it in CscmsContext.Action
                                            where it.ActionType.ActionTypeID == id
                                            select it).FirstOrDefault();

                if (action == null)
                {
                    CSCMS.Data.ActionType at = CscmsContext.ActionType.Where(it => it.ActionTypeID == id).FirstOrDefault();
                    if (at != null)
                    {
                        CscmsContext.DeleteObject(at);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = ACTIONTYPE_DELETE;
                    }
                }
                else
                {
                    messageBox.ErrorMessage = CANNOT_DELETE_ACTIONTYPE;
                    e.Canceled = true;
                }
            }

            if (e.CommandName == MyConsts.GRID_UPDATE)
            {
                if (getValues())
                {
                    short id = (short)grdActionType.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_ACTIONTYPE_ID];
                    CSCMS.Data.ActionType temp = CscmsContext.ActionType.Where(it => it.ActionTypeID != id && it.ActionTypeName.ToLower() == actionTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = ACTIONTYPE_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.ActionType at = CscmsContext.ActionType.Where(it => it.ActionTypeID == id).FirstOrDefault();
                    if (at != null)
                    {
                        setValues(at);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = ACTIONTYPE_UPDATED;
                    }
                }
            }

            if (e.CommandName == MyConsts.GRID_INSERT)
            {
                if (getValues())
                {
                    CSCMS.Data.ActionType temp = CscmsContext.ActionType.Where(it => it.ActionTypeName.ToLower() == actionTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = ACTIONTYPE_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.ActionType at = new CSCMS.Data.ActionType();
                    setValues(at);
                    at.CreateDate = DateTime.Now;
                    CscmsContext.AddToActionType(at);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = ACTIONTYPE_ADDED;
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
