﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="UserCourtManagement.aspx.cs" Inherits="AdminPages_UserCourtManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        Manage User-Court
    </div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <div>
        Users: <asp:DropDownList ID="ddlUserList" runat="server" DataTextField="UserName" DataValueField="UserId" OnSelectedIndexChanged="ddlUserList_OnSelectedIndexChanged" AutoPostBack="true" CausesValidation="false"></asp:DropDownList>
    </div>
    <br />
    <div>
        <telerik:RadGrid ID="grdUserCourts" runat="server" AllowPaging="True" PageSize="10"  AllowSorting="True" OnItemCommand="grdUserCourts_OnItemCommand" 
        OnNeedDataSource="grdUserCourts_OnNeedDataSource" >
            <MasterTableView AutoGenerateColumns="false"  CommandItemDisplay="Top" NoMasterRecordsText="User is not assigned a court" DataKeyNames="OCACourtID">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px" >
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Do you want to delete this court for user?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                     <telerik:GridDropDownColumn HeaderText="CourtName" DataField="OCACourt.OCACourtID" ListValueField="OCACourtID"
                        ListTextField="CourtName" DataSourceID="dsCourt" UniqueName="CourtName" DropDownControlType="DropDownList" />
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
    </div>
    <br />
    <asp:SqlDataSource ID="dsCourt" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
        SelectCommand="SELECT OCACourtID, CourtName from OCACourt"></asp:SqlDataSource>
</asp:Content>
