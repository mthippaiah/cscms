﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="ManageOriginatingCourts.aspx.cs" Inherits="AdminPages_ManageOriginatingCourts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        Manage Originating Court
    </div>
    <CSCMSUC:MessageBox ID="messageBox" runat="server" />
    <br />
    <div>
        OCACourts:
        <asp:DropDownList ID="ddlCourtList" runat="server" DataTextField="CourtName" DataValueField="OCACourtID"
            OnSelectedIndexChanged="ddlCourtList_OnSelectedIndexChanged" AutoPostBack="true"
            CausesValidation="false">
        </asp:DropDownList>
    </div>
    <hr />
    <div>
        <telerik:RadGrid ID="grdOrgCourt" runat="server" AllowPaging="True" PageSize="80"
            AllowSorting="True" OnNeedDataSource="grdOrgCourt_OnNeedDataSource" OnItemCommand="grdOrgCourt_OnItemCommand"
            OnItemDataBound="grdOrgCourt_ItemDataBound">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no counties" DataKeyNames="OriginatingCourtID"
                AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                        HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this Originating Court?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                        HeaderStyle-Width="18px" />
                    <telerik:GridBoundColumn HeaderText="OCACourtName" DataField="OCACourt.CourtName" UniqueName="OCACourtName" />
                    <telerik:GridTemplateColumn HeaderText="CountyName" UniqueName="CountyName">
                        <ItemTemplate>
                            <%# Eval("County.CountyName") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCounty" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HeaderText="OrgCourtName" DataField="CourtName" UniqueName="OrgCourtName" />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder" />
                    <telerik:GridDateTimeColumn DataField="ExpirationDate" HeaderText="ExpirationDate"
                        UniqueName="ExpirationDate" DataFormatString="{0:MM/dd/yyyy}">
                    </telerik:GridDateTimeColumn>
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsOCACourt" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT OCACourtID, CourtName from OCACourt"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT CountyID, CountyName from County"></asp:SqlDataSource>
    </div>
</asp:Content>
