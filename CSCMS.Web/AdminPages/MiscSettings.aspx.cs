﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminPages_MiscSettings : BasePage
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.ddlCourtList.DataSource = CscmsContext.OCACourt.ToList();
        this.ddlCourtList.DataBind();
        this.ddlCourtList.SelectedIndex = 0;
        base.OnInit(e);
    }

    protected void ddlCourtList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshValues();
    }

    private void RefreshValues()
    {
        int id = int.Parse(this.ddlCourtList.SelectedValue);
        TimeSpan ht = CscmsContext.OCACourt.Where(it => it.OCACourtID == id).Select(it => it.DefaultHearingTime).FirstOrDefault();
        hearingTime.SelectedDate = DateTime.Today.Add(ht);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RefreshValues();
        }
    }

    protected void btnSave_OnClick(object sender, EventArgs e)
    {
        int id = int.Parse(this.ddlCourtList.SelectedValue);
        CSCMS.Data.OCACourt ct = CscmsContext.OCACourt.Where(it => it.OCACourtID == id).FirstOrDefault();
        ct.DefaultHearingTime = hearingTime.SelectedDate.Value.TimeOfDay;
        CscmsContext.SaveChanges();
    }
}
