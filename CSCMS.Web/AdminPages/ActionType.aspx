﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="ActionType.aspx.cs" Inherits="AdminPages_ActionType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
<div class="pageTitle">
        Manage ActionType
    </div>
    <cscmsuc:messagebox id="messageBox" runat="server" />
    <div>
        <telerik:RadGrid id="grdActionType" runat="server" AllowPaging="True" PageSize="80"
            AllowSorting="True" DataSourceID="dsActionType"  OnItemCommand="grdActionType_OnItemCommand" >
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no action types" DataKeyNames="ActionTypeID" AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                   <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this action type?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                     <telerik:GridDropDownColumn HeaderText="ActionCategoryName" DataField="ActionCategoryID"
                        ListValueField="ActionCategoryID" ListTextField="ActionCategoryName" DataSourceID="dsActionCat"  UniqueName="ActionCategoryName"
                        DropDownControlType="DropDownList" />
                    <telerik:GridBoundColumn HeaderText="ActionTypeName" DataField="ActionTypeName" UniqueName="ActionTypeName"  />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder"  />
                    <telerik:GridCheckBoxColumn HeaderText="IsExpedite" DataField="IsExpedite" UniqueName="IsExpedite" />                  
                </Columns>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsActionType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT ActionTypeID, ActionCategoryID, ActionTypeName, SortOrder, IsExpedite from ActionType">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsActionCat" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT ActionCategoryID, ActionCategoryName from ActionCategory">
        </asp:SqlDataSource>
    </div>

</asp:Content>

