﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_DispositionType : BasePage
{
    const string REQUIRED_FIELDS_ERROR = "DispositionTypeName and SortOrder are required fields. SortOrder is numeric";
    const string DISPTYPE_DELETED = "Disposition type deleted successfully.";
    const string CANNOT_DELETE_DISPTYPE = "Cannot delete disposition type. There may be data associated with it.";
    const string DISPTYPE_ALREADY_EXISTS = "There is already a disposition type with that name.";
    const string DISPTYPE_UPDATED = "Disposition type updated successfully.";
    const string DISPTYPE_ADDED = "Disposition type added successfully.";

    protected void grdDispType_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string dispTypeName = "", so = "";
            bool isActive = false;
            short sortOrder = 0;

            Func<bool> getValues = delegate()
            {
                dispTypeName = (ei[MyConsts.DB_DISPTYPE_NAME].Controls[0] as TextBox).Text;
                so = (ei[MyConsts.DB_SORT_ORDER].Controls[0] as TextBox).Text;
                isActive = (ei[MyConsts.DB_ISACTIVE].Controls[0] as CheckBox).Checked;

                if (string.IsNullOrEmpty(dispTypeName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = REQUIRED_FIELDS_ERROR;
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.DispositionType> setValues = delegate(CSCMS.Data.DispositionType it)
            {
                it.DispositionTypeName = dispTypeName;
                it.SortOrder = byte.Parse(so);
                it.IsActive = isActive;
                it.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == MyConsts.GRID_DELETE)
            {
                short id = (short)grdDispType.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_DISPTYPE_ID];

                CSCMS.Data.Action action = (from it in CscmsContext.Action
                                            where it.DispositionType.DispositionTypeID == id
                                            select it).FirstOrDefault();

                if (action == null)
                {
                    CSCMS.Data.DispositionType dt = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == id).FirstOrDefault();
                    if (dt != null)
                    {
                        CscmsContext.DeleteObject(dt);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = DISPTYPE_DELETED;
                    }
                }
                else
                {
                    messageBox.ErrorMessage = CANNOT_DELETE_DISPTYPE;
                    e.Canceled = true;
                }
            }

            if (e.CommandName == MyConsts.GRID_UPDATE)
            {
                if (getValues())
                {
                    short id = (short)grdDispType.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_DISPTYPE_ID];
                    CSCMS.Data.DispositionType temp = CscmsContext.DispositionType.Where(it => it.DispositionTypeID != id && it.DispositionTypeName.ToLower() == dispTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = DISPTYPE_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.DispositionType dt = CscmsContext.DispositionType.Where(it => it.DispositionTypeID == id).FirstOrDefault();
                    if (dt != null)
                    {
                        setValues(dt);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = DISPTYPE_UPDATED;
                    }
                }
            }

            if (e.CommandName == MyConsts.GRID_INSERT)
            {
                if (getValues())
                {
                    CSCMS.Data.DispositionType temp = CscmsContext.DispositionType.Where(it => it.DispositionTypeName.ToLower() == dispTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = DISPTYPE_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.DispositionType dt = new CSCMS.Data.DispositionType();
                    setValues(dt);
                    dt.CreateDate = DateTime.Now;
                    CscmsContext.AddToDispositionType(dt);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = DISPTYPE_ADDED;
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
