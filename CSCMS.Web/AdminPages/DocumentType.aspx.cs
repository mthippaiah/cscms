﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_DocumentType : BasePage
{
    const string REQUIRED_FIELDS_ERROR = "DocumentTypeName and SortOrder are required fields. SortOrder is numeric";
    const string DOCTYPE_DELETE_SUCCESS = "Document type deleted successfully.";
    const string CANNOT_DELETE_DOCTYPE = "Cannot delete document type. There may be data associated with it.";
    const string DOCTYPE_ALREADY_EXISTS = "There is already a document type with that name.";
    const string DOCTYPE_UPDATED = "Document type updated successfully.";
    const string DOCTYPE_ADDED = "Document type added successfully.";

    protected void grdDocumentType_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string docTypeName = "", so = "";
            short sortOrder = 0;
            
            Func<bool> getValues = delegate()
            {
                docTypeName = (ei[MyConsts.DB_DOCTYPE_NAME].Controls[0] as TextBox).Text;
                so = (ei[MyConsts.DB_SORT_ORDER].Controls[0] as TextBox).Text;

                if (string.IsNullOrEmpty(docTypeName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = REQUIRED_FIELDS_ERROR;
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.DocumentType> setValues = delegate(CSCMS.Data.DocumentType c)
            {
                c.DocumentTypeName = docTypeName;
                c.SortOrder = byte.Parse(so);
                c.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == MyConsts.GRID_DELETE)
            {
                short id = (short)grdDocumentType.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_DOCTYPE_ID];

                CSCMS.Data.Document doc = (from it in CscmsContext.Document
                                           where it.DocumentType.DocumentTypeID == id
                                           select it).FirstOrDefault();
              
                if (doc == null)
                {
                    CSCMS.Data.DocumentType dt = CscmsContext.DocumentType.Where(it => it.DocumentTypeID == id).FirstOrDefault();
                    if (dt != null)
                    {
                        CscmsContext.DeleteObject(dt);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = DOCTYPE_DELETE_SUCCESS;
                    }
                }
                else
                {
                    messageBox.ErrorMessage = CANNOT_DELETE_DOCTYPE;
                    e.Canceled = true;
                }
            }

            if (e.CommandName == MyConsts.GRID_UPDATE)
            {
                if (getValues())
                {
                    short id = (short)grdDocumentType.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_DOCTYPE_ID];
                    CSCMS.Data.DocumentType temp = CscmsContext.DocumentType.Where(it => it.DocumentTypeID != id &&  it.DocumentTypeName.ToLower() == docTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = DOCTYPE_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.DocumentType dt = CscmsContext.DocumentType.Where(it => it.DocumentTypeID == id).FirstOrDefault();
                    if (dt != null)
                    {
                        setValues(dt);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = DOCTYPE_UPDATED;
                    }
                }
            }

            if (e.CommandName == MyConsts.GRID_INSERT)
            {
                if (getValues())
                {
                    CSCMS.Data.DocumentType temp = CscmsContext.DocumentType.Where(it => it.DocumentTypeName.ToLower() == docTypeName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = DOCTYPE_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.DocumentType dt = new CSCMS.Data.DocumentType();
                    setValues(dt);
                    dt.CreateDate = DateTime.Now;
                    CscmsContext.AddToDocumentType(dt);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = DOCTYPE_ADDED;
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
