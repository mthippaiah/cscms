﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_ManageCounty : BasePage
{
    const string COUNTY_ALREADY_EXISTS = "There is already a county with that name.";
    const string COUNTY_UPDATED = "County updated successfully.";
    const string COUNTY_ADDED = "County added successfully.";
    const string CANNOT_DELETE_COUNTY = "Cannot delete county. There may be data associated with it.";
    const string COUNTY_DELETED = "County deleted successfully.";
    const string REQUIRED_FIELDS_ERROR = "CountyName and SortOrder are required fields. SortOrder is numeric";

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void grdCounty_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        List<CSCMS.Data.County> list = (from cy in CscmsContext.County
                                        select cy).OrderBy(it => it.CountyName).ToList();
        this.grdCounty.DataSource = list;
    }

    protected void grdCounty_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string countyName = "", so = "", color = "";
            short sortOrder = 0;
            Func<bool> getValues = delegate()
            {
                countyName = (ei[MyConsts.DB_COUNTYNAME].Controls[0] as TextBox).Text;
                so = (ei[MyConsts.DB_SORT_ORDER].Controls[0] as TextBox).Text;
                color = (ei[MyConsts.DB_CAL_COLOR].Controls[0] as TextBox).Text;
                if (string.IsNullOrEmpty(countyName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = REQUIRED_FIELDS_ERROR;
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.County> setValues = delegate(CSCMS.Data.County c)
            {
                c.CountyName = countyName;
                c.CalendarColor = color;
                c.SortOrder = short.Parse(so);
                c.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == MyConsts.GRID_DELETE)
            {
                short countyId = (short)grdCounty.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_COUNTYID];

                CSCMS.Data.OCACourtCounty courtCounty = (from cc in CscmsContext.OCACourtCounty
                                                         where cc.County.CountyID == countyId
                                                         select cc).FirstOrDefault();
                CSCMS.Data.Cause cause = CscmsContext.Cause.Where(it => it.County.CountyID == countyId).FirstOrDefault();
                CSCMS.Data.AttorneyCounty atc = CscmsContext.AttorneyCounty.Where(it => it.County.CountyID == countyId).FirstOrDefault();
                CSCMS.Data.OriginatingCourt oc = CscmsContext.OriginatingCourt.Where(it => it.County.CountyID == countyId).FirstOrDefault();

                if (courtCounty == null && cause == null && atc == null && oc == null)
                {
                    CSCMS.Data.County cy = CscmsContext.County.Where(it => it.CountyID == countyId).FirstOrDefault();
                    if (cy != null)
                    {
                        CscmsContext.DeleteObject(cy);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = COUNTY_DELETED;
                    }
                }
                else
                {
                    messageBox.ErrorMessage = CANNOT_DELETE_COUNTY;
                    e.Canceled = true;
                }
            }

            if (e.CommandName == MyConsts.GRID_UPDATE)
            {
                if (getValues())
                {
                    short countyId = (short)grdCounty.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_COUNTYID];
                    CSCMS.Data.County temp = CscmsContext.County.Where(it => it.CountyID != countyId && it.CountyName.ToLower() == countyName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = COUNTY_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.County cy = CscmsContext.County.Where(it => it.CountyID == countyId).FirstOrDefault();
                    if (cy != null)
                    {
                        setValues(cy);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = COUNTY_UPDATED;
                    }
                }
            }

            if (e.CommandName == MyConsts.GRID_INSERT)
            {
                if (getValues())
                {
                    CSCMS.Data.County temp = CscmsContext.County.Where(it => it.CountyName.ToLower() == countyName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = COUNTY_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.County county = new CSCMS.Data.County();
                    setValues(county);
                    county.CreateDate = DateTime.Now;
                    CscmsContext.AddToCounty(county);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = COUNTY_ADDED;
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
