﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true" CodeFile="ServiceType.aspx.cs" Inherits="AdminPages_ServiceType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
<div class="pageTitle">
        Manage ServiceType
    </div>
    <cscmsuc:messagebox id="messageBox" runat="server" />
    <div>
        <telerik:RadGrid id="grdSvcType" runat="server" AllowPaging="True" PageSize="80"
            AllowSorting="True" DataSourceID="dsSvcType"  OnItemCommand="grdSvcType_OnItemCommand" >
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView NoMasterRecordsText="There are no service types" DataKeyNames="ServiceTypeID" AutoGenerateColumns="false" CommandItemDisplay="Top">
                <Columns>
                   <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="18px">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this service type?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" HeaderStyle-Width="18px" />
                    <telerik:GridBoundColumn HeaderText="ServiceTypeName" DataField="ServiceTypeName" UniqueName="ServiceTypeName"  />
                    <telerik:GridBoundColumn HeaderText="SortOrder" DataField="SortOrder" UniqueName="SortOrder"  />
                </Columns>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsSvcType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT ServiceTypeID, ServiceTypeName, SortOrder from ServiceType">
        </asp:SqlDataSource>
    </div>
</asp:Content>

