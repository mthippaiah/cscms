﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_InterpreterLanguage : BasePage
{
    const string REQUIRED_FIELDS_ERROR = "InterpreterLanguageName and SortOrder are required fields. SortOrder is numeric";
    const string INTPL_DELETED = "Interpreter language deleted successfully.";
    const string CANNOT_DELETE_INTPL = "Cannot delete interpreter language. There may be data associated with it.";
    const string INTPL_ALREADY_EXISTS = "There is already a interpreter language with that name.";
    const string INTPL_UPDATED = "Interpreter language updated successfully.";
    const string INTPL_ADDED = "Interpreter language added successfully.";
    protected void grdLanguage_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string langName = "", so = "";
            short sortOrder = 0;

            Func<bool> getValues = delegate()
            {
                langName = (ei[MyConsts.DB_INTPL_NAME].Controls[0] as TextBox).Text;
                so = (ei[MyConsts.DB_SORT_ORDER].Controls[0] as TextBox).Text;

                if (string.IsNullOrEmpty(langName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = REQUIRED_FIELDS_ERROR;
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.IntepreterLanguage> setValues = delegate(CSCMS.Data.IntepreterLanguage lang)
            {
                lang.IntepreterLanguageName = langName;
                lang.SortOrder = byte.Parse(so);
                lang.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == MyConsts.GRID_DELETE)
            {
                short id = (short)grdLanguage.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_INTPL_ID];

                CSCMS.Data.Person p = (from it in CscmsContext.Person
                                             where it.IntepreterLanguage.IntepreterLanguageID == id 
                                             || it.IntepreterLanguage1.IntepreterLanguageID == id
                                             || it.IntepreterLanguage2.IntepreterLanguageID == id
                                             select it).FirstOrDefault();

                if (p == null)
                {
                    CSCMS.Data.IntepreterLanguage lang = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == id).FirstOrDefault();
                    if (lang != null)
                    {
                        CscmsContext.DeleteObject(lang);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = INTPL_DELETED;
                    }
                }
                else
                {
                    messageBox.ErrorMessage = CANNOT_DELETE_INTPL;
                    e.Canceled = true;
                }
            }

            if (e.CommandName == MyConsts.GRID_UPDATE)
            {
                if (getValues())
                {
                    short id = (short)grdLanguage.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_INTPL_ID];
                    CSCMS.Data.IntepreterLanguage temp = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID != id && it.IntepreterLanguageName.ToLower() == langName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = INTPL_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.IntepreterLanguage lang = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageID == id).FirstOrDefault();
                    if (lang != null)
                    {
                        setValues(lang);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = INTPL_UPDATED;
                    }
                }
            }

            if (e.CommandName == MyConsts.GRID_INSERT)
            {
                if (getValues())
                {
                    CSCMS.Data.IntepreterLanguage temp = CscmsContext.IntepreterLanguage.Where(it => it.IntepreterLanguageName.ToLower() == langName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = INTPL_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.IntepreterLanguage lang = new CSCMS.Data.IntepreterLanguage();
                    setValues(lang);
                    lang.CreateDate = DateTime.Now;
                    CscmsContext.AddToIntepreterLanguage(lang);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = INTPL_ADDED;
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
