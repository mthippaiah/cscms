﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
public partial class AdminPages_ActionCategory : BasePage
{
    const string REQUIRED_FIELDS_ERROR = "ActionCategoryName and SortOrder are required fields. SortOrder is numeric";
    const string ACTIONCAT_DELETED = "Action category deleted successfully.";
    const string CANNOT_DELETE_ACTIONCAT = "Cannot delete action category. There may be data associated with it.";
    const string ACTIONCAT_ALREADY_EXISTS = "There is already an action category with that name.";
    const string ACTIONCAT_UPDATED = "Action category updated successfully.";
    const string ACTIONCAT_ADDED = "Action category added successfully.";

    protected void grdActionCat_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string catName = "", so = "";
            short sortOrder = 0;

            Func<bool> getValues = delegate()
            {
                catName = (ei[MyConsts.DB_ACTIONCAT_NAME].Controls[0] as TextBox).Text;
                so = (ei[MyConsts.DB_SORT_ORDER].Controls[0] as TextBox).Text;

                if (string.IsNullOrEmpty(catName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = REQUIRED_FIELDS_ERROR;
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.ActionCategory> setValues = delegate(CSCMS.Data.ActionCategory ac)
            {
                ac.ActionCategoryName = catName;
                ac.SortOrder = byte.Parse(so);
                ac.LastUpdateDate = DateTime.Now;
            };

            if (e.CommandName == MyConsts.GRID_DELETE)
            {
                short id = (short)grdActionCat.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_ACTIONCAT_ID];

                CSCMS.Data.ActionType at = (from it in CscmsContext.ActionType
                                            where it.ActionCategory.ActionCategoryID == id
                                            select it).FirstOrDefault();

                if (at == null)
                {
                    CSCMS.Data.ActionCategory ac = CscmsContext.ActionCategory.Where(it => it.ActionCategoryID == id).FirstOrDefault();
                    if (ac != null)
                    {
                        CscmsContext.DeleteObject(ac);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = ACTIONCAT_DELETED;
                    }
                }
                else
                {
                    messageBox.ErrorMessage = CANNOT_DELETE_ACTIONCAT;
                    e.Canceled = true;
                }
            }

            if (e.CommandName == MyConsts.GRID_UPDATE)
            {
                if (getValues())
                {
                    short id = (short)grdActionCat.MasterTableView.DataKeyValues[e.Item.ItemIndex][MyConsts.DB_ACTIONCAT_ID];
                    CSCMS.Data.ActionCategory temp = CscmsContext.ActionCategory.Where(it => it.ActionCategoryID != id && it.ActionCategoryName.ToLower() == catName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = ACTIONCAT_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.ActionCategory ac = CscmsContext.ActionCategory.Where(it => it.ActionCategoryID == id).FirstOrDefault();
                    if (ac != null)
                    {
                        setValues(ac);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = ACTIONCAT_UPDATED;
                    }
                }
            }

            if (e.CommandName == MyConsts.GRID_INSERT)
            {
                if (getValues())
                {
                    CSCMS.Data.ActionCategory temp = CscmsContext.ActionCategory.Where(it => it.ActionCategoryName.ToLower() == catName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = ACTIONCAT_ALREADY_EXISTS;
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.ActionCategory ac = new CSCMS.Data.ActionCategory();
                    setValues(ac);
                    ac.CreateDate = DateTime.Now;
                    CscmsContext.AddToActionCategory(ac);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = ACTIONCAT_ADDED;
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }
}
