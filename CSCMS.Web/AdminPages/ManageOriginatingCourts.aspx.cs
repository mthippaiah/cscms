﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class AdminPages_ManageOriginatingCourts : BasePage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.ddlCourtList.DataSource = CscmsContext.OCACourt.ToList();
        this.ddlCourtList.DataBind();
        this.ddlCourtList.SelectedIndex = 0;
        base.OnInit(e);
    }

    protected void ddlCourtList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.grdOrgCourt.Rebind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void grdOrgCourt_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        int id = int.Parse(this.ddlCourtList.SelectedValue);
        List<CSCMS.Data.OriginatingCourt> list = (from oc in CscmsContext.OriginatingCourt.Include("OCACourt").Include("County")
                                                  where oc.OCACourt.OCACourtID == id
                                                  select oc).OrderBy(it => it.OCACourt.CourtName).ThenBy(it => it.County.CountyName).ToList();
        this.grdOrgCourt.DataSource = list;
    }

    protected void grdOrgCourt_OnItemCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            GridEditableItem ei = e.Item as GridEditableItem;
            string orgCourtName = "", so = "";
            int? courtId = null, countyId = null;
            short sortOrder = 0;
            DateTime? edt = null;
            Func<bool> getValues = delegate()
            {
                courtId = int.Parse(this.ddlCourtList.SelectedValue);

                ListItem li2 = (ei["CountyName"].FindControl("ddlCounty") as DropDownList).SelectedItem;
                if (li2 != null)
                    countyId = short.Parse(li2.Value);
                edt = (ei["ExpirationDate"].Controls[0] as RadDatePicker).SelectedDate;
                orgCourtName = (ei["OrgCourtName"].Controls[0] as TextBox).Text;
                so = (ei["SortOrder"].Controls[0] as TextBox).Text;
                if (courtId == null || countyId == null || string.IsNullOrEmpty(orgCourtName) || string.IsNullOrEmpty(so) || !short.TryParse(so, out sortOrder))
                {
                    messageBox.ErrorMessage = "OCACourtName, CountyName, OrgCourtName and SortOrder are required fields. SortOrder is numeric";
                    e.Canceled = true;
                }
                return !e.Canceled;
            };

            Action<CSCMS.Data.OriginatingCourt> setValues = delegate(CSCMS.Data.OriginatingCourt c)
            {
                c.County = CscmsContext.County.Where(it => it.CountyID == countyId).FirstOrDefault();
                c.OCACourt = CscmsContext.OCACourt.Where(it => it.OCACourtID == courtId).FirstOrDefault();
                c.CourtName = orgCourtName;
                c.SortOrder = sortOrder;
                c.ExpirationDate = edt;
                c.LastUpdateDate = DateTime.Now;
            };


            if (e.CommandName == "Delete")
            {
                int id = (int)grdOrgCourt.MasterTableView.DataKeyValues[e.Item.ItemIndex]["OriginatingCourtID"];

                CSCMS.Data.Cause cause = CscmsContext.Cause.Where(it => it.OriginatingCourt.OriginatingCourtID == id).FirstOrDefault();

                if (cause == null)
                {
                    CSCMS.Data.OriginatingCourt oc = CscmsContext.OriginatingCourt.Where(it => it.OriginatingCourtID == id).FirstOrDefault();
                    if (oc != null)
                    {
                        CscmsContext.DeleteObject(oc);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "OriginatingCourt deleted successfully.";
                    }
                }
                else
                {
                    messageBox.ErrorMessage = "Cannot delete originating court. There may be data associated with it.";
                    e.Canceled = true;
                }
            }

            if (e.CommandName == "Update")
            {
                if (getValues())
                {
                    int id = (int)grdOrgCourt.MasterTableView.DataKeyValues[e.Item.ItemIndex]["OriginatingCourtID"];
                    CSCMS.Data.OriginatingCourt temp = CscmsContext.OriginatingCourt.Where(it => it.OriginatingCourtID != id && it.OCACourt.OCACourtID == courtId && it.County.CountyID == countyId && it.CourtName.ToLower() == orgCourtName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a originating court associated with ocacourt and county.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.OriginatingCourt oc = CscmsContext.OriginatingCourt.Where(it => it.OriginatingCourtID == id).FirstOrDefault();
                    if (oc != null)
                    {
                        setValues(oc);
                        CscmsContext.SaveChanges();
                        messageBox.SuccessMessage = "OriginatingCourt updated successfully.";
                    }
                }
            }

            if (e.CommandName == "PerformInsert")
            {
                if (getValues())
                {
                    CSCMS.Data.OriginatingCourt temp = CscmsContext.OriginatingCourt.Where(it => it.OCACourt.OCACourtID == courtId && it.County.CountyID == countyId && it.CourtName.ToLower() == orgCourtName.ToLower()).FirstOrDefault();
                    if (temp != null)
                    {
                        messageBox.ErrorMessage = "There is already a originating court associated with ocacourt and county.";
                        e.Canceled = true;
                        return;
                    }

                    CSCMS.Data.OriginatingCourt oc = new CSCMS.Data.OriginatingCourt();
                    setValues(oc);
                    oc.CreateDate = DateTime.Now;
                    CscmsContext.AddToOriginatingCourt(oc);
                    CscmsContext.SaveChanges();
                    messageBox.SuccessMessage = "OriginatingCourt added successfully.";
                }
            }
        }
        catch (Exception ex)
        {
            messageBox.ErrorMessage = ex.ToString();
        }
    }

    protected void ddlCourtName_OnSelectedIndexChanged(object sender, EventArgs args)
    {
        DropDownList ddlCourt = (DropDownList)sender;
        GridEditableItem editItem = (GridEditableItem)ddlCourt.NamingContainer;
        DropDownList ddlCounty = (DropDownList)editItem.FindControl("ddlCounty");
        int courtId = int.Parse(ddlCourt.SelectedItem.Value);
        ddlCounty.DataSource = GetCountyList(courtId);
        ddlCounty.DataTextField = "CountyName";
        ddlCounty.DataValueField = "CountyID";
        ddlCounty.DataBind();
    }

    protected void grdOrgCourt_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.IsInEditMode)
        {
            int courtId = int.Parse(this.ddlCourtList.SelectedValue);
            TextBox txtCourt = (TextBox)(e.Item as GridEditableItem)["OCACourtName"].Controls[0];
            DropDownList ddlCounty = (DropDownList)(e.Item as GridEditableItem)["CountyName"].Controls[1];
            List<CSCMS.Data.County> list = GetCountyList(courtId);
            ddlCounty.DataSource = list;
            ddlCounty.DataTextField = "CountyName";
            ddlCounty.DataValueField = "CountyID";
            ddlCounty.DataBind();

            if (e.Item.ItemIndex > 0)
            {
                int id = (int)grdOrgCourt.MasterTableView.DataKeyValues[e.Item.ItemIndex]["OriginatingCourtID"];
                ddlCounty.SelectedValue = CscmsContext.OriginatingCourt.Where(it => it.OriginatingCourtID == id).Select(it => it.County.CountyID).FirstOrDefault().ToString();
            }
            txtCourt.Text = this.ddlCourtList.SelectedItem.Text;
            txtCourt.Enabled = false;
        }
    }

    private List<CSCMS.Data.County> GetCountyList(int courtId)
    {
        List<CSCMS.Data.County> list = (from ccy in CscmsContext.OCACourtCounty
                                        where ccy.OCACourt.OCACourtID == courtId
                                        && (ccy.ExpirationDate == null || ccy.ExpirationDate > DateTime.Now)
                                        select ccy.County).ToList();
        return list;
    }
}
