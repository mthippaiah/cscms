﻿<%@ Page Title="CSCMS | LookUpTable | Users" Language="C#" MasterPageFile="~/CSCMS.master"
    AutoEventWireup="true" CodeFile="LookupTable.aspx.cs" Inherits="AdminPages_LookupTable" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        function editorClientLoad(editor) {
            var style = editor.get_contentArea().style;
            style.backgroundColor = "#ffffff";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        CSCMS LOOKUP TABLE</div>
    Select &nbsp
    <asp:DropDownList ID="ddlLookUpTable" runat="server" DataSourceID="sqlLookUpTable"
        DataTextField="LKDescription" DataValueField="LKTableName" AutoPostBack="True"
        OnSelectedIndexChanged="ddlLookUpTable_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:SqlDataSource ID="sqlLookUpTable" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
        SelectCommand="GetLookUpTable" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <br />
    <br />
    <br />
    <!--  -->
    <!--  -->
    <!-- sectionBroadcastMessages -->
    <CSCMSUC:SectionContainer ID="sectionBroadcastMessages" runat="server" HeaderText="Broadcast Message"
        Width="100%" DisplayType="Grid" Visible="false">
        <telerik:RadGrid ID="grdBroadcastMessages" runat="server" DataSourceID="dsBroadcastMessages"
            GridLines="None" AllowPaging="True" PageSize="50" AllowAutomaticUpdates="True"
            AllowAutomaticInserts="True" AllowAutomaticDeletes="true" OnItemUpdated="grd1_ItemUpdated"
            OnItemInserted="grd1_ItemInserted" OnItemDeleted="grd1_ItemDeleted" AllowSorting="true"
            Width="750px" OnItemCreated="grdBroadcastMessages_ItemCreated">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView DataSourceID="dsBroadcastMessages" AutoGenerateColumns="false" DataKeyNames="MessageId"
                CommandItemDisplay="Top">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this Broadcast Message?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" />
                    <telerik:GridBoundColumn HeaderText="ID" DataField="MessageId" />
                    <telerik:GridTemplateColumn HeaderText="Message" UniqueName="Message">
                        <ItemTemplate>
                            <%# Eval("Message") %></ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadEditor ID="editor" runat="server" Skin="Office2007" ToolsFile="~/EditorTools.xml"
                                Content='<%# Bind("Message") %>' OnClientLoad="editorClientLoad">
                                <Content></Content>
                            </telerik:RadEditor>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridDateTimeColumn HeaderText=" Start Date" DataField="StartDate" DataFormatString="{0:MM/dd/yyyy}"
                        PickerType="DatePicker" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                    <telerik:GridDateTimeColumn HeaderText=" Expired Date" DataField="MessageExpiredDate"
                        DataFormatString="{0:MM/dd/yyyy}" PickerType="DatePicker" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Center" />
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsBroadcastMessages" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT [MessageId], [Message], [MessageExpiredDate], [StartDate] FROM [BroadcastMessages] ORDER BY [StartDate] DESC, [MessageExpiredDate] DESC"
            DeleteCommand="DELETE FROM [BroadcastMessages] WHERE [MessageId] = @MessageId"
            InsertCommand="INSERT INTO [BroadcastMessages] ([Message], [MessageExpiredDate], [StartDate]) VALUES (@Message, @MessageExpiredDate, @StartDate)"
            UpdateCommand="UPDATE [BroadcastMessages] SET [Message] = @Message, [MessageExpiredDate] = @MessageExpiredDate, [StartDate] = @StartDate WHERE [MessageId] = @MessageId">
            <DeleteParameters>
                <asp:Parameter Name="MessageId" Type="Decimal" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="Message" Type="String" />
                <asp:Parameter Name="MessageExpiredDate" Type="DateTime" />
                <asp:Parameter Name="StartDate" Type="DateTime" />
                <asp:Parameter Name="MessageId" Type="Decimal" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="Message" Type="String" />
                <asp:Parameter Name="MessageExpiredDate" Type="DateTime" />
                <asp:Parameter Name="StartDate" Type="DateTime" />
            </InsertParameters>
        </asp:SqlDataSource>
    </CSCMSUC:SectionContainer>
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!-- sectionOCACourt -->
    <!-- sectionOCACourt -->
    <CSCMSUC:SectionContainer ID="sectionOCACourt" runat="server" HeaderText="OCA Court"
        Width="100%" DisplayType="Grid" Visible="false">
        <telerik:RadGrid ID="grdOCACourt" runat="server" DataSourceID="dsOCACourt" GridLines="None"
            AllowPaging="True" PageSize="50" AllowAutomaticUpdates="True" AllowAutomaticInserts="True"
            AllowAutomaticDeletes="true" OnItemUpdated="grd1_ItemUpdated" OnItemInserted="grd1_ItemInserted"
            OnItemDeleted="grd1_ItemDeleted" AllowSorting="true" Width="750px" OnItemCreated="grdOCACourt_ItemCreated">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView DataSourceID="dsOCACourt" AutoGenerateColumns="false" DataKeyNames="OCACourtID"
                CommandItemDisplay="Top">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this OCA Court?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" />
                    <telerik:GridBoundColumn HeaderText="ID" DataField="OCACourtID" />
                    <telerik:GridBoundColumn HeaderText="Court Name" DataField="CourtName" />
                    <%--        <telerik:GridCheckBoxColumn HeaderText="Public Judge Docket" DataField="PublicPostingDocket" />--%>
                    <telerik:GridDropDownColumn HeaderText="Default Calendar View" DataField="DefaultCalendarView"
                        ListValueField="DefaultCalendarView" ListTextField="DefaultCalendarView" DataSourceID="dsDefaultCalendarView"
                        DropDownControlType="DropDownList" />
                    <telerik:GridNumericColumn DataType="System.Int16" MaxLength="3" UniqueName="SortOrder"
                        SortExpression="SortOrder" HeaderText="Sort Order" DataField="SortOrder" FooterText="">
                    </telerik:GridNumericColumn>
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsDefaultCalendarView" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            SelectCommand="SELECT 'Month' DefaultCalendarView UNION SELECT 'Day' DefaultCalendarView UNION SELECT 'Year' DefaultCalendarView">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsOCACourt" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            DeleteCommand="DELETE FROM [OCACourt] WHERE [OCACourtID] = @OCACourtID" InsertCommand="INSERT INTO [OCACourt] ([CourtName], [SortOrder],  DefaultCalendarView ) VALUES (@CourtName, @SortOrder,  @DefaultCalendarView)"
            SelectCommand="SELECT [OCACourtID], [CourtName], [SortOrder],  DefaultCalendarView FROM [OCACourt]"
            UpdateCommand="UPDATE [OCACourt] SET [CourtName] = @CourtName, [SortOrder] = @SortOrder,   DefaultCalendarView = @DefaultCalendarView WHERE [OCACourtID] = @OCACourtID">
            <DeleteParameters>
                <asp:Parameter Name="OCACourtID" Type="Byte" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="CourtName" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Byte" />
                <asp:Parameter Name="OCACourtID" Type="Byte" />
                <%--                <asp:Parameter Name="PublicPostingDocket" Type="Boolean" />--%>
                <asp:Parameter Name="DefaultCalendarView" Type="String" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="CourtName" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Byte" />
                <%--                <asp:Parameter Name="PublicPostingDocket" Type="Boolean" />--%>
                <asp:Parameter Name="DefaultCalendarView" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
        <CSCMSUC:SectionContainer ID="sectionOCACourtCounty_2" runat="server" HeaderText="OCACourt County"
            Width="100%" DisplayType="Grid" Visible="true">
            <telerik:RadGrid ID="grdOCACourtCounty_2" runat="server" DataSourceID="dsOCACourtCounty_2"
                GridLines="None" AllowPaging="True" PageSize="50" AllowAutomaticUpdates="True"
                AllowAutomaticInserts="True" AllowAutomaticDeletes="true" OnItemUpdated="grd1_ItemUpdated"
                OnItemInserted="grd1_ItemInserted" OnItemDeleted="grd1_ItemDeleted" AllowSorting="true"
                Width="750px" OnItemCreated="grdOCACourtCounty_2_ItemCreated">
                <PagerStyle Mode="NextPrevAndNumeric" />
                <MasterTableView DataSourceID="dsOCACourtCounty_2" AutoGenerateColumns="false" DataKeyNames="OCACourtID, CountyID"
                    CommandItemDisplay="Top">
                    <Columns>
                        <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="Delete this OCA Court/County?" ConfirmDialogType="RadWindow"
                            ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" />
                        <telerik:GridDropDownColumn HeaderText="OCA Court" DataField="OCACourtID" ListValueField="OCACourtID"
                            ListTextField="CourtName" DataSourceID="dsOCACourt2_2" DropDownControlType="DropDownList" />
                        <telerik:GridDropDownColumn HeaderText="County" DataField="CountyID" ListValueField="CountyID"
                            ListTextField="CountyName" DataSourceID="dsCounty3_2" DropDownControlType="DropDownList" />
                        <%--<telerik:GridDropDownColumn HeaderText="Admin Judicial Region" DataField="AdminJudicialRegion"
                            ListValueField="AdminJudicialRegion" ListTextField="RegionName" DataSourceID="dsRegion"
                            DropDownControlType="DropDownList" />--%>
                        <%-- <telerik:GridDropDownColumn HeaderText="Judge" DataField="JudgeUserID" ListValueField="UserID"
                            ListTextField="UserName" DataSourceID="dsUser3_2" DropDownControlType="DropDownList" />--%>
                        <telerik:GridDateTimeColumn HeaderText="Expiration Date" DataField="ExpirationDate"
                            DataFormatString="{0:MM/dd/yyyy}" PickerType="DatePicker" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                    <EditItemTemplate>
                        <br />
                    </EditItemTemplate>
                    <EditFormSettings>
                        <EditColumn ButtonType="ImageButton" />
                    </EditFormSettings>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:SqlDataSource ID="dsOCACourtCounty_2" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                DeleteCommand="DELETE FROM [OCACourtCounty] WHERE [OCACourtID] = @OCACourtID AND [CountyID] = @CountyID"
                InsertCommand="INSERT INTO [OCACourtCounty] ([OCACourtID], [CountyID],  [ExpirationDate]) VALUES (@OCACourtID, @CountyID,  @ExpirationDate)"
                SelectCommand="SELECT [OCACourtID], [CountyID], [ExpirationDate] FROM [OCACourtCounty] WHERE OCACourtID=@OCACourtID ORDER BY [OCACourtID], [CountyID]"
                UpdateCommand="UPDATE [OCACourtCounty] SET  [ExpirationDate] = @ExpirationDate WHERE [OCACourtID] = @OCACourtID AND [CountyID] = @CountyID">
                <SelectParameters>
                    <asp:ControlParameter ControlID="grdOCACourt" DefaultValue="999" Name="OCACourtID"
                        PropertyName="SelectedValue" Type="Int16" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="OCACourtID" Type="Int16" />
                    <asp:Parameter Name="CountyID" Type="Int16" />
                </DeleteParameters>
                <UpdateParameters>
                    <%--                   <asp:Parameter Name="AdminJudicialRegion" Type="Int16" />--%>
                    <%--            <asp:Parameter Name="JudgeUserID" Type="Int16" />--%>
                    <asp:Parameter Name="ExpirationDate" DbType="Date" />
                    <asp:Parameter Name="OCACourtID" Type="Int16" />
                    <asp:Parameter Name="CountyID" Type="Int16" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:Parameter Name="OCACourtID" Type="Int16" />
                    <asp:Parameter Name="CountyID" Type="Int16" />
                    <%--   <asp:Parameter Name="AdminJudicialRegion" Type="Int16" />
                    <asp:Parameter Name="JudgeUserID" Type="Int16" />--%>
                    <asp:Parameter Name="ExpirationDate" DbType="Date" />
                </InsertParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="dsCounty3_2" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                SelectCommand="select CountyID, CountyName from County order by CountyName">
            </asp:SqlDataSource>
             <asp:SqlDataSource ID="dsOCACourt2_2" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                SelectCommand="select OCACourtID, CourtName from OCACourt where OCACOurtID = @OCACourtID order by CourtName">
                <SelectParameters>
                    <asp:ControlParameter ControlID="grdOCACourt" DefaultValue="999" Name="OCACourtID"
                        PropertyName="SelectedValue" Type="Int16" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="dsUser3_2" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                SelectCommand="SELECT [UserId], [UserName], [UserTypeID] FROM [User] WHERE ([UserTypeID] = @UserTypeID) ORDER BY [UserName]">
                <SelectParameters>
                    <asp:Parameter DefaultValue="3" Name="UserTypeID" Type="Int16" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="dsRegion" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                SelectCommand="SELECT [AdminJudicialRegion], [RegionName] FROM [Region]"></asp:SqlDataSource>
        </CSCMSUC:SectionContainer>
        <br />
    </CSCMSUC:SectionContainer>
    <!--  -->
    <!--  -->
    <!-- sectionCounty -->
    <CSCMSUC:SectionContainer ID="sectionCounty" runat="server" HeaderText="County" Width="100%"
        DisplayType="Grid" Visible="false">
        <telerik:RadGrid ID="grdCounty" runat="server" DataSourceID="dsCounty" GridLines="None"
            AllowPaging="True" PageSize="50" AllowAutomaticUpdates="True" AllowAutomaticInserts="True"
            AllowAutomaticDeletes="true" OnItemUpdated="grd1_ItemUpdated" OnItemInserted="grd1_ItemInserted"
            OnItemDeleted="grd1_ItemDeleted" AllowSorting="true" Width="750px" OnItemCreated="grdCounty_ItemCreated">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView DataSourceID="dsCounty" AutoGenerateColumns="false" DataKeyNames="CountyID"
                CommandItemDisplay="Top">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this County?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" />
                    <telerik:GridBoundColumn HeaderText="ID" DataField="CountyID" />
                    <telerik:GridBoundColumn HeaderText="County" DataField="CountyName" />
                    <telerik:GridBoundColumn HeaderText="Calendar Color" DataField="CalendarColor" />
                    <telerik:GridNumericColumn DataType="System.Int16" MaxLength="3" UniqueName="SortOrder"
                        SortExpression="SortOrder" HeaderText="Sort Order" DataField="SortOrder" FooterText="">
                    </telerik:GridNumericColumn>
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            DeleteCommand="DELETE FROM [County] WHERE [CountyID] = @CountyID" InsertCommand="INSERT INTO [County] ([CountyName], [SortOrder], [CalendarColor]) VALUES (@CountyName, @SortOrder, @CalendarColor)"
            SelectCommand="SELECT [CountyID], [CountyName], [SortOrder], [CalendarColor] FROM [County] ORDER BY [CountyName]"
            UpdateCommand="UPDATE [County] SET [CountyName] = @CountyName, [SortOrder] = @SortOrder, [CalendarColor] = @CalendarColor WHERE [CountyID] = @CountyID">
            <DeleteParameters>
                <asp:Parameter Name="CountyID" Type="Int16" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="CountyName" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Int16" />
                <asp:Parameter Name="CalendarColor" Type="String" />
                <asp:Parameter Name="CountyID" Type="Int16" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="CountyName" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Int16" />
                <asp:Parameter Name="CalendarColor" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <!--  -->
        <!--  -->
    </CSCMSUC:SectionContainer>
    <!--  -->
    <!--  -->
    <!-- sectionState -->
    <CSCMSUC:sectioncontainer id="sectionState" runat="server" headertext="State" width="100%"
        displaytype="Grid" visible="false">
        <telerik:RadGrid ID="grdState" runat="server" DataSourceID="dsState" GridLines="None"
            AllowPaging="True" PageSize="50" AllowAutomaticUpdates="True" AllowAutomaticInserts="True"
            AllowAutomaticDeletes="true" OnItemUpdated="grd1_ItemUpdated" OnItemInserted="grd1_ItemInserted"
            OnItemDeleted="grd1_ItemDeleted" AllowSorting="true" Width="750px" OnItemCreated="grdState_ItemCreated">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView DataSourceID="dsState" AutoGenerateColumns="false" DataKeyNames="StateID"
                CommandItemDisplay="Top">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this State Type?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" />
                    <telerik:GridBoundColumn HeaderText="ID" DataField="StateID" />
                    <telerik:GridBoundColumn HeaderText="State" DataField="StateName" />
                    <telerik:GridNumericColumn DataType="System.Int16" MaxLength="3" UniqueName="SortOrder"
                        SortExpression="SortOrder" HeaderText="Sort Order" DataField="SortOrder" FooterText="">
                    </telerik:GridNumericColumn>
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsState" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            DeleteCommand="DELETE FROM [State] WHERE [StateID] = @StateID" InsertCommand="INSERT INTO [State] ([StateName], [SortOrder]) VALUES (@StateName, @SortOrder)"
            SelectCommand="SELECT [StateID], [StateName], [SortOrder] FROM [State]" UpdateCommand="UPDATE [State] SET [StateName] = @StateName, [SortOrder] = @SortOrder WHERE [StateID] = @StateID">
            <DeleteParameters>
                <asp:Parameter Name="StateID" Type="Byte" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="StateName" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Byte" />
                <asp:Parameter Name="StateID" Type="String" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="StateName" Type="String" />
                <asp:Parameter Name="SortOrder" Type="Byte" />
            </InsertParameters>
        </asp:SqlDataSource>
    </CSCMSUC:sectioncontainer>
    <!--  -->
    <!--  -->
    <!-- sectionTicklerType -->
    <CSCMSUC:SectionContainer ID="sectionTicklerType" runat="server" HeaderText="Tickler Type"
        Width="100%" DisplayType="Grid" Visible="false">
        <telerik:RadGrid ID="grdTicklerType" runat="server" DataSourceID="dsTicklerType"
            GridLines="None" AllowPaging="True" PageSize="50" AllowAutomaticUpdates="True"
            AllowAutomaticInserts="True" AllowAutomaticDeletes="true" OnItemUpdated="grd1_ItemUpdated"
            OnItemInserted="grd1_ItemInserted" OnItemDeleted="grd1_ItemDeleted" AllowSorting="true"
            Width="750px" OnItemCreated="grdTicklerType_ItemCreated">
            <PagerStyle Mode="NextPrevAndNumeric" />
            <MasterTableView DataSourceID="dsTicklerType" AutoGenerateColumns="false" DataKeyNames="TicklerTypeID"
                CommandItemDisplay="Top">
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="Delete this Tickler Type?" ConfirmDialogType="RadWindow"
                        ConfirmTitle="Delete" ButtonType="ImageButton" CommandName="Delete" Text="Delete" />
                    <telerik:GridBoundColumn HeaderText="ID" DataField="TicklerTypeID" />
                    <telerik:GridBoundColumn HeaderText="Tickler" DataField="TicklerTypeName" />
                    <telerik:GridNumericColumn DataType="System.Int16" MaxLength="3" UniqueName="DelayDays"
                        SortExpression="DelayDays" HeaderText="Delay Days" DataField="DelayDays" FooterText="">
                    </telerik:GridNumericColumn>
                </Columns>
                <EditItemTemplate>
                    <br />
                </EditItemTemplate>
                <EditFormSettings>
                    <EditColumn ButtonType="ImageButton" />
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsTicklerType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
            DeleteCommand="DELETE FROM [TicklerType] WHERE [TicklerTypeID] = @TicklerTypeID"
            InsertCommand="INSERT INTO [TicklerType] ([TicklerTypeName], [DelayDays]) VALUES (@TicklerTypeName, @DelayDays)"
            SelectCommand="SELECT [TicklerTypeID], [TicklerTypeName], [DelayDays] FROM [TicklerType]"
            UpdateCommand="UPDATE [TicklerType] SET [TicklerTypeName] = @TicklerTypeName, [DelayDays] = @DelayDays WHERE [TicklerTypeID] = @TicklerTypeID">
            <DeleteParameters>
                <asp:Parameter Name="TicklerTypeID" Type="Byte" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="TicklerTypeName" Type="String" />
                <asp:Parameter Name="DelayDays" Type="Int16" />
                <asp:Parameter Name="TicklerTypeID" Type="Byte" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="TicklerTypeName" Type="String" />
                <asp:Parameter Name="DelayDays" Type="Int16" />
            </InsertParameters>
        </asp:SqlDataSource>
    </CSCMSUC:SectionContainer>
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
</asp:Content>
