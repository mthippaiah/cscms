﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMS.master" AutoEventWireup="true"
    CodeFile="UserList.aspx.cs" Inherits="AdminPages_UserList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div class="pageTitle">
        CSCMS USERS</div>
    <CSCMSUC:SectionContainer ID="sectionUsers" runat="server" HeaderText="USERS" Width="100%"
        DisplayType="Grid">
        <telerik:RadGrid ID="grdUsers" runat="server" AllowAutomaticUpdates="true" AllowAutomaticInserts="true"
            AllowPaging="True" PageSize="10" AllowSorting="True" OnItemCommand="grdUsers_ItemCommand"
            OnNeedDataSource="grdUsers_NeedDataSource" EnableAJAX="True" OnItemDataBound="grdUsers_OnItemDataBound" >
            <MasterTableView DataKeyNames="UserId" EditMode="EditForms" CommandItemDisplay="Top">
                <CommandItemSettings AddNewRecordText="Add new user" />
                <Columns>
                    <telerik:GridTemplateColumn HeaderStyle-Width="95px">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnEdit" runat="server" Text="Edit" CommandName="edit" Font-Bold="false" />
                            &nbsp;|&nbsp;
                            <asp:LinkButton ID="btnImpersonate" runat="server" Text="Impersonate" CommandName="impersonate"
                                CommandArgument='<%# Eval("UserName") %>' Font-Bold="false" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HeaderText="User Name" DataField="UserName" />
                    <telerik:GridTemplateColumn HeaderText="Password" UniqueName="PasswordColumn">   
                            <EditItemTemplate> 
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Text='<%# Bind("password") %>' ></asp:TextBox> 
                            </EditItemTemplate>  
                            <ItemTemplate>
                                <asp:Label ID="lblPassword" runat="server" Text='<%# Eval("password") %>' BorderStyle="None" />  
                            </ItemTemplate>  
                        </telerik:GridTemplateColumn> 
                    <telerik:GridBoundColumn HeaderText="Full Name" DataField="UserFullName" />
                    <telerik:GridDropDownColumn HeaderText="User Type" DataField="UserTypeID" ListValueField="UserTypeID"
                        ListTextField="UserTypeName" DataSourceID="dsUserTypes" DropDownControlType="DropDownList" />
                    <telerik:GridDropDownColumn HeaderText="Court" DataField="OCACourtID" ListValueField="OCACourtID" 
                        ListTextField="CourtName" DataSourceID="dsCourtNames" DropDownControlType="DropDownList" />
                    <telerik:GridBoundColumn HeaderText="Email" DataField="Email" />
                    <telerik:GridBoundColumn HeaderText="Phone" DataField="Phone" />
                    <telerik:GridDateTimeColumn HeaderText="Begin Date" DataField="BeginDate" DataFormatString="{0:MM/dd/yyyy}"
                        PickerType="DatePicker" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                    <telerik:GridDateTimeColumn HeaderText="End Date" DataField="EndDate" DataFormatString="{0:MM/dd/yyyy}"
                        PickerType="DatePicker" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                </Columns>
            </MasterTableView>
            <PagerStyle AlwaysVisible="true" />
        </telerik:RadGrid>
        <asp:SqlDataSource ID="dsCourtNames" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                SelectCommand="select OCACourtID, CourtName from OCACourt order by CourtName">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsUserTypes" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                SelectCommand="select UserTypeID, UserTypeName from UserType order by SortOrder">
        </asp:SqlDataSource>
    </CSCMSUC:SectionContainer>
    <telerik:RadAjaxManager ID="ajaxManager" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="grdUsers">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="grdUsers" LoadingPanelID="loadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 80px" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
