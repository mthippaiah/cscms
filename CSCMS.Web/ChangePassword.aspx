﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMSPlain.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <table width="960" border="1" cellspacing="0" id="tblSpace" cellpadding="0" style="border-style: none solid none solid;
        border-width: 2px; border-color: #000000;" runat="server">
        <tr>
            <td align="center" style="background-color: White;">
                <table id="tblChangePassword" runat="server" align="center" border="0" cellpadding="3"
                    cellspacing="0" class="style2" width="400" frame="box" style="border: 3px outset #000099;">
                    <tr>
                        <td align="center" colspan="2" bgcolor="#C4ECFB">
                            <asp:Label ID="lblTableHeader" runat="server" Font-Bold="True" ForeColor="#000099"
                                Text="Change Password"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword"
                                Font-Bold="False">Password:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CurrentPassword" runat="server" Font-Size="Small" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword"
                                Font-Bold="False">New Password:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="NewPassword" runat="server" Font-Size="Small" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                ErrorMessage="New Password is required." ToolTip="New Password is required."
                                ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword"
                                Font-Bold="False">Confirm New Password:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" Font-Size="Small" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                Font-Bold="False" ValidationGroup="ChangePassword1"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="color: red; height: 18px;">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="text-align: center">
                            <asp:Button ID="btnChangePassword" runat="server" Width="120px" BackColor="#C4ECFB"
                                BorderStyle="None" Font-Size="Small" CssClass="submitMedium" Text="Change Password"
                                ValidationGroup="ChangePassword1" OnClick="btnChangePassword_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="text-align: center">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/MemberPages/MyPage.aspx"
                                ToolTip="Go to MyPage!" Font-Size="Small">MyPage</asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <table id="tblPasswordChangeSuccess" runat="server" visible="false">
                    <tr>
                        <td align="center" style="font-weight:bold; font-size:medium">
                            Password Change Successful
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HyperLink ID="goToMyPageLink" runat="server" NavigateUrl="~/MemberPages/MyPage.aspx"
                                ToolTip="Go to MyPage!" Font-Size="Small">MyPage</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
