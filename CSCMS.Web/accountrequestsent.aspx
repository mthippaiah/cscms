﻿<%@ Page Language="C#" MasterPageFile="~/CSCMSPlain.master" AutoEventWireup="true" CodeFile="AccountRequestSent.aspx.cs" Inherits="AccountRequestSent" Title="Request an Account Confirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<meta http-equiv="refresh" content="5;url=Login.aspx">

<table align="center" cellpadding="3" cellspacing="0" class="style2" style="border: 3px outset #000099;" width="400">
    <tr>
        <td align="center" bgcolor="#C4ECFB">
            
            <asp:Label ID="lblRequestConfirmation" runat="server" Font-Bold="True" 
                ForeColor="#000099" Text="Your CHILD SUPPORT CASE MANAGEMENT SYSTEM Account Request has been sent" 
                ToolTip="Your CHILD SUPPORT CASE MANAGEMENT SYSTEM Account Request has been sent"></asp:Label>
            
        </td>
    </tr>
    <tr>
        <td align="center">
            <br />
            You will be contacted soon for verification and <br />
            your account will activated upon validation.<br /><br />
            
            You will now be redirected to the login.<br /><br />
       </td>
    </tr>
</table>
</asp:Content>

