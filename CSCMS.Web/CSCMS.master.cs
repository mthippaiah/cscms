﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using System.Linq;
using Telerik.Web.UI;

public partial class cscms : System.Web.UI.MasterPage
{
    private DateTime pageLoad;
    private string logString;

    public string LogoutUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/AnonPages/Logout.aspx";
        }
    }


    public string WebServiceUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/Services.asmx";
        }
    }

    public string ChangePasswordUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/ChangePassword.aspx";
        }
    }

    public string ChildSupportCalculationUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/MemberPages/ChildSupportCalculation.aspx";
        }
    }

    public string HelpUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/Help/Default.htm";
        }
    }

    #region [Page]
    protected override void OnPreRender(EventArgs e)
    {

        if ((((BasePage)this.Page).CurrentCause != null) && ((BasePage)this.Page).CurrentCause.CauseNumber != null && ((BasePage)this.Page).CurrentAction != null)
        {
            mainMenu.Items.FindItemByValue("ActionSummary").Enabled = true;
            mainMenu.Items.FindItemByValue("ActionSummary").NavigateUrl = "~/MemberPages/ActionDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((BasePage)this.Page).CurrentAction.ActionNumber;
            //mainMenu.Items.FindItemByValue("Administration").Items[3].Enabled = true;
            mainMenu.Items.FindItemByValue("HearingDetail").NavigateUrl = "~/MemberPages/HearingDetail.aspx?cid=" + ((BasePage)this.Page).CurrentCause.CauseGUID + "&a=" + ((BasePage)this.Page).CurrentAction.ActionNumber;

            // display latest hearing or default to add when there is no hearing
            Guid aId = ((BasePage)this.Page).CurrentAction.ActionGUID;
            CSCMS.Data.Hearing latestHearing = ((BasePage)this.Page).CscmsContext.Hearing.Where(it => it.Action.ActionGUID == aId).OrderByDescending(it => it.HearingDate).FirstOrDefault();
            if (latestHearing != null)
                mainMenu.Items.FindItemByValue("HearingDetail").NavigateUrl += "&hid=" + latestHearing.HearingID.ToString();
        }
        else
        {
            mainMenu.Items.FindItemByValue("HearingDetail").Enabled = false;
            mainMenu.Items.FindItemByValue("ActionSummary").Enabled = false;
            //TODO: May be I think, this was intended to not allow the Admin to make changes to the
            //Tables, while a case is open.
            //mainMenu.Items.FindItemByValue("Administration").Items[3].Enabled = false;
        }

        base.OnPreRender(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (((BasePage)this.Page).CscmsContext.Connection.State != ConnectionState.Open)
            {
                ((BasePage)this.Page).CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (((BasePage)this.Page).CurrentCause != null)
            {
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(((BasePage)this.Page).SqlConn,
                    CommandType.StoredProcedure,
                    "LogCourtUserActivity",
                    new SqlParameter("@UserId", ((BasePage)this.Page).CurrentUser.UserID),
                    new SqlParameter("@OCACourtID", ((BasePage)this.Page).CurrentUser.OCACourtID),
                    new SqlParameter("@CauseGUID", ((BasePage)this.Page).CurrentCause.CauseGUID));
            }

            string sqlString = "INSERT INTO ActivityLog (ActivityText) VALUES (' " + Request.Url.Segments[Request.Url.Segments.Length - 1] + "  * D: " + Request.TotalBytes.ToString() + " * IP: " + Request.UserHostAddress.ToString() + " * U: " + ((BasePage)this.Page).CurrentUser.FullName + " * " + " * UI: " +
               (Session["Impersonating"] == null ? "false" : "true") + " * ";
            if (((BasePage)this.Page).CurrentCause != null) sqlString = sqlString + "Cause Number: " + ((BasePage)this.Page).CurrentCause.CauseNumber;
            try
            {
                if (((BasePage)this.Page).CurrentPerson != null) sqlString = sqlString + " PersonGUID: " + ((BasePage)this.Page).CurrentPerson.PersonGUID.ToString();
            }
            catch { }

            logString = sqlString;
            sqlString = sqlString + " * " + " ') ";

            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(((BasePage)this.Page).SqlConn,
                    CommandType.Text, sqlString);

        }
        catch { }
        finally
        {
            if (iOpenedConn && ((BasePage)this.Page).CscmsContext.Connection.State == ConnectionState.Open)
                ((BasePage)this.Page).CscmsContext.Connection.Close();
        }

        mainMenu.Items.FindItemByValue("Logout").NavigateUrl = LogoutUrl;
        mainMenu.Items.FindItemByValue("ChangePassword").NavigateUrl = ChangePasswordUrl;
        mainMenu.Items.FindItemByValue("Help").NavigateUrl = HelpUrl;
        HyperLink1.NavigateUrl = ChildSupportCalculationUrl;

        if (!IsPostBack)
        {
            string pageName = Request.Url.Segments[Request.Url.Segments.Length - 1].ToLower().Replace(".aspx", "");
            RadMenuItem item = mainMenu.Items.FindItemByValue(pageName, true);

            if (item != null)
            {
                item.HighlightPath();
            }

            mainMenu.Items.FindItemByValue("Administration").Visible = ((BasePage)this.Page).CurrentUser.UserType == UserType.OCAAdmin;
            mainMenu.Items.FindItemByValue("ChangePassword").Visible = !(((BasePage)this.Page).CurrentUser.UserType == UserType.OCAAdmin);

            lblUserName.Text = ((BasePage)this.Page).CurrentUser.UserName;
            lblOCACourtName.Text = ((BasePage)this.Page).CurrentUser.OCACourtName;

            btnEndImpersonation.Visible = Convert.ToBoolean(Session["Impersonating"]);

            pageLoad = DateTime.Now;
        }
    }
    #endregion

    #region [Button]
    protected void btnEndImpersonation_Click(object sender, EventArgs e)
    {
        Session["Impersonating"] = false;
        SessionHelper.ResetSessionForUserCourtChange();
        ((BasePage)this.Page).SetUser(((LoggedInUserData)Session["LoggedInUser"]).UserName);
        Response.Redirect("~/AdminPages/UserList.aspx", true);
    }
    #endregion

    protected void OnAjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        caseTree.Initialize();
    }
}
