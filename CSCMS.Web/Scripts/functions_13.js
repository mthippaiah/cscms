﻿function ToggleSection(lnkToggle) {
    var trToggle = document.getElementById(lnkToggle.id.replace(/lnkToggle/, 'trToggle'));
    var imgToggle = document.getElementById(lnkToggle.id.replace(/lnkToggle/, 'imgToggle'));
    var minMax;
    var serviceName;

    if (lnkToggle.pathname.indexOf("MemberPages/ActionDetail.aspx") != -1) {

        if (lnkToggle.id.indexOf("PageContent$fvCase$sectionCase$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "ADCSMinMax";
        }

        if (lnkToggle.id.indexOf("PageContent$fvCase$scChildSupport$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "ADChildSupportMinMax";
        }

        if (lnkToggle.id.indexOf("PageContent$fvCase$scContinuance$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "ADContinuanceMinMax";
        }
    }

    if (lnkToggle.pathname.indexOf("MemberPages/MyPage.aspx") != -1) {

        if (lnkToggle.id.indexOf("PageContent$scTicklers$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "MPTicklersMinMax";
        }

        if (lnkToggle.id.indexOf("PageContent$scCaseLoad$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "MPCaseLoadMinMax";
        }

        if (lnkToggle.id.indexOf("PageContent$scHearings$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "MPTodaysHearingsMinMax";
        }

        if (lnkToggle.id.indexOf("PageContent$scDailyCourtUserActivity$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "MPUserActivityMinMax";
        }

        if (lnkToggle.id.indexOf("PageContent$scMessage$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "MPMessagesMinMax";
        }

        if (lnkToggle.id.indexOf("PageContent$scReminders$lnkToggle") != -1) {
            minMax = "true";
            serviceName = "MPRemindersMinMax";
        }
    }

    if (trToggle.style.display == 'none') {
        trToggle.style.display = 'block';
        imgToggle.src = '../images/minus.png';
        imgToggle.alt = 'Minimize';
        if (minMax != null && minMax != undefined) {

            if (serviceName == "ADCSMinMax")
                Services.ADCSMinMax("Max");

            if (serviceName == "ADChildSupportMinMax")
                Services.ADChildSupportMinMaxMax("Max");

            if (serviceName == "ADContinuanceMinMax")
                Services.ADContinuanceMinMaxMax("Max");
                
            if (serviceName == "MPTicklersMinMax")
                Services.MPTicklersMinMax("Max");

            if (serviceName == "MPCaseLoadMinMax")
                Services.MPCaseLoadMinMax("Max");

            if (serviceName == "MPTodaysHearingsMinMax")
                Services.MPTodaysHearingsMinMax("Max");

            if (serviceName == "MPUserActivityMinMax")
                Services.MPUserActivityMinMax("Max");

            if (serviceName == "MPMessagesMinMax")
                Services.MPMessagesMinMax("Max");

            if (serviceName == "MPRemindersMinMax")
                Services.MPRemindersMinMax("Max");

        }
    }
    else {
        trToggle.style.display = 'none';
        imgToggle.src = '../images/plus.png';
        imgToggle.alt = 'Maximize';
        if (minMax != null && minMax != undefined) {

            if (serviceName == "ADCSMinMax")
                Services.ADCSMinMax("Min");

            if (serviceName == "ADChildSupportMinMax")
                Services.ADChildSupportMinMaxMax("Min");

            if (serviceName == "ADContinuanceMinMax")
                Services.ADContinuanceMinMaxMax("Min");
                
            if (serviceName == "MPTicklersMinMax")
                Services.MPTicklersMinMax("Min");

            if (serviceName == "MPCaseLoadMinMax")
                Services.MPCaseLoadMinMax("Min");

            if (serviceName == "MPTodaysHearingsMinMax")
                Services.MPTodaysHearingsMinMax("Min");

            if (serviceName == "MPUserActivityMinMax")
                Services.MPUserActivityMinMax("Min");

            if (serviceName == "MPMessagesMinMax")
                Services.MPMessagesMinMax("Min");

            if (serviceName == "MPRemindersMinMax")
                Services.MPRemindersMinMax("Min");
        }
    }

}

function CauseSearch() {
}

CauseSearch.prototype.DispRDateSelected = function(sender, args) {
    var a = createDispObjects();
    var dt = a.drdt.get_selectedDate();
    a.dsdt.set_selectedDate(dt);
}
CauseSearch.prototype.ValidateSaveNewCase = function() {
    if (Page_ClientValidate("AddNew") == false)
        return false;
    var a = createDispObjects();
    var dt = a.drdt.get_selectedDate();
    var disp = a.d.options[a.d.selectedIndex].text;
    if (dt != null && disp == '- Select -') {
        alert("Please select disposition.");
        return false;
    }
    if (disp != '- Select -' && dt == null) {
        alert("Please select disposition rendered date.");
        return false;
    }


    return true;
}

CauseSearch.prototype.ActionTypeChange = function(ddl) {
    var at = ddl.options[ddl.selectedIndex].text;
    var a = createCpObjects();
    if (at.indexOf("UIFSA") != -1)
        a.vcpnp.selectedIndex = 1;
    else
        a.vcpnp.selectedIndex = 0;
}

CauseSearch.prototype.SetPersonSearchValues = function(id, sex, fn, ln, mn, suffix, bd) {
    var hf = createHFObjects();

    if (hf.vNCP.value == "Current") {
        var a = createNcpObjects();
        if (sex == 'M') {
            a.vncpSex.selectedIndex = 0;
            a.vncpPersonType.selectedIndex = 0;
        }

        if (sex == 'F') {
            a.vncpSex.selectedIndex = 1;
            a.vncpPersonType.selectedIndex = 1;
        }

        a.vncpFn.value = fn;
        a.vncpLn.value = ln;
        a.vncpMn.value = mn;
        a.vncpSx.value = suffix;
        hf.vNCP.value = id;
        if (bd != "")
            a.vbdate.set_selectedDate(new Date(bd));

        this.DisableNCPControls(true);
    }

    if (hf.vCP.value == "Current") {
        var b = createCpObjects();
        if (sex == 'M') {
            b.vcpSex.selectedIndex = 1;
        }

        if (sex == 'F') {
            b.vcpSex.selectedIndex = 0;
        }

        b.vcpFn.value = fn;
        b.vcpLn.value = ln;
        b.vcpMn.value = mn;
        b.vcpSx.value = suffix;
        hf.vCP.value = id;
        if (bd != "")
            b.vbdate.set_selectedDate(new Date(bd));

        this.DisableCpControls(true);
    }

    if (hf.vChild1.value == "Current") {
        var c1 = createChild1Objects();
        if (sex == 'M') {
            c1.vc1Sex.selectedIndex = 0;
        }

        if (sex == 'F') {
            c1.vc1Sex.selectedIndex = 1;
        }

        c1.vc1Fn.value = fn;
        c1.vc1Ln.value = ln;
        c1.vc1Mn.value = mn;
        c1.vc1Sx.value = suffix;
        hf.vChild1.value = id;
        if (bd != "")
            c1.vbdate.set_selectedDate(new Date(bd));

        this.DisableChild1Controls(true);
    }

    if (hf.vChild2.value == "Current") {
        var c2 = createChild2Objects();
        if (sex == 'M') {
            c2.vc2Sex.selectedIndex = 0;
        }

        if (sex == 'F') {
            c2.vc2Sex.selectedIndex = 1;
        }

        c2.vc2Fn.value = fn;
        c2.vc2Ln.value = ln;
        c2.vc2Mn.value = mn;
        c2.vc2Sx.value = suffix;
        hf.vChild2.value = id;
        if (bd != "")
            c2.vbdate.set_selectedDate(new Date(bd));

        this.DisbaleChild2Controls(true);
    }

    if (hf.vChild3.value == "Current") {
        var c3 = createChild3Objects();
        if (sex == 'M') {
            c3.vc3Sex.selectedIndex = 0;
        }

        if (sex == 'F') {
            c3.vc3Sex.selectedIndex = 1;
        }

        c3.vc3Fn.value = fn;
        c3.vc3Ln.value = ln;
        c3.vc3Mn.value = mn;
        c3.vc3Sx.value = suffix;
        hf.vChild3.value = id;
        if (bd != "")
            c3.vbdate.set_selectedDate(new Date(bd));

        this.DisbaleChild3Controls(true);
    }

    if (hf.vChild4.value == "Current") {
        var c4 = createChild4Objects();
        if (sex == 'M') {
            c4.vc4Sex.selectedIndex = 0;
        }

        if (sex == 'F') {
            c4.vc4Sex.selectedIndex = 1;
        }

        c4.vc4Fn.value = fn;
        c4.vc4Ln.value = ln;
        c4.vc4Mn.value = mn;
        c4.vc4Sx.value = suffix;
        hf.vChild4.value = id;
        if (bd != "")
            c4.vbdate.set_selectedDate(new Date(bd));

        this.DisbaleChild4Controls(true);
    }

    if (hf.vP1.value == "Current") {
        var a = createP1Objects();
        if (sex == 'M') {
            a.vP1Sex.selectedIndex = 0;
        }

        if (sex == 'F') {
            a.vP1Sex.selectedIndex = 1;
        }

        a.vP1Fn.value = fn;
        a.vP1Ln.value = ln;
        a.vP1Mn.value = mn;
        a.vP1Sx.value = suffix;
        hf.vP1.value = id;
        if (bd != "")
            a.vbdate.set_selectedDate(new Date(bd));

        this.DisableP1Controls(true);
    }

    if (hf.vP2.value == "Current") {
        var a = createP2Objects();
        if (sex == 'M') {
            a.vP2Sex.selectedIndex = 0;
        }

        if (sex == 'F') {
            a.vP2Sex.selectedIndex = 1;
        }

        a.vP2Fn.value = fn;
        a.vP2Ln.value = ln;
        a.vP2Mn.value = mn;
        a.vP2Sx.value = suffix;
        hf.vP2.value = id;
        if (bd != "")
            a.vbdate.set_selectedDate(new Date(bd));

        this.DisableP2Controls(true);
    }

    $find('PersonMPE').hide();
    return;
}

CauseSearch.prototype.DisableNCPControls = function(disable) {
    var a = createNcpObjects();
    a.vncpSex.disabled = disable;
    a.vncpPersonType.disabled = disable;
    a.vncpFn.disabled = disable;
    a.vncpLn.disabled = disable;
    a.vncpMn.disabled = disable;
    a.vncpSx.disabled = disable;
    a.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.DisableCpControls = function(disable) {
    var b = createCpObjects();
    b.vcpSex.disabled = disable;
    b.vcpFn.disabled = disable;
    b.vcpLn.disabled = disable;
    b.vcpMn.disabled = disable;
    b.vcpSx.disabled = disable;
    b.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.DisableChild1Controls = function(disable) {
    var c1 = createChild1Objects();
    c1.vc1Sex.disabled = disable;
    c1.vc1Fn.disabled = disable;
    c1.vc1Ln.disabled = disable;
    c1.vc1Mn.disabled = disable;
    c1.vc1Sx.disabled = disable;
    c1.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.DisbaleChild2Controls = function(disable) {
    var c2 = createChild2Objects();
    c2.vc2Sex.disabled = disable;
    c2.vc2Fn.disabled = disable;
    c2.vc2Ln.disabled = disable;
    c2.vc2Mn.disabled = disable;
    c2.vc2Sx.disabled = disable;
    c2.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.DisbaleChild3Controls = function(disable) {
    var c3 = createChild3Objects();
    c3.vc3Sex.disabled = disable;
    c3.vc3Fn.disabled = disable;
    c3.vc3Ln.disabled = disable;
    c3.vc3Mn.disabled = disable;
    c3.vc3Sx.disabled = disable;
    c3.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.DisbaleChild4Controls = function(disable) {
    var c4 = createChild4Objects();
    c4.vc4Sex.disabled = disable;
    c4.vc4Fn.disabled = disable;
    c4.vc4Ln.disabled = disable;
    c4.vc4Mn.disabled = disable;
    c4.vc4Sx.disabled = disable;
    c4.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.DisableP1Controls = function(disable) {
    var a = createP1Objects();
    a.vP1Sex.disabled = disable;
    a.vP1Fn.disabled = disable;
    a.vP1Ln.disabled = disable;
    a.vP1Mn.disabled = disable;
    a.vP1Sx.disabled = disable;
    a.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.DisableP2Controls = function(disable) {
    var a = createP2Objects();
    a.vP2Sex.disabled = disable;
    a.vP2Fn.disabled = disable;
    a.vP2Ln.disabled = disable;
    a.vP2Mn.disabled = disable;
    a.vP2Sx.disabled = disable;
    a.vbdate.set_enabled(!disable);
}

CauseSearch.prototype.Clear = function(btn) {
    var hf = createHFObjects();
    if (btn.id.indexOf("btnClearNCP") != -1) {
        hf.vNCP.value = "";
        a = createNcpObjects();
        a.vncpSex.selectedIndex = 0;
        a.vncpPersonType.selectedIndex = 0;
        a.vncpFn.value = "";
        a.vncpLn.value = "";
        a.vncpMn.value = "";
        a.vncpSx.value = "";
        a.vbdate.clear();
        this.DisableNCPControls(false);
    }

    if (btn.id.indexOf("btnClearCP") != -1) {
        hf.vCP.value = "";
        b = createCpObjects();
        b.vcpSex.selectedIndex = 1;
        b.vcpFn.value = "";
        b.vcpLn.value = "";
        b.vcpMn.value = "";
        b.vcpSx.value = "";
        b.vbdate.clear();
        this.DisableCpControls(false);
    }

    if (btn.id.indexOf("btnClearChild1") != -1) {
        hf.vChild1.value = "";
        c1 = createChild1Objects();
        c1.vc1Sex.selectedIndex = 0;
        c1.vc1Fn.value = "";
        c1.vc1Ln.value = "";
        c1.vc1Mn.value = "";
        c1.vc1Sx.value = "";
        c1.vbdate.clear();
        this.DisableChild1Controls(false);
    }

    if (btn.id.indexOf("btnClearChild2") != -1) {
        hf.vChild2.value = "";
        c2 = createChild2Objects();
        c2.vc2Sex.selectedIndex = 0;
        c2.vc2Fn.value = "";
        c2.vc2Ln.value = "";
        c2.vc2Mn.value = "";
        c2.vc2Sx.value = "";
        c2.vbdate.clear();
        this.DisbaleChild2Controls(false);
    }

    if (btn.id.indexOf("btnClearChild3") != -1) {
        hf.vChild3.value = "";
        c3 = createChild3Objects();
        c3.vc3Sex.selectedIndex = 0;
        c3.vc3Fn.value = "";
        c3.vc3Ln.value = "";
        c3.vc3Mn.value = "";
        c3.vc3Sx.value = "";
        c3.vbdate.clear();
        this.DisbaleChild3Controls(false);
    }

    if (btn.id.indexOf("btnClearChild4") != -1) {
        hf.vChild4.value = "";
        c4 = createChild4Objects();
        c4.vc4Sex.selectedIndex = 0;
        c4.vc4Fn.value = "";
        c4.vc4Ln.value = "";
        c4.vc4Mn.value = "";
        c4.vc4Sx.value = "";
        c4.vbdate.clear();
        this.DisbaleChild4Controls(false);
    }

    if (btn.id.indexOf("btnClearP1") != -1) {
        hf.vP1.value = "";
        a = createP1Objects();
        a.vP1Sex.selectedIndex = 0;
        a.vP1Fn.value = "";
        a.vP1Ln.value = "";
        a.vP1Mn.value = "";
        a.vP1Sx.value = "";
        a.vbdate.clear();
        this.DisableP1Controls(false);
    }

    if (btn.id.indexOf("btnClearP2") != -1) {
        hf.vP2.value = "";
        a = createP2Objects();
        a.vP2Sex.selectedIndex = 0;
        a.vP2Fn.value = "";
        a.vP2Ln.value = "";
        a.vP2Mn.value = "";
        a.vP2Sx.value = "";
        a.vbdate.clear();
        this.DisableP2Controls(false);
    }
}

CauseSearch.prototype.ShowSearchWindow = function(obj) {
    var hf = createHFObjects();

    if (hf.vChild2.value == "Current")
        hf.vChild2.value = "";

    if (hf.vChild1.value == "Current")
        hf.vChild1.value = "";

    if (hf.vChild3.value == "Current")
        hf.vChild3.value = "";

    if (hf.vChild4.value == "Current")
        hf.vChild4.value = "";

    if (hf.vCP.value == "Current")
        hf.vCP.value = "";

    if (hf.vNCP.value == "Current")
        hf.vNCP.value = "";

    if (hf.vP1.value == "Current")
        hf.vP1.value = "";

    if (hf.vP2.value == "Current")
        hf.vP2.value = "";

    if (obj.id.indexOf("btnNCPPersonSearch") != -1) {
        hf.vNCP.value = "Current";
    }

    if (obj.id.indexOf("btnCPPersonSearch") != -1) {
        hf.vCP.value = "Current";
    }

    if (obj.id.indexOf("btnChild1PersonSearch") != -1) {
        hf.vChild1.value = "Current";
    }

    if (obj.id.indexOf("btnChild2PersonSearch") != -1) {
        hf.vChild2.value = "Current";
    }

    if (obj.id.indexOf("btnChild3PersonSearch") != -1) {
        hf.vChild3.value = "Current";
    }

    if (obj.id.indexOf("btnChild4PersonSearch") != -1) {
        hf.vChild4.value = "Current";
    }

    if (obj.id.indexOf("btnP1Search") != -1) {
        hf.vP1.value = "Current";
    }

    if (obj.id.indexOf("btnP2Search") != -1) {
        hf.vP2.value = "Current";
    }

    $find('PersonMPE').show();
    return false;

}

function PersonDetail() {
}

PersonDetail.prototype.SelectNPFlag = function(ddlcust) {
    var a = createMiscObjects();
    if (ddlcust.selectedIndex != 0 && a.npCheck.checked == false) {
        a.npCheck.checked = true;
        this.NPChanged(a.npCheck);
    }
}

PersonDetail.prototype.CounselStatusChange = function(ddlCS) {
    var a = createMiscObjects();
    if (ddlCS.selectedIndex != 0)
        a.napp.checked = false;
}

PersonDetail.prototype.NPChanged = function(npCkb) {
    var a = createNpOnlyObjects();
    if (npCkb.checked == true) {
        a.trcd.style.display = "block";
        a.trcs.style.display = "block";
        a.trdfi.style.display = "block";
        a.trih.style.display = "block";
        a.trnaa.style.display = "block";
        a.trrat.style.display = "block";
    }
    else {
        a.trcd.style.display = "none";
        a.trcs.style.display = "none";
        a.trdfi.style.display = "none";
        a.trih.style.display = "none";
        a.trnaa.style.display = "none";
        a.trrat.style.display = "none";
    }
}

PersonDetail.prototype.ShowSearchWindow = function(obj) {
    var temp = createPersonDetailPersonSearchObjects();
    if (temp.vhfperson.value == "Current")
        temp.vhfperson.value = "";

    if (obj.id.indexOf("btnPersonSearch") != -1) {
        temp.vhfperson.value = "Current";
    }
    $find('PersonMPE').show();
    return false;

}

PersonDetail.prototype.ChangeSelectByValue = function(ddl, value) {
    for (var i = 0; i < ddl.options.length; i++) {
        if (ddl.options[i].value == value) {
            ddl.selectedIndex = i;
            break;
        }
    }
}

PersonDetail.prototype.SetPersonSearchValues = function(id, sex, fn, ln, mn, suffix, bd, intp, fl, secR, dis) {
    var a = createPersonDetailPersonSearchObjects();
    if (a.vhfperson.value == "Current") {
        if (sex == 'M') {
            a.vsex.selectedIndex = 1;
        }

        if (sex == 'F') {
            a.vsex.selectedIndex = 2;
        }

        a.vfn.value = fn;
        a.vln.value = ln;
        a.vmn.value = mn;
        a.vsuffix.value = suffix;
        a.vintp.checked = intp;
        a.vsecr.checked = secR;
        a.vdis.checked = dis;

        this.ChangeSelectByValue(a.vfl, fl);

        if (bd != "")
            a.vbdate.set_selectedDate(new Date(bd));

        a.vhfperson.value = id;

        this.DisableControls(true);
    }
    $find('PersonMPE').hide();
    return;
}

PersonDetail.prototype.Clear = function(btn) {
    var a = createPersonDetailPersonSearchObjects();
    a.vhfperson.value = "";
    a.vsex.selectedIndex = 0;
    a.vfn.value = "";
    a.vln.value = "";
    a.vmn.value = "";
    a.vsuffix.value = "";
    a.vintp.checked = false;
    a.vsecr.checked = false;
    a.vdis.checked = false;
    a.vfl.selectedIndex = 0;
    a.vbdate.clear();

    this.DisableControls(false);

    return false;
}

PersonDetail.prototype.DisableControls = function(disable) {
    var a = createPersonDetailPersonSearchObjects();
    a.vsex.disabled = disable;
    a.vfn.disabled = disable;
    a.vln.disabled = disable;
    a.vmn.disabled = disable;
    a.vsuffix.disabled = disable;
    a.vintp.disabled = disable;
    a.vsecr.disabled = disable;
    a.vdis.disabled = disable;
    a.vfl.disabled = disable;
    a.vbdate.set_enabled(!disable);
}

PersonDetail.prototype.WaivedRightChanged = function(chkWaivedRightToCounsel) {
    var a = waiveRightChangedObjects();
    a.dtOrderDate.disabled = chkWaivedRightToCounsel.checked;
    a.chkTestPositive.disabled = chkWaivedRightToCounsel.checked;
    a.chkIsExcluded.disabled = chkWaivedRightToCounsel.checked;
    a.dtPaternityEstablishedDate.disabled = chkWaivedRightToCounsel.checked;

    a.dtOrderDate.get_textBox().disabled = chkWaivedRightToCounsel.checked;
    a.dtOrderDate.get_popupButton().disabled = chkWaivedRightToCounsel.checked;

    a.dtPaternityEstablishedDate.get_textBox().disabled = chkWaivedRightToCounsel.checked;
    a.dtPaternityEstablishedDate.get_popupButton().disabled = chkWaivedRightToCounsel.checked;

    a.dtResultDate.get_textBox().disabled = chkWaivedRightToCounsel.checked;
    a.dtResultDate.get_popupButton().disabled = chkWaivedRightToCounsel.checked;
}

function ChildSupportCalculation() {
}

ChildSupportCalculation.prototype.ResetMonthlyPaymentCalculation = function() {
    var _A = createMonthlyPaymentCalculationObjects();
    _A.grossWages.clear();
    _A.specialWages.clear();
    _A.monthlyHCost.clear();
    _A.adjAmount.clear();
    _A.calcAmountLabel.innerHTML = "";
    var itm = _A.cbEmployed.findItemByValue('Select Value');
    itm.select();
    itm = _A.cbAdjChildren.findItemByValue('Select Value');
    itm.select();
    itm = _A.cbOtherChildren.findItemByValue('Select Value');
    itm.select();
    return false;
}
ChildSupportCalculation.prototype.ResetArrearsCalculation = function() {
    var _A = createArrearsCalculationObjects()
    _A.arrears.clear();
    _A.interestRate.clear();
    _A.terms.clear();
    _A.RadNumericTextBoxAdjustedAmount2.clear();
    _A.calcAmountLabel.innerHTML = "";
    return false;
}
ChildSupportCalculation.prototype.ResetMedicalPastDueCalculation = function() {
    var _A = createMedicalPastDueCalculationObjects();
    _A.arrears.clear();
    _A.interestRate.clear();
    _A.terms.clear();
    _A.RadNumericTextBoxAdjustedAmount3.clear();
    _A.calcAmountLabel.innerHTML = "";
    return false;
}
ChildSupportCalculation.prototype.CalculateArrears = function() {
    //debugger;
    if (Page_ClientValidate('SUPPORTCALCULATION2') == true) {
        var _A = createCalculateArrearsObjects();
        var v1 = _A.arrears.get_value();
        var v2 = _A.interestRate.get_value();
        var v3 = _A.terms.get_value();
        var v4 = _A.RadNumericTextBoxAdjustedAmount2.GetValue();
        if (v3 == 0 && v4 != 0) {
            var e1 = v1 / v4;
            var e2 = (v1 * v2 * e1) / (100 * 12 * v4)
            var e3 = (v1 * v2 * e2) / (100 * 12 * v4)
            var e4 = (v1 * v2 * e3) / (100 * 12 * v4)
            var e5 = (v1 * v2 * e4) / (100 * 12 * v4)
            var e6 = (v1 * v2 * e5) / (100 * 12 * v4)
            var e7 = (v1 * v2 * e6) / (100 * 12 * v4)
            var e8 = (v1 * v2 * e7) / (100 * 12 * v4)
            var e9 = (v1 * v2 * e8) / (100 * 12 * v4)
            var e10 = (v1 * v2 * e9) / (100 * 12 * v4)
            e1 = e1 + e2 + e3 + e4 + e5 + e6 + e6 + e8 + e9 + e10;
            _A.terms.SetValue(e1.toFixed(0));
            var result1 = (v1 * v2 * e1.toFixed(0)) / (100 * 12);
            result1 = (result1 + v1) / e1.toFixed(0);
            _A.calcAmountLabel.innerHTML = result1.toFixed(2);

        }
        else {
            var result = (v1 * v2 * v3) / (100 * 12);
            result = (result + v1) / v3;
            _A.calcAmountLabel.innerHTML = result.toFixed(2);
            _A.RadNumericTextBoxAdjustedAmount2.SetValue(result);
        }

    }
    return false;
}
ChildSupportCalculation.prototype.CalculatePastDueAmount = function() {
    //debugger;
    if (Page_ClientValidate('SUPPORTCALCULATION3') == true) {
        var _A = createCalculatePastDueAmountObjects();
        var v1 = _A.arrears.get_value();
        var v2 = _A.interestRate.get_value();
        var v3 = _A.terms.get_value();
        var v4 = _A.RadNumericTextBoxAdjustedAmount3.GetValue();
        if (v3 == 0 && v4 != 0) {
            var e1 = v1 / v4;
            var e2 = (v1 * v2 * e1) / (100 * 12 * v4)
            var e3 = (v1 * v2 * e2) / (100 * 12 * v4)
            var e4 = (v1 * v2 * e3) / (100 * 12 * v4)
            var e5 = (v1 * v2 * e4) / (100 * 12 * v4)
            var e6 = (v1 * v2 * e5) / (100 * 12 * v4)
            var e7 = (v1 * v2 * e6) / (100 * 12 * v4)
            var e8 = (v1 * v2 * e7) / (100 * 12 * v4)
            var e9 = (v1 * v2 * e8) / (100 * 12 * v4)
            var e10 = (v1 * v2 * e9) / (100 * 12 * v4)
            e1 = e1 + e2 + e3 + e4 + e5 + e6 + e6 + e8 + e9 + e10;
            _A.terms.SetValue(e1.toFixed(0));
            var result1 = (v1 * v2 * e1.toFixed(0)) / (100 * 12);
            result1 = (result1 + v1) / e1.toFixed(0);
            _A.calcAmountLabel.innerHTML = result1.toFixed(2);
        }
        else {
            var result = (v1 * v2 * v3) / (100 * 12);
            result = (result + v1) / v3;
            _A.calcAmountLabel.innerHTML = result.toFixed(2);
            _A.RadNumericTextBoxAdjustedAmount3.SetValue(result);
        }
        //            var result = (v1 * v2 * v3) / (100 * 12);
        //            result = (result + v1) / v3;
        //            RadNumericTextBoxAdjustedAmount3.SetValue(result);
        //            calcAmountLabel.innerHTML = result.toFixed(2);
    }
    return false;
}

ChildSupportCalculation.prototype.ValidateSections = function() {
    //debugger;
    if (Page_ClientValidate('SUPPORTCALCULATION') == true && Page_ClientValidate('SUPPORTCALCULATION2') == true && Page_ClientValidate('SUPPORTCALCULATION3') == true) {
        return true;
    }
    else {
        return false;
    }
}

function ActionDetail() {
}

ActionDetail.prototype.EnableCaseControls = function() {
    var a = createCaseInfoObjects();
    a.act.disabled = false;
    a.county.disabled = false;
    a.cause.disabled = false;
    a.style.disabled = false;
    a.oagU.disabled = false;
    a.oag1.disabled = false;
    a.oag2.disabled = false;
    a.oag3.disabled = false;
    a.orgC.disabled = false;
    a.fd.set_enabled(true);
    a.act.focus();
}

ActionDetail.prototype.CalculateTotal = function() {
    var a = createADCSObjects();
    var vPP1 = a.txtPP1.get_value();
    var vPP2 = a.txtPP2.get_value();
    var vMS1 = a.txtMS1.get_value();
    var vMS2 = a.txtMS2.get_value();
    var tot = vPP1 + vPP2 + vMS1 + vMS2;
    a.txtTotal.SetValue(tot);
    return false;
}

ActionDetail.prototype.ValidateSaveCase = function() {
    //debugger;
    if (Page_ClientValidate("CaseSave") == false)
        return false;

    var a = createADObjects();
    var selectedDate = a.dtDispRDate.get_selectedDate();
    var disposition = a.ddlDisp.options[a.ddlDisp.selectedIndex].text;
    if (a.isAdmin.value != "OCAAdmin" && disposition == 'System Admin Closure') {
        if (a.hfODisp.value != 'System Admin Closure') {
            alert("Please select a different disposition.");
            return false;
        }
    }

    if (selectedDate != null && disposition == '- Select -') {
        alert("Please select disposition.");
        return false;
    }
    if (disposition != '- Select -' && selectedDate == null) {
        alert("Please select disposition rendered date.");
        return false;
    }
    return true;
}

ActionDetail.prototype.DispRDateSelected = function(sender, args) {
    var a = createADObjects();
    var dt = a.dtDispRDate.get_selectedDate();
    a.dtMedical.set_selectedDate(dt);
    a.dtArrears.set_selectedDate(dt);
    a.dtDispSDate.set_selectedDate(dt);
}

ActionDetail.prototype.ArrAsOfDateSelected = function(sender, args) {
    var a = createADObjects();
    var dt = a.dtArrears.get_selectedDate();
    a.dtMedical.set_selectedDate(dt);
}

function DocumentDetail() {
}

DocumentDetail.prototype.verifyactivex = function() {
    var a = createDDObjects();
    try {
        if (a.fu == null && a.fux != null) {
            var obj = new ActiveXObject("FileUploadActiveX.FileUploadX");
            if (obj == null) {
                this.DisplayFileUploadSetup(a.id);
            }
        }
    }
    catch (Err) {
        this.DisplayFileUploadSetup(a.id);
    }
}

DocumentDetail.prototype.DisplayFileUploadSetup = function(id) {
    id.style.display = 'block';
}

DocumentDetail.prototype.UploadFiles = function() {
    var a = createDDObjects();
    try {

        if (Page_ClientValidate("SaveDocument") == false)
            return false;

        if (a.fu == null && (a.fux == null || a.fux == undefined || a.fux.visible == "False"))  //This is because in edit mode both are not available
            return true;

        if (a.fu != null)  //This is because may be it is a non-IE browser.
            return true;

        if (a.fux != null) {
            var q = a.fux.GetFile();
            if (q != null || q != undefined) {
                var xml = '<data>' + this.element('UserID', a.uid.value) +
                        this.element('ActionGUID', a.aid.value) + '</data>';
                a.fux.SetDocumentMetaData(xml);
                a.fux.Upload();
                var le = a.fux.GetLastError();
                if (le != null) {
                    alert(le);
                    return false;
                }
                return true;
            }
        }
    }
    catch (Err) {
        alert(Err.description);
    }

    return true;
}

DocumentDetail.prototype.element = function(name, content) {
    var xml
    if (!content) {
        xml = '<' + name + '/>'
    }
    else {
        xml = '<' + name + '>' + content + '</' + name + '>'
    }
    return xml
}

function HearingDetail() {
}

HearingDetail.prototype.HearingTimeSelected = function(sender, eventArgs) {
    var a = createHDObjects();
    a.note.focus();
}

HearingDetail.prototype.ValidateSaveHearing = function() {
    if (Page_ClientValidate("HearingSave") == false)
        return false;
    var a = createHDObjects();
    var dt = a.dtDispRDate.get_selectedDate();
    var disp = a.ddlDisp.options[a.ddlDisp.selectedIndex].text;
    if (dt != null && disp == '- Select -') {
        alert("Please select disposition.");
        return false;
    }
    if (disp != '- Select -' && dt == null) {
        alert("Please select disposition rendered date.");
        return false;
    }
    return true;
}

HearingDetail.prototype.DispRDateSelected = function(sender, args) {
    var a = createHDObjects();
    var dt = a.dtDispRDate.get_selectedDate();
    a.dtDispSDate.set_selectedDate(dt);
}

function MyPage() {
}

MyPage.prototype.RadAjaxManagerRequestStarted = function(ajaxManager, eventArgs) {
    if (eventArgs.get_eventTarget().indexOf("btnExportActivity") != -1)
        eventArgs.set_enableAjax(false);

    if (eventArgs.get_eventTarget().indexOf("btnExportHearings") != -1)
        eventArgs.set_enableAjax(false);

    if (eventArgs.get_eventTarget().indexOf("btnExporTickler") != -1)
        eventArgs.set_enableAjax(false);

}

MyPage.prototype.AjaxPanelRequestStart = function(ajaxManager, eventArgs) {
    if (eventArgs.get_eventTarget().indexOf("btnExporTickler") != -1)
        eventArgs.set_enableAjax(false);
}

function Calendar() {
}

Calendar.prototype.AppointmentEditing = function(sender, eventArgs) {
    var causeGuid = eventArgs.get_appointment().get_attributes().getAttribute('CauseGUID');
    var actionNumber = eventArgs.get_appointment().get_attributes().getAttribute('ActionNumber');
    var hearingID = eventArgs.get_appointment().get_id();
    var countyName = eventArgs.get_appointment().get_attributes().getAttribute('County');
    if (causeGuid == "00000000-0000-0000-0000-000000000000") {
        document.location.href = 'Calendar.aspx';
    }
    else if (hearingID == 0) {
        document.location.href = 'Calendar.aspx';
    }

    else {
        document.location.href = 'HearingDetail.aspx?cid=' + causeGuid + '&a=' + actionNumber + '&hid=' + hearingID;
    }
    eventArgs.set_cancel(true);
}

Calendar.prototype.AppointmentMoveStart = function(sender, eventArgs) {
    var causeGuid = eventArgs.get_appointment().get_attributes().getAttribute('CauseGUID');
    if (causeGuid == "00000000-0000-0000-0000-000000000000") {
        eventArgs.set_cancel(true);
    }
}

function PersonSearchControl() {
}

PersonSearchControl.prototype.ValidateSearchInput = function(sender, args) {
    var a = createPSCobjects();
    if (a.fn.value == "" && a.ln.value == "" && a.sex.selectedIndex == 0 && a.bdate.get_selectedDate() == null)
        args.IsValid = false;
    else
        args.IsValid = true;
}

PersonSearchControl.prototype.SetSelectedRowValues = function(targetPage) {
    var a = createPSCobjects();
    var MasterTable = a.grid.get_masterTableView();
    var targetObject = null;
    if (targetPage == "CauseSearch")
        targetObject = new CauseSearch();
    if (targetPage == "PersonDetail")
        targetObject = new PersonDetail();
    var selectedRows = MasterTable.get_selectedItems();
    for (var i = 0; i < selectedRows.length; i++) {
        var row = selectedRows[i];
        var id = MasterTable.getCellByColumnUniqueName(row, "PersonGUID");
        var fn = MasterTable.getCellByColumnUniqueName(row, "FirstName");
        var ln = MasterTable.getCellByColumnUniqueName(row, "LastName");
        var mn = MasterTable.getCellByColumnUniqueName(row, "MiddleName");
        var bd = MasterTable.getCellByColumnUniqueName(row, "Birthdate");
        var suffix = MasterTable.getCellByColumnUniqueName(row, "NameSuffix");
        var sex = MasterTable.getCellByColumnUniqueName(row, "Sex");
        if (bd.innerText == " ")
            bd.innerText = "";
        if (a.hfFD.value == "false")
            targetObject.SetPersonSearchValues(id.innerHTML, sex.innerText, fn.innerText, ln.innerText, mn.innerText, suffix.innerText, bd.innerText);
        else {
            var intp = MasterTable.getCellByColumnUniqueName(row, "InterpreterNeeded");
            var fl = MasterTable.getCellByColumnUniqueName(row, "FirstLanguage");
            var secR = MasterTable.getCellByColumnUniqueName(row, "SecurityRisk");
            var dis = MasterTable.getCellByColumnUniqueName(row, "Disability");
            targetObject.SetPersonSearchValues(id.innerHTML, sex.innerText, fn.innerText, ln.innerText, mn.innerText, suffix.innerText, bd.innerText, intp.innerText, fl.innerText, secR.innerText, dis.innerText);
        }
        return false;
    }
}
