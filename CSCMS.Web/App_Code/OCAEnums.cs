﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OCAEnums
/// </summary>
/// 




public enum UserType
{
    OCAAdmin = 1,
    OCAUser = 2,
    Judge = 3,
    Coordinator = 4,
    ReadOnly = 5
}

public enum ConfirmMessage
{
    NewCaseConfirm = 1,
    MyPageDeleteAllTicklerConfirm = 2,
    DeleteActionConfirm = 3,
    DeletePersonConfirm = 4,
    ChangeToNonConservatorship = 5,
    DeleteCaseConfirm = 6
}






public enum NoteTypeEnum
{
    CaseNote = 1,
    SystemGeneratedNote = 14
}

/// <summary>
/// 
/// </summary>
/// 

public enum PersonTypeEnum
{
    Mother = 1,
    Father = 2,
    Child = 3,
    GrandMother = 4,
    GrandFather = 5,
    Relative = 6,
    Other = 7,
    Attorney = 8,
    GuardianadItem = 9
}

public enum PersonCategory
{
    Child = 1,
    Respondent = 2,
    CaseAttorney = 3

}

public enum PersonRelationshipTypeEnum
{
    ClientAttorney = 7
}

public enum ServiceMethod
{
    PersonalService = 1,
    SiteByPublication = 2,
    Certified = 3,
    PaternityRegistry = 4,
    Substitute = 5,
    Waived = 6,
    Appearance = 7,
    ProSeAnswer = 8
}

public enum ActionTypeEnum
{
    MotionForJudgment = 15,
    MotiontoEnforce = 4,
    MotiontoModify = 2,
    MotionToRevoke = 20,
    UIFSAEnforcement = 8,
    MotionToTransfer = 21
}

public sealed class MyConsts
{
    private MyConsts() { }

    // Reports unique code
    public static readonly string QUERY_RID = "rid";
    public static readonly string CHILDSUPPORTDOCKETPUBLIC = @"EA6043B0-700E-4AA8-9F80-0008AA8DE9F1";
    public static readonly string CHILDSUPPORTDOCKET = @"76B76F66-4897-42FB-BCF9-01E0013206C8";
    public static readonly string PENDINGCASESWITHAGES = @"B878CDB9-813C-4529-9E4E-003C63C9F6A1";
    public static readonly string CASESLACKINGSERVICE = @"434CA007-0D08-4EE4-8188-000063A00AFE";
    public static readonly string SUMMARYREPORTONEXPEDITEDPROCESSCOMPLIANCE = @"908BA37C-6E58-404B-BF41-FAE24AA345F8";
    public static readonly string GENERALCOURTSTATISTICSCHILDSUPPORT = @"23ADD3BE-D493-47E1-AD3F-139017032CF6";
    public static readonly string CHILDSUPPORTDOCKET_2 = @"0B163DF7-B80D-4C84-A4E0-708F70CBFD3E";
    public static readonly string CHILDSUPPORTDOCKETWITHPAGEBREAK = @"DB8DA0B9-3BEA-4560-A542-6DD91BE2D069";
    public static readonly string PENDINGCASEAGEINDAYSANDPENDINGRATE = @"57D7E620-2E2C-43BC-9F7B-78A255288060";
    public static readonly string OUTSTANDING_CAPIAS = @"B4AE6D51-C07F-4dc4-AD07-10DC21E52B56";
    public static readonly string INCARCERATED_PARTIES = @"5393C559-9946-46e7-94EE-F150952E3D98";

    //grid operations
    public static readonly string GRID_DELETE = "Delete";
    public static readonly string GRID_INSERT = "PerformInsert";
    public static readonly string GRID_UPDATE = "Update";


    //Database column constants

    public static readonly string DB_SORT_ORDER = "SortOrder";
    public static readonly string DB_ISACTIVE = "IsActive";

    public static readonly string DB_DOCTYPE_ID = "DocumentTypeID";
    public static readonly string DB_DOCTYPE_NAME = "DocumentTypeName";

    public static readonly string DB_INTPL_NAME = "IntepreterLanguageName";
    public static readonly string DB_INTPL_ID = "IntepreterLanguageID";

    public static readonly string DB_DISPTYPE_NAME = "DispositionTypeName";
    public static readonly string DB_DISPTYPE_ID = "DispositionTypeID";

    public static readonly string DB_ACTIONTYPE_NAME = "ActionTypeName";
    public static readonly string DB_ISEXPEDITE = "IsExpedite";
    public static readonly string DB_ACTIONTYPE_ID = "ActionTypeID";

    public static readonly string DB_SELF_EMPLOYED = "Self-Employed";
    public static readonly string DB_EMPLOYED = "Employed";

    public static readonly string DB_ACTIONCAT_NAME = "ActionCategoryName";
    public static readonly string DB_ACTIONCAT_ID = "ActionCategoryID";

    public static readonly string DB_COUNTYNAME = "CountyName";
    public static readonly string DB_COUNTYID = "CountyID";

    public static readonly string DB_CAL_COLOR = "CalendarColor";
   

    //Special Order Types
    public static readonly string DB_CAPIAS_DEFERRED_COMMITMENT = "Capias - Deferred Commitment";
    public static readonly string DB_CAPIAS_MTRP = "Capias - MTRP";
    public static readonly string DB_CAPIAS_FTA = "Capias - FTA";
    public static readonly string DB_CAPIAS_ATTACHMENT = "Capias - Attachment";
    public static readonly string DB_CAPIAS_COMPLIANCE = "Capias – Compliance";
    public static readonly string DB_COMMITMENT = "Commitment";
    public static readonly string DB_CAPIAS_WITHDRAWN = "Capias - Withdrawn";
    public static readonly string DB_WRIT_WITHDRAWN = "Writ Withdrawn";
    public static readonly string DB_SYSTEM_ADMIN_CLOSURE = "System Admin Closure";
    public static readonly string DB_NONE_CASE_OPEN = "None - Case Open";
    public static readonly string DB_PATERNITY_TEST = "Paternity Test";
}

