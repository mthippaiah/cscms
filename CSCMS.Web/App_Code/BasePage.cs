using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Web.UI;
using System.Data.Linq;
using CSCMS.Data;
using System.Data.EntityClient;

/// <summary>
/// This Page class is common to all sample pages and exists as a place to implement common functionality.
/// </summary>
public class BasePage : System.Web.UI.Page
{
    CscmsEntities context = new CscmsEntities();

    #region [Properties]

    public CscmsEntities CscmsContext
    {
        get
        {
            return context;
        }
    }

    public SqlConnection SqlConn
    {
        get
        {
            return (SqlConnection)((EntityConnection)CscmsContext.Connection).StoreConnection;
        }
    }

    public OCAUser CurrentUser
    {
        get
        {
            if (Session["CurrentUser"] == null)
            {
                if (Session["LoggedInUser"] == null) Response.Redirect(Page.Request.ApplicationRoot() + "/AnonPages/Login.aspx", true);

                SetUser();
            }

            return Session["CurrentUser"] as OCAUser;
        }
    }

    private CSCMS.Data.Cause currentCause;

    public CSCMS.Data.Cause CurrentCause
    {
        get
        {
            if (currentCause == null && Session["CurrentCause_CauseGUID"] != null)
            {
                Guid causeGuid = (Guid)Session["CurrentCause_CauseGUID"];
                currentCause = CscmsContext.Cause.Include("OCACourt").Include("OCACourt1").Include("OriginatingCourt").Include("County").Include("County1").Include("CauseNote").Where(it => it.CauseGUID == causeGuid).FirstOrDefault();
            }

            return currentCause;
        }
        set
        {
            currentCause = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
                Session["CurrentCause_CauseGUID"] = value.CauseGUID;
            else if (value == null)
                Session["CurrentCause_CauseGUID"] = null;
        }
    }


    private CSCMS.Data.Action currentAction;
    public CSCMS.Data.Action CurrentAction
    {
        get
        {
            if (currentAction == null && Session["CurrentAction_ActionGuid"] != null)
            {
                Guid actionGuid = (Guid)Session["CurrentAction_ActionGuid"];
                currentAction = CscmsContext.Action.Include("ActionType.ActionCategory").Include("DispositionDetail").Include("DispositionType").Include("ChildSupport").Where(it => it.ActionGUID == actionGuid).FirstOrDefault();
            }
            return currentAction;
        }
        set
        {
            currentAction = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
                Session["CurrentAction_ActionGuid"] = value.ActionGUID;
            else if (value == null)
                Session["CurrentAction_ActionGuid"] = null;

        }
    }

    private CSCMS.Data.ActionPerson currentActionPerson;

    public CSCMS.Data.ActionPerson CurrentActionPerson
    {
        get
        {
            if (currentActionPerson == null && Session["CurrentActionPerson_ActionGuid"] != null && Session["CurrentActionPerson_PersonGuid"] != null)
            {
                Guid actionGuid = (Guid)Session["CurrentActionPerson_ActionGuid"];
                Guid personGuid = (Guid)Session["CurrentActionPerson_PersonGuid"];
                currentActionPerson = GetActionPerson(actionGuid, personGuid);
                this.currentPerson = this.CurrentActionPerson.Person;
            }

            return currentActionPerson;
        }
        set
        {
            currentActionPerson = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
            {
                Session["CurrentActionPerson_ActionGuid"] = value.Action.ActionGUID;
                Session["CurrentActionPerson_PersonGuid"] = value.Person.PersonGUID;
            }
            else if (value == null)
            {
                Session["CurrentActionPerson_ActionGuid"] = null;
                Session["CurrentActionPerson_PersonGuid"] = null;
            }
        }
    }

    private CSCMS.Data.Person currentPerson;

    public CSCMS.Data.Person CurrentPerson
    {
        get
        {
            if (currentPerson == null && Session["CurrentPerson_PersonGUID"] != null)
            {
                Guid pGuid = (Guid)Session["CurrentPerson_PersonGUID"];
                currentPerson = CscmsContext.Person.Include("State").Include("IntepreterLanguage").Include("IntepreterLanguage1").Include("IntepreterLanguage2").Include("AttorneyCounty").Where(it => it.PersonGUID == pGuid).FirstOrDefault();
            }
            return currentPerson;
        }
        set
        {
            currentPerson = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
                Session["CurrentPerson_PersonGUID"] = value.PersonGUID;
            else if (value == null)
                Session["CurrentPerson_PersonGUID"] = null;

        }
    }

    private CSCMS.Data.Hearing currentHearing;

    public CSCMS.Data.Hearing CurrentHearing
    {
        get
        {
            if (currentHearing == null && Session["CurrentHearing_HearingID"] != null)
            {
                long hearingId = (long)Session["CurrentHearing_HearingID"];
                currentHearing = CscmsContext.Hearing.Include("Action").Where(it => it.HearingID == hearingId).FirstOrDefault();
            }

            return currentHearing;
        }
        set
        {
            currentHearing = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
            {
                Session["CurrentHearing_HearingID"] = value.HearingID;
            }
            else if (value == null)
                Session["CurrentHearing_HearingID"] = null;

        }
    }


    private CSCMS.Data.Document currentDocument;

    public CSCMS.Data.Document CurrentDocument
    {
        get
        {
            if (currentDocument == null && Session["CurrentDocument_DocumentID"] != null)
            {
                int docId = (int)Session["CurrentDocument_DocumentID"];
                currentDocument = CscmsContext.Document.Include("Action").Include("DocumentType").Where(it => it.DocumentID == docId).FirstOrDefault();
            }

            return currentDocument;
        }
        set
        {
            currentDocument = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
                Session["CurrentDocument_DocumentID"] = value.DocumentID;
            else if (value == null)
                Session["CurrentDocument_DocumentID"] = null;

        }
    }

    private CSCMS.Data.Continuance currentContinuance;
    public CSCMS.Data.Continuance CurrentContinuance
    {
        get
        {
            if (currentContinuance == null && Session["CurrentContinuance_ContinuanceID"] != null)
            {
                int conId = (int)Session["CurrentContinuance_ContinuanceID"];
                currentContinuance = CscmsContext.Continuance.Include("Action").Include("Person").Where(it => it.ContinuanceID == conId).FirstOrDefault();
            }

            return currentContinuance;
        }
        set
        {
            currentContinuance = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
                Session["CurrentContinuance_ContinuanceID"] = value.ContinuanceID;
            else if (value == null)
                Session["CurrentContinuance_ContinuanceID"] = null;

        }
    }

    private CSCMS.Data.SpecialOrder currentSpecialOrder;
    public CSCMS.Data.SpecialOrder CurrentSpecialOrder
    {
        get
        {
            if (currentSpecialOrder == null && Session["CurrentSpecialOrder_SpecialOrderID"] != null)
            {
                int spOrId = (int)Session["CurrentSpecialOrder_SpecialOrderID"];
                currentSpecialOrder = CscmsContext.SpecialOrder.Include("ActionPerson.Action").Include("ActionPerson.Person").Include("SpecialOrderType").Where(it => it.SpecialOrderID == spOrId).FirstOrDefault();
            }

            return currentSpecialOrder;
        }
        set
        {
            currentSpecialOrder = value;
            if (value != null && value.EntityState != EntityState.Added && value.EntityState != EntityState.Detached)
                Session["CurrentSpecialOrder_SpecialOrderID"] = value.SpecialOrderID;
            else if (value == null)
                Session["CurrentSpecialOrder_SpecialOrderID"] = null;

        }
    }

    #endregion

    #region Overridden Methods

    protected override void OnUnload(EventArgs e)
    {
        if (currentCause != null)
        {
            this.CurrentCause = currentCause;
        }

        if (currentActionPerson != null)
        {
            this.CurrentActionPerson = currentActionPerson;
        }

        if (currentPerson != null)
        {
            this.CurrentPerson = currentPerson;
        }
        if (currentAction != null)
        {
            this.CurrentAction = currentAction;
        }
        base.OnUnload(e);
    }

    #endregion

    #region Public Methods

    public virtual void LoadCause(Guid causeGuid, short actionNumber)
    {
        bool iOpenedConnection = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConnection = true;
            }
            this.CurrentCause = CscmsContext.Cause.Include("OCACourt").Include("OCACourt1").Include("OriginatingCourt").Include("County").Include("County1").Include("CauseNote").Where(it => it.CauseGUID == causeGuid).FirstOrDefault();
            LoadAction(causeGuid, actionNumber);
        }
        finally
        {
            if (iOpenedConnection && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
    }

    public virtual void LoadAction(Guid causeGuid, short actionNumber)
    {
        this.CurrentAction = CscmsContext.Action.
            Include("ActionType.ActionCategory").
            Include("DispositionDetail").
            Include("ChildSupport").
            Include("DispositionType").
            Where(it => it.Cause.CauseGUID == causeGuid && it.ActionNumber == actionNumber).FirstOrDefault();

        //_action = dc.Actions.FirstOrDefault(t => t.CauseGUID == this.CauseGUID && t.ActionNumber == this.actionNumber);
        //if (_action.ActionGUID == null) _action = dc.Actions.FirstOrDefault(t => t.CauseGUID == this.CauseGUID);
    }

    public virtual void LoadPerson(Guid personGuid)
    {
        this.CurrentPerson = CscmsContext.Person.Include("State").Include("IntepreterLanguage").Include("IntepreterLanguage1").Include("IntepreterLanguage2").Include("AttorneyCounty").Where(it => it.PersonGUID == personGuid).FirstOrDefault();
    }

    private CSCMS.Data.ActionPerson GetActionPerson(Guid actionGuid, Guid personGuid)
    {
        return CscmsContext.ActionPerson.
            Include("Action").
            Include("Person").
            Include("State").
            Include("PersonType").
            Include("CustodialType").
            Include("FatherType").
            Include("GrantParentType").
            Include("Person.State").
            Include("Person.IntepreterLanguage").
            Include("Person.IntepreterLanguage1").
            Include("Person.IntepreterLanguage2").
            Include("Person.AttorneyCounty").
            Where(it => it.Action.ActionGUID == actionGuid && it.Person.PersonGUID == personGuid).FirstOrDefault();
    }

    public virtual void LoadActionPerson(Guid actionGuid, Guid personGuid)
    {
        this.CurrentActionPerson = GetActionPerson(actionGuid, personGuid);
        this.CurrentPerson = this.CurrentActionPerson.Person;
    }

    public static Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;

        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)
                return FoundCtl;
        }

        return null;
    }

    public bool IsMessageVisible(ConfirmMessage message)
    {
        CSCMS.Data.User user = CscmsContext.User.Include("Message").Where(it => it.UserId == this.CurrentUser.UserID).FirstOrDefault();
        int msgId = (int)message;
        CSCMS.Data.Message msg = user.Message.Where(it => it.MessageID == msgId).FirstOrDefault();

        if (msg != null)
            return false;
        else
            return true;
    }

    #endregion

    #region Protected Methods

    public void SetUser()
    {
        if (Session["LoggedInUser"] != null)
            SetUser(((LoggedInUserData)Session["LoggedInUser"]).UserName);
    }

    public void SetUser(string username)
    {
        // See if the UserName has logged in, yet.  If not then we are here by mistake.
        if (string.IsNullOrEmpty(username))
            return;

        bool iOpenedConn = false;
        try
        {
            OCAUser user = new OCAUser();

            // Verify that the UserName follow the same syntax that the database expects ("LDAPDomain\UserName");
            string szUserName = username.ToLower();

            user.UserName = username;

            SqlParameter p1 = new SqlParameter("@UserName", user.UserName);
            SqlParameter p2 = new SqlParameter("@UserID", SqlDbType.SmallInt, 2);
            p2.Direction = ParameterDirection.Output;

            SqlParameter p3 = new SqlParameter("@UserTypeID", SqlDbType.SmallInt, 2);
            p3.Direction = ParameterDirection.Output;

            SqlParameter p4 = new SqlParameter("@FullName", SqlDbType.NVarChar, 100);
            p4.Direction = ParameterDirection.Output;

            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(SqlConn,
            CommandType.StoredProcedure,
            "GetUser", p1, p2, p3, p4);

            if (p2.Value == DBNull.Value)
            {
                Response.Redirect("~/AnonPages/Unauthorized.aspx", true);
                return;
            }
            else
                user.UserID = (short)p2.Value;

            if (p3.Value != DBNull.Value)
                user.UserType = (UserType)((short)p3.Value);

            if (p4.Value != DBNull.Value)
                user.FullName = (string)p4.Value;

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    user.Counties.Add((short)r["CountyID"]);
                }

            }

            IQueryable<CSCMS.Data.OCACourt> queryCourt = CompiledQueryList.GetUserCourt.Invoke(CscmsContext, user.UserID);
            CSCMS.Data.OCACourt court = queryCourt.FirstOrDefault();

            if (court != null)
            {
                user.OCACourtID = court.OCACourtID;
                user.OCACourtName = court.CourtName;

                int utj = (int)UserType.Judge;

                // try to figure out the judge for this court
                IQueryable<CSCMS.Data.User> users = CompiledQueryList.GetUser.Invoke(CscmsContext, court.OCACourtID, (short)utj);
             
                var usersDistinct = users.Distinct();

                if (usersDistinct.Count() == 1)
                {
                    user.OCACourtJudge = usersDistinct.First().UserFullName;
                }
            }

            var lastLogin = CscmsContext.UserUsage.Where(t => t.User.UserId == user.UserID).OrderByDescending(t => t.CreateDate).FirstOrDefault();

            Session["lastLogin"] = "Welcome, " + (string.IsNullOrEmpty(user.FullName) ? "?" : user.FullName.Remove(user.FullName.IndexOf(" ") < 0 ? 0 : (user.FullName.IndexOf(" "))));

            if (lastLogin != null) Session["lastLogin"] = Session["lastLogin"] + ". You last logged into CSCMS on " + lastLogin.CreateDate.ToString();

            CSCMS.Data.UserUsage usage = new CSCMS.Data.UserUsage();
            usage.User = CscmsContext.User.Where(it => it.UserId == user.UserID).FirstOrDefault();
            usage.CreateDate = DateTime.Now;
            CscmsContext.AddToUserUsage(usage);
            CscmsContext.SaveChanges();
            Session["CurrentUser"] = user;
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }

    }

    #endregion
}
