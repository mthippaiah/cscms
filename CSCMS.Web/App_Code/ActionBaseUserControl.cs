﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

public class ActionBaseUserControl : BaseUserControl
{
    public new ActionBasePage Page
    {
        get { return (ActionBasePage)base.Page; }
    }

    protected void btnDocument_Command(object sender, CommandEventArgs e)
    {
        this.Page.btnDocument_Command(sender, e);
    }
}
