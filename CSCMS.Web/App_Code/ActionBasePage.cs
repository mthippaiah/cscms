﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

public class ActionBasePage : BasePage
{
    public void btnDocument_Command(object sender, CommandEventArgs e)
    {
        int docId = Convert.ToInt32(e.CommandArgument);
        CSCMS.Data.Document document = CscmsContext.Document.Where(it => it.DocumentID == docId).FirstOrDefault();
        if (document != null)
        {
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.{1}", document.DocumentFileName, document.FileExtension));
            Response.BinaryWrite(document.FileData.ToArray());
            Response.Flush();
        }
    }

    public string[] GetDocuments(string documentIDs)
    {
        if (!string.IsNullOrEmpty(documentIDs))
        {
            if (documentIDs.EndsWith("|"))
            {
                documentIDs = documentIDs.Substring(0, documentIDs.Length - 1);
            }

            return documentIDs.Split('|');
        }
        else
        {
            return null;
        }
    }

    public string GetActionUrl(string actionType, string actionID)
    {
        switch (actionType)
        {
            case "Hearing":
                return string.Format("~/MemberPages/HearingDetail.aspx?cid={0}&a={2}&hid={1}", this.CurrentCause.CauseGUID, actionID, this.CurrentAction.ActionNumber.ToString());
            case "Order":
                return string.Format("~/MemberPages/OrderDetail.aspx?cid={0}&a={2}&oid={1}", this.CurrentCause.CauseGUID, actionID, this.CurrentAction.ActionNumber.ToString());
            case "Motion":
                return string.Format("~/MemberPages/MotionDetail.aspx?cid={0}&a={2}&mid={1}", this.CurrentCause.CauseGUID, actionID, this.CurrentAction.ActionNumber.ToString());
            case "Settlement":
                return string.Format("~/MemberPages/SettlementDetail.aspx?cid={0}&a={2}&sid={1}", this.CurrentCause.CauseGUID, actionID, this.CurrentAction.ActionNumber.ToString());
            case "Document":
                return string.Format("~/MemberPages/DocumentDetail.aspx?cid={0}&a={2}&did={1}", this.CurrentCause.CauseGUID, actionID, this.CurrentAction.ActionNumber.ToString());
            default:
                throw new Exception("Unknown action type");
        }
    }

}
