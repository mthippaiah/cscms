﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using System.Configuration;
using CSCMS.Data;

namespace CSCMS.Calendar
{
    public class SchedulerProvider : SchedulerProviderBase
    {
        public const int MORNING_START_HOUR = 7;
        public const int AFTERNOON_START_HOUR = 13;
        public const int HEARING_LENGTH = 5;
        public int hearingLength = HEARING_LENGTH;
        public SchedulerProvider()
        {

        }

        public override void Delete(RadScheduler owner, Appointment appointmentToDelete)
        {
            throw new NotImplementedException();
        }

        private void SetViewData(string view, CalendarViewData calViewData, CalendarAppointmentData data)
        {
            switch (view)
            {
                case "DayView": calViewData.DayViewAppt = data;
                    break;
                case "MonthView": calViewData.MonthViewAppt = data;
                    break;
                case "WeekView": calViewData.WeekViewAppt = data;
                    break;
            }
        }

        public override IEnumerable<Appointment> GetAppointments(RadScheduler owner)
        {
            List<HearingData> list = new List<HearingData>();
            CalendarViewData calViewData = null;
            CalendarAppointmentData calData = null;
            if (HttpContext.Current.Session["CalendarViewData"] != null && ((CalendarViewData)HttpContext.Current.Session["CalendarViewData"]).HearingDate == owner.SelectedDate)
            {
                calViewData = (CalendarViewData)HttpContext.Current.Session["CalendarViewData"];
                if (owner.SelectedView == SchedulerViewType.DayView)
                    calData = calViewData.DayViewAppt;

                if (owner.SelectedView == SchedulerViewType.MonthView)
                    calData = calViewData.MonthViewAppt;

                if (owner.SelectedView == SchedulerViewType.WeekView)
                    calData = calViewData.WeekViewAppt;
            }
            else
            {
                calViewData = new CalendarViewData { HearingDate = owner.SelectedDate };
                HttpContext.Current.Session["CalendarViewData"] = calViewData;
            }

            if (calData != null && calData.st == owner.VisibleRangeStart && calData.et == owner.VisibleRangeEnd)
            {
                //For some reason, if I dont clone, there will telerik javascript errors in line
                //var n=(j.get_allowDelete()!=null)?j.get_allowDelete():this.get_allowDelete();

                List<Appointment> tempList = new List<Appointment>();
                foreach (Appointment apt in calData.AppointmentsList)
                {
                    Appointment apt1 = apt.Clone();
                    tempList.Add(apt1);
                }
                return tempList;
            }
            else
            {
                list = Helper.GetCalendarHearings(((BasePage)owner.Page).CurrentUser.OCACourtID, owner.VisibleRangeStart, owner.VisibleRangeEnd);
                calData = new CalendarAppointmentData { /*CalendarData = list,*/ st = owner.VisibleRangeStart, et = owner.VisibleRangeEnd };
                SetViewData(owner.SelectedView.ToString(), calViewData, calData);
            }

            List<Appointment> appointmentsList = new List<Appointment>();
            //////// HN
            //////var hearingCounts =  dc.UpdateCalendarHearingOrders(
            //////    ((BasePage)owner.Page).CurrentUser.OCACourtID,
            //////    owner.VisibleRangeStart,
            //////    owner.VisibleRangeEnd).FirstOrDefault();
            //////hearingLength = HEARING_LENGTH;
            //////owner.MinutesPerRow = 15;
            //////if (hearingCounts != null && owner.VisibleRangeStart.Date == owner.VisibleRangeEnd.Date)
            //////{
            //////    if (hearingCounts.HearingCount > 30)
            //////    {
            //////        hearingLength = 10;
            //////        owner.MinutesPerRow = 10;
            //////    }
            //////    //if (hearingCounts.HearingCount < 10)
            //////    //{
            //////    //    hearingLength = 30;
            //////    //    owner.MinutesPerRow = 30;
            //////    //}
            //////}

            DateTime startDate = DateTime.MinValue;
            TimeSpan morningStartTime = new TimeSpan(MORNING_START_HOUR, 0, 0);
            TimeSpan afternoonStartTime = new TimeSpan(AFTERNOON_START_HOUR, 0, 0);
            int order = 1;
            bool morningFound = false;
            bool afternoonFound = false;

            if (owner.SelectedView != SchedulerViewType.MonthView)
            {
                foreach (HearingData hearing in list)
                {
                    if (hearing.HearingDate == startDate)
                    {
                        if (hearing.MorningDocket)
                        {
                            if (morningFound)
                            {
                                order++;
                                morningStartTime = morningStartTime.Add(new TimeSpan(0, hearingLength, 0));
                            }
                            else
                            {
                                morningFound = true;
                                order = 1;
                            }
                        }
                        else
                        {
                            if (afternoonFound)
                            {
                                order++;
                                afternoonStartTime = afternoonStartTime.Add(new TimeSpan(0, hearingLength, 0));
                            }
                            else
                            {
                                afternoonFound = true;
                                order = 1;
                            }
                        }
                    }
                    else
                    {
                        order = 1;
                        startDate = hearing.HearingDate;
                        morningStartTime = new TimeSpan(MORNING_START_HOUR, 0, 0);
                        afternoonStartTime = new TimeSpan(AFTERNOON_START_HOUR, 0, 0);
                        morningFound = false;
                        afternoonFound = false;
                        if (hearing.MorningDocket) morningFound = true; else afternoonFound = true;
                    }

                    DateTime startTime = startDate + (hearing.MorningDocket ? morningStartTime : afternoonStartTime);
                    Appointment apt = new Appointment(hearing.HearingID, startTime, startTime.AddMinutes(hearingLength), hearing.CauseNumber);
                    apt.Attributes.Add("County", hearing.CountyName);
                    apt.Attributes.Add("CountyID", hearing.CountyID.ToString());
                    apt.Attributes.Add("Style", hearing.Style);
                    //apt.Attributes.Add("HearingType", hearing.HearingTypeName);
                    apt.Attributes.Add("HearingTime", hearing.HearingTime.HasValue ? hearing.HearingTime.Value.Ticks.ToString() : "");
                    apt.Attributes.Add("Color", hearing.CalendarColor);
                    apt.Attributes.Add("Order", order.ToString());
                    apt.Attributes.Add("ActionGUID", hearing.ActionGUID.ToString());
                    apt.Attributes.Add("CauseGUID", hearing.CauseGUID.ToString());
                    apt.Attributes.Add("CauseNumber", hearing.CauseNumber.ToString());
                    apt.Attributes.Add("DayCount", hearing.DayCount.ToString());
                    apt.Attributes.Add("ActionNumber", hearing.ActionNumber.ToString());
                    apt.Attributes.Add("ActionTypeName", hearing.ActionTypeName.ToString());

                    string nhd = "";
                    string d = "";

                    if (hearing.NextHearingDate.HasValue && hearing.NextHearingDate.Value.Date > DateTime.Now.Date)
                        nhd = hearing.NextHearingDate.Value.ToShortDateString();

                    if (hearing.DispositionDate.HasValue)
                        d = "D";

                    if (!string.IsNullOrEmpty(nhd))
                        d = d + (!string.IsNullOrEmpty(d) ? ":" : "") + nhd;

                    if (hearing.ClientAttorneyExists)
                        d = (d + " Atty");

                    apt.Attributes.Add("DispositionDate", d);


                    apt.Attributes.Add("SpecialOrder", hearing.SpecialOrder != null ? "so" : "");
                    //apt.Attributes.Add("SecurityRiskCount", hearing.SecurityRiskCount.ToString());
                    //apt.Attributes.Add("IncarceratedCount", hearing.IncarceratedCount.ToString());
                    //apt.Attributes.Add("CriminalSystemHistoryCount", hearing.CriminalSystemHistoryCount.ToString());
                    //apt.Attributes.Add("DisabilityCount", hearing.DisabilityCount.ToString());
                    //apt.Attributes.Add("DeceasedCount", hearing.DeceasedCount.ToString());
                    //apt.Attributes.Add("TPRCount", hearing.TPRCount.ToString());
                    //apt.Attributes.Add("InterpreterNeededCount", hearing.InterpreterNeededCount.ToString());

                    //apt.Attributes.Add("SpecialDataCount",
                    //    (hearing.SecurityRiskCount.GetValueOrDefault() +
                    //    hearing.IncarceratedCount.GetValueOrDefault() +
                    //    hearing.CriminalSystemHistoryCount.GetValueOrDefault() +
                    //    hearing.DisabilityCount.GetValueOrDefault() +
                    //    hearing.DeceasedCount.GetValueOrDefault() +
                    //    hearing.TPRCount.GetValueOrDefault() +
                    //    hearing.InterpreterNeededCount.GetValueOrDefault()).ToString());

                    string timeOfDay = hearing.MorningDocket ? "Morning" : "Afternoon";
                    apt.Resources.Add(new Resource("DocketTimeOfDay", timeOfDay, timeOfDay));
                    appointmentsList.Add(apt);
                }
            }

            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                        CommandType.StoredProcedure,
                            "GetSchedulingExceptions",
                        new SqlParameter("@OCACourtID", ((BasePage)owner.Page).CurrentUser.OCACourtID), new SqlParameter("@StartDate", owner.VisibleRangeStart), new SqlParameter("@EndDate", owner.VisibleRangeEnd));

            string note = "";
            string afternoonNote = "";
            string time1Note = "";
            string time2Note = "";
            DateTime? timeStart1 = null;
            DateTime? timeEnd1 = null;
            DateTime? timeStart2 = null;
            DateTime? timeEnd2 = null;

            //foreach (var exception in exceptions)
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row["Note"] == DBNull.Value)
                    note = "";
                else
                    note = (string)row["Note"];


                if (row["AfternoonNote"] == DBNull.Value)
                    afternoonNote = "";
                else
                    afternoonNote = (string)row["AfternoonNote"];

                if (row["TimePeriod1StartTime"] == DBNull.Value)
                    timeStart1 = null;
                else
                    timeStart1 = (DateTime)row["TimePeriod1StartTime"];


                if (row["TimePeriod1EndTime"] == DBNull.Value)
                    timeEnd1 = null;
                else
                    timeEnd1 = (DateTime)row["TimePeriod1EndTime"];


                if (row["TimePeriod2StartTime"] == DBNull.Value)
                    timeStart2 = null;
                else
                    timeStart2 = (DateTime)row["TimePeriod2StartTime"];


                if (row["TimePeriod2EndTime"] == DBNull.Value)
                    timeEnd2 = null;
                else
                    timeEnd2 = (DateTime)row["TimePeriod2EndTime"];


                if (row["TimePeriod1Note"] == DBNull.Value)
                    time1Note = "";
                else
                    time1Note = (string)row["TimePeriod1Note"];

                if (row["TimePeriod2Note"] == DBNull.Value)
                    time2Note = "";
                else
                    time2Note = (string)row["TimePeriod2Note"];

                if ((bool)row["BlockMorning"] == true)
                {
                    Appointment apt = new Appointment(
                    "0",
                    ((DateTime)row["ExceptionDate"]).AddHours(MORNING_START_HOUR),
                    ((DateTime)row["ExceptionDate"]).AddHours(MORNING_START_HOUR + 5),
                    note);

                    apt.Attributes.Add("County", "AM Blocked");
                    apt.Attributes.Add("HearingTime", "");
                    apt.Attributes.Add("Color", "White");
                    apt.Attributes.Add("Order", "1");
                    apt.Attributes.Add("CaseGUID", Guid.Empty.ToString());
                    apt.Attributes.Add("DayCount", "0");
                    //apt.Attributes.Add("SecurityRiskCount", "0");
                    //apt.Attributes.Add("IncarceratedCount", "0");
                    //apt.Attributes.Add("CriminalSystemHistoryCount", "0");
                    //apt.Attributes.Add("DisabilityCount", "0");
                    //apt.Attributes.Add("DeceasedCount", "0");
                    //apt.Attributes.Add("TPRCount", "0");
                    //apt.Attributes.Add("InterpreterNeededCount", "0");
                    //apt.Attributes.Add("SpecialDataCount", "0");
                    apt.Resources.Add(new Resource("DocketTimeOfDay", "Morning", "Morning"));

                    appointmentsList.Add(apt);
                }

                if ((bool)row["BlockAfternoon"] == true)
                {
                    Appointment apt = new Appointment(
                    "0",
                    ((DateTime)row["ExceptionDate"]).AddHours(AFTERNOON_START_HOUR),
                    ((DateTime)row["ExceptionDate"]).AddHours(AFTERNOON_START_HOUR + 5),
                    afternoonNote);

                    apt.Attributes.Add("County", "PM Blocked");
                    apt.Attributes.Add("HearingTime", "");
                    apt.Attributes.Add("Color", "White");
                    apt.Attributes.Add("Order", "1");
                    apt.Attributes.Add("CaseGUID", Guid.Empty.ToString());
                    apt.Attributes.Add("DayCount", "0");
                    //apt.Attributes.Add("SecurityRiskCount", "0");
                    //apt.Attributes.Add("IncarceratedCount", "0");
                    //apt.Attributes.Add("CriminalSystemHistoryCount", "0");
                    //apt.Attributes.Add("DisabilityCount", "0");
                    //apt.Attributes.Add("DeceasedCount", "0");
                    //apt.Attributes.Add("TPRCount", "0");
                    //apt.Attributes.Add("InterpreterNeededCount", "0");
                    //apt.Attributes.Add("SpecialDataCount", "0");
                    apt.Resources.Add(new Resource("DocketTimeOfDay", "Afternoon", "Afternoon"));

                    appointmentsList.Add(apt);
                }

                // This is a weird rule, but I just do as I'm told =)
                // If the user did not block out the morning and they did not block out the afternoon,
                // assume that they want to block out the entire day if they added a note in either
                // the morning note or afternoon note
                if (((bool)row["BlockMorning"] == false)
                    && ((bool)row["BlockAfternoon"] == false)
                    && (!string.IsNullOrEmpty(note) || !string.IsNullOrEmpty(afternoonNote)))
                {
                    Appointment apt = new Appointment(
                    "0",
                    ((DateTime)row["ExceptionDate"]).AddHours(MORNING_START_HOUR),
                    ((DateTime)row["ExceptionDate"]).AddHours(AFTERNOON_START_HOUR + 5),
                    !string.IsNullOrEmpty(note) ? note : afternoonNote);

                    apt.Attributes.Add("County", "Day Blocked");
                    apt.Attributes.Add("HearingTime", "");
                    apt.Attributes.Add("Color", "White");
                    apt.Attributes.Add("Order", "1");
                    apt.Attributes.Add("CaseGUID", Guid.Empty.ToString());
                    apt.Attributes.Add("DayCount", "0");
                    //apt.Attributes.Add("SecurityRiskCount", "0");
                    //apt.Attributes.Add("IncarceratedCount", "0");
                    //apt.Attributes.Add("CriminalSystemHistoryCount", "0");
                    //apt.Attributes.Add("DisabilityCount", "0");
                    //apt.Attributes.Add("DeceasedCount", "0");
                    //apt.Attributes.Add("TPRCount", "0");
                    //apt.Attributes.Add("InterpreterNeededCount", "0");
                    //apt.Attributes.Add("SpecialDataCount", "0");
                    apt.Resources.Add(new Resource("DocketTimeOfDay", "Afternoon", "Afternoon"));

                    appointmentsList.Add(apt);
                }

                if (timeStart1.HasValue)
                {
                    Appointment apt = new Appointment(
                    "0",
                    timeStart1.Value,
                    timeEnd1.Value,
                    time1Note);

                    apt.Attributes.Add("County", string.Format("{0:t}-{1:t}", timeStart1.Value, timeEnd1.Value));
                    apt.Attributes.Add("HearingTime", "");
                    apt.Attributes.Add("Color", "White");
                    apt.Attributes.Add("Order", "1");
                    apt.Attributes.Add("CaseGUID", Guid.Empty.ToString());
                    apt.Attributes.Add("DayCount", "0");
                    //apt.Attributes.Add("SecurityRiskCount", "0");
                    //apt.Attributes.Add("IncarceratedCount", "0");
                    //apt.Attributes.Add("CriminalSystemHistoryCount", "0");
                    //apt.Attributes.Add("DisabilityCount", "0");
                    //apt.Attributes.Add("DeceasedCount", "0");
                    //apt.Attributes.Add("TPRCount", "0");
                    //apt.Attributes.Add("InterpreterNeededCount", "0");
                    //apt.Attributes.Add("SpecialDataCount", "0");
                    string timeOfDay = timeStart1.Value.TimeOfDay.Hours < 12 ? "Morning" : "Afternoon";
                    apt.Resources.Add(new Resource("DocketTimeOfDay", timeOfDay, timeOfDay));

                    appointmentsList.Add(apt);
                }

                if (timeStart2.HasValue)
                {
                    Appointment apt = new Appointment(
                    "0",
                    timeStart2.Value,
                    timeEnd2.Value,
                    time2Note);

                    apt.Attributes.Add("County", string.Format("{0:t}-{1:t}", timeStart2, timeEnd2));
                    apt.Attributes.Add("HearingTime", "");
                    apt.Attributes.Add("Color", "White");
                    apt.Attributes.Add("Order", "1");
                    apt.Attributes.Add("CaseGUID", Guid.Empty.ToString());
                    apt.Attributes.Add("DayCount", "0");
                    //apt.Attributes.Add("SecurityRiskCount", "0");
                    //apt.Attributes.Add("IncarceratedCount", "0");
                    //apt.Attributes.Add("CriminalSystemHistoryCount", "0");
                    //apt.Attributes.Add("DisabilityCount", "0");
                    //apt.Attributes.Add("DeceasedCount", "0");
                    //apt.Attributes.Add("TPRCount", "0");
                    //apt.Attributes.Add("InterpreterNeededCount", "0");
                    //apt.Attributes.Add("SpecialDataCount", "0");
                    string timeOfDay = timeStart2.Value.TimeOfDay.Hours < 12 ? "Morning" : "Afternoon";
                    apt.Resources.Add(new Resource("DocketTimeOfDay", timeOfDay, timeOfDay));

                    appointmentsList.Add(apt);
                }

            }

            calData.AppointmentsList = appointmentsList;
            return appointmentsList;
        }

        public override IEnumerable<ResourceType> GetResourceTypes(RadScheduler owner)
        {
            List<ResourceType> resourceTypes = new List<ResourceType>();
            resourceTypes.Add(new ResourceType("DocketTimeOfDay"));
            return resourceTypes;
        }

        public override IEnumerable<Resource> GetResourcesByType(RadScheduler owner, string resourceType)
        {
            List<Resource> resources = new List<Resource>();

            if (resourceType == "DocketTimeOfDay")
            {
                resources.Add(new Resource("DocketTimeOfDay", "Morning", "Morning"));
                resources.Add(new Resource("DocketTimeOfDay", "Afternoon", "Afternoon"));
            }

            return resources;
        }

        public override void Insert(RadScheduler owner, Appointment appointmentToInsert)
        {
            throw new NotImplementedException();
        }

        public override void Update(RadScheduler owner, Appointment appointmentToUpdate)
        {
            using (CscmsEntities context = new CscmsEntities())
            {
                // Get the selected hearing
                long hearingId = (int)appointmentToUpdate.ID;
                CSCMS.Data.Hearing hearing = context.Hearing.Include("Action.Cause.OCACourt").Where(it => it.HearingID == hearingId).FirstOrDefault();

                hearing.HearingDate = appointmentToUpdate.Start.Date;
                hearing.MorningDocket = appointmentToUpdate.Start.Hour < AFTERNOON_START_HOUR;

                List<CSCMS.Data.Hearing> hearings = (from h in context.Hearing
                                                     join a in context.Action on h.Action.ActionGUID equals a.ActionGUID
                                                     join c in context.Cause on a.Cause.CauseGUID equals c.CauseGUID
                                                     where h.HearingDate == appointmentToUpdate.Start.Date
                                                     && c.OCACourt.OCACourtID == hearing.Action.Cause.OCACourt.OCACourtID
                                                     && h.MorningDocket == hearing.MorningDocket && h.HearingID != hearing.HearingID
                                                     select h).OrderBy(it => it.HearingOrder).ToList();

                DateTime startDate = appointmentToUpdate.Start.Date.AddHours(hearing.MorningDocket ? MORNING_START_HOUR : AFTERNOON_START_HOUR);

                byte index = 1;
                bool orderSet = false;

                foreach (CSCMS.Data.Hearing hrng in hearings)
                {
                    if (appointmentToUpdate.Start >= startDate && appointmentToUpdate.Start < startDate.AddMinutes(hearingLength))
                    {
                        hearing.HearingOrder = index;
                        orderSet = true;
                        index++;
                    }

                    if (hrng.HearingOrder != index)
                    {
                        hrng.HearingOrder = index;
                    }

                    startDate = startDate.AddMinutes(hearingLength);
                    index++;
                }

                if (!orderSet)
                {
                    hearing.HearingOrder = index;
                }

                context.SaveChanges();
            }
        }

    }
}