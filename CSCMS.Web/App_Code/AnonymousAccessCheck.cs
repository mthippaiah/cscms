﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Principal;

public static class AnonymousAccessCheck
{
    public static bool IsAnonymousAccessAllowed(HttpRequest request)
    {
        // unfortunately checking if a page allows anonymous access is more complicated than you'd think(I think).
        // here we have to create a "Fake" IPrincipal that will only ever have access to 
        // pages that allow anonymous access.  That way if our fake principal has access,
        // then anonymous access is allowed

        UrlAuthorizationModule urlAuthorizationModule = new UrlAuthorizationModule();
        return UrlAuthorizationModule.CheckUrlAccessForPrincipal(request.Path, AnonymousPrincipal.Instance, request.RequestType);
    }

    private class AnonymousPrincipal : IPrincipal
    {
        private static AnonymousPrincipal _Instance;
        public static AnonymousPrincipal Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new AnonymousPrincipal();

                return _Instance;
            }
        }

        private AnonymousPrincipal()
        {
            _Identity = new AnonymousIdentity();
        }

        private readonly IIdentity _Identity;

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return _Identity; }
        }

        public bool IsInRole(string role)
        {
            return false;
        }

        #endregion

        private class AnonymousIdentity : IIdentity
        {
            #region IIdentity Members
            public string AuthenticationType
            {
                get { return string.Empty; }
            }

            public bool IsAuthenticated
            {
                get { return false; }
            }

            public string Name
            {
                get { return string.Empty; }
            }
            #endregion
        }
    }
}
