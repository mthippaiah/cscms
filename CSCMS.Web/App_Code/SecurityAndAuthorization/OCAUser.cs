﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

[Serializable]
public class OCAUser
{
    private short userID;
    private string userName;
    private string fullName;
    private UserType userType;
    private bool readOnlyRights;
    private short ocaCourtID;
    private string ocaCourtName;
    private string ocaCourtJudge;
    public List<short> counties = new List<short>();

    public short UserID
    {
        get { return userID; }
        set { userID = value; }
    }

    public string UserName
    {
        get { return userName; }
        set { userName = value; }
    }

    public string FullName
    {
        get { return fullName; }
        set { fullName = value; }
    }

    public UserType UserType
    {
        get { return userType; }
        set { userType = value; }
    }

    public bool ViewAllUser
    {
        get { return (userType != UserType.Coordinator && userType != UserType.Judge); }
    }
    public short OCACourtID
    {
        get { return ocaCourtID; }
        set { ocaCourtID = value; }
    }

    public string OCACourtName
    {
        get { return ocaCourtName; }
        set { ocaCourtName = value; }
    }

    public string OCACourtJudge
    {
        get { return ocaCourtJudge; }
        set { ocaCourtJudge = value; }
    }

    public List<short> Counties
    {
        get { return counties; }
    }
}
