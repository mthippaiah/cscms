﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DateHelper
/// </summary>
public class DateHelper
{
    #region Public Methods

    public static DateRange GetRange(string rangeName)
    {
        DateRange dr = new DateRange();

        switch (rangeName)
        {
            case "ThisWeek":
                return (GetThisWeek());
            case "LastWeek":
                return (GetLastWeek());
            case "ThisMonth":
                return (GetThisMonth());
            case "LastMonth":
                return (GetLastMonth());
            case "ThisCalendarYear":
                return (GetThisCalendarYear());
            case "LastCalendarYear":
                return (GetLastCalendarYear());
            case "ThisFiscalYear":
                return (GetThisFiscalYear());
            case "LastFiscalYear":
                return (GetLastFiscalYear());
            default: // If date range name not found, return maximum value
                return (new DateRange(DateTime.MinValue, DateTime.MaxValue));
        }
        //return dr;
    }

    public static DateTime GetDateValue(string dateName)
    {
        switch (dateName)
        {
            case "Today":
                return GetToday();
            case "Yesterday":
                return GetYesterday();
            case "FirstDayOfThisWeek":
                return GetFirstDayOfThisWeek();
            case "LastDayOfThisWeek":
                return GetLastDayOfThisWeek();
            case "FirstDayOfLastWeek":
                return GetFirstDayOfLastWeek();
            case "LastDayOfLastWeek":
                return GetLastDayOfLastWeek();
            case "FirstDayOfThisMonth":
                return GetFirstDayOfThisMonth();
            case "LastDayOfThisMonth":
                return GetLastDayOfThisMonth();
            case "FirstDayOfLastMonth":
                return GetFirstDayOfLastMonth();
            case "LastDayOfLastMonth":
                return GetLastDayOfLastMonth();
            case "FirstDayOfThisCalendarYear":
                return GetFirstDayOfThisCalendarYear();
            case "LastDayOfThisCalendarYear":
                return GetLastDayOfThisCalendarYear();
            case "FirstDayOfLastCalendarYear":
                return GetFirstDayOfLastCalendarYear();
            case "LastDayOfLastCalendarYear":
                return GetLastDayOfLastCalendarYear();
            case "FirstDayOfThisFiscalYear":
                return GetFirstDayOfThisFiscalYear();
            case "LastDayOfThisFiscalYear":
                return GetLastDayOfThisFiscalYear();
            case "FirstDayOfLastFiscalYear":
                return GetFirstDayOfLastFiscalYear();
            case "LastDayOfLastFiscalYear":
                return GetLastDayOfLastFiscalYear();
            default:
                break;
        }

        return DateTime.Now;
    }

    /// <summary>
    /// Evaulates a relative date to produce an actual date
    /// </summary>
    /// <param name="anchor">The base virtual date on which to base our calculations</param>
    /// <param name="scale">The unit of measure to use when defining a date relative to the anchor</param>
    /// <param name="endOfDay">True if the date is an ending date</param>
    /// <param name="value">The quantity of time units (of type scale) to add to the anchor</param>
    /// <returns></returns>
    public static DateTime EvaluateDate(DateAnchorType anchor, DateScaleType scale, long value, bool endOfDay)
    {
        DateTime calculatedDate = DateTime.Now;

        int adjustmentValue = (int)value;

        switch (anchor)
        {
            case DateAnchorType.Absolute:
                //  Ignore the scale for absolute dates
                calculatedDate = ValueToDate(value);
                break;

            //  Add additional cases here
            case DateAnchorType.EndOfFY:
                calculatedDate = CalculateDateWithOffset(GetLastDayOfThisFiscalYear(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.EndOfPFY:             //sj- added previous FY
                calculatedDate = CalculateDateWithOffset(GetLastDayOfLastFiscalYear(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.EndOfMonth:
                calculatedDate = CalculateDateWithOffset(GetLastDayOfThisMonth(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.EndOfWeek:
                calculatedDate = CalculateDateWithOffset(GetLastDayOfThisWeek(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.EndOfCY:
                calculatedDate = CalculateDateWithOffset(GetLastDayOfThisCalendarYear(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.StartOfFY:
                calculatedDate = CalculateDateWithOffset(GetFirstDayOfThisFiscalYear(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.StartOfPFY:             //sj- added previous FY
                calculatedDate = CalculateDateWithOffset(GetFirstDayOfLastFiscalYear(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.StartOfMonth:
                calculatedDate = CalculateDateWithOffset(GetFirstDayOfThisMonth(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.StartOfWeek:
                calculatedDate = CalculateDateWithOffset(GetFirstDayOfThisWeek(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.StartOfCY:
                calculatedDate = CalculateDateWithOffset(GetFirstDayOfThisCalendarYear(), anchor, scale, adjustmentValue);
                break;

            case DateAnchorType.Today:
                calculatedDate = CalculateDateWithOffset(GetToday(), anchor, scale, adjustmentValue);
                break;

            default:
                calculatedDate = DateTime.MinValue;
                break;
        }

        ///  If the caller indicated that the date is an end date, we need to set the time portion 
        ///  of the datetime to the last possible tick of the day.  So we strip out any time that 
        ///  *might* be in the calculated date, add one day then back up one tick.
        if (endOfDay)
        {
            calculatedDate = calculatedDate.Date.AddDays(1).AddTicks(-1);
        }

        return calculatedDate;
    }

    private static DateTime CalculateDateWithOffset(DateTime calculatedDate, DateAnchorType dateAnchorType, DateScaleType dateScaleType, int adjustmentValue)
    {
        DateTime date = DateTime.MinValue;

        switch (dateScaleType)
        {
            case DateScaleType.BusinessDay:
                date = calculatedDate.AddBusinessDays(adjustmentValue);
                break;
            case DateScaleType.CalendarDay:
                date = calculatedDate.AddDays(adjustmentValue);
                break;
            case DateScaleType.Month:
                date = calculatedDate.AddMonths(adjustmentValue);

                if (dateAnchorType == DateAnchorType.EndOfMonth)
                {
                    date = GetLastDayOfMonth(date);
                }
                break;
            case DateScaleType.Week:
                adjustmentValue = adjustmentValue * 7;
                date = calculatedDate.AddDays(adjustmentValue);
                break;
            case DateScaleType.Year:
                date = calculatedDate.AddYears(adjustmentValue);
                break;
            default:
                date = calculatedDate;
                break;
        }

        return date;

    }

    public static long DateToValue(DateTime date)
    {
        return date.Date.Ticks;
    }
    public static DateTime ValueToDate(long value)
    {
        return new DateTime(value);
    }


    #endregion

    #region Single Date Functions

    private static DateTime GetToday()
    {
        return DateTime.Today;
    }

    /// <summary>
    /// Return Yesterday's Date
    /// </summary>
    /// <returns></returns>
    private static DateTime GetYesterday()
    {
        return DateTime.Today.AddDays(-1);
    }

    private static DateTime GetFirstDayOfThisWeek()
    {
        return DateTime.Today.StartOfWeek();
    }

    private static DateTime GetLastDayOfThisWeek()
    {
        return DateTime.Today.EndOfWeek();
    }

    private static DateTime GetFirstDayOfLastWeek()
    {
        return DateTime.Today.AddDays(-7).StartOfWeek();
    }

    private static DateTime GetLastDayOfLastWeek()
    {
        return DateTime.Today.AddDays(-7).EndOfWeek();
    }

    private static int GetMonth(DateTime date)
    {
        return date.Month;
    }

    private static DateTime GetFirstDayOfThisMonth()
    {
        DateTime workingDate = DateTime.Today;
        DateTime dt = DateTime.Parse(workingDate.Month.ToString() + "/" + "1" + "/" + workingDate.Year.ToString());
        return dt;
    }

    private static DateTime GetLastDayOfThisMonth()
    {
        DateTime workingDate = DateTime.Today;
        DateTime dt = DateTime.Parse(workingDate.Month.ToString() + "/" + DateTime.DaysInMonth(workingDate.Year, workingDate.Month).ToString() + "/" + workingDate.Year.ToString()).EndOfDay();
        return dt;
    }

    private static DateTime GetFirstDayOfLastMonth()
    {
        DateTime workingDate = DateTime.Today.AddMonths(-1);
        DateTime dt = DateTime.Parse(workingDate.Month.ToString() + "/" + "01" + "/" + workingDate.Year.ToString());
        return dt;
    }

    private static DateTime GetLastDayOfMonth(DateTime date)
    {
        int numberOfDays = DateTime.DaysInMonth(date.Year, date.Month);

        DateTime dt = new DateTime(date.Year, date.Month, numberOfDays); ;

        return dt;
    }

    private static DateTime GetLastDayOfLastMonth()
    {
        DateTime workingDate = DateTime.Today.AddMonths(-1);
        DateTime dt = DateTime.Parse(workingDate.Month.ToString() + "/" + DateTime.DaysInMonth(workingDate.Year, workingDate.Month).ToString() + "/" + workingDate.Year.ToString()).EndOfDay();
        return dt;
    }

    private static DateTime GetFirstDayOfThisCalendarYear()
    {
        DateTime workingDate = DateTime.Today;
        DateTime dt = DateTime.Parse("01" + "/" + "01" + "/" + workingDate.Year.ToString()); ;
        return dt;
    }

    private static DateTime GetLastDayOfThisCalendarYear()
    {
        DateTime workingDate = DateTime.Today;
        DateTime dt = DateTime.Parse("12" + "/" + "31" + "/" + workingDate.Year.ToString()).EndOfDay();
        return dt;
    }

    private static DateTime GetFirstDayOfLastCalendarYear()
    {
        DateTime workingDate = DateTime.Today.AddYears(-1);
        DateTime dt = DateTime.Parse("01" + "/" + "01" + "/" + workingDate.Year.ToString());
        return dt;
    }

    private static DateTime GetLastDayOfLastCalendarYear()
    {
        DateTime workingDate = DateTime.Today.AddYears(-1);
        DateTime dt = DateTime.Parse("12" + "/" + "31" + "/" + workingDate.Year.ToString()).EndOfDay();
        return dt;
    }

    private static DateTime GetFirstDayOfThisFiscalYear()
    {
        FiscalYear fy = new FiscalYear(DateTime.Now);
        return fy.Start;
    }

    private static DateTime GetLastDayOfThisFiscalYear()
    {
        FiscalYear fy = new FiscalYear(DateTime.Now);
        return fy.End;
    }

    private static DateTime GetFirstDayOfLastFiscalYear()
    {
        FiscalYear fy = new FiscalYear(DateTime.Now.AddYears(-1));
        return fy.Start;
    }

    private static DateTime GetLastDayOfLastFiscalYear()
    {
        FiscalYear fy = new FiscalYear(DateTime.Now.AddYears(-1));
        return fy.End;
    }

    #endregion

    #region Date Range Functions

    private static DateRange GetThisWeek()
    {
        return (new DateRange(GetFirstDayOfThisWeek(), GetLastDayOfThisWeek()));
    }

    private static DateRange GetLastWeek()
    {
        return (new DateRange(GetFirstDayOfLastWeek(), GetLastDayOfLastWeek()));
    }

    private static DateRange GetThisMonth()
    {
        return (new DateRange(GetFirstDayOfThisMonth(), GetLastDayOfThisMonth()));
    }

    private static DateRange GetLastMonth()
    {
        return (new DateRange(GetFirstDayOfLastMonth(), GetLastDayOfLastMonth()));
    }

    private static DateRange GetThisCalendarYear()
    {
        return (new DateRange(GetFirstDayOfThisCalendarYear(), GetLastDayOfThisCalendarYear()));
    }

    private static DateRange GetLastCalendarYear()
    {
        return (new DateRange(GetFirstDayOfLastCalendarYear(), GetLastDayOfLastCalendarYear()));
    }

    private static DateRange GetThisFiscalYear()
    {
        return (new DateRange(GetFirstDayOfThisFiscalYear(), GetLastDayOfThisFiscalYear()));
    }

    private static DateRange GetLastFiscalYear()
    {
        return (new DateRange(GetFirstDayOfLastFiscalYear(), GetLastDayOfLastFiscalYear()));
    }
    #endregion
}

public class DateRange
{
    DateTime _startDate;
    DateTime _endDate;

    public DateRange()
    {
        _startDate = DateTime.MinValue;
        _endDate = DateTime.MinValue;
    }

    public DateRange(DateTime startDate, DateTime endDate)
    {
        _startDate = startDate;
        _endDate = endDate;
    }

    public DateTime StartDate
    {
        get
        {
            return _startDate;
        }
        set
        {
            _startDate = value;
        }
    }

    public DateTime EndDate
    {
        get
        {
            return _endDate;
        }
        set
        {
            _endDate = value;
        }
    }

}

public sealed class FiscalYear
{
    private const int FY_START_MONTH = 9;

    #region Static methods
    /// <summary>
    /// Get the Fiscal Year for the current date 
    /// </summary>
    public static FiscalYear Now { get { return (new FiscalYear(DateTime.Now)); } }

    /// <summary>
    /// Returns the FiscalYear object for the specified fiscal year
    /// </summary>
    /// <param name="fyYear">the fiscal year</param>
    /// <returns></returns>
    public static FiscalYear Get(int fyYear)
    {
        return new FiscalYear(StartDate(fyYear));
    }

    private static DateTime StartDate(int fyYear)
    {
        DateTime yearStart = DateTime.Parse(string.Format("{0}/1/{1}", FY_START_MONTH, fyYear - 1));
        return yearStart;
    }
    #endregion

    public DateTime SpecifiedDate { get; private set; }

    /// <summary>
    /// Gets the Fiscal Year containing the specified date
    /// </summary>
    /// <param name="date">The date for which the fiscal year is to be found.</param>        
    public FiscalYear(DateTime date)
    {
        this.SpecifiedDate = date;

        //  Calculate fiscal year and name
        // TODO: Handle case where FY = calendar year
        Year = SpecifiedDate.Year + ((FY_START_MONTH != 1) && (SpecifiedDate.Month >= FY_START_MONTH) ? 1 : 0);

        //  Calculate fiscal year start and end dates
        Start = FiscalYear.StartDate(Year);
        End = Start.AddYears(1).AddTicks(-1);

        //  Set quarter and month related values
        Quarter = new FiscalQuarter(this);
        Month = new FiscalMonth(this);

        return;
    }

    /// <summary>
    /// Gets the FiscalQuarter for this date
    /// </summary>
    public FiscalQuarter Quarter { get; private set; }
    /// <summary>
    /// Gets the FiscalMonth for this date
    /// </summary>
    public FiscalMonth Month { get; private set; }
    /// <summary>
    /// Gets the fiscal year value
    /// </summary>
    public int Year { get; private set; }
    /// <summary>
    /// Gets a System.String that represents the name of the fiscal year
    /// </summary>
    public string Name { get { return string.Format("FY{0}", Year); } }
    /// <summary>
    /// Returns a System.String that represents the current FiscalYear
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return Name;
    }
    /// <summary>
    /// Gets the start date of this fiscal year
    /// </summary>
    public DateTime Start { get; private set; }
    /// <summary>
    /// Gets the start date of this fiscal year
    /// </summary>
    public DateTime End { get; private set; }
    /// <summary>
    /// Gets the length of time since the start of the fiscal year
    /// </summary>
    public TimeSpan SinceStart { get { return SpecifiedDate - Start; } }
    /// <summary>
    /// Gets the length of time until the end of the fiscal year
    /// </summary>
    public TimeSpan UntilEnd { get { return End - SpecifiedDate; } }
    /// <summary>
    /// Gets the length of the fiscal year in days
    /// </summary>
    public int Days { get { return (End.AddTicks(1) - Start).Days; } }
    /// <summary>
    /// Determines if the specified date falls within this fiscal year
    /// </summary>
    /// <param name="date"></param>
    /// <returns>True if the date falls within the quarter and false otherwise</returns>
    public bool Contains(DateTime date)
    {
        return (date >= Start) && (date <= End);
    }

    public class FiscalQuarter
    {
        private int year;
        private DateTime SpecifiedDate { get; set; }
        private Quarter[] quarters;
        internal FiscalQuarter(FiscalYear fiscalYear)
        {
            year = fiscalYear.Year;
            SpecifiedDate = fiscalYear.SpecifiedDate;

            // Calculate the current quarter number
            Number = ((fiscalYear.SpecifiedDate.Year * 12 + fiscalYear.SpecifiedDate.Month) -
                      (fiscalYear.Start.Year * 12 + fiscalYear.Start.Month)) / 4 + 1;

            // Calculate start dates for each quarter
            quarters = new Quarter[4];
            for (int q = 0; q < 4; ++q)
            {
                quarters[q] = new Quarter(q + 1, fiscalYear.Start.AddMonths(q * 3));
            }
            return;
        }

        public Quarter this[int quarterNumber]
        {
            get
            {
                if ((quarterNumber < 1) || (quarterNumber > 4))
                    throw new ArgumentOutOfRangeException("quarterNumber", "Quarter number must be between 1 and 4");

                return (quarters[quarterNumber - 1]);
            }
        }

        /// <summary>
        /// Returns a System.String that represents the current FiscalQuarter
        /// </summary>
        /// <returns></returns>
        public override string ToString() { return Name; }
        /// <summary>
        /// Gets the name of the quarter not including the year containing the date used to create the FiscalYear
        /// </summary>
        public string Name { get { return quarters[Number - 1].Name; } }
        /// <summary>
        /// Gets the name of quarter including the fiscal year
        /// </summary>
        public string FullName { get { return string.Format("FY{0}{1}", year, Name); } }
        /// <summary>
        /// Gets the ordinal position of the quarter in the fiscal year. I.e. 1, 2, 3, or 4.
        /// </summary>
        public int Number { get; private set; }
        /// <summary>
        /// Gets the start date of fiscal quarter containing the date used to create the FiscalYear
        /// </summary>
        public DateTime Start { get { return quarters[Number - 1].Start; } }
        /// <summary>
        /// Gets the end date of fiscal quarter containing the date used to create the FiscalYear
        /// </summary>
        public DateTime End { get { return quarters[Number - 1].End; } }
        /// <summary>
        /// Gets the length of time since the start of the quarter
        /// </summary>
        public TimeSpan SinceStart { get { return SpecifiedDate - Start; } }
        /// <summary>
        /// Gets the length of time until the end of the quarter
        /// </summary>
        public TimeSpan UntilEnd { get { return End - SpecifiedDate; } }
        /// <summary>
        /// Gets the length of the quarter in days
        /// </summary>
        public int Days { get { return (End.AddTicks(1) - Start).Days; } }
        /// <summary>
        /// Determines if the specified date falls within the same fiscal quarter as the date used to create the FiscalYear
        /// </summary>
        /// <param name="date"></param>
        /// <returns>True if the date falls within the quarter and false otherwise</returns>
        public bool Contains(DateTime date)
        {
            return quarters[Number - 1].Contains(date);
        }

        public class Quarter
        {
            private int number;
            /// <summary>
            /// Gets the name of the quarter not including the year
            /// </summary>
            internal Quarter(int number, DateTime start)
            {
                this.number = number;
                Start = start;
            }
            public string Name { get { return string.Format("Q{0}", number); } }
            /// <summary>
            /// Gets the start date of the fiscal quarter
            /// </summary>
            public DateTime Start { get; set; }
            /// <summary>
            /// Gets the end date of the fiscal quarter
            /// </summary>
            public DateTime End { get { return Start.AddMonths(3).AddTicks(-1); } }
            /// <summary>
            /// Determines if the specified date falls within the fiscal quarter
            /// </summary>
            /// <param name="date"></param>
            /// <returns>True if the date falls within the quarter and false otherwise</returns>
            public bool Contains(DateTime date)
            {
                return (date >= Start) && (date <= End);
            }
        }
    }

    public class FiscalMonth
    {
        private FiscalYear FiscalYear { get; set; }
        private Month[] months;
        internal FiscalMonth(FiscalYear fiscalYear)
        {
            FiscalYear = fiscalYear;

            // Calculate the current month number
            Number = (fiscalYear.SpecifiedDate.Year * 12 + fiscalYear.SpecifiedDate.Month) -
                     (fiscalYear.Start.Year * 12 + fiscalYear.Start.Month) + 1;

            // Calculate start dates for each month
            months = new Month[12];
            for (int m = 0; m < 12; ++m)
            {
                months[m] = new Month(fiscalYear.Start.AddMonths(m));
            }
        }

        public Month this[int monthNumber]
        {
            get
            {
                if ((monthNumber < 1) || (monthNumber > 12))
                    throw new ArgumentOutOfRangeException("monthNumber", "Month number must be between 1 and 12");
                return (months[monthNumber - 1]);
            }
        }

        /// <summary>
        /// Gets the ordinal position of the month in the fiscal year. I.e. 1, 2, 3, ... 12.
        /// </summary>
        public int Number { get; private set; }
        /// <summary>
        /// Gets the start date of month containing the date used to create the FiscalYear
        /// </summary>
        public DateTime Start { get { return months[Number - 1].Start; } }
        /// <summary>
        /// Gets the end date of month containing the date used to create the FiscalYear
        /// </summary>
        public DateTime End { get { return months[Number - 1].End; } }
        /// <summary>
        /// Determines if the specified date falls within the same month as the date used to create the FiscalYear
        /// </summary>
        /// <param name="date"></param>
        /// <returns>True if the date falls within the quarter and false otherwise</returns>
        public bool Contains(DateTime date)
        {
            return months[Number - 1].Contains(date);
        }

        public class Month
        {
            internal Month(DateTime start)
            {
                Start = start;
            }

            /// <summary>
            /// Gets the start date of the month
            /// </summary>
            public DateTime Start { get; set; }
            /// <summary>
            /// Gets the end date of the month
            /// </summary>
            public DateTime End { get { return Start.AddMonths(1).AddTicks(-1); } }
            /// <summary>
            /// Determines if the specified date falls within the month
            /// </summary>
            /// <param name="date"></param>
            /// <returns>True if the date falls within the month and false otherwise</returns>
            public bool Contains(DateTime date)
            {
                return (date >= Start) && (date <= End);
            }

        }
    }
}

public enum DateAnchorType
{
    Absolute = 0,
    Today = 1,
    StartOfWeek = 2,
    EndOfWeek = 3,
    StartOfMonth = 4,
    EndOfMonth = 5,
    StartOfCY = 6,
    EndOfCY = 7,
    StartOfFY = 8,
    EndOfFY = 9,
    StartOfPFY = 10,
    EndOfPFY = 11,
}

public enum DateScaleType
{
    CalendarDay = 0,
    BusinessDay = 1,
    Week = 2,
    Month = 3,
    Year = 4,
}