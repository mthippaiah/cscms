﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Extensions
/// </summary>
public static class Extensions
{
    public static string ApplicationRoot(this HttpRequest request)
    {
        return (request.ApplicationPath == "/" ? "" : request.ApplicationPath);
    }

    public static DateTime EndOfDay(this DateTime date)
    {
        return (date.Subtract(new TimeSpan(0, date.Hour, date.Minute, date.Second, date.Millisecond)).AddDays(1).AddTicks(-1));
    }

    public static DateTime StartOfWeek(this DateTime date)
    {
        return (date.AddDays((int)date.DayOfWeek * -1));
    }

    public static DateTime EndOfWeek(this DateTime date)
    {
        return (date.AddDays((7 - (int)date.DayOfWeek)).AddMilliseconds(-1));
    }

    public static FiscalYear GetFiscalYear(this DateTime date)
    {
        return new FiscalYear(date);
    }

    public static bool IsBusinessDay(this DateTime date)
    {
        return BusinessCalendar.IsBusinessDay(date);
    }

    public static DateTime AddBusinessDays(this DateTime date, int days)
    {
        return BusinessCalendar.AddBusinessDays(date, days);
    }

    public static DateTime AddDays(this DateTime date, int days, bool requireBusinessDay)
    {
        date = date.AddDays(days);
        if (requireBusinessDay)
        {
            date = BusinessCalendar.GetNextBusinessDay(date);
        }
        return date;
    }

   
    //Any collection that implements IEnumerable<T> can use this extension method
    public static bool IsNullOrEmpty<T>(this IEnumerable<T> items)
    {
        if (items == null || items.Count() == 0)
        {
            return true;
        }
        return false;
    }

    //
    public static bool Contains(this IEnumerable<string> items, string item, bool ignoreCase)
    {
        if (ignoreCase)
        {
            return items.Contains(item, new CaseInSensitiveCompare());
        }
        else
        {
            return items.Contains(item);
        }
    }

    public static T GetValueForKey<T>(this Dictionary<string, T> items, string key, bool ignoreCase)
    {
        if (items.Keys.Contains(key, true))
        {
            foreach (string k in items.Keys)
            {
                if (k.ToLower() == key.ToLower())
                {
                    return items[k];
                }
            }
        }
        return default(T);
    }

    public static string GetBase(this Uri uri)
    {
        return uri.GetLeftPart(UriPartial.Authority);
    } 
}

public static class BusinessCalendar
{
    static SortedList<DateTime, string> holidays;
    static BusinessCalendar()
    {
        holidays = new SortedList<DateTime, string>
            {
              {DateTime.Parse("09-01-08").Date, "Labor Day"},
              {DateTime.Parse("11-11-08").Date, "Veteran's Day"}
            };
    }

    /// <summary>
    /// Determines if the specified day is a business day or not
    /// </summary>
    /// <param name="date">Date to check</param>
    /// <returns>false if the specified day falls on a weekend or a holiday, and true otherwise</returns>
    public static bool IsBusinessDay(DateTime date)
    {
        bool weekend = (date.DayOfWeek == DayOfWeek.Sunday) || (date.DayOfWeek == DayOfWeek.Saturday);
        bool holiday = holidays.ContainsKey(date.Date);
        return !(weekend || holiday);
    }

    /// <summary>
    /// Returns a date that is a business day and is the specified number of days from the supplied date
    /// </summary>
    /// <param name="date">The starting date</param>
    /// <param name="days">The number of business days to add (or subtract)</param>
    /// <returns></returns>
    public static DateTime AddBusinessDays(DateTime date, int days)
    {
        //  If you add zero days to a day, this function returns the original day reguardless of 
        //  whether it is a business day or not.
        int delta = Math.Sign(days);
        while (days != 0)
        {
            date = date.AddDays(delta);
            days -= IsBusinessDay(date) ? delta : 0;
        }
        return date;
    }

    /// <summary>
    /// Returns specififed date if it is a business day, or earliest date that is a business day following it
    /// </summary>
    /// <param name="date">The starting date for the search</param>
    /// <returns>The earliest date that is greater than or equal to the specified date and is a business day.</returns>
    public static DateTime GetNextBusinessDay(DateTime date)
    {
        while (!date.IsBusinessDay())
        {
            date = date.AddDays(1);
        }
        return date;
    }

    /// <summary>
    /// Returns the number of business between the two dates (not counting the startdate)
    /// </summary>
    /// <param name="startDate"></param>
    /// <param name="endDate"></param>
    /// <returns>The number of business days</returns>
    public static int CountBusinessDays(DateTime startDate, DateTime endDate)
    {
        int bizDays = 0;
        int days = (endDate.Date - startDate.Date).Days;

        if (days != 0)
        {
            for (int d = 1; d <= Math.Abs(days); ++d)
            {
                bizDays += IsBusinessDay(startDate.AddDays(d * Math.Sign(days))) ? 1 : 0;
            }
        }
        return bizDays;
    }
}

public class CaseInSensitiveCompare : IEqualityComparer<string>
{

    public bool Equals(string x, string y)
    {
        return x.ToLower() == y.ToLower();
    }

    public int GetHashCode(string obj)
    {
        throw new NotImplementedException();
    }
}