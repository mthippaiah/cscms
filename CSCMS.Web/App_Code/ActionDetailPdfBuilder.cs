﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Telerik.Web.UI;
/// <summary>
/// Summary description for ActionDetailPdfBuilder
/// </summary>
public class ActionDetailPdfBuilder
{
    static float margin = 36f;
    static float width = 594f;
    static float top = 841f;
    static float headerHt = 40;


    public ActionDetailPdfBuilder()
    {
    }

    public static void Build(System.Web.UI.Control ctrl, System.Web.UI.Control ctrlCaseNotes, string pdfile, ActionHeaderData header)
    {
        PdfPage page = new PdfPage();
        page.ShowPageCount = true;
        var doc = new Document(PageSize.A4, 36f, 36f, 36f, 36f);
        PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(pdfile, FileMode.Create));
        writer.PageEvent = page;
        doc.Open();

        PdfAction jAction = PdfAction.JavaScript("this.print(true);\r", writer);
        //writer.AddJavaScript(jAction);
        BuildHeader(header, doc);
        BuildCaseInformation(ctrl, doc);
        BuildPersonTable(ctrl, doc);
        BuildHearingsAndDocuments(ctrl, doc);
        BuildOrders(ctrl, doc);
        BuildChildSupport(ctrl, doc);
        BuildContinuance(ctrl, doc);
        BuildCaseNotes(ctrlCaseNotes, doc);
        doc.Close();
    }

    public static void BuildHeader(ActionHeaderData header, Document doc)
    {
        Action<PdfPCell> setLeftProperties = delegate(PdfPCell cell)
        {
            cell.BackgroundColor = BaseColor.CYAN;
            cell.Colspan = 1;
            cell.BorderWidthRight = 0f;
            cell.BorderWidthBottom = 0f;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
        };

        Action<PdfPCell> setMidProperties = delegate(PdfPCell cell)
        {
            cell.BackgroundColor = BaseColor.CYAN;
            cell.Colspan = 1;
            cell.BorderWidthLeft = 0f;
            cell.BorderWidthRight = 0f;
            cell.BorderWidthBottom = 0f;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
        };

        Action<PdfPCell> setEndProperties = delegate(PdfPCell cell)
        {
            cell.BackgroundColor = BaseColor.CYAN;
            cell.Colspan = 1;
            cell.BorderWidthLeft = 0f;
            cell.BorderWidthBottom = 0f;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        };

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        PdfPTable table = new PdfPTable(3);
        table.HorizontalAlignment = Element.ALIGN_LEFT;
        table.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        table.LockedWidth = true;

        PdfPCell cLeft = new PdfPCell(new Phrase("Region: " + header.Region, times));
        setLeftProperties(cLeft);
        table.AddCell(cLeft);

        PdfPCell cMid = new PdfPCell(new Phrase("Action: " + header.ActionNumber + "/" + header.NumOfAction, times));
        setMidProperties(cMid);
        table.AddCell(cMid);

        PdfPCell cEnd = new PdfPCell(new Phrase("Active/Inactive: " + header.ActiveStatus, times));
        setEndProperties(cEnd);
        table.AddCell(cEnd);

        cLeft = new PdfPCell(new Phrase("Court: " + header.CourtName, times));
        setLeftProperties(cLeft);
        cLeft.BorderWidthTop = 0f;
        table.AddCell(cLeft);

        cMid = new PdfPCell(new Phrase("", times));
        setMidProperties(cMid);
        cMid.BorderWidthTop = 0f;
        table.AddCell(cMid);

        cEnd = new PdfPCell(new Phrase("Penidng/Disposed: " + header.PendingStatus, times));
        setEndProperties(cEnd);
        cEnd.BorderWidthTop = 0f;
        table.AddCell(cEnd);

        cLeft = new PdfPCell(new Phrase("Judge: " + header.JudgeName, times));
        setLeftProperties(cLeft);
        cLeft.BorderWidthTop = 0f;
        cLeft.BorderWidthBottom = 0.2f;
        table.AddCell(cLeft);

        cMid = new PdfPCell(new Phrase("", times));
        setMidProperties(cMid);
        cMid.BorderWidthTop = 0f;
        cMid.BorderWidthBottom = 0.2f;
        table.AddCell(cMid);

        cEnd = new PdfPCell(new Phrase("Action Age(In Days): " + header.ActionAge, times));
        setEndProperties(cEnd);
        cEnd.BorderWidthTop = 0f;
        cEnd.BorderWidthBottom = 0.2f;
        table.AddCell(cEnd);

        doc.Add(table);

    }

    public static void BuildCaseInformation(System.Web.UI.Control ctrl, Document doc)
    {
        string actionType = "", county = "", causeNumber = "", style = "", dtR = "", dtS = "", lnpSvcDt = "", appFileDt = "", trsCourt = "", oagUnit = "",
        oag1 = "", oag2 = "", oag3 = "", orgCourt = "", filingDate = "", disp = "", dispDetail = "";
        bool ckbActiveChecked = false, ckbSharedChecked = false, ckbActiveEnabled = false;

        System.Web.UI.Control c = Helper.FindControlRecursive(ctrl, "ddlActionType");
        actionType = ((DropDownList)c).SelectedItem.Text;
        c = Helper.FindControlRecursive(ctrl, "ddlCounty");
        county = ((DropDownList)c).SelectedItem.Text.Replace("- Select -", "");
        c = Helper.FindControlRecursive(ctrl, "txtCauseNumber");
        causeNumber = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtStyle");
        style = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtStyle");
        style = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "dtDispositionRenderedDate");
        dtR = ((RadDatePicker)c).SelectedDate.HasValue ? ((RadDatePicker)c).SelectedDate.Value.ToString("MM/dd/yyyy") : "";
        c = Helper.FindControlRecursive(ctrl, "dtDispositionSignedDate");
        dtS = ((RadDatePicker)c).SelectedDate.HasValue ? ((RadDatePicker)c).SelectedDate.Value.ToString("MM/dd/yyyy") : "";
        c = Helper.FindControlRecursive(ctrl, "lblServiceLastNecessaryParyDate");
        lnpSvcDt = ((Label)c).Text;
        c = Helper.FindControlRecursive(ctrl, "dtAppealFiledDate");
        appFileDt = ((RadDatePicker)c).SelectedDate.HasValue ? ((RadDatePicker)c).SelectedDate.Value.ToString("MM/dd/yyyy") : "";
        c = Helper.FindControlRecursive(ctrl, "ddlTransferToOCACourt");
        trsCourt = ((DropDownList)c).SelectedItem.Text.Replace("- Select -", "");
        c = Helper.FindControlRecursive(ctrl, "txtOAGOffice");
        oagUnit = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtOAG1");
        oag1 = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtOAG2");
        oag2 = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtOAG3");
        oag3 = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "ddlReferringCourt");
        orgCourt = ((DropDownList)c).SelectedItem.Text.Replace("- Select -", "");
        c = Helper.FindControlRecursive(ctrl, "dtFilingDate");
        filingDate = ((RadDatePicker)c).SelectedDate.HasValue ? ((RadDatePicker)c).SelectedDate.Value.ToString("MM/dd/yyyy") : "";
        c = Helper.FindControlRecursive(ctrl, "ddlDisposition");
        disp = ((DropDownList)c).SelectedItem.Text.Replace("- Select -", "");
        c = Helper.FindControlRecursive(ctrl, "ddlDispositionDetail");
        dispDetail = ((DropDownList)c).SelectedItem.Text.Replace("- Select -", "");
        c = Helper.FindControlRecursive(ctrl, "chkActive");
        ckbActiveChecked = ((CheckBox)c).Checked;
        ckbActiveEnabled = ((CheckBox)c).Enabled;
        c = Helper.FindControlRecursive(ctrl, "chkShared");

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(2);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;

        float[] widths = new float[] { 261f, 261f };
        pTable.SetWidths(widths);

        PdfPCell caseInfo = new PdfPCell(new Phrase("CASE INFORMATION", times));
        caseInfo.Colspan = 2;
        caseInfo.BackgroundColor = CMYKColor.LIGHT_GRAY;
        caseInfo.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(caseInfo);

        Action<PdfPCell> setProperties = delegate(PdfPCell cell)
        {
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        };

        Action<PdfPCell> setLeftMiddleProperties = delegate(PdfPCell cell)
        {
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidthBottom = 0f;
            cell.BorderWidthRight = 0f;
            cell.BorderWidthTop = 0f;
        };

        Action<PdfPCell> setRightMiddleProperties = delegate(PdfPCell cell)
        {
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidthBottom = 0f;
            cell.BorderWidthLeft = 0f;
            cell.BorderWidthTop = 0f;
        };

        PdfPCell left = new PdfPCell(new Phrase("ActionType: " + actionType, times));
        setProperties(left);
        left.BorderWidthBottom = 0f;
        left.BorderWidthRight = 0f;
        pTable.AddCell(left);

        PdfPCell right = new PdfPCell(new Phrase("OAG Unit#: " + oagUnit, times));
        setProperties(right);
        right.BorderWidthBottom = 0f;
        right.BorderWidthLeft = 0f;
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("County: " + county, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        right = new PdfPCell(new Phrase("OAG #: " + oag1 + " " + oag2 + " " + oag3, times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("Cause Number: " + causeNumber, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        right = new PdfPCell(new Phrase("Originating Court: " + orgCourt, times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("Style: " + style, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        right = new PdfPCell(new Phrase("Filing Date: " + filingDate, times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("_________________________________________________", times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        right = new PdfPCell(new Phrase("_______________________________________________", times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("Disposition Rendered Date: " + dtR, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        right = new PdfPCell(new Phrase("Disposition: " + disp, times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("Disposition Signed Date: " + dtS, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        right = new PdfPCell(new Phrase("Disposition Detail: " + dispDetail, times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("Last NP Service Date: " + lnpSvcDt, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        //right = new PdfPCell(new Phrase("Active: " + (ckbActiveChecked ? "yes" : "no"), times));
        right = new PdfPCell(new Phrase("", times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("Appeal Filed Date: " + appFileDt, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        //right = new PdfPCell(new Phrase("Share with Other Courts: " + (ckbSharedChecked ? "yes" : "no"), times));
        right = new PdfPCell(new Phrase("", times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("Transfer To: " + trsCourt, times));
        setProperties(left);
        left.BorderWidthBottom = 0f;
        left.BorderWidthTop = 0f;
        left.Colspan = 2;
        pTable.AddCell(left);

        left = new PdfPCell(new Phrase("_________________________________________________", times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        right = new PdfPCell(new Phrase("_______________________________________________", times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        c = Helper.FindControlRecursive(ctrl, "rptRelatedCases");
        RepeaterItemCollection list = ((Repeater)c).Items;
     
        Regex r = new Regex(">\r\n.*</a>");
        Regex href1 = new Regex("<a href=\".*\r\n");
        Regex href2 = new Regex("\".*\"");
        Paragraph p1 = new Paragraph(new Phrase("Related Cause(s):\n", times));
        Font link = FontFactory.GetFont(BaseFont.TIMES_ROMAN, 8, Font.UNDERLINE, CMYKColor.BLUE);
        foreach (RepeaterItem item in list)
        {
            string str = ((System.Web.UI.DataBoundLiteralControl)(item.Controls[2])).Text;
            string str1 = r.Match(str).Value.Replace(">\r\n", "").Replace("</a>", "").Trim();
            string str2 = href1.Match(str).Value;
            str2 = href2.Match(str2).Value.Replace("\"", "").Trim();
            Anchor anchor = new Anchor(str1 + Environment.NewLine, link);
            anchor.Reference = string.Format("{0}://{1}:{2}{3}/MemberPages/{4}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url.Port, HttpContext.Current.Request.ApplicationRoot(), str2);
            p1.Add(anchor);
        }

        left = new PdfPCell();
        setProperties(left);
        left.BorderWidthRight = 0f;
        left.BorderWidthTop = 0f;
        left.AddElement(p1);
        pTable.AddCell(left);

        c = Helper.FindControlRecursive(ctrl, "rptRelatedActions");
        list = ((Repeater)c).Items;
        p1 = new Paragraph(new Phrase("Related Action(s):\n", times));
        r = new Regex(">.*</a>");

        Regex r2 = new Regex(">\r\n.*\r\n.*</td");
        href1 = new Regex("<a href=.*\r\n");
        href2 = new Regex("\".*\"");
        foreach (RepeaterItem item in list)
        {
            string str = ((System.Web.UI.DataBoundLiteralControl)(item.Controls[1].Controls[0])).Text;
            string str1 = r.Match(str).Value.Replace(">", "").Replace("&nbsp;", "").Replace("</a", "").Trim();


            string hstr2 = href1.Match(str).Value;
            hstr2 = href2.Match(hstr2).Value.Replace("\"", "").Trim();
           


            string str2 = ((System.Web.UI.DataBoundLiteralControl)(item.Controls[4])).Text;
            MatchCollection coln = r2.Matches(str2);

            string temp = "";
            foreach (Match m in coln)
            {
               temp +=("  " +  m.Value.Replace(">\r\n", "").Replace("</td", "").Trim());
            }

            Anchor anchor = new Anchor(new Phrase(str1 + temp + Environment.NewLine, link));
            anchor.Reference = string.Format("{0}://{1}:{2}{3}/MemberPages/{4}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url.Port, HttpContext.Current.Request.ApplicationRoot(), hstr2);
            p1.Add(anchor);
        }
        right = new PdfPCell();
        setProperties(right);
        right.BorderWidthLeft = 0f;
        right.BorderWidthTop = 0f;
        right.AddElement(p1);
        pTable.AddCell(right);

        doc.Add(pTable);
    }

    public static void BuildPersonTable(System.Web.UI.Control ctrl, Document doc)
    {
        System.Web.UI.Control c = Helper.FindControlRecursive(ctrl, "grdPersons");
        GridDataItemCollection coln = ((RadGrid)c).Items;

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(13);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 20f, 70f, 38f, 50f, 38f, 48f, 40f, 20f, 48f, 35f, 20f, 55f, 35f };
        pTable.SetWidths(widths);

        PdfPCell cell = new PdfPCell(new Phrase("PERSONS", times));
        cell.Colspan = 13;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);
  
        times = new Font(bfTimes, 8, Font.BOLD, BaseColor.BLACK);

        cell = new PdfPCell(new Phrase("NP?", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Name", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Person Type", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Custodial", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Father\nType", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Birth Date", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Age", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Sex", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Service Date", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Service\nType", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("NC?", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Attorney", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Counsel status", times));
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        foreach (GridDataItem item in coln)
        {
            bool b = (item["NPFlag"].Controls[0] as CheckBox).Checked;
            string np = b ? "Yes" : "No";
            pTable.AddCell(new Phrase(np, times));

            string name = ((item.FindControl("lnkPerson") as HyperLink).Controls[0] as System.Web.UI.DataBoundLiteralControl).Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(name, times));

            string ptname = item["PersonTypeName"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(ptname, times));

            string ctname = item["CustodialTypeName"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(ctname, times));

            string ftname = item["FatherTypeName"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(ftname, times));

            string bdate = item["BirthDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(bdate, times));

            string age = (item["Age"].Controls[0] as System.Web.UI.DataBoundLiteralControl).Text.Replace("\r\n", "").Replace("&nbsp;", " ").Trim();
            pTable.AddCell(new Phrase(age, times));

            string sex = item["Sex"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(sex, times));

            string svcdate = item["ServiceDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(svcdate, times));

            string svcType = item["ServiceTypeName"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(svcType, times));

            b = (item["NeedAttorneyAppointed"].Controls[0] as CheckBox).Checked;
            string natapp = b ? "Yes" : "No";
            pTable.AddCell(new Phrase(natapp, times));

            string att = item["Attorney"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(att, times));

            string cs = item["CounselStatus"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(cs, times));

            //string rel = item["Relationships"].Text.Replace("&nbsp;", " ");
            //pTable.AddCell(new Phrase(rel, times));
        }
        doc.Add(pTable);
    }

    public static void BuildHearingsAndDocuments(System.Web.UI.Control ctrl, Document doc)
    {
        System.Web.UI.Control c = Helper.FindControlRecursive(ctrl, "grdActions");
        GridDataItemCollection coln = ((RadGrid)c).Items;

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(4);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 47f, 38f, 120f, 316f };
        pTable.SetWidths(widths);

        PdfPCell cell = new PdfPCell(new Phrase("HEARINGS and DOCUMENTS", times));
        cell.Colspan = 4;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);
      
        times = new Font(bfTimes, 8, Font.BOLD, BaseColor.BLACK);

        cell = new PdfPCell(new Phrase("Date", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Time", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Hearing/Document", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Note", times));
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        foreach (GridDataItem item in coln)
        {
            string date = item["Date"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(date, times));

            string htime = item["HearingTime"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(htime, times));

            string action = (item.FindControl("lnkAction") as HyperLink).Text;
            pTable.AddCell(new Phrase(action, times));

             string note = item["Note"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(note, times));
        }
        doc.Add(pTable);

    }

    public static void BuildOrders(System.Web.UI.Control ctrl, Document doc)
    {
        System.Web.UI.Control c = Helper.FindControlRecursive(ctrl, "grdOrders");
        GridDataItemCollection coln = ((RadGrid)c).Items;

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(9);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 80f, 80f, 47f, 47f, 42f, 47f, 47f, 30f, 100f };
        pTable.SetWidths(widths);

        PdfPCell cell = new PdfPCell(new Phrase("ORDERS", times));
        cell.Colspan = 9;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);
     
        times = new Font(bfTimes, 8, Font.BOLD, BaseColor.BLACK);

        cell = new PdfPCell(new Phrase("Name", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Type", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Ordered\nDate", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Issued\nDate", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Bond\nAmount", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Arrest\nDate", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Release\nDate", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Days\nIncarcerated", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Note", times));
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        foreach (GridDataItem item in coln)
        {
            string name = ((item.FindControl("lnkPerson") as HyperLink).Controls[0] as System.Web.UI.DataBoundLiteralControl).Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(name, times));

            string type = item["CommitmentOrCapias"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(type, times));

            string odt = item["OrderedDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(odt, times));

            string idt = item["IssuedDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(idt, times));

            string ba = item["BondAmount"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(ba, times));

            string adt = item["ArrestCommitmentEffectiveDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(adt, times));

            string rdt = item["ReleaseDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(rdt, times));

            string dic = item["DaysIncarcerated"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(dic, times));
           
            string note = item["SpecialOrderNote"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(note, times));
        }
        doc.Add(pTable);
    }

    public static void BuildChildSupport(System.Web.UI.Control ctrl, Document doc)
    {
        string csjAmt = "", csjasof = "", msjAmt = "", msjasof = "", ccsAmt = "", cmsAmt = "", carrAmt = "", msarrAmt = "", totpAmt = "";

        System.Web.UI.Control c = Helper.FindControlRecursive(ctrl, "txtArrearage");
        csjAmt = ((RadNumericTextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "dtArrearageAsOf");
        csjasof = ((RadDatePicker)c).SelectedDate.HasValue ? ((RadDatePicker)c).SelectedDate.Value.ToString("MM/dd/yyyy") : "";
        c = Helper.FindControlRecursive(ctrl, "txtMedicalPastDue");
        msjAmt = ((RadNumericTextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "dtMedicalPastDueAsOf");
        msjasof = ((RadDatePicker)c).SelectedDate.HasValue ? ((RadDatePicker)c).SelectedDate.Value.ToString("MM/dd/yyyy") : "";
        c = Helper.FindControlRecursive(ctrl, "txtPP1");
        ccsAmt = ((RadNumericTextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtMS1");
        cmsAmt = ((RadNumericTextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtPP2");
        carrAmt = ((RadNumericTextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtMS2");
        msarrAmt = ((RadNumericTextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "txtTotal");
        totpAmt = ((RadNumericTextBox)c).Text;

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.BOLD, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(3);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;

        float[] widths = new float[] { 173f, 173f, 173 };
        pTable.SetWidths(widths);

        PdfPCell caseInfo = new PdfPCell(new Phrase("CHILD SUPPORT", times));
        caseInfo.Colspan = 3;
        caseInfo.BackgroundColor = CMYKColor.LIGHT_GRAY;
        caseInfo.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(caseInfo);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        Action<PdfPCell> setProperties = delegate(PdfPCell cell)
        {
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        };

        Action<PdfPCell> setLeftMiddleProperties = delegate(PdfPCell cell)
        {
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidthBottom = 0f;
            cell.BorderWidthRight = 0f;
            cell.BorderWidthTop = 0f;
        };

        Action<PdfPCell> setRightMiddleProperties = delegate(PdfPCell cell)
        {
            cell.Colspan = 1;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.BorderWidthBottom = 0f;
            cell.BorderWidthLeft = 0f;
            cell.BorderWidthTop = 0f;
        };

        PdfPCell left = new PdfPCell(new Phrase("CS Judgement Amount: " + csjAmt, times));
        setProperties(left);
        left.BorderWidthBottom = 0f;
        left.BorderWidthRight = 0f;
        pTable.AddCell(left);

        PdfPCell mid = new PdfPCell(new Phrase("As of: " + csjasof, times));
        setProperties(mid);
        mid.BorderWidthBottom = 0f;
        mid.BorderWidthRight = 0f;
        mid.BorderWidthLeft = 0f;
        pTable.AddCell(mid);

        PdfPCell right = new PdfPCell(new Phrase("Current CS Amount: " + ccsAmt, times));
        setProperties(right);
        right.BorderWidthBottom = 0f;
        right.BorderWidthLeft = 0f;
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("MS Judgement Amount: " + msjAmt, times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        mid = new PdfPCell(new Phrase("As of: " + msjasof, times));
        setLeftMiddleProperties(mid);
        mid.BorderWidthLeft = 0f;
        pTable.AddCell(mid);

        right = new PdfPCell(new Phrase("Current MS Amount: " + cmsAmt, times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("", times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        mid = new PdfPCell(new Phrase("", times));
        setLeftMiddleProperties(mid);
        mid.BorderWidthLeft = 0f;
        pTable.AddCell(mid);

        right = new PdfPCell(new Phrase("CS Arrearage Amount: " + carrAmt, times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);

        left = new PdfPCell(new Phrase("", times));
        setLeftMiddleProperties(left);
        pTable.AddCell(left);

        mid = new PdfPCell(new Phrase("", times));
        setLeftMiddleProperties(mid);
        mid.BorderWidthLeft = 0f;
        pTable.AddCell(mid);

        right = new PdfPCell(new Phrase("MS Arrearage Amount: " + msarrAmt , times));
        setRightMiddleProperties(right);
        pTable.AddCell(right);
        
        left = new PdfPCell(new Phrase("", times));
        setProperties(left);
        left.BorderWidthRight = 0f;
        left.BorderWidthTop = 0f;
        pTable.AddCell(left);

        mid = new PdfPCell(new Phrase("", times));
        setProperties(mid);
        mid.BorderWidthTop = 0f;
        mid.BorderWidthRight = 0f;
        mid.BorderWidthLeft = 0f;
        pTable.AddCell(mid);

        right = new PdfPCell(new Phrase("Total Payment Amount: " + totpAmt, times));
        setProperties(right);
        right.BorderWidthLeft = 0f;
        right.BorderWidthTop = 0f;
        pTable.AddCell(right);

        doc.Add(pTable);
    }

    public static void BuildContinuance(System.Web.UI.Control ctrl, Document doc)
    {
        System.Web.UI.Control c = Helper.FindControlRecursive(ctrl, "grdContinuances");
        GridDataItemCollection coln = ((RadGrid)c).Items;

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(5);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 36f, 120f, 47f, 47f, 270f };
        pTable.SetWidths(widths);

        PdfPCell cell = new PdfPCell(new Phrase("CONTINUANCE", times));
        cell.Colspan = 5;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);
       
        times = new Font(bfTimes, 8, Font.BOLD, BaseColor.BLACK);

        cell = new PdfPCell(new Phrase("Agreed", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Requested By", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Continuance\nDate", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Continued To\nDate", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("Note", times));
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        foreach (GridDataItem item in coln)
        {
            bool b = (item["Agreed"].Controls[0] as CheckBox).Checked;
            string ag = b ? "Yes" : "No";
            pTable.AddCell(new Phrase(ag, times));

            string reqBy = ((item.FindControl("lnkRequestedBy") as HyperLink).Controls[0] as System.Web.UI.DataBoundLiteralControl).Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(reqBy, times));

            string cdt = item["ContinuanceDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(cdt, times));

            string ctdt = item["ContinuedToDate"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(ctdt, times));

            string note = item["ContinuanceNote"].Text.Replace("&nbsp;", " ");
            pTable.AddCell(new Phrase(note, times));
        }
        doc.Add(pTable);
    }

    public static void BuildCaseNotes(System.Web.UI.Control ctrl, Document doc)
    {
        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(2);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 48f, 462f};
        pTable.SetWidths(widths);


        PdfPCell cell = new PdfPCell(new Phrase("CASE NOTES", times));
        cell.Colspan = 2;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);
  
        times = new Font(bfTimes, 8, Font.BOLD, BaseColor.BLACK);

        cell = new PdfPCell(new Phrase("Date", times));
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("CaseNote", times));
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        System.Web.UI.Control c = Helper.FindControlRecursive(ctrl, "txtCaseNote");
        string note = ((TextBox)c).Text;
        c = Helper.FindControlRecursive(ctrl, "dtNoteDate");
        string date = ((RadDatePicker)c).SelectedDate.HasValue ? ((RadDatePicker)c).SelectedDate.Value.ToString("MM/dd/yyyy") : "";

        pTable.AddCell(new Phrase(date, times));
        pTable.AddCell(new Phrase(note, times));

        c = Helper.FindControlRecursive(ctrl, "lvCaseNotes");
        IList<ListViewDataItem> list = ((ListView)c).Items;
        foreach (ListViewDataItem item in list)
        {
            string noteDate = ((System.Web.UI.DataBoundLiteralControl)(item.Controls[2])).Text;
            noteDate = noteDate.Replace("<td>", "").Replace("</td>", "").Replace("\r\n", "").Trim();
            string str = (item.FindControl("noteToolTip") as RadToolTip).Text;
            pTable.AddCell(new Phrase(noteDate, times));
            pTable.AddCell(new Phrase(str, times));
        }
        doc.Add(pTable);
    }
}
