﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using CSCMS.Data;
using Telerik.Web.UI;
using System.Data.EntityClient;
using System.Data.Objects;
/// <summary>
/// Summary description for Helper
/// </summary>
public sealed class Helper
{
    public static DateTime? GetLastNecessaryPartyServiceDate(SqlConnection conn, Guid actionGuid)
    {
        SqlParameter p4 = new SqlParameter("@RETURN_VALUE", SqlDbType.Date, 8);
        p4.Direction = ParameterDirection.ReturnValue;

        Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(conn,
                CommandType.StoredProcedure,
                "GetActionServiceDateonLastNP",
                new SqlParameter("@ActionGUID", actionGuid), p4);

        if (p4.Value != DBNull.Value)
        {
            return ((DateTime)p4.Value);
        }

        return null;
    }

    public static void SetActionStatusWhenHearingUpdatedOrDeleted(CSCMS.Data.CscmsEntities CscmsContext, CSCMS.Data.Action action, CSCMS.Data.Hearing h)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (action.DispositionRenderedDate != null)
            {
                bool futureHearingExists = false;

                if (h == null)
                {
                    h = CscmsContext.Hearing.Where(it => it.Action.ActionGUID == action.ActionGUID && it.HearingDate > action.DispositionRenderedDate).FirstOrDefault();

                    if (h != null)
                        futureHearingExists = true;
                }
                else
                {
                    if (h.HearingDate > action.DispositionRenderedDate)
                        futureHearingExists = true;

                }

                if (futureHearingExists)
                    action.Inactive = true;
                else
                    action.Inactive = false;
            }

            CscmsContext.SaveChanges();

            SqlConnection con = (SqlConnection)((EntityConnection)CscmsContext.Connection).StoreConnection;
            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(con,
          CommandType.StoredProcedure,
          "UpdateActionStatus",
          new SqlParameter("@ActionGUID", action.ActionGUID));
        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
            {
                CscmsContext.Connection.Close();
            }
        }
    }

    public static bool IsSectionMinimized(string section)
    {
        if (HttpContext.Current.Session[section] != null)
        {
            string minMax = (string)HttpContext.Current.Session[section];
            if (minMax == "Min")
                return true;
        }
        return false;
    }

    public static Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(
            Expression<Func<TElement, TValue>> valueSelector, IEnumerable<TValue> values)
    {
        if (null == valueSelector) { throw new ArgumentNullException("valueSelector"); }
        if (null == values) { throw new ArgumentNullException("values"); }

        ParameterExpression p = valueSelector.Parameters.Single();

        // p => valueSelector(p) == values[0] || valueSelector(p) == ...

        if (!values.Any()) { return e => false; }

        var equals = values.Select(value => (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));
        var body = equals.Aggregate<Expression>((accumulate, equal) => Expression.Or(accumulate, equal));

        return Expression.Lambda<Func<TElement, bool>>(body, p);
    }

    public static string GetFormattedName(string firstName, string middleName, string lastName, string suffix)
    {
        return string.Format("{0}{1}{2}{3}",
                    firstName,
                    !string.IsNullOrEmpty(middleName) ? " " + middleName : "",
                    !string.IsNullOrEmpty(lastName) ? " " + lastName : "",
                    !string.IsNullOrEmpty(suffix) ? " " + suffix : "");
    }
    public static string GetFormattedName2(string firstName, string middleName, string lastName, string suffix)
    {
        return string.Format("{0},{1}{2}{3}",
                    lastName,
                    !string.IsNullOrEmpty(firstName) ? " " + firstName : "",
                    !string.IsNullOrEmpty(middleName) ? " " + middleName : "",
                    !string.IsNullOrEmpty(suffix) ? " " + "" : "");
    }
    public static string ReverseDelimitedString(string value, string delimiter)
    {
        string[] values = value.Split(new string[] { delimiter }, StringSplitOptions.None);
        string[] reversedValues = values.Reverse().ToArray();
        return string.Join(delimiter, reversedValues);
    }

    public static string CalculateAge(DateTime? birthDate)
    {
        if (birthDate.HasValue)
        {
            TimeSpan tsAge = DateTime.Today - birthDate.Value;
            DateTime dtAge = DateTime.MinValue.Add(tsAge);
            int year = dtAge.Year == 1 ? 0 : dtAge.Year - 1;
            int month = dtAge.Year == 1 && dtAge.Month == 1 ? 0 : dtAge.Month - 1;
            return string.Format("{0} Yrs {1} Mos", year, month);
        }
        else
        {
            return "&nbsp;";
        }
    }

    public static string EvaluateDbStringValue(object input)
    {
        if (input == DBNull.Value)
            return "";

        return (string)input;
    }

    public static void DeletePaternityTest(CscmsEntities CscmsContext, Guid actionGUID, Guid personGUID, bool saveChanges, bool deleteOrder)
    {
        CSCMS.Data.PaternityTest paternityTest = CscmsContext.PaternityTest.Where(p => p.ActionPerson.Action.ActionGUID == actionGUID && p.ActionPerson.Person.PersonGUID == personGUID).FirstOrDefault();

        if (paternityTest != null)
        {
            CscmsContext.DeleteObject(paternityTest);

            if (deleteOrder)
            {
                CSCMS.Data.SpecialOrder so = CscmsContext.SpecialOrder.Where(it => it.ActionPerson.Action.ActionGUID == actionGUID && it.ActionPerson.Person.PersonGUID == personGUID && it.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_PATERNITY_TEST).FirstOrDefault();
                if (so != null)
                    CscmsContext.DeleteObject(so);
            }

            if (saveChanges)
                CscmsContext.SaveChanges();
        }
    }

    public static void AddorUpdatePaternityTest(CscmsEntities CscmsContext, PaternityTestData ptd, bool saveChanges, bool addorUpdateOrderToo)
    {
        CSCMS.Data.PaternityTest paternityTest = CscmsContext.PaternityTest.Where(p => p.ActionPerson.Action.ActionGUID == ptd.ActionGuid && p.ActionPerson.Person.PersonGUID == ptd.PersonGuid).FirstOrDefault();
        if (paternityTest == null && ptd.OrderedDate != null)
        {
            paternityTest = new CSCMS.Data.PaternityTest();
            paternityTest.CreateDate = DateTime.Now;
            paternityTest.LastUpdateDate = DateTime.Now;
            paternityTest.ActionPerson = CscmsContext.ActionPerson.Where(it => it.Action.ActionGUID == ptd.ActionGuid && it.Person.PersonGUID == ptd.PersonGuid).FirstOrDefault();
            CscmsContext.AddToPaternityTest(paternityTest);
        }

        if (paternityTest != null)
        {
            paternityTest.NotOrder = ptd.NotOrder;
            paternityTest.TestPositive = ptd.TestPositive;
            paternityTest.IsExcluded = ptd.isExcluded;
            paternityTest.PaternityTestNote = ptd.Note;
            paternityTest.OrderedDate = ptd.OrderedDate;
            paternityTest.PaternityEstablishedDate = ptd.PaternityestablishedDate;
            paternityTest.ResultDate = ptd.ResultDate;
        }

        if (addorUpdateOrderToo)
        {
            CSCMS.Data.SpecialOrder so = CscmsContext.SpecialOrder.Where(it => it.ActionPerson.Action.ActionGUID == ptd.ActionGuid && it.ActionPerson.Person.PersonGUID == ptd.PersonGuid && it.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_PATERNITY_TEST).FirstOrDefault();
            if (so == null)
            {
                so = new SpecialOrder();
                so.ActionPerson = CscmsContext.ActionPerson.Where(it => it.Action.ActionGUID == ptd.ActionGuid && it.Person.PersonGUID == ptd.PersonGuid).FirstOrDefault();
                so.CreateDate = DateTime.Now;
                so.CommitmentOrCapias = "2"; //Other
                so.SpecialOrderType = CscmsContext.SpecialOrderType.Where(it => it.SpecialOrderTypeName == MyConsts.DB_PATERNITY_TEST).FirstOrDefault();
                CscmsContext.AddToSpecialOrder(so);
            }
            so.LastUpdateDate = DateTime.Now;
            so.OrderedDate = ptd.OrderedDate;
            so.SpecialOrderNote = ptd.Note;
        }

        if (saveChanges)
            CscmsContext.SaveChanges();
    }

    public static List<HearingData> GetCalendarHearings(short courtId, DateTime st, DateTime et)
    {
        List<HearingData> list = new List<HearingData>();
        using (CscmsEntities CscmsContext = new CscmsEntities())
        {
            CscmsContext.Connection.Open();
            var query1 = CompiledQueryList.GetHearingForCourtAndDate.Invoke(CscmsContext, courtId, st, et);

            //TODO: if the calendar dates st and et are different, then it means, count for each specific date that fall between st and et, so this calculation wrong
            //But we don't seem to use this daycount for display in the application anywhere, so far as I know. so I am ok with this.
            if (st == et)
                query1 = query1.Where(it => it.HearingDate == st.Date);
            else
                query1 = query1.Where(it => it.HearingDate >= st.Date && it.HearingTime >= st.TimeOfDay && it.HearingDate <= et.Date && it.HearingTime <= et.TimeOfDay);

            ((ObjectQuery)query1).MergeOption = MergeOption.NoTracking;
            int dayCount = query1.Count();

            var query = CompiledQueryList.GetHearingData.Invoke(CscmsContext, courtId, dayCount);

            if (st == et)
                query = query.Where(it => it.HearingDate == st.Date);
            else
                query = query.Where(it => it.HearingDate >= st.Date && it.HearingDate <= et.Date);

            query = query.OrderBy(it => it.HearingDate).ThenByDescending(it => it.MorningDocket).ThenBy(it => it.HearingTime).ThenBy(it => it.HearingOrder).ThenBy(it => it.CountyName).ThenBy(it => it.CauseNumber);

            ((ObjectQuery)query).MergeOption = MergeOption.NoTracking;
            list = query.ToList();
        }

        foreach (HearingData item in list)
        {
            if (item.HearingTime != null)
            {
                item.DisplayTime = item.HearingTime.ToString();
            }
            if (item.HearingType != null)
            {
                item.HearingTypeID = item.HearingType.HearingTypeID;
                item.HearingTypeName = item.HearingType.HearingTypeName;
            }
            item.Style = item.Style.Replace("ITIO ", "");
            item.Style = item.Style.Replace("INIO ", "");
            item.ActionTypeName = item.ActionTypeName.Replace("Enforcement", "En");
            item.ActionTypeName = item.ActionTypeName.Replace("Enforce", "En");
            item.ActionTypeName = item.ActionTypeName.Replace("Revoke", "Re");
            item.ActionTypeName = item.ActionTypeName.Replace("Original", "Org");
            item.ActionTypeName = item.ActionTypeName.Replace("Petition", "Pe");
            item.ActionTypeName = item.ActionTypeName.Replace("Judgment", "Jud");
            item.ActionTypeName = item.ActionTypeName.Replace("Motion", "M");
        }
        return list;
    }

    public static DataSet GetPendingCases(int rangeType, short courtId)
    {
        DataSet ds = new DataSet();
        string connString = ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString;
        SqlConnection conn = new SqlConnection(connString);
        SqlCommand command = new SqlCommand("RPT_GetPendingCasesWithAgeRange", conn);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.Add("@CountyName", SqlDbType.VarChar).Value = "All";
        command.Parameters.Add("@OCACourtID", SqlDbType.Int).Value = courtId;
        command.Parameters.Add("@FromAgeInDays", SqlDbType.Int);
        command.Parameters.Add("@ToAgeInDays", SqlDbType.Int);

        if (rangeType == 1)
        {
            command.Parameters["@FromAgeInDays"].Value = 0;
            command.Parameters["@ToAgeInDays"].Value = 90;
        }

        if (rangeType == 2)
        {
            command.Parameters["@FromAgeInDays"].Value = 91;
            command.Parameters["@ToAgeInDays"].Value = 180;
        }

        if (rangeType == 3)
        {
            command.Parameters["@FromAgeInDays"].Value = 181;
            command.Parameters["@ToAgeInDays"].Value = 365;
        }

        if (rangeType == 4)
        {
            command.Parameters["@FromAgeInDays"].Value = 366;
            command.Parameters["@ToAgeInDays"].Value = DBNull.Value;
        }

        SqlDataAdapter da = new SqlDataAdapter(command);
        da.Fill(ds);
        return ds;
    }

    public static ActionHeaderData GetActionHeaderData(Guid actionGuid)
    {
        ActionHeaderData data = null;
        if (HttpContext.Current.Session["ActionHeaderData"] != null)
        {
            data = (ActionHeaderData)HttpContext.Current.Session["ActionHeaderData"];
            if (data.ActionGUID != actionGuid)
            {
                data = null;
                HttpContext.Current.Session["ActionHeaderData"] = null;
            }
        }

        if (data == null)
        {
            using (CscmsEntities CscmsContext = new CscmsEntities())
            {
                CscmsContext.Connection.Open();
                string judgeName = "";
                if (HttpContext.Current.Session["CurrentUser"] != null)
                {
                    OCAUser user = (OCAUser)HttpContext.Current.Session["CurrentUser"];
                    judgeName = user.OCACourtJudge;
                }
                IQueryable<ActionHeaderData> headerData = CompiledQueryList.GetHeaderData.Invoke(CscmsContext, actionGuid);
                ((ObjectQuery)headerData).MergeOption = MergeOption.NoTracking;
                data = headerData.FirstOrDefault();

                if (data != null)
                {
                    data.ActionGUID = actionGuid;
                    data.JudgeName = judgeName;
                    data.ActionNumber = data._ActionNumber.ToString();
                    data.FilingDate = data._FilingDate.HasValue ? data._FilingDate.Value.ToString("MM/dd/yyyy") : "";
                    data.NumOfAction = data._NumOfAction.HasValue ? data._NumOfAction.Value.ToString() : "";
                }

                SqlParameter p1 = new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4);
                p1.Direction = ParameterDirection.ReturnValue;

                SqlConnection con = (SqlConnection)((EntityConnection)CscmsContext.Connection).StoreConnection;
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(con,
                        CommandType.StoredProcedure,
                        "GetActionAging",
                        new SqlParameter("@ActionGUID", actionGuid), p1);


                if (p1.Value != DBNull.Value)
                    data.ActionAge = p1.Value.ToString();

                SqlParameter p2 = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar, 40);
                p2.Direction = ParameterDirection.ReturnValue;

                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(con,
                        CommandType.StoredProcedure,
                        "GetActionPrimaryStatus",
                        new SqlParameter("@ActionGUID", actionGuid), p2);

                if (p2.Value != DBNull.Value)
                    data.PendingStatus = p2.Value.ToString();


                SqlParameter p3 = new SqlParameter("@RETURN_VALUE", SqlDbType.VarChar, 40);
                p3.Direction = ParameterDirection.ReturnValue;

                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(con,
                        CommandType.StoredProcedure,
                        "GetActionStatus",
                        new SqlParameter("@ActionGUID", actionGuid), p3);

                if (p3.Value != DBNull.Value)
                    data.ActiveStatus = p3.Value.ToString();

                HttpContext.Current.Session["ActionHeaderData"] = data;
            }
        }

        return data;
    }

    public static Dictionary<DateTime, string> GetDateHearingSummary(CSCMS.Data.CscmsEntities CscmsContext, DateTime st, DateTime et, short OcaCourtID)
    {
        Dictionary<DateTime, string> result = new Dictionary<DateTime, string>();
        IQueryable<DateCounty> dateHearing = CompiledQueryList.GetDateHearing.Invoke(CscmsContext, OcaCourtID, st, et);
        List<DateCounty> list = dateHearing.ToList();

        while (st <= et)
        {
            List<string> counties = list.Where(it => it.HearingDate == st).Select(it => it.CountyName).OrderBy(it => it).Distinct().ToList();
            if (counties.Count > 2)
            {
                string s = "";
                foreach (string c in counties)
                {
                    int count = list.Where(it => it.HearingDate == st && it.CountyName == c).Count();
                    s += c.Substring(0, 3) + "(" + count.ToString() + ")";
                }
                result.Add(st, s);
            }
            else
            {
                string s = "";
                foreach (string c in counties)
                {
                    int count = list.Where(it => it.HearingDate == st && it.CountyName == c).Count();
                    s += c + "(" + count.ToString() + ")";
                }
                result.Add(st, s);
            }
            st = st.AddDays(1);
        }
        return result;
    }

    public static DataSet GetCaseByOAGNumber(SqlConnection conn, short userId, string oagCauseNumber)
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                     CommandType.StoredProcedure,
                     "SearchByOAGCauseNumber",
                     new SqlParameter("@UserId", userId), new SqlParameter("@OAGCauseNumber", oagCauseNumber));
        return ds;
    }

    public static DataSet GetCaseByCauseNumber(SqlConnection conn, short userId, string causeNumber)
    {
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(conn,
                            CommandType.StoredProcedure,
                            "SearchByCauseNumber",
                            new SqlParameter("@UserId", userId),
                            new SqlParameter("@CauseNumber", causeNumber));
        return ds;
    }

    public static CSCMS.Data.Hearing GetLatestHearing(CscmsEntities CscmsContext, Guid causeGuid, short numberOfAction)
    {
        bool iOpenedConn = false;
        try
        {
            if (CscmsContext.Connection.State != ConnectionState.Open)
            {
                CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            var query = CompiledQueryList.GetLatestHearing.Invoke(CscmsContext, causeGuid, numberOfAction);
            List<CSCMS.Data.Hearing> hList = query.ToList();
            if (hList.Count > 0)
                return hList[0];

        }
        finally
        {
            if (iOpenedConn && CscmsContext.Connection.State == ConnectionState.Open)
                CscmsContext.Connection.Close();
        }
        return null;
    }

    public static System.Web.UI.Control FindControlRecursive(System.Web.UI.Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (System.Web.UI.Control controlToSearch in rootControl.Controls)
        {
            System.Web.UI.Control controlToReturn =
                FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }

    public static CSCMS.Data.OCACourt GetCaseCourt(CscmsEntities CscmsContext, Guid causeGuid)
    {
        CSCMS.Data.OCACourt court = (from c in CscmsContext.Cause
                                     where c.CauseGUID == causeGuid
                                     select c.OCACourt).FirstOrDefault();
        return court;
    }

    public static void ExecuteMergePersons(Guid idToDelete, Guid idToKeep)
    {
        Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                   CommandType.StoredProcedure,
                   "MergePersons",
                   new SqlParameter("@PersonGuidToDelete", idToDelete),
                   new SqlParameter("@PersonGuidToKeep", idToKeep));
    }

    public static DataSet ExecuteFindActionWithTwoPersons(Guid idToDelete, Guid idToKeep)
    {
        return Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                 CommandType.StoredProcedure,
                 "FindActionWithTwoPersons",
                  new SqlParameter("@PersonGUIDA", idToDelete),
                  new SqlParameter("@PersonGUIDB", idToKeep));

    }

    public static DocumentDir CreateDirectory(string courtId, string userId, string actionGuid, string fileRootDir)
    {
        string fileUploadPath = "";

        if (string.IsNullOrEmpty(fileRootDir))
            fileUploadPath = HttpContext.Current.Server.MapPath("~/documents");
        else
            fileUploadPath = fileRootDir;

        fileUploadPath = fileUploadPath.TrimEnd('\\');

        string courtLevelDir = fileUploadPath + "\\" + courtId.ToString();
        string userLevelDir = courtLevelDir + "\\" + userId;
        string actionLevelDir = userLevelDir + "\\" + actionGuid;

        DocumentDir dd = new DocumentDir();
        if (!string.IsNullOrEmpty(courtId) && !Directory.Exists(courtLevelDir))
        {
            Directory.CreateDirectory(courtLevelDir);
        }

        if (!string.IsNullOrEmpty(userId) && !Directory.Exists(userLevelDir))
        {
            Directory.CreateDirectory(userLevelDir);
        }

        if (!string.IsNullOrEmpty(actionGuid) && !Directory.Exists(actionLevelDir))
        {
            Directory.CreateDirectory(actionLevelDir);
        }
        dd.CourtLevelDir = courtLevelDir;
        dd.UserLevelDir = userLevelDir;
        dd.ActionLevelDir = actionLevelDir;
        return dd;
    }

    public static void DeleteFiles(string dir, string pattern, DateTime dt)
    {
        DirectoryInfo di = new DirectoryInfo(dir);
        FileInfo[] fis = di.GetFiles(pattern, SearchOption.TopDirectoryOnly);
        List<FileInfo> flist = fis.Where(it => it.CreationTime.Date <= dt.Date).ToList();

        foreach (FileInfo fi in flist)
        {
            try
            {
                File.Delete(fi.FullName);
            }
            catch
            {
            }
        }
    }


}

[Serializable]
public class DateCounty
{
    public DateTime HearingDate { get; set; }
    public string CountyName { get; set; }
}

[Serializable]
public class DocumentDir
{
    public string CourtLevelDir { get; set; }
    public string UserLevelDir { get; set; }
    public string ActionLevelDir { get; set; }
}

[Serializable]
public class HearingData
{
    public long HearingID { get; set; }
    public Guid ActionGUID { get; set; }
    public Guid CauseGUID { get; set; }
    public DateTime HearingDate { get; set; }
    public byte? HearingTypeID { get; set; }
    public string HearingTypeName { get; set; }
    public byte? HearingOrder { get; set; }
    public string CauseNumber { get; set; }
    public short ActionNumber { get; set; }
    public string Style { get; set; }
    public short CountyID { get; set; }
    public string CountyName { get; set; }
    public string CalendarColor { get; set; }
    public bool MorningDocket { get; set; }
    public TimeSpan? HearingTime { get; set; }
    public string DisplayTime { get; set; }
    public DateTime? ActionFiledDate { get; set; }
    public string ActionTypeName { get; set; }
    public string OAGCauseNumber { get; set; }
    public DateTime? DispositionDate { get; set; }
    public DateTime? NextHearingDate { get; set; }
    public int DayCount { get; set; }
    public CSCMS.Data.HearingType HearingType { get; set; }
    public CSCMS.Data.SpecialOrder SpecialOrder { get; set; }
    public bool ClientAttorneyExists { get; set; }
}

[Serializable]
public class ActionHeaderData
{
    public Guid ActionGUID { get; set; }
    public string Region { get; set; }
    public string CourtName { get; set; }
    public string JudgeName { get; set; }
    public string FilingDate { get; set; }
    public string ActionNumberWithType { get; set; }
    public string CountyName { get; set; }
    public string CauseNumber { get; set; }
    public string ActiveStatus { get; set; }
    public string PendingStatus { get; set; }
    public string ActionAge { get; set; }
    public string ActionNumber { get; set; }
    public string NumOfAction { get; set; }
    public string ActionTypeName { get; set; }
    public short _ActionNumber { get; set; }
    public short? _NumOfAction { get; set; }
    public DateTime? _FilingDate { get; set; }
}

[Serializable]
public class PendingCaseSummary
{
    public int RangeTypeID { get; set; }
    public string RangeName { get; set; }
    public int NumberOfCase { get; set; }
}

[Serializable]
public class CalendarAppointmentData
{
    public DateTime st { get; set; }
    public DateTime et { get; set; }
    //public List<HearingData> CalendarData { get; set; }
    public List<Appointment> AppointmentsList { get; set; }
}

[Serializable]
public class CalendarViewData
{
    private Dictionary<DateTime, string> monthViewData = null;
    public DateTime HearingDate { get; set; }
    public CalendarAppointmentData DayViewAppt { get; set; }
    public CalendarAppointmentData MonthViewAppt { get; set; }
    public CalendarAppointmentData WeekViewAppt { get; set; }
    public Dictionary<DateTime, string> MonthViewData
    {
        get
        {
            return monthViewData;
        }
        set
        {
            monthViewData = value;
        }
    }
}

[Serializable]
public class PaternityTestData
{
    public bool TestPositive { get; set; }
    public string Note { get; set; }
    public DateTime? OrderedDate { get; set; }
    public DateTime? PaternityestablishedDate { get; set; }
    public DateTime? ResultDate { get; set; }
    public Guid ActionGuid { get; set; }
    public Guid PersonGuid { get; set; }
    public bool NotOrder { get; set; }
    public bool isExcluded { get; set; }
}

[Serializable]
public class LoggedInUserData
{
    public short UserID { get; set; }
    public string UserName { get; set; }
}

[Serializable]
public class TicklerData
{
    public short OCACourtID { get; set; }
    public byte TicklerTypeID { get; set; }
    public DateTime? TriggerDate { get; set; }
    public string Tickler { get; set; }
    public Guid ActionGUID { get; set; }
    public string CauseNumber { get; set; }
    public string CountyName { get; set; }
    public short ActionNumber { get; set; }
    public Guid CauseGUID { get; set; }
    public short DelayDays { get; set; }
}