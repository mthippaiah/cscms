﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SessionHelper
/// </summary>
public class SessionHelper
{
	public static void ResetRefreshDataSessionVariables()
    {
        HttpContext.Current.Session["PendingCaseWithAge"] = null;
        HttpContext.Current.Session["CauseSummaryByOCACourt"] = null;
        HttpContext.Current.Session["Ticklers0"] = null;
        HttpContext.Current.Session["Ticklers1"] = null;
        HttpContext.Current.Session["Ticklers2"] = null;
        HttpContext.Current.Session["Ticklers3"] = null;
        HttpContext.Current.Session["MyPageHearings"] = null;
        HttpContext.Current.Session["CalendarViewData"] = null;
        HttpContext.Current.Session["ActionHeaderData"] = null;
    }

    public static void ResetSearchSessionVariables()
    {
        HttpContext.Current.Session["SearchtxtCauseNumber"] = null;
        HttpContext.Current.Session["SearchtxtOAGCauseNumber"] = null;
        HttpContext.Current.Session["SearchtxtStyle"] = null;
        HttpContext.Current.Session["SearchtxtLastName"] = null;
        HttpContext.Current.Session["SearchtxtFirstName"] = null;
        HttpContext.Current.Session["SearchdtFilingDateFrom"] = null;
        HttpContext.Current.Session["SearchdtFilingDateTo"] = null;
        HttpContext.Current.Session["SearchddlCounty"] = null;
        HttpContext.Current.Session["SearchddlPersonType"] = null;
        HttpContext.Current.Session["SearchddlCaseStatus"] = null;
        HttpContext.Current.Session["SearchddlDisposed"] = null;
        HttpContext.Current.Session["SearchddlActionType"] = null;
        HttpContext.Current.Session["SearchddlLNPServiced"] = null;
        HttpContext.Current.Session["SearchddlFutureHearingSet"] = null;
    }

    public static void ResetChildSupportSessionVariables()
    {
        //Childsupport calculation
        HttpContext.Current.Session["RadComboBoxEmployed"] = null;
        HttpContext.Current.Session["RadComboBoxNumOtherChildren"] = null;
        HttpContext.Current.Session["RadNumericTextBoxGrossWages"] = null;
        HttpContext.Current.Session["RadNumericTextBoxSpecialWages"] = null;
        HttpContext.Current.Session["RadNumericTextBoxMonthlyHealthCareCost"] = null;
        HttpContext.Current.Session["LabelNumChildren"] = null;
        HttpContext.Current.Session["RadComboBoxAdjNumChildren"] = null;
    }

    public static void ResetSessionForUserCourtChange()
    {
        ResetRefreshDataSessionVariables();
        ResetSearchSessionVariables();
        ResetChildSupportSessionVariables();

        HttpContext.Current.Session["previouslyViewCases"] = null;
        HttpContext.Current.Session["CurrentAction_ActionGuid"] = null;
        HttpContext.Current.Session["HearingDateTime"] = null;
        HttpContext.Current.Session["Calendar.SelectedDate"] = null;
        HttpContext.Current.Session["ShowActivityDateTime"] = null;
        HttpContext.Current.Session["MyPageShowHearing"] = null;

        HttpContext.Current.Session["NewCauseAdded"] = null;
        HttpContext.Current.Session["txtAttorneySearchLastName"] = null;
        HttpContext.Current.Session["txtAttorneySearchFirstName"] = null;

        HttpContext.Current.Session["CurrentSpecialOrder_SpecialOrderID"] = null;
        HttpContext.Current.Session["CurrentContinuance_ContinuanceID"] = null;
        HttpContext.Current.Session["CurrentDocument_DocumentID"] = null;
        HttpContext.Current.Session["CurrentHearing_HearingID"] = null;
        HttpContext.Current.Session["CurrentPerson_PersonGUID"] = null;
        HttpContext.Current.Session["CurrentActionPerson_PersonGuid"] = null;
        HttpContext.Current.Session["CurrentActionPerson_ActionGuid"] = null;
        HttpContext.Current.Session["CurrentAction_ActionGuid"] = null;
        HttpContext.Current.Session["CurrentCause_CauseGUID"] = null;
    }
}
