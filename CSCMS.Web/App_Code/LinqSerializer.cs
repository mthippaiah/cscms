﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.IO;

public class LinqSerializer<T> : ISerializable where T : class, new()
{
    public T Item { get; set; }

    #region ISerializable Members

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        DataContractSerializer dcs = new DataContractSerializer(typeof(T));
        StringBuilder sb = new StringBuilder();
        XmlWriter writer = XmlWriter.Create(sb);
        dcs.WriteObject(writer, Item);
        writer.Close();
        string xml = sb.ToString();

        info.AddValue("item", xml);
    }

    public LinqSerializer(SerializationInfo info, StreamingContext ctxt)
    {
        string xml = info.GetString("item");
        TextReader tr = new StringReader(xml);
        XmlReader reader = XmlReader.Create(tr);
        DataContractSerializer ser = new DataContractSerializer(typeof(T));
        Item = (T)ser.ReadObject(reader, true);
    }

    public LinqSerializer(T item)
    {
        Item = item;
    }

    public LinqSerializer() { }

    #endregion

    public static string Serialize(T item)
    {
        SerializationInfo info = new SerializationInfo(typeof(T), new FormatterConverter());
        StreamingContext context = new StreamingContext(System.Runtime.Serialization.StreamingContextStates.All);
        LinqSerializer<T> serializer = new LinqSerializer<T>(item);
        serializer.GetObjectData(info, context);
        return info.GetString("item");
    }

    public static T Deserialize(string xml)
    {
        SerializationInfo info = new SerializationInfo(typeof(T), new FormatterConverter());
        StreamingContext context = new StreamingContext(System.Runtime.Serialization.StreamingContextStates.All);
        info.AddValue("item", xml);
        LinqSerializer<T> serializer = new LinqSerializer<T>(info, context);
        return serializer.Item;
    }
}
