﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public abstract class BaseUserControl : System.Web.UI.UserControl
{
    public event EventHandler SaveClick;

    public new BasePage Page
    {
        get { return (BasePage)base.Page; }
    }

    private bool hasData = false;

    public bool HasData
    {
        get { return hasData; }
        set { hasData = value; }
    }

    public void SaveClicked(object sender, EventArgs e)
    {
        if (SaveClick != null)
        {
            SaveClick(sender, e);
        }
    }
}
