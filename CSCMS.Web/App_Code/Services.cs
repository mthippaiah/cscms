﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;


/// <summary>
/// Summary description for WebServices
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public class Services : System.Web.Services.WebService
{
    [WebMethod(true)]
    public void ADCSMinMax(string minMax)
    {
        HttpContext.Current.Session["ActionDetailCaseSectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void ADChildSupportMinMaxMax(string minMax)
    {
        HttpContext.Current.Session["ActionDetailChildSupportSectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void ADContinuanceMinMaxMax(string minMax)
    {
        HttpContext.Current.Session["ActionDetailContinuanceSectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void MPTicklersMinMax(string minMax)
    {
        HttpContext.Current.Session["MyPageTicklersSectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void MPCaseLoadMinMax(string minMax)
    {
        HttpContext.Current.Session["MyPageCaseLoadSectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void MPTodaysHearingsMinMax(string minMax)
    {
        HttpContext.Current.Session["MyPageTodaysHearingsSectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void MPUserActivityMinMax(string minMax)
    {
        HttpContext.Current.Session["MyPageUserActivitySectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void MPMessagesMinMax(string minMax)
    {
        HttpContext.Current.Session["MyPageBroadCastMessagesSectionMinMax"] = minMax;
    }

    [WebMethod(true)]
    public void MPRemindersMinMax(string minMax)
    {
        HttpContext.Current.Session["MyPageRemindersSectionMinMax"] = minMax;
    }
}


