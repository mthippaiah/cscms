﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class AdminBasePage : BasePage
{
	public AdminBasePage()
	{
		
	}

    protected override void OnLoad(EventArgs e)
    {
        if (Session["LoggedInUser"] != null)
        {
            if (this.CurrentUser.UserType != UserType.OCAAdmin)
            {
                Response.Redirect("~/AnonPages/Unauthorized.aspx", true);
            }
        }
        base.OnLoad(e);
    }
}
