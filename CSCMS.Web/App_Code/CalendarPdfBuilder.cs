﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Telerik.Web.UI;
/// <summary>
/// Summary description for CalendarPdfBuilder
/// </summary>
public class CalendarPdfBuilder
{
    static float margin = 36f;
    static float width = 594f;
    static float top = 841f;
    static float headerHt = 40;

    public CalendarPdfBuilder()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void Build(RadScheduler scheduler, string pdfile)
    {
        PdfPage page = new PdfPage();
        page.ShowPageCount = true;
        var doc = new Document(PageSize.A4, 36f, 36f, 36f, 36f);
        PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(pdfile, FileMode.Create));
        writer.PageEvent = page;
        doc.Open();

        PdfAction jAction = PdfAction.JavaScript("this.print(true);\r", writer);
        //writer.AddJavaScript(jAction);
        if (scheduler.SelectedView == SchedulerViewType.DayView)
            BuildDayView(scheduler, doc);
        if (scheduler.SelectedView == SchedulerViewType.WeekView)
            BuildWeekView(scheduler, doc);
        if (scheduler.SelectedView == SchedulerViewType.MonthView)
            BuildMonthView(scheduler, doc);
        doc.Close();
    }

    private static BaseColor GetColor(string clr)
    {
        BaseColor color = null;

        switch (clr)
        {
            case "DarkBlue": color = new CMYKColor(100, 100, 0, 45);
                break;
            case "DarkGreen": color = new CMYKColor(41, 0, 41, 69);
                break;
            case "DarkRed": color = new CMYKColor(0, 100, 100, 45);
                break;
            case "Violet": color = new CMYKColor(7, 39, 0, 40);
                break;
            case "Blue": color = CMYKColor.BLUE;
                break;
            case "Green": color = CMYKColor.GREEN;
                break;
            case "Orange": color = CMYKColor.ORANGE;
                break;
            case "Pink": color = CMYKColor.PINK;
                break;
            case "Red": color = CMYKColor.RED;
                break;
            case "Yellow": color = CMYKColor.YELLOW;
                break;
        }

        return color;
    }

    private static void BuildDayView(RadScheduler scheduler, Document doc)
    {
        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(7);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 20f, 40f, 80f, 80f, 170f, 80f, 50f };
        pTable.SetWidths(widths);

        PdfPCell cell = new PdfPCell(new Phrase("CALENDAR", times));
        cell.Colspan = 3;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.BorderWidthRight = 0f;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("DAY VIEW, " + scheduler.SelectedDate.ToLongDateString(), times));
        cell.Colspan = 4;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.BorderWidthLeft = 0f;
        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        Func<PdfPCell, string, PdfPCell> setPropLeft = delegate(PdfPCell c, string clr)
        {
            c.BorderWidthRight = 0f;
            //c.BackgroundColor = GetColor(clr);
            return c;
        };

        Func<PdfPCell, string, PdfPCell> setProps = delegate(PdfPCell c, string clr)
        {
            c.BorderWidthRight = 0f;
            c.BorderWidthLeft = 0f;
            //c.BackgroundColor = GetColor(clr);
            return c;
        };

        Func<PdfPCell, string, PdfPCell> setPropsRight = delegate(PdfPCell c, string clr)
        {
            c.BorderWidthLeft = 0f;
            //c.BackgroundColor = GetColor(clr);
            return c;
        };

        foreach (Appointment apt in scheduler.Appointments)
        {
            pTable.AddCell(setPropLeft(new PdfPCell(new Phrase(apt.Attributes["Order"], times)), apt.CssClass.Replace("rsCategory", "")));

            if (!string.IsNullOrEmpty(apt.Attributes["HearingTime"]))
            {
                DateTime ht = DateTime.Today.AddTicks(long.Parse(apt.Attributes["HearingTime"]));
                pTable.AddCell(setProps(new PdfPCell(new Phrase(ht.TimeOfDay.ToString(), times)), apt.CssClass.Replace("rsCategory", "")));
            }

            pTable.AddCell(setProps(new PdfPCell(new Phrase(apt.Attributes["County"], times)), apt.CssClass.Replace("rsCategory", "")));
            pTable.AddCell(setProps(new PdfPCell(new Phrase(apt.Subject, times)), apt.CssClass.Replace("rsCategory", "")));
            pTable.AddCell(setProps(new PdfPCell(new Phrase(apt.Attributes["Style"], times)), apt.CssClass.Replace("rsCategory", "")));
            pTable.AddCell(setProps(new PdfPCell(new Phrase(apt.Attributes["ActionTypeName"], times)), apt.CssClass.Replace("rsCategory", "")));
            pTable.AddCell(setPropsRight(new PdfPCell(new Phrase(apt.Attributes["DispositionDate"], times)), apt.CssClass.Replace("rsCategory", "")));
        }
        doc.Add(pTable);
    }

    private static void BuildWeekView(RadScheduler scheduler, Document doc)
    {
        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(5);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 104f, 104f, 104f, 104f, 104f };
        pTable.SetWidths(widths);

        PdfPCell cell = new PdfPCell(new Phrase("CALENDAR", times));
        cell.Colspan = 1;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.BorderWidthRight = 0f;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("WEEK VIEW, " + scheduler.VisibleRangeStart.ToString("MM/dd/yyyy") + " - " + scheduler.VisibleRangeEnd.ToString("MM/dd/yyyy"), times));
        cell.Colspan = 4;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.BorderWidthLeft = 0f;
        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        DateTime st = scheduler.VisibleRangeStart;
        for (int i = 0; i < 5; i++)
        {
            cell = new PdfPCell(new Phrase(st.DayOfWeek.ToString().Substring(0, 3) + "," + st.Day.ToString(), times));
            cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
            cell.HorizontalAlignment = 1;
            pTable.AddCell(cell);
            st = st.AddDays(1);
        }

        st = scheduler.VisibleRangeStart;
        for (int i = 0; i < 5; i++)
        {
            PdfPTable temp = new PdfPTable(2);

            cell = new PdfPCell(temp);

            List<Appointment> list = scheduler.Appointments.Where(it => it.Start.Date == st.Date && it.End.Date == st.Date).ToList();

            foreach (Appointment apt in list)
            {
                PdfPCell icell = new PdfPCell(new Phrase(apt.Attributes["County"], times));
                int nRows = (int)(apt.Duration.TotalMinutes / 5);
                icell.Rowspan = nRows;
                icell.Colspan = 1;
                icell.BorderWidthRight = 0f;
                temp.AddCell(icell);

                icell = new PdfPCell(new Phrase(apt.Subject, times));
                icell.Rowspan = nRows;
                icell.Colspan = 1;
                icell.BorderWidthLeft = 0f;
                temp.AddCell(icell);
            }
            temp.CompleteRow();
            pTable.AddCell(cell);
            st = st.AddDays(1);
        }
        doc.Add(pTable);
    }

    private static void BuildMonthView(RadScheduler scheduler, Document doc)
    {
        string month = scheduler.SelectedDate.ToString("MMM");
        Dictionary<DateTime, string> calendarData = new Dictionary<DateTime, string>();

        if (HttpContext.Current.Session["CalendarViewData"] != null)
        {
            if (((CalendarViewData)HttpContext.Current.Session["CalendarViewData"]).MonthViewData != null)
                calendarData = ((CalendarViewData)HttpContext.Current.Session["CalendarViewData"]).MonthViewData;
        }

        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);
        doc.Add(new Phrase("\n", times));

        PdfPTable pTable = new PdfPTable(5);
        pTable.HorizontalAlignment = Element.ALIGN_CENTER;
        pTable.TotalWidth = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
        pTable.LockedWidth = true;
        pTable.KeepTogether = true;

        float[] widths = new float[] { 104f, 104f, 104f, 104f, 104f };
        pTable.SetWidths(widths);

        PdfPCell cell = new PdfPCell(new Phrase("CALENDAR", times));
        cell.Colspan = 1;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.BorderWidthRight = 0f;
        cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);

        cell = new PdfPCell(new Phrase("CALENDAR MONTH VIEW, " + scheduler.SelectedDate.ToString("MMM") + ", " + scheduler.SelectedDate.Year.ToString(), times));
        cell.Colspan = 4;
        cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
        cell.BorderWidthLeft = 0f;
        cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right
        pTable.AddCell(cell);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        times = new Font(bfTimes, 8, Font.NORMAL, BaseColor.BLACK);

        DateTime st = scheduler.VisibleRangeStart;
        for (int i = 0; i < 5; i++)
        {
            cell = new PdfPCell(new Phrase(st.DayOfWeek.ToString().Substring(0, 3), times));
            cell.BackgroundColor = CMYKColor.LIGHT_GRAY;
            cell.HorizontalAlignment = 1;
            pTable.AddCell(cell);
            st = st.AddDays(1);
        }

        string label = "";
        string m = "";
        bool cmSet = false;
        for (DateTime i = scheduler.VisibleRangeStart; i < scheduler.VisibleRangeEnd; i = i.AddDays(1))
        {
            m = "";
            if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday)
            {
                PdfPTable temp = new PdfPTable(2);
                cell = new PdfPCell(temp);
                cell.FixedHeight = 72f;
                label = "";
                if (calendarData.ContainsKey(i.Date))
                {
                    label = calendarData[i.Date];
                }

                if (!cmSet)
                {
                    m = i.ToString("MMM");
                    m = (m != month ? m : "");
                    if (m == "")
                    {
                        m = month;
                        cmSet = true;
                    }
                }
                else if (cmSet && i.ToString("MMM") != month)
                    m = i.ToString("MMM");

                PdfPCell celH = new PdfPCell(new Phrase(i.Day.ToString() + " " + m + " " + label, times));
                celH.BackgroundColor = CMYKColor.LIGHT_GRAY;
                celH.HorizontalAlignment = 0;
                celH.Colspan = 2;
                temp.AddCell(celH);

                List<Appointment> list = scheduler.Appointments.Where(it => it.Start.Date == i.Date && it.End.Date == i.Date).ToList();

                for (int j = 0; j < 2; j++)
                {
                    Appointment apt = list.ElementAtOrDefault(j);
                    PdfPCell icell = new PdfPCell(new Phrase((apt != null) ? apt.Attributes["County"] : "", times));
                    icell.Colspan = 1;
                    icell.BorderWidthRight = 0f;
                    if (apt == null)
                        icell.FixedHeight = 15f;
                    icell.Border = 0;
                    temp.AddCell(icell);

                    icell = new PdfPCell(new Phrase((apt != null) ? apt.Subject : "", times));
                    icell.Colspan = 1;
                    icell.BorderWidthLeft = 0f;
                    if (apt == null)
                        icell.FixedHeight = 15f;
                    icell.Border = 0;
                    temp.AddCell(icell);
                }
                pTable.AddCell(cell);
            }
        }
        doc.Add(pTable);
    }

}
