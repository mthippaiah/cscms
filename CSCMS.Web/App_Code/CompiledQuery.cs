﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using CSCMS.Data;




/// <summary>
/// Summary description for CompiledQuery
/// </summary>
public static class CompiledQueryList
{
    public static readonly Func<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.ActionPerson>> GetActionPerson =
CompiledQuery.Compile<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.ActionPerson>>(
(CscmsContext, actionGuid, pid) => (from ap in CscmsContext.ActionPerson
                                    where ap.Action.ActionGUID == actionGuid && ap.Person.PersonGUID == pid
                                    select ap));

    public static readonly Func<CscmsEntities, int, IQueryable<CSCMS.Data.SpecialOrder>> GetSpecialOrderForSoId =
CompiledQuery.Compile<CscmsEntities, int, IQueryable<CSCMS.Data.SpecialOrder>>(
(CscmsContext, soid) => (from so in CscmsContext.SpecialOrder.Include("SpecialOrderType").Include("ActionPerson.Person")
                         where so.SpecialOrderID == soid
                         select so));

    public static readonly Func<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.SpecialOrder>> GetSpecialOrderWithArrestDate =
CompiledQuery.Compile<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.SpecialOrder>>(
  (CscmsContext, pid, actionGuid) => (from so in CscmsContext.SpecialOrder
                                      where so.ActionPerson.Action.ActionGUID == actionGuid && so.ActionPerson.Person.PersonGUID == pid
                                      && so.ArrestCommitmentEffectiveDate != null
                                      select so));


    public static readonly Func<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.SpecialOrder>> GetIncarceratedSpecialOrder =
CompiledQuery.Compile<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.SpecialOrder>>(
   (CscmsContext, pid, actionGuid) => (from so in CscmsContext.SpecialOrder
                                       where so.ActionPerson.ActionGUID == actionGuid && so.ActionPerson.PersonGUID == pid
                                       && so.ArrestCommitmentEffectiveDate != null && so.ReleaseDate == null
                                       select so).OrderByDescending(it => it.OrderedDate));


    public static readonly Func<CscmsEntities, Guid, Guid, DateTime?, IQueryable<CSCMS.Data.SpecialOrder>> GetWithDrawnSpecialOrder =
CompiledQuery.Compile<CscmsEntities, Guid, Guid, DateTime?, IQueryable<CSCMS.Data.SpecialOrder>>(
       (CscmsContext, pid, actionGuid, ordDate) => (from so in CscmsContext.SpecialOrder
                                                    where so.ActionPerson.ActionGUID == actionGuid && so.ActionPerson.PersonGUID == pid
                                                                   && so.OrderedDate >= ordDate
                                                                   && (so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_CAPIAS_WITHDRAWN ||
                                                                   so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_WRIT_WITHDRAWN)
                                                    select so).OrderByDescending(it => it.OrderedDate));


    public static readonly Func<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.SpecialOrder>> GetCapiasSpecialOrder =
CompiledQuery.Compile<CscmsEntities, Guid, Guid, IQueryable<CSCMS.Data.SpecialOrder>>(
       (CscmsContext, pid, actionGuid) => (from so in CscmsContext.SpecialOrder
                                           where so.ActionPerson.ActionGUID == actionGuid && so.ActionPerson.PersonGUID == pid
                               && so.ArrestCommitmentEffectiveDate == null && so.ReleaseDate == null && so.OrderedDate != null
                               && ((so.SpecialOrderType != null && (so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_CAPIAS_FTA ||
                               so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_CAPIAS_ATTACHMENT ||
                               so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_CAPIAS_MTRP ||
                               so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_CAPIAS_DEFERRED_COMMITMENT ||
                               so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_CAPIAS_COMPLIANCE ||
                               so.SpecialOrderType.SpecialOrderTypeName == MyConsts.DB_COMMITMENT)) || (so.SpecialOrderType == null && !string.IsNullOrEmpty(so.CommitmentOrCapias)))
                                           select so).OrderByDescending(it => it.OrderedDate));

    public static readonly Func<CscmsEntities, short, short, string, IQueryable<ActionResult>> GetCauseWithStatus =
CompiledQuery.Compile<CscmsEntities, short, short, string, IQueryable<ActionResult>>(
       (CscmsContext, ocaCourtID, countyID, status) => (from c in CscmsContext.Cause
                                                        where c.OCACourt.OCACourtID == ocaCourtID && c.County.CountyID == countyID && ((status != null && c.CauseStatus == status) || status == null)
                                                        orderby c.CauseNumber
                                                        select new ActionResult
                                                       {
                                                           CauseGUID = c.CauseGUID,
                                                           CauseNumber = c.CauseNumber,
                                                           ActionNumber = c.NumberOfAction

                                                       }).Take(100));

    public static readonly Func<CscmsEntities, IQueryable<CourtCountyResult>> GetCourtCountyResult =
CompiledQuery.Compile<CscmsEntities, IQueryable<CourtCountyResult>>(
       (CscmsContext) => (from c in CscmsContext.OCACourtCounty
                          where c.ExpirationDate == null || c.ExpirationDate > DateTime.Today
                          orderby c.OCACourtID, c.County.CountyName
                          select new CourtCountyResult
                          {
                              OCACourtID = c.OCACourtID,
                              CountyID = c.CountyID,
                              CountyName = c.County.CountyName
                          }));


    public static readonly Func<CscmsEntities, short, IQueryable<CourtCountyResult>> GetCourtCountyResultForJudgeOrCoordinator =
 CompiledQuery.Compile<CscmsEntities, short, IQueryable<CourtCountyResult>>(
        (CscmsContext, courtId) => (from c in CscmsContext.OCACourtCounty
                                    where c.OCACourtID == courtId && (c.ExpirationDate == null || c.ExpirationDate > DateTime.Today)
                                    orderby c.County.CountyName
                                    select new CourtCountyResult
                                    {
                                        OCACourtID = c.OCACourtID,
                                        CountyID = c.CountyID,
                                        CountyName = c.County.CountyName
                                    }));


    public static readonly Func<CscmsEntities, Guid, IQueryable<CSCMS.Data.CauseNote>> GetCauseNote =
  CompiledQuery.Compile<CscmsEntities, Guid, IQueryable<CSCMS.Data.CauseNote>>(
         (CscmsContext, causeGuid) => (from cn in CscmsContext.CauseNote
                                       join nt in CscmsContext.NoteType on cn.NoteTypeID equals nt.NoteTypeID
                                       where cn.CauseGUID == causeGuid
                                       orderby cn.NoteDate descending, cn.NoteID descending
                                       select cn));


    public static readonly Func<CscmsEntities, Guid, IQueryable<CaseNotesData>> GetCaseNotes =
   CompiledQuery.Compile<CscmsEntities, Guid, IQueryable<CaseNotesData>>(
          (CscmsContext, causeGuid) => (from cn in CscmsContext.CauseNote
                                        join nt in CscmsContext.NoteType on cn.NoteTypeID equals nt.NoteTypeID
                                        where cn.CauseGUID == causeGuid
                                        orderby cn.NoteDate descending, cn.NoteID descending
                                        select new CaseNotesData
                                        {
                                            NoteDate = cn.NoteDate,
                                            NoteText = cn.Note.NoteText,
                                            NoteID = cn.NoteID,
                                            NoteTypeID = cn.NoteTypeID
                                        }));

    public static readonly Func<CscmsEntities, short, IQueryable<CSCMS.Data.OCACourt>> GetUserCourt =
   CompiledQuery.Compile<CscmsEntities, short, IQueryable<CSCMS.Data.OCACourt>>(
          (CscmsContext, userId) => (from uc in CscmsContext.UserCourt
                                     join oc in CscmsContext.OCACourt on uc.OCACourt.OCACourtID equals oc.OCACourtID
                                     where uc.User.UserId == userId
                                     select oc).Distinct().OrderBy(it => it.CourtName));

    public static readonly Func<CscmsEntities, short, DateTime, DateTime, IQueryable<DateCounty>> GetDateHearing =
    CompiledQuery.Compile<CscmsEntities, short, DateTime, DateTime, IQueryable<DateCounty>>(
           (CscmsContext, ocaCourtId, st, et) => (from h in CscmsContext.Hearing
                                                  join a in CscmsContext.Action on h.Action.ActionGUID equals a.ActionGUID
                                                  join c in CscmsContext.Cause on a.Cause.CauseGUID equals c.CauseGUID
                                                  join cy in CscmsContext.County on c.County.CountyID equals cy.CountyID
                                                  where c.OCACourt.OCACourtID == ocaCourtId && h.HearingDate >= st && h.HearingDate <= et
                                                  select new DateCounty
                                                  {
                                                      HearingDate = h.HearingDate,
                                                      CountyName = cy.CountyName
                                                  }));


    public static readonly Func<CscmsEntities, Guid, short, IQueryable<CSCMS.Data.Hearing>> GetLatestHearing =
    CompiledQuery.Compile<CscmsEntities, Guid, short, IQueryable<CSCMS.Data.Hearing>>(
           (CscmsContext, causeGuid, numberOfAction) => (from h in CscmsContext.Hearing
                                                         join a in CscmsContext.Action on h.Action.ActionGUID equals a.ActionGUID
                                                         where a.Cause.CauseGUID == causeGuid && a.ActionNumber == numberOfAction
                                                         select h).OrderByDescending(it => it.HearingDate).Take(1));

    public static readonly Func<CscmsEntities, UserTicklerQueryData, IQueryable<CSCMS.Data.UserTicklerDismissed>> GetUserTicklerDismissed =
    CompiledQuery.Compile<CscmsEntities, UserTicklerQueryData, IQueryable<CSCMS.Data.UserTicklerDismissed>>(
           (CscmsContext, queryData) => (from it in CscmsContext.UserTicklerDismissed
                                         where it.OCACourt.OCACourtID == queryData.OcaCourtId && it.TicklerType.TicklerTypeID == queryData.TicklerTypeId
                                         && it.TriggerDate == queryData.TriggerDate && it.Tickler == queryData.TicklerMessage
                                         && it.ActionGUID == queryData.ActionGuid
                                         select it));

    public static readonly Func<CscmsEntities, short, IQueryable<CSCMS.Data.County>> GetUserCounties =
    CompiledQuery.Compile<CscmsEntities, short, IQueryable<CSCMS.Data.County>>(
           (CscmsContext, ocaCourtId) => (from ccy in CscmsContext.OCACourtCounty
                                          join cy in CscmsContext.County on ccy.County.CountyID equals cy.CountyID
                                          join uc in CscmsContext.UserCourt on ccy.OCACourt.OCACourtID equals uc.OCACourt.OCACourtID
                                          where ccy.OCACourt.OCACourtID == ocaCourtId
                                          select cy));


    public static readonly Func<CscmsEntities, short, short, IQueryable<CSCMS.Data.User>> GetUser =
    CompiledQuery.Compile<CscmsEntities, short, short, IQueryable<CSCMS.Data.User>>(
           (CscmsContext, ocaCourtId, userTypeId) => (from u in CscmsContext.User
                                                      join ucc in CscmsContext.UserCourt on u.UserId equals ucc.User.UserId
                                                      where ucc.OCACourt.OCACourtID == ocaCourtId && u.UserType.UserTypeID == userTypeId
                                                      select u));

    public static readonly Func<CscmsEntities, Guid, IQueryable<ActionHeaderData>> GetHeaderData =
     CompiledQuery.Compile<CscmsEntities, Guid, IQueryable<ActionHeaderData>>(
            (CscmsContext, actionGuid) => (from a in CscmsContext.Action
                                           join c in CscmsContext.Cause on a.Cause.CauseGUID equals c.CauseGUID
                                           join oca in CscmsContext.OCACourt on c.OCACourt.OCACourtID equals oca.OCACourtID
                                           where a.ActionGUID == actionGuid
                                           select new ActionHeaderData
                                           {
                                               Region = oca.Region.RegionName,
                                               CourtName = oca.CourtName,
                                               CountyName = c.County.CountyName,
                                               _FilingDate = a.ActionFiledDate,
                                               CauseNumber = c.CauseNumber,
                                               _ActionNumber = a.ActionNumber,
                                               _NumOfAction = c.NumberOfAction,
                                               ActionTypeName = a.ActionType.ActionTypeName
                                           }));

    public static readonly Func<CscmsEntities, short, DateTime, DateTime, IQueryable<CSCMS.Data.Hearing>> GetHearingForCourtAndDate =
    CompiledQuery.Compile<CscmsEntities, short, DateTime, DateTime, IQueryable<CSCMS.Data.Hearing>>(
            (CscmsContext, courtId, st, et) => (from h in CscmsContext.Hearing
                                                join x in CscmsContext.Action on h.Action.ActionGUID equals x.ActionGUID
                                                join cs in CscmsContext.Cause on x.Cause.CauseGUID equals cs.CauseGUID
                                                where cs.OCACourt.OCACourtID == courtId
                                                && h.HearingDate >= st && h.HearingDate <= et
                                                select h));

    public static readonly Func<CscmsEntities, short, int, IQueryable<HearingData>> GetHearingData =
    CompiledQuery.Compile<CscmsEntities, short, int, IQueryable<HearingData>>(
            (CscmsContext, courtId, dayCount) => (from h in CscmsContext.Hearing
                                                  join x in CscmsContext.Action on h.Action.ActionGUID equals x.ActionGUID
                                                  join cs in CscmsContext.Cause on x.Cause.CauseGUID equals cs.CauseGUID
                                                  join cy in CscmsContext.County on cs.County.CountyID equals cy.CountyID
                                                  join xt in CscmsContext.ActionType on x.ActionType.ActionTypeID equals xt.ActionTypeID
                                                  let lh = (from ht in CscmsContext.HearingType
                                                            where ht.HearingTypeID == h.HearingTypeID
                                                            select ht).FirstOrDefault()
                                                  let hd = (from h1 in CscmsContext.Hearing
                                                            where h1.Action.ActionGUID == h.Action.ActionGUID
                                                            select h1.HearingDate).OrderByDescending(it => it).FirstOrDefault()
                                                  let so = (from o in CscmsContext.SpecialOrder
                                                            where o.ActionPerson.Action.ActionGUID == h.Action.ActionGUID
                                                            && o.OrderedDate >= h.HearingDate
                                                            select o).FirstOrDefault()
                                                  let att = (from rp in CscmsContext.ActionPersonRelatedPerson
                                                             join ap in CscmsContext.ActionPerson on new { rp.ActionPerson.ActionGUID, rp.ActionPerson.Person.PersonGUID } equals new { ap.Action.ActionGUID, ap.Person.PersonGUID }
                                                             where rp.ActionPerson.Action.ActionGUID == h.Action.ActionGUID
                                                             && ap.Action.ActionGUID == h.Action.ActionGUID
                                                             && rp.PersonRelationshipType.PersonRelationshipTypeName == "Client-Attorney"
                                                             select rp).FirstOrDefault()
                                                  where cs.OCACourt.OCACourtID == courtId
                                                  select new HearingData
                                                  {
                                                      HearingID = h.HearingID,
                                                      ActionGUID = x.ActionGUID,
                                                      CauseGUID = cs.CauseGUID,
                                                      HearingDate = h.HearingDate,
                                                      HearingType = lh,
                                                      HearingOrder = h.HearingOrder,
                                                      CauseNumber = cs.CauseNumber,
                                                      ActionNumber = x.ActionNumber,
                                                      Style = x.Style,
                                                      CountyID = cy.CountyID,
                                                      CountyName = cy.CountyName,
                                                      CalendarColor = cy.CalendarColor,
                                                      MorningDocket = h.MorningDocket,
                                                      HearingTime = h.HearingTime,
                                                      ActionFiledDate = x.ActionFiledDate,
                                                      ActionTypeName = xt.ActionTypeName,
                                                      OAGCauseNumber = x.OAGCauseNumber,
                                                      DispositionDate = x.DispositionRenderedDate,
                                                      NextHearingDate = hd,
                                                      DayCount = dayCount,
                                                      SpecialOrder = so,
                                                      ClientAttorneyExists = (att != null)
                                                  }));
}


[Serializable]
public class UserTicklerQueryData
{
    public short OcaCourtId { get; set; }
    public byte TicklerTypeId { get; set; }
    public Guid ActionGuid { get; set; }
    public DateTime TriggerDate { get; set; }
    public string TicklerMessage { get; set; }
}

[Serializable]
public class CaseNotesData
{
    public DateTime NoteDate { get; set; }
    public string NoteText { get; set; }
    public long NoteID { get; set; }
    public byte NoteTypeID { get; set; }
}

[Serializable]
public class CourtCountyResult
{

    private short _OCACourtID;

    private short _CountyID;

    private string _CountyName;

    public CourtCountyResult()
    {
    }

    [global::System.Runtime.Serialization.DataMemberAttribute(Order = 1)]
    public short OCACourtID
    {
        get
        {
            return this._OCACourtID;
        }
        set
        {
            if ((this._OCACourtID != value))
            {
                this._OCACourtID = value;
            }
        }
    }

    [global::System.Runtime.Serialization.DataMemberAttribute(Order = 2)]
    public short CountyID
    {
        get
        {
            return this._CountyID;
        }
        set
        {
            if ((this._CountyID != value))
            {
                this._CountyID = value;
            }
        }
    }

    [global::System.Runtime.Serialization.DataMemberAttribute(Order = 3)]
    public string CountyName
    {
        get
        {
            return this._CountyName;
        }
        set
        {
            if ((this._CountyName != value))
            {
                this._CountyName = value;
            }
        }
    }
}

[Serializable]
public class ActionResult
{

    private System.Guid _CaseGUID;

    private string _CauseNumber;

    private System.Nullable<short> _ActionNumber;

    public ActionResult()
    {
    }

    [global::System.Runtime.Serialization.DataMemberAttribute(Order = 1)]
    public System.Guid CauseGUID
    {
        get
        {
            return this._CaseGUID;
        }
        set
        {
            if ((this._CaseGUID != value))
            {
                this._CaseGUID = value;
            }
        }
    }

    [global::System.Runtime.Serialization.DataMemberAttribute(Order = 2)]
    public string CauseNumber
    {
        get
        {
            return this._CauseNumber;
        }
        set
        {
            if ((this._CauseNumber != value))
            {
                this._CauseNumber = value;
            }
        }
    }

    [global::System.Runtime.Serialization.DataMemberAttribute(Order = 3)]
    public System.Nullable<short> ActionNumber
    {
        get
        {
            return this._ActionNumber;
        }
        set
        {
            if ((this._ActionNumber != value))
            {
                this._ActionNumber = value;
            }
        }
    }
}