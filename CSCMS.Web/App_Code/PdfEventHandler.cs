﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;

public class PdfPage : iTextSharp.text.pdf.PdfPageEventHelper
{
    public bool ShowPageCount { get; set; }

    protected Font footer
    {
        get
        {
            BaseColor grey = new BaseColor(128, 128, 128);
            Font font = FontFactory.GetFont("Arial", 9, Font.NORMAL, CMYKColor.BLACK);
            return font;
        }
    }
    public override void OnStartPage(PdfWriter writer, Document doc)
    {
        PdfPTable tbl = new PdfPTable(1);
        tbl.TotalWidth = doc.PageSize.Width - doc.RightMargin - doc.LeftMargin;
        tbl.LockedWidth = true;
        tbl.HorizontalAlignment = Element.ALIGN_LEFT;

        if (ShowPageCount)
        {
            Phrase p = new Phrase("Page " + doc.PageNumber, footer);
            PdfPCell cell = new PdfPCell(p);
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.Border = 0;
            tbl.AddCell(cell);
        }
        tbl.WriteSelectedRows(0, -1, 0, (doc.Top + 25), writer.DirectContent);
    }

    public override void OnEndPage(PdfWriter writer, Document doc)
    {
        //PdfPTable tbl = new PdfPTable(1);
        //tbl.TotalWidth = doc.PageSize.Width;
        //tbl.HorizontalAlignment = Element.ALIGN_CENTER;

        //if (ShowPageCount)
        //{
        //    Paragraph para = new Paragraph("Page " + doc.PageNumber, footer);
        //    float botMarg = doc.BottomMargin + 10;
        //    if (ShowPageCount) botMarg += 5;

        //    PdfPCell cell = new PdfPCell(para);
        //    cell.Border = 0;
        //    tbl.AddCell(cell);
        //}
        //tbl.WriteSelectedRows(0, -1, 0, (doc.BottomMargin + 10), writer.DirectContent);
    }
}

