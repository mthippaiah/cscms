﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace CSCMS.WebControls
{
    [PersistChildren(true), ParseChildren(false)]
    public class SectionContainer : WebControl
    {
        #region Private Fields

        private string headerText;
        private DisplayType displayType = DisplayType.Standard;
        private bool enableMinimize = true;
        private bool minimized = false;
        private Color mainBackColor;

        #endregion

        #region Public / Protected Methods

        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);
            string cssClass;

            switch (this.displayType)
            {
                case DisplayType.Note:
                    cssClass = "sectionContainerNote";
                    break;

                case DisplayType.NoPadding:
                    cssClass = "sectionContainerNoPadding";
                    break;

                case DisplayType.Grid:
                    cssClass = "sectionContainerGrid";
                    break;

                default:
                    cssClass = "sectionContainer";
                    break;
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "1");

            if (!this.Width.IsEmpty)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Width, this.Width.ToString());
            }

            if (!this.Height.IsEmpty)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Height, this.Height.ToString());
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            if (!string.IsNullOrEmpty(this.headerText))
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write(this.headerText);
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
                writer.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, "right");
                writer.RenderBeginTag(HtmlTextWriterTag.Th);

                if (this.enableMinimize)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, this.UniqueID + "$lnkToggle");
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
                    writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "ToggleSection(this);");
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, this.UniqueID + "$imgToggle");

                    if (this.minimized)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, "../images/plus.png");
                        writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Maximize");
                    }
                    else
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Src, "../images/minus.png");
                        writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Minimize");
                    }

                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag(); // IMG
                    writer.RenderEndTag(); // A
                }

                writer.Write("&nbsp;");
                writer.RenderEndTag(); // 2nd TH
                writer.RenderEndTag(); // TR
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.UniqueID + "$trToggle");

            if (this.minimized)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
            writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");

            if (!this.mainBackColor.IsEmpty)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, ColorTranslator.ToHtml(this.mainBackColor));
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
        }

        public override void RenderEndTag(HtmlTextWriter writer)
        {
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();

            base.RenderEndTag(writer);
        }

        #endregion

        #region Public Properties

        public string HeaderText
        {
            get { return headerText; }
            set { headerText = value; }
        }

        public DisplayType DisplayType
        {
            get { return displayType; }
            set { displayType = value; }
        }

        public bool EnableMinimize
        {
            get { return enableMinimize; }
            set { enableMinimize = value; }
        }

        public bool Minimized
        {
            get { return minimized; }
            set { minimized = value; }
        }

        public Color MainBackColor
        {
            get { return mainBackColor; }
            set { mainBackColor = value; }
        }

        #endregion
    }

    public enum DisplayType
    {
        Standard,
        Note,
        NoPadding,
        Grid
    }
}