using System;
using System.Web.UI;

namespace CSCMS.WebControls
{
    /// <summary>
    /// Control used to manage success, error, and other info messages.
    /// </summary>
    public class MessageBox : Control
    {
        #region Private Fields

        private string m_SuccessMessage;
        private string m_ErrorMessage;
        private string m_InfoMessage;
        private bool m_Fade;

        #endregion

        #region Constructor

        public MessageBox()
        {

        }

        #endregion

        #region Protected Methods

        protected override void Render(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(this.m_SuccessMessage) || !string.IsNullOrEmpty(this.m_ErrorMessage) || !string.IsNullOrEmpty(this.m_InfoMessage))
            {
                string divUniqueId = this.UniqueID + "$messageBoxDiv";
                writer.WriteLine("<div style=\"clear: both\"></div><div id=\"" + divUniqueId + "\"><div class=\"messagebox_top\"></div><div class=\"messagebox_mid\"><ul class=\"messageList\">");

                if (!string.IsNullOrEmpty(this.m_SuccessMessage))
                {
                    writer.Write(string.Format("<li class=\"successMsg\">{0}</li>", this.m_SuccessMessage));
                }

                if (!string.IsNullOrEmpty(this.m_ErrorMessage))
                {
                    writer.Write(string.Format("<li class=\"errorMsg\">{0}</li>", this.m_ErrorMessage));
                }

                if (!string.IsNullOrEmpty(this.m_InfoMessage))
                {
                    writer.Write(string.Format("<li class=\"infoMsg\">{0}</li>", this.m_InfoMessage));
                }

                writer.Write("</ul></div><div class=\"messagebox_lwr\"></div></div>");

                if ((string.IsNullOrEmpty(this.m_ErrorMessage) && string.IsNullOrEmpty(this.m_InfoMessage)) || this.Fade)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "MessageBoxHide", "setTimeout( function() { document.getElementById('" + divUniqueId + "').style.display = 'none'; }, 5000 );", true);
                }
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a message indicating success.
        /// </summary>
        public string SuccessMessage
        {
            get { return this.m_SuccessMessage; }
            set { this.m_SuccessMessage = value; }
        }

        /// <summary>
        /// Gets or sets a message indicating error.
        /// </summary>
        public string ErrorMessage
        {
            get { return this.m_ErrorMessage; }
            set { this.m_ErrorMessage = value; }
        }

        /// <summary>
        /// Gets or sets an informational message.
        /// </summary>
        public string InfoMessage
        {
            get { return this.m_InfoMessage; }
            set { this.m_InfoMessage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the control should fade away
        /// </summary>
        public bool Fade
        {
            get { return this.m_Fade; }
            set { this.m_Fade = value; }
        }

        #endregion
    }
}