﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PersonMerge.ascx.cs" Inherits="UserControls_PersonMerge" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
    <table width="100%">
        <tr>
            <td class="fieldLabel">
                Person:
            </td>
            <td>
                <asp:DropDownList ID="ddlPerson" runat="server" Width="200" AutoPostBack="true" OnSelectedIndexChanged="ddlPerson_SelectedIndexChanged" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                First Name:
            </td>
            <td>
                <asp:Label ID="lblFirstName" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Last Name:
            </td>
            <td>
                <asp:Label ID="lblLastName" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Middle Name:
            </td>
            <td>
                <asp:Label ID="lblMiddleName" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Sex:
            </td>
            <td>
                <asp:Label ID="lblSex" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Birth Date:
            </td>
            <td>
                <asp:Label ID="lblBirthDate" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Address 1:
            </td>
            <td>
                <asp:Label ID="lblAddress1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Address 2:
            </td>
            <td>
                <asp:Label ID="lblAddress2" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Address 3:
            </td>
            <td>
                <asp:Label ID="lblAddress3" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                City:
            </td>
            <td>
                <asp:Label ID="lblCity" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                State:
            </td>
            <td>
                <asp:Label ID="lblState" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Zip:
            </td>
            <td>
                <asp:Label ID="lblZip" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Firm:
            </td>
            <td>
                <asp:Label ID="lblFirm" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="fieldLabel">
                Bar Card No:
            </td>
            <td>
                <asp:Label ID="lblBarCardNo" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <!-- grdCases -->
                <telerik:RadGrid ID="grdCases" runat="server" AllowPaging="True" PageSize="10" AllowSorting="True">
                    <MasterTableView NoMasterRecordsText="Your search found no results. Try broadening your search criteria.">
                        <Columns>
                            <telerik:GridHyperLinkColumn HeaderText="Cause Number" DataNavigateUrlFields="CauseGUID, NumberOfAction"
                                DataNavigateUrlFormatString="../MemberPages/ActionDetail.aspx?cid={0}&a={1}" SortExpression="CauseNumber"
                                DataTextField="CauseNumber" ItemStyle-Font-Bold="true" />
                            <telerik:GridBoundColumn HeaderText="Style" DataField="Style" />
                            <telerik:GridBoundColumn HeaderText="OAG" DataField="OAGCauseNumber" />

                        </Columns>
                    </MasterTableView>
                    <PagerStyle AlwaysVisible="true" />
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px"
    Transparency="30">
    <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
        style="border: 0px; padding-top: 90px;" />
</telerik:RadAjaxLoadingPanel>
<asp:SqlDataSource ID="dsPersons" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
    SelectCommand="SearchCaseByPersonGUID" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
    OnSelecting="dsPersons_Selecting">
    <SelectParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
        <asp:Parameter Name="PersonGUID" Type="string" />
    </SelectParameters>
</asp:SqlDataSource>
