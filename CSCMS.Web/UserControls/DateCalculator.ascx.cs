﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class UserControls_DateCalculator : System.Web.UI.UserControl
{
    public event EventHandler DateSetClicked;
    DateTime? selectedDate = null;

    protected void ButtonSetCalendar_Click(object sender, EventArgs e)
    {
        selectedDate = CalculateDate();
        if (DateSetClicked != null)
            DateSetClicked(sender, e);
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        selectedDate = dtRadDatePicker.SelectedDate;
        if (DateSetClicked != null)
            DateSetClicked(sender, e);
    }

    private DateTime CalculateDate()
    {
        DateAnchorType? danchor = null;
        if (!string.IsNullOrEmpty(RadComboBoxAnchorType.Text))
        {
            danchor = (DateAnchorType)Enum.Parse(typeof(DateAnchorType), RadComboBoxAnchorType.SelectedValue);
        }

        DateScaleType? dscale = null;
        if (!string.IsNullOrEmpty(RadComboBoxScaleType.Text))
        {
            dscale = (DateScaleType)Enum.Parse(typeof(DateScaleType), RadComboBoxScaleType.SelectedValue);
        }

        long? dateValue = (long)this.RadNumericTextBoxScaleValue.Value;
        if (this.RadComboBoxScaleDirection.Text == "-")
            dateValue = -dateValue;


        DateTime? setDate = null;
        if (danchor != null && dscale != null && dateValue != null)
        {
            setDate = DateHelper.EvaluateDate(danchor.Value, dscale.Value, dateValue.Value, false);
        }

        //if (!string.IsNullOrEmpty(this.RadTimePickerCalTime.DateInput.Text))
        //{
        //    setDate = new DateTime(setDate.Value.Year,
        //                setDate.Value.Month,
        //                setDate.Value.Day,
        //                RadTimePickerCalTime.SelectedDate.Value.Hour,
        //                RadTimePickerCalTime.SelectedDate.Value.Minute,
        //                RadTimePickerCalTime.SelectedDate.Value.Second);
        //}
        return setDate.Value;
    }

    public DateTime GetDate()
    {
        if (selectedDate.HasValue)
            return selectedDate.Value;
        else
            return DateTime.Now;
    }

}
