﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSCMS.WebControls;

public partial class UserControls_CaseNotes : BaseUserControl
{
    protected const int MAX_NOTE_LENGTH = 205;
    bool saveNoteClicked = false;
    protected bool NewNote
    {
        get { return Convert.ToBoolean(ViewState["NewNote"]); }
        set { ViewState["NewNote"] = value; }
    }

    protected int NoteID
    {
        get { return Convert.ToInt32(ViewState["NoteID"]); }
        set { ViewState["NoteID"] = value; }
    }

    public void SetCaseNoteSize()
    {
        this.btnCreateNewNote.Visible = false;
        this.btnDeleteNote.Visible = false;
        this.btnSaveNote.Visible = false;
        this.txtCaseNote.Height = Unit.Pixel(500);

    }

    public bool WasSaved
    {
        get
        {
            return saveNoteClicked;
        }
    }

    public void SetFocus()
    {
        if (WasSaved)
            txtCaseNote.Focus();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["po"] == "1")
            {
                return;
            }

            dtNoteDate.MaxDate = DateTime.Today;

            if (!string.IsNullOrEmpty(Request.QueryString["cid"]))
            {
                if (this.Page.CurrentCause == null || this.Page.CurrentCause.CauseGUID != new Guid(Request.QueryString["cid"]))
                {
                    this.Page.LoadCause(new Guid(Request.QueryString["cid"]), short.Parse(Request.QueryString["a"]));
                }
            }

            if (this.Page.CurrentCause.CauseNote.Count == 0)
            {
                lblNoNotes.Visible = true;
                PrepareForNewNote();
            }
            else
            {
                SetEditableNote();
            }
            if (this.Page.CurrentUser.ViewAllUser && Convert.ToBoolean(Session["Impersonating"]) == false) btnSaveNote.Visible = false;
            btnDeleteNote.Visible = btnSaveNote.Visible;
            btnCreateNewNote.Visible = btnSaveNote.Visible;

            SetCaseNotesListViewData();

            this.DataBind();
        }
    }

    private void SetCaseNotesListViewData()
    {
        IQueryable<CaseNotesData> notes = CompiledQueryList.GetCaseNotes.Invoke(((BasePage)Page).CscmsContext, this.Page.CurrentCause.CauseGUID);

        if (this.NewNote)
        {
            lvCaseNotes.DataSource = notes;
        }
        else
        {
            // Skip the first note since it will be displayed in the text box
            lvCaseNotes.DataSource = notes.Skip(1);
        }
    }

    public void SaveNote()
    {
        btnSaveNote_Click(null, null);
    }


    protected void btnSaveNote_Click(object sender, EventArgs e)
    {
        saveNoteClicked = true;
        CSCMS.Data.CauseNote causeNote = null;
        bool iOpnedConn = false;
        try
        {
            if (((BasePage)Page).CscmsContext.Connection.State != System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Open();
                iOpnedConn = true;
            }

            if (this.NewNote)
            {
                causeNote = new CSCMS.Data.CauseNote();
                causeNote.Cause = ((BasePage)Page).CscmsContext.Cause.Where(it => it.CauseGUID == this.Page.CurrentCause.CauseGUID).FirstOrDefault();
                byte noteTypeId = (byte)NoteTypeEnum.CaseNote;
                causeNote.NoteType = ((BasePage)Page).CscmsContext.NoteType.Where(it => it.NoteTypeID == noteTypeId).FirstOrDefault();
                causeNote.CreateDate = DateTime.Now;
                causeNote.LastUpdateDate = DateTime.Now;

                causeNote.Note = new CSCMS.Data.Note();
                causeNote.Note.CreateDate = DateTime.Now;
                causeNote.Note.LastUpdateDate = DateTime.Now;
                causeNote.NoteDate = dtNoteDate.SelectedDate.Value;

                ((BasePage)Page).CscmsContext.AddToCauseNote(causeNote);
            }
            else
            {
                causeNote = ((BasePage)Page).CscmsContext.CauseNote.Include("Note").Where(it => it.Note.NoteID == this.NoteID).FirstOrDefault();
            }

            if (causeNote != null && causeNote.Note != null)
                causeNote.Note.NoteText = txtCaseNote.Text;

            ((BasePage)Page).CscmsContext.SaveChanges();

            this.NewNote = false;
            string actionNum = Request.QueryString["a"];
            if (string.IsNullOrEmpty(actionNum))
                actionNum = ((BasePage)Page).CurrentAction.ActionNumber.ToString();

            this.Page.LoadCause(this.Page.CurrentCause.CauseGUID, short.Parse(actionNum));
            SetEditableNote();
        }
        finally
        {
            if (iOpnedConn && ((BasePage)Page).CscmsContext.Connection.State == System.Data.ConnectionState.Open)
                ((BasePage)Page).CscmsContext.Connection.Close();
        }
    }

    protected void btnCreateNewNote_Click(object sender, EventArgs e)
    {
        PrepareForNewNote();
        SetCaseNotesListViewData();
        lvCaseNotes.DataBind();
    }

    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        bool iOpnedConn = false;
        try
        {
            if (((BasePage)Page).CscmsContext.Connection.State != System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Open();
                iOpnedConn = true;
            }
            long noteId = (long)Convert.ToDecimal(e.CommandArgument);
            CSCMS.Data.CauseNote causeNote = ((BasePage)Page).CscmsContext.CauseNote.Include("Note").Where(it => it.Note.NoteID == noteId).FirstOrDefault();
            ((BasePage)Page).CscmsContext.DeleteObject(causeNote.Note);
            ((BasePage)Page).CscmsContext.DeleteObject(causeNote);
            ((BasePage)Page).CscmsContext.SaveChanges();

            string actionNum = Request.QueryString["a"];
            if (string.IsNullOrEmpty(actionNum))
                actionNum = ((BasePage)Page).CurrentAction.ActionNumber.ToString();

            this.Page.LoadCause(this.Page.CurrentCause.CauseGUID, short.Parse(actionNum));
            SetCaseNotesListViewData();
            lvCaseNotes.DataBind();
        }
        finally
        {
            if (iOpnedConn && ((BasePage)Page).CscmsContext.Connection.State == System.Data.ConnectionState.Open)
                ((BasePage)Page).CscmsContext.Connection.Close();
        }
    }

    protected void btnDeleteNote_Click(object sender, EventArgs e)
    {
        bool iOpnedConn = false;
        try
        {
            if (((BasePage)Page).CscmsContext.Connection.State != System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Open();
                iOpnedConn = true;
            }
            CSCMS.Data.CauseNote causeNote = ((BasePage)Page).CscmsContext.CauseNote.Include("Note").Where(it => it.Note.NoteID == this.NoteID).FirstOrDefault();
            if (causeNote != null)
            {
                if (causeNote.Note != null)
                    ((BasePage)Page).CscmsContext.DeleteObject(causeNote.Note);

                ((BasePage)Page).CscmsContext.DeleteObject(causeNote);
                ((BasePage)Page).CscmsContext.SaveChanges();
            }

            this.Page.LoadCause(this.Page.CurrentCause.CauseGUID, short.Parse(Request.QueryString["a"]));
            SetCaseNotesListViewData();
            lvCaseNotes.DataBind();
            SetEditableNote();
        }
        finally
        {
            if (iOpnedConn && ((BasePage)Page).CscmsContext.Connection.State == System.Data.ConnectionState.Open)
                ((BasePage)Page).CscmsContext.Connection.Close();
        }
    }

    protected string GetFormattedNote(string note, bool shortNote)
    {
        if (shortNote)
        {
            if (note.Length > MAX_NOTE_LENGTH)
            {
                note = note.Substring(0, MAX_NOTE_LENGTH - 3) + "...";
            }
        }
        else
        {
            note = note.Replace(Environment.NewLine, "<br>");
        }

        return note;
    }

    private void PrepareForNewNote()
    {
        this.NewNote = true;
        btnCreateNewNote.Visible = false;
        btnDeleteNote.Visible = false;
        txtCaseNote.Text = string.Empty;
        dtNoteDate.SelectedDate = DateTime.Today;
    }

    public void SetEditableNote()
    {
        IQueryable<CSCMS.Data.CauseNote> queryCauseNote = CompiledQueryList.GetCauseNote.Invoke(((BasePage)Page).CscmsContext, this.Page.CurrentCause.CauseGUID);

        CSCMS.Data.CauseNote causeNote = queryCauseNote.FirstOrDefault();
        if (causeNote != null)
        {
            if (causeNote.NoteDate > DateTime.Today)
            {
                dtNoteDate.MaxDate = causeNote.NoteDate;
            }
            if (!causeNote.NoteReference.IsLoaded)
                causeNote.NoteReference.Load();

            txtCaseNote.Text = causeNote.Note.NoteText;
            dtNoteDate.SelectedDate = causeNote.NoteDate;
            this.NoteID = Convert.ToInt32(causeNote.Note.NoteID);
            this.NewNote = false;
            btnCreateNewNote.Visible = true;
            lblNoNotes.Visible = false;
            btnDeleteNote.Visible = true;
        }
    }
}