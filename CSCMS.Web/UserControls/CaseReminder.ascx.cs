﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSCMS.WebControls;
using System.Xml.Linq;
using Telerik.Web.UI;

using CSCMS.Data;

public partial class UserControls_CaseReminder : BaseUserControl
{
    private bool readOnly = false;
    private bool showExpired = false;
    public bool ReadOnly
    {
        get { return readOnly; }
        set { readOnly = value; }
    }

    private int existingID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (this.Page.CurrentCause != null) txtCauseNumber.Text = this.Page.CurrentCause.CauseNumber;

            if (!string.IsNullOrEmpty(Request.QueryString["rid"]))
            {
                existingID = int.Parse(Request.QueryString["rid"].ToString());


                CSCMS.Data.Reminder reminder = ((BasePage)Page).CscmsContext.Reminder.Where(it => it.ReminderID == existingID && (it.User.UserId == this.Page.CurrentUser.UserID || it.OCACourtID == this.Page.CurrentUser.OCACourtID)).FirstOrDefault();

                if (reminder != null)
                {
                    btnAddReminder.Text = "Update";

                    dtStartDate.SelectedDate = reminder.StartDate;
                    dtEffectiveDate.SelectedDate = reminder.EffectiveDate;
                    txtReminder.Text = reminder.ReminderNote;
                    //chkCourtReminder.Checked = reminder.CauseGUID == null ? false : true;

                    if (reminder.CauseGUID != null)
                    {
                        CSCMS.Data.Cause cause = ((BasePage)Page).CscmsContext.Cause.Where(p => p.CauseGUID == reminder.CauseGUID).FirstOrDefault();
                        if (cause != null) txtCauseNumber.Text = cause.CauseNumber;
                    }
                }
                else
                {
                    Response.Redirect("Reminder.aspx", true);
                }

            }
        }
    }

    protected void grdReminder_DeleteCommand(object source, GridCommandEventArgs e)
    {
        Int32 reminderID = Convert.ToInt32(grdReminder.MasterTableView.DataKeyValues[e.Item.ItemIndex]["ReminderID"]);
        using (CscmsEntities dc = new CscmsEntities())
        {
            dc.Connection.Open();
            CSCMS.Data.Reminder reminder = dc.Reminder.Where(p => p.ReminderID == reminderID).FirstOrDefault();
            if (reminder != null)
            {
                dc.DeleteObject(reminder);
                dc.SaveChanges();
            }
        }
        Response.Redirect("Reminder.aspx?message=Delete", true);
    }

    protected void btnAddReminder_Click(object sender, EventArgs e)
    {
        bool iOpenedCon = false;
        try
        {
            if (((BasePage)Page).CscmsContext.Connection.State != System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Open();
                iOpenedCon = true;
            }

            CSCMS.Data.Reminder reminder;

            //reminder.CauseGUID = this.Page.CurrentCause.CauseGUID;

            if (btnAddReminder.Text == "Update")
            {
                existingID = int.Parse(Request.QueryString["rid"].ToString());
                reminder = ((BasePage)Page).CscmsContext.Reminder.Where(p => p.ReminderID == existingID && (p.User.UserId == this.Page.CurrentUser.UserID || p.OCACourtID == this.Page.CurrentUser.OCACourtID)).FirstOrDefault();
            }
            else
            {
                reminder = new CSCMS.Data.Reminder();
                ((BasePage)Page).CscmsContext.AddToReminder(reminder);
            }

            reminder.CreateDate = DateTime.Now;
            reminder.LastUpdateDate = DateTime.Now;
            reminder.User = ((BasePage)Page).CscmsContext.User.Where(it => it.UserId == this.Page.CurrentUser.UserID).FirstOrDefault();
            reminder.ReminderNote = txtReminder.Text;
            reminder.EffectiveDate = dtEffectiveDate.SelectedDate;
            reminder.StartDate = this.dtStartDate.SelectedDate;

            if (this.Page != null && this.Page is BasePage)
                reminder.OCACourtID = ((BasePage)this.Page).CurrentUser.OCACourtID;

            if (!string.IsNullOrEmpty(txtCauseNumber.Text.Trim()))
            {
                if (this.Page.CurrentCause != null && txtCauseNumber.Text.Trim().ToLower() == this.Page.CurrentCause.CauseNumber.Trim().ToLower())
                {
                    reminder.CauseGUID = this.Page.CurrentCause.CauseGUID;
                }
                else
                {
                    CSCMS.Data.Cause cause = ((BasePage)Page).CscmsContext.Cause.Where(it => it.CauseNumber == txtCauseNumber.Text && it.OCACourt.OCACourtID == this.Page.CurrentUser.OCACourtID).FirstOrDefault();

                    if (cause == null)
                    {
                        cvtxtCauseNumber.IsValid = false;
                        return;
                    }
                    else
                    {
                        reminder.CauseGUID = cause.CauseGUID;
                    }
                }
            }

            ((BasePage)Page).CscmsContext.SaveChanges();


            if (btnAddReminder.Text == "Update")
                Response.Redirect("Reminder.aspx?message=Update", true);
            else
                Response.Redirect("Reminder.aspx?message=Add", true);
        }
        finally
        {
            if (iOpenedCon && ((BasePage)Page).CscmsContext.Connection.State == System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Close();
            }
        }
    }

    protected void dsReminder_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@OCACourtID"].Value = this.Page.CurrentUser.OCACourtID.ToString();
        e.Command.Parameters["@UserID"].Value = this.Page.CurrentUser.UserID.ToString();
        e.Command.Parameters["@All"].Value = true;
        e.Command.Parameters["@ShowExpried"].Value = showExpired;
    }
    protected void ButtonIncludeExpired_Click(object sender, EventArgs e)
    {
        if (this.ButtonIncludeExpired.Text == "Show Expired")
        {
            this.showExpired = true;
            ButtonIncludeExpired.Text = "Hide Expired";
        }
        else if (this.ButtonIncludeExpired.Text == "Hide Expired")
        {
            this.showExpired = false;
            ButtonIncludeExpired.Text = "Show Expired";
        }
        grdReminder.Rebind();
    }
}