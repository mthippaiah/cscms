﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class UserControls_ReportList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadReports();

            foreach (RadTreeNode myNodes in tree.GetAllNodes())
            {
                myNodes.Attributes["target"] = "_blank";
            } 
        }
    }

    protected void LoadReports()
    {

        //RadTreeNode caseNode = new RadTreeNode("Case", "Case");
        //tree.Nodes.Add(caseNode);

        //caseNode.Expanded = true;
        //caseNode.Nodes.Add(new RadTreeNode("Case", "Case1"));
        //caseNode.Nodes.Add(new RadTreeNode("Relationships (Genogram) Diagram", "Case2"));
        //caseNode.Nodes.Add(new RadTreeNode("Case Summary", "Case3", "~/MemberPages/CaseSummaryReport.aspx"));

        RadTreeNode courtNode = new RadTreeNode("Court", "Court");
        tree.Nodes.Add(courtNode);

        courtNode.Expanded = true;
        courtNode.Nodes.Add(new RadTreeNode("Child Support Docket (Public)", "Court1", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.CHILDSUPPORTDOCKETPUBLIC));
        courtNode.Nodes.Add(new RadTreeNode("Child Support Judge's Docket", "Court2", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.CHILDSUPPORTDOCKET));
        courtNode.Nodes.Add(new RadTreeNode("Pending Cases With Age", "Court3", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.PENDINGCASESWITHAGES));
        courtNode.Nodes.Add(new RadTreeNode("Pending Expedited Cases Lacking Service", "Court4", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.CASESLACKINGSERVICE));
        courtNode.Nodes.Add(new RadTreeNode("Summary Report On Expedited Process Compliance", "Court5", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.SUMMARYREPORTONEXPEDITEDPROCESSCOMPLIANCE));
        courtNode.Nodes.Add(new RadTreeNode("General Court Statistics Child Support", "Court6", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.GENERALCOURTSTATISTICSCHILDSUPPORT));
        courtNode.Nodes.Add(new RadTreeNode("Child Support Docket", "Court7", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.CHILDSUPPORTDOCKET_2));
        courtNode.Nodes.Add(new RadTreeNode("Child Support Docket With Page Break", "Court7", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.CHILDSUPPORTDOCKETWITHPAGEBREAK));
        courtNode.Nodes.Add(new RadTreeNode("Pending Case Age In Days And Pending Rate", "Court8", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.PENDINGCASEAGEINDAYSANDPENDINGRATE));
        courtNode.Nodes.Add(new RadTreeNode("Outstanding Capias", "Court8", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.OUTSTANDING_CAPIAS));
        courtNode.Nodes.Add(new RadTreeNode("Incarcerated Parties", "Court8", "~/MemberPages/ReportView.aspx?" + MyConsts.QUERY_RID + "=" + MyConsts.INCARCERATED_PARTIES));

        //RadTreeNode outComeNode = new RadTreeNode("Outcome Measures (Courts)", "OutCome");
        //tree.Nodes.Add(outComeNode);

        //outComeNode.Expanded = true;


        //outComeNode.Nodes.Add(new RadTreeNode(@"Case & Child Workload Measures by [Court & Measures / Month] and by [Measure & Courts / Month]", "OutCome2"));

        //if (this.CurrentUser.UserType == UserType.OCAAdmin)
        //{
        //    RadTreeNode systemNode = new RadTreeNode("System Administrator", "System");
        //    tree.Nodes.Add(systemNode);

        //    systemNode.Expanded = true;
        //    systemNode.Nodes.Add(new RadTreeNode("Report Usage", "System1", "~/MemberPages/ReportView.aspx?cid=B878CDB9-813C-4529-9E4E-003C63C9F6A1"));
        //    systemNode.Nodes.Add(new RadTreeNode("System Usage", "System2", "~/MemberPages/ReportView.aspx?cid=b8d3703e-4b6b-4671-a38d-0042c020433e"));
        //    systemNode.Nodes.Add(new RadTreeNode("Court & User Configuration", "System3", "~/MemberPages/ReportView.aspx?cid=053FE7EC-00B4-4D3C-99FE-0036E0125893"));

        //}

    }

    protected void tree_NodeExpand(object sender, RadTreeNodeEventArgs e)
    {
    }
}
