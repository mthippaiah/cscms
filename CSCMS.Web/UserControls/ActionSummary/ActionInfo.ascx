﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActionInfo.ascx.cs" Inherits="UserControls_ActionSummary_ActionInfo" %>
<CSCMSUC:SectionContainer ID="sectionCause" runat="server" Width="100%" DisplayType="Note">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td valign="top">
                <table cellspacing="0" border="0" width="100%" class="tableSmallFont" style="margin-right: 30px;">
                    <tr>
                        <td valign="top" style="width: 20%">
                            <table cellspacing="0">
                                <tr>
                                    <td class="fieldLabel">
                                        Region:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRegion" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Court:
                                    </td>
                                    <td>
                                        <%= this.CurrentUser.OCACourtName %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Judge:
                                    </td>
                                    <td>
                                        <%= this.CurrentUser.OCACourtJudge %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Filing Date:
                                    </td>
                                    <td>
                                        <%= ((DateTime)(this.CurrentAction.ActionFiledDate)).ToString("MM/dd/yyyy") %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 46%">
                            <table cellspacing="0">
                                <tr>
                                    <td class="fieldLabel">
                                        Action:
                                    </td>
                                    <td>
                                        <%= this.CurrentAction.ActionNumber %>
                                        /
                                        <%= this.CurrentCause.NumberOfAction %>
                                        &nbsp&nbsp-&nbsp
                                        <asp:Label ID="lblActionCategory" runat="server" />
                                        &nbsp-&nbsp
                                        <asp:Label ID="lblActionName" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        County:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCounty" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Cause No:
                                    </td>
                                    <td>
                                        <a href="ActionDetail.aspx?cid=<%= this.CurrentCause.CauseGUID %>&a=<%= this.CurrentAction.ActionNumber %>">
                                            <%= this.CurrentCause.CauseNumber %></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 34%">
                            <table cellspacing="0">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="text-align: right">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnRefreshHeader" runat="server" CssClass="submitMedium" Text="Refresh"
                                            OnClick="btnRefreshHeader_Click" CausesValidation="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Active/Inactive:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatus" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Pending/Disposed:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPending" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Action Age (in days):
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAging" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</CSCMSUC:SectionContainer>
<telerik:RadAjaxManagerProxy ID="AjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnRefreshHeader">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="sectionCause" LoadingPanelID="loadingPanel" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Height="75px" Width="75px">
    <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
        style="border: 0px; padding-top: 50px;" />
</telerik:RadAjaxLoadingPanel>
