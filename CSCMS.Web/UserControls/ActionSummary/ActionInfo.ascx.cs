﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Linq;

public partial class UserControls_ActionSummary_ActionInfo : BaseUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {
        ActionHeaderData data = Helper.GetActionHeaderData(this.CurrentAction.ActionGUID);
        if (data != null)
        {
            lblRegion.Text = data.Region;
            lblAging.Text = data.ActionAge;
            lblPending.Text = data.PendingStatus;
            lblStatus.Text = data.ActiveStatus;
            lblCounty.Text = data.CountyName;
            lblActionName.Text = this.CurrentAction.ActionType.ActionTypeName;
            lblActionCategory.Text = this.CurrentAction.ActionType.ActionCategory.ActionCategoryName;
        }
    }

    protected void btnRefreshHeader_Click(object sender, EventArgs args)
    {
        Session["ActionHeaderData"] = null;
        LoadData();
    }

    public OCAUser CurrentUser
    {
        get
        {
            return Session["CurrentUser"] as OCAUser;
        }
    }

    private Guid caseGuid;
    public Guid CauseGUID
    {
        get { return caseGuid; }
        set { caseGuid = value; }
    }

    private Guid actionGuid;
    public Guid ActionGuid
    {
        get { return actionGuid; }
        set { actionGuid = value; }
    }

    private short actionNumber;

    public short ActionNumber
    {
        get { return actionNumber; }
        set { actionNumber = value; }
    }


    private CSCMS.Data.Cause _cause;
    public CSCMS.Data.Cause CurrentCause
    {
        get
        {
            if (_cause == null)
            {
                _cause = this.Page.CscmsContext.Cause.Include("OCACourt").Include("OCACourt1").Include("OriginatingCourt").Include("County").Include("County1").Include("CauseNote").Where(it => it.CauseGUID == this.CauseGUID).FirstOrDefault();
            }

            return _cause;
        }
    }


    private CSCMS.Data.Action _action;
    public CSCMS.Data.Action CurrentAction
    {
        get
        {
            if (_action == null)
            {
                Guid actionGuid = (Guid)Session["CurrentAction_ActionGuid"];
                _action = this.Page.CscmsContext.Action.Include("ActionType.ActionCategory").Include("DispositionDetail").Include("DispositionType").Include("ChildSupport").Where(it => it.Cause.CauseGUID == this.CauseGUID && it.ActionNumber == this.actionNumber).FirstOrDefault();

            }

            return _action;
        }
    }

    private bool print;
    public bool Print
    {
        get { return print; }
        set
        {
            print = value;

            if (value)
            {
                //spnEdit.Visible = false;
            }
        }
    }
}
