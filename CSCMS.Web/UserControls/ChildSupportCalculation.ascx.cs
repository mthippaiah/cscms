﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Text;
using Telerik.Web.UI;

public partial class UserControls_ChildSupportCalculation : System.Web.UI.UserControl
{
    public event EventHandler CaluclateClicked;
    double calculatedAmount = 0.00;

    static string[] colNames = new string[] { "OneKids", "TwoKids", "ThreeKids", "FourKids", "FiveKids", "SixKids", "SevenKids" };

    protected override void OnInit(EventArgs e)
    {
        List<string> yearList = ((BasePage)Page).CscmsContext.EmployedPerson.OrderByDescending(it => it.CalendarYear).Select(it => it.CalendarYear).Distinct().ToList();
        foreach (string s in yearList)
        {
            this.RadComboBoxCalendarYear.Items.Add(new RadComboBoxItem(s, s));
        }
        this.RadComboBoxCalendarYear.SelectedIndex = 0;

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool iOpenedConn = false;
        try
        {
            if (((BasePage)Page).CscmsContext.Connection.State != System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            short chilPErsonTypeId = ((BasePage)Page).CscmsContext.PersonType.Where(it => it.PersonTypeName == "Child").Select(it => it.PersonTypeID).FirstOrDefault();
            int numOfChildren = ((BasePage)Page).CscmsContext.ActionPerson.Where(it => it.PersonType.PersonTypeID == chilPErsonTypeId && it.ActionGUID == CurrentActionGuid).Count();
            this.LabelNumChildren.Text = numOfChildren.ToString();
        }
        finally
        {
            if (iOpenedConn && ((BasePage)Page).CscmsContext.Connection.State == System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Close();
            }
        }

        // call from main page
        if (!Page.IsPostBack)
        {
            if (CurrentActionGuid.ToString() == "00000000-0000-0000-0000-000000000000")
            {
                ButtonApplyAmount.Visible = false;
                ButtonClose.Visible = false;
                ButtonApplyAmount2.Visible = false;
                ButtonAppyAmount3.Visible = false;
                ButtonApplyAll.Visible = false;
                ButtonClose2.Visible = false;
                ButtonClose3.Visible = false;
                ButtonClose4.Visible = false;
            }
            if (Session["RadNumericTextBoxGrossWages"] != null) RadNumericTextBoxGrossWages.Text = Session["RadNumericTextBoxGrossWages"].ToString();
            if (Session["RadNumericTextBoxSpecialWages"] != null) RadNumericTextBoxSpecialWages.Text = Session["RadNumericTextBoxSpecialWages"].ToString();
            if (Session["RadNumericTextBoxMonthlyHealthCareCost"] != null) RadNumericTextBoxMonthlyHealthCareCost.Text = Session["RadNumericTextBoxMonthlyHealthCareCost"].ToString();
            if (Session["RadComboBoxNumOtherChildren"] != null) RadComboBoxNumOtherChildren.SelectedIndex = int.Parse(Session["RadComboBoxNumOtherChildren"].ToString());
            if (Session["RadComboBoxAdjNumChildren"] != null) RadComboBoxAdjNumChildren.SelectedIndex = int.Parse(Session["RadComboBoxAdjNumChildren"].ToString());
            if (Session["RadComboBoxEmployed"] != null) RadComboBoxEmployed.SelectedIndex = int.Parse(Session["RadComboBoxEmployed"].ToString());
        }
        this.ButtonReset.Attributes.Add("onClick", "return ResetMonthlyPaymentCalculation();");
        this.ButtonReset2.Attributes.Add("onClick", "return ResetArrearsCalculation();");
        this.ButtonReset3.Attributes.Add("onClick", "return ResetMedicalPastDueCalculation();");
    }

    public Guid CurrentActionGuid
    {
        get;
        set;
    }

    public string DestinationMonthlyAmountTextBoxClientID
    {
        get;
        set;
    }

    public string DestinationArrearsAmountTextBoxClientID
    {
        get;
        set;
    }

    public string DestinationArrearsPaymentAmountTextBoxClientID
    {
        get;
        set;
    }

    public string DestinationMedicalPastDueAmountTextBoxClientID
    {
        get;
        set;
    }

    public string DestinationMedicalPastDuePaymentAmountTextBoxClientID
    {
        get;
        set;
    }

    private void RegisterMonthlyApplyAmountClientScript(string destinationTextBoxClientID)
    {
        // Define the name and type of the client script on the page.
        String csName = "MonthlyAmountClientScript";
        Type csType = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the client script is already registered.
        if (!cs.IsClientScriptBlockRegistered(csType, csName))
        {
            StringBuilder csText = new StringBuilder();
            csText.Append("<script type=\"text/javascript\"> function ApplyAdjustedAmount() {");
            csText.Append("var radAdjuAmount = $find('" + this.RadNumericTextBoxAdjustedAmount.ClientID + "');");
            csText.Append("var targetTextbox = $find('" + destinationTextBoxClientID + "');");
            csText.Append("targetTextbox.set_value(radAdjuAmount.get_value()); return false;");
            csText.Append("}");
            csText.Append("</script>");
            cs.RegisterClientScriptBlock(csType, csName, csText.ToString());
        }
    }

    private void RegisterArrearsAmountClientScript(string destinationArrearsTextBoxClientID, string destinationArrearsPayemntTextBoxClientID)
    {
        // Define the name and type of the client script on the page.
        String csName = "ArrearsAmountClientScript";
        Type csType = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the client script is already registered.
        if (!cs.IsClientScriptBlockRegistered(csType, csName))
        {
            StringBuilder csText = new StringBuilder();
            csText.Append("<script type=\"text/javascript\"> function ApplyArrearsAmount() {");
            csText.Append("var radArrearsAmount = $find('" + this.RadNumericTextBoxArrearsAmount.ClientID + "');");
            csText.Append("var radCalculatedAmount = document.getElementById('" + this.LabelCalculatedAmount2.ClientID + "');");
            csText.Append("var radAdjuAmount = $find('" + this.RadNumericTextBoxAdjustedAmount2.ClientID + "');");
            csText.Append("var targetArrearsTextbox = $find('" + destinationArrearsTextBoxClientID + "');");
            csText.Append("var targetPaymentAmount = $find('" + destinationArrearsPayemntTextBoxClientID + "');");
            csText.Append("targetArrearsTextbox.set_value(radArrearsAmount.get_value());");
            csText.Append("targetPaymentAmount.set_value(radAdjuAmount.get_value()); return false;");
            csText.Append("}");
            csText.Append("</script>");
            cs.RegisterClientScriptBlock(csType, csName, csText.ToString());
        }
    }

    private void RegisterPastDueMedicalAmountClientScript(string destinationMedicalPastDueTextBoxClientID, string destinationMedicalPastDuePayemntTextBoxClientID)
    {
        // Define the name and type of the client script on the page.
        String csName = "PastDueMedicalClientScript";
        Type csType = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the client script is already registered.
        if (!cs.IsClientScriptBlockRegistered(csType, csName))
        {
            StringBuilder csText = new StringBuilder();
            csText.Append("<script type=\"text/javascript\"> function ApplyPastDueMedicalAmount() {");
            csText.Append("var radArrearsAmount = $find('" + this.RadNumericTextBoxArrearsAmount2.ClientID + "');");
            csText.Append("var radCalculatedAmount = document.getElementById('" + this.LabelCalculatedAmount3.ClientID + "');");
            csText.Append("var radAdjuAmount = $find('" + this.RadNumericTextBoxAdjustedAmount3.ClientID + "');");
            csText.Append("var targetArrearsTextbox = $find('" + destinationMedicalPastDueTextBoxClientID + "');");
            csText.Append("var targetPaymentAmount = $find('" + destinationMedicalPastDuePayemntTextBoxClientID + "');");
            csText.Append("targetArrearsTextbox.set_value(radArrearsAmount.get_value());");
            csText.Append("targetPaymentAmount.set_value(radAdjuAmount.get_value()); return false;");
            csText.Append("}");
            csText.Append("</script>");
            cs.RegisterClientScriptBlock(csType, csName, csText.ToString());
        }
    }

    private void RegisterApplyAllClientScript()
    {
        // Define the name and type of the client script on the page.
        String csName = "ApplyAllClientScript";
        Type csType = this.GetType();

        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Check to see if the client script is already registered.
        if (!cs.IsClientScriptBlockRegistered(csType, csName))
        {
            StringBuilder csText = new StringBuilder();
            csText.Append("<script type=\"text/javascript\"> function ApplyAll() {");
            csText.Append("ApplyAdjustedAmount();");
            csText.Append("ApplyArrearsAmount();");
            csText.Append("ApplyPastDueMedicalAmount();return CloseChildCalcultionPopup();");
            csText.Append("}");
            csText.Append("</script>");
            cs.RegisterClientScriptBlock(csType, csName, csText.ToString());
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        RegisterMonthlyApplyAmountClientScript(this.DestinationMonthlyAmountTextBoxClientID);
        RegisterArrearsAmountClientScript(this.DestinationArrearsAmountTextBoxClientID, this.DestinationArrearsPaymentAmountTextBoxClientID);
        RegisterPastDueMedicalAmountClientScript(this.DestinationMedicalPastDueAmountTextBoxClientID, this.DestinationMedicalPastDuePaymentAmountTextBoxClientID);
        RegisterApplyAllClientScript();
        base.OnPreRender(e);
    }

    protected void ButtonCalculate_Click(object sender, EventArgs e)
    {
        CaculateChildSupportMonthlyPayment();

        if (CaluclateClicked != null)
            CaluclateClicked(sender, e);

        Session["RadNumericTextBoxGrossWages"] = RadNumericTextBoxGrossWages.Text;
        Session["RadNumericTextBoxSpecialWages"] = RadNumericTextBoxSpecialWages.Text;
        Session["RadNumericTextBoxMonthlyHealthCareCost"] = RadNumericTextBoxMonthlyHealthCareCost.Text;
        Session["LabelNumChildren"] = this.LabelNumChildren.Text;
        Session["RadComboBoxAdjNumChildren"] = RadComboBoxAdjNumChildren.SelectedIndex.ToString();
        Session["RadComboBoxNumOtherChildren"] = RadComboBoxNumOtherChildren.SelectedIndex.ToString();
        Session["RadComboBoxEmployed"] = RadComboBoxEmployed.SelectedIndex.ToString();

    }

    private void CaculateChildSupportMonthlyPayment()
    {
        double netWage = (this.RadNumericTextBoxGrossWages.Value.HasValue ? this.RadNumericTextBoxGrossWages.Value.Value : 0.00);
        // + (this.RadNumericTextBoxSpecialWages.Value.HasValue ? this.RadNumericTextBoxSpecialWages.Value.Value : 0.00);

        decimal mgw = (decimal)netWage; //  -(this.RadNumericTextBoxMonthlyHealthCareCost.Value.HasValue ? this.RadNumericTextBoxMonthlyHealthCareCost.Value.Value : 0.00);
        decimal calcWage = new decimal(0.00);
        decimal maxWage = new decimal(7500.00);

        int numOtherChildren = int.Parse(this.RadComboBoxNumOtherChildren.SelectedValue);
        int adjNumChildren = int.Parse(this.RadComboBoxAdjNumChildren.SelectedValue);
        CSCMS.Data.PercentofNetResources obj = null;
        bool iOpenedConn = false;
        try
        {
            if (((BasePage)Page).CscmsContext.Connection.State != System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Open();
                iOpenedConn = true;
            }

            if (this.RadComboBoxEmployed.SelectedValue == MyConsts.DB_EMPLOYED)
            {
                calcWage = ((BasePage)Page).CscmsContext.EmployedPerson.Where(it => it.MonthlyGrossWages_.Value <= (decimal)mgw && it.CalendarYear == this.RadComboBoxCalendarYear.SelectedValue && it.AboveCap == false).OrderByDescending(it => it.MonthlyGrossWages_).Select(it => it.NetMonthlyIncome_.Value).Take(1).First();
            }

            if (this.RadComboBoxEmployed.SelectedValue == MyConsts.DB_SELF_EMPLOYED)
            {
                calcWage = ((BasePage)Page).CscmsContext.SelfEmployed.Where(it => it.MonthlyGrossWages_.Value <= (decimal)mgw && it.CalendarYear == this.RadComboBoxCalendarYear.SelectedValue && it.AboveCap == false).OrderByDescending(it => it.MonthlyGrossWages_).Select(it => it.NetMonthlyIncome_.Value).Take(1).First();
            }

            obj = ((BasePage)Page).CscmsContext.PercentofNetResources.Where(it => it.ID == numOtherChildren && (it.BeginCalendarYear == this.RadComboBoxCalendarYear.SelectedValue || it.EndCalendarYear == this.RadComboBoxCalendarYear.SelectedValue)).FirstOrDefault();
        }
        finally
        {
            if (iOpenedConn && ((BasePage)Page).CscmsContext.Connection.State == System.Data.ConnectionState.Open)
            {
                ((BasePage)Page).CscmsContext.Connection.Close();
            }
        }

        calcWage = calcWage + (decimal)(this.RadNumericTextBoxSpecialWages.Value.HasValue ? this.RadNumericTextBoxSpecialWages.Value.Value : 0.00);

        if (calcWage > maxWage) calcWage = maxWage;

        calcWage = calcWage - (decimal)(this.RadNumericTextBoxMonthlyHealthCareCost.Value.HasValue ? this.RadNumericTextBoxMonthlyHealthCareCost.Value.Value : 0.00);

        PropertyInfo[] props = obj.GetType().GetProperties();
        int index = adjNumChildren - 1;
        if (index > 6)
            index = 6;

        PropertyInfo prop = props.Where(it => it.Name == colNames[index]).FirstOrDefault();
        double percent = (double)prop.GetValue(obj, null);
        calculatedAmount = ((percent * (double)calcWage) / 100);

        this.LabelCalculatedAmount.Text = String.Format("{0:0.##}", calculatedAmount);

        this.RadNumericTextBoxAdjustedAmount.Value = calculatedAmount;
    }

    private void InitializeControls()
    {
        this.RadComboBoxAdjNumChildren.SelectedIndex = 0;
        this.RadComboBoxEmployed.SelectedIndex = 0;
        this.RadComboBoxNumOtherChildren.SelectedIndex = 0;
        this.RadNumericTextBoxGrossWages.Text = "";
        this.RadNumericTextBoxSpecialWages.Text = "";
        this.RadNumericTextBoxMonthlyHealthCareCost.Text = "";
        this.RadNumericTextBoxAdjustedAmount.Text = "";
    }

    public void Reset()
    {
        InitializeControls();
    }

    public double GetAmount()
    {
        return this.RadNumericTextBoxAdjustedAmount.Value.Value;
    }

    private void CalculateArrears()
    {
        this.LabelCalculatedAmount2.Text = ArrearsCalcHelper(RadNumericTextBoxArrearsAmount, RadNumericTextBoxInterestRate, RadNumericTextBoxTerms).ToString();
        if (!string.IsNullOrEmpty(this.LabelCalculatedAmount2.Text))
            this.RadNumericTextBoxAdjustedAmount2.Value = double.Parse(this.LabelCalculatedAmount2.Text);
    }

    private void CalculateMedicalArrears()
    {
        this.LabelCalculatedAmount3.Text = ArrearsCalcHelper(RadNumericTextBoxArrearsAmount2, RadNumericTextBoxInterestRate2, RadNumericTextBoxTerms2).ToString();
        this.RadNumericTextBoxAdjustedAmount3.Value = double.Parse(this.LabelCalculatedAmount3.Text);
    }

    private double ArrearsCalcHelper(RadNumericTextBox arrears, RadNumericTextBox rate, RadNumericTextBox terms)
    {
        double interest = (arrears.Value.Value * rate.Value.Value * terms.Value.Value) / (100 * 12);
        double total = interest + arrears.Value.Value;
        return Math.Round((total / this.RadNumericTextBoxTerms2.Value.Value), 2);
    }

    protected void ButtonCalculateAll_Click(object sender, EventArgs e)
    {
        CaculateChildSupportMonthlyPayment();
        CalculateArrears();
        CalculateMedicalArrears();

        if (CaluclateClicked != null)
            CaluclateClicked(sender, e);
    }
}
