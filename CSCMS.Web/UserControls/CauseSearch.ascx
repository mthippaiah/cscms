﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CauseSearch.ascx.cs" Inherits="UserControls_CauseSearch" %>
<asp:HiddenField ID="HFSearchDataSource" runat="server" />
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top" style="width: 50%">
            <!-- sectionSearchByCase -->
            <CSCMSUC:SectionContainer ID="sectionSearchByCase" runat="server" HeaderText="Search by Case"
                Width="100%">
                <asp:Panel ID="pnlCase" runat="server" DefaultButton="btnSearchByCase">
                    <div style="height: 260px;">
                        <table cellspacing="3" cellpadding="1">
                            <tr>
                                <td class="fieldLabel">
                                    Cause Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCauseNumber" runat="server" Width="200" Columns="20" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    OAG Number:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOAGCauseNumber" runat="server" Width="200" Columns="10" ToolTip=":D for duplicated" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    County:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCounty" runat="server" Width="204" DataSourceID="dsCounty"
                                        DataValueField="CountyID" DataTextField="CountyName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsCounty" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="GetUserCounties" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                                        OnSelecting="dsCounty_Selecting">
                                        <SelectParameters>
                                            <asp:Parameter Name="UserId" Type="Int32" />
                                            <asp:Parameter Name="CourtID" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Action Type:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlActionType" runat="server" Width="204" DataSourceID="dsActionType"
                                        DataValueField="ActionTypeID" DataTextField="ActionTypeName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsActionType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="SELECT ActionTypeID, ActionTypeName FROM ActionType order by SortOrder">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Style:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtStyle" runat="server" Width="200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Active/Inactive:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCaseStatus" runat="server" Width="204">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                        <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                        <asp:ListItem Value="I">Inactive</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Pending/Disposed:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDisposed" runat="server" Width="204">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                        <asp:ListItem Value="D">Disposed</asp:ListItem>
                                        <asp:ListItem Value="P">Pending</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    LNPServiced
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLNPServiced" runat="server" Width="204">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    FutureHearingSet
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlFutureHearingSet" runat="server" Width="204">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Last Action Filing Date:
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="dtFilingDateFrom" runat="server" Width="92" />
                                    &nbsp;<b>to</b>&nbsp;
                                    <telerik:RadDatePicker ID="dtFilingDateTo" runat="server" Width="92" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="text-align: center; padding-bottom: 5px;">
                        <asp:Button ID="btnSearchByCase" runat="server" CssClass="submit" Text="Search by Case"
                            CausesValidation="false" OnClick="btnSearchByCase_Click" /></div>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
        </td>
        <td style="width: 10px;">
            &nbsp;&nbsp;&nbsp;
        </td>
        <td valign="top" style="width: 50%;">
            <!-- sectionPerson -->
            <CSCMSUC:SectionContainer ID="sectionPerson" runat="server" HeaderText="Search by Person"
                Width="100%">
                <asp:Panel ID="pnlPerson" runat="server" DefaultButton="btnSearchByPerson">
                    <div style="height: 260px;">
                        <table cellspacing="3" cellpadding="1">
                            <tr>
                                <td class="fieldLabel">
                                    Last Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" Width="200" />
                                    <asp:RequiredFieldValidator ID="rfvtxtLastName" runat="server" ControlToValidate="txtLastName"
                                        ValidationGroup="SearchByPerson" ErrorMessage="Please enter a Last Name">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    First Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" Width="200" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Person Type:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPersonType" runat="server" Width="204" DataSourceID="dsPersonType"
                                        DataValueField="PersonTypeID" DataTextField="PersonTypeName" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">- Select -</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsPersonType" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                                        SelectCommand="SELECT PersonTypeID, PersonTypeName FROM PersonType order by SortOrder">
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    Birth Date:
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="dtBirthdate" runat="server" Width="92" />
                                </td>
                            </tr>
                            <tr>
                                <td class="fieldLabel">
                                    NP Only:
                                </td>
                                <td>
                                    <asp:CheckBox ID="ckbNPOnly" runat="server" Text="" Checked="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="text-align: center; padding-bottom: 5px;">
                        <asp:Button ID="btnSearchByPerson" runat="server" CssClass="submit" Text="Search by Person"
                            ValidationGroup="SearchByPerson" OnClick="btnSearchByPerson_Click" /></div>
                </asp:Panel>
            </CSCMSUC:SectionContainer>
        </td>
    </tr>
</table>
