﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
public partial class UserControls_PersonSearch : System.Web.UI.UserControl
{
    public event EventHandler SearchClicked;
    public event EventHandler SearchPageChanged;
    private bool fulldetails = false;
    public bool RequiresCompleteDetails
    {
        get
        {
            return fulldetails;
        }
        set
        {
            fulldetails = value;
            if (fulldetails)
            {
                HFFullDetails.Value = "true";
            }
        }
    }

    public string ParentPageName
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void RadGridPersons_OnPageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        if (SearchPageChanged != null)
            SearchPageChanged(sender, null);
    }

    protected void RadGridPersons_OnNeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (this.IsPostBack)
        {
            DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
               CommandType.StoredProcedure,
               "SearchPerson",
               new SqlParameter("@LastName", this.txtLastName.Text),
               new SqlParameter("@FirstName", !string.IsNullOrEmpty(this.txtFirstName.Text) ? txtFirstName.Text : null),
                new SqlParameter("@MiddleName", !string.IsNullOrEmpty(this.txtMiddleName.Text) ? txtMiddleName.Text : null),
               new SqlParameter("@BirthDate", this.dtBirthdate.SelectedDate.HasValue ? dtBirthdate.SelectedDate.Value : (DateTime?)null),
               new SqlParameter("@Sex", !string.IsNullOrEmpty(this.ddlSex.SelectedValue) ? this.ddlSex.SelectedValue : null));

            this.RadGridPersons.DataSource = ds;

            if (ds.Tables[0].Rows.Count > 0)
                this.btnSelectPerson.Visible = true;
        }
        else
        {
            RadGridPersons.DataSource = new string[] { };
            RadGridPersons.MasterTableView.NoMasterRecordsText = "No records";
            this.btnSelectPerson.Visible = false;
        }

    }

    protected void btnSearch_Click(object sender, EventArgs args)
    {
        RadGridPersons.Rebind();

        if (SearchClicked != null)
            SearchClicked(sender, args);
    }
}