﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseReminder.ascx.cs"
    Inherits="UserControls_CaseReminder" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="75px"
    Width="75px">
    <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
        style="border: 0px; padding-top: 40px" />
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="loadingPanel">
    <%-- --%>
    <asp:Panel ID="Panel3" runat="server" DefaultButton="btnAddReminder">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td align="left">
                    <asp:Button ID="ButtonIncludeExpired" runat="server" Text="Show Expired" 
                        Width="80" CssClass="submitSmall" onclick="ButtonIncludeExpired_Click"/>
                </td>
            </tr>
             <tr>
                <td height="5">
                </td>
            </tr>
            <tr id="Tr2" runat="server">
                <td>
                    <!-- dsReminder -->
                    <telerik:RadGrid ID="grdReminder" runat="server" DataSourceID="dsReminder" AllowPaging="True"
                        PageSize="10" AllowSorting="True" OnDeleteCommand="grdReminder_DeleteCommand">
                        <MasterTableView NoMasterRecordsText="There are currently no reminders." DataKeyNames="ReminderID">
                            <Columns>
                                <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="45">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" CausesValidation="false" runat="server" ItemStyle-Width="45"
                                            ImageUrl="../images/x_sm.gif" OnClientClick="return confirm('Are you sure you want to delete this reminder?');"
                                            CommandName="delete" Visible="true" /></ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn HeaderText="Start Date" HeaderStyle-Width="90" DataField="StartDate"
                                    DataFormatString="{0:MM/dd/yyyy}" />
                                <telerik:GridBoundColumn HeaderText="End Date" HeaderStyle-Width="90" DataField="EffectiveDate"
                                    DataFormatString="{0:MM/dd/yyyy}" />
                                <telerik:GridHyperLinkColumn HeaderText="Reminder" HeaderStyle-Width="520" DataNavigateUrlFields="ReminderID"
                                    DataNavigateUrlFormatString="../MemberPages/Reminder.aspx?rid={0}" SortExpression="ReminderNote"
                                    DataTextField="ReminderNote" ItemStyle-Font-Bold="true" />
                                <telerik:GridTemplateColumn HeaderText="Cause Number" HeaderStyle-Width="100">
                                    <ItemTemplate>
                                        <asp:HyperLink Visible='<%# (Eval("CauseNumber").ToString() != "" ) %>' ID="HyperLinkColumnCauseGUID"
                                            runat="server" NavigateUrl='<%#  "../MemberPages/ActionDetail.aspx?" + "cid=" + Eval("CauseGUID").ToString() + "&a=" + Eval("NumberOfAction").ToString() %>'><%# Eval("CauseNumber")%></asp:HyperLink>
                                        <asp:Label ID="nothing1" runat="server" Text='<%# Eval("CauseNumber").ToString() %>'
                                            Visible='<%# (Eval("CauseNumber").ToString() == "" ) %>' BorderStyle="None" BorderWidth="0"></asp:Label>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn HeaderText="Days" HeaderStyle-Width="90" DataField="DaysToEffective" />
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <!-- dsReminder -->
                    <asp:SqlDataSource ID="dsReminder" runat="server" ConnectionString="<%$ ConnectionStrings:CSCMSConnectionString %>"
                        SelectCommand="GetReminderByUserID" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false"
                        OnSelecting="dsReminder_Selecting">
                        <SelectParameters>
                            <asp:Parameter Name="OCACourtID" Type="Int32" />
                            <asp:Parameter Name="UserID" Type="Int32" />
                            <asp:Parameter Name="All" Type="Boolean" />
                            <asp:Parameter Name="ShowExpried" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
            <tr id="Tr3" runat="server" visible='<%# (this.ReadOnly == false) %>'>
                <td align="left">
                    <table>
                        <tr>
                            <td align="right">
                                Start Date:
                            </td>
                            <td align="left">
                                <telerik:RadDatePicker ID="dtStartDate" runat="server" Width="120px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                End Date:
                            </td>
                            <td align="left">
                                <telerik:RadDatePicker ID="dtEffectiveDate" runat="server" Width="120px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Reminder:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtReminder" runat="server" MaxLength="750" Width="480" />
                                <asp:RequiredFieldValidator ID="rfvReminder" ValidationGroup="ReminderGroup" runat="server"
                                    ControlToValidate="txtReminder" ErrorMessage="Please enter Reminder Text">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Cause#:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCauseNumber" runat="server" MaxLength="20" Width="90" />
                                <asp:CustomValidator ID="cvtxtCauseNumber" runat="server" ValidationGroup="ReminderGroup"
                                    ErrorMessage="&nbsp;*" ControlToValidate="txtCauseNumber" EnableViewState="False"
                                    Text="&nbsp;*"></asp:CustomValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td align="right">
                            Court Scope:
                            </td>
                            <td  align="left">
                                <asp:CheckBox ID="chkCourtReminder" runat="server" Width="100" 
                                    Visible="true" Text="" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                            </td>
                            <td align="left">
                                <asp:Button ID="btnAddReminder" runat="server" Text="Save" Width="50" CssClass="submitSmall"
                                    ValidationGroup="ReminderGroup" OnClick="btnAddReminder_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="5">
                </td>
            </tr>
        </table>
    </asp:Panel>
</telerik:RadAjaxPanel>
