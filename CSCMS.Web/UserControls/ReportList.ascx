﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportList.ascx.cs" Inherits="UserControls_ReportList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

    <CSCMSUC:SectionContainer ID="SectionContainer1" runat="server" HeaderText="Report"
        Width="600px">
        <table cellspacing="3" cellpadding="1" align="left">
            <tr>
                <td align="left">
                    <telerik:RadTreeView ID="tree" runat="server" Skin="Office2007" OnNodeExpand="tree_NodeExpand" />
                </td>
            </tr>
        </table>
    </CSCMSUC:SectionContainer>