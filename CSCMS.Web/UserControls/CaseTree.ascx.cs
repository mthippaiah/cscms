﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CSCMS.Data;

public partial class UserControls_CaseTree : BaseUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void Initialize()
    {
        LoadCourts();
        LoadCounties();
        LoadPreviouslyViewCases();
    }

    private void LoadCourts()
    {
        if (this.Page.CurrentUser.UserType == UserType.Judge || this.Page.CurrentUser.UserType == UserType.Coordinator)
        {
            // This is a Judge / Coordinator, so only load their court
            RadTreeNode courtNode = new RadTreeNode(this.Page.CurrentUser.OCACourtName, this.Page.CurrentUser.OCACourtID.ToString());
            courtNode.Expanded = true;
            tree.Nodes.Add(courtNode);
        }
        else
        {
            // This user has access to all courts
            var courts = (from c in ((BasePage)Page).CscmsContext.OCACourt
                          where (c.ExpirationDate < DateTime.Today || c.ExpirationDate == null)
                          orderby c.SortOrder
                          select c);

            foreach (OCACourt court in courts)
            {
                tree.Nodes.Add(new RadTreeNode(court.CourtName, court.OCACourtID.ToString()));
            }
        }
        RadTreeNode previouslyNotes = new RadTreeNode("Previously Viewed Cases", "99");
        previouslyNotes.Expanded = true;
        tree.Nodes.Add(previouslyNotes);
    }

    private void LoadPreviouslyViewCases()
    {
        RadTreeNode courtNode = tree.Nodes.FindNodeByValue("99");
        Dictionary<string, string> previouslyViewCases = new Dictionary<string, string>();
        if (Session["previouslyViewCases"] != null) previouslyViewCases = (Dictionary<string, string>)Session["previouslyViewCases"];
        if ((((BasePage)this.Page).CurrentCause != null) && ((BasePage)this.Page).CurrentCause.CauseNumber != null && (!previouslyViewCases.ContainsKey(((BasePage)this.Page).CurrentCause.CauseNumber)))
        {
            if (previouslyViewCases.Count > 4)
            {
                Dictionary<string, string> tmppreviouslyViewCases = new Dictionary<string, string>();
                previouslyViewCases.Remove(previouslyViewCases.ElementAt(0).Key);
                foreach (KeyValuePair<string, string> currentCause in previouslyViewCases)
                {
                    tmppreviouslyViewCases.Add(currentCause.Key, currentCause.Value);
                }
                previouslyViewCases = tmppreviouslyViewCases;
            }
            previouslyViewCases.Add(((BasePage)this.Page).CurrentCause.CauseNumber, ((BasePage)this.Page).CurrentCause.CauseGUID.ToString() + "&a=" + ((BasePage)this.Page).CurrentCause.NumberOfAction.ToString());
        }
        if (courtNode != null)
        {
            foreach (KeyValuePair<string, string> currentCause in previouslyViewCases)
            {
                courtNode.Nodes.Add(new RadTreeNode(currentCause.Key, currentCause.Value, "~/MemberPages/ActionDetail.aspx?cid=" + currentCause.Value));
            }
        }
        Session["previouslyViewCases"] = previouslyViewCases;
    }

    private void LoadCounties()
    {
        IQueryable<CourtCountyResult> OCAcourtCounties;

        if (this.Page.CurrentUser.UserType == UserType.Judge || this.Page.CurrentUser.UserType == UserType.Coordinator)
        {
            OCAcourtCounties = CompiledQueryList.GetCourtCountyResultForJudgeOrCoordinator.Invoke(((BasePage)Page).CscmsContext, this.Page.CurrentUser.OCACourtID);
        }
        else
        {
            OCAcourtCounties = CompiledQueryList.GetCourtCountyResult.Invoke(((BasePage)Page).CscmsContext);
        }

        if (tree.Nodes.Count == 0)
        {
            return;
        }

        RadTreeNode courtNode = tree.Nodes[0];

        foreach (CourtCountyResult courtCounty in OCAcourtCounties)
        {
            if (courtNode.Value != courtCounty.OCACourtID.ToString())
            {
                // find the correct court node
                courtNode = tree.Nodes.FindNodeByValue(courtCounty.OCACourtID.ToString());
            }

            RadTreeNode countyNode = new RadTreeNode(courtCounty.CountyName, courtCounty.CountyID.ToString());
            courtNode.Nodes.Add(countyNode);

            RadTreeNode openNode = new RadTreeNode("Active", "Active");
            openNode.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
            countyNode.Nodes.Add(openNode);

            RadTreeNode closedNode = new RadTreeNode("Inactive", "Inactive");
            closedNode.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
            countyNode.Nodes.Add(closedNode);

            RadTreeNode allNode = new RadTreeNode("All", "all");
            allNode.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
            countyNode.Nodes.Add(allNode);
        }

    }

    protected void tree_NodeExpand(object sender, RadTreeNodeEventArgs e)
    {
        SetChildNodes(e.Node);
    }

    private void SetChildNodes(RadTreeNode node)
    {
        if (node.Value == "Active" || node.Value == "Inactive" || node.Value == "all")
        {
            short countyID = short.Parse(node.ParentNode.Value);
            short ocaCourtID = short.Parse(node.ParentNode.ParentNode.Value);

            IQueryable<ActionResult> causes = null;

            switch (node.Value)
            {
                case "Active":
                    causes = CompiledQueryList.GetCauseWithStatus.Invoke(((BasePage)Page).CscmsContext, ocaCourtID, countyID, "A");
                    break;

                case "Inactive":
                    causes = CompiledQueryList.GetCauseWithStatus.Invoke(((BasePage)Page).CscmsContext, ocaCourtID, countyID, "I");
                    break;

                case "all":
                    causes = CompiledQueryList.GetCauseWithStatus.Invoke(((BasePage)Page).CscmsContext, ocaCourtID, countyID, null);
                    break;
            }

            foreach (ActionResult c in causes)
            {
                node.Nodes.Add(new RadTreeNode(c.CauseNumber, c.CauseGUID.ToString(), "~/MemberPages/ActionDetail.aspx?cid=" + c.CauseGUID.ToString() + "&a=" + c.ActionNumber.ToString()));
            }

        }
    }
}


