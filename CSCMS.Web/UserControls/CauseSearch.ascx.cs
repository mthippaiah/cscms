﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;
using Telerik.Web.UI.GridExcelBuilder;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using AjaxControlToolkit;

public delegate void CauseSearchResultDelegate(DataSet ds, bool isSearchByCase);

public partial class UserControls_CauseSearch : System.Web.UI.UserControl
{
    public event CauseSearchResultDelegate CauseSearchResult;

    public void DoSearch()
    {
        if (HFSearchDataSource.Value == "Case")
            btnSearchByCase_Click(this, null);

        if (HFSearchDataSource.Value == "Person")
            btnSearchByPerson_Click(this, null);
    }

    public int CaseStatusIndex
    {
        get
        {
            return this.ddlCaseStatus.SelectedIndex;
        }
        set
        {
            this.ddlCaseStatus.SelectedIndex = value; 
        }
    }

    public int DisposedIndex
    {
        get
        {
            return this.ddlDisposed.SelectedIndex;
        }
        set
        {
            this.ddlDisposed.SelectedIndex = value;
        }
    }

    public string CauseNumberText
    {
        get
        {
            return this.txtCauseNumber.Text;
        }
        set
        {
            this.txtCauseNumber.Text = value;
        }
    }

    public string OAGCauseNumberText
    {
        get
        {
            return this.txtOAGCauseNumber.Text;
        }
        set
        {
            this.txtOAGCauseNumber.Text = value;
        }
    }

    public void MinimizeSearchByCaseSection(bool minimize)
    {
        sectionPerson.Minimized = minimize;
    }

    public void MinimizeSearchByPersonSection(bool minimize)
    {
        sectionSearchByCase.Minimized = minimize;
    }

    public void ClearSearchParameters()
    {
        SessionHelper.ResetSearchSessionVariables();
        txtCauseNumber.Text = string.Empty;
        txtOAGCauseNumber.Text = string.Empty; ;

        txtStyle.Text = string.Empty; ;
        txtLastName.Text = string.Empty; ;
        txtFirstName.Text = string.Empty; ;
        dtFilingDateFrom.SelectedDate = null;
        dtFilingDateTo.SelectedDate = null;
        ddlCounty.SelectedIndex = 0;
        ddlPersonType.SelectedIndex = 0;
        ddlCaseStatus.SelectedIndex = 0;
        ddlDisposed.SelectedIndex = 0;
        ddlActionType.SelectedIndex = 0;
        ddlLNPServiced.SelectedIndex = 0;
        ddlFutureHearingSet.SelectedIndex = 0;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.Page.Form.DefaultFocus = this.txtCauseNumber.ClientID;

            try
            {
                if (Session["SearchtxtCauseNumber"] != null) txtCauseNumber.Text = Session["SearchtxtCauseNumber"].ToString();
                if (Session["SearchtxtOAGCauseNumber"] != null) txtOAGCauseNumber.Text = Session["SearchtxtOAGCauseNumber"].ToString();
                if (Session["SearchtxtStyle"] != null) txtStyle.Text = Session["SearchtxtStyle"].ToString();
                if (Session["SearchtxtLastName"] != null) txtLastName.Text = Session["SearchtxtLastName"].ToString();
                if (Session["SearchtxtFirstName"] != null) txtFirstName.Text = Session["SearchtxtFirstName"].ToString();
                if (Session["SearchddlCounty"] != null) ddlCounty.SelectedIndex = int.Parse(Session["SearchddlCounty"].ToString());
                if (Session["SearchddlPersonType"] != null) ddlPersonType.SelectedIndex = int.Parse(Session["SearchddlPersonType"].ToString());
                if (Session["SearchddlCaseStatus"] != null) ddlCaseStatus.SelectedIndex = int.Parse(Session["SearchddlCaseStatus"].ToString());
                if (Session["SearchdtFilingDateFrom"] != null)  dtFilingDateFrom.SelectedDate = DateTime.Parse(Session["SearchdtFilingDateFrom"].ToString());
                if (Session["SearchdtFilingDateTo"] != null) dtFilingDateTo.SelectedDate = DateTime.Parse(Session["SearchdtFilingDateTo"].ToString());
                if (Session["SearchddlDisposed"] != null) ddlDisposed.SelectedIndex = int.Parse(Session["SearchddlDisposed"].ToString());
                if (Session["SearchddlActionType"] != null) ddlActionType.SelectedIndex = int.Parse(Session["SearchddlActionType"].ToString());
                if (Session["SearchddlLNPServiced"] != null) ddlLNPServiced.SelectedIndex = int.Parse(Session["SearchddlLNPServiced"].ToString());
                if (Session["SearchddlFutureHearingSet"] != null) this.ddlFutureHearingSet.SelectedIndex = int.Parse(Session["SearchddlFutureHearingSet"].ToString());

            }
            catch { }
        }
    }

    protected void dsCounty_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@UserId"].Value = ((BasePage)Page).CurrentUser.UserID;
        e.Command.Parameters["@CourtID"].Value = ((BasePage)Page).CurrentUser.OCACourtID;
    }

    public void btnSearchByCase_Click(object sender, EventArgs e)
    {
        DataSet ds = ExecuteSearchByCase();

        if (CauseSearchResult != null)
            CauseSearchResult(ds, true);
    }

    public DataSet ExecuteSearchByCase()
    {
        HFSearchDataSource.Value = "Case";
        SaveSearchParameters();
        bool? lnpServiced = null;
        bool? futureHearingSet = null;
        if(this.ddlLNPServiced.SelectedIndex > 0 )
        {
            lnpServiced = (ddlLNPServiced.SelectedValue == "0"?false:true);
        }

        if(this.ddlFutureHearingSet.SelectedIndex > 0)
        {
            futureHearingSet = (ddlFutureHearingSet.SelectedValue == "0"?false:true);
        }

        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
            CommandType.StoredProcedure,
            "SearchByCase",
            new SqlParameter("@UserId", ((BasePage)Page).CurrentUser.UserID),
            new SqlParameter("@CauseNumber", !string.IsNullOrEmpty(txtCauseNumber.Text) ? txtCauseNumber.Text : null),
            new SqlParameter("@OAGNumber", !string.IsNullOrEmpty(txtOAGCauseNumber.Text) ? this.txtOAGCauseNumber.Text : null),
            new SqlParameter("@Style", !string.IsNullOrEmpty(txtStyle.Text) ? this.txtStyle.Text : null),
            new SqlParameter("@FilingFromDate", dtFilingDateFrom.SelectedDate.HasValue ? dtFilingDateFrom.SelectedDate.Value : (DateTime?)null),
            new SqlParameter("@FilingToDate", dtFilingDateTo.SelectedDate.HasValue ? dtFilingDateTo.SelectedDate.Value : (DateTime?)null),
            new SqlParameter("@CountyID", ddlCounty.SelectedIndex > 0 ? short.Parse(ddlCounty.SelectedValue) : (short?)null),
            new SqlParameter("@CaseStatusOpen", ddlCaseStatus.SelectedIndex > 0 ? ddlCaseStatus.SelectedValue : null),
            new SqlParameter("@CaseDisposed", ddlDisposed.SelectedIndex > 0 ? ddlDisposed.SelectedValue : null),
            new SqlParameter("@ActionType", ddlActionType.SelectedIndex > 0 ? ddlActionType.SelectedValue : null),
            new SqlParameter("@LNPServiced", lnpServiced),
            new SqlParameter("@FutureHearingSet", futureHearingSet));
        return ds;
    }

    protected void btnSearchByPerson_Click(object sender, EventArgs e)
    {
        DataSet ds = ExecuteSearchByPerson();

        if (CauseSearchResult != null)
            CauseSearchResult(ds, false);
    }

    public DataSet ExecuteSearchByPerson()
    {
        HFSearchDataSource.Value = "Person";
        SaveSearchParameters();
        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
           CommandType.StoredProcedure,
           "SearchCaseByPerson",
           new SqlParameter("@UserId", ((BasePage)Page).CurrentUser.UserID),
           new SqlParameter("@LastName", !string.IsNullOrEmpty(txtLastName.Text) ? txtLastName.Text : null),
           new SqlParameter("@FirstName", !string.IsNullOrEmpty(txtFirstName.Text) ? this.txtFirstName.Text : null),
           new SqlParameter("@PersonTypeId", ddlPersonType.SelectedIndex > 0 ? int.Parse(ddlPersonType.SelectedValue) : (int?)null),
           new SqlParameter("@Birthdate", dtBirthdate.SelectedDate.HasValue ? dtBirthdate.SelectedDate.Value : (DateTime?)null),
           new SqlParameter("@NPOnly", ckbNPOnly.Checked ? true : (bool?)null));
        return ds;
    }

    private void SaveSearchParameters()
    {
        Session["SearchtxtCauseNumber"] = txtCauseNumber.Text;
        Session["SearchtxtOAGCauseNumber"] = txtOAGCauseNumber.Text;

        Session["SearchtxtStyle"] = txtStyle.Text;
        Session["SearchtxtLastName"] = txtLastName.Text;
        Session["SearchtxtFirstName"] = txtFirstName.Text;
        Session["SearchdtFilingDateFrom"] = dtFilingDateFrom.SelectedDate.ToString();
        Session["SearchdtFilingDateTo"] = dtFilingDateTo.SelectedDate.ToString();
        Session["SearchddlCounty"] = ddlCounty.SelectedIndex.ToString();
        Session["SearchddlPersonType"] = ddlPersonType.SelectedIndex.ToString();
        Session["SearchddlCaseStatus"] = ddlCaseStatus.SelectedIndex.ToString();
        Session["SearchddlDisposed"] = ddlDisposed.SelectedIndex.ToString();
        Session["SearchddlActionType"] = ddlActionType.SelectedIndex.ToString();
        Session["SearchddlLNPServiced"] = ddlLNPServiced.SelectedIndex.ToString();
        Session["SearchddlFutureHearingSet"] = this.ddlFutureHearingSet.SelectedIndex.ToString();
    }

    //public List<CSCMS.Data.DispositionType> GetDispositionTypes()
    //{
    //    using (CSCMS.Data.CscmsEntities CscmsContext = new CSCMS.Data.CscmsEntities())
    //    {
    //        CscmsContext.Connection.Open();
    //        CscmsContext.DispositionType.MergeOption = System.Data.Objects.MergeOption.NoTracking;
    //        List<CSCMS.Data.DispositionType> list = CscmsContext.DispositionType.Where(it => it.IsActive == true).ToList();
    //        list = list.Where(it => !it.DispositionTypeName.Contains(MyConsts.DB_NONE_CASE_OPEN) && !it.DispositionTypeName.Contains(MyConsts.DB_SYSTEM_ADMIN_CLOSURE)).ToList();
    //        list = list.OrderBy(it => it.SortOrder).ToList();
    //        return list;
    //    }
    //}
}
