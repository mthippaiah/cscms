﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseNavigation.ascx.cs"
    Inherits="UserControls_CaseNavigation" %>
<asp:Panel runat="server" DefaultButton="btnSearch">
    <asp:Label ID="Label2" runat="server" Font-Bold="true">OAG #: </asp:Label>&nbsp;
    <asp:TextBox ID="txtOAGCauseNumber" runat="server" Font-Bold="true" Width="70" MaxLength="10"></asp:TextBox>
    <asp:Label ID="Label1" runat="server" Font-Bold="true">Cause #: </asp:Label>&nbsp;
    <asp:TextBox ID="txtCauseNumber" Width="100px" runat="server" Font-Bold="true" MaxLength="25"
        AccessKey="C"></asp:TextBox>&nbsp;
    <asp:Button ID="btnSearch" runat="server" Text="&nbsp;Detail&nbsp;" CssClass="submitMedium"
        OnClick="btnSearch_Click" CausesValidation="False" />&nbsp;
    <asp:Button ID="btnHearing" runat="server" Text="&nbsp;Hearing&nbsp;" CssClass="submitMedium"
        OnClick="btnHearing_Click" CausesValidation="False" />&nbsp;
    <asp:Label ID="lblNavigate" runat="server" Font-Bold="true">Navigate: </asp:Label>&nbsp;
    <asp:DropDownList ID="ddlCaseNavigation" runat="server" Width="180px" onchange="document.location.href = this.options[this.selectedIndex].value" />
</asp:Panel>
