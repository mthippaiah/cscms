﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_PersonMerge : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ddlPerson_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPerson.SelectedIndex == 0)
        {
            ClearFields();
        }
        else
        {
            Guid id = new Guid(ddlPerson.SelectedValue);
            CSCMS.Data.Person person = ((BasePage)Page).CscmsContext.Person.Include("State").Where(it => it.PersonGUID == id).FirstOrDefault();
            //Person person = dc.Persons.Single(t => t.PersonGUID == new Guid(ddlPerson.SelectedValue));

            lblFirstName.Text = person.FirstName;
            lblLastName.Text = person.LastName;
            lblMiddleName.Text = person.MiddleName;
            lblSex.Text = person.Sex == "M" ? "Male" : person.Sex == "F" ? "Female" : "";
            lblBirthDate.Text = person.Birthdate.HasValue ? person.Birthdate.Value.ToString("MM/dd/yyyy") : "";
            //lblRace1.Text = person.RaceType != null ? person.RaceType.RaceTypeName : "";
            //lblRace2.Text = person.RaceType1 != null ? person.RaceType1.RaceTypeName : "";
            //lblEthnicity.Text = person.EthnicityType != null ? person.EthnicityType.EthnicityTypeName : "";
            lblAddress1.Text = person.Address1;
            lblAddress2.Text = person.Address2;
            lblAddress3.Text = person.Address3;
            lblCity.Text = person.City;
            if (!person.StateReference.IsLoaded)
                person.StateReference.Load();
            if (person.State != null)
                lblState.Text = person.State.StateID;
            lblZip.Text = person.ZipCode.HasValue ? person.ZipCode.ToString() : "";
            lblFirm.Text = person.Firm;
            lblBarCardNo.Text = person.BarCardNumber.HasValue ? person.BarCardNumber.ToString() : "";

            grdCases.DataSourceID = dsPersons.ID;
            grdCases.DataBind();
        }
    }

    public Guid PersonGuid
    {
        get
        {
            if (ddlPerson.SelectedIndex > 0)
            {
                return new Guid(ddlPerson.SelectedValue);
            }
            else
            {
                return Guid.Empty;
            }
        }
    }

    public void SetPersonList(List<ListItem> items)
    {
        ClearFields();
        ddlPerson.Items.Clear();
        ddlPerson.Items.Add(new ListItem("- Select -", ""));

        foreach (ListItem item in items)
        {
            ddlPerson.Items.Add(item);
        }
    }

    private void ClearFields()
    {
        lblFirstName.Text = "";
        lblLastName.Text = "";
        lblMiddleName.Text = "";
        lblSex.Text = "";
        lblBirthDate.Text = "";
        //lblRace1.Text = "";
        //lblRace2.Text = "";
        //lblEthnicity.Text = "";
        lblAddress1.Text = "";
        lblAddress2.Text = "";
        lblAddress3.Text = "";
        lblCity.Text = "";
        lblState.Text = "";
        lblZip.Text = "";
        lblFirm.Text = "";
        lblBarCardNo.Text = "";
    }
    protected void dsPersons_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@UserId"].Value = 1;  //  this.CurrentUser.UserID;
        e.Command.Parameters["@PersonGUID"].Value = ddlPerson.SelectedValue;  //  this.CurrentUser.UserID;
    }
}
