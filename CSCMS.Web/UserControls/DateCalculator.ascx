﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateCalculator.ascx.cs"
    Inherits="UserControls_DateCalculator" %>
<div style="float:left; width:470px">
    <asp:Panel ID="pnldateCalculator" runat="server" DefaultButton="ButtonSetCalendar">
        <div style="float: left; width: 160px">
            <telerik:RadComboBox ID="RadComboBoxAnchorType" runat="server" Width="160px">
                <Items>
                    <telerik:RadComboBoxItem Text="Today" Value="Today" />
                    <telerik:RadComboBoxItem Text="Start of Calendar Year" Value="StartOfCY" />
                    <telerik:RadComboBoxItem Text="End of Calendar Year" Value="EndOfCY" />
                    <telerik:RadComboBoxItem Text="Start of Fiscal Year" Value="StartOfFY" />
                    <telerik:RadComboBoxItem Text="End of Fiscal Year" Value="EndOfFY" />
                    <telerik:RadComboBoxItem Text="Start of Month" Value="StartOfMonth" />
                    <telerik:RadComboBoxItem Text="End of Month" Value="EndOfMonth" />
                    <telerik:RadComboBoxItem Text="Start of week" Value="StartOfWeek" />
                    <telerik:RadComboBoxItem Text="End of week" Value="EndOfWeek" />
                    <telerik:RadComboBoxItem Text="Start of Previous Fiscal Year" Value="StartOfPFY" />
                    <telerik:RadComboBoxItem Text="End of Previous Fiscal Year" Value="EndOfPFY" />
                </Items>
            </telerik:RadComboBox>
        </div>
        <div style="float: left; width: 35px; text-align: right">
            <telerik:RadComboBox ID="RadComboBoxScaleDirection" runat="server" Width="30px">
                <Items>
                    <telerik:RadComboBoxItem Text="+" />
                    <telerik:RadComboBoxItem Text="-" />
                </Items>
            </telerik:RadComboBox>
        </div>
        <div style="float: left; width: 35px; text-align: right">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxScaleValue" runat="server" Width="20px"
                Value="0">
                <NumberFormat DecimalDigits="0" />
            </telerik:RadNumericTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="RadNumericTextBoxScaleValue"
                Width="10px" ValidationGroup="DateCalculation" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
        </div>
        <div style="float: left; width: 125px; text-align: right; vertical-align: middle">
            <telerik:RadComboBox ID="RadComboBoxScaleType" runat="server" Width="120px">
                <Items>
                    <telerik:RadComboBoxItem Text="Business Day" Value="BusinessDay" />
                    <telerik:RadComboBoxItem Text="Calendar Day" Value="CalendarDay" />
                    <telerik:RadComboBoxItem Text="Month" Value="Month" />
                    <telerik:RadComboBoxItem Text="Week" Value="Week" />
                    <telerik:RadComboBoxItem Text="Year" Value="Year" />
                </Items>
            </telerik:RadComboBox>
        </div>
        <div style="text-align: left;">
            &nbsp;
            <asp:Button ID="ButtonSetCalendar" runat="server" Text="Go" CssClass="submitMedium"
                ValidationGroup="DateCalculation" Style="width: 60px; vertical-align: middle"
                OnClick="ButtonSetCalendar_Click" />
        </div>
    </asp:Panel>
</div>
<div style="float:left; width:250px">
    <asp:Panel ID="pnlDatePicker" runat="server" DefaultButton="btnGo">
        <div style="float: left; width: 105px; height: 25px; text-align: left;">
            <telerik:RadDatePicker ID="dtRadDatePicker" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="DateRequiredFieldValidator" ControlToValidate="dtRadDatePicker"
                Width="10px" runat="server" ErrorMessage="*" ValidationGroup="DateSelection"></asp:RequiredFieldValidator>
        </div>
        <div style="float: left;">
            <asp:Button ID="btnGo" runat="server" Text="Go" Style="width: 60px; vertical-align: middle"
                CssClass="submitMedium" ValidationGroup="DateSelection" OnClick="btnGo_Click" />
        </div>
    </asp:Panel>
</div>
