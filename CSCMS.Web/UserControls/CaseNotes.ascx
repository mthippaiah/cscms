﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseNotes.ascx.cs" Inherits="UserControls_CaseNotes" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<telerik:RadAjaxLoadingPanel ID="loadingPanel" Runat="server" height="75px" width="75px">
    <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>' style="border:0px; padding-top: 40px" />
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
    <table cellspacing="0" cellpadding="0" width="100%" height="100%" border="0">
        <tr>
            <td valign="top">
                <b>Cause Notes:</b>
                <asp:ValidationSummary ID="valSummary" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="CaseNotes" HeaderText="Please enter a note and note date." />
                <table width="100%" cellspacing="0" cellpadding="2" border="0">
                    <tr>
                        <td colspan="3">
                            <asp:TextBox ID="txtCaseNote" runat="server" TextMode="MultiLine" Width="425" Height="70" />
                            <asp:RequiredFieldValidator ID="rfvCaseNote" runat="server" ControlToValidate="txtCaseNote" ErrorMessage="" ValidationGroup="CaseNotes" Display="None"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="fieldLabel" nowrap>Note Date: </td>
                        <td>
                            <telerik:RadDatePicker ID="dtNoteDate" Runat="server" Width="50" />
                            <asp:RequiredFieldValidator ID="rfvNoteDate" runat="server" ControlToValidate="dtNoteDate" ErrorMessage="" ValidationGroup="CaseNotes" Display="None"></asp:RequiredFieldValidator>
                        </td>
                        <td width="270">
                            <asp:Button ID="btnSaveNote" runat="server" Text="Save Note" CssClass="submitSmall" ValidationGroup="CaseNotes" OnClick="btnSaveNote_Click" />
                            &nbsp;
                            <asp:Button ID="btnCreateNewNote" runat="server" Text="Create New Note" CssClass="submitSmall" Width="100" OnClick="btnCreateNewNote_Click" />
                            &nbsp;
                            <asp:Button ID="btnDeleteNote" runat="server" Text="Delete Note" CssClass="submitSmallRed" Width="76" OnClick="btnDeleteNote_Click" />
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" style="border-left: solid 1px #5588BB;">
                <div class="noteList">
                    <asp:Label ID="lblNoNotes" runat="server" Visible="false" Text="&nbsp;There are currently no notes for this case." Font-Size="11px" ForeColor="#990000" />
                    <table width="447" cellspacing="0" cellpadding="2">
                        <asp:ListView ID="lvCaseNotes" runat="server" ItemPlaceholderID="itemPlaceholder">
                            <LayoutTemplate>
                                <tr id="itemPlaceholder" runat="server"></tr>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr class="bottomSectionAlt">
                                    <td width="20px" align="center"><asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../images/x_sm.gif" Visible='<%# ( (byte)Eval("NoteTypeID") != (byte)NoteTypeEnum.SystemGeneratedNote ) && (!this.Page.CurrentUser.ViewAllUser || (Convert.ToBoolean(Session["Impersonating"]) == true)) %>' CommandArgument='<%# Eval("NoteID") %>' OnCommand="btnDelete_Command" OnClientClick="return confirm('Are you sure you want to delete this note?');" /></td>
                                    <td><%# Eval("NoteDate", "{0:MM/dd/yyyy}") %></td>
                                    <td id="tdNote" runat="server">
                                        <telerik:RadToolTip ID="noteToolTip" runat="server" TargetControlID="tdNote" Position="TopCenter" Text='<%# GetFormattedNote(Eval("NoteText") as string, false) %>' Skin="Outlook" Animation="Fade" Width="600" Sticky="true" Visible='<%# (Eval("NoteText") as string).Length > MAX_NOTE_LENGTH %>' />
                                        <%# GetFormattedNote(Eval("NoteText") as string, false)%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr>
                                    <td align="center"><asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../images/x_sm.gif" Visible='<%# ( (byte)Eval("NoteTypeID") != (byte)NoteTypeEnum.SystemGeneratedNote ) && (!this.Page.CurrentUser.ViewAllUser || (Convert.ToBoolean(Session["Impersonating"]) == true)) %>' CommandArgument='<%# Eval("NoteID") %>' OnCommand="btnDelete_Command" OnClientClick="return confirm('Are you sure you want to delete this note?');" /></td>
                                    <td><%# Eval("NoteDate", "{0:MM/dd/yyyy}") %></td>
                                    <td id="tdNote" runat="server">
                                        <telerik:RadToolTip ID="noteToolTip" runat="server" TargetControlID="tdNote" Position="TopCenter" Text='<%# GetFormattedNote(Eval("NoteText") as string, false) %>' Skin="Outlook" Animation="Fade" Width="600" Sticky="true" Visible='<%# (Eval("NoteText") as string).Length > MAX_NOTE_LENGTH %>' />
                                        <%# GetFormattedNote(Eval("NoteText") as string, false)%>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
