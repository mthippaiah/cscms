﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_CaseNavigation : System.Web.UI.UserControl
{
    private bool includeEmptyItem = false;
    const string INVALID_CASE_COURT = "Cannot open. Sorry!. Case belongs to a different court ";

    public bool IncludeEmptyItem
    {
        get { return includeEmptyItem; }
        set { includeEmptyItem = value; }
    }

    public CSCMS.WebControls.MessageBox MessageBox { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cid"]))
            {
                LoadNavigation();
            }
            else
            {
                lblNavigate.Visible = false;
                ddlCaseNavigation.Visible = false;
            }
        }
    }

    public void LoadNavigation()
    {
        ddlCaseNavigation.Items.Clear();
        ddlCaseNavigation.ClearSelection();

        if (includeEmptyItem)
        {
            ddlCaseNavigation.Items.Add(new ListItem("", "#"));
        }

        //ddlCaseNavigation.Items.Add(new ListItem("- Case Summary", "CaseSummary.aspx?cid=" + Request.QueryString["cid"]));

        ddlCaseNavigation.Items.Add(new ListItem("- Case Detail", "ActionDetail.aspx?cid=" + Request.QueryString["cid"] + "&a=" + Request.QueryString["a"]));
        ddlCaseNavigation.Items.Add(new ListItem("- Add Hearing", "HearingDetail.aspx?cid=" + Request.QueryString["cid"] + "&a=" + Request.QueryString["a"]));
        //ddlCaseNavigation.Items.Add(new ListItem("- Relationships", "Relationships.aspx?cid=" + Request.QueryString["cid"] + "&a=" + Request.QueryString["a"]));

        DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
            CommandType.StoredProcedure,
            "GetActionPersons",
            new SqlParameter("@ActionGUID", ((BasePage)this.Page).CurrentAction.ActionGUID));

        foreach (DataRow row in ds.Tables[0].Rows)
        {
            ListItem item = new ListItem(string.Format("{0} - {1}",
                Helper.GetFormattedName(Helper.EvaluateDbStringValue(row["FirstName"]), Helper.EvaluateDbStringValue(row["MiddleName"]), Helper.EvaluateDbStringValue(row["LastName"]), Helper.EvaluateDbStringValue(row["NameSuffix"])),
                row["PersonTypeName"]), string.Format("PersonDetail.aspx?cid={0}&a={2}&pid={1}", Request.QueryString["cid"], row["PersonGUID"], Request.QueryString["a"]));

            ddlCaseNavigation.Items.Add(item);

            if (!string.IsNullOrEmpty(Request.QueryString["pid"]) && ((Guid)row["PersonGUID"]).CompareTo(new Guid(Request.QueryString["pid"])) == 0)
            {
                item.Selected = true;
            }
            else if (Request.Url.AbsolutePath.ToLower().Contains("persondetail.aspx") && ((BasePage)this.Page).CurrentActionPerson != null && ((BasePage)this.Page).CurrentActionPerson.PersonGUID == (Guid)row["PersonGUID"])
            {
                item.Selected = true;
            }
        }

        ddlCaseNavigation.Items.Add(new ListItem("- Add Reminder", "Reminder.aspx?cid=" + Request.QueryString["cid"] + "&a=" + Request.QueryString["a"]));

        if (Request.Url.AbsolutePath.ToLower().Contains("relationships.aspx"))
        {
            ddlCaseNavigation.Items.FindByText("- Relationships").Selected = true;
        }
        else if (Request.Url.AbsolutePath.ToLower().Contains("casedetail.aspx"))
        {
            ddlCaseNavigation.Items.FindByText("- Case Detail").Selected = true;
        }
        //else if (Request.Url.AbsolutePath.ToLower().Contains("casesummary.aspx"))
        //{
        //    ddlCaseNavigation.Items.FindByText("- Case Summary").Selected = true;
        //}
    }

    protected void btnHearing_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCauseNumber.Text) && string.IsNullOrEmpty(this.txtOAGCauseNumber.Text)) return;
        bool iOpenedConn = false;
        try
        {
            if (((BasePage)this.Page).CscmsContext.Connection.State != ConnectionState.Open)
            {
                ((BasePage)this.Page).CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (!string.IsNullOrEmpty(txtCauseNumber.Text))
            {
                DataSet ds = Helper.GetCaseByCauseNumber(((BasePage)this.Page).SqlConn, ((BasePage)this.Page).CurrentUser.UserID, txtCauseNumber.Text);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(((BasePage)this.Page).CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);
                    if (c.OCACourtID == ((BasePage)this.Page).CurrentUser.OCACourtID)
                        OpenHearingScreen((Guid)ds.Tables[0].Rows[0]["CauseGUID"], (short)ds.Tables[0].Rows[0]["NumberOfAction"]);
                    else
                        MessageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&cn={0}", txtCauseNumber.Text), true);
                }
            }
            else
            {
                DataSet ds = Helper.GetCaseByOAGNumber(((BasePage)this.Page).SqlConn, ((BasePage)this.Page).CurrentUser.UserID, this.txtOAGCauseNumber.Text);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(((BasePage)this.Page).CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);

                    if (c.OCACourtID == ((BasePage)this.Page).CurrentUser.OCACourtID)
                        OpenHearingScreen((Guid)ds.Tables[0].Rows[0]["CauseGUID"], (short)ds.Tables[0].Rows[0]["NumberOfAction"]);
                    else
                        MessageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&oag={0}", this.txtOAGCauseNumber.Text), true);
                }
            }
        }
        finally
        {
            if (iOpenedConn && ((BasePage)this.Page).CscmsContext.Connection.State == ConnectionState.Open)
                ((BasePage)this.Page).CscmsContext.Connection.Close();
        }
    }

    private void OpenHearingScreen(Guid causeGUID, short numberOfAction)
    {
        CSCMS.Data.Hearing h = Helper.GetLatestHearing(((BasePage)this.Page).CscmsContext, causeGUID, numberOfAction);
        if (h != null)
        {
            Response.Redirect(string.Format("~/MemberPages/HearingDetail.aspx?cid={0}&a={1}&hid={2}", causeGUID, numberOfAction, h.HearingID), true);
        }
        else
        {
            Response.Redirect(string.Format("~/MemberPages/HearingDetail.aspx?cid={0}&a={1}", causeGUID, numberOfAction), true);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCauseNumber.Text) && string.IsNullOrEmpty(this.txtOAGCauseNumber.Text)) return;
        bool iOpenedConn = false;
        try
        {
            if (((BasePage)this.Page).CscmsContext.Connection.State != ConnectionState.Open)
            {
                ((BasePage)this.Page).CscmsContext.Connection.Open();
                iOpenedConn = true;
            }
            if (!string.IsNullOrEmpty(txtCauseNumber.Text))
            {
                DataSet ds = Helper.GetCaseByCauseNumber(((BasePage)this.Page).SqlConn, ((BasePage)this.Page).CurrentUser.UserID, txtCauseNumber.Text);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(((BasePage)this.Page).CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);

                    if (c.OCACourtID == ((BasePage)this.Page).CurrentUser.OCACourtID)
                        Response.Redirect(string.Format("~/MemberPages/ActionDetail.aspx?cid={0}&a={1}", ds.Tables[0].Rows[0]["CauseGUID"].ToString(), ds.Tables[0].Rows[0]["NumberOfAction"].ToString()), true);
                    else
                        MessageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&cn={0}", txtCauseNumber.Text), true);
                }
            }
            else
            {
                DataSet ds = Helper.GetCaseByOAGNumber(((BasePage)this.Page).SqlConn, ((BasePage)this.Page).CurrentUser.UserID, this.txtOAGCauseNumber.Text);

                if (ds.Tables[0].Rows.Count == 1)
                {
                    CSCMS.Data.OCACourt c = Helper.GetCaseCourt(((BasePage)this.Page).CscmsContext, (Guid)ds.Tables[0].Rows[0]["CauseGUID"]);

                    if (c.OCACourtID == ((BasePage)this.Page).CurrentUser.OCACourtID)
                        Response.Redirect(string.Format("~/MemberPages/ActionDetail.aspx?cid={0}&a={1}", ds.Tables[0].Rows[0]["CauseGUID"].ToString(), ds.Tables[0].Rows[0]["NumberOfAction"].ToString()), true);
                    else
                        MessageBox.ErrorMessage = INVALID_CASE_COURT + c.CourtName;
                }
                else
                {
                    Response.Redirect(string.Format("~/MemberPages/CauseSearch.aspx?CaseTypeID=0&oag={0}", this.txtOAGCauseNumber.Text), true);
                }
            }
        }
        finally
        {
            if (iOpenedConn && ((BasePage)this.Page).CscmsContext.Connection.State == ConnectionState.Open)
                ((BasePage)this.Page).CscmsContext.Connection.Close();
        }
    }
}