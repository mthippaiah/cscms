﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseTree.ascx.cs" Inherits="UserControls_CaseTree" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="tree" runat="server" Skin="Office2007" OnNodeExpand="tree_NodeExpand" />
