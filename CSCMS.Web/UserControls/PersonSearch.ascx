﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PersonSearch.ascx.cs"
    Inherits="UserControls_PersonSearch" %>

<script type="text/jscript">
    var pscjava = new PersonSearchControl();
    function createPSCobjects() {
        var fn = document.getElementById("<%= txtFirstName.ClientID %>");
        var ln = document.getElementById("<%= txtLastName.ClientID %>");
        var sex = document.getElementById("<%= ddlSex.ClientID %>");
        var bdate = $find("<%= dtBirthdate.ClientID %>");
        var hfFD = document.getElementById("<%= HFFullDetails.ClientID %>");
        var grid = $find("<%=RadGridPersons.ClientID %>");
        return { fn: fn, ln: ln, sex: sex, bdate: bdate, hfFD: hfFD, grid: grid };
    }
    function SetSelectedRowValues() {
        return pscjava.SetSelectedRowValues('<%= this.ParentPageName %>')
    }
</script>

<asp:UpdatePanel ID="upPersonSearch" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="PSC_outerframe">
            <asp:HiddenField ID="HFFullDetails" runat="server" Value="false" />
            <div class="PSC_column1">
                Last Name:
            </div>
            <div class="PSC_column2">
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            </div>
            <div class="PSC_column1">
                First Name:
            </div>
            <div class="PSC_column2">
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            </div>
            <div class="PSC_column1">
                Middle Name:
            </div>
            <div class="PSC_column2">
                <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
            </div>
            <div class="PSC_column1">
                Sex:
            </div>
            <div class="PSC_column2">
                <asp:DropDownList ID="ddlSex" runat="server">
                    <asp:ListItem Value="">Select</asp:ListItem>
                    <asp:ListItem Value="F">Female</asp:ListItem>
                    <asp:ListItem Value="M">Male</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="PSC_column1">
                Birth Date:
            </div>
            <div class="PSC_column2">
                <telerik:RadDatePicker ID="dtBirthdate" runat="server">
                </telerik:RadDatePicker>
            </div>
            <div class="PSC_header">
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                    CssClass="submitMedium" />
                <asp:Button ID="btnClose1" runat="server" Text="Close" OnClientClick="return HidePopup();"
                    CssClass="submitMedium" />
            </div>
            <div>
                <telerik:RadGrid ID="RadGridPersons" runat="server" AllowPaging="True" AllowSorting="True"
                    OnNeedDataSource="RadGridPersons_OnNeedDataSource" OnPageIndexChanged="RadGridPersons_OnPageIndexChanged">
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                    <MasterTableView NoMasterRecordsText="There are currently no persons." DataKeyNames="PersonGUID">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="LastName" DataField="LastName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="FirstName" DataField="FirstName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="MiddleName" DataField="MiddleName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="BirthDate" DataField="Birthdate">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Sex" DataField="Sex">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Suffix" DataField="NameSuffix" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="InterpreterNeeded" DataField="InterpreterNeeded"
                                Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="FirstLanguage" DataField="FirstLanguage" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="SecurityRisk" DataField="SecurityRisk" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Disability" DataField="Disability" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="PersonGuid" DataField="PersonGUID" Display="false">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
            <div>
                <asp:Button ID="btnSelectPerson" runat="server" Text="Select Person" OnClientClick="return SetSelectedRowValues();"
                    CssClass="submitMedium" />
            </div>
            <div>
                <asp:CustomValidator ID="someid" ClientValidationFunction="pscjava.ValidateSearchInput"
                    runat="server" ValidationGroup="PersonSearch" ErrorMessage="Specify at least one value"></asp:CustomValidator>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress AssociatedUpdatePanelID="upPersonSearch" ID="progPersonSearch"
    runat="server">
    <ProgressTemplate>
        <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
            style="border: 0px; padding-top: 50px;" />
    </ProgressTemplate>
</asp:UpdateProgress>
