﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChildSupportCalculation.ascx.cs"
    Inherits="UserControls_ChildSupportCalculation" %>

<script type="text/jscript">
    var cssjava = new ChildSupportCalculation();
    function createMonthlyPaymentCalculationObjects() {
        var grossWages = $find("<%= RadNumericTextBoxGrossWages.ClientID %>");
        var cbEmployed = $find("<%= RadComboBoxEmployed.ClientID %>");
        var specialWages = $find("<%= RadNumericTextBoxSpecialWages.ClientID %>");
        var cbAdjChildren = $find("<%= RadComboBoxAdjNumChildren.ClientID %>");
        var monthlyHCost = $find("<%= RadNumericTextBoxMonthlyHealthCareCost.ClientID %>");
        var cbOtherChildren = $find("<%= RadComboBoxNumOtherChildren.ClientID %>");
        var adjAmount = $find("<%= RadNumericTextBoxAdjustedAmount.ClientID %>");
        var amountLabelId = "<%= LabelCalculatedAmount.ClientID %>";
        var calcAmountLabel = document.getElementById("<%= LabelCalculatedAmount.ClientID %>");
        return { grossWages: grossWages, cbEmployed: cbEmployed, specialWages: specialWages, cbAdjChildren: cbAdjChildren, monthlyHCost: monthlyHCost, cbOtherChildren: cbOtherChildren, adjAmount: adjAmount, amountLabelId: amountLabelId, calcAmountLabel: calcAmountLabel };
    }
    function createArrearsCalculationObjects() {
        var arrears = $find("<%= RadNumericTextBoxArrearsAmount.ClientID %>");
        var interestRate = $find("<%= RadNumericTextBoxInterestRate.ClientID %>");
        var terms = $find("<%= RadNumericTextBoxTerms.ClientID %>");
        var calcAmountLabel = document.getElementById("<%= LabelCalculatedAmount2.ClientID %>");
        var RadNumericTextBoxAdjustedAmount2 = $find("<%= RadNumericTextBoxAdjustedAmount2.ClientID %>");
        return { arrears: arrears, interestRate: interestRate, terms: terms, calcAmountLabel: calcAmountLabel, RadNumericTextBoxAdjustedAmount2: RadNumericTextBoxAdjustedAmount2 };
    }
    function createMedicalPastDueCalculationObjects() {
        var arrears = $find("<%= RadNumericTextBoxArrearsAmount2.ClientID %>");
        var interestRate = $find("<%= RadNumericTextBoxInterestRate2.ClientID %>");
        var terms = $find("<%= RadNumericTextBoxTerms2.ClientID %>");
        var calcAmountLabel = document.getElementById("<%= LabelCalculatedAmount3.ClientID %>");
        var RadNumericTextBoxAdjustedAmount3 = $find("<%= RadNumericTextBoxAdjustedAmount3.ClientID %>");
        return { arrears: arrears, interestRate: interestRate, terms: terms, calcAmountLabel: calcAmountLabel, RadNumericTextBoxAdjustedAmount3: RadNumericTextBoxAdjustedAmount3 };
    }
    function createCalculateArrearsObjects() {
        var arrears = $find("<%= RadNumericTextBoxArrearsAmount.ClientID %>");
        var interestRate = $find("<%= RadNumericTextBoxInterestRate.ClientID %>");
        var terms = $find("<%= RadNumericTextBoxTerms.ClientID %>");
        var calcAmountLabel = document.getElementById("<%= LabelCalculatedAmount2.ClientID %>");
        var RadNumericTextBoxAdjustedAmount2 = $find("<%= RadNumericTextBoxAdjustedAmount2.ClientID %>");
        return { arrears: arrears, interestRate: interestRate, terms: terms, calcAmountLabel: calcAmountLabel, RadNumericTextBoxAdjustedAmount2: RadNumericTextBoxAdjustedAmount2 };
    }
    function createCalculatePastDueAmountObjects() {
        var arrears = $find("<%= RadNumericTextBoxArrearsAmount2.ClientID %>");
        var interestRate = $find("<%= RadNumericTextBoxInterestRate2.ClientID %>");
        var terms = $find("<%= RadNumericTextBoxTerms2.ClientID %>");
        var calcAmountLabel = document.getElementById("<%= LabelCalculatedAmount3.ClientID %>");
        var RadNumericTextBoxAdjustedAmount3 = $find("<%= RadNumericTextBoxAdjustedAmount3.ClientID %>");
        return { arrears: arrears, interestRate: interestRate, terms: terms, calcAmountLabel: calcAmountLabel, RadNumericTextBoxAdjustedAmount3: RadNumericTextBoxAdjustedAmount3 };
    }

    function ResetMonthlyPaymentCalculation() {
        return cssjava.ResetMonthlyPaymentCalculation();
    }
    function ResetArrearsCalculation() {
        return cssjava.ResetArrearsCalculation();
    }
    function ResetMedicalPastDueCalculation() {
        return cssjava.ResetMedicalPastDueCalculation();
    }
    function CloseChildCalcultionPopup() {
        var calcPopup = $find("ModalPopExtBehaviorID");
        calcPopup.hide();
        return false;
    }
</script>

<asp:UpdatePanel ID="upChilSupport" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="CSC_outerframe">
            <div class="CSC_header">
                Child Support Amount
            </div>
            <div class="CSC_column1">
                Calendar Year:
            </div>
            <div class="CSC_column2">
                <telerik:RadComboBox ID="RadComboBoxCalendarYear" runat="server" Width="130px" ZIndex="10000000"
                    CssClass="CSC_control">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ValidationGroup="SUPPORTCALCULATION"
                    CssClass="CSC_control" ControlToValidate="RadComboBoxCalendarYear" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Monthly Gross Taxable Income (of NCP):
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxGrossWages" runat="server" Width="126px">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="SUPPORTCALCULATION"
                    ControlToValidate="RadNumericTextBoxGrossWages" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Employed/Self-Employed:
            </div>
            <div class="CSC_column2">
                <telerik:RadComboBox ID="RadComboBoxEmployed" runat="server" MarkFirstMatch="True"
                    Width="130px" ZIndex="10000000" CssClass="CSC_control">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Selected="True" Text="Select Value" Value="Select Value" />
                        <telerik:RadComboBoxItem runat="server" Text="Employed" Value="Employed" />
                        <telerik:RadComboBoxItem runat="server" Text="Self-Employed" Value="Self-Employed" />
                    </Items>
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="SUPPORTCALCULATION"
                    CssClass="CSC_control" ControlToValidate="RadComboBoxEmployed" ErrorMessage="*"
                    InitialValue="Select Value"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Monthly Other Nontaxable Income(Military etc.):
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxSpecialWages" runat="server" Width="126px">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="SUPPORTCALCULATION"
                    ControlToValidate="RadNumericTextBoxSpecialWages" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Number of Children Before the Court (entered in CSMS):
            </div>
            <div class="CSC_column2">
                <asp:Label ID="LabelNumChildren" runat="server" Width="20px" Text=" "></asp:Label>
            </div>
            <div class="CSC_column1">
                Adjusted Number of Children Before the Court:
            </div>
            <div class="CSC_column2">
                <telerik:RadComboBox ID="RadComboBoxAdjNumChildren" runat="server" Width="130px"
                    ZIndex="10000000" CssClass="CSC_control">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Selected="True" Text="Select Value" Value="Select Value" />
                        <telerik:RadComboBoxItem runat="server" Text="1" Value="1" />
                        <telerik:RadComboBoxItem runat="server" Text="2" Value="2" />
                        <telerik:RadComboBoxItem runat="server" Text="3" Value="3" />
                        <telerik:RadComboBoxItem runat="server" Text="4" Value="4" />
                        <telerik:RadComboBoxItem runat="server" Text="5" Value="5" />
                        <telerik:RadComboBoxItem runat="server" Text="6" Value="6" />
                        <telerik:RadComboBoxItem runat="server" Text="7" Value="7" />
                        <telerik:RadComboBoxItem runat="server" Text="8" Value="8" />
                        <telerik:RadComboBoxItem runat="server" Text="9" Value="9" />
                        <telerik:RadComboBoxItem runat="server" Text="10" Value="10" />
                        <telerik:RadComboBoxItem runat="server" Text="11" Value="11" />
                        <telerik:RadComboBoxItem runat="server" Text="12" Value="12" />
                    </Items>
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="SUPPORTCALCULATION"
                    CssClass="CSC_control" ControlToValidate="RadComboBoxAdjNumChildren" ErrorMessage="*"
                    InitialValue="Select Value"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Number of Other Children (outside of this Action):
            </div>
            <div class="CSC_column2">
                <telerik:RadComboBox ID="RadComboBoxNumOtherChildren" runat="server" Width="130px"
                    ZIndex="10000000" CssClass="CSC_control">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Selected="True" Text="Select Value" Value="Select Value" />
                        <telerik:RadComboBoxItem runat="server" Text="0" Value="0" />
                        <telerik:RadComboBoxItem runat="server" Text="1" Value="1" />
                        <telerik:RadComboBoxItem runat="server" Text="2" Value="2" />
                        <telerik:RadComboBoxItem runat="server" Text="3" Value="3" />
                        <telerik:RadComboBoxItem runat="server" Text="4" Value="4" />
                        <telerik:RadComboBoxItem runat="server" Text="5" Value="5" />
                        <telerik:RadComboBoxItem runat="server" Text="6" Value="6" />
                        <telerik:RadComboBoxItem runat="server" Text="7" Value="7" />
                        <telerik:RadComboBoxItem runat="server" Text="8" Value="8" />
                        <telerik:RadComboBoxItem runat="server" Text="9" Value="9" />
                        <telerik:RadComboBoxItem runat="server" Text="10" Value="10" />
                        <telerik:RadComboBoxItem runat="server" Text="11" Value="11" />
                        <telerik:RadComboBoxItem runat="server" Text="12" Value="12" />
                    </Items>
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="SUPPORTCALCULATION"
                    CssClass="CSC_control" ControlToValidate="RadComboBoxNumOtherChildren" ErrorMessage="*"
                    InitialValue="Select Value"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Monthly Health Care Cost:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxMonthlyHealthCareCost" runat="server"
                    Width="126px">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="SUPPORTCALCULATION"
                    ControlToValidate="RadNumericTextBoxMonthlyHealthCareCost" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Monthly Calculated Child Support Amount:
            </div>
            <div class="CSC_column2">
                <asp:Label ID="LabelCalculatedAmount" runat="server" Font-Bold="True" Text=" "></asp:Label>
            </div>
            <div class="CSC_column1">
                Monthly User Adjusted Child Support Amount:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxAdjustedAmount" runat="server" Width="126px">
                </telerik:RadNumericTextBox>
            </div>
            <div class="CSC_buttons">
                <asp:Button ID="ButtonCalculate" runat="server" Text="Calculate" ValidationGroup="SUPPORTCALCULATION"
                    OnClick="ButtonCalculate_Click" />
                <asp:Button ID="ButtonReset" runat="server" Text="Reset" />
                <asp:Button ID="ButtonApplyAmount" runat="server" Text="Apply Amount" OnClientClick="return ApplyAdjustedAmount();" />
                <asp:Button ID="ButtonClose" runat="server" Text="Close" OnClientClick="return CloseChildCalcultionPopup();" />
            </div>
            <div class="CSC_header">
                Arrears Amortization
            </div>
            <div class="CSC_column1">
                Arrears Amount:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxArrearsAmount" runat="server" Width="126px">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="SUPPORTCALCULATION2"
                    ControlToValidate="RadNumericTextBoxArrearsAmount" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Interest Rate:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxInterestRate" runat="server" Width="126px"
                    Value="6">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ValidationGroup="SUPPORTCALCULATION2"
                    ControlToValidate="RadNumericTextBoxInterestRate" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Terms(in months. Enter 0 to Calculate) :
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxTerms" runat="server" Width="126px"
                    DataType="System.Int16">
                    <NumberFormat DecimalDigits="0" />
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ValidationGroup="SUPPORTCALCULATION2"
                    ControlToValidate="RadNumericTextBoxTerms" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Monthly Payment Amount:
            </div>
            <div class="CSC_column2">
                <asp:Label ID="LabelCalculatedAmount2" runat="server" Font-Bold="True" Text=" "></asp:Label>
            </div>
            <div class="CSC_column1">
                Monthly User Estimated/Adjusted Payment Amount:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxAdjustedAmount2" runat="server" Width="126px">
                </telerik:RadNumericTextBox>
            </div>
            <div class="CSC_buttons">
                <asp:Button ID="ButtonCalculateArrears" runat="server" Text="Calculate" OnClientClick="return cssjava.CalculateArrears();" />
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="ButtonReset2" runat="server" Text="Reset" />
                &nbsp;&nbsp;&nbsp;<asp:Button ID="ButtonApplyAmount2" runat="server" Text="Apply Amount"
                    OnClientClick="return ApplyArrearsAmount();" />
                &nbsp; &nbsp;<asp:Button ID="ButtonClose2" runat="server" Text="Close" OnClientClick="return CloseChildCalcultionPopup();" />
            </div>
            <div class="CSC_header">
                Past Due Medical Amount
            </div>
            <div class="CSC_column1">
                Arrears Amount:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxArrearsAmount2" runat="server" Width="126px">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ValidationGroup="SUPPORTCALCULATION3"
                    ControlToValidate="RadNumericTextBoxArrearsAmount2" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Interest Rate:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxInterestRate2" runat="server" Width="126px"
                    Value="6">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ValidationGroup="SUPPORTCALCULATION3"
                    ControlToValidate="RadNumericTextBoxInterestRate2" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Terms(in months. Enter 0 to Calculate):
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxTerms2" runat="server" Width="126px"
                    DataType="System.Int16">
                    <NumberFormat DecimalDigits="0" />
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ValidationGroup="SUPPORTCALCULATION3"
                    ControlToValidate="RadNumericTextBoxTerms2" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
            <div class="CSC_column1">
                Monthly Payment Amount:
            </div>
            <div class="CSC_column2">
                <asp:Label ID="LabelCalculatedAmount3" runat="server" Font-Bold="True" Text=" "></asp:Label>
            </div>
            <div class="CSC_column1">
                Monthly User Estimated/Adjusted Payment Amount:
            </div>
            <div class="CSC_column2">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxAdjustedAmount3" runat="server" Width="126px">
                </telerik:RadNumericTextBox>
            </div>
            <div class="CSC_buttons">
                <asp:Button ID="ButtonMedicalAmount" runat="server" Text="Calculate" OnClientClick="return cssjava.CalculatePastDueAmount();" />
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="ButtonReset3" runat="server" Text="Reset" />
                &nbsp;&nbsp;&nbsp;<asp:Button ID="ButtonAppyAmount3" runat="server" Text="Apply Amount"
                    OnClientClick="return ApplyPastDueMedicalAmount();" />
                &nbsp; &nbsp;<asp:Button ID="ButtonClose3" runat="server" Text="Close" OnClientClick="return CloseChildCalcultionPopup();" />
            </div>
            <div>
                <asp:Button ID="ButtonCalculateAll" runat="server" Text="Calculate All" OnClick="ButtonCalculateAll_Click"
                    OnClientClick="return cssjava.ValidateSections();" />
                &nbsp;&nbsp;&nbsp;<asp:Button ID="ButtonApplyAll" runat="server" Text="Apply All"
                    OnClientClick="return ApplyAll()" />
                &nbsp; &nbsp;<asp:Button ID="ButtonClose4" runat="server" Text="Close" OnClientClick="return CloseChildCalcultionPopup();" />
                &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/MemberPages/CalculatorRelatedTables.aspx"
                    Target="_blank" ToolTip="Child Support Calculator Tables" CssClass="submitMedium">Help</asp:HyperLink>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress AssociatedUpdatePanelID="upChilSupport" ID="progChilSupport"
            runat="server">
            <ProgressTemplate>
                <img alt="Loading..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Office2007.Common.loading.gif") %>'
                    style="border: 0px; padding-top: 50px;" />
            </ProgressTemplate>
        </asp:UpdateProgress>