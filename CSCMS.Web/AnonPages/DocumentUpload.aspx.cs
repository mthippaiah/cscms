﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class AnonPages_DocumentUpload : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string userId = "";
        CSCMS.Data.User user = null;
        if (this.Page.Request.Form["UserID"] != null)
        {
            userId = this.Page.Request.Form["UserID"];
            short uid = short.Parse(userId);
            user = CscmsContext.User.Where(it => it.UserId == uid).FirstOrDefault();
        }
        if (user != null && this.Page.Request.Form["SecretWord"] != null && this.Page.Request.Form["SecretWord"] == "ActiveXFileUpload")
        {
            string actionGuid = "";

            if (this.Page.Request.Form["ActionGUID"] != null)
            {
                actionGuid = this.Page.Request.Form["ActionGUID"];
            }
            // Check to see if a file was actually selected
            if (this.fileUpload.PostedFile != null && fileUpload.PostedFile.ContentLength > 0)
            {
                Guid aid = new Guid(actionGuid);

                short courtId = (from a in CscmsContext.Action
                                 join c in CscmsContext.Cause on a.Cause.CauseGUID equals c.CauseGUID
                                 where a.ActionGUID == aid
                                 select c.OCACourt.OCACourtID).FirstOrDefault();

                DocumentDir dd = Helper.CreateDirectory(courtId.ToString(), userId, actionGuid, ConfigurationManager.AppSettings["FileUploadPath"]);

                // Get the filename
                string fileName = Path.GetFileName(fileUpload.PostedFile.FileName);
                // Save the file to the folder
                fileName = Path.Combine(dd.ActionLevelDir + "\\", fileName);
                
                int pos = int.Parse(this.Page.Request.Form["SeekPosition"]);
                int fileLength = int.Parse(this.Page.Request.Form["FileLength"]);  //This is the actual file length

                if (File.Exists(fileName) && pos == 0)
                    File.Delete(fileName);

                if (File.Exists(fileName))
                {
                    byte[] bytes = new byte[fileUpload.PostedFile.InputStream.Length];
                    fileUpload.PostedFile.InputStream.Read(bytes, 0, (int)fileUpload.PostedFile.InputStream.Length);
                    using (FileStream fs = File.Open(fileName, FileMode.Append, FileAccess.Write))
                    {
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }
                else
                {
                    fileUpload.PostedFile.SaveAs(fileName);
                }
                Response.Write("File upload complete, UserID: " + userId + " ActionGUID: " + actionGuid);
            }

        }
        else
        {
            Response.Redirect(this.Page.Request.ApplicationRoot() + "/AnonPages/Unauthorized.aspx");
        }
    }
}
