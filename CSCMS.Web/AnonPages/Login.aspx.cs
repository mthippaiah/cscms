﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using EncryptDecrypt;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using CSCMS.Data;

public partial class Login : BasePage
{
    public ConnectionStringSettings connString;
    public SqlConnection conn;
    const string LOCKOUT_MSG = "Your account has been locked out.";
    const string STD_MSG = "Invalid user name and/or password";
    const string ACT_EXPIRED = "Account has expired!.";
    const string ACT_NOT_ACTIVATED = "Account not activated!.";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // get the user cookie
            HttpCookie cookie1 = Request.Cookies.Get("Username");
            //HttpCookie cookie2 = Request.Cookies.Get("Password");
            HttpCookie cookie3 = Request.Cookies.Get("RememberMe");

            if (cookie1 != null || cookie3 != null)
            {
                // Just Load the Values in TextBoxes From Cookies
                txtBoxUserName.Text = cookie1.Value.ToString().Replace("''", "'");
                //txtBoxPassword.Text = cookie2.Value.ToString();
                chkBoxRememberMe.Checked = true;
                txtBoxPassword.Focus();
            }
            else
            {
                txtBoxUserName.Focus();
            }
        }
    }

    protected void btnLogIn_Click(object sender, EventArgs e)
    {
        Action<string> showError = delegate(string s)
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.Text = s;
        };

        Page.Validate();
        if (!Page.IsValid) return;

        string prepStringPW = null;
        EncDec encryptString = new EncDec();

        string temp = encryptString.EncryptText("qwert");
        prepStringPW = encryptString.EncryptText(txtBoxPassword.Text);

        // find out if the email address is being used for the login name
        CSCMS.Data.User user = null;
        if (txtBoxUserName.Text.Contains("@"))  //TODO: There is better way to find if this is an email id.
        {
            user = base.CscmsContext.User.Where(it => it.email_address == txtBoxUserName.Text.Trim()).FirstOrDefault();
        }
        else
        {
            user = base.CscmsContext.User.Where(it => it.UserName == txtBoxUserName.Text.Trim()).FirstOrDefault();
        }

        if (user != null)
        {
            if (user.LockoutUntil.HasValue && user.LockoutUntil.Value > DateTime.Now)
            {
                showError(LOCKOUT_MSG);
                return;
            }

            if (user.EndDate.HasValue && user.EndDate.Value < DateTime.Now)
            {
                showError(ACT_EXPIRED);
                return;
            }

            if (user.password != prepStringPW)
            {
                if (!CheckAndLockOutAccount(user))
                    showError(STD_MSG);
                return;
            }

            if (user.active)
            {
                Session["LoggedInUser"] = new LoggedInUserData { UserID = user.UserId, UserName = user.UserName };

                user.last_login_dts = DateTime.Now;
                base.CscmsContext.SaveChanges();

                Response.Cookies["UID"].Value = user.UserId.ToString();
                Response.Cookies["LoginDTS"].Value = DateTime.Now.ToString();

                HandleRememberMe();

                FormsAuthentication.SetAuthCookie(user.UserName, false);

                // if this is the users first time to login, make them change their password
                // use the initial_activation value to determin this.

                if (Request.Cookies["ReturnUrl"] != null && Request.Cookies["ReturnUrl"].Value != "" && !Request.Cookies["ReturnUrl"].Value.Contains("Login.aspx"))
                {
                    string returnUrl = this.Request.Cookies["ReturnUrl"].Value;
                    HttpCookie myCookie = new HttpCookie("ReturnUrl");
                    myCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie);
                    Response.Redirect(returnUrl, false);
                    return;
                }
                Response.Redirect(this.Page.Request.ApplicationRoot() + "/MemberPages/MyPage.aspx");
            }
            else
            {
                showError(ACT_NOT_ACTIVATED);
            }
        }
        else
        {
            showError(STD_MSG);
        }
    }

    private void HandleRememberMe()
    {
        if ((this.chkBoxRememberMe.Checked))
        {
            HttpCookie cookie = new HttpCookie("RememberMe", "1");
            HttpCookie usernamecookie = new HttpCookie("Username", txtBoxUserName.Text.Trim().Replace("'", "''"));
            //HttpCookie passwordcookie = new HttpCookie("Password", txtBoxPassword.Text);
            usernamecookie.Expires = DateTime.Now.AddMonths(3);
            //passwordcookie.Expires = DateTime.Now.AddMonths(3);
            cookie.Expires = DateTime.Now.AddMonths(3);

            Response.Cookies.Add(cookie);
            Response.Cookies.Add(usernamecookie);
            //Response.Cookies.Add(passwordcookie);

            //Response.Cookies["UID"].Expires = DateTime.Now.AddMonths(3);
        }
        else
        {
            HttpCookie cookie = new HttpCookie("RememberMe", "0");
            HttpCookie usernamecookie = new HttpCookie("Username", null);
            //HttpCookie passwordcookie = new HttpCookie("Password", null);
            usernamecookie.Expires = DateTime.Now.AddMonths(3);
            //passwordcookie.Expires = DateTime.Now.AddMonths(3);
            cookie.Expires = DateTime.Now.AddMonths(3);

            Response.Cookies.Add(cookie);
            Response.Cookies.Add(usernamecookie);
            //Response.Cookies.Add(passwordcookie);
        }
    }
    private bool CheckAndLockOutAccount(User user)
    {
        int loginAttempts = int.Parse(hfLoginAttempts.Value);
        int allowedAttempts = int.Parse(ConfigurationManager.AppSettings["LockoutLoginAttempts"]);
        if (loginAttempts >= allowedAttempts)
        {
            int minutes = int.Parse(ConfigurationManager.AppSettings["LockoutMinutes"]);
            this.lblErrorMessage.Text = LOCKOUT_MSG;
            DateTime lockOutUntill = DateTime.Now.AddMinutes(minutes);
            //user.LockoutUntil = lockOutUntill
            base.CscmsContext.SaveChanges();
            return true;
        }
        else
        {
            hfLoginAttempts.Value = (loginAttempts + 1).ToString();
        }
        return false;
    }

}
