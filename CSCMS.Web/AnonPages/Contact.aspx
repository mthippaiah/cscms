﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMSPlain.master" AutoEventWireup="true"
    CodeFile="Contact.aspx.cs" Inherits="AnonPages_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <table width="960" border="1" cellspacing="0" id="tblSpace" cellpadding="0" style="border-style: none solid none solid;
        border-width: 2px; border-color: #000000;">
        <tr>
            <td style="height: 500px; vertical-align:top;">
                <pre>
                    <br />
                    The CSCMS Administrator and OCA Service Desk software support can be reached via email 
                    at <a href="mailto:Service.Desk@txcourts.gov?Subject=cscms">Service.Desk@txcourts.gov.</a>
                </pre>
            </td>
        </tr>
    </table>
</asp:Content>
