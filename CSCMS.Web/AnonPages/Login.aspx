﻿<%@ Page Title="CSCMS - Login" Language="C#" MasterPageFile="~/CSCMSPlain.master"
    AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style2
        {
            width: 400px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
<asp:HiddenField ID="hfLoginAttempts" runat="server" Value="0" />
<table width="960" border="1" cellspacing="0" id="tblSpace" cellpadding="0" 
    style="border-style: none solid none solid; border-width: 2px; border-color: #000000;">
    <tr>
        <td align="center" style="background-color:White;">
            <br /><br />
    <table id="tblLogin" align="center" border="0" cellpadding="3" cellspacing="0" class="style2"
        width="400" frame="box" style="border: 3px outset #000099;">
        <tr>
            <td align="center" colspan="2" bgcolor="#C4ECFB">
                <asp:Label ID="lblTableHeader" runat="server" Font-Bold="True" ForeColor="#000099"
                    Text="Login Form"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" width="200">
                <asp:Label ID="lblUserName" runat="server" Font-Size="Small" Text="User Name:" ToolTip="User Name"></asp:Label>
            </td>
            <td align="left" width="200">
                <asp:TextBox ID="txtBoxUserName" runat="server" Width="160px" Text="" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ErrorMessage="User Name is a required field"
                    Font-Size="Small" ToolTip="User Name is a required field" ControlToValidate="txtBoxUserName">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lblPassword" runat="server" Font-Size="Small" Text="Password:" ToolTip="Password"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtBoxPassword" runat="server" TextMode="Password" Width="160px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ErrorMessage="Password is a required field"
                    Font-Size="Small" ToolTip="Password is a required field" ControlToValidate="txtBoxPassword">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:CheckBox ID="chkBoxRememberMe" runat="server" Font-Size="Small" Text="&nbsp;Remember me next time&nbsp;"
                    ToolTip="Remember me next time" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:ValidationSummary ID="loginValidationSummary" runat="server" Font-Size="Small" />
                <asp:Label ID="lblErrorMessage" runat="server" Font-Size="Small" ForeColor="Red"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnLogIn" runat="server" BackColor="#C4ECFB" BorderStyle="None" CssClass="submitMedium"
                    Font-Size="Small" Text="&nbsp;Login&nbsp;" onclick="btnLogIn_Click" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:HyperLink ID="forgotPasswordHyperLink" runat="server" NavigateUrl="Contact.aspx"
                    ToolTip="Forgot User Name / Password?" Font-Size="Small">Forgot User Name / Password?</asp:HyperLink>
            </td>
        </tr>
    </table>
        <br /><br />
        </td>
    </tr>
</table>
</asp:Content>
