﻿using System;

public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Abandon();
        System.Web.Security.FormsAuthentication.SignOut();
        if (Session["LoggedInUser"] != null)
            Session.Remove("LoggedInUser"); ;
        //Response.Redirect("Login.aspx");
    }
}
