﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AnonPages_Contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void OnPreRender(EventArgs e)
    {
        ((CSCMSPlain)this.Master).SetFooterLink1Url(this.Page.Request.ApplicationRoot() + "/MemberPages/MyPage.aspx", "MyPage");
        base.OnPreRender(e);
    }
}
