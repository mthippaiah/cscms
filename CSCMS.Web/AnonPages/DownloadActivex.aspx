﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CSCMSPlain.master" AutoEventWireup="true"
    CodeFile="DownloadActivex.aspx.cs" Inherits="AnonPages_DownloadActivex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="Server">
    <div>
        <pre style="text-align: left">
        <b>Please download setup.zip and run setup.exe to install FileUpload activex control.
       This control enables user to upload documents by dragging and dropping into the box,
       in addition to file lookup. Please note, drag-drop works only with Internet Explorer browser.
       Other browser need to use file lookup. 
       
       Instructions:
       1.  Copy the setup.zip to a directory on your machine.
       2.  Extract the files to the same directory.
       3.  Double click setup.exe. 
       </b>
    </pre>
    </div>
    <div style="text-align:center">
        <asp:HyperLink ID="fileDownLoad" Text="setup.zip" runat="server"></asp:HyperLink>
    </div>
</asp:Content>
