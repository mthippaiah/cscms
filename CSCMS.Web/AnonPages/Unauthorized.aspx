﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cscmsPlain.master" AutoEventWireup="true" CodeFile="Unauthorized.aspx.cs" Inherits="AnonPages_Unauthorized" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
<br />
<CSCMSUC:MessageBox ID="messageBox" runat="server" ErrorMessage="You are not authorized to access this page. Please contact the CSCMS administrator for more information." />
</asp:Content>

