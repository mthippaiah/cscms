﻿<%@ Page Title="Logout" Language="C#" MasterPageFile="~/CSCMSPlain.master" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
        .style7
        {
            width: 400px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" Runat="Server">
<table width="960" border="1" cellspacing="0" id="tblSpace" cellpadding="0" 
    style="border-style: none solid none solid; border-width: 2px; border-color: #000000;">
    <tr>
        <td align="center" style="background-color:White;">
            <br /><br />
    <table id="tblLogout" class="style7" align="center" cellpadding="3" 
    cellspacing="0" width="400" frame="box" border="0" 
    style="border: 3px outset #000099;">
        <tr>
            <td align="center" bgcolor="#C4ECFB">
                <asp:Label ID="lblTableHeader" runat="server" Font-Bold="True" 
                    ForeColor="#000099" Text="Log Out Successful" ToolTip="Log Out Successful"></asp:Label>
                </td>
        </tr>
        <tr>
            <td align="center" style="font-size:small;">
                <br />
                    You have been logged out of the Office of 
                    Court Administration CHILD SUPPORT CASE MANAGEMENT SYSTEM system. Re-entry can be accomplished by
                    <a href="Login.aspx">System Login</a>.
                <br /><br />              
            </td>
        </tr>
    </table>
        <br /><br />
        </td>
    </tr>
</table>
</asp:Content>

