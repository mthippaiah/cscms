﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AnonPages_DownloadActivex : System.Web.UI.Page
{
    public string ActivexSetupZipUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/bin/setup.zip";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.fileDownLoad.NavigateUrl = ActivexSetupZipUrl;
    }
}
