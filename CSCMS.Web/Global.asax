﻿<%@ Application Language="C#" %>

<script RunAt="server">

    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {
        if (!AnonymousAccessCheck.IsAnonymousAccessAllowed(this.Request))
        {
            if (!this.Request.IsAuthenticated)
            {
                if (this.Request.Url.AbsoluteUri.ToLower().Contains(".aspx"))
                {
                    this.Response.Cookies["ReturnUrl"].Value = this.Request.Url.AbsoluteUri;
                }

                if (!this.Request.Url.AbsoluteUri.Contains("Login.aspx"))
                {
                    Response.Redirect(HttpContext.Current.Request.ApplicationRoot() + "/AnonPages/Login.aspx");
                    return;
                }
            }
        }
    }

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        if (!this.Context.Request.Url.AbsoluteUri.Contains("ScriptResource.axd"))
        {
            // Code that runs when an unhandled error occurs
            Exception objErr = Server.GetLastError().GetBaseException();
            string err = "Error in: " + Request.Url.ToString() + System.Environment.NewLine + System.Environment.NewLine;
            err += "Error Message: " + System.Environment.NewLine + objErr.Message.ToString() + System.Environment.NewLine + System.Environment.NewLine;
            err += "Stack Trace: " + System.Environment.NewLine + objErr.StackTrace.ToString();

            DateTime dt = DateTime.Now;
            string strFileName = "Global_ASAX_dt_" + dt.Month.ToString() + "-" + dt.Day.ToString();
            strFileName += "-" + dt.Year.ToString() + "_Time_" + dt.Hour.ToString() + "_";
            strFileName += (dt.Minute.ToString() + "_" + dt.Second.ToString() + "_" + dt.Millisecond.ToString() + ".txt");

            if (!System.IO.Directory.Exists(Server.MapPath("~") + @"\Errors"))
                System.IO.Directory.CreateDirectory(Server.MapPath("~") + @"\Errors");

            string LogFilePath = Server.MapPath("~") + "\\\\Errors\\\\" + strFileName;
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(LogFilePath))
            {
                sw.WriteLine(err);
            }
        }
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>

