﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EncryptDecrypt;

public partial class ChangePassword : BasePage
{
    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        LoggedInUserData temp = (LoggedInUserData)Session["LoggedInUser"];

        CSCMS.Data.User user = CscmsContext.User.Where(it => it.UserId == temp.UserID).FirstOrDefault();
        if (user != null)
        {
            string oldpwd = user.password;
            EncDec ed = new EncDec();
            string pwd = ed.EncryptText(this.CurrentPassword.Text);

            if(pwd != user.password)
            {
                FailureText.Text = "Invalid password";
                return;
            }

            user.password = ed.EncryptText(this.NewPassword.Text);
            CscmsContext.SaveChanges();
            tblChangePassword.Visible = false;
            tblPasswordChangeSuccess.Visible = true;
        }
    }
}
