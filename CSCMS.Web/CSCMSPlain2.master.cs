﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Principal;
using System.Linq;
using Telerik.Web.UI;

public partial class CSCMSPlain2 : System.Web.UI.MasterPage
{

    private DateTime pageLoad;
    private string logString;

    public string CscmsImageUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/images/cscms.gif";
        }
    }

    public string ContactsUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/AnonPages/Contact.aspx";
        }
    }

    public string LogoutUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/AnonPages/Logout.aspx";
        }
    }

    public string MyPageUrl
    {
        get
        {
            return this.Page.Request.ApplicationRoot() + "/MemberPages/MyPage.aspx";
        }
    }

    public void SetFooterLink1Url(string href, string innerText)
    {
        footerLink1.HRef = href;
        footerLink1.InnerText = innerText;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        topBkImage.Attributes.Add("Style", "background-image: url('" + Page.Request.ApplicationRoot() + "/images/topbg.jpg" + "');height: 40px; padding-left: 20px;");
        SetFooterLink1Url(ContactsUrl, "Contacts");
        mainMenu.Items.FindItemByValue("MyPage").NavigateUrl = MyPageUrl;
        mainMenu.Items.FindItemByValue("Logout").NavigateUrl = LogoutUrl;

        try
        {
            if (((BasePage)this.Page).CurrentCause != null)
            {
                Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                    CommandType.StoredProcedure,
                    "LogCourtUserActivity",
                    new SqlParameter("@UserId", ((BasePage)this.Page).CurrentUser.UserID),
                    new SqlParameter("@OCACourtID", ((BasePage)this.Page).CurrentUser.OCACourtID),
                    new SqlParameter("@CauseGUID", ((BasePage)this.Page).CurrentCause.CauseGUID));
            }

            string sqlString = "INSERT INTO ActivityLog (ActivityText) VALUES (' " + Request.Url.Segments[Request.Url.Segments.Length - 1] + "  * D: " + Request.TotalBytes.ToString() + " * IP: " + Request.UserHostAddress.ToString() + " * U: " + ((BasePage)this.Page).CurrentUser.FullName + " * " + " * UI: " +
                            (Session["Impersonating"] == null ? "false" : "true") + " * ";
            if (((BasePage)this.Page).CurrentCause != null) sqlString = sqlString + "Cause Number: " + ((BasePage)this.Page).CurrentCause.CauseNumber;
            try
            {
                if (((BasePage)this.Page).CurrentPerson != null) sqlString = sqlString + " PersonGUID: " + ((BasePage)this.Page).CurrentPerson.PersonGUID.ToString();
            }
            catch { }
            logString = sqlString;
            sqlString = sqlString + " * " + " ') ";
            Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery(ConfigurationManager.ConnectionStrings["CSCMSConnectionString"].ConnectionString,
                    CommandType.Text, sqlString);

        }
        catch { }

        if (!IsPostBack)
        {
            string pageName = Request.Url.Segments[Request.Url.Segments.Length - 1].ToLower().Replace(".aspx", "");
            RadMenuItem item = mainMenu.Items.FindItemByValue(pageName, true);

            if (item != null)
            {
                item.HighlightPath();
            }

            //mainMenu.Items.FindItemByValue("Administration").Visible = ((BasePage)this.Page).CurrentUser.UserType == UserType.OCAAdmin;

            string pageName2 = this.Page.ToString().Substring(4, this.Page.ToString().Substring(4).Length - 5);

            if (pageName2.ToUpper() == "MEMBERPAGES_CHILDSUPPORTCALCULATION".ToUpper()
                || pageName2.ToUpper() == "MEMBERPAGES_CALCULATORRELATEDTABLES".ToUpper())
            {
                mainMenu.Items.FindItemByValue("MyPage").Visible = false;
                mainMenu.Items.FindItemByValue("Logout").Visible = false;
            }


            pageLoad = DateTime.Now;
        }
    }
}
