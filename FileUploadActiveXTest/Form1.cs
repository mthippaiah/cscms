﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FileUploadActiveXTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dragDropPanelControl1_DroppedByteArrayAvailable(object sender, string filename, byte[] bytes)
        {
            string filePath = @"C:\DragDrop\upload\";
            FileStream fs = File.Create(filePath + filename);
            fs.Write(bytes, 0, bytes.Length);
            fs.Flush();
            fs.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.fileUploadX2.SetDocumentMetaData("<data><UserID>24</UserID><ActionGUID>4C8084EC-E4BB-4f1b-AF01-808FCCB3856F</ActionGUID></data>");
           this.fileUploadX2.Upload();
        }
    }
}
