﻿namespace FileUploadActiveXTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.fileUploadX2 = new FileUploadActiveX.FileUploadX();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(144, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fileUploadX2
            // 
            this.fileUploadX2.AllowedFileExtensions = "wpd, doc, docx, pdf, ppt, txt, xls, xlsx, gif, jpg, png, tif";
            this.fileUploadX2.BackColor = System.Drawing.Color.Cyan;
            this.fileUploadX2.BgColor = "227, 241, 254";
            this.fileUploadX2.FileSizeLimitinMB = 10;
            this.fileUploadX2.FormFileInputCtrlId = "caseFile";
            this.fileUploadX2.Location = new System.Drawing.Point(12, 12);
            this.fileUploadX2.Name = "fileUploadX2";
            this.fileUploadX2.Size = new System.Drawing.Size(405, 25);
            this.fileUploadX2.TabIndex = 2;
            this.fileUploadX2.UploadUrl = "http://localhost/dragdroptest/document.aspx";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cyan;
            this.ClientSize = new System.Drawing.Size(418, 78);
            this.Controls.Add(this.fileUploadX2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
         private FileUploadActiveX.FileUploadX fileUploadX2;


    }
}

