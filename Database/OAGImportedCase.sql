USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OAGImportedCase_OCACourt]') AND parent_object_id = OBJECT_ID(N'[dbo].[OAGImportedCase]'))
ALTER TABLE [dbo].[OAGImportedCase] DROP CONSTRAINT [FK_OAGImportedCase_OCACourt]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGImportedCase_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGImportedCase] DROP CONSTRAINT [DF_OAGImportedCase_CreateDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGImportedCase]    Script Date: 08/25/2011 16:39:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAGImportedCase]') AND type in (N'U'))
DROP TABLE [dbo].[OAGImportedCase]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGImportedCase]    Script Date: 08/25/2011 16:39:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OAGImportedCase](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CauseNumber] [nvarchar](50) NOT NULL,
	[OCACourtID] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OAGImportedCase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OAGImportedCase]  WITH CHECK ADD  CONSTRAINT [FK_OAGImportedCase_OCACourt] FOREIGN KEY([OCACourtID])
REFERENCES [dbo].[OCACourt] ([OCACourtID])
GO

ALTER TABLE [dbo].[OAGImportedCase] CHECK CONSTRAINT [FK_OAGImportedCase_OCACourt]
GO

ALTER TABLE [dbo].[OAGImportedCase] ADD  CONSTRAINT [DF_OAGImportedCase_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO


