USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetReminderByUserID]    Script Date: 01/19/2011 14:50:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReminderByUserID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetReminderByUserID]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetReminderByUserID]    Script Date: 01/19/2011 14:50:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--exec GetReminderByUserID @OCACourtID=37,@UserID=70
-- CREATE PROCEDURE [dbo].[GetReminderByUserID]
CREATE PROCEDURE [dbo].[GetReminderByUserID]
	@OCACourtID int
	,@UserID int
	,@EffectiveDate Date
AS
BEGIN

	SET @EffectiveDate = ISNULL(@EffectiveDate,GETDATE())

	SELECT r.ReminderID 
	,r.UserId
	, r.OCACourtID 
	,r.ReminderNote 
	,r.EffectiveDate 
	,r.CauseGUID 
	,r.Inactive 
	,r.CreateDate 
	,r.LastUpdateDate 
	,r.[Version] 
	,c.CauseNumber
	,case when r.CauseGUID is null then convert(bit,0)
	else convert(bit,1) end as CourtScope
	, r.CauseGUID 
	, c.NumberOfAction 
	,  CONVERT(int, DATEDIFF(d, GETDATE(), EffectiveDate  ))  AS DaysToEffective
	FROM Reminder r LEFT JOIN Cause c ON r.CauseGUID = c.CauseGUID
	WHERE UserId = @UserID
	AND (EffectiveDate <= @EffectiveDate )
	ORDER BY EffectiveDate
End



GO


