USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OAGActionMap_ActionType]') AND parent_object_id = OBJECT_ID(N'[dbo].[OAGActionMap]'))
ALTER TABLE [dbo].[OAGActionMap] DROP CONSTRAINT [FK_OAGActionMap_ActionType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGActionMap_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGActionMap] DROP CONSTRAINT [DF_OAGActionMap_CreateDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGActionMap_LastUpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGActionMap] DROP CONSTRAINT [DF_OAGActionMap_LastUpdatedDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGActionMap]    Script Date: 08/25/2011 16:36:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAGActionMap]') AND type in (N'U'))
DROP TABLE [dbo].[OAGActionMap]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGActionMap]    Script Date: 08/25/2011 16:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OAGActionMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OAGActionCode] [nvarchar](4) NOT NULL,
	[OAGActionDescription] [varchar](150) NULL,
	[ActionTypeID] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OAGActionMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OAGActionMap]  WITH CHECK ADD  CONSTRAINT [FK_OAGActionMap_ActionType] FOREIGN KEY([ActionTypeID])
REFERENCES [dbo].[ActionType] ([ActionTypeID])
GO

ALTER TABLE [dbo].[OAGActionMap] CHECK CONSTRAINT [FK_OAGActionMap_ActionType]
GO

ALTER TABLE [dbo].[OAGActionMap] ADD  CONSTRAINT [DF_OAGActionMap_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[OAGActionMap] ADD  CONSTRAINT [DF_OAGActionMap_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdatedDate]
GO


