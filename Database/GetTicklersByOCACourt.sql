USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetTicklersByOCACourt]    Script Date: 08/26/2011 13:31:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTicklersByOCACourt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTicklersByOCACourt]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetTicklersByOCACourt]    Script Date: 08/26/2011 13:31:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES
-- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES
--
-- YOU MUST MODIFY [DismissTicklersByOCACourt] stored procedure if you modify this stored procedure.
-- 
-- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES
-- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES -- NOTES

-- CREATE PROCEDURE [dbo].[GetTicklersByOCACourt]
CREATE PROCEDURE [dbo].[GetTicklersByOCACourt]
(
	@OCACourtID smallint,
	@RequestedTickler smallint
)
AS
BEGIN

	SET NOCOUNT ON;
   
   	declare @temp table
	(
		OCACourtID smallint not null,
		TicklerTypeID tinyint not null,
		TriggerDate date not null,
		Tickler nvarchar(500) not null,
		ActionGUID uniqueidentifier not null,
		CauseNumber nvarchar(50) not null,
		CountyName nvarchar(50) not null,
		ActionNumber smallint not null,
		CauseGUID uniqueidentifier not null,
		DelayDays smallint not null
	)
	
	if @RequestedTickler = 3 
	begin
	insert into @temp
	SELECT @OCACourtID as OCACourtID
	, h.TicklerTypeID
	, DATEADD (dd,h.delaydays,a.ActionFiledDate) as TriggerDate
	,'Cause was transferred to your court from ' + fc.CourtName + ' Court by Judge ' + u.UserFullName 
	+ ' on ' +  ISNULL(CONVERT(VARCHAR(10), a.DispositionSignedDate, 101),' ')
	+ '. ' as Tickler
	, a.ActionGUID AS ActionGUID
	, c.CauseNumber 
	, cn.CountyName
	, a.ActionNumber
	, a.CauseGUID AS CauseGUID
	, h.DelayDays
	FROM [Action] a
	JOIN Cause c ON c.CauseGUID=a.CauseGUID 
	JOIN [County] cn ON c.CountyID = cn.CountyID
	JOIN OCACourt fc ON fc.OCACourtID = c.OCACourtID
	JOIN UserCourt uc ON uc.OCACourtID = fc.OCACourtID 
	JOIN [User] u ON u.UserId = uc.UserID AND u.UserTypeID = 3
	JOIN TicklerType h
	ON TicklerTypeID = 1
	WHERE c.TransferToOCACourtID IS NOT NULL AND a.ActionTypeID = 21
	AND c.TransferToOCACourtID = @OCACourtID
	AND u.UserTypeID = 3
	and c.TransferToOCACourtID <> 3
	end
	
	
	if @RequestedTickler = 1 
	begin
	insert into @temp	
	SELECT @OCACourtID as OCACourtID
	, h.TicklerTypeID
	, DATEADD(dd, h.delaydays, pt.OrderedDate) as TriggerDate
	, 'No Test Results have been entered and 45 days or more have passed since a Paternity testing was ordered on ' + CONVERT(VARCHAR(10), pt.OrderedDate, 101) + '. ' as Tickler
	, a.ActionGUID AS ActionGUID
	, c.CauseNumber 
	, cn.CountyName
	, a.ActionNumber
	, a.CauseGUID AS CauseGUID
	, h.DelayDays
	FROM [Action] a
	JOIN Cause c ON c.CauseGUID=a.CauseGUID 
	JOIN PaternityTest pt ON pt.ActionGUID = a.ActionGUID
	JOIN [County] cn
	ON c.CountyID = cn.CountyID
	JOIN TicklerType h
	ON TicklerTypeID = 2
	LEFT JOIN Hearing ah ON ah.ActionGUID = a.ActionGUID AND ah.HearingDate >= GETDATE()
	where datediff(dd, pt.OrderedDate,GETDATE()) > h.DelayDays 
	AND pt.ResultDate IS NULL
	AND c.OCACourtID = @OCACourtID AND ah.HearingDate IS NULL 
	
	end
	
	
	if @RequestedTickler = 2 
	begin
	insert into @temp
	SELECT @OCACourtID as OCACourtID
	, h.TicklerTypeID
	, DATEADD(dd, h.delaydays, pt.ResultDate) as TriggerDate
	, 'Paternity Established Date has not been entered and 5 days or more have passed since Positive Result was entered on ' + CONVERT(VARCHAR(10), pt.ResultDate, 101) + '. ' as Tickler
	, a.ActionGUID AS ActionGUID
	, c.CauseNumber 
	, cn.CountyName
	, a.ActionNumber
	, a.CauseGUID AS CauseGUID
	, h.DelayDays
	FROM [Action] a
	JOIN Cause c ON c.CauseGUID=a.CauseGUID 
	JOIN PaternityTest pt ON pt.ActionGUID = a.ActionGUID
	JOIN [County] cn
	ON c.CountyID = cn.CountyID
	JOIN TicklerType h
	ON TicklerTypeID = 3
	LEFT JOIN Hearing ah ON ah.ActionGUID = a.ActionGUID AND ah.HearingDate >= GETDATE()
	where datediff(dd, pt.ResultDate, GETDATE()) > h.DelayDays 
	AND pt.PaternityEstablishedDate IS NULL AND ISNULL(pt.TestPositive,0) = 1
	AND c.OCACourtID = @OCACourtID  AND ah.HearingDate IS NULL 
	end
	
	if  @RequestedTickler = 0 
	begin
	insert into @temp
	SELECT @OCACourtID as OCACourtID
	, h.TicklerTypeID
	, DATEADD(dd, h.delaydays, ah.HearingDate) as TriggerDate
	, 'Action has not been disposed, all necessary parties have been served, no future hearing is scheduled, and there is no outstanding Capias or Commitment Order. 45 days or more have passed since last hearing on ' + CONVERT(VARCHAR(10), ah.HearingDate, 101) + '. ' AS Tickler
	, a.ActionGUID AS ActionGUID
	, c.CauseNumber 
	, cn.CountyName
	, a.ActionNumber
	, a.CauseGUID AS CauseGUID
	, h.DelayDays
	FROM [Action] a
	join ActionType at on a.ActionTypeID = at.ActionTypeID
	JOIN Cause c ON c.CauseGUID=a.CauseGUID 
	JOIN [County] cn
	ON c.CountyID = cn.CountyID
	JOIN Hearing ah ON ah.ActionGUID = a.ActionGUID
	JOIN TicklerType h
	ON TicklerTypeID = 4
	WHERE c.OCACourtID = @OCACourtID
	AND datediff(dd, ah.HearingDate, GETDATE()) > h.DelayDays 
	AND ah.HearingDate < DATEADD(d,-1,GETDATE())
	AND ah.NextActionDate IS NULL
	and at.ActionTypeName <> 'Other (Non-expedited)'    -- 'Motion to Stay Income Withholding' is replaced by 'Other (Non-expedited)'
	and at.ActionTypeName <> 'After Trial Motion'
	and dbo.GetActionServiceDateonLastNP(a.ActionGUID) is not null
	and (
	     Not exists(select so1.ActionGUID from SpecialOrder so1 where so1.ActionGUID = a.ActionGUID and so1.OrderedDate >= ah.HearingDate)
	     or 
	     Exists(select so2.ActionGUID from SpecialOrder so2 where so2.ActionGUID = a.ActionGUID and so2.SpecialOrderTypeID in (4,6) and so2.OrderedDate >= ah.HearingDate)
	    )

	end
	
	
	select OCACourtID, TicklerTypeID, TriggerDate, Tickler, ActionGUID, CauseNumber, CountyName, ActionNumber, CauseGUID, DelayDays
	from @temp
	EXCEPT
	-- Filter out any Ticklers the user has dismissed.
	select @OCACourtID, u.TicklerTypeID, u.TriggerDate, u.Tickler 
	, u.ActionGUID 
	, c.CauseNumber 
	, i.CountyName 
	, a.ActionNumber
	, a.CauseGUID AS CauseGUID
	, h.DelayDays
	FROM [UserTicklerDismissed] u
	JOIN Action a ON u.ActionGUID=a.ActionGUID
	JOIN Cause c
	on a.CauseGUID=c.CauseGUID
	JOIN County i
	on c.CountyID = i.CountyID
	JOIN TicklerType h
	ON h.TicklerTypeID = u.TicklerTypeID
	WHERE u.OCACourtID = @OCACourtID
	order by TriggerDate desc


end




GO


