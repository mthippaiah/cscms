USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_ChildSupportJudgesDocket]    Script Date: 03/23/2011 11:31:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_ChildSupportJudgesDocket]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_ChildSupportJudgesDocket]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_ChildSupportJudgesDocket]    Script Date: 03/23/2011 11:31:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/12/2010
-- Description:	Get data For Child Support Docket
-- =============================================
CREATE PROCEDURE [dbo].[RPT_ChildSupportJudgesDocket] 
	@HearingDate DATETIME,
	@CountyName varchar(50),
	@OCACourtID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Temporary tables to filter the data
	declare @temp TABLE
	(
    		_countyid smallint
	)
    
    if @CountyName = 'All'begin
		insert into @temp 
		select CountyID from County 
    end
    else begin
		insert into @temp
		select CountyID from County where CountyName = @CountyName
    end
    
	SELECT ActionType.ActionTypeName ActionType
		,Cause.CauseNumber
		,[Action].Style
		,[Action].OAGCauseNumber
		,County.CountyName
		,[Hearing].HearingDate
		,[Hearing].HearingTime
		,Region.RegionName
		,[User].UserName
		,CONVERT(bit,1) IsHearing
		,'' FirstName
		,[User].UserFullName LastName
		,Cause.CauseGUID CaseGUID
		,[Action].ActionFiledDate FilingDate
		,[Action].ActionNumber ActionNum
		,OCACourt.CourtName CourtName
		,'' SpecialCaseRequirement
		,dbo.GetActionServiceDateonLastNP([Action].ActionGUID) AS ServiceOnLastNecessaryPartyDate
		,0 HearingbDeleted
		,0 CausebDeleted
		,null DocketNotes
		,[Action].ActionGUID
		,DATEDIFF(d, [Action].ActionFiledDate, GETDATE()) as 'Filing Age'
		,(select top 1 Person.FirstName + ' ' + Person.MiddleName + ' ' +  Person.LastName
		   from ActionPerson ap
		   inner join CustodialType ct on ct.CustodialTypeID = ap.CustodialTypeID 
		   INNER JOIN Person ON ap.PersonGUID=Person.PersonGUID
		   where ct.CustodialTypeID = 1
		   and ap.ActionGUID= [Action].ActionGUID) as NCP
	FROM Hearing Hearing 
		INNER JOIN [Action] [Action] ON [Action].ActionGUID=[Hearing].ActionGUID 
		INNER JOIN [Cause] [Cause] ON [Action].CauseGUID=[Cause].CauseGUID 
		INNER JOIN County County ON [Cause].CountyID=County.CountyID 
		INNER JOIN OCACourt OCACourt ON [Cause].OCACourtID=OCACourt.OCACourtID
		INNER JOIN UserCourt UserCourt ON UserCourt.OCACourtID=[Cause].OCACourtID
		INNER JOIN [User] [User] ON UserCourt.UserID=[User].UserID AND [User].UserTypeID=3
		INNER JOIN ActionType ActionType ON [Action].ActionTypeID=ActionType.ActionTypeID 
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
	WHERE [Hearing].HearingDate = @HearingDate
		AND Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
	ORDER BY County.CountyName

END



GO


