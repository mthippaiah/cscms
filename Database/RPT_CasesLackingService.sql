USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_CasesLackingService]    Script Date: 03/22/2011 17:05:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_CasesLackingService]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_CasesLackingService]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_CasesLackingService]    Script Date: 03/22/2011 17:05:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/18/2010
-- Description:	Case lacking service
-- =============================================
CREATE PROCEDURE [dbo].[RPT_CasesLackingService] 
	@StartDate DATETIME,
	@EndDate DATETIME,
	@CountyName varchar(50),
	@OCACourtID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Temporary tables to filter the data
	declare @temp TABLE
	(
    		_countyid smallint
	)
    
    if @CountyName = 'All'begin
		insert into @temp 
		select CountyID from County 
    end
    else begin
		insert into @temp
		select CountyID from County where CountyName = @CountyName
    end
    

	declare @temp2 TABLE
	(
		ActionType nvarchar(50),
		CauseNumber nvarchar(50),
		FilingDate date null,
		ServiceOnLastNecessaryPartyDate Date null,
		ActionNum smallint,
		CountyName nvarchar(50),
		CourtName nvarchar(75),
		RegionName nvarchar(50),
		DispositionDate date null,
		NCP nvarchar(250) null,
		OAGCauseNumber nvarchar(50) null,
		DaysSinceFiling int
	)
	   
    insert into @temp2
	SELECT ActionType.ActionTypeName ActionType
		,Cause.CauseNumber
		,a.ActionFiledDate FilingDate
		,dbo.GetActionServiceDateonLastNP(a.ActionGUID) AS ServiceOnLastNecessaryPartyDate
		,a.ActionNumber ActionNum
		,County.CountyName
		,OCACourt.CourtName
		,Region.RegionName
		,a.DispositionRenderedDate DispositionDate
		,(p.FirstName + ' ' + p.LastName) as NCP
		,a.OAGCauseNumber
		,DATEDIFF(d, a.ActionFiledDate, GETDATE()) DaysSinceFiling 
	FROM   Cause  
		inner join [Action] a on a.CauseGUID = Cause.CauseGUID
		inner join [ActionPerson] ap on ap.ActionGUID = a.ActionGUID
		inner join Person p on p.PersonGUID = ap.PersonGUID
		INNER JOIN ActionType ActionType ON a.ActionTypeID=ActionType.ActionTypeID
		INNER JOIN County County ON Cause.CountyID = County.CountyID 
		inner join OCACourt on OCACourt.OCACourtID = Cause.OCACourtID
		inner join Region on OCACourt.RegionID = Region.RegionID
	WHERE   NOT (ActionType.ActionTypeName = 'After Trial Motion' OR ActionType.ActionTypeName ='Other (Non-expedited)' OR ActionType.ActionTypeName = 'Motion to Stay Income Withholding') 
		AND ap.CustodialTypeID = 1
		and a.ActionFiledDate between @StartDate and  @EndDate
		AND Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
		and a.DispositionRenderedDate is null
 ORDER BY OCACourt.CourtName, County.CountyName, cause.CauseNumber, ServiceOnLastNecessaryPartyDate
 
		
		select * from @temp2
		where ServiceOnLastNecessaryPartyDate is null
END



GO


