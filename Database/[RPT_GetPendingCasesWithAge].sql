USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetPendingCasesWithAge]    Script Date: 01/31/2011 16:19:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetPendingCasesWithAge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetPendingCasesWithAge]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetPendingCasesWithAge]    Script Date: 01/31/2011 16:19:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/14/2010
-- Description:	Get Pending Cases Which are so many days old
-- =============================================
CREATE PROCEDURE [dbo].[RPT_GetPendingCasesWithAge]
	@CountyName varchar(50),
	@OCACourtID INT,
	@AgeInDays INT = null
AS
BEGIN
	SET NOCOUNT ON;

    --Temporary tables to filter the data
	declare @temp TABLE
	(
    		_countyid smallint
	)
    
    if @CountyName = 'All'begin
		insert into @temp 
		select CountyID from County 
    end
    else begin
		insert into @temp
		select CountyID from County where CountyName = @CountyName
    end
 
 
    declare @temp2 TABLE
	(
		CauseNumber nvarchar(50),
		ActionNumber smallint,
		ActionGuid uniqueidentifier,
		ActionServiceDateOnLastNP Date null
	)
	
	insert into @temp2
	SELECT distinct Cause.CauseNumber, a.ActionNumber, a.ActionGUID
		,dbo.GetActionServiceDateonLastNP(a.ActionGUID)
	FROM   Cause 
		join [Action] a on Cause.CauseGUID = a.CauseGUID
		INNER JOIN County County ON Cause.CountyID=County.CountyID 
		INNER JOIN OCACourtCounty OCACourtCounty ON County.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
	where ActionStatus = 'A'
		and Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
 
 
    SELECT distinct
		a.ActionGUID
		,Cause.CauseNumber
		,County.CountyName
		,OCACourt.CourtName
		,Region.RegionName
		,T2.ActionServiceDateOnLastNP AS ServiceOnLastNecessaryPartyDate
		,A.DispositionRenderedDate, A.ActionNumber
		,A.OAGCauseNumber
		,Cause.CauseGUID
		,a.ActionFiledDate as FileDate
		,datediff(d,T2.ActionServiceDateOnLastNP, getdate()) as AgeInDays
		,(select top 1 p.LastName + ',' + p.FirstName FROM [ActionPerson] ap
			inner join Person p on p.PersonGUID = ap.PersonGUID
			where ap.ActionGUID = a.ActionGUID
			AND ap.CustodialTypeID=1) as NCP  
	FROM   Cause 
		join [Action] a on Cause.CauseGUID = a.CauseGUID
		INNER JOIN County County ON Cause.CountyID=County.CountyID 
		INNER JOIN OCACourtCounty OCACourtCounty ON County.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
		inner join @temp2 T2 on T2.ActionGUID = a.ActionGUID
	where A.DispositionRenderedDate is null
		and	T2.ActionServiceDateOnLastNP is not null 
	    and (@AgeInDays is null or datediff(d,T2.ActionServiceDateOnLastNP, getdate()) >= @AgeInDays)
	    and ActionStatus = 'A'
		and Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
		
	ORDER BY County.CountyName, Cause.CauseNumber
    
END



GO


