USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_ChildSupportDocket]    Script Date: 03/23/2011 11:30:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_ChildSupportDocket]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_ChildSupportDocket]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_ChildSupportDocket]    Script Date: 03/23/2011 11:30:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/12/2010
-- Description:	Get data For Child Support Docket
-- =============================================
CREATE PROCEDURE [dbo].[RPT_ChildSupportDocket] 
	@HearingDate DATETIME,
	@CountyName varchar(50),
	@OCACourtID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	--Temporary tables to filter the data
	declare @temp TABLE
	(
    		_countyid smallint
	)
    
    if @CountyName = 'All'begin
		insert into @temp 
		select CountyID from County 
    end
    else begin
		insert into @temp
		select CountyID from County where CountyName = @CountyName
    end
    
	declare @PartyData TABLE 
	(
		ActionGUID uniqueidentifier,
		partyname varchar(250) null,
		servicedate date null,
		attyname varchar(250) null,
		partytype int null
	)
    
    insert into @PartyData
    select
        [Action].ActionGUID
		,Person.FirstName + ' ' + Person.MiddleName + ' ' +  Person.LastName
		,s.ServiceDate ServiceDate
		,rp.FirstName + ' ' + rp.LastName AttyName
		,ct.CustodialTypeID
	FROM Hearing Hearing 
		INNER JOIN [Action] [Action] ON [Action].ActionGUID=[Hearing].ActionGUID 
		INNER JOIN [Cause] [Cause] ON [Action].CauseGUID=[Cause].CauseGUID 
		INNER JOIN County County ON [Cause].CountyID=County.CountyID 
		INNER JOIN OCACourt OCACourt ON [Cause].OCACourtID=OCACourt.OCACourtID
		INNER JOIN UserCourt UserCourt ON UserCourt.OCACourtID=[Cause].OCACourtID
	 	INNER JOIN ActionPerson ON ActionPerson.ActionGUID=[Action].ActionGUID
		inner join CustodialType ct on ct.CustodialTypeID = ActionPerson.CustodialTypeID 
		INNER JOIN Person ON ActionPerson.PersonGUID=Person.PersonGUID
		JOIN PersonType pt ON pt.PersonTypeID=ActionPerson.PersonTypeID
		LEFT JOIN [Service] s ON s.ActionGUID=[Action].ActionGUID AND s.PersonGUID = Person.PersonGUID 
		LEFT JOIN ActionPersonRelatedPerson ON (ActionPersonRelatedPerson.PersonGUID = Person.PersonGUID AND ActionPersonRelatedPerson.PersonRelationshipTypeID=7 )
		LEFT JOIN Person rp ON rp.PersonGUID = ActionPersonRelatedPerson.RelatedPersonGUID
	WHERE ActionPerson.NPFlag = 1
		and ct.CustodialTypeID in (0, 1)
		and [Hearing].HearingDate = @HearingDate
		AND Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
		
	SELECT ActionType.ActionTypeName ActionType
		,Cause.CauseNumber
		,[Action].Style
		,[Action].OAGCauseNumber
		,County.CountyName
		,[Hearing].HearingDate
		,[Hearing].HearingTime
		,Region.RegionName
		,[User].UserName
		,[User].UserFullName LastName
		,Cause.CauseGUID CaseGUID
		,OCACourt.CourtName CourtName
		,[Action].ActionGUID
		,(select top 1 partyname from @PartyData pd where pd.ActionGUID = [Action].ActionGUID and pd.partytype = 0) as CustodialParentName
		,(select top 1 partyname from @PartyData pd where pd.ActionGUID = [Action].ActionGUID and pd.partytype = 1) as NonCustodialParentName
		,(select top 1 servicedate from @PartyData pd where pd.ActionGUID = [Action].ActionGUID and pd.partytype = 0) as CustodialServiceDate
		,(select top 1 servicedate from @PartyData pd where pd.ActionGUID = [Action].ActionGUID and pd.partytype = 1) as NonCustodialServiceDate
		,(select top 1 attyname from @PartyData pd where pd.ActionGUID = [Action].ActionGUID and pd.partytype = 0) as CustodialAttorneyName
		,(select top 1 attyname from @PartyData pd where pd.ActionGUID = [Action].ActionGUID and pd.partytype = 1) as NonCustodialAttorneyName
		,(SELECT top 1 note.NoteText   FROM [CauseNote] cn
				inner join note on note.NoteID = cn.NoteID
				inner join NoteType nt on nt.NoteTypeID = cn.NoteTypeID
				where cn.CauseGUID = [Cause].CauseGUID) as CauseNote
	FROM Hearing Hearing 
		INNER JOIN [Action] [Action] ON [Action].ActionGUID=[Hearing].ActionGUID 
		INNER JOIN [Cause] [Cause] ON [Action].CauseGUID=[Cause].CauseGUID 
		INNER JOIN County County ON [Cause].CountyID=County.CountyID 
		INNER JOIN OCACourt OCACourt ON [Cause].OCACourtID=OCACourt.OCACourtID
		INNER JOIN UserCourt UserCourt ON UserCourt.OCACourtID=[Cause].OCACourtID
		INNER JOIN [User] [User] ON UserCourt.UserID=[User].UserID AND [User].UserTypeID=3
		INNER JOIN ActionType ActionType ON [Action].ActionTypeID=ActionType.ActionTypeID 
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
	WHERE [Hearing].HearingDate = @HearingDate
		AND Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
	ORDER BY County.CountyName


END




GO


