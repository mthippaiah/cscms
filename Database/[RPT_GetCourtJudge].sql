USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetCourtJudge]    Script Date: 01/31/2011 10:51:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetCourtJudge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetCourtJudge]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetCourtJudge]    Script Date: 01/31/2011 10:51:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/11/2010
-- Description:	Get Court Judge
-- =============================================
CREATE PROCEDURE [dbo].[RPT_GetCourtJudge] 
	@OCACourtID SmallINT
	
AS
BEGIN
	SET NOCOUNT ON;

	select u.UserFullName from [User] u 
	inner join UserType ut on ut.UserTypeID = u.UserTypeID
	inner join [UserCourt] uc on uc.UserID = u.UserId
	where ut.UserTypeID = 3
	and uc.OCACourtID = @OCACourtID
 
END


GO


