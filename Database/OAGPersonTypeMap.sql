USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OAGPersonTypeMap_PersonType]') AND parent_object_id = OBJECT_ID(N'[dbo].[OAGPersonTypeMap]'))
ALTER TABLE [dbo].[OAGPersonTypeMap] DROP CONSTRAINT [FK_OAGPersonTypeMap_PersonType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGFatherTypeMap_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGPersonTypeMap] DROP CONSTRAINT [DF_OAGFatherTypeMap_CreateDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGFatherTypeMap_LastUpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGPersonTypeMap] DROP CONSTRAINT [DF_OAGFatherTypeMap_LastUpdatedDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGPersonTypeMap]    Script Date: 08/25/2011 16:39:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAGPersonTypeMap]') AND type in (N'U'))
DROP TABLE [dbo].[OAGPersonTypeMap]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGPersonTypeMap]    Script Date: 08/25/2011 16:39:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OAGPersonTypeMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](2) NOT NULL,
	[PersonTypeID] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OAGPersonTypeMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OAGPersonTypeMap]  WITH CHECK ADD  CONSTRAINT [FK_OAGPersonTypeMap_PersonType] FOREIGN KEY([PersonTypeID])
REFERENCES [dbo].[PersonType] ([PersonTypeID])
GO

ALTER TABLE [dbo].[OAGPersonTypeMap] CHECK CONSTRAINT [FK_OAGPersonTypeMap_PersonType]
GO

ALTER TABLE [dbo].[OAGPersonTypeMap] ADD  CONSTRAINT [DF_OAGFatherTypeMap_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[OAGPersonTypeMap] ADD  CONSTRAINT [DF_OAGFatherTypeMap_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdatedDate]
GO


