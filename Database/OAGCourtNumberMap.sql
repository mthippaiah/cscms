USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OAGCourtNumberMap_OCACourt]') AND parent_object_id = OBJECT_ID(N'[dbo].[OAGCourtNumberMap]'))
ALTER TABLE [dbo].[OAGCourtNumberMap] DROP CONSTRAINT [FK_OAGCourtNumberMap_OCACourt]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGCourtNumberMap_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGCourtNumberMap] DROP CONSTRAINT [DF_OAGCourtNumberMap_CreateDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGCourtNumberMap_LastUpdateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGCourtNumberMap] DROP CONSTRAINT [DF_OAGCourtNumberMap_LastUpdateDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGCourtNumberMap]    Script Date: 08/25/2011 16:37:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAGCourtNumberMap]') AND type in (N'U'))
DROP TABLE [dbo].[OAGCourtNumberMap]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGCourtNumberMap]    Script Date: 08/25/2011 16:37:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OAGCourtNumberMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OuNumber] [nvarchar](5) NOT NULL,
	[OCACourtID] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OAGCourtNumberMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_OAGCourtNumberMap] UNIQUE NONCLUSTERED 
(
	[OuNumber] ASC,
	[OCACourtID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OAGCourtNumberMap]  WITH CHECK ADD  CONSTRAINT [FK_OAGCourtNumberMap_OCACourt] FOREIGN KEY([OCACourtID])
REFERENCES [dbo].[OCACourt] ([OCACourtID])
GO

ALTER TABLE [dbo].[OAGCourtNumberMap] CHECK CONSTRAINT [FK_OAGCourtNumberMap_OCACourt]
GO

ALTER TABLE [dbo].[OAGCourtNumberMap] ADD  CONSTRAINT [DF_OAGCourtNumberMap_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[OAGCourtNumberMap] ADD  CONSTRAINT [DF_OAGCourtNumberMap_LastUpdateDate]  DEFAULT (getdate()) FOR [LastUpdateDate]
GO


