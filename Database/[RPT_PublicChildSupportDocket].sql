USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_PublicChildSupportDocket]    Script Date: 03/23/2011 11:46:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_PublicChildSupportDocket]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_PublicChildSupportDocket]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_PublicChildSupportDocket]    Script Date: 03/23/2011 11:46:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: Oct 11, 2010
-- Description:	Child Support Docket (Public) Report
-- =============================================
CREATE PROCEDURE [dbo].[RPT_PublicChildSupportDocket]
	@HearingDate DATETIME,
	@CountyName varchar(50),
	@OCACourtID INT
AS
BEGIN
	SET NOCOUNT ON;
    
    --Temporary tables to filter the data
	declare @temp TABLE
	(
    		_countyid smallint
	)
    
    if @CountyName = 'All'begin
		insert into @temp 
		select CountyID from County 
    end
    else begin
		insert into @temp
		select CountyID from County where CountyName = @CountyName
    end
        
	SELECT [Hearing].HearingDate EventDate
		,[Hearing].HearingTime EventTime
		,CONVERT(bit,1) IsHearing
		,[Cause].CauseNumber
		,[Action].OAGCauseNumber
		,County.CountyName
		,'' FirstName
		,[User].UserFullName LastName
		,[User].UserName
		,ActionType.ActionTypeName ActionType
		,Region.RegionName
		,OCACourt.CourtName
		,[Action].ActionNumber ActionNum
		,[Action].Style Style
		,[Action].ActionFiledDate FilingDate
		,'' SpecialCaseRequirement
		,[Action].CauseGUID CaseGUID
		,[Action].ActionGUID
		,0 HearingbDeleted
		,0 CausebDeleted
		,[Cause].OCACourtID 
		,DATEDIFF(d, [Action].ActionFiledDate, GETDATE()) FilingAge
		,(select top 1 Person.FirstName + ' ' + Person.MiddleName + ' ' +  Person.LastName
		   from ActionPerson ap
		   inner join CustodialType ct on ct.CustodialTypeID = ap.CustodialTypeID 
		   INNER JOIN Person ON ap.PersonGUID=Person.PersonGUID
		   where ct.CustodialTypeID = 1
		   and ap.ActionGUID= [Action].ActionGUID) as NCP
	FROM  [Hearing] [Hearing] 
	INNER JOIN [Action] [Action] ON [Action].ActionGUID=[Hearing].ActionGUID 
	INNER JOIN [Cause] [Cause] ON [Action].CauseGUID=[Cause].CauseGUID 
	INNER JOIN County County ON [Cause].CountyID=County.CountyID 
	INNER JOIN OCACourt OCACourt ON [Cause].OCACourtID=OCACourt.OCACourtID
	INNER JOIN UserCourt UserCourt ON UserCourt.OCACourtID=[Cause].OCACourtID
	INNER JOIN [User] [User] ON UserCourt.UserID=[User].UserID AND [User].UserTypeID=3
	INNER JOIN ActionType ActionType ON [Action].ActionTypeID=ActionType.ActionTypeID 
	INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
	WHERE 
		[Hearing].HearingDate = @HearingDate
		AND [Cause].CountyID in (select _countyid from @temp)
		AND [Cause].OCACourtID = @OCACourtID
	ORDER BY [User].UserName, [Cause].CauseNumber

END


GO


