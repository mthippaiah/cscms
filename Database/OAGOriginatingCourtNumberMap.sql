USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OAGOriginatingCourtNumberMap_OriginatingCourt]') AND parent_object_id = OBJECT_ID(N'[dbo].[OAGOriginatingCourtNumberMap]'))
ALTER TABLE [dbo].[OAGOriginatingCourtNumberMap] DROP CONSTRAINT [FK_OAGOriginatingCourtNumberMap_OriginatingCourt]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGOriginatingCourtNumberMap_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGOriginatingCourtNumberMap] DROP CONSTRAINT [DF_OAGOriginatingCourtNumberMap_CreateDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGOriginatingCourtNumberMap_LastUpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGOriginatingCourtNumberMap] DROP CONSTRAINT [DF_OAGOriginatingCourtNumberMap_LastUpdatedDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGOriginatingCourtNumberMap]    Script Date: 08/25/2011 16:40:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAGOriginatingCourtNumberMap]') AND type in (N'U'))
DROP TABLE [dbo].[OAGOriginatingCourtNumberMap]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGOriginatingCourtNumberMap]    Script Date: 08/25/2011 16:40:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OAGOriginatingCourtNumberMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OriginatingCourtNumber] [nvarchar](3) NOT NULL,
	[OriginatingCourtID] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OAGOriginatingCourtNumberMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OAGOriginatingCourtNumberMap]  WITH CHECK ADD  CONSTRAINT [FK_OAGOriginatingCourtNumberMap_OriginatingCourt] FOREIGN KEY([OriginatingCourtID])
REFERENCES [dbo].[OriginatingCourt] ([OriginatingCourtID])
GO

ALTER TABLE [dbo].[OAGOriginatingCourtNumberMap] CHECK CONSTRAINT [FK_OAGOriginatingCourtNumberMap_OriginatingCourt]
GO

ALTER TABLE [dbo].[OAGOriginatingCourtNumberMap] ADD  CONSTRAINT [DF_OAGOriginatingCourtNumberMap_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[OAGOriginatingCourtNumberMap] ADD  CONSTRAINT [DF_OAGOriginatingCourtNumberMap_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdatedDate]
GO


