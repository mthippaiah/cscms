USE [CSCMS]
GO
/****** Object:  StoredProcedure [dbo].[GetCauseSummaryByOCACourt]    Script Date: 12/07/2011 09:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- CREATE PROCEDURE [dbo].[GetCauseSummaryByOCACourt]
ALTER PROCEDURE [dbo].[GetCauseSummaryByOCACourt](@OCACourtID smallint)
AS
BEGIN
	--[GetCauseSummaryByOCACourt] 2
	
	--SELECT 
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND ISNULL(ActionStatus,'A') = 'A' AND ISNULL(ActionDisposed,'P') = 'D'   ) ,0 ) NumberOfCaseAD,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND ISNULL(ActionStatus,'A') = 'A' AND ISNULL(ActionDisposed,'P') = 'P'   ) ,0 ) NumberOfCaseAP,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND  ISNULL(ActionStatus,'A') = 'I'   ) ,0 ) NumberOfCaseI,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0)  ) ,0 ) NumberOfCaseA

 --   SELECT 'Active/Disposed Cases' 'CaseTypeName', 1 CaseTypeID, 1 Disposed,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND DispositionRenderedDate is not null and ((ActionStatus is not null and ActionStatus = 'A') or a.Inactive = 1)) ,0 ) NumberOfCase
	--UNION
	--SELECT 'Active/Pending Cases' 'CaseTypeName', 1 CaseTypeID, 2 Disposed ,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND DispositionRenderedDate is null and  ISNULL(ActionStatus,'A') = 'A') ,0 ) NumberOfCase
 --	UNION
	--SELECT 'Inactive Cases' 'CaseTypeName', 2 CaseTypeID, 0 Disposed,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND  DispositionRenderedDate is not null and ((ActionStatus is null or ActionStatus = 'I') or a.Inactive = 0)) ,0 ) NumberOfCase
	--UNION 
	--SELECT 'All Cases' 'CaseTypeName', 0 CaseTypeID, 0 Disposed,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0)  ) ,0 ) NumberOfCase
		
SELECT 'Active/Disposed Cases' 'CaseTypeName', 1 CaseTypeID, 1 Disposed,
ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) 
AND a.DispositionRenderedDate is not null and a.Inactive = 1) ,0 ) NumberOfCase

UNION

SELECT 'Active/Pending Cases' 'CaseTypeName', 1 CaseTypeID, 2 Disposed ,
ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) 
AND DispositionRenderedDate is null),0 ) NumberOfCase

UNION

SELECT 'Inactive Cases' 'CaseTypeName', 2 CaseTypeID, 0 Disposed,
ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) 
AND a.DispositionRenderedDate is not null and (a.Inactive=0 or a.Inactive is null)) ,0 ) NumberOfCase

UNION 

SELECT 'All Cases' 'CaseTypeName', 0 CaseTypeID, 0 Disposed,
ISNULL( (SELECT COUNT(a.ActionGUID) FROM Action a, Cause b 
WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0)
),0 ) NumberOfCase
	
END


