USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetCauseSummaryByOCACourt]    Script Date: 01/14/2011 16:43:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCauseSummaryByOCACourt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCauseSummaryByOCACourt]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetCauseSummaryByOCACourt]    Script Date: 01/14/2011 16:43:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- CREATE PROCEDURE [dbo].[GetCauseSummaryByOCACourt]
CREATE PROCEDURE [dbo].[GetCauseSummaryByOCACourt](@OCACourtID smallint)
AS
BEGIN
	--[GetCauseSummaryByOCACourt] 2
	
	--SELECT 
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND ISNULL(ActionStatus,'A') = 'A' AND ISNULL(ActionDisposed,'P') = 'D'   ) ,0 ) NumberOfCaseAD,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND ISNULL(ActionStatus,'A') = 'A' AND ISNULL(ActionDisposed,'P') = 'P'   ) ,0 ) NumberOfCaseAP,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND  ISNULL(ActionStatus,'A') = 'I'   ) ,0 ) NumberOfCaseI,
	--ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0)  ) ,0 ) NumberOfCaseA
	
	
	SELECT 'Active/Disposed Cases' 'CaseTypeName', 1 CaseTypeID, 1 Disposed,
	ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND ISNULL(ActionStatus,'A') = 'A' AND ISNULL(ActionDisposed,'P') = 'D'   ) ,0 ) NumberOfCase
	UNION
	SELECT 'Active/Pending Cases' 'CaseTypeName', 1 CaseTypeID, 2 Disposed ,
	ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND ISNULL(ActionStatus,'A') = 'A' AND ISNULL(ActionDisposed,'P') = 'P'   ) ,0 ) NumberOfCase
	UNION
	  
	SELECT 'InActive Cases' 'CaseTypeName', 2 CaseTypeID, 0 Disposed,
	ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0) AND  ISNULL(ActionStatus,'A') = 'I'   ) ,0 ) NumberOfCase
	UNION 
	SELECT 'All Cases' 'CaseTypeName', 0 CaseTypeID, 0 Disposed,
	ISNULL( (SELECT COUNT(*) FROM Action a, Cause b WHERE a.CauseGUID=b.CauseGUID AND (OCACourtID=@OCACourtID OR @OCACourtID=0)  ) ,0 ) NumberOfCase
		
	
END


GO


