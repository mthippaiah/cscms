USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[OAG_GetNewCasesForCourt]    Script Date: 08/26/2011 05:06:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAG_GetNewCasesForCourt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[OAG_GetNewCasesForCourt]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[OAG_GetNewCasesForCourt]    Script Date: 08/26/2011 05:06:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[OAG_GetNewCasesForCourt] 
	@OCACourtId int
AS
BEGIN
	
	SET NOCOUNT ON;
	
	declare @temp table
	(
		causeNumber varchar(15) null
	)
	
	 insert into @temp
	 SELECT a.CauseNumber from OAGDATA a
	 where a.OuNumber in (select OuNumber from OAGCourtNumberMap t1 where t1.OCACourtID = @OCACourtId)
   	 except
	 select c.CauseNumber from Cause c
	 where c.OCACourtID = @OCACourtId  
    
    
    SELECT distinct a.OuNumber, a.CauseNumber from OAGDATA a
    where a.OuNumber in (select OuNumber from OAGCourtNumberMap t1 where t1.OCACourtID = @OCACourtId)
    and a.CauseNumber in (select causeNumber from @temp)
END




GO


