USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetPastHearings]    Script Date: 01/31/2011 10:49:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetPastHearings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetPastHearings]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetPastHearings]    Script Date: 01/31/2011 10:49:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ravi nanjundappa
-- Create date: 10/13/2010
-- Description:	Get Past Hearings
-- =============================================
CREATE PROCEDURE [dbo].[RPT_GetPastHearings]
@ActionGUID uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[Hearing].HearingDate
		,[Hearing].ActionGUID
		,[Hearing].HearingNote Comments
	FROM  [Hearing] Hearing
	WHERE [Hearing].ActionGUID = @ActionGUID
	ORDER BY [Hearing].HearingDate

END

GO


