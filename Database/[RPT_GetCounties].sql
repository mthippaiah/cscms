USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetCounties]    Script Date: 01/31/2011 10:52:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetCounties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetCounties]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetCounties]    Script Date: 01/31/2011 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[RPT_GetCounties]
	@OCACourtID smallint
AS
BEGIN
	select 0, 'All' as CountyName
	union
	SELECT distinct cn.CountyID , cn.CountyName 
	FROM County cn
	JOIN OCACourtCounty cc ON cc.CountyID = cn.CountyID 
	JOIN UserCourt uc ON uc.OCACourtID = cc.OCACourtID
	WHERE uc.OCACourtID = @OCACourtID
	ORDER BY CountyName 
END



GO


