USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionAging]    Script Date: 01/28/2011 12:43:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetActionAging]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetActionAging]
GO

USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionAging]    Script Date: 01/28/2011 12:43:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- HN 06/2009
--6.3.	Action Aging
--6.3.1.	The Action Age field is displayed at the Action level and is a protected (non-editable) field displaying a calculated value. 
--The Action Age calculation starts the day after the Last Necessary Party Service Date (LNPSD). 
--[The Last Necessary Party Service Date field is displayed at the Action level, 
--and is a protected (non-editable) field displaying a derived value of the latest date value entered into the Service Date 
--field for any of that action�s Necessary Party person records. ]
--6.3.2.	The default Action Age calculation is the difference between the  date of the day after the Last Necessary 
--Party Service Date and the current day�s date.
--6.3.3.	Triggers to stop the Action Age accrual once it is started are as follows:
--o	Any disposition reason with exception of �Referred to District Judge�
--o	A �Capias Issue Date� entered on the person record of a Necessary Party.
--6.3.4.	Triggers that engage Action Age accrual to begin again once stopped are as follows:
--o	A Compliance Hearing date that is scheduled after Action Age accrual stopped due to a �Capias Issue Date� entered on the person record of a Necessary Party. The number of days from the date accrual stopped through the date accrual started again are not included in the Action Age. The calculation on re-start takes the number of days from the previous accrual timeframe(s) and adds those to the next consecutive accrual timeframe.
 
-- =============================================
-- CREATE FUNCTION [dbo].[GetActionAging]
CREATE FUNCTION [dbo].[GetActionAging]
	(@ActionGUID uniqueidentifier) RETURNS int
AS
BEGIN
	-- SELECT dbo.[GetActionAging] ('108B42CE-5B6C-403D-A046-12CD8740BF17')
	
	DECLARE @Aging int
	DECLARE @LastNPServiceDate DATE
	SET @Aging = null
	
	DECLARE @PrimaryStatus VARCHAR(20)
	DECLARE @DispositionRenderedDate DATE
	
	DECLARE @DispositionTypeID INT 
	
	DECLARE @StopDate DATE
	SET @StopDate = GETDATE()
	
	-- select * from  DispositionType
	-- 19	Referred to District Judge
	-- 7	Capias - FTA
	-- 21	Capias � Compliance

	-- IF THERE IS NP, return null		
	IF NOT EXISTS(SELECT ActionGUID FROM ActionPerson WHERE ActionGUID=@ActionGUID AND NPFlag = 1) 
	BEGIN
		SET @Aging = null -- RETURN VALUES NULL
	END
	ELSE
	BEGIN
		
		-- IF THERE IS NP with no service, return null
		IF EXISTS (SELECT pa.PersonGUID FROM ActionPerson pa 
			LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID
			AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
			WHERE s.PersonGUID IS null AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1) 
		BEGIN
			SET @Aging = null -- RETURN VALUES NULL
		END
		ELSE
		BEGIN
		
			-- Get latest NP service date
			SELECT @LastNPServiceDate = MAX(ISNULL(ServiceDate,'1/1/1970')) 
			FROM ActionPerson pa 
			LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID
			AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
			WHERE pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
	
			IF EXISTS(SELECT * FROM [Action] WHERE ActionGUID = @ActionGUID AND DispositionSignedDate IS NOT NULL)
				SELECT @StopDate = DATEADD(d,1,DispositionSignedDate)  FROM [Action] WHERE ActionGUID = @ActionGUID 

			SET @Aging= DATEDIFF(d,@LastNPServiceDate,@StopDate) - 1

		END

	END
	
	RETURN @Aging

END





GO


