USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetIncarceratedParties]    Script Date: 07/01/2011 10:19:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetIncarceratedParties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetIncarceratedParties]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetIncarceratedParties]    Script Date: 07/01/2011 10:19:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 6/30/2011
-- Description:	Incarcerated parties
-- =============================================

CREATE PROCEDURE [dbo].[RPT_GetIncarceratedParties]
	@CountyName varchar(50)
	,@OCACourtID INT
	,@BeginIssuedDate DATETIME = NULL
	,@EndIssuedDate DATETIME = NULL
	,@PrimarySort varchar(50) = NULL
	,@SecondarySort varchar(50)= NULL
as
begin

	--Temporary table to filter the data
	declare @temp table (_countyid smallint)
	declare @strSQL varchar(5000)
	create table #output (
		CauseNumber nvarchar(50) null
		,CountyName nvarchar(50) null
		,Style nvarchar(50) null
		,FirstName nvarchar(50) null
		,LastName nvarchar(50) null
		,OrderedDate date null
		,IssuedDate date null
		,ArrestDate date null
		,SpecialOrderTypeName nvarchar(75) null
		,BondAmount money null
	)

	if @CountyName = 'All'
	begin
		insert into @temp (_countyid)
			select County.CountyID from County 
			INNER JOIN OCACourtCounty ON County.CountyID=OCACourtCounty.CountyID
			INNER JOIN OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
			where OCACourt.OCACourtID = @OCACourtID
	end
	else
	begin
		insert into @temp (_countyid)
			select CountyID from County where CountyName = @CountyName
	end

	insert into #output (CauseNumber, CountyName, Style, FirstName, LastName, OrderedDate, IssuedDate, ArrestDate, SpecialOrderTypeName, BondAmount)
		select distinct cs.CauseNumber, cy.CountyName, a.Style, p.FirstName, p.LastName, so.OrderedDate, so.IssuedDate, so.ArrestCommitmentEffectiveDate, sot.SpecialOrderTypeName, so.BondAmount
		from [Action] a
		inner join [Cause] cs on cs.CauseGUID = a.CauseGUID
		inner join [ActionPerson] ap on ap.ActionGUID = a.ActionGUID
		inner join Person p on p.PersonGUID = ap.PersonGUID
		inner join OCACourt ct on ct.OCACourtID = cs.OCACourtID
		inner join County cy on cy.CountyID = cs.CountyID
		left join SpecialOrder so on so.ActionGUID = ap.ActionGUID and so.PersonGUID = ap.PersonGUID
		left join SpecialOrderType sot on sot.SpecialOrderTypeID = so.SpecialOrderTypeID
		where ct.OCACourtID = @OCACourtID
			and cs.OCACourtID = @OCACourtID
			AND cs.CountyID in (select _countyid from @temp)
			and (ap.InJail = 1 or (so.ArrestCommitmentEffectiveDate is not null and so.ReleaseDate is null))
			and a.DispositionRenderedDate is null
			and (@BeginIssuedDate is null or so.IssuedDate is null or so.IssuedDate >= @BeginIssuedDate)
			and (@EndIssuedDate is null or so.IssuedDate is null or so.IssuedDate <= @EndIssuedDate)
		order by cy.CountyName

		set @strSQL='select * from #output order by CountyName'
		if isnull(@PrimarySort, '')<>''
			set @strSQL = @strSQL + ', '+@PrimarySort

		if isnull(@SecondarySort, '')<>'' and isnull(@PrimarySort, '')<>isnull(@SecondarySort, '')
			set @strSQL = @strSQL + ', '+@SecondarySort
		
		execute(@strSQL)
		drop table #output
end

GO


