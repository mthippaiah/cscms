USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionPrimaryStatus]    Script Date: 01/31/2011 14:49:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetActionPrimaryStatus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetActionPrimaryStatus]
GO

USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionPrimaryStatus]    Script Date: 01/31/2011 14:49:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- HN 06/2009
-- 
--7.1.	Active/Inactive: SDCMS referenced cases as being Open or Closed. 
--CSCMS doesn�t use the open or closed status. Instead, 
--CSCMS uses the primary status of Disposed or Pending and 
--secondary status of Active or Inactive for each Action. 
--An Action without a Disposition Type and Disposition Date will have a status of Pending and Active. 
--However, an Action having a Disposition Type and Disposition Date may have a status of either Active or Inactive. 
--When Disposition Type and Disposition Date (saving data for either of these two fields requires data entered in both fields) data is entered, 
--the system defaults the Active/Inactive flag to Inactive. 
--However, the user can edit the Active/Inactive field to toggle between saving the record as Active and 
--Inactive an unlimited amount of times, with the only validation being that a Disposition Type 
--and Disposition Date must be populated in order to save the record as Inactive. 
-- =============================================

CREATE FUNCTION [dbo].[GetActionPrimaryStatus](@ActionGUID uniqueidentifier) RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @PrimaryStatus VARCHAR(20)
	DECLARE @DispositionTypeID INT 
	
	SET @PrimaryStatus='Pending'
		
	if EXISTS(SELECT DispositionTypeID FROM Action WHERE ActionGUID=@ActionGUID and DispositionTypeID is not null)
	begin
		   		
		   if not exists(select DispositionTypeID FROM Action WHERE ActionGUID=@ActionGUID and DispositionTypeID in
				 (select DispositionTypeID from DispositionType where DispositionTypeName in (select SpecialOrderTypeName from SpecialOrderType)))
			  SET @PrimaryStatus='Disposed'
	end

	RETURN @PrimaryStatus

END
GO