USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GeneralCourtStatisticsHearing]    Script Date: 01/31/2011 16:17:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GeneralCourtStatisticsHearing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GeneralCourtStatisticsHearing]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GeneralCourtStatisticsHearing]    Script Date: 01/31/2011 16:17:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 11/3/2010
-- Description:	General Court Statistics
-- =============================================
CREATE PROCEDURE [dbo].[RPT_GeneralCourtStatisticsHearing] 
	@OCACourtID INT,
    @startDate DATETIME = null,
    @endDate DateTime = null

AS
BEGIN
	SET NOCOUNT ON;

	SELECT CountyName, a.ActionFiledDate, ht.HearingTypeName
	FROM  Cause 
		inner join [Action] a on a.CauseGUID = Cause.CauseGUID
		INNER JOIN County c ON c.CountyID= Cause.CountyID
		INNER JOIN OCACourtCounty OCACourtCounty ON c.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		inner join Hearing h on h.ActionGUID = a.ActionGUID
		inner join HearingType ht on ht.HearingTypeID = h.HearingTypeID
	where (@startDate is null or a.ActionFiledDate >= @startDate)
		and (@endDate is null or a.ActionFiledDate <= @endDate)
		and Cause.CountyID in (select CountyID from County)
		and OCACourt.OCACourtID = @OCACourtID
		and Cause.CauseNumber NOT LIKE 'TEST%'
	ORDER BY c.CountyName, ht.HearingTypeName

END





GO


