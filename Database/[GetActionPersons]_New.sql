USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetActionPersons]    Script Date: 03/14/2011 09:53:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetActionPersons]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetActionPersons]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetActionPersons]    Script Date: 03/14/2011 09:53:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- CREATE PROCEDURE [dbo].[GetActionPersons]
CREATE PROCEDURE [dbo].[GetActionPersons]
(
	@ActionGUID uniqueidentifier
)
AS
BEGIN
--or pcrp.RelatedPersonGUID = ap.PersonGUID
WITH Persons AS
(
	SELECT
		ap.NPFlag
		,p.PersonGUID
		,p.FirstName
		,p.MiddleName
		,p.LastName
		,p.NameSuffix
		,p.Birthdate
		,p.Sex
	    ,PersonTypeName
	    ,ct.CustodialTypeName
	    ,ft.FatherTypeName
		,ap.NeedAttorneyAppointed
		, (SELECT CAST(pr.PersonRelationshipTypeName  + ' ' + ISNULL(p1.FirstName, '') + ' ' + ISNULL(p1.LastName, '') + '|' AS VARCHAR(MAX)) FROM				 ActionPersonRelatedPerson pcrp 
		INNER JOIN PersonRelationshipType pr ON pcrp.PersonRelationshipTypeID = pr.PersonRelationshipTypeID  
		INNER JOIN Person p1 ON pcrp.RelatedPersonGUID = p1.PersonGUID 
		WHERE (pcrp.PersonGUID = ap.PersonGUID ) AND pcrp.ActionGUID = ap.ActionGUID FOR XML PATH (''))
		
		AS Relationships
			
		, (SELECT MIN(SortOrder) FROM ActionPerson pcpt INNER JOIN PersonType pt ON pcpt.PersonTypeID = pt.PersonTypeID WHERE pcpt.PersonGUID = ap.PersonGUID AND pcpt.ActionGUID = ap.ActionGUID) AS SortOrder
	,CONVERT(DATETIME, (SELECT MAX(ServiceDate) FROM Service WHERE Service.ActionGUID=@ActionGUID AND Service.PersonGUID = ap.PersonGUID)) AS ServiceDate
	,CONVERT(VARCHAR(100),'') AS Attorney
	,case when ap.CounselStatus is null 
				then ''
		  when ap.CounselStatus = 1
		        then 'Appointed'
		  when ap.CounselStatus = 2
				then 'Retained'
		  when ap.CounselStatus = 3
				then 'Waived/Refused'
	end as CounselStatus
	FROM ActionPerson ap
		JOIN Person p ON ap.PersonGUID = p.PersonGUID
		JOIN PersonType pt ON ap.PersonTypeID = pt.PersonTypeID
		LEFT JOIN CustodialType ct ON ct.CustodialTypeID=ap.CustodialTypeID 
		LEFT JOIN FatherType ft ON ft.FatherTypeID = ap.FatherTypeID 
	WHERE ap.ActionGUID = @ActionGUID
)
SELECT
	NPFlag
	,PersonGUID
	,FirstName
	,MiddleName
	,LastName
	,NameSuffix
	,BirthDate
	,Sex
	,PersonTypeName
	,CASE
		WHEN RIGHT(Relationships, 1) = '|' THEN LEFT(Relationships, LEN(Relationships) - 1)
		ELSE Relationships
	END AS Relationships
	, ServiceDate
	, Attorney
	,CounselStatus
	, CustodialTypeName
	, FatherTypeName
FROM Persons
ORDER BY SortOrder

END





GO


