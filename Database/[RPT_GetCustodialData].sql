USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetCustodialData]    Script Date: 10/27/2010 11:19:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetCustodialData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetCustodialData]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetCustodialData]    Script Date: 10/27/2010 11:19:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/11/2010
-- Description:	Gets Custodial/Non-Custodial Person data
-- =============================================
CREATE PROCEDURE [dbo].[RPT_GetCustodialData]
	@ActionGUID  uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;
    
   	declare @temp TABLE
	(
		ActionGuid uniqueidentifier,
		NcpName varchar(250) null,
		CpName varchar(250) null,
		NcpAttorneyName VarChar(250) null,
		CpAttorneyName varchar(250)null
	)

	insert into @temp(ActionGuid) values(@ActionGUID)
		
	declare @cpname varchar(250)
	declare @ncpname varchar(250)
	declare @ncpattname varchar(250)
	declare @cpattname varchar(250)
	
	select @cpname = null, @ncpname=null, @ncpattname=null, @cpattname=null
	
	SET @cpname = (select top 1 p.LastName + ',' + p.FirstName FROM [ActionPerson] ap
	inner join Person p on p.PersonGUID = ap.PersonGUID
    where ap.ActionGUID = @ActionGUID
	AND ap.CustodialTypeID=0)
	
	SET @ncpname = (select top 1 p.LastName + ',' + p.FirstName FROM [ActionPerson] ap
	inner join Person p on p.PersonGUID = ap.PersonGUID
    where ap.ActionGUID = @ActionGUID
	AND ap.CustodialTypeID=1)
	
	
	set @ncpattname = (SELECT top 1
		rp.FirstName + ' ' + rp.LastName AttyName
	FROM ActionPerson
	INNER JOIN Person ON ActionPerson.PersonGUID=Person.PersonGUID
	inner join ActionPersonRelatedPerson ON (ActionPersonRelatedPerson.PersonGUID = Person.PersonGUID AND ActionPersonRelatedPerson.PersonRelationshipTypeID=7 )
	inner join Person rp ON rp.PersonGUID = ActionPersonRelatedPerson.RelatedPersonGUID
	WHERE ActionPerson.ActionGUID = @ActionGuid
	and ActionPerson.CustodialTypeID=1)
	
	set @cpattname = (SELECT top 1
		rp.FirstName + ' ' + rp.LastName AttyName
	FROM ActionPerson
	INNER JOIN Person ON ActionPerson.PersonGUID=Person.PersonGUID
	inner join ActionPersonRelatedPerson ON (ActionPersonRelatedPerson.PersonGUID = Person.PersonGUID AND ActionPersonRelatedPerson.PersonRelationshipTypeID=7 )
	inner join Person rp ON rp.PersonGUID = ActionPersonRelatedPerson.RelatedPersonGUID
	WHERE ActionPerson.ActionGUID = @ActionGuid
	and ActionPerson.CustodialTypeID=0)
	
	update @temp set CpName = @cpname where ActionGuid = @ActionGUID
	update @temp set NcpName = @ncpname where ActionGuid = @ActionGUID
	update @temp set NcpAttorneyName = @ncpattname where ActionGuid = @ActionGUID
	update @temp set CpAttorneyName = @cpattname where ActionGuid = @ActionGUID
	
	select ActionGuid, NcpName, CpName, NcpAttorneyName, CpAttorneyName from @temp
END



GO


