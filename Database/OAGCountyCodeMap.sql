USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OAGCountyCodeMap_County]') AND parent_object_id = OBJECT_ID(N'[dbo].[OAGCountyCodeMap]'))
ALTER TABLE [dbo].[OAGCountyCodeMap] DROP CONSTRAINT [FK_OAGCountyCodeMap_County]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGCountyCodeMap_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGCountyCodeMap] DROP CONSTRAINT [DF_OAGCountyCodeMap_CreateDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGCountyCodeMap_LastUpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGCountyCodeMap] DROP CONSTRAINT [DF_OAGCountyCodeMap_LastUpdatedDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGCountyCodeMap]    Script Date: 08/25/2011 16:37:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAGCountyCodeMap]') AND type in (N'U'))
DROP TABLE [dbo].[OAGCountyCodeMap]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGCountyCodeMap]    Script Date: 08/25/2011 16:37:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OAGCountyCodeMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountyCode] [nvarchar](3) NOT NULL,
	[CountyID] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OAGCountyCodeMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OAGCountyCodeMap]  WITH CHECK ADD  CONSTRAINT [FK_OAGCountyCodeMap_County] FOREIGN KEY([CountyID])
REFERENCES [dbo].[County] ([CountyID])
GO

ALTER TABLE [dbo].[OAGCountyCodeMap] CHECK CONSTRAINT [FK_OAGCountyCodeMap_County]
GO

ALTER TABLE [dbo].[OAGCountyCodeMap] ADD  CONSTRAINT [DF_OAGCountyCodeMap_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[OAGCountyCodeMap] ADD  CONSTRAINT [DF_OAGCountyCodeMap_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdatedDate]
GO


