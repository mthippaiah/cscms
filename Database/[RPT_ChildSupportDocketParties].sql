USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_ChildSupportDocketParties]    Script Date: 03/28/2011 15:33:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_ChildSupportDocketParties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_ChildSupportDocketParties]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_ChildSupportDocketParties]    Script Date: 03/28/2011 15:33:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/12/2010
-- Description:	Gets Parties For a Case/CauseID
-- =============================================
CREATE PROCEDURE [dbo].[RPT_ChildSupportDocketParties] 
	@ActionGUID uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

 
    declare @temp table
    (
		specialorderid int not null
    )
    
	
	insert into @temp
	select so1.SpecialOrderID from SpecialOrder so1
	where so1.ActionGUID = @ActionGUID
		  and so1.LastUpdateDate = (select top 1 LastUpdateDate from SpecialOrder so2
								where so2.PersonGUID = so1.PersonGUID and so1.ActionGUID = so2.ActionGUID
								order by so2.LastUpdateDate desc)
		  



	SELECT 
		a.ActionGUID
		,s.ServiceDate ServiceDate
		,ActionPerson.NPFlag NecessaryParty
		,Person.FirstName
		,Person.MiddleName
		,Person.LastName
		,'Party Name' = Case ct.CustodialTypeID when 0 then
				'Custodial Parent:' + Person.FirstName + ' ' + Person.MiddleName + ' ' +  Person.LastName
			    when 1 then
			     'Non-Custodial Parent:' + Person.FirstName + ' ' + Person.MiddleName + ' ' +  Person.LastName
			     end
		,pt.PersonTypeName Relationship
		,rp.FirstName + ' ' + rp.LastName AttyName
		,DBO.GetActionServiceDateonLastNP(a.ActionGUID) AS ServiceOnLastNecessaryPartyDate
		,(case when so.CommitmentOrCapias is not null and so.CommitmentOrCapias = '1' 
				then 1
			   else 0
		  end) CapiasIssued
		,(case when so.ArrestCommitmentEffectiveDate is null
				then 0
				when so.ArrestCommitmentEffectiveDate is not null and so.ReleaseDate is null
			    then 1
			    else 0
		   end) InJail
		,ActionPerson.PersonTypeID RelationshipTypeID
		,0 IndToCasebDeleted
		,ABS(DATEDIFF(d,DBO.GetActionServiceDateonLastNP(a.ActionGUID), GETDATE())) as 'ServiceAge'
	FROM [Action] a
		INNER JOIN ActionPerson ON ActionPerson.ActionGUID=a.ActionGUID
		inner join CustodialType ct on ct.CustodialTypeID = ActionPerson.CustodialTypeID 
		INNER JOIN Person ON ActionPerson.PersonGUID=Person.PersonGUID
		JOIN PersonType pt ON pt.PersonTypeID=ActionPerson.PersonTypeID
		LEFT JOIN [Service] s ON s.ActionGUID=a.ActionGUID AND s.PersonGUID = Person.PersonGUID 
		LEFT JOIN ActionPersonRelatedPerson ON (ActionPersonRelatedPerson.PersonGUID = Person.PersonGUID AND ActionPersonRelatedPerson.PersonRelationshipTypeID=7 )
		LEFT JOIN Person rp ON rp.PersonGUID = ActionPersonRelatedPerson.RelatedPersonGUID
		left join SpecialOrder so on so.ActionGUID = a.ActionGUID and so.PersonGUID = Person.PersonGUID and so.SpecialOrderID in (select specialorderid from @temp)
	WHERE ActionPerson.NPFlag = 1
		AND a.ActionGUID = @ActionGUID
	ORDER BY ct.CustodialTypeID desc, ActionPerson.PersonTypeID

END


GO


