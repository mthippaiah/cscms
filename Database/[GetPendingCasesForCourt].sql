USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetPendingCasesForCourt]    Script Date: 03/22/2011 17:06:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPendingCasesForCourt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPendingCasesForCourt]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetPendingCasesForCourt]    Script Date: 03/22/2011 17:06:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/14/2010
-- Description:	Get Pending Cases 
-- =============================================
CREATE PROCEDURE [dbo].[GetPendingCasesForCourt]
	@OCACourtID INT
AS
BEGIN
	SET NOCOUNT ON;
 
	--Temporary tables to filter the data
	declare @temp TABLE
	(
    		_countyid smallint
	)
    
	insert into @temp
	select cy.CountyID from County cy 
		INNER JOIN OCACourtCounty OCACourtCounty ON cy.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		where OCACourt.OCACourtID = @OCACourtID
    
 
    declare @temp2 TABLE
	(
		CauseNumber nvarchar(50),
		ActionGuid uniqueidentifier,
		ActionServiceDateOnLastNP Date null
	)
	
	insert into @temp2
	    SELECT distinct
		Cause.CauseNumber
		,a.ActionGUID
		,(SELECT MAX(ISNULL(ServiceDate,'1/1/1970')) 
			FROM ActionPerson pa 
			LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID 
			AND pa.ActionGUID = a.ActionGUID AND pa.NPFlag = 1
			WHERE pa.ActionGUID = a.ActionGUID AND pa.NPFlag = 1)
	FROM   Cause 
		join [Action] a on Cause.CauseGUID = a.CauseGUID
		INNER JOIN ActionType ActionType ON a.ActionTypeID=ActionType.ActionTypeID
		INNER JOIN County County ON Cause.CountyID=County.CountyID 
		INNER JOIN OCACourtCounty OCACourtCounty ON County.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
		inner join ActionPerson ap on ap.ActionGUID = a.ActionGUID
		left join [Service] svc on svc.ActionGUID = a.ActionGUID and svc.PersonGUID = ap.PersonGUID
	where A.DispositionRenderedDate is null
	    and ActionStatus = 'A'
	    and  NOT (ActionType.ActionTypeName = 'After Trial Motion' OR ActionType.ActionTypeName ='Other (Non-expedited)' OR ActionType.ActionTypeName = 'Motion to Stay Income Withholding') 
		and Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
		and ap.NPFlag = 1
		and NOT EXISTS (SELECT pa.PersonGUID FROM ActionPerson pa 
							LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID AND pa.ActionGUID = a.ActionGUID
							WHERE s.PersonGUID IS null AND pa.ActionGUID = a.ActionGUID AND pa.NPFlag = 1)
	   
    SELECT distinct
		a.ActionGUID
		,ActionType.ActionTypeName
		,Cause.CauseNumber
		,County.CountyName
		,OCACourt.CourtName
		,Region.RegionName
		,T2.ActionServiceDateOnLastNP AS ServiceOnLastNecessaryPartyDate
		,A.DispositionRenderedDate, A.ActionNumber
		,A.OAGCauseNumber
		,Cause.CauseGUID
		,a.ActionFiledDate as FileDate
		,datediff(d,T2.ActionServiceDateOnLastNP, getdate()) as AgeInDays
		,(select top 1 p.LastName + ',' + p.FirstName FROM [ActionPerson] ap
			inner join Person p on p.PersonGUID = ap.PersonGUID
			where ap.ActionGUID = a.ActionGUID
			AND ap.CustodialTypeID=1) as NCP  
	FROM   Cause 
		join [Action] a on Cause.CauseGUID = a.CauseGUID
		INNER JOIN ActionType ActionType ON a.ActionTypeID=ActionType.ActionTypeID
		INNER JOIN County County ON Cause.CountyID=County.CountyID 
		INNER JOIN OCACourtCounty OCACourtCounty ON County.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
		inner join @temp2 T2 on T2.ActionGUID = a.ActionGUID
	where A.DispositionRenderedDate is null
	    and ActionStatus = 'A'
	    and  NOT (ActionType.ActionTypeName = 'After Trial Motion' OR ActionType.ActionTypeName ='Other (Non-expedited)' OR ActionType.ActionTypeName = 'Motion to Stay Income Withholding') 
		and Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
	ORDER BY County.CountyName, Cause.CauseNumber
	
END


GO


