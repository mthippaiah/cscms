USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_SummaryReportExpeditedProcessCompliance]    Script Date: 03/22/2011 17:04:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_SummaryReportExpeditedProcessCompliance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_SummaryReportExpeditedProcessCompliance]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_SummaryReportExpeditedProcessCompliance]    Script Date: 03/22/2011 17:04:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/19/2010
-- Description:	Summary Report on Expedited Process Compliance
-- =============================================
CREATE PROCEDURE [dbo].[RPT_SummaryReportExpeditedProcessCompliance]
	@Year INT,
	@Month Int,
	@OCACourtID INT
AS
BEGIN
	SET NOCOUNT ON;

	declare @temp TABLE
	(
		CauseNumber nvarchar(50),
		ActionNumber smallint,
		ActionGuid uniqueidentifier,
		ActionServiceDateOnLastNP Date null
	)
	
	insert into @temp
	SELECT distinct Cause.CauseNumber, a.ActionNumber, a.ActionGUID
		,dbo.GetActionServiceDateonLastNP(a.ActionGUID)
	FROM Cause
		inner join [Action] a on a.CauseGUID = Cause.CauseGUID 
		INNER JOIN ActionType ActionType ON a.ActionTypeID=ActionType.ActionTypeID
		INNER JOIN County County ON Cause.CountyID = County.CountyID 
		inner join OCACourt on OCACourt.OCACourtID = Cause.OCACourtID
	WHERE   NOT (ActionType.ActionTypeName = 'After Trial Motion' OR ActionType.ActionTypeName ='Other (Non-expedited)' OR ActionType.ActionTypeName = 'Motion to Stay Income Withholding')
		and OCACourt.OCACourtID = @OCACourtID
		and a.ActionFiledDate is not null
	
	SELECT DISTINCT 
		Cause.CauseNumber
		,ActionType.ActionTypeName  ActionType
		,a.DispositionRenderedDate DispositionDate
		,dbo.GetActionServiceDateonLastNP(a.ActionGUID) AS ServiceOnLastNecessaryPartyDate
		,a.ActionNumber ActionNum
		,County.CountyName 
		,OCACourt.CourtName
		,a.ActionFiledDate FilingDate
		,Case
			when T1.ActionServiceDateOnLastNP >= a.ActionFiledDate then 
				ABS(DATEDIFF(d,a.DispositionRenderedDate, T1.ActionServiceDateOnLastNP))
			else ABS(DATEDIFF(d,a.DispositionRenderedDate, a.ActionFiledDate))
		end 'DateDiff'
	FROM Cause
		inner join [Action] a on a.CauseGUID = Cause.CauseGUID 
		INNER JOIN ActionType ActionType ON a.ActionTypeID=ActionType.ActionTypeID
		INNER JOIN County County ON Cause.CountyID = County.CountyID 
		inner join OCACourt on OCACourt.OCACourtID = Cause.OCACourtID
		inner join @temp T1 on T1.ActionGuid = a.ActionGUID 
	WHERE   NOT (ActionType.ActionTypeName = 'After Trial Motion' OR ActionType.ActionTypeName ='Other (Non-expedited)' OR ActionType.ActionTypeName = 'Motion to Stay Income Withholding')
		and a.ActionFiledDate is not null
		AND T1.ActionServiceDateOnLastNP IS NOT NULL
		AND YEAR(T1.ActionServiceDateOnLastNP) = @Year 
		and MONTH(T1.ActionServiceDateOnLastNP) = @Month
		
	ORDER BY County.CountyName, Cause.CauseNumber
END


GO


