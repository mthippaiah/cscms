USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_OutStandingCapias]    Script Date: 03/28/2011 15:21:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_OutStandingCapias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_OutStandingCapias]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_OutStandingCapias]    Script Date: 03/28/2011 15:21:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: Jan 25, 2011
-- Description:	Outstanding capias
-- =============================================
CREATE PROCEDURE [dbo].[RPT_OutStandingCapias]
	@OCACourtID INT
AS
BEGIN
	SET NOCOUNT ON;
    
    declare @temp table
    (
		specialorderid int not null
    )
    
	
	insert into @temp
	select so1.SpecialOrderID from SpecialOrder so1
	where so1.LastUpdateDate = (select top 1 LastUpdateDate from SpecialOrder so2
								where so2.PersonGUID = so1.PersonGUID and so1.ActionGUID = so2.ActionGUID
								order by so2.LastUpdateDate desc)

	SELECT [Cause].CauseNumber
		,a.OAGCauseNumber
		,County.CountyName
		,OCACourt.CourtName
		,a.ActionNumber ActionNum
		,a.Style Style
		,a.ActionFiledDate FilingDate
		,a.CauseGUID CaseGUID
		,a.ActionGUID
		,[Cause].OCACourtID 
		,p.FirstName
		,p.LastName
		,so.BondAmount
		,so.IssuedDate
	FROM  [Action] a
	INNER JOIN [Cause]ON a.CauseGUID=[Cause].CauseGUID 
	INNER JOIN County County ON [Cause].CountyID=County.CountyID 
	INNER JOIN OCACourt OCACourt ON [Cause].OCACourtID=OCACourt.OCACourtID
	Inner Join SpecialOrder so on so.ActionGUID = a.ActionGUID
	inner join Person p on p.PersonGUID = so.PersonGUID
	inner join @temp t on t.specialorderid = so.SpecialOrderID
	WHERE [Cause].OCACourtID = @OCACourtID
	and so.OrderedDate is not null
	and so.ArrestCommitmentEffectiveDate is null
	ORDER BY [Cause].CauseNumber

END



GO


