USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SpecialOrderType_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SpecialOrderType] DROP CONSTRAINT [DF_SpecialOrderType_CreateDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SpecialOrderType_LastUpdateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SpecialOrderType] DROP CONSTRAINT [DF_SpecialOrderType_LastUpdateDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[SpecialOrderType]    Script Date: 03/25/2011 14:37:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpecialOrderType]') AND type in (N'U'))
DROP TABLE [dbo].[SpecialOrderType]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[SpecialOrderType]    Script Date: 03/25/2011 14:37:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SpecialOrderType](
	[SpecialOrderTypeID] [smallint] IDENTITY(1,1) NOT NULL,
	[SpecialOrderTypeName] [nvarchar](50) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[Version] [timestamp] NOT NULL,
 CONSTRAINT [PK_SpecialOrderType] PRIMARY KEY CLUSTERED 
(
	[SpecialOrderTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SpecialOrderType] ADD  CONSTRAINT [DF_SpecialOrderType_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[SpecialOrderType] ADD  CONSTRAINT [DF_SpecialOrderType_LastUpdateDate]  DEFAULT (getdate()) FOR [LastUpdateDate]
GO

use CSCMS
go

INSERT INTO [SpecialOrderType]([SpecialOrderTypeName] ,[SortOrder]) VALUES ('Capias - FTA', 20)
INSERT INTO [SpecialOrderType]([SpecialOrderTypeName] ,[SortOrder]) VALUES ('Capias - Attachment', 10)
INSERT INTO [SpecialOrderType]([SpecialOrderTypeName] ,[SortOrder]) VALUES ('Capias - MTRP', 30)
INSERT INTO [SpecialOrderType]([SpecialOrderTypeName] ,[SortOrder]) VALUES ('Capias - Withdrawn', 40)
INSERT INTO [SpecialOrderType]([SpecialOrderTypeName] ,[SortOrder]) VALUES ('Commitment', 50)
INSERT INTO [SpecialOrderType]([SpecialOrderTypeName] ,[SortOrder]) VALUES ('Writ Withdrawn', 60)

GO

Alter table DispositionType ADD IsActive bit not null DEFAULT 1

Update DispositionType set IsActive = 0 
where DispositionTypeName in ('Capias - FTA', 'Capias - Attachment','Capias - MTRP' )

Alter table SpecialOrder ADD SpecialOrderTypeID smallint null

go

USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionAging]    Script Date: 03/28/2011 11:54:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetActionAging]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetActionAging]
GO

USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionAging]    Script Date: 03/28/2011 11:54:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- HN 06/2009
--6.3.	Action Aging
--6.3.1.	The Action Age field is displayed at the Action level and is a protected (non-editable) field displaying a calculated value. 
--The Action Age calculation starts the day after the Last Necessary Party Service Date (LNPSD). 
--[The Last Necessary Party Service Date field is displayed at the Action level, 
--and is a protected (non-editable) field displaying a derived value of the latest date value entered into the Service Date 
--field for any of that action�s Necessary Party person records. ]
--6.3.2.	The default Action Age calculation is the difference between the  date of the day after the Last Necessary 
--Party Service Date and the current day�s date.
--6.3.3.	Triggers to stop the Action Age accrual once it is started are as follows:
--o	Any disposition reason with exception of �Referred to District Judge�
--o	A �Capias Issue Date� entered on the person record of a Necessary Party.
--6.3.4.	Triggers that engage Action Age accrual to begin again once stopped are as follows:
--o	A Compliance Hearing date that is scheduled after Action Age accrual stopped due to a �Capias Issue Date� entered on the person record of a Necessary Party. The number of days from the date accrual stopped through the date accrual started again are not included in the Action Age. The calculation on re-start takes the number of days from the previous accrual timeframe(s) and adds those to the next consecutive accrual timeframe.
 
-- =============================================
-- CREATE FUNCTION [dbo].[GetActionAging]
CREATE FUNCTION [dbo].[GetActionAging]
	(@ActionGUID uniqueidentifier) RETURNS int
AS
BEGIN
	-- SELECT dbo.[GetActionAging] ('108B42CE-5B6C-403D-A046-12CD8740BF17')
	
	DECLARE @Aging int
	DECLARE @LastNPServiceDate DATE
	SET @Aging = null
	
	DECLARE @PrimaryStatus VARCHAR(20)
	DECLARE @DispositionRenderedDate DATE
	
	DECLARE @DispositionTypeID INT 
	
	DECLARE @StopDate DATE
	SET @StopDate = GETDATE()
	
	-- DispositionTypeID   Diposition 
	----------------------------------
	-- 6	               Capias - MTRP (Arrest Warrant)
	-- 7	               Capias - FTA
	-- 5	               Capias � Attachment
    ----------------------------------
    
    declare @lnp_person_guid uniqueidentifier
	declare @arrest_date date
	declare @sotype Varchar(50)
	declare @soordereddate date
	set @lnp_person_guid = null
	set @arrest_date = null
		
	-- IF THERE IS NO NP, return null		
	IF NOT EXISTS(SELECT ActionGUID FROM ActionPerson WHERE ActionGUID=@ActionGUID AND NPFlag = 1) 
	BEGIN
		SET @Aging = null -- RETURN VALUES NULL
	END		
   	ELSE
	BEGIN
		-- IF THERE IS NP with no service, return null
		IF EXISTS (SELECT pa.PersonGUID FROM ActionPerson pa 
			LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID
			AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
			WHERE s.PersonGUID IS null AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1) 
		BEGIN
			SET @Aging = null -- RETURN VALUES NULL
		END
		ELSE
		BEGIN
		
			-- Get latest NP service date
			SELECT @LastNPServiceDate = MAX(ISNULL(ServiceDate,'1/1/1970')) 
			FROM ActionPerson pa 
			LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID
			AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
			WHERE pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
			
			SELECT @arrest_date=ArrestCommitmentEffectiveDate, @sotype=sot.SpecialOrderTypeName, @soordereddate=so.OrderedDate  from SpecialOrder so 
				inner join SpecialOrderType sot on sot.SpecialOrderTypeID = so.SpecialOrderTypeID
				where so.ActionGUID = @ActionGUID 
				      and so.SpecialOrderID = (select top 1 SpecialOrderID from SpecialOrder so2 where so2.ActionGUID = @ActionGUID order by so2.LastUpdateDate desc)
	
	      
			IF EXISTS(select a.DispositionTypeID from [Action] a 
				where a.ActionGUID=@ActionGUID and a.DispositionTypeID is not null and a.DispositionTypeID not in
				 (select DispositionTypeID from DispositionType where DispositionTypeName in (select SpecialOrderTypeName from SpecialOrderType)))
			begin
					IF EXISTS(SELECT * FROM [Action] WHERE ActionGUID = @ActionGUID AND DispositionRenderedDate IS NOT NULL)
						SELECT @StopDate = DATEADD(d,1,DispositionRenderedDate)  FROM [Action] WHERE ActionGUID = @ActionGUID 

					SET @Aging= DATEDIFF(d,@LastNPServiceDate,@StopDate) - 1
			end
			else IF EXISTS(select a.DispositionTypeID from [Action] a   -- if for old data, if these dispositions exists
				where a.ActionGUID=@ActionGUID and a.DispositionTypeID is not null and a.DispositionTypeID in
				 (select DispositionTypeID from DispositionType where DispositionTypeName in ('Capias - Attachment', 'Capias - MTRP','Capias - FTA')))
			begin
				set @Aging= null
				if @sotype is not null and (@sotype = 'Capias Withdrawn' or @soType = 'Writ Withdrawn')
					set @Aging= DATEDIFF(d,@soordereddate,getdate())
				else if @arrest_date is not null
				begin
					SET @Aging= DATEDIFF(d,@arrest_date,getdate())
				end
			end
			else if not exists (select a.DispositionTypeID from [Action] a   
				where a.ActionGUID=@ActionGUID and a.DispositionTypeID is not null)
			begin
				set @Aging= null
				if @sotype is not null and (@sotype = 'Capias Withdrawn' or @soType = 'Writ Withdrawn')
					set @Aging= DATEDIFF(d,@soordereddate,getdate())
				else if @arrest_date is not null
				begin
					SET @Aging= DATEDIFF(d,@arrest_date,getdate())
				end
				else if @sotype is null or (@sotype <> 'Capias - Attachment' and @soType <> 'Capias - MTRP' and  @soType <> 'Capias - FTA' and  @soType <> 'Commitment')
				begin
					SET @Aging= DATEDIFF(d,@LastNPServiceDate,getdate()) - 1
				end
			end
		END

	END
	
	RETURN @Aging

END

GO


USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionPrimaryStatus]    Script Date: 01/31/2011 14:49:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetActionPrimaryStatus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetActionPrimaryStatus]
GO

USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionPrimaryStatus]    Script Date: 01/31/2011 14:49:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- HN 06/2009
-- 
--7.1.	Active/Inactive: SDCMS referenced cases as being Open or Closed. 
--CSCMS doesn�t use the open or closed status. Instead, 
--CSCMS uses the primary status of Disposed or Pending and 
--secondary status of Active or Inactive for each Action. 
--An Action without a Disposition Type and Disposition Date will have a status of Pending and Active. 
--However, an Action having a Disposition Type and Disposition Date may have a status of either Active or Inactive. 
--When Disposition Type and Disposition Date (saving data for either of these two fields requires data entered in both fields) data is entered, 
--the system defaults the Active/Inactive flag to Inactive. 
--However, the user can edit the Active/Inactive field to toggle between saving the record as Active and 
--Inactive an unlimited amount of times, with the only validation being that a Disposition Type 
--and Disposition Date must be populated in order to save the record as Inactive. 
-- =============================================

CREATE FUNCTION [dbo].[GetActionPrimaryStatus](@ActionGUID uniqueidentifier) RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @PrimaryStatus VARCHAR(20)
	DECLARE @DispositionTypeID INT 
	
	SET @PrimaryStatus='Pending'
		
	if EXISTS(SELECT DispositionTypeID FROM Action WHERE ActionGUID=@ActionGUID and DispositionTypeID is not null)
	begin
		   		
		   if not exists(select DispositionTypeID FROM Action WHERE ActionGUID=@ActionGUID and DispositionTypeID in
				 (select DispositionTypeID from DispositionType where DispositionTypeName in (select SpecialOrderTypeName from SpecialOrderType)))
			  SET @PrimaryStatus='Disposed'
	end

	RETURN @PrimaryStatus

END
GO