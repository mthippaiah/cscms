USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetPendingCasesWithAgeRange]    Script Date: 02/10/2011 10:23:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetPendingCasesWithAgeRange]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetPendingCasesWithAgeRange]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetPendingCasesWithAgeRange]    Script Date: 02/10/2011 10:23:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/14/2010
-- Description:	Get Pending Cases Which are so many days old
-- =============================================
CREATE PROCEDURE [dbo].[RPT_GetPendingCasesWithAgeRange]
	@CountyName varchar(50),
	@OCACourtID INT,
	@FromAgeInDays INT,
	@ToAgeInDays INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

    --Temporary tables to filter the data
	declare @temp TABLE
	(
    		_countyid smallint
	)
    
    if @CountyName = 'All'begin
		insert into @temp
		select cy.CountyID from County cy 
			INNER JOIN OCACourtCounty OCACourtCounty ON cy.CountyID=OCACourtCounty.CountyID
			INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
			where OCACourt.OCACourtID = @OCACourtID
    end
    else begin
		insert into @temp
		select CountyID from County where CountyName = @CountyName
    end
 
 
    declare @temp2 TABLE
	(
		CauseNumber nvarchar(50),
		ActionGuid uniqueidentifier,
		ActionServiceDateOnLastNP Date null
	)
	
	insert into @temp2
	    SELECT distinct
		Cause.CauseNumber
		,a.ActionGUID
		,(SELECT MAX(ISNULL(ServiceDate,'1/1/1970')) 
			FROM ActionPerson pa 
			LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID 
			AND pa.ActionGUID = a.ActionGUID AND pa.NPFlag = 1
			WHERE pa.ActionGUID = a.ActionGUID AND pa.NPFlag = 1)
	FROM   Cause 
		join [Action] a on Cause.CauseGUID = a.CauseGUID
		INNER JOIN ActionType ActionType ON a.ActionTypeID=ActionType.ActionTypeID
		INNER JOIN County County ON Cause.CountyID=County.CountyID 
		INNER JOIN OCACourtCounty OCACourtCounty ON County.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
		inner join ActionPerson ap on ap.ActionGUID = a.ActionGUID
		left join [Service] svc on svc.ActionGUID = a.ActionGUID and svc.PersonGUID = ap.PersonGUID
	where A.DispositionRenderedDate is null
	    and ActionStatus = 'A'
		and Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
		and ap.NPFlag = 1
		and NOT EXISTS (SELECT pa.PersonGUID FROM ActionPerson pa 
							LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID AND pa.ActionGUID = a.ActionGUID
							WHERE s.PersonGUID IS null AND pa.ActionGUID = a.ActionGUID AND pa.NPFlag = 1)
	   
    SELECT distinct
		a.ActionGUID
		,ActionType.ActionTypeName
		,Cause.CauseNumber
		,County.CountyName
		,OCACourt.CourtName
		,Region.RegionName
		,T2.ActionServiceDateOnLastNP AS ServiceOnLastNecessaryPartyDate
		,A.DispositionRenderedDate, A.ActionNumber
		,A.OAGCauseNumber
		,Cause.CauseGUID
		,a.ActionFiledDate as FileDate
		,datediff(d,T2.ActionServiceDateOnLastNP, getdate()) as AgeInDays
		,(select top 1 p.LastName + ',' + p.FirstName FROM [ActionPerson] ap
			inner join Person p on p.PersonGUID = ap.PersonGUID
			where ap.ActionGUID = a.ActionGUID
			AND ap.CustodialTypeID=1) as NCP  
	FROM   Cause 
		join [Action] a on Cause.CauseGUID = a.CauseGUID
		INNER JOIN ActionType ActionType ON a.ActionTypeID=ActionType.ActionTypeID
		INNER JOIN County County ON Cause.CountyID=County.CountyID 
		INNER JOIN OCACourtCounty OCACourtCounty ON County.CountyID=OCACourtCounty.CountyID
		INNER JOIN OCACourt OCACourt ON OCACourtCounty.OCACourtID=OCACourt.OCACourtID
		INNER JOIN Region Region ON OCACourt.RegionID=Region.RegionID
		inner join @temp2 T2 on T2.ActionGUID = a.ActionGUID
	where A.DispositionRenderedDate is null
		and datediff(d,T2.ActionServiceDateOnLastNP, getdate()) >= @FromAgeInDays 
	    and (@ToAgeInDays is null or datediff(d,T2.ActionServiceDateOnLastNP, getdate()) <= @ToAgeInDays)
	    and ActionStatus = 'A'
		and Cause.CountyID in (select _countyid from @temp)
		and OCACourt.OCACourtID = @OCACourtID
	ORDER BY County.CountyName, Cause.CauseNumber
    
END





GO


