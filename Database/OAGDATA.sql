USE [CSCMS]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_OAGDATA_CreateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OAGDATA] DROP CONSTRAINT [DF_OAGDATA_CreateDate]
END

GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGDATA]    Script Date: 08/25/2011 16:38:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OAGDATA]') AND type in (N'U'))
DROP TABLE [dbo].[OAGDATA]
GO

USE [CSCMS]
GO

/****** Object:  Table [dbo].[OAGDATA]    Script Date: 08/25/2011 16:38:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OAGDATA](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RecordType] [nvarchar](1) NULL,
	[CsdCaseId] [nvarchar](10) NULL,
	[CauseNumber] [nvarchar](15) NULL,
	[LegalActnCode] [nvarchar](4) NULL,
	[LegalActnDesc] [nvarchar](30) NULL,
	[CountyCode] [nvarchar](3) NULL,
	[DomesticViolenceInd] [nvarchar](1) NULL,
	[OuNumber] [nvarchar](5) NULL,
	[OuContactPhoneNumber] [nvarchar](14) NULL,
	[JudgeName] [nvarchar](45) NULL,
	[CourtNumber] [nvarchar](3) NULL,
	[OriginatingCourtNumber] [nvarchar](3) NULL,
	[FileDate] [nvarchar](8) NULL,
	[DocketDate] [nvarchar](8) NULL,
	[MbrSrvdFname1] [nvarchar](15) NULL,
	[MbrSrvdMname1] [nvarchar](10) NULL,
	[MbrSrvdLname1] [nvarchar](20) NULL,
	[MbrSrvdRtc1] [nvarchar](2) NULL,
	[MbrSrvdRtd1] [nvarchar](2) NULL,
	[MbrSrvdFname2] [nvarchar](15) NULL,
	[MbrSrvdMname2] [nvarchar](10) NULL,
	[MbrSrvdLname2] [nvarchar](20) NULL,
	[MbrSrvdRtc2] [nvarchar](2) NULL,
	[MbrSrvdRtd2] [nvarchar](2) NULL,
	[MbrSrvdFname3] [nvarchar](15) NULL,
	[MbrSrvdMname3] [nvarchar](10) NULL,
	[MbrSrvdLname3] [nvarchar](20) NULL,
	[MbrSrvdRtc3] [nvarchar](2) NULL,
	[MbrSrvdRtd3] [nvarchar](2) NULL,
	[MbrSrvdFname4] [nvarchar](15) NULL,
	[MbrSrvdMname4] [nvarchar](10) NULL,
	[MbrSrvdLname4] [nvarchar](20) NULL,
	[MbrSrvdRtc4] [nvarchar](2) NULL,
	[MbrSrvdRtd4] [nvarchar](2) NULL,
	[DpFname1] [nvarchar](15) NULL,
	[DpMname1] [nvarchar](10) NULL,
	[DpLname1] [nvarchar](20) NULL,
	[DpDOB1] [nvarchar](8) NULL,
	[DpFname2] [nvarchar](15) NULL,
	[DpMname2] [nvarchar](10) NULL,
	[DpLname2] [nvarchar](20) NULL,
	[DpDOB2] [nvarchar](8) NULL,
	[DpFname3] [nvarchar](15) NULL,
	[DpMname3] [nvarchar](10) NULL,
	[DpLname3] [nvarchar](20) NULL,
	[DpDOB3] [nvarchar](8) NULL,
	[DpFname4] [nvarchar](15) NULL,
	[DpMname4] [nvarchar](10) NULL,
	[DpLname4] [nvarchar](20) NULL,
	[DpDOB4] [nvarchar](8) NULL,
	[DpFname5] [nvarchar](15) NULL,
	[DpMname5] [nvarchar](10) NULL,
	[DpLname5] [nvarchar](20) NULL,
	[DpDOB5] [nvarchar](8) NULL,
	[DpFname6] [nvarchar](15) NULL,
	[DpMname6] [nvarchar](10) NULL,
	[DpLname6] [nvarchar](20) NULL,
	[DpDOB6] [nvarchar](8) NULL,
	[DpFname7] [nvarchar](15) NULL,
	[DpMname7] [nvarchar](10) NULL,
	[DpLname7] [nvarchar](20) NULL,
	[DpDOB7] [nvarchar](8) NULL,
	[DpFname8] [nvarchar](15) NULL,
	[DpMname8] [nvarchar](10) NULL,
	[DpLname8] [nvarchar](20) NULL,
	[DpDOB8] [nvarchar](8) NULL,
	[DpFname9] [nvarchar](15) NULL,
	[DpMname9] [nvarchar](10) NULL,
	[DpLname9] [nvarchar](20) NULL,
	[DpDOB9] [nvarchar](8) NULL,
	[DpFname10] [nvarchar](15) NULL,
	[DpMname10] [nvarchar](10) NULL,
	[DpLname10] [nvarchar](20) NULL,
	[DpDOB10] [nvarchar](8) NULL,
	[Filler] [nvarchar](76) NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OCADATA] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OAGDATA] ADD  CONSTRAINT [DF_OAGDATA_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO


