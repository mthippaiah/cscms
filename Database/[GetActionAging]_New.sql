USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionAging]    Script Date: 07/11/2011 09:58:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetActionAging]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetActionAging]
GO

USE [CSCMS]
GO

/****** Object:  UserDefinedFunction [dbo].[GetActionAging]    Script Date: 07/11/2011 09:58:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetActionAging]
	(@ActionGUID uniqueidentifier) RETURNS int
AS
BEGIN
	DECLARE @Aging int
	DECLARE @LastNPServiceDate DATE
	DECLARE @DispositionRenderedDate DATE
	DECLARE @StopDate DATE
	declare @arrest_date date
	declare @sotype Varchar(50)
	declare @soordereddate date
	declare @capiasexists bit
	declare @withdrwanexists bit
	declare @to_exists bit
	
	set @capiasexists = 0
	set @withdrwanexists = 0
	set @to_exists = 0
	set @arrest_date = null
	set @soordereddate = null
	set @DispositionRenderedDate = null
	SET @Aging = null
	SET @StopDate = GETDATE()
		
	-- IF THERE IS NO NP, return null		
	IF NOT EXISTS(SELECT ActionGUID FROM ActionPerson WHERE ActionGUID=@ActionGUID AND NPFlag = 1) 
	BEGIN
		SET @Aging = null -- RETURN VALUES NULL
	END		
   	ELSE
	BEGIN
		-- IF THERE IS NP with no service, return null
		IF (exists (select a.ActionGUID from [Action] a 
				inner join ActionType at on a.ActionTypeID = at.ActionTypeID
				inner join ActionPerson pa on a.ActionGUID = pa.ActionGUID
				LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
				where a.ActionGUID = @ActionGUID and s.PersonGUID IS null AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1 and at.ActionTypeName not like 'UIFSA%') 
	   or 
	   exists (select a.ActionGUID from [Action] a 
				inner join ActionType at on a.ActionTypeID = at.ActionTypeID
				inner join ActionPerson pa on a.ActionGUID = pa.ActionGUID
				LEFT JOIN [Service] s ON pa.ActionGUID = s.ActionGUID AND pa.PersonGUID = s.PersonGUID AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1
				where a.ActionGUID = @ActionGUID and s.PersonGUID IS null AND pa.ActionGUID = @ActionGUID AND pa.NPFlag = 1 and pa.CustodialTypeID = 1 and at.ActionTypeName like 'UIFSA%') 
		   )
		BEGIN
			SET @Aging = null -- RETURN VALUES NULL
		END
		ELSE
		BEGIN
		
			-- Get latest NP service date
			SELECT @LastNPServiceDate = dbo.GetActionServiceDateonLastNP(@ActionGUID)
			
			if exists(select top 1 SpecialOrderID from SpecialOrder so
				                               inner join SpecialOrderType sot on so.SpecialOrderTypeID = sot.SpecialOrderTypeID
				                               where so.ActionGUID = @ActionGUID 
				                               and sot.SpecialOrderTypeName in ('Capias - FTA', 'Capias - Attachment', 'Capias - MTRP', 'Commitment')
				                               order by so.OrderedDate desc)
			begin
				set @capiasexists = 1
				SELECT @arrest_date=ArrestCommitmentEffectiveDate, @sotype=sot.SpecialOrderTypeName, @soordereddate=so.OrderedDate  from SpecialOrder so 
					inner join SpecialOrderType sot on sot.SpecialOrderTypeID = so.SpecialOrderTypeID
					where so.ActionGUID = @ActionGUID 
						  and so.SpecialOrderID = (select top 1 SpecialOrderID from SpecialOrder so2
												   inner join SpecialOrderType sot2 on so2.SpecialOrderTypeID = sot2.SpecialOrderTypeID
												   where so2.ActionGUID = @ActionGUID 
												   and sot2.SpecialOrderTypeName in ('Capias - FTA', 'Capias - Attachment', 'Capias - MTRP', 'Commitment')
												   order by so2.OrderedDate desc)
			end     
			
			if @capiasexists = 1 and exists(select top 1 SpecialOrderID from SpecialOrder so
				                               inner join SpecialOrderType sot on so.SpecialOrderTypeID = sot.SpecialOrderTypeID
				                               where so.ActionGUID = @ActionGUID 
				                               and sot.SpecialOrderTypeName in ('Capias - Withdrawn','Writ Withdrawn')
				                               and so.OrderedDate >= @soordereddate
				                               order by so.OrderedDate desc)
			begin
				set @withdrwanexists = 1
				SELECT @arrest_date=ArrestCommitmentEffectiveDate, @sotype=sot.SpecialOrderTypeName, @soordereddate=so.OrderedDate  from SpecialOrder so 
					inner join SpecialOrderType sot on sot.SpecialOrderTypeID = so.SpecialOrderTypeID
					where so.ActionGUID = @ActionGUID 
						  and so.SpecialOrderID = (select top 1 SpecialOrderID from SpecialOrder so2
												   inner join SpecialOrderType sot2 on so2.SpecialOrderTypeID = sot2.SpecialOrderTypeID
												   where so2.ActionGUID = @ActionGUID 
												   and so2.OrderedDate >= @soordereddate
												   and sot2.SpecialOrderTypeName in ('Capias - Withdrawn','Writ Withdrawn')
												   order by so2.OrderedDate desc)
			end
			
			
			if exists(select top 1 SpecialOrderID from SpecialOrder so
				                               inner join SpecialOrderType sot on so.SpecialOrderTypeID = sot.SpecialOrderTypeID
				                               where so.ActionGUID = @ActionGUID 
				                               and sot.SpecialOrderTypeName in ('Temporary Order (Stops Aging)')
				                               order by so.OrderedDate desc)
			begin
				set @to_exists = 1
				SELECT @arrest_date=ArrestCommitmentEffectiveDate, @sotype=sot.SpecialOrderTypeName, @soordereddate=so.OrderedDate  from SpecialOrder so 
					inner join SpecialOrderType sot on sot.SpecialOrderTypeID = so.SpecialOrderTypeID
					where so.ActionGUID = @ActionGUID 
						  and so.SpecialOrderID = (select top 1 SpecialOrderID from SpecialOrder so2
												   inner join SpecialOrderType sot2 on so2.SpecialOrderTypeID = sot2.SpecialOrderTypeID
												   where so2.ActionGUID = @ActionGUID 
												   and sot2.SpecialOrderTypeName in ('Temporary Order (Stops Aging)')
												   order by so2.OrderedDate desc)
			end     
			
			select @DispositionRenderedDate = DispositionRenderedDate FROM [Action] WHERE ActionGUID = @ActionGUID
					
	      
			IF EXISTS(select a.DispositionTypeID from [Action] a 
				where a.ActionGUID=@ActionGUID and a.DispositionTypeID is not null)
			begin
					if @DispositionRenderedDate IS NOT NULL and @withdrwanexists = 1 and (@sotype = 'Capias - Withdrawn' or @soType = 'Writ Withdrawn')
					begin
						if  @soordereddate is not null and @soordereddate < @DispositionRenderedDate 
						begin
							SELECT @StopDate = DATEADD(d,1,@DispositionRenderedDate)
							set @Aging= DATEDIFF(d,@soordereddate,@StopDate)
					    end

						if  @soordereddate is not null and @soordereddate > @DispositionRenderedDate 
						begin
							set @Aging= DATEDIFF(d,@LastNPServiceDate,@DispositionRenderedDate)
					    end
					end
					else IF @DispositionRenderedDate IS NOT NULL and  @capiasexists = 1 and @arrest_date is not null and @sotype <> 'Capias � Compliance' and @soType <> 'Capias - Deferred Commitment' 
					begin
						if  @arrest_date < @DispositionRenderedDate 
						begin
							SELECT @StopDate = DATEADD(d,1,@DispositionRenderedDate)
							set @Aging= DATEDIFF(d,@arrest_date,@StopDate)
					    end

						if  @arrest_date > @DispositionRenderedDate 
						begin
							set @Aging= DATEDIFF(d,@LastNPServiceDate,@DispositionRenderedDate)
					    end
					end
					else if @DispositionRenderedDate IS NOT NULL 
					begin
						set @Aging= DATEDIFF(d,@LastNPServiceDate,@DispositionRenderedDate)
					end
			end
			else if @to_exists = 1 and @LastNPServiceDate is not null
			begin
				set @Aging= DATEDIFF(d,@LastNPServiceDate,@soordereddate)
			end
			else if @withdrwanexists = 1 and (@sotype = 'Capias - Withdrawn' or @soType = 'Writ Withdrawn')
			begin
				SELECT @StopDate = DATEADD(d,1,getdate())
				set @Aging= DATEDIFF(d,@soordereddate,@StopDate)
			end
			else if @capiasexists = 1 and @arrest_date is not null and @sotype <> 'Capias � Compliance' and @soType <> 'Capias - Deferred Commitment' 
			begin
					SELECT @StopDate = DATEADD(d,1,getdate())
					set @Aging= DATEDIFF(d,@arrest_date,@StopDate)
			end
			else if @capiasexists = 1 and @arrest_date is null and (@sotype = 'Capias - FTA'  OR  @soType = 'Capias - Attachment' OR @soType = 'Capias - MTRP' OR @soType = 'Commitment' ) 
			begin
				set @Aging= 0
			end
			else if @LastNPServiceDate is not null
			begin
				set @Aging= DATEDIFF(d,@LastNPServiceDate,getdate())
			end	
		end
	end	
	RETURN @Aging

END



GO


