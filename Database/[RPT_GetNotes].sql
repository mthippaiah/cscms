USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetNotes]    Script Date: 01/31/2011 10:50:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RPT_GetNotes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[RPT_GetNotes]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[RPT_GetNotes]    Script Date: 01/31/2011 10:50:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ravi Nanjundappa
-- Create date: 10/13/2010
-- Description:	Gets the Notes for a CauseID
-- =============================================
CREATE PROCEDURE [dbo].[RPT_GetNotes] 
	@CauseID uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT cn.CreateDate, note.NoteText   FROM [CauseNote] cn
		inner join note on note.NoteID = cn.NoteID
		inner join NoteType nt on nt.NoteTypeID = cn.NoteTypeID
	where cn.CauseGUID = @CauseID
 
END


GO


