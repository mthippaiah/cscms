USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetReminderByUserID]    Script Date: 03/23/2011 10:17:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReminderByUserID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetReminderByUserID]
GO

USE [CSCMS]
GO

/****** Object:  StoredProcedure [dbo].[GetReminderByUserID]    Script Date: 03/23/2011 10:17:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




--exec GetReminderByUserID @OCACourtID=37,@UserID=70
-- CREATE PROCEDURE [dbo].[GetReminderByUserID]
CREATE PROCEDURE [dbo].[GetReminderByUserID]
	@OCACourtID int
	,@UserID int
	,@All bit = NULL
	,@ShowExpried bit = NULL
AS
BEGIN

	Declare @SQLQuery AS NVarchar(2000)

	Set @SQLQuery = 'SELECT r.ReminderID 
		,r.UserId
		, r.OCACourtID 
		,r.ReminderNote 
		,r.StartDate
		,r.EffectiveDate 
		,r.CauseGUID 
		,r.Inactive 
		,r.CreateDate 
		,r.LastUpdateDate 
		,r.[Version] 
		,c.CauseNumber
		,case when r.CauseGUID is null then convert(bit,0)
		else convert(bit,1) end as CourtScope
		, r.CauseGUID 
		, c.NumberOfAction 
		,  CONVERT(int, DATEDIFF(d, GETDATE(), r.EffectiveDate  ))  AS DaysToEffective
		FROM Reminder r LEFT JOIN Cause c ON r.CauseGUID = c.CauseGUID
		WHERE (r.UserId = ' + CAST(@UserID AS NVARCHAR(10)) + 
		      ' OR (r.UserId <> ' + CAST(@UserID AS NVARCHAR(10)) +
		      ' and r.OCACourtID = ' + CAST(@OCACourtID AS NVARCHAR(10)) +
		      ' and r.CauseGUID is not null))'
		          
		

    --Show only current reminders
	if @All is null   
		Set @SQLQuery = @SQLQuery + ' and (r.StartDate is null or r.StartDate <= GETDATE())
		AND (r.EffectiveDate is null or r.EffectiveDate >= GETDATE() )
		ORDER BY EffectiveDate'

	-- show all reminders including expired
	if @All = 1 and (@ShowExpried is null or @ShowExpried = 1)
	Set @SQLQuery = @SQLQuery + ' ORDER BY EffectiveDate'

	-- show all reminders except expired
	if @All = 1 and @ShowExpried is not null and @ShowExpried = 0
		Set @SQLQuery = @SQLQuery + ' AND (r.EffectiveDate is null or r.EffectiveDate >= GETDATE() )
		ORDER BY EffectiveDate'

	-- show only expired
	if @All = 0 and @ShowExpried is not null and @ShowExpried = 1
		Set @SQLQuery = @SQLQuery + ' AND (r.EffectiveDate is not null and r.EffectiveDate < GETDATE() )
		ORDER BY EffectiveDate'

	EXECUTE(@SQLQuery)
	
End




GO


