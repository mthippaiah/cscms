USE [CSCMS]
GO
/****** Object:  StoredProcedure [dbo].[SearchByCase]    Script Date: 12/07/2011 09:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- CREATE PROCEDURE [dbo].[SearchByCase]

ALTER PROCEDURE [dbo].[SearchByCase]
(
	@UserId int,
	@CauseNumber nvarchar(100) = NULL,
	@OAGNumber nvarchar(20) = NULL,
	@Style nvarchar(100) = NULL, 							  
	@FilingFromDate date = NULL,
	@FilingToDate date = NULL,
	@CountyID smallint = NULL,
	@CaseStatusOpen nvarchar(1) = NULL,
	@CaseDisposed nvarchar(1)= NULL,
	@ActionType nvarchar(100) = NULL, -- NULL = no value specified, 0 = closed, 1 = open
	@LNPServiced bit = NULL,
	@FutureHearingSet bit = NULL
)
AS
BEGIN
	declare @temp table
	(
		CauseGUID uniqueidentifier not null,
		CauseNumber nvarchar(50) not null,
		OAGCauseNumber nchar(50) null,
		Style nvarchar(50) null,
		CountyName nvarchar(50) not null,
		LastActionFiledDate date null,
		CauseStatus char(1) null,
		NumberOfAction smallint null,
		ActionTypeName nvarchar(50) not null,
		ActionNumber smallint not null,
		LNPServiceDate Date null,
		FutureHearingDate Date null
	)
	-- [SearchByCase]  1, null,null,null,null,null,'A'	
	-- [SearchByCase]  1, null,null,null,null,null,NULL	
	-- [SearchByCase]  1, null,null,null,null,null,'I'	
	
	-- NOTES: 
	-- if @CauseNumber IS NOT NULL, SEARCH BY @CauseNumber ONLY ?
	-- IF @OAGNumber IS :D, SEARCH FOR DUPLICATE @OAGNumber ONLY
	-- IF @OAGNumber IS NOT NULL AND NOT :D, SEARCH BY @OAGNumber ONLY
	
	SET NOCOUNT ON;

	DECLARE @UserTypeID smallint
	DECLARE @Restricted bit = 0
	
	SELECT @UserTypeID = UserTypeID FROM [User] WHERE UserId = @UserId

	IF @UserTypeID IN (3,4) SET @Restricted = 1

	set @CauseNumber = RTRIM(LTRIM(@CauseNumber))

	set @CauseNumber = REPLACE(REPLACE(REPLACE(REPLACE(@CauseNumber,',',''),'-','') ,')',''),'(','')
	set @CauseNumber = REPLACE(REPLACE(REPLACE(REPLACE(@CauseNumber,'/',''),'&','') ,';',''),'.','')
	set @CauseNumber = REPLACE(REPLACE(REPLACE(REPLACE(@CauseNumber,' ',''),'&','') ,';',''),'.','')

	-- IF @OAGNumber IS :D, SEARCH FOR DUPLICATE @OAGNumber ONLY
	IF @OAGNumber=':D'
	BEGIN
		SELECT DISTINCT 
			c.CauseGUID
			,c.CauseNumber
			,a.OAGCauseNumber
			,a.Style
			,co.CountyName
			,c.LastActionFiledDate
			,c.CauseStatus
			,c.NumberOfAction
			,at.ActionTypeName
			,a.ActionNumber
		FROM Cause c
			JOIN Action a ON (c.CauseGUID = a.CauseGUID AND c.NumberOfAction = a.ActionNumber ) -- join last action only
			JOIN County co ON c.CountyID = co.CountyID
			JOIN UserCourt uc ON c.OCACourtID = uc.OCACourtID
			AND uc.UserID = @UserId
			JOIN ActionType at ON a.ActionTypeID=at.ActionTypeID
		WHERE
			uc.UserID = @UserId AND 
			(@Restricted = 0 OR uc.UserID IS NOT NULL) AND
			(@FilingFromDate IS NULL OR c.LastActionFiledDate >= @FilingFromDate) AND
			(@FilingToDate IS NULL OR c.LastActionFiledDate <= @FilingToDate) AND
			(@CountyID IS NULL OR c.CountyID = @CountyID) AND
			--(@CaseStatusOpen IS NULL OR (@CaseStatusOpen = a.ActionStatus)) AND
			--(@CaseDisposed IS NULL OR (@CaseDisposed = a.ActionDisposed)) AND 
			--(@ActionType IS NULL OR (@ActionType = a.ActionTypeID)) AND
			c.OAGCauseNumber IN
			(
			SELECT OAGCauseNumber FROM (SELECT COUNT(*) AS CNT , OAGCauseNumber FROM [Cause] c1
				WHERE c1.OAGCauseNumber = c.OAGCauseNumber and c1.OAGCauseNumber IS NOT NULL
				GROUP BY OAGCauseNumber HAVING COUNT(*) > 1) X
			)
			ORDER BY OAGCauseNumber, CauseNumber
	END
	
	-- IF @OAGNumber IS NOT NULL AND NOT :D, SEARCH BY @OAGNumber ONLY - 
	-- USER ENTER COMPLETED OAG NUMBER
	ELSE IF @OAGNumber IS NOT NULL AND LEN(@OAGNumber) = 10
	BEGIN
		-- WHEN USER ENTER AN EXACT OAGNUMBER AND CAUSE NUMBER, THEN WE WILL NOT SEARCH FOR THAT COURT BUT SEARCH FOR ANY COURT TO RETURN 
		-- THE NOTE OF THE CASE IN A NEW WINDOW
		
		IF @CauseNumber IS NOT NULL 
		BEGIN
			SELECT TOP 1  
				c.CauseGUID
				,c.CauseNumber
				,a.OAGCauseNumber
				,a.Style
				,co.CountyName
				,c.LastActionFiledDate
				,c.CauseStatus
				,c.NumberOfAction
				,at.ActionTypeName
				,a.ActionNumber
			FROM Cause c
				JOIN Action a ON (c.CauseGUID = a.CauseGUID AND c.NumberOfAction = a.ActionNumber  ) -- join last action 
				JOIN County co ON c.CountyID = co.CountyID
				JOIN ActionType at ON a.ActionTypeID=at.ActionTypeID
			WHERE
				(a.OAGCauseNumber = @OAGNumber or a.OAGCauseNumber2 = @OAGNumber or a.OAGCauseNumber3 = @OAGNumber)
				AND CauseNumberClean = @CauseNumber
				AND ISNULL(SharedWithOtherCourt,0) = 1
		END
		ELSE
		BEGIN
 			SELECT DISTINCT 
				c.CauseGUID
				,c.CauseNumber
				,a.OAGCauseNumber
				,a.Style
				,co.CountyName
				,c.LastActionFiledDate
				,c.CauseStatus
				,c.NumberOfAction
				,at.ActionTypeName
				,a.ActionNumber
			FROM Cause c
				JOIN Action a ON (c.CauseGUID = a.CauseGUID AND c.NumberOfAction = a.ActionNumber ) -- Join Last Action
				JOIN County co ON c.CountyID = co.CountyID
				JOIN UserCourt uc ON c.OCACourtID = uc.OCACourtID
				AND uc.UserID = @UserId
				JOIN ActionType at ON a.ActionTypeID=at.ActionTypeID
			WHERE
				uc.UserID = @UserId AND 
				(@Restricted = 0 OR uc.UserID IS NOT NULL) AND
				(a.OAGCauseNumber = @OAGNumber or a.OAGCauseNumber2 = @OAGNumber or a.OAGCauseNumber3 = @OAGNumber)
				--(@CaseStatusOpen IS NULL OR (@CaseStatusOpen = a.ActionStatus)) AND
				--(@CaseDisposed IS NULL OR (@CaseDisposed = a.ActionDisposed)) AND 
				--(@ActionType IS NULL OR (@ActionType = a.ActionTypeID)) AND
				ORDER BY CauseNumber

		END
	END
	
	-- IF @OAGNumber IS NOT NULL AND NOT :D, SEARCH BY @OAGNumber ONLY
	ELSE IF @OAGNumber IS NOT NULL
	BEGIN
 		SELECT DISTINCT 
			c.CauseGUID
			,c.CauseNumber
			,a.OAGCauseNumber
			,a.Style
			,co.CountyName
			,c.LastActionFiledDate
			,c.CauseStatus
			,c.NumberOfAction
			,at.ActionTypeName
			,a.ActionNumber
		FROM Cause c
			JOIN Action a ON (c.CauseGUID = a.CauseGUID AND c.NumberOfAction = a.ActionNumber ) -- join last action only
			JOIN County co ON c.CountyID = co.CountyID
			JOIN UserCourt uc ON c.OCACourtID = uc.OCACourtID
			JOIN ActionType at ON a.ActionTypeID=at.ActionTypeID
		WHERE
			(uc.UserID = @UserId or c.SharedWithOtherCourt = 1) AND 
			(@Restricted = 0 OR uc.UserID IS NOT NULL) AND
			c.OAGCauseNumber LIKE '%' + @OAGNumber + '%' AND 
			(@FilingFromDate IS NULL OR c.LastActionFiledDate >= @FilingFromDate) AND
			(@FilingToDate IS NULL OR c.LastActionFiledDate <= @FilingToDate) AND
			(@CountyID IS NULL OR c.CountyID = @CountyID) 
			--(@CaseStatusOpen IS NULL OR (@CaseStatusOpen = a.ActionStatus)) AND
			--(@CaseDisposed IS NULL OR (@CaseDisposed = a.ActionDisposed)) AND 
			--(@ActionType IS NULL OR (@ActionType = a.ActionTypeID)) AND
			
			ORDER BY OAGCauseNumber, CauseNumber
	END
	ELSE IF @CauseNumber IS NOT NULL
	BEGIN
		SELECT DISTINCT TOP 500
			c.CauseGUID
			,c.CauseNumber
			,a.OAGCauseNumber
			,a.Style
			,co.CountyName
			,c.LastActionFiledDate
			,c.CauseStatus
			,c.NumberOfAction
			,at.ActionTypeName
			,a.ActionNumber
		FROM Cause c
			JOIN Action a ON (c.CauseGUID = a.CauseGUID AND c.NumberOfAction = a.ActionNumber ) -- join last action only
			JOIN ActionType at ON a.ActionTypeID=at.ActionTypeID
			JOIN County co ON c.CountyID = co.CountyID
			LEFT JOIN UserCourt uc ON c.OCACourtID = uc.OCACourtID
		WHERE
			(uc.UserID = @UserId OR @Restricted = 0 or c.SharedWithOtherCourt = 1) AND 
			(@Restricted = 0 OR uc.UserID IS NOT NULL) AND
			(c.CauseNumberClean LIKE '%' + @CauseNumber + '%') 
		ORDER BY CauseNumber
	END

	ELSE
	BEGIN

		Declare @IsActive bit
		If(@CaseDisposed = 'P' and @CaseStatusOpen = 'A')
			Set @IsActive = null 

		If(@CaseDisposed = 'D' and @CaseStatusOpen = 'A')
			Set @IsActive = 1	
	
		INSERT into @temp
		select distinct c.CauseGUID
			,c.CauseNumber
			,a.OAGCauseNumber
			,a.Style
			,co.CountyName
			,c.LastActionFiledDate
			,c.CauseStatus
			,c.NumberOfAction
			,at.ActionTypeName
			,a.ActionNumber
			,dbo.GetActionServiceDateonLastNP(a.ActionGUID)
			,(select top 1 h.HearingDate from Hearing h
												where h.ActionGUID = a.ActionGUID
												and h.HearingDate >= GETDATE())
		FROM Cause c
			JOIN Action a ON (c.CauseGUID = a.CauseGUID)
			JOIN ActionType at ON a.ActionTypeID=at.ActionTypeID
			JOIN County co ON c.CountyID = co.CountyID
			LEFT JOIN UserCourt uc ON c.OCACourtID = uc.OCACourtID
			AND uc.UserID = @UserId
		WHERE
			(uc.UserID = @UserId OR @Restricted = 0) AND 
			(@Restricted = 0 OR uc.UserID IS NOT NULL) AND
			--(@CauseNumber IS NULL OR c.CauseNumberClean LIKE '%' + @CauseNumber + '%') AND
			--(@OAGNumber IS NULL OR (a.OAGCauseNumber LIKE '%' + @OAGNumber + '%') 
			--	OR (a.OAGCauseNumber2 LIKE '%' + @OAGNumber + '%') 
			--	OR (a.OAGCauseNumber3 LIKE '%' + @OAGNumber + '%')  ) AND
			(@Style IS NULL OR a.Style LIKE '%' + @Style + '%') AND
			(@FilingFromDate IS NULL OR c.LastActionFiledDate >= @FilingFromDate) AND
			(@FilingToDate IS NULL OR c.LastActionFiledDate <= @FilingToDate) AND
			(@CountyID IS NULL OR c.CountyID = @CountyID) AND
			(@CaseStatusOpen IS NULL OR (@CaseStatusOpen = 'A' and @CaseDisposed='D' and a.Inactive = 1) OR (@CaseStatusOpen = 'A' and @CaseDisposed='P' and (a.Inactive is null or a.Inactive = 0)) OR (@CaseStatusOpen = 'I' and a.DispositionRenderedDate is not null)) AND		
			(@CaseDisposed IS NULL OR (@CaseDisposed = 'D' and a.DispositionRenderedDate is not null) or (@CaseDisposed = 'P' and a.DispositionRenderedDate is null)) AND 
			(@ActionType IS NULL OR (@ActionType = a.ActionTypeID))
			
		SELECT DISTINCT TOP 500
			CauseGUID
			,CauseNumber
			,OAGCauseNumber
			,Style
			,CountyName
			,LastActionFiledDate
			,CauseStatus
			,NumberOfAction
			,ActionTypeName
			,ActionNumber
		FROM @temp T1
		WHERE
			(@LNPServiced IS NULL OR (@LNPServiced = 1 and T1.LNPServiceDate is not null) OR (@LNPServiced = 0 and T1.LNPServiceDate is null)) and
			(@FutureHearingSet is null OR (@FutureHearingSet = 1 and T1.FutureHearingDate is not null ) OR (@FutureHearingSet = 0 and T1.FutureHearingDate is null))
		ORDER BY CauseNumber
	END
	--OR a.OAGCauseNumber2 LIKE '%' + @OAGNumber + '%' OR a.OAGCauseNumber3 LIKE '%' + @OAGNumber + '%'
    --lblStatus.Text = dc.GetActionStatus(this.CurrentAction.ActionGUID);
    --lblPending.Text = dc.GetActionPrimaryStatus(this.CurrentAction.ActionGUID);
   
            
END



